#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This script loads census tract information for patients in emr_patient to the gen_pop_tools.patient_census_tract Table
# Created for the Allegheny County Project.  The input file has 3 fields, patient_natural_key, mrn, census_tract 
# currently the mrn is not used.
# Written by Jmiller 9-14-2020
#
import sys
import argparse
import psycopg2
from psycopg2.extensions import adapt

parser = argparse.ArgumentParser(description="read an input file with patient_id, mrn, census tract and lookup the patient_id and insert a row into the gen_pop_tools.patient_census_tract table for each row in the text file.\n" +
    "Usage: LoadCensusTract.py <database_url> <database_password> <input_text_file>")
parser.add_argument("url")
parser.add_argument("password")
parser.add_argument("input_text_file")
aArg = parser.parse_args()

sUrl = aArg.url
sPassword = aArg.password
sFileName = aArg.input_text_file

try:
    # connect to the database
    sError = "Could not connect to the specified database."
    conn = None
    conn = psycopg2.connect(host=sUrl, dbname='esp', user='esp', password=sPassword)
    conn.set_session(autocommit=True)
    cursor = conn.cursor()
    
    # open the input file
    sError = "Problem reading from file: " + sFileName
    file = open(sFileName, "r")
    asLine = file.readlines()
    file.close()

    # For each row - lookup the patient_id based on the natural_key
    for line in asLine:
        #sline = line.strip()
        aline = line.split('^')
        #sNatKey = 'FAKE-JJM012' 
        sNatKey = aline[0]
        sCensusTract = aline[2]
        #sNatKey = sNatKey.strip()
        #sCensusTract = sCensusTract.strip()
        
        # escape the description for sql
        #sCensusTract = str(adapt(sCensusTract))
        
        sys.stderr.write( "looking up patient with natural key" + sNatKey + "\n")
        sys.stdout.write( sNatKey + "\n")
        sys.stdout.write( sCensusTract + "\n")
        # add the code and description to the database
        try:
            ##sQry = ("insert into gen_pop_tools.patient_census_tract (patient_id, census_tract_id) SELECT (select id from emr_patient where natural_key = '" + sNatKey + "') , '" + sCensusTract + "' WHERE NOT EXISTS (select patient_id from gen_pop_tools.patient_census_tract c, emr_patient p where c.patient_id=p.id and p.natural_key = '" + sNatKey + "');")
            sQry = ("insert into gen_pop_tools.patient_census_tract (patient_id, census_tract_id) SELECT (select id from emr_patient where natural_key = '" + sNatKey + "') , '" + sCensusTract + "' WHERE NOT EXISTS (select patient_id from gen_pop_tools.patient_census_tract c, emr_patient p where c.patient_id=p.id and p.natural_key = '" + sNatKey + "');")
            #sys.stderr.write( sQry + "\n")

            cursor.execute(sQry)
        except Exception as e:
            sys.stderr.write("***" + "Error executing sql: " + sQry + "\n")
            sys.stderr.write("***" + str(e) + "\n")

except Exception as e:
    sys.stderr.write("***" + sError + "\n")
    sys.stderr.write("***" + str(e) + "\n")
    sys.exit(1)

finally:
    if conn:
        conn.close()

sys.exit(0)

