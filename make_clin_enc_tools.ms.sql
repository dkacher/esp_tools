if object_id('gen_pop_tools.clin_enc' , 'U') is not null drop table gen_pop_tools.clin_enc;
create table gen_pop_tools.clin_enc
( patient_id integer
 ,date date
 ,source varchar(3),
 primary key(patient_id, date, source));

if object_id('gen_pop_tools.make_clin_enc', 'P') is not null drop procedure gen_pop_tools.make_clin_enc;
Go
create procedure gen_pop_tools.make_clin_enc(
        @p_pulldate date)
as
begin
  declare @e_date date;
  declare @pid int;
  --loop through lx, rx, dx, vs, immu tables by last updated timestamp and create clin_enc util table
  declare cursrec cursor
    for select distinct patient_id, date
        from emr_labresult
        where date=@p_pulldate;
  open cursrec
  fetch next from cursrec into @pid, @e_date;
  while @@FETCH_STATUS = 0
    begin
      insert into gen_pop_tools.clin_enc (patient_id, date, source)
        values (@pid, @e_date, 'lx');
      fetch next from cursrec into @pid, @e_date;
    end;
  close cursrec;
  deallocate cursrec;

  declare cursrec cursor
    for select distinct patient_id, date
        from emr_prescription
        where date=@p_pulldate;
  open cursrec
  fetch next from cursrec into @pid, @e_date;
  while @@FETCH_STATUS = 0
    begin
      insert into gen_pop_tools.clin_enc (patient_id, date, source)
        values (@pid, @e_date, 'rx');
      fetch next from cursrec into @pid, @e_date;
    end;
  close cursrec;
  deallocate cursrec;
  
  declare cursrec cursor
    for select distinct patient_id, date
        from emr_immunization
        where date=@p_pulldate;
  open cursrec
  fetch next from cursrec into @pid, @e_date;
  while @@FETCH_STATUS = 0
    begin
      insert into gen_pop_tools.clin_enc (patient_id, date, source)
        values (@pid, @e_date, 'imm');
      fetch next from cursrec into @pid, @e_date;
    end;
  close cursrec;
  deallocate cursrec;

  declare cursrec cursor
    for select distinct patient_id, date
        from emr_encounter e
        where ((e.weight>0 or e.height>0
               or e.bp_systolic>0 or e.bp_diastolic>0
               or e.temperature>0 or e.pregnant=1 or e.edd is not null)
               or exists (select null from emr_encounter_dx_codes dx
                          where dx.encounter_id=e.id and dx.dx_code_id<>'icd9:799.9'))
               and date=@p_pulldate;
  open cursrec
  fetch next from cursrec into @pid, @e_date;
  while @@FETCH_STATUS = 0
    begin
      insert into gen_pop_tools.clin_enc (patient_id, date, source)
        values (@pid, @e_date, 'enc');
      fetch next from cursrec into @pid, @e_date;
    end;
  close cursrec;
  deallocate cursrec;

  select @p_pulldate as pulldate;
  return;
end;
