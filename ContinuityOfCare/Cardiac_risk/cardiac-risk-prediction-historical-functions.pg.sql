CREATE OR REPLACE FUNCTION gen_pop_tools.downfill_bool(
  intable character varying, 
  partby character varying, 
  orderby character varying, 
  col character varying, 
  bool character varying,
  enddate character varying DEFAULT 'year_month'::character varying, 
  extlim integer DEFAULT 23)
  RETURNS void AS
$BODY$
DECLARE
  holdval double precision;
  holdbool boolean;
  holddt varchar;
  patid int;
  incr int;
  cursrow record;
  loopsql varchar;
  sqltxt varchar;
  counter int :=0;
BEGIN
  loopsql := 'select distinct patient_id from ' || intable || ' order by patient_id';
  for patid in execute loopsql
  loop
    holdval := null;
    holdbool :=null;
    holddt := null;
    incr := 0;
    sqltxt := 'select ' || partby || ' as partid, ' || orderby || ' as orderid, ' || col
                         || ' as updtcol, ' || bool || ' as updtbool, ' || enddate || ' as enddate '
                         || 'from ' || intable || ' where patient_id=' || patid || ' order by '
                         || orderby;
    for cursrow in execute sqltxt
    loop
      if (cursrow.updtcol is null and holdval is not null and incr < extlim) then
        sqltxt := 'update ' || intable || ' set ' || col || '=''' || holdval || ''' , ' || bool || '=' || holdbool || ' where '
                          || partby || '=' || cursrow.partid || ' and ' || orderby || '='''
                          || cursrow.orderid || '''';
        execute sqltxt;
        if cursrow.orderid > holddt then
          incr := incr+1;
        end if;
      elsif (cursrow.updtcol is not null) then
        holdval:=cursrow.updtcol;
        holdbool:=cursrow.updtbool;
        holddt:=cursrow.enddate;
        incr := 0;
      else
        holdval := null;
        holddt := null;
        incr := 0;
      end if;
    end loop;
    counter := counter+1;
    if counter % 1000 = 0 then
      raise notice 'current patient count: %', counter;
    end if;
  end loop;
END;
$BODY$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION gen_pop_tools.downfill_dub(
  intable character varying, 
  partby character varying, 
  orderby character varying, 
  col character varying, 
  enddate character varying DEFAULT 'year_month'::character varying, 
  extlim integer DEFAULT 23)
  RETURNS void AS
$BODY$
DECLARE
  holdval double precision;
  holddt varchar;
  patid int;
  incr int;
  cursrow record;
  loopsql varchar;
  sqltxt varchar;
  counter int :=0;
BEGIN
  loopsql := 'select distinct patient_id from ' || intable || ' order by patient_id';
  for patid in execute loopsql
  loop
    holdval := null;
    holddt := null;
    incr := 0;
    sqltxt := 'select ' || partby || ' as partid, ' || orderby || ' as orderid, ' || col
                         || ' as updtcol, ' || enddate || ' as enddate '
                         || 'from ' || intable || ' where patient_id=' || patid || ' order by '
                         || orderby;
    for cursrow in execute sqltxt
    loop
      if (cursrow.updtcol is null and holdval is not null and incr < extlim) then
        sqltxt := 'update ' || intable || ' set ' || col || '=''' || holdval || ''' where '
                          || partby || '=' || cursrow.partid || ' and ' || orderby || '='''
                          || cursrow.orderid || '''';
        execute sqltxt;
        if cursrow.orderid > holddt then
          incr := incr+1;
        end if;
      elsif (cursrow.updtcol is not null) then
        holdval:=cursrow.updtcol;
        holddt:=cursrow.enddate;
        incr := 0;
      else
        holdval := null;
        holddt := null;
        incr := 0;
      end if;
    end loop;
    counter := counter+1;
    if counter % 1000 = 0 then
      raise notice 'current patient count: %', counter;
    end if;
  end loop;
END;
$BODY$
LANGUAGE plpgsql;

--see https://stackoverflow.com/questions/34075360/is-there-a-workaround-when-an-underflow-error-occurs-in-postgresql
create or replace function calculate_with_error
    (baseline double precision,
     ln_age double precision,
     ln_age2 double precision,
     ln_t_chol double precision,
     ln_ageXt_chol double precision,
     ln_hdl_c double precision,
     ln_ageXhdl_c double precision,
     ln_bp double precision,
     ln_agexbp double precision,
     cur_smk double precision,
     cur_smkXln_age double precision,
     diab double precision,
     mean double precision,
     rs_type varchar,
     patient_id integer)
returns double precision language plpgsql as $$
begin
    return case rs_type
       when 'fb' 
       then 1-baseline^exp((ln_age + ln_t_chol + ln_hdl_c + ln_ageXhdl_c + ln_bp + ln_agexbp + cur_smk + diab) - mean)
       when 'fo'
       then 1-baseline^exp((ln_age + ln_age2 + ln_t_chol + ln_ageXt_chol + ln_hdl_c + ln_ageXhdl_c + ln_bp + cur_smk + cur_smkXln_age + diab ) - mean)
       when 'mb' 
       then 1-baseline^exp((ln_age + ln_t_chol + ln_hdl_c + ln_bp + cur_smk + diab ) - mean)
       when 'mo'
       then 1-baseline^exp((ln_age + ln_t_chol + ln_ageXt_chol + ln_hdl_c + ln_ageXhdl_c + ln_bp + cur_smk + cur_smkXln_age + diab ) - mean)
   end;
    exception when numeric_value_out_of_range then 
        raise exception 'Value out of range for baseline = % ln_age= % ln_age2= % ln_t_chol= % ln_ageXt_chol= %
    ln_hdl_c= % ln_ageXhdl_c= % ln_bp= % ln_ageXbp= % cur_smk= %
    cur_smkXln_age= % diab= % mean= % rs_type= % patient_id= %', 
    baseline, ln_age, ln_age2, ln_t_chol, ln_ageXt_chol, 
    ln_hdl_c, ln_ageXhdl_c, ln_bp, ln_agexbp, cur_smk, 
    cur_smkXln_age, diab, mean, rs_type, patient_id;
end $$;


