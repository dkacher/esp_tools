IF object_id('gen_pop_tools.downfill_bool', 'P') IS NOT NULL DROP PROCEDURE gen_pop_tools.downfill_bool;
GO
CREATE PROCEDURE gen_pop_tools.downfill_bool(
  @intable varchar(100), 
  @partby varchar(100), 
  @orderby varchar(100), 
  @col varchar(100), 
  @bool varchar(100),
  @extlim varchar(100) = '24' 
  )
AS
  DECLARE @dynsql nvarchar(max)
  set @dynsql = '
  merge into ' + @intable + ' using 
  (select ' + @partby + ' partid, ' + @orderby + ' orderid, ' + @col + ' sourcecol, ' + @bool + ' boolcol, 
    case
      when coalesce(lead(' + @orderby + ') over (partition by ' + @partby + ' order by ' + @orderby + '), stuff(left(convert(varchar, getdate(),112),6),5,0,''_''))
           <= stuff(left(convert(varchar, dateadd(month, ' + @extlim + ', convert(datetime, replace(' + @orderby + ', ''_'', '''')+''01'', 112)),112),6),5,0,''_'')   
           then coalesce(lead(' + @orderby + ') over (partition by ' + @partby + ' order by ' + @orderby + '), stuff(left(convert(varchar, getdate(),112),6),5,0,''_''))
      else stuff(left(convert(varchar, dateadd(month, ' + @extlim + ', convert(datetime, replace(' + @orderby + ', ''_'', '''')+''01'', 112)),112),6),5,0,''_'')
      end nextdate
  from ' + @intable + '
  where ' + @col + ' is not null) as sourcetab
  on ' + @partby + '=partid and ' + @orderby + ' > orderid and ' + @orderby + ' < nextdate
  when matched then update
  set ' + @col + '=sourcecol, ' + @bool + '=boolcol;
  ';
  execute sp_executesql @dynsql;
GO


IF object_id('gen_pop_tools.downfill_dub', 'P') IS NOT NULL DROP PROCEDURE gen_pop_tools.downfill_dub;
GO
CREATE PROCEDURE gen_pop_tools.downfill_dub(
  @intable varchar(100), 
  @partby varchar(100), 
  @orderby varchar(100), 
  @col varchar(100), 
  @extlim varchar(100) = '24' 
  )
AS
  DECLARE @dynsql nvarchar(max)
  set @dynsql = '
  merge into ' + @intable +
  ' using
  (select ' + @partby + ' partid,  ' + @orderby + ' orderid,  ' + @col + ' sourcecol,
    case
      when coalesce(lead(' + @orderby + ') over (partition by ' + @partby + ' order by ' + @orderby + '), stuff(left(convert(varchar(max), getdate(),112),6),5,0,''_''))
           <= stuff(left(convert(varchar(max), dateadd(month, ' + @extlim + ', convert(datetime, replace(' + @orderby + ', ''_'', '''')+''01'', 112)),112),6),5,0,''_'')
           then coalesce(lead(' + @orderby + ') over (partition by ' + @partby + ' order by ' + @orderby + '), stuff(left(convert(varchar(max), getdate(),112),6),5,0,''_''))
      else stuff(left(convert(varchar(max), dateadd(month, ' + @extlim + ', convert(datetime, replace(' + @orderby + ', ''_'', '''')+''01'', 112)),112),6),5,0,''_'')
      end nextdate
  from ' + @intable + '
  where ' + @col + ' is not null) as sourcetab
  on ' + @partby + '=partid and ' + @orderby + ' > orderid and ' + @orderby + ' < nextdate
  when matched then update
  set ' + @col + '=sourcecol;
  ';
  execute sp_executesql @dynsql;
GO

--see https://stackoverflow.com/questions/34075360/is-there-a-workaround-when-an-underflow-error-occurs-in-postgresql
--calculate with error is not needed for ms sql as it will assign zero of underflow errors.
--CREATE PROCEDURE gen_pop_tools.calculate_with_error...
