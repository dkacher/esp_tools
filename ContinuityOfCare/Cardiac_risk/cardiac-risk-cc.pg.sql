set search_path to gen_pop_tools, public;

-- smoking records, select most recent status for each year 
drop table if exists cc_cr_smk;
create table cc_cr_smk as
select distinct on (patient_id, substr(year_month,1,4)) patient_id, smoking::integer, year_month, substr(year_month,1,4) rpt_yr
FROM tt_smoking
order by patient_id, substr(year_month,1,4), year_month desc;

-- smoking meds
drop table if exists cc_cr_smk_med;
create table cc_cr_smk_med as
select patient_id, to_char(date,'yyyy') rpt_yr
from emr_prescription
where lower(name) similar to '%(bupropion|zyban|aplenzin|wellbutrin|forfivo|nicotine|chantix|varenicline|nicoderm|habitrol|nicotrol|proStep|nicorette|commit)%'
group by patient_id, to_char(date,'yyyy');

/*
drop table if exists cc_cr_bupropion;
create table cc_cr_bupropion as
select patient_id, to_char(date,'yyyy') rpt_yr
from emr_prescription
where lower(name) similar to '%(bupropion|zyban|aplenzin|wellbutrin|forfivo)%'
group by patient_id, to_char(date,'yyyy');

drop table if exists cc_cr_nicotine;
create table cc_cr_nicotine as
select patient_id, to_char(date,'yyyy') rpt_yr
from emr_prescription
where name ilike '%nicotine%'
group by patient_id, to_char(date,'yyyy');
*/

-- a1c 
/*
drop table if exists cc_cr_ha1c;
create table cc_cr_ha1c as
select distinct patient_id, to_char(date,'yyyy') rpt_yr, result_float
from emr_labresult l
join conf_labtestmap m on m.native_code=l.native_code
where m.test_name='a1c' and result_float>0;
*/
-- most recent a1c record (last in year)
drop table if exists cc_cr_ha1c;
create table cc_cr_ha1c as
select distinct on (patient_id, to_char(date,'yyyy')) patient_id, to_char(date,'yyyy') rpt_yr, date, result_float
from emr_labresult l
join conf_labtestmap m on m.native_code=l.native_code
where m.test_name='a1c' and result_float>0 
order by patient_id, to_char(date,'yyyy'), date desc;

-- patients with known diabetes
drop table if exists cc_cr_diab;
create table cc_cr_diab as
select distinct on (patient_id, substr(year_month,1,4))
    patient_id, substr(year_month,1,4) rpt_yr, diabetes as diab
from gen_pop_tools.cr_diab_seq
where diabetes = 1
order by patient_id, substr(year_month,1,4), year_month desc;

drop table if exists cc_cr_antidiab;
create table cc_cr_antidiab as
select patient_id, to_char(date,'yyyy') rpt_yr
from emr_prescription
where lower(name) similar to '%(metformin|glyburide|pramlintide|exenatide|sitagliptin|meglitinide|nateglinide|repaglinide|glimepiride|glipizide|gliclazide|rosiglitizone|pioglitazone|glucagon|miglitol|insulin)%'
group by patient_id, to_char(date,'yyyy');

-- ldl
/*
drop table if exists cc_cr_ldl;
create table cc_cr_ldl as
select distinct patient_id, to_char(date,'yyyy') rpt_yr, result_float
from emr_labresult l
join conf_labtestmap m on m.native_code=l.native_code
where m.test_name='cholesterol-ldl' and result_float>0;
*/

-- most recent ldl record (last in year)
drop table if exists cc_cr_ldl;
create table cc_cr_ldl as
select distinct on (patient_id, to_char(date,'yyyy')) patient_id, to_char(date,'yyyy') rpt_yr, date, result_float
from emr_labresult l
join conf_labtestmap m on m.native_code=l.native_code
where m.test_name='cholesterol-ldl' and result_float>0 
order by patient_id, to_char(date,'yyyy'), date desc;

-- adding new lipid lowering medications (need to check if miss anything)
drop table if exists cc_cr_ldl_med;
create table cc_cr_ldl_med as
select patient_id, to_char(date,'yyyy') rpt_yr
from emr_prescription
where lower(name) similar to '%(ezetimibe|zetia)%'
  or lower(name) similar to '%(fenofibrate|trilipix|triglide|antara|lipofen|fibricor|fenoglide|fenofibric acid)%'
  or lower(name) similar to '%(gemfibrozil|lopid)%'
  or lower(name) similar to '%(atorvastatin|lipitor)%'
  or lower(name) similar to '%(fluvastatin|lescol)%'
  or lower(name) similar to '%(lovastatin|altoprev)%'
  or lower(name) similar to '%(pitavastatin|livalo)%'
  or lower(name) similar to '%(pravastatin|pravachol)%'
  or lower(name) similar to '%(rosuvastatin|crestor)%'
  or lower(name) similar to '%(simvastatin|zocor|flolipid)%'
  -- new meds
  or lower(name) similar to '%(tricor|antara|fenoglide|fibricor|lipidil|lipofen|lofibra)%' 
  or lower(name) similar to '%(niacin|nicotinic acid|niaspan|niacor)%' 
  or lower(name) similar to '%(cholestyramine|questran|prevalite|colesevelam|welchol|colestipol|colestid)%' 
  or lower(name) similar to '%(icosapent ethyl|vescepa|omega-3 acid ethyl esters|lovaza|omtryg|omega-3 carboxylic acids|epanova)%' 
  or lower(name) similar to '%(praluent|alirocumab|repatha|evolocumab)%' 
group by patient_id, to_char(date,'yyyy');

-- bp measured; select most recent sbp and dbp values for each year from tt_enc_bp table
drop table if exists cc_cr_bp;
create table cc_cr_bp as
select distinct on (patient_id, to_char(date,'yyyy')) patient_id, date, bp_systolic as sbp, bp_diastolic as dbp, to_char(date,'yyyy') rpt_yr
from gen_pop_tools.tt_enc_bp
order by patient_id, to_char(date,'yyyy'), date desc;

-- hypertension (currently using most recent dxhypertension status for each year)
drop table if exists cc_cr_hypert;
create table cc_cr_hypert as
select distinct c.patient_id, substr(seq.year_month,1,4) rpt_yr,
	   last_value(cah.status) over (partition by c.patient_id, substr(seq.year_month,1,4) order by strtdt range between current row and unbounded following) as hypertension
from nodis_case c
join tt_pat_seq_enc seq on seq.patient_id=c.patient_id
join (select case_id, status, date as strtdt, 
             case 
                when lead(date) over (partition by case_id order by date) is not null 
                   then lead(date) over (partition by case_id order by date)
                else now()
             end enddt 
      from nodis_caseactivehistory) cah on c.id=cah.case_id 
                                        and cah.strtdt<=to_date(seq.year_month,'yyyy_mm')
                                        and cah.enddt>=to_date(seq.year_month,'yyyy_mm')+interval '1 month'
where c.condition='diagnosedhypertension';


-- antihypertension med
drop table if exists cc_cr_antihypert;
create table cc_cr_antihypert as
select patient_id, to_char(date,'yyyy') rpt_yr 
           from cr_hist_antihypert_rx
group by patient_id, to_char(date,'yyyy');


-- bring everything together
drop table if exists cc_cr_plus_risk; 
create table cc_cr_plus_risk as
select t0.patient_id,
       t0.rpt_yr, 
       case  
	     when t0.cv_risk=1 then 'cv_risk_100'
         when t0.cv_risk>=.2 and t0.cv_risk<1 then 'cv_risk_20up'
         when t0.cv_risk>=.15 and t0.cv_risk<.20 then 'cv_risk_15_20'
         when t0.cv_risk>=.10 and t0.cv_risk<.15 then 'cv_risk_10_15'
         when t0.cv_risk>=.05 and t0.cv_risk<.10 then 'cv_risk_5_10'
         when t0.cv_risk<.05 then 'cv_risk_0_5'
		end as risk_percentile,
	   --smoking status recorded in the past 3 years
	   case when s.patient_id is not null or s2.patient_id is not null or s3.patient_id is not null then t0.patient_id else null end cv_risk_smk_rcd,
       --smoking: most recent smoking status as current smoker in the past 3 years
       case when (s.smoking=4 or (s.smoking is null and s2.smoking=4) or (s.smoking is null and s2.smoking is null and s3.smoking=4)) then t0.patient_id else null end cv_risk_smk,
	   --smoking + no smoking cessation medications in the past 3 years
       case when (s.smoking=4 or (s.smoking is null and s2.smoking=4) or (s.smoking is null and s2.smoking is null and s3.smoking=4))  
	   and (b.patient_id is null and b2.patient_id is null and b3.patient_id is null)
	   then t0.patient_id else null end cv_risk_smk_nobn,
       --A1c measured in the past 3 years
	   case when a1c.patient_id is not null or a1c2.patient_id is not null or a1c3.patient_id is not null then t0.patient_id else null end cv_risk_a1c,
	   --A1c measured and known diabetes 
	   case when diab=1 and (a1c.patient_id is not null or a1c2.patient_id is not null or a1c3.patient_id is not null) then t0.patient_id else null end cv_risk_diab,
	   --diabetes with most recent A1C >=9 
       case when diab=1 and (a1c.result_float>=9 or (a1c.result_float is null and a1c2.result_float >=9) or (a1c.result_float is null and a1c2.result_float is null and a1c3.result_float >=9))  
	   then t0.patient_id else null end cv_risk_diab_a1c,
       --A1c measured and diabetes with most recent A1C >=9 and no anti-diabetic medication in the past 3 years
       case when diab=1 and (a1c.result_float>=9 or (a1c.result_float is null and a1c2.result_float >=9) or (a1c.result_float is null and a1c2.result_float is null and a1c3.result_float >=9))   
	   and ad.patient_id is null and ad2.patient_id is null and ad3.patient_id is null then t0.patient_id else null end cv_risk_a1c_nt,
	   --ldl measured in the past 3 years
       case when ldl.patient_id is not null or ldl2.patient_id is not null or ldl3.patient_id is not null then t0.patient_id else null end cv_risk_ldl_rcd,
	   --ldl 160 most recent in the past 3 years
       case when(ldl.result_float>=160 or (ldl.result_float is null and ldl2.result_float>=160) or 
	   (ldl.result_float is null and ldl2.result_float is null and ldl3.result_float>=160)) then t0.patient_id else null end cv_risk_ldl,
       --ldl 160 and no lipid lowering meds
       case when(ldl.result_float>=160 or (ldl.result_float is null and ldl2.result_float>=160) or 
	   (ldl.result_float is null and ldl2.result_float is null and ldl3.result_float>=160)) and lm.patient_id is null and lm2.patient_id is null and lm3.patient_id is null
	   then t0.patient_id else null end cv_risk_ldl_ns,
       --bp measured in the past 3 years
	   case when bp.patient_id is not null or bp2.patient_id is not null or bp3.patient_id is not null then t0.patient_id else null end cv_risk_bp,
       --bp measured with active hypertension (based on diagnosedhypertension definition)
       case when h.hypertension in ('C', 'U', 'UK') and (bp.patient_id is not null or bp2.patient_id is not null or bp3.patient_id is not null) then t0.patient_id else null end cv_risk_hyp,
       --uncontrolled hypertension: most recent SBP >=140 mmHg or DBP >=90 mmHg after the start date of the active hypertension; 
	   case when h.hypertension ='U' and (bp.patient_id is not null or bp2.patient_id is not null or bp3.patient_id is not null) then t0.patient_id end as cv_risk_uchyp,
	   --uncontrolled hypertension and no meds
       case when h.hypertension='U' and (bp.patient_id is not null or bp2.patient_id is not null or bp3.patient_id is not null) and ah.patient_id is null and ah2.patient_id is null and ah3.patient_id is null then t0.patient_id else null end cv_risk_uchypu,
       case when upper(substr(p.gender,1,1)) = 'F' then 'Female'
            when upper(substr(p.gender,1,1)) = 'M' then 'Male'
            else 'Unknown'
       end sex,
       case 
         when (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity) is not null
           then (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity)
         when (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race) is not null
           then (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race)
         else 'UNKNOWN'
       end race_ethnicity,
       case 
         when age <=39 then '20-39'
         when age>=40 and age<=59 then '40-59'
         when age>=60 then '60 and over'
       end as agegroup            
from cc_cr_yearly_risk_score t0
JOIN emr_patient p ON t0.patient_id = p.id
-- smoking 
left join cc_cr_smk s on t0.patient_id=s.patient_id and t0.rpt_yr= s.rpt_yr
left join cc_cr_smk s2 on t0.patient_id=s2.patient_id and to_number(t0.rpt_yr,'9999')=to_number(s2.rpt_yr,'9999')+1
left join cc_cr_smk s3 on t0.patient_id=s3.patient_id and to_number(t0.rpt_yr,'9999')=to_number(s3.rpt_yr,'9999')+2
left join cc_cr_smk_med b on t0.patient_id=b.patient_id and t0.rpt_yr= b.rpt_yr
left join cc_cr_smk_med b2 on t0.patient_id=b2.patient_id and to_number(t0.rpt_yr,'9999')=to_number(b2.rpt_yr,'9999')+1
left join cc_cr_smk_med b3 on t0.patient_id=b3.patient_id and to_number(t0.rpt_yr,'9999')=to_number(b3.rpt_yr,'9999')+2
left join cc_cr_ha1c a1c on t0.patient_id=a1c.patient_id and t0.rpt_yr=a1c.rpt_yr
left join cc_cr_ha1c a1c2 on t0.patient_id=a1c2.patient_id and to_number(t0.rpt_yr,'9999')=to_number(a1c2.rpt_yr,'9999')+1
left join cc_cr_ha1c a1c3 on t0.patient_id=a1c3.patient_id and to_number(t0.rpt_yr,'9999')=to_number(a1c3.rpt_yr,'9999')+2
left join cc_cr_diab d on t0.patient_id = d.patient_id and t0.rpt_yr=d.rpt_yr
left join cc_cr_antidiab ad on t0.patient_id=ad.patient_id and t0.rpt_yr=ad.rpt_yr
left join cc_cr_antidiab ad2 on t0.patient_id=ad2.patient_id and to_number(t0.rpt_yr,'9999')=to_number(ad2.rpt_yr,'9999')+1
left join cc_cr_antidiab ad3 on t0.patient_id=ad3.patient_id and to_number(t0.rpt_yr,'9999')=to_number(ad3.rpt_yr,'9999')+2
left join cc_cr_ldl ldl on t0.patient_id=ldl.patient_id and t0.rpt_yr=ldl.rpt_yr
left join cc_cr_ldl ldl2 on t0.patient_id=ldl2.patient_id and to_number(t0.rpt_yr,'9999')=to_number(ldl2.rpt_yr,'9999')+1
left join cc_cr_ldl ldl3 on t0.patient_id=ldl3.patient_id and to_number(t0.rpt_yr,'9999')=to_number(ldl3.rpt_yr,'9999')+2
left join cc_cr_ldl_med lm on t0.patient_id=lm.patient_id and t0.rpt_yr=lm.rpt_yr
left join cc_cr_ldl_med lm2 on t0.patient_id=lm2.patient_id and to_number(t0.rpt_yr,'9999')=to_number(lm2.rpt_yr,'9999')+1
left join cc_cr_ldl_med lm3 on t0.patient_id=lm3.patient_id and to_number(t0.rpt_yr,'9999')=to_number(lm3.rpt_yr,'9999')+2
left join cc_cr_bp bp on t0.patient_id=bp.patient_id and to_number(t0.rpt_yr,'9999')=to_number(bp.rpt_yr,'9999')
left join cc_cr_bp bp2 on t0.patient_id=bp2.patient_id and to_number(t0.rpt_yr,'9999')=to_number(bp2.rpt_yr,'9999')+1
left join cc_cr_bp bp3 on t0.patient_id=bp3.patient_id and to_number(t0.rpt_yr,'9999')=to_number(bp3.rpt_yr,'9999')+2
left join cc_cr_hypert h on t0.patient_id=h.patient_id and t0.rpt_yr=h.rpt_yr
left join cc_cr_antihypert ah on t0.patient_id=ah.patient_id and t0.rpt_yr = ah.rpt_yr
left join cc_cr_antihypert ah2 on t0.patient_id=ah2.patient_id and to_number(t0.rpt_yr,'9999')=to_number(ah2.rpt_yr,'9999')+1
left join cc_cr_antihypert ah3 on t0.patient_id=ah3.patient_id and to_number(t0.rpt_yr,'9999')=to_number(ah3.rpt_yr,'9999')+2
where t0.cv_risk is not null;

--Now get the crossing of all stratifiers
drop table if exists cr_strat1;
create temporary table cr_strat1 (id1 varchar(2), name1 varchar(15));
insert into cr_strat1 values ('1','index_enc_yr'),('x','x');
drop table if exists cr_strat2;
create temporary table cr_strat2 (id2 varchar(2), name2 varchar(15));
insert into cr_strat2 values ('2','agegroup'),('x','x');
drop table if exists cr_strat4;
create temporary table cr_strat4 (id4 varchar(2), name4 varchar(15));
insert into cr_strat4 values ('4','sex'),('x','x');
drop table if exists cr_strat5;
create temporary table cr_strat5 (id5 varchar(2), name5 varchar(15));
insert into cr_strat5 values ('5','race_ethnicity'),('x','x');
--add risk percentile stratifier
drop table if exists cr_strat6;
create temporary table cr_strat6 (id6 varchar(2), name6 varchar(15));
insert into cr_strat6 values ('6','risk_percentile'),('x','x');
drop table if exists cc_cr_stratifiers_crossed;
create table cc_cr_stratifiers_crossed as
select * from cr_strat1, cr_strat2, cr_strat4, cr_strat5, cr_strat6;

--create stub table to contain summary results
drop table if exists gen_pop_tools.cc_cr_risk_summaries;
create table gen_pop_tools.cc_cr_risk_summaries (
  rpt_yr varchar(4),
  agegroup varchar(20), 
  sex varchar(10), 
  race_ethnicity varchar(25),
  risk_percentile varchar(15),
  total numeric(15,1),
  cv_risk_smk_rcd numeric(15,1), 
  cv_risk_smk numeric(15,1),
  cv_risk_smk_nobn numeric(15,1),  
  cv_risk_a1c numeric(15,1),
  cv_risk_diab numeric(15,1), 
  cv_risk_diab_a1c numeric(15,1), 
  cv_risk_a1c_nt numeric(15,1), 
  cv_risk_ldl_rcd numeric(15,1), 
  cv_risk_ldl numeric(15,1),
  cv_risk_ldl_ns numeric(15,1), 
  cv_risk_bp numeric(15,1), 
  cv_risk_hyp numeric(15,1),
  cv_risk_uchyp numeric(15,1),
  cv_risk_uchypu numeric(15,1)
);
  
   --Run the various groupings.  This would be much easier in Postgres 9.5 where the "grouping sets" query feature was added.
do
$$
  declare
    cursrow record;
    namerow record;
    nameset text;
    groupby text[];
    insrtsql text;
    partsql text;
    tmpsql text;
    ordsql text;
    i integer;
  begin
    for cursrow in execute 'select name1, name2, name4, name5, name6 from cc_cr_stratifiers_crossed'
    loop
      i:=0;
      groupby:=null;
      insrtsql:='insert into gen_pop_tools.cc_cr_risk_summaries (rpt_yr, agegroup, sex, race_ethnicity, risk_percentile, total, cv_risk_smk_rcd,
        cv_risk_smk, cv_risk_smk_nobn, cv_risk_a1c, cv_risk_diab, cv_risk_diab_a1c, cv_risk_a1c_nt, cv_risk_ldl_rcd, cv_risk_ldl, cv_risk_ldl_ns, 
		cv_risk_bp, cv_risk_hyp, cv_risk_uchyp, cv_risk_uchypu) 
        (select';
      if cursrow.name1='x' then 
        tmpsql:=' ''x''::varchar rpt_yr,';
      else
        tmpsql:=' rpt_yr,';
        i:=i+1;
        groupby[i]:='rpt_yr';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name2='x' then 
        tmpsql:=' ''x''::varchar agegroup,';
      else
        tmpsql:=' agegroup,';
        i:=i+1;
        groupby[i]:='agegroup';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name4='x' then 
        tmpsql:=' ''x''::varchar sex,';
      else
        tmpsql:=' sex,';
        i:=i+1;
        groupby[i]:='sex';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name5='x' then 
        tmpsql:=' ''x''::varchar race_ethnicity,';
      else
        tmpsql:=' race_ethnicity,';
        i:=i+1;
        groupby[i]:='race_ethnicity';
      end if;
      insrtsql:=insrtsql||tmpsql;
	  if cursrow.name6='x' then 
        tmpsql:=' ''x''::varchar risk_percentile,';
      else
        tmpsql:=' risk_percentile,';
        i:=i+1;
        groupby[i]:='risk_percentile';
      end if;
      insrtsql:=insrtsql||tmpsql;
      insrtsql:=insrtsql||'
         count(distinct patient_id) total,
		 count(distinct cv_risk_smk_rcd) cv_risk_smk_rcd, 
         count(distinct cv_risk_smk) cv_risk_smk,
         count(distinct cv_risk_smk_nobn) cv_risk_smk_nobn,
         count(distinct cv_risk_a1c) cv_risk_a1c,
		 count(distinct cv_risk_diab) cv_risk_diab,
		 count(distinct cv_risk_diab_a1c) cv_risk_diab_a1c,
		 count(distinct cv_risk_a1c_nt) cv_risk_a1c_nt,
		 count(distinct cv_risk_ldl_rcd) cv_risk_ldl_rcd,
         count(distinct cv_risk_ldl) cv_risk_ldl,
         count(distinct cv_risk_ldl_ns) cv_risk_ldl_ns,
		 count(distinct cv_risk_bp) cv_risk_bp,
         count(distinct cv_risk_hyp) cv_risk_hyp,
		 count(distinct cv_risk_uchyp) cv_risk_uchyp,
         count(distinct cv_risk_uchypu) cv_risk_uchypu
         from cc_cr_plus_risk
         ';
      if i> 0 then 
        insrtsql:=insrtsql||'group by ';
        i:=1;
        while i <= array_length(groupby, 1)
        loop
          if i >1 then 
            insrtsql:=insrtsql||', '; 
          end if;
          insrtsql:=insrtsql||groupby[i];
          i:=i+1;
        end loop;
      end if;
      insrtsql:=insrtsql||')';
      execute insrtsql;
      -- raise notice '%', insrtsql;
    end loop;
  end;      
$$ 
language plpgsql;

--Gather up the values of the stratifiers and assign code. 
drop table if exists gen_pop_tools.cc_cr_agegroup_codevals;
Create table gen_pop_tools.cc_cr_agegroup_codevals (agegroup varchar(11), codeval varchar(1));
insert into gen_pop_tools.cc_cr_agegroup_codevals (agegroup, codeval) 
values ('20-39','1'),('40-59','2'),('60 and over','3');

drop table if exists gen_pop_tools.cc_cr_sex_codevals;
Create table gen_pop_tools.cc_cr_sex_codevals (sex varchar(7), codeval varchar(1));
insert into gen_pop_tools.cc_cr_sex_codevals (sex, codeval) 
values ('Female','1'),('Male','2');

drop table if exists gen_pop_tools.cc_cr_race_ethnicity_codevals;
Create table gen_pop_tools.cc_cr_race_ethnicity_codevals (race_ethnicity varchar(10), codeval varchar(1));
insert into gen_pop_tools.cc_cr_race_ethnicity_codevals (race_ethnicity, codeval) 
values ('ASIAN','1'),('BLACK','2'),('CAUCASIAN','3'),('HISPANIC','4'),('OTHER','5'),('UNKNOWN','6');

drop table if exists gen_pop_tools.cc_cr_risk_percentile_codevals;
Create table gen_pop_tools.cc_cr_risk_percentile_codevals (risk_percentile varchar(15), codeval varchar(1));
insert into gen_pop_tools.cc_cr_risk_percentile_codevals (risk_percentile, codeval) 
values ('cv_risk_100','1'),('cv_risk_20up','2'),('cv_risk_15_20','3'),('cv_risk_10_15','4'),('cv_risk_5_10','5'),('cv_risk_0_5','6');


--now write out to JSON 
copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
'["year","age_group","sex","race_ethnicity","risk_percentile"]'::json filters,
'{"a":{"name":"All patients with calculable risk or presence of disease","description":"Patients with sufficient data to determine presence of cardiovascular disease, or calculate a score for risk according to the American College of Cardiology’s Atherosclerotic Cardiovascular Disease (ASCVD) Risk Score algorithm.","nest":"0","pcalc":"a"},"b":{"name":"Smoking status recorded","description":"Patients with smoking status recorded in the past 3 years","nest":"1","pcalc":"a"},"c":{"name":"Current smoker","description":"Patients who are current smokers based on their most recent status","nest":"2","pcalc":"b"},"d":{"name":"Current smoker without smoking cessation prescription","description":"Patients who are current smokers and no prescription for smoking cessation medication in the past 3 years.","nest":"3","pcalc":"c"},"e":{"name":"Hemaglobin A1C measured","description":"Patients with hemaglobin A1C measured in the past 3 years","nest":"1","pcalc":"a"},"f":{"name":"Known diabetes","description":"Patients with known diabetes","nest":"2","pcalc":"e"},"g":{"name":"Known diabetes with A1C>=9","description":"Patients with known diabetes and whose most recent hemoglobin A1C test value in the past 3 years is equal or greater than 9.","nest":"3","pcalc":"f"},"h":{"name":"Patients with A1C>=9 and no antidiabetic prescription","description":"Patients whose most recent hemoglobin A1C test value is equal or greater than 9 but who did not have prescription for an antidiabetic in the past 3 years.","nest":"4","pcalc":"g"},"i":{"name":"LDL cholesterol measured","description":"Patients with a LDL cholesterol test within the past 3 years.","nest":"1","pcalc":"a"},"j":{"name":"Patients with LDL>=160","description":"Patients whose most recent LDL cholesterol test within the past three years is equal or above 160.","nest":"2","pcalc":"i"},"k":{"name":"Patients with LDL>=160 and no lipid-lowering prescription","description":"Patients whose most recent LDL cholesterol test is equal or above 160 but who did not have a prescription for lipid-lowering medication in the past 3 years.","nest":"3","pcalc":"j"},"l":{"name":"Blood pressure measured","description":"Patients with blood pressure measured within the past three years.","nest":"1","pcalc":"a"},"m":{"name":"Patients with hypertension","description":"Patients with a diagnosis for hypertension.","nest":"2","pcalc":"l"},"n":{"name":"Patients with uncontrolled hypertension","description":"Patients who have active hypertension and whose most recent SBP is >=140 or most recent DBP is >=90","nest":"3","pcalc":"m"},"o":{"name":"Patients with uncontrolled hypertension and no antihypertensives prescription","description":"Patients with uncontrolled hypertension and who did not have a prescription for hypertension within the past 3 years.","nest":"4","pcalc":"n"}}'::json rowmeta,
(select '{'||array_to_string(array_agg(row_),',')||'}' from  (select ('"'||
          case when rpt_yr = 'x' then 'YEAR' else rpt_yr end ||
          case when agegroup <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end ||
          case when sex <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end ||
		  case when race_ethnicity <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end ||
          case when risk_percentile <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_risk_percentile_codevals cv where t0.risk_percentile=cv.risk_percentile)
          else 'x' end || '":{"a":'|| rtrim(to_char(total,'FM99999999999999.9'), '.')
		  || ',"b":' || rtrim(to_char(cv_risk_smk_rcd,'FM99999999999999.9'), '.')
          || ',"c":' || rtrim(to_char(cv_risk_smk,'FM99999999999999.9'), '.')
          || ',"d":' || rtrim(to_char(cv_risk_smk_nobn,'FM99999999999999.9'), '.')
          || ',"e":' || rtrim(to_char(cv_risk_a1c,'FM99999999999999.9'), '.')
		  || ',"f":' || rtrim(to_char(cv_risk_diab,'FM99999999999999.9'), '.')
	      || ',"g":' || rtrim(to_char(cv_risk_diab_a1c,'FM99999999999999.9'), '.')
          || ',"h":' || rtrim(to_char(cv_risk_a1c_nt,'FM99999999999999.9'), '.')
          || ',"i":' || rtrim(to_char(cv_risk_ldl_rcd,'FM99999999999999.9'), '.')
          || ',"j":' || rtrim(to_char(cv_risk_ldl,'FM99999999999999.9'), '.')
          || ',"k":' || rtrim(to_char(cv_risk_ldl_ns,'FM99999999999999.9'), '.')
		  || ',"l":' || rtrim(to_char(cv_risk_bp,'FM99999999999999.9'), '.')
          || ',"m":' || rtrim(to_char(cv_risk_hyp,'FM99999999999999.9'), '.')
          || ',"n":' || rtrim(to_char(cv_risk_uchyp,'FM99999999999999.9'), '.')
          || ',"o":' || rtrim(to_char(cv_risk_uchypu,'FM99999999999999.9'), '.')
          || '}') row_
from gen_pop_tools.cc_cr_risk_summaries t0 ) t00)::json rowdata) t1) to :'pathToFile';