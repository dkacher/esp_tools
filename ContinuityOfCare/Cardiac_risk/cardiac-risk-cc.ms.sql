-- smoking records, select most recent status for each year 
IF object_id('gen_pop_tools.cc_cr_smk', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_smk;
GO
SELECT patient_id, smoking, year, year_month, rpt_yr 
INTO gen_pop_tools.cc_cr_smk
FROM (SELECT distinct patient_id, CAST(smoking as integer) smoking, substring(year_month, 1, 4) year, year_month, 
        SUBSTRING(year_month,1,4) rpt_yr, 
        ROW_NUMBER() OVER (PARTITION BY patient_id, substring(year_month, 1, 4) ORDER BY patient_id, substring(year_month, 1, 4) DESC) rownumber
        FROM gen_pop_tools.tt_smoking) t0
WHERE rownumber = 1
ORDER BY patient_id, year, year_month desc;
GO

-- smoking meds

IF object_id('gen_pop_tools.cc_cr_smk_med', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_smk_med;
GO
select patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) rpt_yr
INTO gen_pop_tools.cc_cr_smk_med
from dbo.emr_prescription
where lower(name) LIKE '%bupropion%'
  or lower(name) LIKE '%zyban%'
  or lower(name) LIKE '%aplenzin%'
  or lower(name) LIKE '%wellbutrin%'
  or lower(name) LIKE '%forfivo%'
  or lower(name) LIKE '%nicotine%'
  or lower(name) LIKE '%chantix%'
  or lower(name) LIKE '%varenicline%'
  or lower(name) LIKE '%nicoderm%'
  or lower(name) LIKE '%habitrol%'
  or lower(name) LIKE '%nicotrol%'
  or lower(name) LIKE '%proStep%'
  or lower(name) LIKE '%nicorette%'
  or lower(name) LIKE '%commit%'
group by patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4);
GO


-- most recent a1c record (last in year)
IF object_id('gen_pop_tools.cc_cr_ha1c', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_ha1c;
GO
SELECT patient_id, rpt_yr, date, result_float
INTO gen_pop_tools.cc_cr_ha1c
FROM (SELECT DISTINCT patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) rpt_yr, date, result_float,
        ROW_NUMBER() OVER(PARTITION BY patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) 
        ORDER BY patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) desc) rownumber
        from gen_pop_tools.tt_sublab l
            where l.test_name='a1c' and result_float>0) AS t0
WHERE rownumber=1
order by patient_id, rpt_yr, date desc;
GO

-- patients with known diabetes. 
IF object_id('gen_pop_tools.cc_cr_diab', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_diab;
GO
select patient_id, rpt_yr, diab
INTO gen_pop_tools.cc_cr_diab
FROM (SELECT DISTINCT patient_id, SUBSTRING(year_month,1,4) rpt_yr, diabetes diab,
        ROW_NUMBER() OVER(PARTITION BY patient_id, substring(year_month,1,4) ORDER BY patient_id, substring(year_month,1,4) DESC) as rownumber
    from (select patient_id, year_month, type_1_diabetes diabetes from gen_pop_tools.tt_type1 where type_1_diabetes = 1
	      union 
		  select patient_id, year_month, type2 diabetes from gen_pop_tools.tt_type2 where type2 = 1) as t00) AS t0
WHERE rownumber = 1
order by patient_id, rpt_yr desc;
GO

IF object_id('gen_pop_tools.cc_cr_antidiab', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_antidiab;
GO
select patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) rpt_yr
INTO gen_pop_tools.cc_cr_antidiab
from dbo.emr_prescription
where 
    lower(name) LIKE '%metformin%'
    or lower(name) LIKE '%glyburide%'
    or lower(name) LIKE '%pramlintide%'
    or lower(name) LIKE '%exenatide%'
    or lower(name) LIKE '%sitagliptin%'
    or lower(name) LIKE '%meglitinide%'
    or lower(name) LIKE '%nateglinide%'
    or lower(name) LIKE '%repaglinide%'
    or lower(name) LIKE '%glimepiride%'
    or lower(name) LIKE '%glipizide%'
    or lower(name) LIKE '%gliclazide%'
    or lower(name) LIKE '%rosiglitizone%'
    or lower(name) LIKE '%pioglitazone%'
    or lower(name) LIKE '%glucagon%'
    or lower(name) LIKE '%miglitol%'
    or lower(name) LIKE '%insulin%'
group by patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4);
GO

-- ldl
/*
IF object_id('gen_pop_tools.', 'U') IS NOT NULL DROP TABLE gen_pop_tools.;
GO
drop table if exists gen_pop_tools.cc_cr_ldl;
create table gen_pop_tools.cc_cr_ldl as
select distinct patient_id, to_char(date,'yyyy') rpt_yr, result_float
from emr_labresult l
join conf_labtestmap m on m.native_code=l.native_code
where m.test_name='cholesterol-ldl' and result_float>0;
*/

-- most recent ldl record (last in year)
IF object_id('gen_pop_tools.cc_cr_ldl', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_ldl;
GO
SELECT patient_id, rpt_yr, date, result_float
INTO gen_pop_tools.cc_cr_ldl
FROM (SELECT DISTINCT patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) rpt_yr, date, result_float,
        ROW_NUMBER() OVER(PARTITION BY patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) 
        ORDER BY patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) desc) rownumber
        from gen_pop_tools.tt_sublab l
            where l.test_name='cholesterol-ldl' and result_float>0 ) AS t0
WHERE rownumber=1
order by patient_id, rpt_yr, date desc;
GO

-- adding new lipid lowering medications (need to check if miss anything)
IF object_id('gen_pop_tools.cc_cr_ldl_med', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_ldl_med;
GO
select patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) rpt_yr
INTO gen_pop_tools.cc_cr_ldl_med
from dbo.emr_prescription
where lower(name) LIKE '%ezetimibe%'
  or lower(name) LIKE '%zetia%'
  or lower(name) LIKE '%fenofibrate%'
  or lower(name) LIKE '%trilipix%'
  or lower(name) LIKE '%triglide%'
  or lower(name) LIKE '%antara%'
  or lower(name) LIKE '%lipofen%'
  or lower(name) LIKE '%fibricor%'
  or lower(name) LIKE '%fenoglide%'
  or lower(name) LIKE '%fenofibric acid%'
  or lower(name) LIKE '%gemfibrozil%'
  or lower(name) LIKE '%lopid%'
  or lower(name) LIKE '%atorvastatin%'
  or lower(name) LIKE '%lipitor%'
  or lower(name) LIKE '%fluvastatin%'
  or lower(name) LIKE '%lescol%'
  or lower(name) LIKE '%lovastatin%'
  or lower(name) LIKE '%altoprev%'
  or lower(name) LIKE '%pitavastatin%'
  or lower(name) LIKE '%livalo%'
  or lower(name) LIKE '%pravastatin%'
  or lower(name) LIKE '%pravachol%'
  or lower(name) LIKE '%rosuvastatin%'
  or lower(name) LIKE '%crestor%'
  or lower(name) LIKE '%simvastatin%'
  or lower(name) LIKE '%zocor%'
  or lower(name) LIKE '%flolipid%'
  -- new meds
  or lower(name) LIKE '%tricor%'
  or lower(name) LIKE '%lipidil%'
  or lower(name) LIKE '%lofibra%'
  or lower(name) LIKE '%niacin%'
  or lower(name) LIKE '%nicotinic acid%'
  or lower(name) LIKE '%niaspan%'
  or lower(name) LIKE '%niacor%'
  or lower(name) LIKE '%cholestyramine%'
  or lower(name) LIKE '%questran%'
  or lower(name) LIKE '%prevalite%'
  or lower(name) LIKE '%colesevelam%'
  or lower(name) LIKE '%welchol%'
  or lower(name) LIKE '%colestipol%'
  or lower(name) LIKE '%colestid%'
  or lower(name) LIKE '%icosapent ethyl%'
  or lower(name) LIKE '%vescepa%'
  or lower(name) LIKE '%omega-3 acid ethyl esters%'
  or lower(name) LIKE '%lovaza%'
  or lower(name) LIKE '%omtryg%'
  or lower(name) LIKE '%omega-3 carboxylic acids%'
  or lower(name) LIKE '%epanova%'
  or lower(name) LIKE '%praluent%'
  or lower(name) LIKE '%alirocumab%'
  or lower(name) LIKE '%repatha%'
  or lower(name) LIKE '%evolocumab%'
group by patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4);
GO

-- bp measured; select most recent sbp and dbp values for each year from tt_enc_bp table
IF object_id('gen_pop_tools.cc_cr_bp', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_bp;
GO
SELECT patient_id, date, sbp, dbp, rpt_yr
INTO gen_pop_tools.cc_cr_bp
FROM (SELECT DISTINCT patient_id, date, bp_systolic as sbp, bp_diastolic as dbp,
        SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) rpt_yr,
        ROW_NUMBER() OVER(PARTITION BY patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) 
        ORDER BY patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) desc) rownumber
        FROM gen_pop_tools.tt_enc_bp) AS t0
WHERE rownumber=1
order by patient_id, rpt_yr, date desc;
GO

-- hypertension (currently using most recent dxhypertension status for each year)
IF object_id('gen_pop_tools.cc_cr_hypert', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_hypert;
GO
select distinct c.patient_id, SUBSTRING(seq.year_month,1,4) rpt_yr,
	   last_value(cah.status) over (partition by c.patient_id, SUBSTRING(seq.year_month,1,4) 
  order by strtdt range between current row and unbounded following) as hypertension
INTO gen_pop_tools.cc_cr_hypert
from nodis_case c
join gen_pop_tools.tt_pat_seq_enc seq on seq.patient_id=c.patient_id
join (select case_id, status, date as strtdt, 
             case 
                when lead(date) over (partition by case_id order by date) is not null 
                   then lead(date) over (partition by case_id order by date)
                else getdate()
             end enddt 
      from nodis_caseactivehistory) cah on c.id=cah.case_id 
            and cah.strtdt<=CONVERT(DATE, REPLACE(seq.year_month, '_', '')  + '01', 112)
            and cah.enddt>=DATEADD(month, 1, CONVERT(DATE, REPLACE(seq.year_month, '_', '')  + '01', 112))
where c.condition='diagnosedhypertension';
GO

-- antihypertension med
IF object_id('gen_pop_tools.cc_cr_antihypert', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_antihypert;
GO
select patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4) rpt_yr 
INTO gen_pop_tools.cc_cr_antihypert
from gen_pop_tools.cr_hist_antihypert_rx
group by patient_id, SUBSTRING(CONVERT(varchar(10), date, 112), 1, 4);
GO


-- bring everything together
IF object_id('gen_pop_tools.cc_cr_plus_risk', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_plus_risk;
GO
select t0.patient_id,
       t0.rpt_yr, 
       case  
	     when t0.cv_risk=1 then 'cv_risk_100'
         when t0.cv_risk>=.2 and t0.cv_risk<1 then 'cv_risk_20up'
         when t0.cv_risk>=.15 and t0.cv_risk<.20 then 'cv_risk_15_20'
         when t0.cv_risk>=.10 and t0.cv_risk<.15 then 'cv_risk_10_15'
         when t0.cv_risk>=.05 and t0.cv_risk<.10 then 'cv_risk_5_10'
         when t0.cv_risk<.05 then 'cv_risk_0_5'
		end as risk_percentile,
	   --smoking status recorded in the past 3 years
	   case when s.patient_id is not null or s2.patient_id is not null or s3.patient_id is not null then t0.patient_id else null end cv_risk_smk_rcd,
       --smoking: most recent smoking status as current smoker in the past 3 years
       case when (s.smoking=4 or (s.smoking is null and s2.smoking=4) or (s.smoking is null and s2.smoking is null and s3.smoking=4)) then t0.patient_id else null end cv_risk_smk,
	   --smoking + no smoking cessation medications in the past 3 years
       case when (s.smoking=4 or (s.smoking is null and s2.smoking=4) or (s.smoking is null and s2.smoking is null and s3.smoking=4))  
	   and (b.patient_id is null and b2.patient_id is null and b3.patient_id is null)
	   then t0.patient_id else null end cv_risk_smk_nobn,
       --A1c measured in the past 3 years
	   case when a1c.patient_id is not null or a1c2.patient_id is not null or a1c3.patient_id is not null then t0.patient_id else null end cv_risk_a1c,
	   --A1c measured and known diabetes 
	   case when diab=1 and (a1c.patient_id is not null or a1c2.patient_id is not null or a1c3.patient_id is not null) then t0.patient_id else null end cv_risk_diab,
	   --diabetes with most recent A1C >=9 
       case when diab=1 and (a1c.result_float>=9 or (a1c.result_float is null and a1c2.result_float >=9) or (a1c.result_float is null and a1c2.result_float is null and a1c3.result_float >=9))  
	   then t0.patient_id else null end cv_risk_diab_a1c,
       --A1c measured and diabetes with most recent A1C >=9 and no anti-diabetic medication in the past 3 years
       case when diab=1 and (a1c.result_float>=9 or (a1c.result_float is null and a1c2.result_float >=9) or (a1c.result_float is null and a1c2.result_float is null and a1c3.result_float >=9))   
	   and ad.patient_id is null and ad2.patient_id is null and ad3.patient_id is null then t0.patient_id else null end cv_risk_a1c_nt,
	   --ldl measured in the past 3 years
       case when ldl.patient_id is not null or ldl2.patient_id is not null or ldl3.patient_id is not null then t0.patient_id else null end cv_risk_ldl_rcd,
	   --ldl 160 most recent in the past 3 years
       case when(ldl.result_float>=160 or (ldl.result_float is null and ldl2.result_float>=160) or 
	   (ldl.result_float is null and ldl2.result_float is null and ldl3.result_float>=160)) then t0.patient_id else null end cv_risk_ldl,
       --ldl 160 and no lipid lowering meds
       case when(ldl.result_float>=160 or (ldl.result_float is null and ldl2.result_float>=160) or 
	   (ldl.result_float is null and ldl2.result_float is null and ldl3.result_float>=160)) and lm.patient_id is null and lm2.patient_id is null and lm3.patient_id is null
	   then t0.patient_id else null end cv_risk_ldl_ns,
       --bp measured in the past 3 years
	   case when bp.patient_id is not null or bp2.patient_id is not null or bp3.patient_id is not null then t0.patient_id else null end cv_risk_bp,
       --bp measured with active hypertension (based on diagnosedhypertension definition)
       case when h.hypertension in ('C', 'U', 'UK') and (bp.patient_id is not null or bp2.patient_id is not null or bp3.patient_id is not null) then t0.patient_id else null end cv_risk_hyp,
       --uncontrolled hypertension: most recent SBP >=140 mmHg or DBP >=90 mmHg after the start date of the active hypertension; 
	   case when h.hypertension ='U' and (bp.patient_id is not null or bp2.patient_id is not null or bp3.patient_id is not null) then t0.patient_id end as cv_risk_uchyp,
	   --uncontrolled hypertension and no meds
       case when h.hypertension='U' and (bp.patient_id is not null or bp2.patient_id is not null or bp3.patient_id is not null) and ah.patient_id is null and ah2.patient_id is null and ah3.patient_id is null then t0.patient_id else null end cv_risk_uchypu,
       case when upper(SUBSTRING(p.gender,1,1)) = 'F' then 'Female'
            when upper(SUBSTRING(p.gender,1,1)) = 'M' then 'Male'
            else 'Unknown'
       end sex,
       case 
         when (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity) is not null
           then (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity)
         when (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race) is not null
           then (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race)
         else 'UNKNOWN'
       end race_ethnicity,
       case 
         when age <=39 then '20-39'
         when age>=40 and age<=59 then '40-59'
         when age>=60 then '60 and over'
       end as agegroup            
INTO gen_pop_tools.cc_cr_plus_risk
from gen_pop_tools.cc_cr_yearly_risk_score t0
JOIN dbo.emr_patient p ON t0.patient_id = p.id
-- smoking 
left join gen_pop_tools.cc_cr_smk s on t0.patient_id=s.patient_id and t0.rpt_yr= s.rpt_yr
left join gen_pop_tools.cc_cr_smk s2 on t0.patient_id=s2.patient_id and CAST(t0.rpt_yr AS int)=CAST(s2.rpt_yr AS int)+1
left join gen_pop_tools.cc_cr_smk s3 on t0.patient_id=s3.patient_id and CAST(t0.rpt_yr AS int)=CAST(s3.rpt_yr AS int)+2
left join gen_pop_tools.cc_cr_smk_med b on t0.patient_id=b.patient_id and t0.rpt_yr= b.rpt_yr
left join gen_pop_tools.cc_cr_smk_med b2 on t0.patient_id=b2.patient_id and CAST(t0.rpt_yr AS int)=CAST(b2.rpt_yr AS int)+1
left join gen_pop_tools.cc_cr_smk_med b3 on t0.patient_id=b3.patient_id and CAST(t0.rpt_yr AS int)=CAST(b3.rpt_yr AS int)+2
left join gen_pop_tools.cc_cr_ha1c a1c on t0.patient_id=a1c.patient_id and t0.rpt_yr=a1c.rpt_yr
left join gen_pop_tools.cc_cr_ha1c a1c2 on t0.patient_id=a1c2.patient_id and CAST(t0.rpt_yr AS int)=CAST(a1c2.rpt_yr AS int)+1
left join gen_pop_tools.cc_cr_ha1c a1c3 on t0.patient_id=a1c3.patient_id and CAST(t0.rpt_yr AS int)=CAST(a1c3.rpt_yr AS int)+2
left join gen_pop_tools.cc_cr_diab d on t0.patient_id = d.patient_id and t0.rpt_yr=d.rpt_yr
left join gen_pop_tools.cc_cr_antidiab ad on t0.patient_id=ad.patient_id and t0.rpt_yr=ad.rpt_yr
left join gen_pop_tools.cc_cr_antidiab ad2 on t0.patient_id=ad2.patient_id and CAST(t0.rpt_yr AS int)=CAST(ad2.rpt_yr AS int)+1
left join gen_pop_tools.cc_cr_antidiab ad3 on t0.patient_id=ad3.patient_id and CAST(t0.rpt_yr AS int)=CAST(ad3.rpt_yr AS int)+2
left join gen_pop_tools.cc_cr_ldl ldl on t0.patient_id=ldl.patient_id and t0.rpt_yr=ldl.rpt_yr
left join gen_pop_tools.cc_cr_ldl ldl2 on t0.patient_id=ldl2.patient_id and CAST(t0.rpt_yr AS int)=CAST(ldl2.rpt_yr AS int)+1
left join gen_pop_tools.cc_cr_ldl ldl3 on t0.patient_id=ldl3.patient_id and CAST(t0.rpt_yr AS int)=CAST(ldl3.rpt_yr AS int)+2
left join gen_pop_tools.cc_cr_ldl_med lm on t0.patient_id=lm.patient_id and t0.rpt_yr=lm.rpt_yr
left join gen_pop_tools.cc_cr_ldl_med lm2 on t0.patient_id=lm2.patient_id and CAST(t0.rpt_yr AS int)=CAST(lm2.rpt_yr AS int)+1
left join gen_pop_tools.cc_cr_ldl_med lm3 on t0.patient_id=lm3.patient_id and CAST(t0.rpt_yr AS int)=CAST(lm3.rpt_yr AS int)+2
left join gen_pop_tools.cc_cr_bp bp on t0.patient_id=bp.patient_id and CAST(t0.rpt_yr AS int)=CAST(bp.rpt_yr AS int)
left join gen_pop_tools.cc_cr_bp bp2 on t0.patient_id=bp2.patient_id and CAST(t0.rpt_yr AS int)=CAST(bp2.rpt_yr AS int)+1
left join gen_pop_tools.cc_cr_bp bp3 on t0.patient_id=bp3.patient_id and CAST(t0.rpt_yr AS int)=CAST(bp3.rpt_yr AS int)+2
left join gen_pop_tools.cc_cr_hypert h on t0.patient_id=h.patient_id and t0.rpt_yr=h.rpt_yr
left join gen_pop_tools.cc_cr_antihypert ah on t0.patient_id=ah.patient_id and t0.rpt_yr = ah.rpt_yr
left join gen_pop_tools.cc_cr_antihypert ah2 on t0.patient_id=ah2.patient_id and CAST(t0.rpt_yr AS int)=CAST(ah2.rpt_yr AS int)+1
left join gen_pop_tools.cc_cr_antihypert ah3 on t0.patient_id=ah3.patient_id and CAST(t0.rpt_yr AS int)=CAST(ah3.rpt_yr AS int)+2
where t0.cv_risk is not null;
GO

--Now get the crossing of all stratifiers
IF object_id('gen_pop_tools.gen_pop_tools.cr_strat1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gen_pop_tools.cr_strat1;
GO
create table gen_pop_tools.cr_strat1 (id1 varchar(2), name1 varchar(15));
insert into gen_pop_tools.cr_strat1 values ('1','index_enc_yr'),('x','x');
IF object_id('gen_pop_tools.cr_strat2', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_strat2;
GO
create table gen_pop_tools.cr_strat2 (id2 varchar(2), name2 varchar(15));
GO
insert into gen_pop_tools.cr_strat2 values ('2','agegroup'),('x','x');
GO
IF object_id('gen_pop_tools.cr_strat4', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_strat4;
GO
create table gen_pop_tools.cr_strat4 (id4 varchar(2), name4 varchar(15));
GO
insert into gen_pop_tools.cr_strat4 values ('4','sex'),('x','x');
GO
IF object_id('gen_pop_tools.cr_strat5', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_strat5;
GO
create table gen_pop_tools.cr_strat5 (id5 varchar(2), name5 varchar(15));
GO
insert into gen_pop_tools.cr_strat5 values ('5','race_ethnicity'),('x','x');
GO
--add risk percentile stratifier
IF object_id('gen_pop_tools.cr_strat6', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_strat6;
GO
create table gen_pop_tools.cr_strat6 (id6 varchar(2), name6 varchar(15));
GO
insert into gen_pop_tools.cr_strat6 values ('6','risk_percentile'),('x','x');
GO
IF object_id('gen_pop_tools.cc_cr_stratifiers_crossed', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_stratifiers_crossed;
GO
select * 
INTO gen_pop_tools.cc_cr_stratifiers_crossed
from gen_pop_tools.cr_strat1, gen_pop_tools.cr_strat2, gen_pop_tools.cr_strat4, gen_pop_tools.cr_strat5, gen_pop_tools.cr_strat6;
GO

--create stub table to contain summary results
IF object_id('gen_pop_tools.cc_cr_risk_summaries', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_risk_summaries;
GO
create table gen_pop_tools.cc_cr_risk_summaries (
  rpt_yr varchar(4),
  agegroup varchar(20), 
  sex varchar(10), 
  race_ethnicity varchar(25),
  risk_percentile varchar(15),
  total numeric(15,1),
  cv_risk_smk_rcd numeric(15,1), 
  cv_risk_smk numeric(15,1),
  cv_risk_smk_nobn numeric(15,1),  
  cv_risk_a1c numeric(15,1),
  cv_risk_diab numeric(15,1), 
  cv_risk_diab_a1c numeric(15,1), 
  cv_risk_a1c_nt numeric(15,1), 
  cv_risk_ldl_rcd numeric(15,1), 
  cv_risk_ldl numeric(15,1),
  cv_risk_ldl_ns numeric(15,1), 
  cv_risk_bp numeric(15,1), 
  cv_risk_hyp numeric(15,1),
  cv_risk_uchyp numeric(15,1),
  cv_risk_uchypu numeric(15,1)
);
GO
  
   --Run the various groupings.  This would be much easier in Postgres 9.5 where the "grouping sets" query feature was added.
BEGIN
  DECLARE @nameset varchar(max);
  DECLARE @insrtsql nvarchar(max);
  DECLARE @partsql nvarchar(max);
  DECLARE @tmpsql nvarchar(max);
  DECLARE @ordsql nvarchar(max);
  DECLARE @i integer;
  DECLARE @name1 varchar(max);
  DECLARE @name2 varchar(max);
  DECLARE @name4 varchar(max);
  DECLARE @name5 varchar(max);
  DECLARE @name6 varchar(max);
  DECLARE @groupclauses TABLE(id int, val nvarchar(max));
  DECLARE @groupby nvarchar(max);
  DECLARE cur CURSOR FOR select name1, name2, name4, name5, name6 from gen_pop_tools.cc_cr_stratifiers_crossed;
  OPEN cur  
  FETCH NEXT FROM cur INTO @name1, @name2, @name4, @name5, @name6 
  WHILE @@FETCH_STATUS = 0 BEGIN
      SET @i=0;
      SET @insrtsql='insert into gen_pop_tools.cc_cr_risk_summaries (rpt_yr, agegroup, sex, race_ethnicity, risk_percentile, total, cv_risk_smk_rcd,
        cv_risk_smk, cv_risk_smk_nobn, cv_risk_a1c, cv_risk_diab, cv_risk_diab_a1c, cv_risk_a1c_nt, cv_risk_ldl_rcd, cv_risk_ldl, cv_risk_ldl_ns, 
		cv_risk_bp, cv_risk_hyp, cv_risk_uchyp, cv_risk_uchypu) 
        (select';
      if cursrow.name1='x'
        SET @tmpsql=' CAST(''x'' AS varchar(max)) rpt_yr,';
      else BEGIN
        SET @tmpsql=' rpt_yr,';
        SET @i=i+1;
        insert into @groupclauses(id, val) VALUES(i, 'rpt_yr');
      END;
      SET @insrtsql=insrtsql+tmpsql;
      if cursrow.name2='x'
        SET @tmpsql=' ''x'' AS varchar(max)) agegroup,';
      else BEGIN
        SET @tmpsql=' agegroup,';
        SET @i=i+1;
        insert into @groupclauses(id, val) VALUES(i, 'agegroup');
        SET @groupclauses[i]='agegroup';
      END;
      SET @insrtsql=insrtsql+tmpsql;
      if cursrow.name4='x'
        SET @tmpsql=' ''x'' AS varchar(max)) sex,';
      else BEGIN
        SET @tmpsql=' sex,';
        SET @i=i+1;
        insert into @groupclauses(id, val) VALUES(i, 'sex');
        SET @groupclauses[i]='sex';
      END;
      SET @insrtsql=insrtsql+tmpsql;
      if cursrow.name5='x'
        SET @tmpsql=' ''x'' AS varchar(max)) race_ethnicity,';
      else BEGIN
        SET @tmpsql=' race_ethnicity,';
        SET @i=i+1;
        insert into @groupclauses(id, val) VALUES(i, 'race_ethnicity');
        SET @groupclauses[i]='race_ethnicity';
      END;
      SET @insrtsql=insrtsql+tmpsql;
	  if cursrow.name6='x'
        SET @tmpsql=' ''x'' AS varchar(max)) risk_percentile,';
      else BEGIN
        SET @tmpsql=' risk_percentile,';
        SET @i=i+1;
        insert into @groupclauses(id, val) VALUES(i, 'risk_percentile');
        SET @groupclauses[i]='';
      END;
      SET @insrtsql=insrtsql+tmpsql;
      SET @insrtsql=insrtsql+'
         count(distinct patient_id) total,
		 count(distinct cv_risk_smk_rcd) cv_risk_smk_rcd, 
         count(distinct cv_risk_smk) cv_risk_smk,
         count(distinct cv_risk_smk_nobn) cv_risk_smk_nobn,
         count(distinct cv_risk_a1c) cv_risk_a1c,
		 count(distinct cv_risk_diab) cv_risk_diab,
		 count(distinct cv_risk_diab_a1c) cv_risk_diab_a1c,
		 count(distinct cv_risk_a1c_nt) cv_risk_a1c_nt,
		 count(distinct cv_risk_ldl_rcd) cv_risk_ldl_rcd,
         count(distinct cv_risk_ldl) cv_risk_ldl,
         count(distinct cv_risk_ldl_ns) cv_risk_ldl_ns,
		 count(distinct cv_risk_bp) cv_risk_bp,
         count(distinct cv_risk_hyp) cv_risk_hyp,
		 count(distinct cv_risk_uchyp) cv_risk_uchyp,
         count(distinct cv_risk_uchypu) cv_risk_uchypu
         from gen_pop_tools.cc_cr_plus_risk
         ';
      if i> 0 BEGIN
        SET @insrtsql=@insrtsql+'group by ';
        SET @i=1;
        SET @length = (SELECT Count(*) FROM @groupclauses);
        while @i <= @length BEGIN
          if @i >1 BEGIN
            SET @insrtsql=@insrtsql+', '; 
          END;
          SET @groupby = (SELECT val FROM @groupclauses WHERE id=@i);
          SET @insrtsql=@insrtsql+@groupby;
          SET @i=@i+1;
        END;
      END;
      SET @insrtsql=insrtsql+')';
      EXECUTE sp_executesql insrtsql;
    FETCH NEXT FROM cur INTO @name1, @name2, @name4, @name5, @name6 
  END;
  CLOSE cur;
  DEALLOCATE cur;
END;
GO

--Gather up the values of the stratifiers and assign code. 
IF object_id('gen_pop_tools.cc_cr_agegroup_codevals', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_agegroup_codevals;
GO
Create table gen_pop_tools.cc_cr_agegroup_codevals (agegroup varchar(11), codeval varchar(1));
GO
insert into gen_pop_tools.cc_cr_agegroup_codevals (agegroup, codeval) 
values ('20-39','1'),('40-59','2'),('60 and over','3');
GO

IF object_id('gen_pop_tools.cc_cr_sex_codevals', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_sex_codevals;
GO
Create table gen_pop_tools.cc_cr_sex_codevals (sex varchar(7), codeval varchar(1));
GO
insert into gen_pop_tools.cc_cr_sex_codevals (sex, codeval) 
values ('Female','1'),('Male','2');
GO

IF object_id('gen_pop_tools.cc_cr_race_ethnicity_codevals', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_race_ethnicity_codevals;
GO
Create table gen_pop_tools.cc_cr_race_ethnicity_codevals (race_ethnicity varchar(10), codeval varchar(1));
GO
insert into gen_pop_tools.cc_cr_race_ethnicity_codevals (race_ethnicity, codeval) 
values ('ASIAN','1'),('BLACK','2'),('CAUCASIAN','3'),('HISPANIC','4'),('OTHER','5'),('UNKNOWN','6');
GO

IF object_id('gen_pop_tools.cc_cr_risk_percentile_codevals', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_risk_percentile_codevals;
GO
Create table gen_pop_tools.cc_cr_risk_percentile_codevals (risk_percentile varchar(15), codeval varchar(1));
GO
insert into gen_pop_tools.cc_cr_risk_percentile_codevals (risk_percentile, codeval) 
values ('cv_risk_100','1'),('cv_risk_20up','2'),('cv_risk_15_20','3'),('cv_risk_10_15','4'),('cv_risk_5_10','5'),('cv_risk_0_5','6');
GO

--now write out to JSON 
/*
MS SQL code will look like
this requires enabling xp_cmdshell (creates security issue) and running the BCP command in the shell
exec xp_cmdshell 'bcp "query goes here" queryout "c:\temp\outfile.txt" -C -N -S <server> -U <user> -d <database> -P <password>';
GO

copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
CAST('["year","age_group","sex","race_ethnicity","risk_percentile"]' AS json) filters,
'{"a":{"name":"All patients with calculable risk or presence of disease","description":"Patients with sufficient data to determine presence of cardiovascular disease, or calculate a score for risk according to the American College of Cardiology’s Atherosclerotic Cardiovascular Disease (ASCVD) Risk Score algorithm.","nest":"0","pcalc":"a"},"b":{"name":"Smoking status recorded","description":"Patients with smoking status recorded in the past 3 years","nest":"1","pcalc":"a"},"c":{"name":"Current smoker","description":"Patients who are current smokers based on their most recent status","nest":"2","pcalc":"b"},"d":{"name":"Current smoker without smoking cessation prescription","description":"Patients who are current smokers and no prescription for smoking cessation medication in the past 3 years.","nest":"3","pcalc":"c"},"e":{"name":"Hemaglobin A1C measured","description":"Patients with hemaglobin A1C measured in the past 3 years","nest":"1","pcalc":"a"},"f":{"name":"Known diabetes","description":"Patients with known diabetes","nest":"2","pcalc":"e"},"g":{"name":"Known diabetes with A1C>=9","description":"Patients with known diabetes and whose most recent hemoglobin A1C test value in the past 3 years is equal or greater than 9.","nest":"3","pcalc":"f"},"h":{"name":"Patients with A1C>=9 and no antidiabetic prescription","description":"Patients whose most recent hemoglobin A1C test value is equal or greater than 9 but who did not have prescription for an antidiabetic in the past 3 years.","nest":"4","pcalc":"g"},"i":{"name":"LDL cholesterol measured","description":"Patients with a LDL cholesterol test within the past 3 years.","nest":"1","pcalc":"a"},"j":{"name":"Patients with LDL>=160","description":"Patients whose most recent LDL cholesterol test within the past three years is equal or above 160.","nest":"2","pcalc":"i"},"k":{"name":"Patients with LDL>=160 and no lipid-lowering prescription","description":"Patients whose most recent LDL cholesterol test is equal or above 160 but who did not have a prescription for lipid-lowering medication in the past 3 years.","nest":"3","pcalc":"j"},"l":{"name":"Blood pressure measured","description":"Patients with blood pressure measured within the past three years.","nest":"1","pcalc":"a"},"m":{"name":"Patients with hypertension","description":"Patients with a diagnosis for hypertension.","nest":"2","pcalc":"l"},"n":{"name":"Patients with uncontrolled hypertension","description":"Patients who have active hypertension and whose most recent SBP is >=140 or most recent DBP is >=90","nest":"3","pcalc":"m"},"o":{"name":"Patients with uncontrolled hypertension and no antihypertensives prescription","description":"Patients with uncontrolled hypertension and who did not have a prescription for hypertension within the past 3 years.","nest":"4","pcalc":"n"}}'::json rowmeta,
CAST(select '{'+array_to_string(array_agg(row_),',')+'}' from  (select ('"'+
          case when rpt_yr = 'x' then 'YEAR' else rpt_yr end +
          case when agegroup <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end +
          case when sex <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end +
		  case when race_ethnicity <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end +
          case when risk_percentile <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_risk_percentile_codevals cv where t0.risk_percentile=cv.risk_percentile)
          else 'x' end + '":{"a":'+ rtrim(to_char(total,'FM99999999999999.9'), '.')
		  + ',"b":' + rtrim(to_char(cv_risk_smk_rcd,'FM99999999999999.9'), '.')
          + ',"c":' + rtrim(to_char(cv_risk_smk,'FM99999999999999.9'), '.')
          + ',"d":' + rtrim(to_char(cv_risk_smk_nobn,'FM99999999999999.9'), '.')
          + ',"e":' + rtrim(to_char(cv_risk_a1c,'FM99999999999999.9'), '.')
		  + ',"f":' + rtrim(to_char(cv_risk_diab,'FM99999999999999.9'), '.')
	      + ',"g":' + rtrim(to_char(cv_risk_diab_a1c,'FM99999999999999.9'), '.')
          + ',"h":' + rtrim(to_char(cv_risk_a1c_nt,'FM99999999999999.9'), '.')
          + ',"i":' + rtrim(to_char(cv_risk_ldl_rcd,'FM99999999999999.9'), '.')
          + ',"j":' + rtrim(to_char(cv_risk_ldl,'FM99999999999999.9'), '.')
          + ',"k":' + rtrim(to_char(cv_risk_ldl_ns,'FM99999999999999.9'), '.')
		  + ',"l":' + rtrim(to_char(cv_risk_bp,'FM99999999999999.9'), '.')
          + ',"m":' + rtrim(to_char(cv_risk_hyp,'FM99999999999999.9'), '.')
          + ',"n":' + rtrim(to_char(cv_risk_uchyp,'FM99999999999999.9'), '.')
          + ',"o":' + rtrim(to_char(cv_risk_uchypu,'FM99999999999999.9'), '.')
          + '}') row_
from gen_pop_tools.cc_cr_risk_summaries t0 ) t00) AS json) rowdata) t1) to :'pathToFile';
GO
*/

-- get rid of temporary tables
DROP TABLE gen_pop_tools.cr_strat1;
DROP TABLE gen_pop_tools.cr_strat2;
DROP TABLE gen_pop_tools.cr_strat4;
DROP TABLE gen_pop_tools.cr_strat5;
DROP TABLE gen_pop_tools.cr_strat6;
