﻿--set client_min_messages to error;

set search_path to gen_pop_tools, public;

DROP TABLE IF EXISTS cr_hist_coefficients;
CREATE TABLE cr_hist_coefficients AS
SELECT 
-29.799::real f_ln_age,
17.114::real f_b_ln_age,
12.344::real m_ln_age,
2.469::real m_b_ln_age,
4.884::real f_ln_age2,
13.54::real f_ln_chol,
0.94::real f_b_ln_chol,
11.853::real m_ln_chol,
0.302::real m_b_ln_chol,
-3.114::real f_ln_ageXchol,
-2.664::real m_ln_ageXchol,
-13.578::real f_ln_hdl_C,
-18.92::real f_b_ln_hdl_C,
-7.99::real m_ln_hdl_C,
-0.307::real m_b_ln_hdl_C,
3.149::real f_ln_ageXhdl_C,
4.475::real f_b_ln_ageXhdl_C,
1.769::real m_ln_ageXhdl_C,
2.019::real f_ln_tr_sys_bp,
29.291::real f_b_ln_tr_sys_bp,
1.797::real m_ln_tr_sys_bp,
1.916::real m_b_ln_tr_sys_bp,
-6.432::real f_b_ln_ageXtr_sys_bp,
1.957::real f_ln_u_sys_bp,
27.82::real f_b_ln_u_sys_bp,
1.764::real m_ln_u_sys_bp,
1.809::real m_b_ln_u_sys_bp,
-6.087::real f_b_ln_ageXu_sys_bp,
7.574::real f_cur_smk,
0.691::real f_b_cur_smk,
7.837::real m_cur_smk,
0.549::real m_b_cur_smk,
-1.665::real f_cur_smkXln_age,
-1.795::real m_cur_smkXln_age,
0.661::real f_diab,
0.874::real f_b_diab,
0.658::real m_diab,
0.645::real m_b_diab,
0.9665::real f_bsline,
0.9533::real f_b_bsline,
0.9144::real m_bsline,
0.8954::real m_b_bsline
;

DROP TABLE IF EXISTS cr_hist_icd;
create table cr_hist_icd ( dx_code_id varchar(20) );
insert into cr_hist_icd (dx_code_id)
  values ('icd9:410%'),
         ('icd9:411%'),
         ('icd9:412%'),
         ('icd9:413%'),
         ('icd9:414.0%'),
         ('icd9:414.2%'),
         ('icd9:414.3%'),
         ('icd9:414.8%'),
         ('icd9:414.9%'),
         ('icd9:433%'),
         ('icd9:434%'),
         ('icd9:437.0%'),
         ('icd9:437.1%'),
         ('icd9:437.9%'),
         ('icd9:438%'),
         ('icd9:440%'),
         ('icd9:443,9%'),
         ('icd10:I20%'),
         ('icd10:I21%'),
         ('icd10:I22%'),
         ('icd10:I23%'),
         ('icd10:I24%'),
         ('icd10:I25%'),
         ('icd10:I63%'),
         ('icd10:I65%'),
         ('icd10:I66%'),
         ('icd10:I67.2%'),
         ('icd10:I67.8%'),
         ('icd10:I69.3%'),
         ('icd10:I69.4%'),
         ('icd10:I70%'),
         ('icd10:I73.9%');

DROP TABLE IF EXISTS cr_hist_antihypertensives;
create table cr_hist_antihypertensives ( generic_name varchar(100) );
insert into cr_hist_antihypertensives (generic_name)
  values ('hydrochlorothiazide'),
         ('chlorthalidone'),
         ('indapamide'),
         ('amlodipine'),
         ('clevidipine'),
         ('felodipine'),
         ('isradipine'),
         ('nicardipine'),
         ('nifedipine'),
         ('nisoldipine'),
         ('diltiazem'),
         ('verapamil'),
         ('acebutolol'),
         ('atenolol'),
         ('betaxolol'),
         ('bisoprolol'),
         ('carvedilol'),
         ('labetalol'),
         ('metoprolol'),
         ('nadolol'),
         ('nebivolol'),
         ('pindolol'),
         ('propranolol'),
         ('benazepril'),
         ('catopril'),
         ('enalapril'),
         ('fosinopril'),
         ('lisinopril'),
         ('moexipril'),
         ('perindopril'),
         ('quinapril'),
         ('ramipril'),
         ('trandolapril'),
         ('candesartan'),
         ('eprosartan'),
         ('irbesartan'),
         ('losartan'),
         ('olmesartan'),
         ('telmisartan'),
         ('valsartan'),
         ('clonidine'),
         ('doxazosin'),
         ('guanfacine'),
         ('methyldopa'),
         ('prazosin'),
         ('terazosin'),
         ('eplerenone'),
         ('spironolactone'),
         ('aliskiren'),
         ('hydralazine');       

--here are patients who can be scored
DROP TABLE IF EXISTS cr_hist_index_patients;
CREATE TABLE cr_hist_index_patients AS
SELECT distinct 
       T1.id as patient_id,
       T1.date_of_birth,
       case when upper(substr(t1.gender,1,1)) = 'F' then 'Female'
         when upper(substr(t1.gender,1,1)) = 'M' then 'Male'
       end sex,
       case 
         when (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=t1.race and t00.target_value='BLACK') is not null
           then 'BLACK'
         else 'OTHER'
       end race
FROM emr_patient T1
WHERE substr(t1.gender,1,1) in ('M', 'F')
AND ((T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%') or last_name is null);

--these are the patients with cvdisease diagnosis encounters
drop table if exists cr_dx_encs;
create table cr_dx_encs as 
select enc.patient_id, to_char(min(enc.date),'yyyy_mm') as year_month
from emr_encounter enc
join emr_encounter_dx_codes dx on dx.encounter_id=enc.id
join cr_hist_icd crdx on dx.dx_code_id like crdx.dx_code_id
group by enc.patient_id;

--these are the qualifying encounters for risk assessment
drop table if exists cr_enc_pat;
create table cr_enc_pat as
select enc.patient_id, 
       to_char(enc.date,'yyyy_mm') as year_month
from emr_encounter enc 
JOIN clin_enc T2 on T2.patient_id=enc.patient_id and t2.date=enc.date and t2.source='enc'
where not exists (select null from cr_dx_encs dxe 
                     where dxe.patient_id=enc.patient_id 
                     and to_char(enc.date,'yyyy_mm')>=dxe.year_month)
group by enc.patient_id, to_char(enc.date,'yyyy_mm');
alter table cr_enc_pat add primary key (patient_id, year_month);
analyze cr_enc_pat;

-- Gather up all of the cr lab tests
DROP TABLE IF EXISTS cr_hist_labs;
CREATE TABLE cr_hist_labs AS 
SELECT distinct on (T1.patient_id, T2.test_name, to_char(t1.date,'yyyy_mm')) --"distinct on" is non-standard sql.  Postgres only. First row in sorted group. 
       T1.patient_id, T2.test_name, to_char(t1.date,'yyyy_mm') year_month, 
       t1.result_float as result
FROM emr_labresult T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
WHERE test_name in ('cholesterol-total', 'cholesterol-hdl')
and t1.result_float>0
AND T1.date >= '01-01-2010'::date
order by T1.patient_id, T2.test_name, to_char(t1.date,'yyyy_mm'), t1.date desc, t1.result_float desc ;
drop table if exists cr_hist_tchol;
create table cr_hist_tchol as
select t0.patient_id, t0.year_month, t1.result
from tt_pat_seq t0 
left join (select * from cr_hist_labs where test_name='cholesterol-total') 
     t1 on t0.patient_id=t1.patient_id and t0.year_month=t1.year_month;
CREATE INDEX cr_hist_tchol_idx
  ON gen_pop_tools.cr_hist_tchol
  USING btree
  (patient_id, year_month);
--downfill total chol 23 months
select gen_pop_tools.downfill_dub('gen_pop_tools.cr_hist_tchol'::varchar,
                              'patient_id'::varchar,
                              'year_month'::varchar,
                              'result'::varchar,
                              'year_month'::varchar);
drop table if exists cr_hist_hdlc;
create table cr_hist_hdlc as
select t0.patient_id, t0.year_month, t1.result
from tt_pat_seq t0 
left join (select * from cr_hist_labs where test_name='cholesterol-hdl') 
     t1 on t0.patient_id=t1.patient_id and t0.year_month=t1.year_month;
CREATE INDEX cr_hist_hdlc_idx
  ON gen_pop_tools.cr_hist_hdlc
  USING btree
  (patient_id, year_month);
--downfil hdl chol 23 months
select gen_pop_tools.downfill_dub('gen_pop_tools.cr_hist_hdlc'::varchar,
                              'patient_id'::varchar,
                              'year_month'::varchar,
                              'result'::varchar,
                              'year_month'::varchar);

--Find the anterhypertensives
DROP table if exists cr_hist_antihypert_rx;
CREATE TABLE cr_hist_antihypert_rx AS
SELECT distinct p.patient_id, p.date
FROM emr_prescription p
join static_drugsynonym ds on p.name ilike '%' || ds.other_name || '%'
join cr_hist_antihypertensives ah on lower(ds.generic_name) = ah.generic_name
AND p.date >= '01-01-2010'::date;
alter table cr_hist_antihypert_rx add primary key (patient_id, date);
analyze cr_hist_antihypert_rx;

-- Gather up all the systolic BP data and figure out treated or not
DROP TABLE IF EXISTS cr_hist_bp_raw;
CREATE TABLE cr_hist_bp_raw AS 
SELECT T1.patient_id, t1.date, t1.bp_systolic
FROM emr_encounter T1 
WHERE t1.bp_systolic between 50 and 500 -- reality, please.
AND T1.date >= '01-01-2010'::date;
CREATE INDEX cr_hist_bp_raw_idx
  ON gen_pop_tools.cr_hist_bp_raw
  USING btree
  (patient_id, date);
DROP Table if exists cr_hist_bp_treated;
create table cr_hist_bp_treated as
select t1.*,  
   case when exists (select null from cr_hist_antihypert_rx rx where rx.patient_id=t1.patient_id and rx.date between t1.date - interval '2 years' and t1.date - interval '1 day') then TRUE else FALSE end as treated
from cr_hist_bp_raw t1;

DROP TABLE IF EXISTS cr_hist_bp;
CREATE TABLE cr_hist_bp AS -- last in month
SELECT distinct on (t1.patient_id, to_char(t1.date,'yyyy_mm')) --"distinct on" is non-standard sql.  Postgres only. First row in sorted group. 
    T1.patient_id, to_char(t1.date,'yyyy_mm') year_month, treated, t1.bp_systolic
FROM cr_hist_bp_treated T1 
WHERE t1.bp_systolic between 50 and 500
AND T1.date >= '01-01-2010'::date
order by t1.patient_id, to_char(t1.date,'yyyy_mm'), t1.date desc, t1.bp_systolic desc;
DROP TABLE IF EXISTS cr_hist_bp_seq;
CREATE TABLE cr_hist_bp_seq AS 
SELECT T0.patient_id, t0.year_month, t1.treated, t1.bp_systolic
FROM tt_pat_seq t0
left join cr_hist_bp T1 on t0.patient_id=t1.patient_id and t0.year_month=t1.year_month;
CREATE INDEX cr_hist_bp_seq_idx
  ON gen_pop_tools.cr_hist_bp_seq
  USING btree
  (patient_id, year_month);
select gen_pop_tools.downfill_bool('gen_pop_tools.cr_hist_bp_seq'::varchar,
                              'patient_id'::varchar,
                              'year_month'::varchar,
                              'bp_systolic'::varchar,
                              'treated'::varchar,
                              'year_month'::varchar);

drop table if exists cr_smoke_seq;
create table cr_smoke_seq as
select t0.patient_id, t0.year_month, 
case 
    when t1.smoking::integer>0 then t1.smoking::integer
    else 0 end as smoking 
FROM tt_pat_seq t0
left join tt_smoking t1 on t0.patient_id=t1.patient_id and t0.year_month=t1.year_month;
CREATE INDEX cr_smoke_seq_idx
  ON gen_pop_tools.cr_smoke_seq
  USING btree
  (patient_id, year_month);
analyze cr_smoke_seq;
select gen_pop_tools.downfill2('gen_pop_tools.cr_smoke_seq'::varchar,
                              'patient_id'::varchar,
                              'year_month'::varchar,
                              'smoking'::varchar,
							  'year_month'::varchar,
							   500::integer);

drop table if exists cr_diab_seq;
create table cr_diab_seq as
select distinct t0.patient_id, t0.year_month, 
       case when t1.type_1_diabetes=1 or t2.type2=1 then 1 else 0 end as diabetes
FROM tt_pat_seq t0
left join tt_type1 t1 on t0.patient_id=t1.patient_id and t0.year_month >= t1.year_month
left join tt_type2 t2 on t0.patient_id=t2.patient_id and t0.year_month=t2.year_month;
CREATE INDEX cr_diab_seq_idx
  ON gen_pop_tools.cr_diab_seq
  USING btree
  (patient_id, year_month);
analyze cr_diab_seq;

alter table cr_hist_index_patients add primary key (patient_id);
analyze cr_hist_index_patients;
alter table cr_dx_encs add primary key (patient_id, year_month);
analyze cr_dx_encs;
alter table cr_hist_tchol add primary key (patient_id, year_month);
analyze cr_hist_tchol;
alter table cr_hist_hdlc add primary key (patient_id, year_month);
analyze cr_hist_hdlc;
alter table cr_hist_bp_seq add primary key (patient_id, year_month);
analyze cr_hist_bp_seq;

-- Bring together all of the fields and values
drop table if exists cr_hist_index_data;
CREATE TABLE cr_hist_index_data AS
SELECT t0.year_month,
  T1.patient_id,
  t1.sex,
  t1.race,
  case t10.race
    when 'CAUCASIAN' then 1
    when 'ASIAN' then 2
    when 'BLACK' then 3
    when 'HISPANIC' then 4
    when 'OTHER' then 5
    else 6
  end as fullrace,
  date_part('year', age(to_date(t0.year_month,'yyyy_mm'),t1.date_of_birth::date)) as age,
  case when t2.patient_id is not null then 1 else 0 end as cvd,
  t4.result as t_chol,
  t5.result as hdl_chol,
  t6.bp_systolic, t6.treated,
  t7.smoking,
  t8.diabetes,
  case when t1.race='BLACK' and t1.sex='Female' then 'fb'
       when t1.race='OTHER' and t1.sex='Female' then 'fo'
       when t1.race='BLACK' and t1.sex='Male' then 'mb'
       when t1.race='OTHER' and t1.sex='Male' then 'mo'
  end as rs_type       
FROM tt_pat_seq t0
left Join cr_hist_index_patients T1 on t1.patient_id=t0.patient_id
left join cr_dx_encs t2 on t0.patient_id=t2.patient_id and t0.year_month>=t2.year_month
left join cr_hist_tchol t4 on t0.patient_id=t4.patient_id and t0.year_month=t4.year_month
left join cr_hist_hdlc t5 on t0.patient_id=t5.patient_id and t0.year_month=t5.year_month
left join cr_hist_bp_seq t6 on t0.patient_id=t6.patient_id and t0.year_month=t6.year_month
left join cr_smoke_seq t7 on t0.patient_id=t7.patient_id and t0.year_month=t7.year_month
left join cr_diab_seq t8 on t0.patient_id=t8.patient_id and t0.year_month=t8.year_month
left join tt_pat t10 on t0.patient_id=t10.patient_id
where exists (select null from cr_enc_pat t3 
              where t0.patient_id=t3.patient_id 
                    and to_date(t3.year_month,'yyyy_mm') 
                    between to_date(t0.year_month,'yyyy_mm') - interval '2 years' 
                    and to_date(t0.year_month,'yyyy_mm') - interval '1 month') ;

-- Compute the values to be used in the risk score calculation
drop table if exists cr_hist_pat_sum_x_value;
CREATE TABLE cr_hist_pat_sum_x_value AS
SELECT patient_id, year_month, fullrace as race, sex, age, rs_type, smoking,
(ln(age) * case when i.rs_type='fb' then c.f_b_ln_age 
                when i.rs_type='fo' then c.f_ln_age 
                when i.rs_type='mb' then c.m_b_ln_age 
                when i.rs_type='mo' then c.m_ln_age end) ln_age,
(ln(age)^2 * case when i.rs_type='fo' then c.f_ln_age2 else null end) ln_age2,
(ln(t_chol) * case when i.rs_type='fb' then c.f_b_ln_chol 
                   when i.rs_type='fo' then c.f_ln_chol 
                   when i.rs_type='mb' then c.m_b_ln_chol 
                   when i.rs_type='mo' then c.m_ln_chol end) ln_t_chol,
(ln(t_chol) * ln(age) * case when i.rs_type='fo' then c.f_ln_ageXchol when i.rs_type='mo' then c.m_ln_ageXchol else null end) ln_ageXt_chol,
(ln(hdl_chol) * case when i.rs_type='fb' then c.f_b_ln_hdl_c 
                     when i.rs_type='fo' then c.f_ln_hdl_c 
                     when i.rs_type='mb' then c.m_b_ln_hdl_c 
                     when i.rs_type='mo' then c.m_ln_hdl_c end) ln_hdl_c,
(ln(hdl_chol) * ln(age) * case when i.rs_type='fb' then c.f_b_ln_ageXhdl_c 
                               when i.rs_type='fo' then c.f_ln_ageXhdl_c 
                               when i.rs_type='mo' then c.m_ln_ageXhdl_c else null end) ln_ageXhdl_c,
(case 
  when treated then
    (ln(bp_systolic) * case when i.rs_type='fb' then c.f_b_ln_tr_sys_bp 
                            when i.rs_type='fo' then c.f_ln_tr_sys_bp 
                            when i.rs_type='mb' then c.m_b_ln_tr_sys_bp 
                            when i.rs_type='mo' then c.m_ln_tr_sys_bp end)
  when not treated then 
    (ln(bp_systolic) * case when i.rs_type='fb' then c.f_b_ln_u_sys_bp 
                            when i.rs_type='fo' then c.f_ln_u_sys_bp 
                            when i.rs_type='mb' then c.m_b_ln_u_sys_bp 
                            when i.rs_type='mo' then c.m_ln_u_sys_bp end)
 end) ln_bp,
(case 
  when treated then
    (ln(bp_systolic) * ln(age) * case when i.rs_type='fb' then c.f_b_ln_agextr_sys_bp end)
  when not treated then 
    (ln(bp_systolic) * ln(age) * case when i.rs_type='fb' then c.f_b_ln_ageXu_sys_bp end) 
 end) ln_ageXbp,
(case when smoking=4 then (case when i.rs_type='fb' then c.f_b_cur_smk 
                                when i.rs_type='fo' then c.f_cur_smk 
                                when i.rs_type='mb' then c.m_b_cur_smk 
                                when i.rs_type='mo' then c.m_cur_smk end)
      when smoking<4 then 0 end) cur_smk,
(case when smoking=4 then (ln(age) * case when i.rs_type='fo' then c.f_cur_smkXln_age when i.rs_type='mo' then c.m_cur_smkXln_age end) 
      when smoking<4 then 0 end) cur_smkXln_age,
(case when diabetes=1 then (case when i.rs_type='fb' then c.f_b_diab 
                                 when i.rs_type='fo' then c.f_diab 
                                 when i.rs_type='mb' then c.m_b_diab 
                                 when i.rs_type='mo' then c.m_diab end)
      when diabetes=0 then 0 end) diab,
(case when i.rs_type='fb' then c.f_b_bsline 
      when i.rs_type='fo' then c.f_bsline 
      when i.rs_type='mb' then c.m_b_bsline 
      when i.rs_type='mo' then c.m_bsline end) baseline,
(case when i.rs_type='fb' then 86.61 
      when i.rs_type='fo' then -29.18 
      when i.rs_type='mb' then 19.54 
      when i.rs_type='mo' then 61.18 end) mean,
 cvd
FROM cr_hist_index_data i,
cr_hist_coefficients c
where age between 20 and 79;
--this table gets used by Riskscape
drop table if exists gen_pop_tools.cc_cr_risk_score;
create table gen_pop_tools.cc_cr_risk_score as 
select patient_id, year_month, race, sex, age, cur_smk, smoking,
  case
     when cvd=1 then 1
     else calculate_with_error(baseline, ln_age, ln_age2, ln_t_chol, ln_ageXt_chol, ln_hdl_c, ln_ageXhdl_c, ln_bp, ln_ageXbp, cur_smk, cur_smkXln_age, diab, mean, rs_type,            patient_id)
  end cv_risk 
from cr_hist_pat_sum_x_value;

drop table if exists gen_pop_tools.cc_cr_yearly_risk_score;
create table gen_pop_tools.cc_cr_yearly_risk_score as 
select distinct on (patient_id, substr(year_month,1,4))
    patient_id, substr(year_month,1,4) rpt_yr, race, sex, age, cur_smk, smoking, cv_risk 
from gen_pop_tools.cc_cr_risk_score
order by patient_id, substr(year_month,1,4), year_month desc;
