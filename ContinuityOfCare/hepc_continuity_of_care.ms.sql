﻿USE ESP_TEST
GO
/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                       Continuity of Care weekly Tables
--
--------------------------------------------------------------------------------
--
-- @author: Karen Eberhardt <keberhardt@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2017 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some PostgreSQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/
--This table is used to map local race values to standard set.
IF object_id('gen_pop_tools.cc_racemap', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_racemap;
GO
create table gen_pop_tools.cc_racemap
  (source_field  varchar(65),
   source_value  varchar(100),
   target_value  varchar(100),
   primary key (source_field, source_value)); --primary key enforces unique mapping
GO
--add any local race values to be mapped to the set here.
insert into gen_pop_tools.cc_racemap
(source_field, source_value, target_value) values
('ethnicity','Y','HISPANIC'),
('ethnicity','HISPANIC/LATINO','HISPANIC'),
('race','American Indian / Alaska Native','OTHER'),
('race','American Indian/Alaska Native','OTHER'),
('race','Asian','ASIAN'),
('race','Black / African American','BLACK'),
('race','Black/African American','BLACK'),
('race','More than One Race','OTHER'),
('race','Native Hawaiian','OTHER'),
('race','Other Pacific Islander','OTHER'),
('race','Pacific Islander','OTHER'),
('race','White','CAUCASIAN'),
('race','NATIVE HAWAI','OTHER'),
('race','ALASKAN','OTHER'),
('race','ASIAN','ASIAN'),
('race','CAUCASIAN','CAUCASIAN'),
('race','INDIAN','ASIAN'),
('race','NAT AMERICAN','OTHER'),
('race','HISPANIC','HISPANIC'),
('race','OTHER','OTHER'),
('race','PACIFIC ISLANDER/HAWAIIAN','OTHER'),
('race','AMERICAN INDIAN/ALASKAN NATIVE','OTHER'),
('race','WHITE','CAUCASIAN'),
('race','BLACK','BLACK');
GO

-- Using tt_pat_seq_enc, so this code must be run after tt stuff
-- This table has one row per year (FROM gen_pop_tools.2010) per patient (including current year)
--  where patient has had at least one encounter for that year.
--  This is the "Total Population Observed"
IF object_id('gen_pop_tools.cc_pat_seq_enc', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_pat_seq_enc;
GO
select patient_id, 
  SUBSTRING(year_month,1,4) index_enc_yr 
INTO gen_pop_tools.cc_pat_seq_enc
FROM gen_pop_tools.tt_pat_seq_enc
where (SUBSTRING(year_month,6,2)='12' 
        or year_month=(select max(year_month) FROM gen_pop_tools.tt_pat_seq_enc)) 
      and prior1>=1 and SUBSTRING(year_month,1,4)>='2010';
GO

-- This table joins in patient demographic data	  
IF object_id('gen_pop_tools.cc_pat_seq_enc_patinfo', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_pat_seq_enc_patinfo;
GO
SELECT ip.patient_id, ip.index_enc_yr, 
case when upper(SUBSTRING(p.gender,1,1)) = 'F' then 'Female'
	 when upper(SUBSTRING(p.gender,1,1)) = 'M' then 'Male'
	 else 'Unknown'
end sex,
case 
     when (select target_value FROM gen_pop_tools.tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity) is not null
	   then (select target_value FROM gen_pop_tools.tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity)
     when (select target_value FROM gen_pop_tools.tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race) is not null
	   then (select target_value FROM gen_pop_tools.tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race)
     else 'UNKNOWN'
  end race_ethnicity
, case
    when datepart(year, date_of_birth)<1945 then 'Born prior to 1945'
	when datepart(year, date_of_birth)>=1945 and datepart(year, date_of_birth)<= 1965 then 'Born 1945 to 1965'
	when datepart(year, date_of_birth)>1965 then 'Born after 1965'
    else 'Unknown'
  end as birth_cohort
,  case 
    when gen_pop_tools.age_2(getdate(), date_of_birth)<=19 then '0-19'
    when gen_pop_tools.age_2(getdate(), date_of_birth)>=20 and gen_pop_tools.age_2(getdate(), date_of_birth) <=39 then '20-39'
    when gen_pop_tools.age_2(getdate(), date_of_birth)>=40 and gen_pop_tools.age_2(getdate(), date_of_birth) <=59 then '40-59'
    when gen_pop_tools.age_2(getdate(), date_of_birth)>=60 then 'Over 59'
    else 'UNKNOWN'
  end as agegroup
INTO gen_pop_tools.cc_pat_seq_enc_patinfo
FROM gen_pop_tools.cc_pat_seq_enc ip
JOIN dbo.emr_patient p ON ip.patient_id = p.id;
GO

-- Find all patients who ever had a mapped hep_c lab test
IF object_id('gen_pop_tools.cc_hepc_labs', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_labs;
GO
SELECT patient_id, name, date
INTO gen_pop_tools.cc_hepc_labs
FROM dbo.hef_event 
WHERE name like 'lx:hepatitis_c%'
  and date>=CAST('2010-01-01' AS date);
GO
-- Find the date of the first hep_c lab result (any test type)
IF object_id('gen_pop_tools.cc_hepc_firsttest_dates', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_firsttest_dates;
GO
SELECT patient_id, CAST(DATEPART(year, min(date)) AS varchar(4)) firsttest_yr
INTO gen_pop_tools.cc_hepc_firsttest_dates
FROM gen_pop_tools.cc_hepc_labs
GROUP BY patient_id;
GO

-- Find all patients who ever had a positive hep_c lab result
IF object_id('gen_pop_tools.cc_hepc_pos_labs', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_pos_labs;
GO
SELECT patient_id, name, date 
INTO gen_pop_tools.cc_hepc_pos_labs
FROM gen_pop_tools.cc_hepc_labs 
WHERE name in (
'lx:hepatitis_c_elisa:positive',
'lx:hepatitis_c_riba:positive',
'lx:hepatitis_c_rna:positive',
'lx:hepatitis_c_signal_cutoff:positive');
GO
-- Find the date of the first pos hep_c lab result (any test type)
IF object_id('gen_pop_tools.cc_hepc_index_dates', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_index_dates;
GO
SELECT patient_id, CAST(DATEPART(year, min(date)) AS varchar(4)) index_enc_yr, min(date) AS index_pos_date
INTO gen_pop_tools.cc_hepc_index_dates
FROM gen_pop_tools.cc_hepc_pos_labs
GROUP BY patient_id;
GO

 -- Get RNA lab details for the index pos patients
IF object_id('gen_pop_tools.cc_hepc_rna_lab', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_rna_lab;
GO
SELECT l.patient_id, l.native_name, l.date, l.result_float
INTO gen_pop_tools.cc_hepc_rna_lab
FROM dbo.emr_labresult l
join dbo.conf_labtestmap m on l.native_code = m.native_code 
join gen_pop_tools.cc_hepc_index_dates ip on ip.patient_id=l.patient_id
WHERE l.date >= ip.index_pos_date
AND m.test_name = 'hepatitis_c_rna';
GO
-- now get a patient-year table
IF object_id('gen_pop_tools.cc_hepc_rna_lab_yr', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_rna_lab_yr;
GO
SELECT patient_id, CAST(DATEPART(year, date) AS varchar(4)) rna_yr
INTO gen_pop_tools.cc_hepc_rna_lab_yr
FROM gen_pop_tools.cc_hepc_rna_lab
group by patient_id, CAST(DATEPART(year, date) AS varchar(4));
GO

-- Get pos RNA (detectable) years
IF object_id('gen_pop_tools.cc_hepc_rna_pos', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_rna_pos;
GO
SELECT patient_id, CAST(DATEPART(year, date) AS varchar(4)) rna_pos_yr
INTO gen_pop_tools.cc_hepc_rna_pos
FROM gen_pop_tools.cc_hepc_pos_labs
where name='lx:hepatitis_c_rna:positive'
group by patient_id, CAST(DATEPART(year, date) AS varchar(4));
GO

-- Get most recent RNA test
IF object_id('gen_pop_tools.cc_hepc_most_recent_rna', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_most_recent_rna;
GO
SELECT patient_id, max(date) last_rna_date
INTO gen_pop_tools.cc_hepc_most_recent_rna
FROM gen_pop_tools.cc_hepc_rna_lab
GROUP BY patient_id;
GO
-- Get patients whose last RNA test was pos
IF object_id('gen_pop_tools.cc_hepc_most_recent_rna_pos', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_most_recent_rna_pos;
GO
SELECT mr.patient_id, mr.last_rna_date, CAST(DATEPART(year, mr.last_rna_date) AS varchar(4)) last_rna_pos_yr
INTO gen_pop_tools.cc_hepc_most_recent_rna_pos
FROM gen_pop_tools.cc_hepc_most_recent_rna mr
join gen_pop_tools.cc_hepc_pos_labs pl on pl.date=mr.last_rna_date and pl.patient_id=mr.patient_id and pl.name='lx:hepatitis_c_rna:positive'
group by mr.patient_id, mr.last_rna_date, CAST(DATEPART(year, mr.last_rna_date) AS varchar(4)); --in case there are multiple pos labs on last rna date.
GO

IF object_id('gen_pop_tools.cc_hepc_most_recent_rna_neg', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_most_recent_rna_neg;
GO
SELECT mr.patient_id, mr.last_rna_date, CAST(DATEPART(year, mr.last_rna_date) AS varchar(4)) last_rna_neg_yr
INTO gen_pop_tools.cc_hepc_most_recent_rna_neg
FROM gen_pop_tools.cc_hepc_most_recent_rna mr
EXCEPT select * FROM gen_pop_tools.cc_hepc_most_recent_rna_pos;
GO

 -- Find patients with an ESP case for acute hep_c
IF object_id('gen_pop_tools.cc_hepc_esp_acute', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_esp_acute;
GO
SELECT patient_id, date as hepc_acute_date, CAST(DATEPART(year, date) AS varchar(4)) hepc_acute_yr
INTO gen_pop_tools.cc_hepc_esp_acute
FROM dbo.nodis_case c
WHERE condition = 'hepatitis_c' and exists (select null FROM dbo.nodis_caseactivehistory cac where c.id=cac.case_id and cac.status='HEP_C-A');
GO

-- Find all hepc meds for index patients
IF object_id('gen_pop_tools.cc_hepc_all_rx', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_all_rx;
GO
select ip.patient_id, rx.name, rx.date, start_date, end_date, refills 
INTO gen_pop_tools.cc_hepc_all_rx
FROM dbo.emr_prescription rx
join gen_pop_tools.cc_hepc_index_dates ip on rx.patient_id = ip.patient_id
WHERE 
  (lower(rx.name) like lower('%interferon alfa-2b%')
  OR lower(rx.name) like lower('%pegylated interferon alfa%')
  OR lower(rx.name) like lower('%sofosbuvir%')
  OR lower(rx.name) like lower('%ribavirin%')
  OR lower(rx.name) like lower('%boceprevir%')
  OR lower(rx.name) like lower('%telaprevir%')
  OR lower(rx.name) like lower('%simeprevir%')
  OR lower(rx.name) like lower('%ledipasvir%sofosbuvir%')
  OR lower(rx.name) like lower('%ombitasvir%paritaprevir%ritonavir%dasabuvir%')
  OR lower(rx.name) like lower('%ombitasvir%paritaprevir%ritonavir%')
  OR lower(rx.name) like lower('%daclatasvir%')
  OR lower(rx.name) like lower('%elbasvir%grazoprevir%')
  OR lower(rx.name) like lower('%sofosbuvir%velpatasvir%')
  OR lower(rx.name) like lower('%Pegintron%')
  OR lower(rx.name) like lower('%Pegasys%')
  OR lower(rx.name) like lower('%Sovaldi%')
  OR lower(rx.name) like lower('%Copegus%')
  OR lower(rx.name) like lower('%Rebetol%')
  OR lower(rx.name) like lower('%Virazole%')
  OR lower(rx.name) like lower('%Rebetron%')
  OR lower(rx.name) like lower('%Victrelis%')
  OR lower(rx.name) like lower('%Incivek%')
  OR lower(rx.name) like lower('%Olysio%')
  OR lower(rx.name) like lower('%Harvoni%')
  OR lower(rx.name) like lower('%Viekira Pak%')
  OR lower(rx.name) like lower('%Technivie%')
  OR lower(rx.name) like lower('%Daklinza%')
  OR lower(rx.name) like lower('%Zepatier%')
  OR lower(rx.name) like lower('%Epclusa%')
  OR lower(rx.name) like lower('%OMBITAS%PARITAPRE%RITONA%DASAB%')
  OR lower(rx.name) like lower('%OMBITA%PARITAP%RITON%DASABUVIR%')
  OR lower(rx.name) like lower('%OMBIT%PARITAP%RITONAV%')
  OR lower(rx.name) like lower('%PEG-INTRON%')
  OR lower(rx.name) like lower('%INTRON%')
  OR lower(rx.name) like lower('%RIBASPHERE%')
  OR lower(rx.name) like lower('%PEGINTERFERON ALFA-2A%')
  OR lower(rx.name) like lower('%INTERFERON ALPHA 2-B%'))
  and coalesce(rx.date, rx.start_date)>=CAST('2010-01-01' AS date);
GO

-- Filter For HepC Rx with a Date or Start Date Following the Index Positive Date
-- Compute variations on the end_date
IF object_id('gen_pop_tools.cc_hepc_rx_after_ip', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_rx_after_ip;
GO
select ip.patient_id, ip.index_pos_date, rx.name, rx.date, rx.start_date, rx.end_date, rx.refills,
DATEADD(day, (30 * (coalesce(CAST(refills AS integer) +1, CAST(1 AS integer)))), start_date) computed_end_date_start_30_refill,
DATEADD(day, 90, start_date) computed_end_date_start_plus90
INTO gen_pop_tools.cc_hepc_rx_after_ip
FROM gen_pop_tools.cc_hepc_index_dates ip, gen_pop_tools.cc_hepc_all_rx rx
WHERE ip.patient_id = rx.patient_id
AND (rx.date >= ip.index_pos_date or rx.start_date >= ip.index_pos_date);
GO

-- RX first start and last end
IF object_id('gen_pop_tools.cc_hepc_rx_after_ip_start_end', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_rx_after_ip_start_end;
GO
SELECT t1.patient_id, min(coalesce(t1.start_date, t1.date)) min_start, max(t2.end_dt) max_end
INTO gen_pop_tools.cc_hepc_rx_after_ip_start_end
FROM gen_pop_tools.cc_hepc_rx_after_ip t1
  INNER JOIN (
   SELECT patient_id as patid, end_date as end_dt FROM gen_pop_tools.cc_hepc_rx_after_ip
   UNION ALL
   SELECT patient_id as patid, computed_end_date_start_30_refill as end_dt FROM gen_pop_tools.cc_hepc_rx_after_ip
   UNION ALL
   SELECT patient_id as patid, computed_end_date_start_plus90 as end_dt FROM gen_pop_tools.cc_hepc_rx_after_ip
   ) t2 on t2.patid = t1.patient_id
GROUP BY t1.patient_id;
GO

-- Acute Resolved no treatment
IF object_id('gen_pop_tools.cc_hepc_acute_res_norx', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_acute_res_norx;
GO
select a.patient_id, a.hepc_acute_yr
INTO gen_pop_tools.cc_hepc_acute_res_norx
FROM gen_pop_tools.cc_hepc_esp_acute a
join gen_pop_tools.cc_hepc_most_recent_rna_neg rn on rn.patient_id=a.patient_id -- resolved
left join gen_pop_tools.cc_hepc_rx_after_ip rx on rx.patient_id=a.patient_id
where rx.patient_id is null -- no rx
group by a.patient_id, a.hepc_acute_yr;
GO

-- Acute persistent
IF object_id('gen_pop_tools.cc_hepc_acute_persist', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_acute_persist;
GO
select a.patient_id, a.hepc_acute_yr
INTO gen_pop_tools.cc_hepc_acute_persist
FROM gen_pop_tools.cc_hepc_esp_acute a
left join gen_pop_tools.cc_hepc_most_recent_rna_pos pl on pl.patient_id=a.patient_id 
  and a.hepc_acute_date<=DATEADD(year, -1, pl.last_rna_date)
left join gen_pop_tools.cc_hepc_rx_after_ip rx on rx.patient_id=a.patient_id 
  and a.hepc_acute_date<=DATEADD(year, -1, coalesce(rx.date,rx.start_date))
where rx.patient_id is not null or pl.patient_id is not null -- either rx or pl after 1 year
group by a.patient_id, a.hepc_acute_yr;
GO

-- Acute Unknowns
IF object_id('gen_pop_tools.cc_hepc_acute_unknown', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_acute_unknown;
GO
SELECT a.patient_id, a.hepc_acute_yr
INTO gen_pop_tools.cc_hepc_acute_unknown
FROM gen_pop_tools.cc_hepc_esp_acute a
left join gen_pop_tools.cc_hepc_acute_persist p on p.patient_id=a.patient_id and p.hepc_acute_yr=a.hepc_acute_yr
left join gen_pop_tools.cc_hepc_acute_res_norx rx on rx.patient_id=a.patient_id and rx.hepc_acute_yr=a.hepc_acute_yr
where p.patient_id is null and rx.patient_id is null;
GO

-- HepC Chronic
IF object_id('gen_pop_tools.cc_hepc_chronic', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_chronic;
GO
SELECT ip.patient_id, CAST(DATEPART(year, ip.index_pos_date) AS varchar(4)) index_pos_yr
INTO gen_pop_tools.cc_hepc_chronic
FROM gen_pop_tools.cc_hepc_index_dates ip
left join gen_pop_tools.cc_hepc_esp_acute a on a.patient_id=ip.patient_id
left join gen_pop_tools.cc_hepc_acute_persist ap on ap.patient_id=ip.patient_id
left join gen_pop_tools.cc_hepc_acute_unknown au on au.patient_id=ip.patient_id
where a.patient_id is null or ap.patient_id is not null or au.patient_id is not null;
GO

-- Chronic, no rx
IF object_id('gen_pop_tools.cc_hepc_chronic_norx', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_chronic_norx;
GO
select ch.patient_id, ch.index_pos_yr
INTO gen_pop_tools.cc_hepc_chronic_norx
FROM gen_pop_tools.cc_hepc_chronic ch
left join gen_pop_tools.cc_hepc_rx_after_ip rx on ch.patient_id=rx.patient_id
where rx.patient_id is null;
GO

-- Chronic, with rx
IF object_id('gen_pop_tools.cc_hepc_chronic_rx', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_chronic_rx;
GO
select ch.patient_id, ch.index_pos_yr
INTO gen_pop_tools.cc_hepc_chronic_rx
FROM gen_pop_tools.cc_hepc_chronic ch
left join gen_pop_tools.cc_hepc_chronic_norx norx on ch.patient_id=norx.patient_id
where norx.patient_id is null;
GO

-- Chronic, no rx, resolved
IF object_id('gen_pop_tools.cc_hepc_chronic_norx_res', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_chronic_norx_res;
GO
SELECT ch.patient_id, ch.index_pos_yr
INTO gen_pop_tools.cc_hepc_chronic_norx_res
FROM gen_pop_tools.cc_hepc_chronic_norx ch
join gen_pop_tools.cc_hepc_most_recent_rna_neg nrna on nrna.patient_id=ch.patient_id;
GO

-- Chronic, no rx, persistent
IF object_id('gen_pop_tools.cc_hepc_chronic_norx_persist', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_chronic_norx_persist;
GO
SELECT ch.patient_id, ch.index_pos_yr
INTO gen_pop_tools.cc_hepc_chronic_norx_persist
FROM gen_pop_tools.cc_hepc_chronic_norx ch
join gen_pop_tools.cc_hepc_most_recent_rna_pos prna on prna.patient_id=ch.patient_id;
GO

-- Chronic, no rx, unknown
IF object_id('gen_pop_tools.cc_hepc_chronic_norx_unknown', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_chronic_norx_unknown;
GO
SELECT ch.patient_id, ch.index_pos_yr
INTO gen_pop_tools.cc_hepc_chronic_norx_unknown
FROM gen_pop_tools.cc_hepc_chronic_norx ch
left join gen_pop_tools.cc_hepc_most_recent_rna_pos prna on prna.patient_id=ch.patient_id
left join gen_pop_tools.cc_hepc_most_recent_rna_neg nrna on nrna.patient_id=ch.patient_id
where prna.patient_id is null and nrna.patient_id is null;
GO

-- Chronic, rx, no viral load test
IF object_id('gen_pop_tools.cc_hepc_chronic_rx_nolb', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_chronic_rx_nolb;
GO
SELECT ch.patient_id, ch.index_pos_yr
INTO gen_pop_tools.cc_hepc_chronic_rx_nolb
FROM gen_pop_tools.cc_hepc_chronic_rx ch
join gen_pop_tools.cc_hepc_rx_after_ip_start_end rx on rx.patient_id=ch.patient_id
left join gen_pop_tools.cc_hepc_rna_lab lb on lb.patient_id=rx.patient_id and lb.date>rx.min_start 
where lb.patient_id is null;
GO

-- Chronic, rx, subsequent viral load test
IF object_id('gen_pop_tools.cc_hepc_chronic_rx_lb', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_chronic_rx_lb;
GO
SELECT ch.patient_id, ch.index_pos_yr
INTO gen_pop_tools.cc_hepc_chronic_rx_lb
FROM gen_pop_tools.cc_hepc_chronic_rx ch
left join gen_pop_tools.cc_hepc_chronic_rx_nolb rx on rx.patient_id=ch.patient_id
where rx.patient_id is null;
GO

-- Chronic, rx, last rna negative
IF object_id('gen_pop_tools.cc_hepc_chronic_rx_last_neg', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_chronic_rx_last_neg;
GO
SELECT ch.patient_id, ch.index_pos_yr
INTO gen_pop_tools.cc_hepc_chronic_rx_last_neg
FROM gen_pop_tools.cc_hepc_chronic_rx_lb ch
join gen_pop_tools.cc_hepc_most_recent_rna_neg rna on rna.patient_id=ch.patient_id;
GO

-- Chronic, rx, negative sustained
IF object_id('gen_pop_tools.cc_hepc_chronic_rx_neg_sustained', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_chronic_rx_neg_sustained;
GO
SELECT ch.patient_id, ch.index_pos_yr
INTO gen_pop_tools.cc_hepc_chronic_rx_neg_sustained
FROM gen_pop_tools.cc_hepc_chronic_rx_lb ch
join gen_pop_tools.cc_hepc_most_recent_rna_neg rna on rna.patient_id=ch.patient_id
join gen_pop_tools.cc_hepc_rx_after_ip_start_end rx on rx.patient_id=rna.patient_id 
   and DATEADD(week, 24, rx.max_end) <= rna.last_rna_date;
GO

-- Join it all together
IF object_id('gen_pop_tools.cc_hepc_by_pat', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_by_pat;
GO
SELECT t0.patient_id, t0.index_enc_yr
 , t0.sex, t0.race_ethnicity, t0.birth_cohort, t0.agegroup
 , t1.patient_id tested0
 , t2.patient_id indexpos0
 , t3.patient_id vltest0
 , t4.patient_id vload0
 , t5.patient_id lastvlpos0
 , t6.patient_id lastvlneg0
 , t7.patient_id tested1
 , t8.patient_id indexpos1
 , t9.patient_id vltest1
 , t10.patient_id vload1
 , t11.patient_id lastvlpos1
 , t12.patient_id lastvlneg1
 , t13.patient_id acute
 , t14.patient_id acute_res_norx
 , t15.patient_id acute_res_prst
 , t16.patient_id acute_res_unkn
 , t17.patient_id chronic
 , t18.patient_id chron_notrt
 , t19.patient_id chron_notrt_res
 , t20.patient_id chron_notrt_prst
 , t21.patient_id chron_notrt_unkn
 , t22.patient_id chron_trt
 , t23.patient_id chron_trt_unkn
 , t24.patient_id chron_trt_vload
 , t25.patient_id chron_trt_lastvlneg
 , t26.patient_id chron_trt24_lastvlneg
INTO gen_pop_tools.cc_hepc_by_pat
FROM gen_pop_tools.cc_racemap t0
left join gen_pop_tools.cc_hepc_firsttest_dates t1 
  on t0.patient_id=t1.patient_id 
     and t0.index_enc_yr>=t1.firsttest_yr
left join gen_pop_tools.cc_hepc_pos_labs t2
  on t0.patient_id=t2.patient_id 
     and t0.index_enc_yr>=CAST(DATEPART(year, t2.index_pos_date) AS varchar(4))
left join gen_pop_tools.cc_hepc_rna_lab_yr t3
  on t2.patient_id=t3.patient_id 
left join gen_pop_tools.cc_hepc_rna_pos t4 
  on t3.patient_id=t4.patient_id 
left join gen_pop_tools.cc_hepc_most_recent_rna_pos t5 
  on t3.patient_id=t5.patient_id 
left join gen_pop_tools.cc_hepc_most_recent_rna_neg t6 
  on t3.patient_id=t6.patient_id 
left join gen_pop_tools.cc_hepc_firsttest_dates t7 
  on t0.patient_id=t7.patient_id 
     and t0.index_enc_yr=t7.firsttest_yr
left join gen_pop_tools.cc_hepc_pos_labs t8
  on t0.patient_id=t8.patient_id 
     and t0.index_enc_yr=CAST(DATEPART(year, t8.index_pos_date) AS varchar(4))
left join gen_pop_tools.cc_hepc_rna_lab_yr t9
  on t8.patient_id=t9.patient_id 
left join gen_pop_tools.cc_hepc_rna_pos t10 
  on t9.patient_id=t10.patient_id 
left join gen_pop_tools.cc_hepc_most_recent_rna_pos t11 
  on t9.patient_id=t11.patient_id 
left join gen_pop_tools.cc_hepc_most_recent_rna_neg t12
  on t9.patient_id=t12.patient_id 
left join gen_pop_tools.cc_hepc_esp_acute t13
  on t0.patient_id=t13.patient_id
      and t0.index_enc_yr=t13.hepc_acute_yr
left join gen_pop_tools.cc_hepc_acute_res_norx t14
  on t13.patient_id=t14.patient_id
left join gen_pop_tools.cc_hepc_acute_persist t15
  on t13.patient_id=t15.patient_id
left join gen_pop_tools.cc_hepc_acute_unknown t16
  on t13.patient_id=t16.patient_id
left join gen_pop_tools.cc_hepc_chronic t17
  on t0.patient_id=t17.patient_id
     and t0.index_enc_yr=t17.index_pos_yr
left join gen_pop_tools.cc_hepc_chronic_norx t18
  on t17.patient_id=t18.patient_id
left join gen_pop_tools.cc_hepc_chronic_norx_res t19
  on t18.patient_id=t19.patient_id
left join gen_pop_tools.cc_hepc_chronic_norx_persist t20
  on t18.patient_id=t20.patient_id
left join gen_pop_tools.cc_hepc_chronic_norx_unknown t21
  on t18.patient_id=t21.patient_id
left join gen_pop_tools.cc_hepc_chronic_rx t22
  on t17.patient_id=t22.patient_id
left join gen_pop_tools.cc_hepc_chronic_rx_nolb t23
  on t22.patient_id=t23.patient_id
left join gen_pop_tools.cc_hepc_chronic_rx_lb t24
  on t22.patient_id=t24.patient_id
left join gen_pop_tools.cc_hepc_chronic_rx_last_neg t25
  on t24.patient_id=t25.patient_id
left join gen_pop_tools.cc_hepc_chronic_rx_neg_sustained t26
  on t24.patient_id=t26.patient_id
group by t0.patient_id, t0.index_enc_yr
 , t0.sex, t0.race_ethnicity, t0.birth_cohort, t0.agegroup
 , t1.patient_id 
 , t2.patient_id 
 , t3.patient_id 
 , t4.patient_id 
 , t5.patient_id 
 , t6.patient_id 
 , t7.patient_id 
 , t8.patient_id 
 , t9.patient_id 
 , t10.patient_id
 , t11.patient_id
 , t12.patient_id
 , t13.patient_id 
 , t14.patient_id
 , t15.patient_id
 , t16.patient_id
 , t17.patient_id 
 , t18.patient_id  
 , t19.patient_id  
 , t20.patient_id  
 , t21.patient_id  
 , t22.patient_id  
 , t23.patient_id  
 , t24.patient_id  
 , t25.patient_id  
 , t26.patient_id  
;
GO

--Now get the crossing of all stratifiers
IF object_id('gen_pop_tools.strat1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.strat1;
GO
create table gen_pop_tools.strat1 (id1 varchar(2), name1 varchar(15));
GO
insert into gen_pop_tools.strat1 values ('1','index_enc_yr'),('x','x');
GO
IF object_id('gen_pop_tools.strat2', 'U') IS NOT NULL DROP TABLE gen_pop_tools.strat2;
GO
create table gen_pop_tools.strat2 (id2 varchar(2), name2 varchar(15));
GO
insert into gen_pop_tools.strat2 values ('2','agegroup'),('x','x');
GO
IF object_id('gen_pop_tools.strat3', 'U') IS NOT NULL DROP TABLE gen_pop_tools.strat3;
GO
create table gen_pop_tools.strat3 (id3 varchar(2), name3 varchar(15));
GO
insert into gen_pop_tools.strat3 values ('3','birth_cohort'),('x','x');
GO
IF object_id('gen_pop_tools.strat4', 'U') IS NOT NULL DROP TABLE gen_pop_tools.strat4;
GO
create table gen_pop_tools.strat4 (id4 varchar(2), name4 varchar(15));
GO
insert into gen_pop_tools.strat4 values ('4','sex'),('x','x');
GO
IF object_id('gen_pop_tools.strat5', 'U') IS NOT NULL DROP TABLE gen_pop_tools.strat5;
GO
create table gen_pop_tools.strat5 (id5 varchar(2), name5 varchar(15));
GO
insert into gen_pop_tools.strat5 values ('5','race_ethnicity'),('x','x');
GO
IF object_id('gen_pop_tools.cc_stratifiers_crossed', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_stratifiers_crossed;
GO
create table gen_pop_tools.cc_stratifiers_crossed as
GO
select * FROM gen_pop_tools.strat1, gen_pop_tools.strat2, gen_pop_tools.strat3, gen_pop_tools.strat4, gen_pop_tools.strat5;
GO

--create stub table to contain summary results
IF object_id('gen_pop_tools.cc_hepc_summaries', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hepc_summaries;
GO
create table gen_pop_tools.cc_hepc_summaries (
  index_enc_yr varchar(4),
  agegroup varchar(10), 
  birth_cohort varchar(25), 
  sex varchar(10), 
  race_ethnicity varchar(25),
  total integer,
  tested0 integer,
  indexpos0 integer,
  vltest0 integer,
  vload0 integer,
  lastvlpos0 integer,
  lastvlneg0 integer,
  tested1 integer,
  indexpos1 integer,
  vltest1 integer,
  vload1 integer,
  lastvlpos1 integer,
  lastvlneg1 integer,
  acute integer,
  acute_res_norx integer,
  acute_res_prst integer,
  acute_res_unkn integer,
  chronic integer,
  chron_notrt integer,
  chron_notrt_res integer,
  chron_notrt_prst integer,
  chron_notrt_unkn integer,
  chron_trt integer,
  chron_trt_unkn integer,
  chron_trt_vload integer,
  chron_trt_lastvlneg integer,
  chron_trt24_lastvlneg integer);
GO

 --Run the various groupings.  This would be much easier in Postgres 9.5 where the "grouping sets" query feature was added.
BEGIN
  DECLARE @groupclauses TABLE(id int, val nvarchar(max));
  DECLARE @groupby nvarchar(max);
  DECLARE @insrtsql nvarchar(max);
  DECLARE @tmpsql nvarchar(max);
  DECLARE @i integer;
  DECLARE @name1 varchar(max);
  DECLARE @name2 varchar(max);
  DECLARE @name3 varchar(max);
  DECLARE @name4 varchar(max);
  DECLARE @name5 varchar(max);
  DECLARE cur CURSOR FOR select name1, name2, name3, name4, name5 FROM gen_pop_tools.cc_cr_stratifiers_crossed;
  OPEN cur  
  FETCH NEXT FROM gen_pop_tools.cur INTO @name1, @name2, @name3, @name4, @name5 
  WHILE @@FETCH_STATUS = 0 BEGIN
    SET @i=0;
    SET @insrtsql:='insert into gen_pop_tools.cc_hepc_summaries (index_enc_yr, agegroup, birth_cohort, sex, race_ethnicity, 
      total, tested0, indexpos0, vltest0, vload0, lastvlpos0, lastvlneg0,
      tested1, indexpos1, vltest1, vload1, lastvlpos1, lastvlneg1,
      acute, acute_res_norx, acute_res_prst, acute_res_unkn, 
      chronic, chron_notrt, chron_notrt_res, chron_notrt_prst, chron_notrt_unkn, 
      chron_trt, chron_trt_unkn, chron_trt_vload, chron_trt_lastvlneg, chron_trt24_lastvlneg) 
      (select';
    if @name1='x'
      SET @tmpsql=' CAST(''x'' AS varchar(max)) index_enc_yr,';
    else BEGIN
      SET @tmpsql=' index_enc_yr,';
      SET @i=@i+1;
      insert into @groupclauses(id, val) VALUES(@i, 'index_enc_yr');
    END;
    SET @insrtsql=@insrtsql+@tmpsql;
    if @name2='x'
      SET @tmpsql=' CAST(''x'' AS varchar(max)) agegroup,';
    else BEGIN 
      SET @tmpsql=' agegroup,';
      SET @i=@i+1;
      insert into @groupclauses(id, val) VALUES(@i, 'agegroup');
    END;
    SET @insrtsql=@insrtsql+@tmpsql;
    if @name3='x'
      SET @tmpsql=' CAST(''x'' AS varchar(max)) birth_cohort,';
    else BEGIN 
      SET @tmpsql=' birth_cohort,';
      SET @i=@i+1;
      insert into @groupclauses(id, val) VALUES(@i, 'birth_cohort');
    END;
    SET @insrtsql=@insrtsql+@tmpsql;
    if @name4='x'
      SET @tmpsql=' CAST(''x'' AS varchar(max)) sex,';
    else BEGIN 
      SET @tmpsql=' sex,';
      SET @i=@i+1;
      insert into @groupclauses(id, val) VALUES(@i, 'sex');
    END;
    SET @insrtsql=@insrtsql+@tmpsql;
    if @name5='x'
      SET @tmpsql=' CAST(''x'' AS varchar(max)) race_ethnicity,';
    else BEGIN 
      SET @tmpsql=' race_ethnicity,';
      SET @i=@i+1;
      insert into @groupclauses(id, val) VALUES(@i, 'race_ethnicity');
    END;
    SET @insrtsql=@insrtsql+@tmpsql;
    SET @insrtsql=@insrtsql+'
       count(distinct patient_id) total,
       count(distinct tested0) tested0,
       count(distinct indexpos0) indexpos0,
       count(distinct vltest0) vltest0,
       count(distinct vload0) vload0,
       count(distinct lastvlpos0) lastvlpos0,
       count(distinct lastvlneg0) lastvlneg0,
       count(distinct tested1) tested1,
       count(distinct indexpos1) indexpos1,
       count(distinct vltest1) vltest1,
       count(distinct vload1) vload1,
       count(distinct lastvlpos1) lastvlpos1,
       count(distinct lastvlneg1) lastvlneg1,
       count(distinct acute) acute,
       count(distinct acute_res_norx) acute_res_norx,
       count(distinct acute_res_prst) acute_res_prst,
       count(distinct acute_res_unkn) acute_res_unkn,
       count(distinct chronic) chronic,
       count(distinct chron_notrt) chron_notrt,
       count(distinct chron_notrt_res) chron_notrt_res,
       count(distinct chron_notrt_prst) chron_notrt_prst,
       count(distinct chron_notrt_unkn) chron_notrt_unkn,
       count(distinct chron_trt) chron_trt,
       count(distinct chron_trt_unkn) chron_trt_unkn,
       count(distinct chron_trt_vload) chron_trt_vload,
       count(distinct chron_trt_lastvlneg) chron_trt_lastvlneg,
       count(distinct chron_trt24_lastvlneg) chron_trt24_lastvlneg
       FROM gen_pop_tools.cc_hepc_by_pat
       ';
    if @i> 0 BEGIN 
      DECLARE @length int;
      SET @insrtsql=@insrtsql+'group by ';
      SET @i=1;
      SET @length = (SELECT Count(*) FROM gen_pop_tools.@groupclauses);
      while @i <= @length BEGIN
        if @i >1 BEGIN 
          SET @insrtsql=@insrtsql+', '; 
        END;
        SET @groupby = (SELECT val FROM gen_pop_tools.@groupclauses WHERE id=@i);
        SET @insrtsql=@insrtsql+@groupby;
        SET @i=@i+1;
      END;
    END;
    SET @insrtsql=@insrtsql+')';
    execute @insrtsql;
    --raise notice '%', @insrtsql;
    FETCH NEXT FROM gen_pop_tools.cur INTO @name1, @name2, @name3, @name4, @name5 
  END;
  CLOSE cur;
  DEALLOCATE cur;
END;
GO

--Gather up the values of the stratifiers and assign code.
IF object_id('gen_pop_tools.cc_agegroup_codevals', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_agegroup_codevals;
GO
Create table gen_pop_tools.cc_agegroup_codevals (agegroup varchar(7), codeval varchar(1));
GO
insert into gen_pop_tools.cc_agegroup_codevals (agegroup, codeval) 
values ('0-19','1'),('20-39','2'),('40-59','3'),('Over 59','4'),('UNKNOWN','5');
GO

IF object_id('gen_pop_tools.cc_birth_cohort_codevals', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_birth_cohort_codevals;
GO
Create table gen_pop_tools.cc_birth_cohort_codevals (birth_cohort varchar(18), codeval varchar(1));
GO
insert into gen_pop_tools.cc_birth_cohort_codevals (birth_cohort, codeval) 
values ('Born prior to 1945','1'),('Born 1945 to 1965','2'),('Born after 1965','3'),('Unknown','4');
GO

IF object_id('gen_pop_tools.cc_sex_codevals', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_sex_codevals;
GO
Create table gen_pop_tools.cc_sex_codevals (sex varchar(7), codeval varchar(1));
GO
insert into gen_pop_tools.cc_sex_codevals (sex, codeval) 
values ('Female','1'),('Male','2'),('Unknown','3');
GO

IF object_id('gen_pop_tools.cc_race_ethnicity_codevals', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_race_ethnicity_codevals;
GO
Create table gen_pop_tools.cc_race_ethnicity_codevals (race_ethnicity varchar(10), codeval varchar(1));
GO
insert into gen_pop_tools.cc_race_ethnicity_codevals (race_ethnicity, codeval) 
values ('ASIAN','1'),('BLACK','2'),('CAUCASIAN','3'),('HISPANIC','4'),('OTHER','5'),('UNKNOWN','6');
GO

--now write out to JSON
copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
    '["year","age_group","birth_cohort","sex","race_ethnicity"]'::json filters,
    '{"a":{"name":"Total Population Under Surveillance","description":"Total population observed","nest":"0","pcalc":"a"},"b":{"name":"Patients tested for hepatitis C in selected year or prior years","description":"Patients with an encounter in the selected year(s) whose first test for hepatitis C (elisa or RNA) is in or before selected year","nest":"1","pcalc":"a"},"c":{"name":"Patients with a positive result (RNA or ELISA) in selected year or prior years","description":"Patients with an encounter in the selected year(s) with a positive elisa or RNA hepatitis C result in or before selected year","nest":"2","pcalc":"b"},"d":{"name":"Viral load measured during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with an RNA test (any result value) >= 0 days following first positive hepatitis result","nest":"3","pcalc":"c"},"e":{"name":"Detectable viral load during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with a detectable RNA test >= 0 days following first positive hepatitis result","nest":"4","pcalc":"d"},"f":{"name":"Most recent viral load positive","description":"Patients with an encounter in the selected year(s) whose last viral load was positive","nest":"4","pcalc":"d"}, "g":{"name":"Most recent viral load negative","description":"Patients with an encounter in the selected year(s) whose last viral load was negative","nest":"4","pcalc":"d"},"h":{"name":"Patients tested for hepatitis C in selected year(s)","description":"Patients with an encounter in the selected year(s) whose first test for hepatitis C (elisa or RNA) is in selected year only","nest":"1","pcalc":"a"}, "@i":{"name":"Patients with a positive result (RNA or ELISA) in selected year(s)","description":"Patients with an encounter in the selected year(s) with a positive elisa or RNA hepatitis C result in selected year only","nest":"2","pcalc":"h"}, "j":{"name":"Viral load measured during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with an RNA test (any result value) >= 0 days following first positive hepatitis result","nest":"3","pcalc":"@i"},"k":{"name":"Detectable viral load during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with a detectable RNA test >= 0 days following first positive hepatitis result","nest":"4","pcalc":"j"},"l":{"name":"Most recent viral load positive","description":"Patients with an encounter in the selected year(s) whose last viral load was positive","nest":"4","pcalc":"j"}, "m":{"name":"Most recent viral load negative","description":"Patients with an encounter in the selected year(s) whose last viral load was negative","nest":"4","pcalc":"j"}, "n":{"name":"Acute hepatitis C","description":"Patients who met the ESP case definition for acute hep C in the selected year(s)","nest":"3","pcalc":"c"}, "o":{"name":"Resolved, no treatment","description":"Acute hepatitis C cases whose most recent viral load was negative and who have no evidence of ever having been treated","nest":"4","pcalc":"n"}, "p":{"name":"Persistently infected","description":"Acute hepatitis C cases with a positive load at least 1 year following the acute case date or who received medication at least 1 year following the acute case date","nest":"4","pcalc":"n"}, "q":{"name":"Unknown status","description":"Acute hepatitis C cases who have not had a negative viral load within 1 year following case identification or who have not had a viral load measured ≥ 1 year following case identification","nest":"4","pcalc":"n"}, "r":{"name":"Chronic hepatitis C","description":"Patients that had a positive hepatitis C lab and did not spontaneously clear the infection without treatment. Note: this includes acute cases that are persistently infected or have an unknown status","nest":"3","pcalc":"c"}, "s":{"name":"Never on treatment","description":"Chronic hepatitis C cases who have no evidence of being treated","nest":"4","pcalc":"r"},"t":{"name":"Resolved","description":"Chronic hepatitis C cases who have no evidence of being treated and whose last viral load was undetected","nest":"5","pcalc":"s"}, "u":{"name":"Persistently infected","description":"Chronic hepatitis C cases who have no evidence of being treated and whose last viral load was positive","nest":"5","pcalc":"s"}, "v":{"name":"Unknown status","description":"Chronic hepatitis C cases who have no evidence of being treated and have never had a viral load","nest":"5","pcalc":"s"}, "w":{"name":"Received treatment","description":"Chronic hep C cases ever started on HCV treatment","nest":"4","pcalc":"r"}, "x":{"name":"Viral load not measured","description":"Chronic hepatitis C cases who have no follow-up viral load since starting treatment","nest":"5","pcalc":"w"}, "y":{"name":"Viral load measured","description":"Chronic hepatitis C cases who have a follow-up viral load (any result) since starting treatment","nest":"5","pcalc":"w"}, "z":{"name":"Virologic suppression","description":"Chronic hepatitis C cases whose last viral load was undetectable (whether on treatment or post treatment)","nest":"6","pcalc":"y"}, "aa":{"name":"Sustained virological response","description":"Chronic hepatitis C cases with an undetectable viral load who have been off HCV medications for > 24 weeks","nest":"6","pcalc":"y"}}'::json rowmeta,
(select '{'+array_to_string(array_agg(row_),',')+'}' FROM gen_pop_tools. (select ('"'+
        case when index_enc_yr = 'x' then 'YEAR' else index_enc_yr end +
          case when agegroup <> 'x' 
            then (select codeval FROM gen_pop_tools.cc_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end +
          case when birth_cohort <> 'x' 
            then (select codeval FROM gen_pop_tools.cc_birth_cohort_codevals cv where t0.birth_cohort=cv.birth_cohort)
          else 'x' end +
          case when sex <> 'x' 
            then (select codeval FROM gen_pop_tools.cc_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end +
          case when race_ethnicity <> 'x' 
            then (select codeval FROM gen_pop_tools.cc_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end + '":{"a":'+ total
		  + ',"b":' + tested0
		  + ',"c":' + indexpos0 
		  + ',"d":' + vltest0
		  + ',"e":' + vload0
		  + ',"f":' + lastvlpos0
		  + ',"g":' + lastvlneg0
		  + ',"h":' + tested1 
		  + ',"@i":' + indexpos1 
		  + ',"j":' + vltest1  
		  + ',"k":' + vload1 
		  + ',"l":' + lastvlpos1 
		  + ',"m":' + lastvlneg1 
		  + ',"n":' + acute
		  + ',"o":' + acute_res_norx 
		  + ',"p":' + acute_res_prst
		  + ',"q":' + acute_res_unkn 
		  + ',"r":' + chronic
		  + ',"s":' + chron_notrt
		  + ',"t":' + chron_notrt_res
		  + ',"u":' + chron_notrt_prst
		  + ',"v":' + chron_notrt_unkn
		  + ',"w":' + chron_trt
		  + ',"x":' + chron_trt_unkn
		  + ',"y":' + chron_trt_vload
		  + ',"z":' + chron_trt_lastvlneg
		  + ',"aa":' + chron_trt24_lastvlneg
		  + '}') row_
FROM gen_pop_tools.cc_hepc_summaries t0 ) t00)::json rowdata) t1) to :'pathToFile';




  

