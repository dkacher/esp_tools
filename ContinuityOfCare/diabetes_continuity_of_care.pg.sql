﻿/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                       Continuity of Care weekly Tables
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2017 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some PostgreSQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/
set search_path to gen_pop_tools, public;

--This table is used to map local race values to standard set.
drop table if exists cc_racemap;
create table cc_racemap
  (source_field  varchar(65),
   source_value  varchar(100),
   target_value  varchar(100),
   primary key (source_field, source_value)); --primary key enforces unique mapping
--add any local race values to be mapped to the set here.
insert into cc_racemap
(source_field, source_value, target_value) values
('ethnicity','Y','HISPANIC'),
('ethnicity','HISPANIC/LATINO','HISPANIC'),
('race','American Indian / Alaska Native','OTHER'),
('race','American Indian/Alaska Native','OTHER'),
('race','Asian','ASIAN'),
('race','Black / African American','BLACK'),
('race','Black/African American','BLACK'),
('race','More than One Race','OTHER'),
('race','Native Hawaiian','OTHER'),
('race','Other Pacific Islander','OTHER'),
('race','Pacific Islander','OTHER'),
('race','White','CAUCASIAN'),
('race','NATIVE HAWAI','OTHER'),
('race','ALASKAN','OTHER'),
('race','ASIAN','ASIAN'),
('race','CAUCASIAN','CAUCASIAN'),
('race','INDIAN','ASIAN'),
('race','NAT AMERICAN','OTHER'),
('race','HISPANIC','HISPANIC'),
('race','OTHER','OTHER'),
('race','PACIFIC ISLANDER/HAWAIIAN','OTHER'),
('race','AMERICAN INDIAN/ALASKAN NATIVE','OTHER'),
('race','WHITE','CAUCASIAN'),
('race','BLACK','BLACK');

-- Using tt_pat_seq_enc, so this code must be run after tt stuff
-- This table has one row per year (from 2010) per patient (including current year)
--  where patient has had at least one encounter for that year.
--  This is the "Total Population Observed"
drop table if exists cc_pat_seq_enc;
create table cc_pat_seq_enc AS
select patient_id, 
  substr(year_month,1,4) index_enc_yr 
from tt_pat_seq_enc
where (substr(year_month,6,2)='12' 
        or year_month=(select max(year_month) from tt_pat_seq_enc)) 
      and prior1>=1 and substr(year_month,1,4)>='2010';

-- This table joins in patient demographic data	  
DROP TABLE IF EXISTS cc_pat_seq_enc_patinfo;
CREATE TABLE cc_pat_seq_enc_patinfo AS
SELECT ip.patient_id, ip.index_enc_yr, 
case when upper(substr(p.gender,1,1)) = 'F' then 'Female'
	 when upper(substr(p.gender,1,1)) = 'M' then 'Male'
	 else 'Unknown'
end sex,
case 
     when (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity) is not null
	   then (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity)
     when (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race) is not null
	   then (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race)
     else 'UNKNOWN'
  end race_ethnicity
,  case 
    when date_part('year', age(current_date, date_of_birth))<=19 then '0-19'
    when date_part('year', age(current_date, date_of_birth))>=20 and date_part('year', age(current_date, date_of_birth)) <=39 then '20-39'
    when date_part('year', age(current_date, date_of_birth))>=40 and date_part('year', age(current_date, date_of_birth)) <=59 then '40-59'
    when date_part('year', age(current_date, date_of_birth))>=60 then 'Over 59'
    else 'UNKNOWN'
  end as agegroup
FROM cc_pat_seq_enc ip
JOIN emr_patient p ON ip.patient_id = p.id;
	   
--get a1c lab tests
drop table if exists cc_diab_a1c_dt;
create table cc_diab_a1c_dt as
select patient_id, date a1c_dt, result_float
from emr_labresult t0
join conf_labtestmap t1 on t0.native_code=t1.native_code
where t1.test_name='a1c';
drop table if exists cc_diab_a1c;
create table cc_diab_a1c as
select patient_id, to_char(a1c_dt,'yyyy') as a1c_yr, result_float
from cc_diab_a1c_dt t0;
--ever tested
drop table if exists cc_diab_a1c_yr;
create table cc_diab_a1c_yr as
select distinct patient_id, a1c_yr
from cc_diab_a1c;

--get diabetes cases
drop table if exists cc_diab_case;
create table cc_diab_case as
SELECT c.patient_id
	, seq.index_enc_yr 
FROM nodis_case c
join cc_pat_seq_enc seq on seq.patient_id=c.patient_id
join (select h.case_id, h.status, h.date as strtdt, 
             case 
                 when lead(h.date) over (partition by h.case_id order by h.date) is not null 
                    then lead(h.date) over (partition by h.case_id order by h.date)
                 when c.isactive then current_date
             end enddt 
      from nodis_caseactivehistory h 
      join nodis_case c on c.id=h.case_id) cah 
  on c.id=cah.case_id 
     and to_char(cah.strtdt,'yyyy')<=seq.index_enc_yr
     and to_char(cah.enddt,'yyyy')>=seq.index_enc_yr
WHERE c.condition = 'diabetes:type-2' and cah.status in ('I','R')
group by c.patient_id, seq.index_enc_yr;

-- Find all diab meds for index patients
DROP TABLE IF EXISTS cc_diab_rx;
CREATE TABLE cc_diab_rx AS
select patient_id, to_char(date,'yyyy') rx_yr
from hef_event 
WHERE name ~* 
'metformin|glyburide|test-strips|lancets|pramlintide|exenatide|sitagliptin|meglitinide|nateglinide|repaglinide|glimepiride|glipizide|gliclazide|rosiglitizone|pioglitazone|acetone|glucagon|miglitol|insulin'
group by patient_id, to_char(date,'yyyy');

--bring in last a1c for each year.
drop table if exists cc_diab_case_a1c;
create table cc_diab_case_a1c as
select distinct t0.patient_id, t0.index_enc_yr, result_float a1c
from cc_diab_case t0
left join (select distinct patient_id, to_char(a1c_dt,'yyyy') a1c_yr, result_float, 
                  row_number() over (partition by patient_id, to_char(a1c_dt,'yyyy') 
                                     order by a1c_dt desc) rn
           from cc_diab_a1c_dt)		   
          t1 on t1.patient_id=t0.patient_id and t1.a1c_yr=t0.index_enc_yr
where rn=1 or rn is null;

--treated cases
drop table if exists cc_diab_case_rx;
create table cc_diab_case_rx as
select t0.patient_id, t0.index_enc_yr case_rx_yr, t0.a1c
from cc_diab_case_a1c t0
join cc_diab_rx t1 on t1.patient_id = t0.patient_id 
  and to_number(nullif(t0.index_enc_yr,''),'9999')>=to_number(nullif(t1.rx_yr,''),'9999')
  and to_number(nullif(t0.index_enc_yr,''),'9999')<=to_number(nullif(t1.rx_yr,''),'9999')+1
group by t0.patient_id, t0.index_enc_yr, t0.a1c;
--untreated case
drop table if exists cc_diab_case_norx;
create table cc_diab_case_norx as
select t0.patient_id, t0.index_enc_yr case_norx_yr, t0.a1c
from cc_diab_case_a1c t0
left join cc_diab_case_rx t1 on t1.patient_id=t0.patient_id
  and t1.case_rx_yr=t0.index_enc_yr
where t1.patient_id is null;

-- Join it all together
DROP TABLE IF EXISTS cc_diab_by_pat;
CREATE TABLE cc_diab_by_pat as
SELECT t0.patient_id, t0.index_enc_yr
 , t0.sex, t0.race_ethnicity, t0.agegroup
 ,t1.patient_id as a1ctest
 ,t2.patient_id as a1ctest3
 ,t3.patient_id as a1ctestever
 ,t4.patient_id as diabetic
 ,t5.patient_id as norx
 ,t6.patient_id as norx7
 ,t7.patient_id as norx79
 ,t8.patient_id as norx9
 ,t9.patient_id as rx
 ,t10.patient_id as rx7
 ,t11.patient_id as rx79
 ,t12.patient_id as rx9
 ,t14.patient_id as all7
 ,t15.patient_id as all79
 ,t16.patient_id as all9
 ,t17.patient_id as norx_noa1c
 ,t18.patient_id as rx_noa1c
 ,t19.patient_id as all_noa1c 
FROM cc_pat_seq_enc_patinfo t0
left join cc_diab_a1c_yr t1 
  on t1.patient_id=t0.patient_id and t1.a1c_yr=t0.index_enc_yr
left join cc_diab_a1c_yr t2
  on t2.patient_id=t0.patient_id 
    and to_number(nullif(t0.index_enc_yr,''),'9999') >= to_number(nullif(t2.a1c_yr,''),'9999') 
    and to_number(nullif(t0.index_enc_yr,''),'9999') <= to_number(nullif(t2.a1c_yr,''),'9999') + 2
left join cc_diab_a1c_yr t3
  on t3.patient_id=t0.patient_id 
    and to_number(nullif(t0.index_enc_yr,''),'9999') >= to_number(nullif(t3.a1c_yr,''),'9999') 
left join cc_diab_case_a1c t4
  on t4.patient_id=t0.patient_id and t0.index_enc_yr=t4.index_enc_yr
left join cc_diab_case_norx t5 on t5.patient_id=t0.patient_id
  and t5.case_norx_yr=t0.index_enc_yr
left join cc_diab_case_norx t6 on t6.patient_id=t0.patient_id
  and t6.case_norx_yr=t0.index_enc_yr and t6.a1c<7
left join cc_diab_case_norx t7 on t7.patient_id=t0.patient_id
  and t7.case_norx_yr=t0.index_enc_yr and t7.a1c>=7 and t7.a1c<=9
left join cc_diab_case_norx t8 on t8.patient_id=t0.patient_id
  and t8.case_norx_yr=t0.index_enc_yr and t8.a1c>9
left join cc_diab_case_norx t17 on t17.patient_id=t0.patient_id
  and t17.case_norx_yr=t0.index_enc_yr and t17.a1c is null
left join cc_diab_case_rx t9 on t9.patient_id=t0.patient_id
  and t9.case_rx_yr=t0.index_enc_yr
left join cc_diab_case_rx t10 on t10.patient_id=t0.patient_id
  and t10.case_rx_yr=t0.index_enc_yr and t10.a1c<7
left join cc_diab_case_rx t11 on t11.patient_id=t0.patient_id
  and t11.case_rx_yr=t0.index_enc_yr and t11.a1c>=7 and t11.a1c<=9
left join cc_diab_case_rx t12 on t12.patient_id=t0.patient_id
  and t12.case_rx_yr=t0.index_enc_yr and t12.a1c>9
left join cc_diab_case_rx t18 on t18.patient_id=t0.patient_id
  and t18.case_rx_yr=t0.index_enc_yr and t18.a1c is null
left join cc_diab_case_a1c t14 on t14.patient_id=t0.patient_id
  and t14.index_enc_yr=t0.index_enc_yr and t14.a1c<7
left join cc_diab_case_a1c t15 on t15.patient_id=t0.patient_id
  and t15.index_enc_yr=t0.index_enc_yr and t15.a1c>=7 and t15.a1c<=9
left join cc_diab_case_a1c t16 on t16.patient_id=t0.patient_id
  and t16.index_enc_yr=t0.index_enc_yr and t16.a1c>9
left join cc_diab_case_a1c t19 on t19.patient_id=t0.patient_id
  and t19.index_enc_yr=t0.index_enc_yr and t19.a1c is null
group by t0.patient_id, t0.index_enc_yr
 , t0.sex, t0.race_ethnicity, t0.agegroup
 , t1.patient_id 
 , t2.patient_id
 , t3.patient_id
 , t4.patient_id
 , t5.patient_id
 , t6.patient_id
 , t7.patient_id
 , t8.patient_id
 , t9.patient_id
 , t10.patient_id
 , t11.patient_id
 , t12.patient_id
 , t14.patient_id
 , t15.patient_id
 , t16.patient_id
 , t17.patient_id
 , t18.patient_id
 , t19.patient_id
;

--Now get the crossing of all stratifiers
drop table if exists strat1;
create temporary table strat1 (id1 varchar(2), name1 varchar(15));
insert into strat1 values ('1','index_enc_yr'),('x','x');
drop table if exists strat2;
create temporary table strat2 (id2 varchar(2), name2 varchar(15));
insert into strat2 values ('2','agegroup'),('x','x');
drop table if exists strat4;
create temporary table strat4 (id4 varchar(2), name4 varchar(15));
insert into strat4 values ('4','sex'),('x','x');
drop table if exists strat5;
create temporary table strat5 (id5 varchar(2), name5 varchar(15));
insert into strat5 values ('5','race_ethnicity'),('x','x');
drop table if exists cc_stratifiers_crossed;
create table cc_stratifiers_crossed as
select * from strat1, strat2, strat4, strat5;

--create stub table to contain summary results
drop table if exists gen_pop_tools.cc_diab_summaries;
create table gen_pop_tools.cc_diab_summaries (
  index_enc_yr varchar(4),
  agegroup varchar(10), 
  sex varchar(10), 
  race_ethnicity varchar(25),
  total integer,
  a1ctest integer,
  a1ctest3 integer,
  a1ctestever integer,
  diabetic integer,
  norx integer,
  norx7 integer,
  norx79 integer,
  norx9 integer,
  norx_noa1c integer,
  rx integer,
  rx7 integer,
  rx79 integer,
  rx9 integer,
  rx_noa1c integer,
  all7 integer,
  all79 integer,
  all9 integer,
  all_noa1c integer);

 --Run the various groupings.  This would be much easier in Postgres 9.5 where the "grouping sets" query feature was added.
do
$$
  declare
    cursrow record;
    namerow record;
    nameset text;
    groupby text[];
    insrtsql text;
    partsql text;
    tmpsql text;
    ordsql text;
    i integer;
  begin
    for cursrow in execute 'select name1, name2, name4, name5 from cc_stratifiers_crossed'
    loop
      i:=0;
      groupby:=null;
      insrtsql:='insert into gen_pop_tools.cc_diab_summaries (index_enc_yr, agegroup, sex, race_ethnicity, 
        total, a1ctest, a1ctest3, a1ctestever, diabetic, norx, norx7, norx79, norx9, norx_noa1c, rx, rx7, rx79, rx9, rx_noa1c,
        all7, all79, all9, all_noa1c) 
        (select';
      if cursrow.name1='x' then 
        tmpsql:=' ''x''::varchar index_enc_yr,';
      else
        tmpsql:=' index_enc_yr,';
        i:=i+1;
        groupby[i]:='index_enc_yr';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name2='x' then 
        tmpsql:=' ''x''::varchar agegroup,';
      else
        tmpsql:=' agegroup,';
        i:=i+1;
        groupby[i]:='agegroup';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name4='x' then 
        tmpsql:=' ''x''::varchar sex,';
      else
        tmpsql:=' sex,';
        i:=i+1;
        groupby[i]:='sex';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name5='x' then 
        tmpsql:=' ''x''::varchar race_ethnicity,';
      else
        tmpsql:=' race_ethnicity,';
        i:=i+1;
        groupby[i]:='race_ethnicity';
      end if;
      insrtsql:=insrtsql||tmpsql;
      insrtsql:=insrtsql||'
         count(distinct patient_id) total,
         count(distinct a1ctest) a1ctest,
         count(distinct a1ctest3) a1ctest3,
         count(distinct a1ctestever) a1ctestever,
         count(distinct diabetic) diabetic,
         count(distinct norx) norx,
         count(distinct norx7) norx7,
         count(distinct norx79) norx79,
         count(distinct norx9) norx9,
         count(distinct norx_noa1c) norx_noa1c,
         count(distinct rx) rx,
         count(distinct rx7) rx7,
         count(distinct rx79) rx79,
         count(distinct rx9) rx9,
         count(distinct rx_noa1c) rx_noa1c,
         count(distinct all7) all7,
         count(distinct all79) all79,
         count(distinct all9) all9,
         count(distinct all_noa1c) all_noa1c
         from cc_diab_by_pat
         ';
      if i> 0 then 
        insrtsql:=insrtsql||'group by ';
        i:=1;
        while i <= array_length(groupby, 1)
        loop
          if i >1 then 
            insrtsql:=insrtsql||', '; 
          end if;
          insrtsql:=insrtsql||groupby[i];
          i:=i+1;
        end loop;
      end if;
      insrtsql:=insrtsql||')';
      execute insrtsql;
      --raise notice '%', insrtsql;
    end loop;
  end;      
$$ 
language plpgsql;

--Gather up the values of the stratifiers and assign code.
drop table if exists gen_pop_tools.cc_agegroup_codevals;
Create table gen_pop_tools.cc_agegroup_codevals (agegroup varchar(7), codeval varchar(1));
insert into gen_pop_tools.cc_agegroup_codevals (agegroup, codeval) 
values ('0-19','1'),('20-39','2'),('40-59','3'),('Over 59','4'),('UNKNOWN','5');

drop table if exists gen_pop_tools.cc_sex_codevals;
Create table gen_pop_tools.cc_sex_codevals (sex varchar(7), codeval varchar(1));
insert into gen_pop_tools.cc_sex_codevals (sex, codeval) 
values ('Female','1'),('Male','2'),('Unknown','3');

drop table if exists gen_pop_tools.cc_race_ethnicity_codevals;
Create table gen_pop_tools.cc_race_ethnicity_codevals (race_ethnicity varchar(10), codeval varchar(1));
insert into gen_pop_tools.cc_race_ethnicity_codevals (race_ethnicity, codeval) 
values ('ASIAN','1'),('BLACK','2'),('CAUCASIAN','3'),('HISPANIC','4'),('OTHER','5'),('UNKNOWN','6');

--now write out to JSON
copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
    '["year","age_group","sex","race_ethnicity"]'::json filters,
    '{"a":{"name":"Individuals with at least one encounter in the chosen year(s)","description":"Total population observed","nest":"0","pcalc":"a"},"b":{"name":"≥1 hemoglobin A1C test in the index year","description":"Count of individuals with ≥1 hemoglobin A1C test in the index year","nest":"1","pcalc":"a"},"c":{"name":"≥1 hemoglobin A1C test within the index or 2 preceding years","description":"Count of individuals with ≥1 hemoglobin A1C test within the index or 2 prior years","nest":"1","pcalc":"a"},"d":{"name":"≥1 hemoglobin A1C test ever","description":"Count of individuals with ≥1 hemoglobin A1C test ever","nest":"1","pcalc":"a"},"e":{"name":"Count of individuals with diabetes","description":"Count of patients with diabetes","nest":"1","pcalc":"a"},"f":{"name":"Last hemoglobin A1C <7.0","description":"Count of diabetes patients with hemoglobin A1C <7.0 in the preceding 2 years","nest":"2","pcalc":"e"},"g":{"name":"Last hemoglobin A1C 7-9","description":"Count of diabetes patients with hemoglobin A1C 7-9 in the preceding 2 years","nest":"2","pcalc":"e"},"h":{"name":"Last hemoglobin A1C >9","description":"Count of diabetes patients with hemoglobin A1C >9 in the preceding 2 years","nest":"2","pcalc":"e"},"i":{"name":"Hemoglobin A1C not checked","description":"Count of patients in whom hemoglobin A1C not checked in preceding 2 years","nest":"2","pcalc":"e"},"j":{"name":"Count of individuals with diabetes NOT on diabetes meds ","description":"Count of diabetes patients not receiving diabetes meds in the preceding 2 years","nest":"2","pcalc":"e"},"k":{"name":"Last hemoglobin A1C <7.0","description":"Count of diabetes patients not receiving diabetes meds whose most recent hemoglobin A1C <7.0 in the preceding 2 years","nest":"3","pcalc":"j"},"l":{"name":"Last hemoglobin A1C 7-9","description":"Count of diabetes patients not receiving diabetes meds whose most recent hemoglobin A1C 7-9 in the  preceding 2 years","nest":"3","pcalc":"j"},"m":{"name":"Last hemoglobin A1C >9","description":"Count of diabetes patients not receiving diabetes meds whose most recent hemoglobin A1C >9 in the preceding 2 years","nest":"3","pcalc":"j"},"n":{"name":"Hemoglobin A1C not checked","description":"Count of patients not receiving diabetes meds in whom hemoglobin A1C was not checked in the preceding 2 years","nest":"3","pcalc":"j"},"o":{"name":"Count of individuals with diabetes on meds","description":"Count of diabetes patients prescribed diabetes meds in preceding 2 years","nest":"2","pcalc":"e"},"p":{"name":"Last hemoglobin A1C <7.0","description":"Count of diabetes patients receiving diabetes meds whose most recent hemoglobin A1C <7.0 in the preceding 2 years","nest":"3","pcalc":"o"},"q":{"name":"Last hemoglobin A1C 7-9","description":"Count of diabetes patients prescribed diabetes meds whose most recent hemoglobin A1C 7-9 in preceding 2 years","nest":"3","pcalc":"o"},"r":{"name":"Last hemoglobin A1C >9","description":"Count of diabetes patients prescribed diabetes meds whose most recent hemoglobin A1C >9 in the preceding 2 years","nest":"3","pcalc":"o"},"s":{"name":"Hemoglobin A1C not checked","description":"Count of patients prescribed diabetes meds whose hemoglobin A1C not checked in preceding 2 years","nest":"3","pcalc":"o"}}'::json rowmeta,
(select '{'||array_to_string(array_agg(row_),',')||'}' from  (select ('"'||
        case when index_enc_yr = 'x' then 'YEAR' else index_enc_yr end ||
          case when agegroup <> 'x' 
            then (select codeval from gen_pop_tools.cc_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end ||
          case when sex <> 'x' 
            then (select codeval from gen_pop_tools.cc_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end ||
          case when race_ethnicity <> 'x' 
            then (select codeval from gen_pop_tools.cc_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end || '":{"a":'|| total
		  || ',"b":' || a1ctest
		  || ',"c":' || a1ctest3
		  || ',"d":' || a1ctestever
		  || ',"e":' || diabetic
		  || ',"f":' || all7
		  || ',"g":' || all79
		  || ',"h":' || all9
		  || ',"i":' || all_noa1c
		  || ',"j":' || norx
		  || ',"k":' || norx7
		  || ',"l":' || norx79 
		  || ',"m":' || norx9
		  || ',"n":' || norx_noa1c		  
		  || ',"o":' || rx 
		  || ',"p":' || rx7 
		  || ',"q":' || rx79 
		  || ',"r":' || rx9 
		  || ',"s":' || rx_noa1c 
		  || '}') row_
from gen_pop_tools.cc_diab_summaries t0 ) t00)::json rowdata) t1) to :'pathToFile';




  

