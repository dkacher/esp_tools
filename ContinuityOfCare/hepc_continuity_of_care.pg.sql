﻿/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                       Continuity of Care weekly Tables
--
--------------------------------------------------------------------------------
--
-- @author: Karen Eberhardt <keberhardt@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2017 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some PostgreSQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/
set search_path to gen_pop_tools, public;

--This table is used to map local race values to standard set.
drop table if exists cc_racemap;
create table cc_racemap
  (source_field  varchar(65),
   source_value  varchar(100),
   target_value  varchar(100),
   primary key (source_field, source_value)); --primary key enforces unique mapping
--add any local race values to be mapped to the set here.
insert into cc_racemap
(source_field, source_value, target_value) values
('ethnicity','Y','HISPANIC'),
('ethnicity','HISPANIC/LATINO','HISPANIC'),
('race','American Indian / Alaska Native','OTHER'),
('race','American Indian/Alaska Native','OTHER'),
('race','Asian','ASIAN'),
('race','Black / African American','BLACK'),
('race','Black/African American','BLACK'),
('race','More than One Race','OTHER'),
('race','Native Hawaiian','OTHER'),
('race','Other Pacific Islander','OTHER'),
('race','Pacific Islander','OTHER'),
('race','White','CAUCASIAN'),
('race','NATIVE HAWAI','OTHER'),
('race','ALASKAN','OTHER'),
('race','ASIAN','ASIAN'),
('race','CAUCASIAN','CAUCASIAN'),
('race','INDIAN','ASIAN'),
('race','NAT AMERICAN','OTHER'),
('race','HISPANIC','HISPANIC'),
('race','OTHER','OTHER'),
('race','PACIFIC ISLANDER/HAWAIIAN','OTHER'),
('race','AMERICAN INDIAN/ALASKAN NATIVE','OTHER'),
('race','WHITE','CAUCASIAN'),
('race','BLACK','BLACK');

-- Using tt_pat_seq_enc, so this code must be run after tt stuff
-- This table has one row per year (from 2010) per patient (including current year)
--  where patient has had at least one encounter for that year.
--  This is the "Total Population Observed"
drop table if exists cc_pat_seq_enc;
create table cc_pat_seq_enc AS
select patient_id, 
  substr(year_month,1,4) index_enc_yr 
from tt_pat_seq_enc
where (substr(year_month,6,2)='12' 
        or year_month=(select max(year_month) from tt_pat_seq_enc)) 
      and prior1>=1 and substr(year_month,1,4)>='2010';

-- This table joins in patient demographic data	  
DROP TABLE IF EXISTS cc_pat_seq_enc_patinfo;
CREATE TABLE cc_pat_seq_enc_patinfo AS
SELECT ip.patient_id, ip.index_enc_yr, 
case when upper(substr(p.gender,1,1)) = 'F' then 'Female'
	 when upper(substr(p.gender,1,1)) = 'M' then 'Male'
	 else 'Unknown'
end sex,
case 
     when (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity) is not null
	   then (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity)
     when (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race) is not null
	   then (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race)
     else 'UNKNOWN'
  end race_ethnicity
, case
    when date_part('year', date_of_birth)<1945 then 'Born prior to 1945'
	when date_part('year', date_of_birth)>=1945 and date_part('year', date_of_birth)<= 1965 then 'Born 1945 to 1965'
	when date_part('year', date_of_birth)>1965 then 'Born after 1965'
    else 'Unknown'
  end as birth_cohort
,  case 
    when date_part('year', age(current_date, date_of_birth))<=19 then '0-19'
    when date_part('year', age(current_date, date_of_birth))>=20 and date_part('year', age(current_date, date_of_birth)) <=39 then '20-39'
    when date_part('year', age(current_date, date_of_birth))>=40 and date_part('year', age(current_date, date_of_birth)) <=59 then '40-59'
    when date_part('year', age(current_date, date_of_birth))>=60 then 'Over 59'
    else 'UNKNOWN'
  end as agegroup
FROM cc_pat_seq_enc ip
JOIN emr_patient p ON ip.patient_id = p.id;

-- Find all patients who ever had a mapped hep_c lab test
DROP TABLE IF EXISTS cc_hepc_labs;
CREATE TABLE cc_hepc_labs AS
SELECT patient_id, name, date
FROM hef_event 
WHERE name like 'lx:hepatitis_c%'
  and date>='2010-01-01'::date;
-- Find the date of the first hep_c lab result (any test type)
DROP TABLE IF EXISTS cc_hepc_firsttest_dates;
CREATE TABLE cc_hepc_firsttest_dates AS
SELECT patient_id, to_char(min(date),'yyyy') firsttest_yr
FROM cc_hepc_labs
GROUP BY patient_id;

-- Find all patients who ever had a positive hep_c lab result
DROP TABLE IF EXISTS cc_hepc_pos_labs;
CREATE TABLE cc_hepc_pos_labs AS
SELECT patient_id, name, date 
FROM cc_hepc_labs 
WHERE name in (
'lx:hepatitis_c_elisa:positive',
'lx:hepatitis_c_riba:positive',
'lx:hepatitis_c_rna:positive',
'lx:hepatitis_c_signal_cutoff:positive');
-- Find the date of the first pos hep_c lab result (any test type)
DROP TABLE IF EXISTS cc_hepc_index_dates;
CREATE TABLE cc_hepc_index_dates AS
SELECT patient_id, to_char(min(date),'yyyy') index_enc_yr, min(date) index_pos_date
FROM cc_hepc_pos_labs
GROUP BY patient_id;

 -- Get RNA lab details for the index pos patients
DROP TABLE IF EXISTS cc_hepc_rna_lab;
CREATE TABLE cc_hepc_rna_lab AS
SELECT l.patient_id, l.native_name, l.date, l.result_float
FROM emr_labresult l
join conf_labtestmap m on l.native_code = m.native_code 
join cc_hepc_index_dates ip on ip.patient_id=l.patient_id
WHERE l.date >= ip.index_pos_date
AND m.test_name = 'hepatitis_c_rna';
-- now get a patient-year table
DROP TABLE IF EXISTS cc_hepc_rna_lab_yr;
CREATE TABLE cc_hepc_rna_lab_yr AS
SELECT patient_id, to_char(date,'yyyy') rna_yr
from cc_hepc_rna_lab
group by patient_id, to_char(date,'yyyy');

-- Get pos RNA (detectable) years
DROP TABLE IF EXISTS cc_hepc_rna_pos;
CREATE TABLE cc_hepc_rna_pos as
SELECT patient_id, to_char(date,'yyyy') rna_pos_yr
from cc_hepc_pos_labs
where name='lx:hepatitis_c_rna:positive'
group by patient_id, to_char(date,'yyyy');

-- Get most recent RNA test
DROP TABLE IF EXISTS cc_hepc_most_recent_rna;
CREATE TABLE cc_hepc_most_recent_rna AS
SELECT patient_id, max(date) last_rna_date
FROM cc_hepc_rna_lab
GROUP BY patient_id;
-- Get patients whose last RNA test was pos
DROP TABLE IF EXISTS cc_hepc_most_recent_rna_pos;
CREATE TABLE cc_hepc_most_recent_rna_pos AS
SELECT mr.patient_id, mr.last_rna_date, to_char(mr.last_rna_date,'yyyy') last_rna_pos_yr
from cc_hepc_most_recent_rna mr
join cc_hepc_pos_labs pl on pl.date=mr.last_rna_date and pl.patient_id=mr.patient_id and pl.name='lx:hepatitis_c_rna:positive'
group by mr.patient_id, mr.last_rna_date, to_char(mr.last_rna_date,'yyyy'); --in case there are multiple pos labs on last rna date.
DROP TABLE IF EXISTS cc_hepc_most_recent_rna_neg;
CREATE TABLE cc_hepc_most_recent_rna_neg AS
SELECT mr.patient_id, mr.last_rna_date, to_char(mr.last_rna_date,'yyyy') last_rna_neg_yr
from cc_hepc_most_recent_rna mr
EXCEPT select * from cc_hepc_most_recent_rna_pos;

 -- Find patients with an ESP case for acute hep_c
DROP TABLE IF EXISTS cc_hepc_esp_acute;
CREATE TABLE cc_hepc_esp_acute AS
SELECT patient_id, date as hepc_acute_date, to_char(date,'yyyy') hepc_acute_yr
FROM nodis_case c
WHERE condition = 'hepatitis_c' and exists (select null from nodis_caseactivehistory cac where c.id=cac.case_id and cac.status='HEP_C-A');

-- Find all hepc meds for index patients
DROP TABLE IF EXISTS cc_hepc_all_rx;
CREATE TABLE cc_hepc_all_rx AS
select ip.patient_id, rx.name, rx.date, start_date, end_date, refills 
from emr_prescription rx
join cc_hepc_index_dates ip on rx.patient_id = ip.patient_id
WHERE rx.name ~* 
'interferon alfa-2b|pegylated interferon alfa|sofosbuvir|ribavirin|boceprevir
|telaprevir|simeprevir|ledipasvir.*sofosbuvir|ombitasvir.*paritaprevir.*ritonavir.*dasabuvir
|ombitasvir.*paritaprevir.*ritonavir|daclatasvir|elbasvir.*grazoprevir|sofosbuvir.*velpatasvir
|Pegintron|Pegasys|Sovaldi|Copegus|Rebetol|Virazole|Rebetron|Victrelis
|Incivek|Olysio|Harvoni|Viekira Pak|Technivie|Daklinza|Zepatier|Epclusa
|OMBITAS.*PARITAPRE.*RITONA.*DASAB|OMBITA.*PARITAP.*RITON.*DASABUVIR|OMBIT.*PARITAP.*RITONAV
|PEG-INTRON|INTRON|RIBASPHERE|PEGINTERFERON ALFA-2A|INTERFERON ALPHA 2-B'
  and coalesce(rx.date, rx.start_date)>='2010-01-01'::date;

-- Filter For HepC Rx with a Date or Start Date Following the Index Positive Date
-- Compute variations on the end_date
DROP TABLE IF EXISTS cc_hepc_rx_after_ip;
CREATE TABLE cc_hepc_rx_after_ip AS
select ip.patient_id, ip.index_pos_date, rx.name, rx.date, rx.start_date, rx.end_date, rx.refills,
start_date + (30 * (coalesce(refills::integer +1, 1::integer)))computed_end_date_start_30_refill,
start_date + 90 computed_end_date_start_plus90
FROM cc_hepc_index_dates ip, cc_hepc_all_rx rx
WHERE ip.patient_id = rx.patient_id
AND (rx.date >= ip.index_pos_date or rx.start_date >= ip.index_pos_date);

-- RX first start and last end
DROP TABLE IF EXISTS cc_hepc_rx_after_ip_start_end;
CREATE TABLE cc_hepc_rx_after_ip_start_end AS
SELECT patient_id, min(coalesce(start_date, date)) min_start, 
  max(GREATEST (end_date, computed_end_date_start_30_refill, computed_end_date_start_plus90)) max_end
FROM cc_hepc_rx_after_ip
GROUP BY patient_id;

-- Acute Resolved no treatment
DROP TABLE IF EXISTS cc_hepc_acute_res_norx;
CREATE TABLE cc_hepc_acute_res_norx AS
select a.patient_id, a.hepc_acute_yr
from cc_hepc_esp_acute a
join cc_hepc_most_recent_rna_neg rn on rn.patient_id=a.patient_id -- resolved
left join cc_hepc_rx_after_ip rx on rx.patient_id=a.patient_id
where rx.patient_id is null -- no rx
group by a.patient_id, a.hepc_acute_yr;

-- Acute persistent
DROP TABLE IF EXISTS cc_hepc_acute_persist;
CREATE TABLE cc_hepc_acute_persist AS
select a.patient_id, a.hepc_acute_yr
from cc_hepc_esp_acute a
left join cc_hepc_most_recent_rna_pos pl on pl.patient_id=a.patient_id 
  and a.hepc_acute_date<=pl.last_rna_date-interval '1 year'
left join cc_hepc_rx_after_ip rx on rx.patient_id=a.patient_id 
  and a.hepc_acute_date<=coalesce(rx.date,rx.start_date)-interval '1 year'
where rx.patient_id is not null or pl.patient_id is not null -- either rx or pl after 1 year
group by a.patient_id, a.hepc_acute_yr;

-- Acute Unknowns
DROP TABLE IF EXISTS cc_hepc_acute_unknown;
CREATE TABLE cc_hepc_acute_unknown AS
SELECT a.patient_id, a.hepc_acute_yr
from cc_hepc_esp_acute a
left join cc_hepc_acute_persist p on p.patient_id=a.patient_id and p.hepc_acute_yr=a.hepc_acute_yr
left join cc_hepc_acute_res_norx rx on rx.patient_id=a.patient_id and rx.hepc_acute_yr=a.hepc_acute_yr
where p.patient_id is null and rx.patient_id is null;

-- HepC Chronic
DROP TABLE IF EXISTS cc_hepc_chronic;
CREATE TABLE cc_hepc_chronic AS
SELECT ip.patient_id, to_char(ip.index_pos_date,'yyyy') index_pos_yr
FROM cc_hepc_index_dates ip
left join cc_hepc_esp_acute a on a.patient_id=ip.patient_id
left join cc_hepc_acute_persist ap on ap.patient_id=ip.patient_id
left join cc_hepc_acute_unknown au on au.patient_id=ip.patient_id
where a.patient_id is null or ap.patient_id is not null or au.patient_id is not null;

-- Chronic, no rx
DROP TABLE IF EXISTS cc_hepc_chronic_norx;
CREATE TABLE cc_hepc_chronic_norx AS
select ch.patient_id, ch.index_pos_yr
from cc_hepc_chronic ch
left join cc_hepc_rx_after_ip rx on ch.patient_id=rx.patient_id
where rx.patient_id is null;

-- Chronic, with rx
DROP TABLE IF EXISTS cc_hepc_chronic_rx;
CREATE TABLE cc_hepc_chronic_rx AS
select ch.patient_id, ch.index_pos_yr
from cc_hepc_chronic ch
left join cc_hepc_chronic_norx norx on ch.patient_id=norx.patient_id
where norx.patient_id is null;

-- Chronic, no rx, resolved
DROP TABLE IF EXISTS cc_hepc_chronic_norx_res;
CREATE TABLE cc_hepc_chronic_norx_res AS
SELECT ch.patient_id, ch.index_pos_yr
FROM cc_hepc_chronic_norx ch
join cc_hepc_most_recent_rna_neg nrna on nrna.patient_id=ch.patient_id;

-- Chronic, no rx, persistent
DROP TABLE IF EXISTS cc_hepc_chronic_norx_persist;
CREATE TABLE cc_hepc_chronic_norx_persist AS
SELECT ch.patient_id, ch.index_pos_yr
FROM cc_hepc_chronic_norx ch
join cc_hepc_most_recent_rna_pos prna on prna.patient_id=ch.patient_id;

-- Chronic, no rx, unknown
DROP TABLE IF EXISTS cc_hepc_chronic_norx_unknown;
CREATE TABLE cc_hepc_chronic_norx_unknown AS
SELECT ch.patient_id, ch.index_pos_yr
FROM cc_hepc_chronic_norx ch
left join cc_hepc_most_recent_rna_pos prna on prna.patient_id=ch.patient_id
left join cc_hepc_most_recent_rna_neg nrna on nrna.patient_id=ch.patient_id
where prna.patient_id is null and nrna.patient_id is null;

-- Chronic, rx, no viral load test
DROP TABLE IF EXISTS cc_hepc_chronic_rx_nolb;
CREATE TABLE cc_hepc_chronic_rx_nolb as
SELECT ch.patient_id, ch.index_pos_yr
from cc_hepc_chronic_rx ch
join cc_hepc_rx_after_ip_start_end rx on rx.patient_id=ch.patient_id
left join cc_hepc_rna_lab lb on lb.patient_id=rx.patient_id and lb.date>rx.min_start 
where lb.patient_id is null;

-- Chronic, rx, subsequent viral load test
DROP TABLE IF EXISTS cc_hepc_chronic_rx_lb;
CREATE TABLE cc_hepc_chronic_rx_lb as
SELECT ch.patient_id, ch.index_pos_yr
from cc_hepc_chronic_rx ch
left join cc_hepc_chronic_rx_nolb rx on rx.patient_id=ch.patient_id
where rx.patient_id is null;

-- Chronic, rx, last rna negative
DROP TABLE IF EXISTS cc_hepc_chronic_rx_last_neg;
CREATE TABLE cc_hepc_chronic_rx_last_neg as
SELECT ch.patient_id, ch.index_pos_yr
from cc_hepc_chronic_rx_lb ch
join cc_hepc_most_recent_rna_neg rna on rna.patient_id=ch.patient_id;

-- Chronic, rx, negative sustained
DROP TABLE IF EXISTS cc_hepc_chronic_rx_neg_sustained;
CREATE TABLE cc_hepc_chronic_rx_neg_sustained as
SELECT ch.patient_id, ch.index_pos_yr
from cc_hepc_chronic_rx_lb ch
join cc_hepc_most_recent_rna_neg rna on rna.patient_id=ch.patient_id
join cc_hepc_rx_after_ip_start_end rx on rx.patient_id=rna.patient_id 
   and rx.max_end + interval '24 weeks' <= rna.last_rna_date;

-- Join it all together
DROP TABLE IF EXISTS cc_hepc_by_pat;
CREATE TABLE cc_hepc_by_pat as
SELECT t0.patient_id, t0.index_enc_yr
 , t0.sex, t0.race_ethnicity, t0.birth_cohort, t0.agegroup
 , t1.patient_id tested0
 , t2.patient_id indexpos0
 , t3.patient_id vltest0
 , t4.patient_id vload0
 , t5.patient_id lastvlpos0
 , t6.patient_id lastvlneg0
 , t7.patient_id tested1
 , t8.patient_id indexpos1
 , t9.patient_id vltest1
 , t10.patient_id vload1
 , t11.patient_id lastvlpos1
 , t12.patient_id lastvlneg1
 , t13.patient_id acute
 , t14.patient_id acute_res_norx
 , t15.patient_id acute_res_prst
 , t16.patient_id acute_res_unkn
 , t17.patient_id chronic
 , t18.patient_id chron_notrt
 , t19.patient_id chron_notrt_res
 , t20.patient_id chron_notrt_prst
 , t21.patient_id chron_notrt_unkn
 , t22.patient_id chron_trt
 , t23.patient_id chron_trt_unkn
 , t24.patient_id chron_trt_vload
 , t25.patient_id chron_trt_lastvlneg
 , t26.patient_id chron_trt24_lastvlneg
FROM cc_pat_seq_enc_patinfo t0
left join cc_hepc_firsttest_dates t1 
  on t0.patient_id=t1.patient_id 
     and t0.index_enc_yr>=t1.firsttest_yr
left join cc_hepc_index_dates t2
  on t0.patient_id=t2.patient_id 
     and t0.index_enc_yr>=to_char(t2.index_pos_date,'yyyy')
left join cc_hepc_rna_lab_yr t3
  on t2.patient_id=t3.patient_id 
left join cc_hepc_rna_pos t4 
  on t3.patient_id=t4.patient_id 
left join cc_hepc_most_recent_rna_pos t5 
  on t3.patient_id=t5.patient_id 
left join cc_hepc_most_recent_rna_neg t6 
  on t3.patient_id=t6.patient_id 
left join cc_hepc_firsttest_dates t7 
  on t0.patient_id=t7.patient_id 
     and t0.index_enc_yr=t7.firsttest_yr
left join cc_hepc_index_dates t8
  on t0.patient_id=t8.patient_id 
     and t0.index_enc_yr=to_char(t8.index_pos_date,'yyyy')
left join cc_hepc_rna_lab_yr t9
  on t8.patient_id=t9.patient_id 
left join cc_hepc_rna_pos t10 
  on t9.patient_id=t10.patient_id 
left join cc_hepc_most_recent_rna_pos t11 
  on t9.patient_id=t11.patient_id 
left join cc_hepc_most_recent_rna_neg t12
  on t9.patient_id=t12.patient_id 
left join cc_hepc_esp_acute t13
  on t0.patient_id=t13.patient_id
      and t0.index_enc_yr=t13.hepc_acute_yr
left join cc_hepc_acute_res_norx t14
  on t13.patient_id=t14.patient_id
left join cc_hepc_acute_persist t15
  on t13.patient_id=t15.patient_id
left join cc_hepc_acute_unknown t16
  on t13.patient_id=t16.patient_id
left join cc_hepc_chronic t17
  on t0.patient_id=t17.patient_id
     and t0.index_enc_yr=t17.index_pos_yr
left join cc_hepc_chronic_norx t18
  on t17.patient_id=t18.patient_id
left join cc_hepc_chronic_norx_res t19
  on t18.patient_id=t19.patient_id
left join cc_hepc_chronic_norx_persist t20
  on t18.patient_id=t20.patient_id
left join cc_hepc_chronic_norx_unknown t21
  on t18.patient_id=t21.patient_id
left join cc_hepc_chronic_rx t22
  on t17.patient_id=t22.patient_id
left join cc_hepc_chronic_rx_nolb t23
  on t22.patient_id=t23.patient_id
left join cc_hepc_chronic_rx_lb t24
  on t22.patient_id=t24.patient_id
left join cc_hepc_chronic_rx_last_neg t25
  on t24.patient_id=t25.patient_id
left join cc_hepc_chronic_rx_neg_sustained t26
  on t24.patient_id=t26.patient_id
group by t0.patient_id, t0.index_enc_yr
 , t0.sex, t0.race_ethnicity, t0.birth_cohort, t0.agegroup
 , t1.patient_id 
 , t2.patient_id 
 , t3.patient_id 
 , t4.patient_id 
 , t5.patient_id 
 , t6.patient_id 
 , t7.patient_id 
 , t8.patient_id 
 , t9.patient_id 
 , t10.patient_id
 , t11.patient_id
 , t12.patient_id
 , t13.patient_id 
 , t14.patient_id
 , t15.patient_id
 , t16.patient_id
 , t17.patient_id 
 , t18.patient_id  
 , t19.patient_id  
 , t20.patient_id  
 , t21.patient_id  
 , t22.patient_id  
 , t23.patient_id  
 , t24.patient_id  
 , t25.patient_id  
 , t26.patient_id  
;

--Now get the crossing of all stratifiers
drop table if exists strat1;
create temporary table strat1 (id1 varchar(2), name1 varchar(15));
insert into strat1 values ('1','index_enc_yr'),('x','x');
drop table if exists strat2;
create temporary table strat2 (id2 varchar(2), name2 varchar(15));
insert into strat2 values ('2','agegroup'),('x','x');
drop table if exists strat3;
create temporary table strat3 (id3 varchar(2), name3 varchar(15));
insert into strat3 values ('3','birth_cohort'),('x','x');
drop table if exists strat4;
create temporary table strat4 (id4 varchar(2), name4 varchar(15));
insert into strat4 values ('4','sex'),('x','x');
drop table if exists strat5;
create temporary table strat5 (id5 varchar(2), name5 varchar(15));
insert into strat5 values ('5','race_ethnicity'),('x','x');
drop table if exists cc_stratifiers_crossed;
create table cc_stratifiers_crossed as
select * from strat1, strat2, strat3, strat4, strat5;

--create stub table to contain summary results
drop table if exists gen_pop_tools.cc_hepc_summaries;
create table gen_pop_tools.cc_hepc_summaries (
  index_enc_yr varchar(4),
  agegroup varchar(10), 
  birth_cohort varchar(25), 
  sex varchar(10), 
  race_ethnicity varchar(25),
  total integer,
  tested0 integer,
  indexpos0 integer,
  vltest0 integer,
  vload0 integer,
  lastvlpos0 integer,
  lastvlneg0 integer,
  tested1 integer,
  indexpos1 integer,
  vltest1 integer,
  vload1 integer,
  lastvlpos1 integer,
  lastvlneg1 integer,
  acute integer,
  acute_res_norx integer,
  acute_res_prst integer,
  acute_res_unkn integer,
  chronic integer,
  chron_notrt integer,
  chron_notrt_res integer,
  chron_notrt_prst integer,
  chron_notrt_unkn integer,
  chron_trt integer,
  chron_trt_unkn integer,
  chron_trt_vload integer,
  chron_trt_lastvlneg integer,
  chron_trt24_lastvlneg integer);

 --Run the various groupings.  This would be much easier in Postgres 9.5 where the "grouping sets" query feature was added.
do
$$
  declare
    cursrow record;
    namerow record;
    nameset text;
    groupby text[];
    insrtsql text;
    updtsql0 text;
    updtsql1 text;
    updtsql2 text;
    updtsql3 text;
	updtsql4 text;
    partsql text;
    tmpsql text;
    ordsql text;
    i integer;
  begin
    for cursrow in execute 'select name1, name2, name3, name4, name5 from cc_stratifiers_crossed'
    loop
      i:=0;
      groupby:=null;
      insrtsql:='insert into gen_pop_tools.cc_hepc_summaries (index_enc_yr, agegroup, birth_cohort, sex, race_ethnicity, 
        total, tested0, indexpos0, vltest0, vload0, lastvlpos0, lastvlneg0,
        tested1, indexpos1, vltest1, vload1, lastvlpos1, lastvlneg1,
        acute, acute_res_norx, acute_res_prst, acute_res_unkn, 
        chronic, chron_notrt, chron_notrt_res, chron_notrt_prst, chron_notrt_unkn, 
        chron_trt, chron_trt_unkn, chron_trt_vload, chron_trt_lastvlneg, chron_trt24_lastvlneg) 
        (select';
      if cursrow.name1='x' then 
        tmpsql:=' ''x''::varchar index_enc_yr,';
      else
        tmpsql:=' index_enc_yr,';
        i:=i+1;
        groupby[i]:='index_enc_yr';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name2='x' then 
        tmpsql:=' ''x''::varchar agegroup,';
      else
        tmpsql:=' agegroup,';
        i:=i+1;
        groupby[i]:='agegroup';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name3='x' then 
        tmpsql:=' ''x''::varchar birth_cohort,';
      else
        tmpsql:=' birth_cohort,';
        i:=i+1;
        groupby[i]:='birth_cohort';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name4='x' then 
        tmpsql:=' ''x''::varchar sex,';
      else
        tmpsql:=' sex,';
        i:=i+1;
        groupby[i]:='sex';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name5='x' then 
        tmpsql:=' ''x''::varchar race_ethnicity,';
      else
        tmpsql:=' race_ethnicity,';
        i:=i+1;
        groupby[i]:='race_ethnicity';
      end if;
      insrtsql:=insrtsql||tmpsql;
      insrtsql:=insrtsql||'
         count(distinct patient_id) total,
         count(distinct tested0) tested0,
         count(distinct indexpos0) indexpos0,
         count(distinct vltest0) vltest0,
         count(distinct vload0) vload0,
         count(distinct lastvlpos0) lastvlpos0,
         count(distinct lastvlneg0) lastvlneg0,
         count(distinct tested1) tested1,
         count(distinct indexpos1) indexpos1,
         count(distinct vltest1) vltest1,
         count(distinct vload1) vload1,
         count(distinct lastvlpos1) lastvlpos1,
         count(distinct lastvlneg1) lastvlneg1,
         count(distinct acute) acute,
         count(distinct acute_res_norx) acute_res_norx,
         count(distinct acute_res_prst) acute_res_prst,
         count(distinct acute_res_unkn) acute_res_unkn,
         count(distinct chronic) chronic,
         count(distinct chron_notrt) chron_notrt,
         count(distinct chron_notrt_res) chron_notrt_res,
         count(distinct chron_notrt_prst) chron_notrt_prst,
         count(distinct chron_notrt_unkn) chron_notrt_unkn,
         count(distinct chron_trt) chron_trt,
         count(distinct chron_trt_unkn) chron_trt_unkn,
         count(distinct chron_trt_vload) chron_trt_vload,
         count(distinct chron_trt_lastvlneg) chron_trt_lastvlneg,
         count(distinct chron_trt24_lastvlneg) chron_trt24_lastvlneg
         from cc_hepc_by_pat
         ';
      if i> 0 then 
        insrtsql:=insrtsql||'group by ';
        i:=1;
        while i <= array_length(groupby, 1)
        loop
          if i >1 then 
            insrtsql:=insrtsql||', '; 
          end if;
          insrtsql:=insrtsql||groupby[i];
          i:=i+1;
        end loop;
      end if;
      insrtsql:=insrtsql||')';
      execute insrtsql;
      --raise notice '%', insrtsql;
    end loop;
  end;      
$$ 
language plpgsql;

--Gather up the values of the stratifiers and assign code.
drop table if exists gen_pop_tools.cc_agegroup_codevals;
Create table gen_pop_tools.cc_agegroup_codevals (agegroup varchar(7), codeval varchar(1));
insert into gen_pop_tools.cc_agegroup_codevals (agegroup, codeval) 
values ('0-19','1'),('20-39','2'),('40-59','3'),('Over 59','4'),('UNKNOWN','5');

drop table if exists gen_pop_tools.cc_birth_cohort_codevals;
Create table gen_pop_tools.cc_birth_cohort_codevals (birth_cohort varchar(18), codeval varchar(1));
insert into gen_pop_tools.cc_birth_cohort_codevals (birth_cohort, codeval) 
values ('Born prior to 1945','1'),('Born 1945 to 1965','2'),('Born after 1965','3'),('Unknown','4');

drop table if exists gen_pop_tools.cc_sex_codevals;
Create table gen_pop_tools.cc_sex_codevals (sex varchar(7), codeval varchar(1));
insert into gen_pop_tools.cc_sex_codevals (sex, codeval) 
values ('Female','1'),('Male','2'),('Unknown','3');

drop table if exists gen_pop_tools.cc_race_ethnicity_codevals;
Create table gen_pop_tools.cc_race_ethnicity_codevals (race_ethnicity varchar(10), codeval varchar(1));
insert into gen_pop_tools.cc_race_ethnicity_codevals (race_ethnicity, codeval) 
values ('ASIAN','1'),('BLACK','2'),('CAUCASIAN','3'),('HISPANIC','4'),('OTHER','5'),('UNKNOWN','6');

--now write out to JSON
copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
    '["year","age_group","birth_cohort","sex","race_ethnicity"]'::json filters,
    '{"a":{"name":"Total Population Under Surveillance","description":"Total population observed","nest":"0","pcalc":"a"},"b":{"name":"Patients tested for hepatitis C in selected year or prior years","description":"Patients with an encounter in the selected year(s) whose first test for hepatitis C (elisa or RNA) is in or before selected year","nest":"1","pcalc":"a"},"c":{"name":"Patients with a positive result (RNA or ELISA) in selected year or prior years","description":"Patients with an encounter in the selected year(s) with a positive elisa or RNA hepatitis C result in or before selected year","nest":"2","pcalc":"b"},"d":{"name":"Viral load measured during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with an RNA test (any result value) >= 0 days following first positive hepatitis result","nest":"3","pcalc":"c"},"e":{"name":"Detectable viral load during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with a detectable RNA test >= 0 days following first positive hepatitis result","nest":"4","pcalc":"d"},"f":{"name":"Most recent viral load positive","description":"Patients with an encounter in the selected year(s) whose last viral load was positive","nest":"4","pcalc":"d"}, "g":{"name":"Most recent viral load negative","description":"Patients with an encounter in the selected year(s) whose last viral load was negative","nest":"4","pcalc":"d"},"h":{"name":"Patients tested for hepatitis C in selected year(s)","description":"Patients with an encounter in the selected year(s) whose first test for hepatitis C (elisa or RNA) is in selected year only","nest":"1","pcalc":"a"}, "i":{"name":"Patients with a positive result (RNA or ELISA) in selected year(s)","description":"Patients with an encounter in the selected year(s) with a positive elisa or RNA hepatitis C result in selected year only","nest":"2","pcalc":"h"}, "j":{"name":"Viral load measured during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with an RNA test (any result value) >= 0 days following first positive hepatitis result","nest":"3","pcalc":"i"},"k":{"name":"Detectable viral load during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with a detectable RNA test >= 0 days following first positive hepatitis result","nest":"4","pcalc":"j"},"l":{"name":"Most recent viral load positive","description":"Patients with an encounter in the selected year(s) whose last viral load was positive","nest":"4","pcalc":"j"}, "m":{"name":"Most recent viral load negative","description":"Patients with an encounter in the selected year(s) whose last viral load was negative","nest":"4","pcalc":"j"}, "n":{"name":"Acute hepatitis C","description":"Patients who met the ESP case definition for acute hep C in the selected year(s)","nest":"3","pcalc":"c"}, "o":{"name":"Resolved, no treatment","description":"Acute hepatitis C cases whose most recent viral load was negative and who have no evidence of ever having been treated","nest":"4","pcalc":"n"}, "p":{"name":"Persistently infected","description":"Acute hepatitis C cases with a positive load at least 1 year following the acute case date or who received medication at least 1 year following the acute case date","nest":"4","pcalc":"n"}, "q":{"name":"Unknown status","description":"Acute hepatitis C cases who have not had a negative viral load within 1 year following case identification or who have not had a viral load measured ≥ 1 year following case identification","nest":"4","pcalc":"n"}, "r":{"name":"Chronic hepatitis C","description":"Patients that had a positive hepatitis C lab and did not spontaneously clear the infection without treatment. Note: this includes acute cases that are persistently infected or have an unknown status","nest":"3","pcalc":"c"}, "s":{"name":"Never on treatment","description":"Chronic hepatitis C cases who have no evidence of being treated","nest":"4","pcalc":"r"},"t":{"name":"Resolved","description":"Chronic hepatitis C cases who have no evidence of being treated and whose last viral load was undetected","nest":"5","pcalc":"s"}, "u":{"name":"Persistently infected","description":"Chronic hepatitis C cases who have no evidence of being treated and whose last viral load was positive","nest":"5","pcalc":"s"}, "v":{"name":"Unknown status","description":"Chronic hepatitis C cases who have no evidence of being treated and have never had a viral load","nest":"5","pcalc":"s"}, "w":{"name":"Received treatment","description":"Chronic hep C cases ever started on HCV treatment","nest":"4","pcalc":"r"}, "x":{"name":"Viral load not measured","description":"Chronic hepatitis C cases who have no follow-up viral load since starting treatment","nest":"5","pcalc":"w"}, "y":{"name":"Viral load measured","description":"Chronic hepatitis C cases who have a follow-up viral load (any result) since starting treatment","nest":"5","pcalc":"w"}, "z":{"name":"Virologic suppression","description":"Chronic hepatitis C cases whose last viral load was undetectable (whether on treatment or post treatment)","nest":"6","pcalc":"y"}, "aa":{"name":"Sustained virological response","description":"Chronic hepatitis C cases with an undetectable viral load who have been off HCV medications for > 24 weeks","nest":"6","pcalc":"y"}}'::json rowmeta,
(select '{'||array_to_string(array_agg(row_),',')||'}' from  (select ('"'||
        case when index_enc_yr = 'x' then 'YEAR' else index_enc_yr end ||
          case when agegroup <> 'x' 
            then (select codeval from gen_pop_tools.cc_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end ||
          case when birth_cohort <> 'x' 
            then (select codeval from gen_pop_tools.cc_birth_cohort_codevals cv where t0.birth_cohort=cv.birth_cohort)
          else 'x' end ||
          case when sex <> 'x' 
            then (select codeval from gen_pop_tools.cc_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end ||
          case when race_ethnicity <> 'x' 
            then (select codeval from gen_pop_tools.cc_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end || '":{"a":'|| total
		  || ',"b":' || tested0
		  || ',"c":' || indexpos0 
		  || ',"d":' || vltest0
		  || ',"e":' || vload0
		  || ',"f":' || lastvlpos0
		  || ',"g":' || lastvlneg0
		  || ',"h":' || tested1 
		  || ',"i":' || indexpos1 
		  || ',"j":' || vltest1  
		  || ',"k":' || vload1 
		  || ',"l":' || lastvlpos1 
		  || ',"m":' || lastvlneg1 
		  || ',"n":' || acute
		  || ',"o":' || acute_res_norx 
		  || ',"p":' || acute_res_prst
		  || ',"q":' || acute_res_unkn 
		  || ',"r":' || chronic
		  || ',"s":' || chron_notrt
		  || ',"t":' || chron_notrt_res
		  || ',"u":' || chron_notrt_prst
		  || ',"v":' || chron_notrt_unkn
		  || ',"w":' || chron_trt
		  || ',"x":' || chron_trt_unkn
		  || ',"y":' || chron_trt_vload
		  || ',"z":' || chron_trt_lastvlneg
		  || ',"aa":' || chron_trt24_lastvlneg
		  || '}') row_
from gen_pop_tools.cc_hepc_summaries t0 ) t00)::json rowdata) t1) to :'pathToFile';




  

