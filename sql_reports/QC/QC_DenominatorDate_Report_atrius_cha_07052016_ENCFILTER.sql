--# of encounters (all encounter types)
drop table IF EXISTS temp_allencounters;
create table temp_allencounters as select count(*) countenc, date_trunc('month', date) datemonth from emr_encounter 
where date >= '01-01-2010' 
and date <= now()::date  
and raw_encounter_type NOT IN ('HISTORY', 'O-HISTORY', 'I-HISTORY')
group by  datemonth  order by datemonth asc;

--# of laboratory results (all values; use results date)
drop table IF EXISTS temp_alllabresults;
create table temp_alllabresults as select count(*) countlabres, date_trunc('month', result_date) datemonth 
from emr_labresult 
where result_date >= '01-01-2010' 
and result_date <= now()::date 
group by datemonth  order by datemonth asc;

--# of prescriptions (all Rxs)
drop table IF EXISTS temp_allrx;
create table temp_allrx as select count(*) countrx, date_trunc('month', date) datemonth 
from emr_prescription 
where date >= '01-01-2010' 
and date <= now()::date  
group by datemonth  order by datemonth asc;

--# of encounters where weight is populated
drop table IF EXISTS temp_hasweight;
create table temp_hasweight as select count(*) countweight, date_trunc('month', date) datemonth from emr_encounter 
where date >= '01-01-2010' 
and date <= now()::date  
and weight is not null 
and raw_encounter_type NOT IN ('HISTORY', 'O-HISTORY', 'I-HISTORY')
group by datemonth  order by datemonth asc;

--# of encounters where height is populated
drop table IF EXISTS temp_hasheight;
create table temp_hasheight as select count(*) countheight, date_trunc('month', date) datemonth from emr_encounter 
where date >= '01-01-2010' 
and date <= now()::date  
and height is not null 
and raw_encounter_type NOT IN ('HISTORY', 'O-HISTORY', 'I-HISTORY')
group by datemonth  order by datemonth asc;

--# of encounters where bmi is populated
drop table IF EXISTS temp_hasbmi;
create table temp_hasbmi as select count(*) countbmi, date_trunc('month', date) datemonth from emr_encounter 
where date >= '01-01-2010' 
and date <= now()::date  
and bmi is not null 
and raw_encounter_type NOT IN ('HISTORY', 'O-HISTORY', 'I-HISTORY')
group by datemonth  order by datemonth asc;

--# of encounters where temp is populated
drop table IF EXISTS temp_hastemp;
create table temp_hastemp as select count(*) counttemp, date_trunc('month', date) datemonth from emr_encounter 
where date >= '01-01-2010' 
and date <= now()::date  
and temperature is not null 
and raw_encounter_type NOT IN ('HISTORY', 'O-HISTORY', 'I-HISTORY')
group by datemonth  order by datemonth asc;

--# of patients who have non null tobacco use records in a month
drop table IF EXISTS temp_patientswithtobuse;
create table temp_patientswithtobuse as select count(distinct patient_id) countpatientstobuse, date_trunc('month',date) datemonth from emr_socialhistory
where date >= '01-01-2010' 
and date <= now()::date
and tobacco_use is not null  group by  datemonth  order by datemonth asc;

--# of distinct patients patients with >=1 encounter during the month
drop table IF EXISTS temp_patientswithvisits;
create table temp_patientswithvisits as select count(distinct patient_id) countpatients, date_trunc('month',date) datemonth from emr_encounter 
where date >= '01-01-2010' 
and date <= now()::date  
and raw_encounter_type NOT IN ('HISTORY', 'O-HISTORY', 'I-HISTORY')
group by datemonth  order by datemonth asc;

drop table IF EXISTS qc_denominator_data;
create table qc_denominator_data as 
select enc.datemonth::date, countenc, countlabres, countrx, countweight, countheight, countbmi, counttemp, countpatientstobuse, countpatients
from temp_allencounters enc
join temp_alllabresults lab 
on lab.datemonth = enc.datemonth 
join temp_allrx rx 
on rx.datemonth = enc.datemonth 
join temp_hasweight wt
on wt.datemonth = enc.datemonth
join temp_hasheight ht
on ht.datemonth = enc.datemonth 
join temp_hasbmi bmi
on bmi.datemonth = enc.datemonth 
join temp_hastemp temp
on temp.datemonth = enc.datemonth 
join temp_patientswithvisits patwithvis
on patwithvis.datemonth = enc.datemonth
join temp_patientswithtobuse
on temp_patientswithtobuse.datemonth = enc.datemonth;

drop table IF EXISTS temp_allencounters;
drop table IF EXISTS temp_alllabresults;
drop table IF EXISTS temp_allrx;
drop table IF EXISTS temp_hasweight;
drop table IF EXISTS temp_hasheight;
drop table IF EXISTS temp_hasbmi;
drop table IF EXISTS temp_hastemp;
drop table IF EXISTS temp_patientswithtobuse;
drop table IF EXISTS temp_patientswithvisits;

select * from qc_denominator_data;


