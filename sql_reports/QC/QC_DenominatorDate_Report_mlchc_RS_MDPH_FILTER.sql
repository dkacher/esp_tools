--# of encounters 
drop table IF EXISTS temp_allencounters;
create table temp_allencounters as 
select count(*) countenc, center_id, date_trunc('month', date) datemonth 
from emr_patient p, emr_encounter e, static_enc_type_lookup s 
where p.id = e.patient_id 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
and date >= '01-01-2010' 
and center_id != '1'
and date <= now()::date  
group by center_id, datemonth  order by datemonth, center_id::int asc;

-- --# of encounters -only MEDICAL
-- drop table IF EXISTS temp_allmedencounters;
-- create table temp_allmedencounters as 
-- select count(*) countmedenc, center_id, date_trunc('month', date) datemonth from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
-- and center_id != '1'
-- and substring(raw_encounter_type from 1 for 1) = 'Y'
-- and date >= '01-01-2010' 
-- and date <= now()::date  group by center_id, datemonth  order by datemonth, center_id::int asc;


-- --# of missed visits 
-- drop table IF EXISTS temp_missedvisits;
-- create table temp_missedvisits  as select count(*) countmissvis, center_id, date_trunc('month', date) datemonth from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
-- and (raw_encounter_type = 'NO SHOW' or raw_encounter_type = 'APPT CANCELLED')
-- and center_id != '1'
-- and date >= '01-01-2010' 
-- and date <= now()::date  group by center_id, datemonth  order by datemonth, center_id::int asc;


--# of laboratory results (all values; use date column which is what is used in in algorithms)
drop table IF EXISTS temp_alllabresults;
create table temp_alllabresults as select count(*) countlabres, center_id, date_trunc('month', date) datemonth 
from emr_patient, emr_labresult 
where emr_patient.id = emr_labresult.patient_id 
and center_id != '1'
and result_date >= '01-01-2010' 
and result_date <= now()::date 
group by center_id, datemonth  order by datemonth, center_id::int asc;

--# of prescriptions (all Rx�s)
drop table IF EXISTS temp_allrx;
create table temp_allrx as select count(*) countrx, center_id, date_trunc('month', date) datemonth 
from emr_patient, emr_prescription where emr_patient.id = emr_prescription.patient_id 
and center_id != '1'
and date >= '01-01-2010' 
and date <= now()::date  group by center_id, datemonth  order by datemonth, center_id::int asc;

--# of encounters where weight is populated
drop table IF EXISTS temp_hasweight;
create table temp_hasweight as 
select count(*) countweight, center_id, date_trunc('month', date) datemonth 
from emr_patient p, emr_encounter e, static_enc_type_lookup s
where p.id = e.patient_id 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
and center_id != '1'
and date >= '01-01-2010' 
and date <= now()::date  
and weight is not null 
group by center_id, datemonth  order by datemonth, center_id::int asc;

--# of encounters where height is populated
drop table IF EXISTS temp_hasheight;
create table temp_hasheight as 
select count(*) countheight, center_id, date_trunc('month', date) datemonth 
from emr_patient p, emr_encounter e, static_enc_type_lookup s
where p.id = e.patient_id 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
and center_id != '1'
and date >= '01-01-2010' 
and date <= now()::date  
and height is not null 
group by center_id, datemonth  order by datemonth, center_id::int asc;

--# of encounters where bmi is populated
drop table IF EXISTS temp_hasbmi;
create table temp_hasbmi as 
select count(*) countbmi, center_id, date_trunc('month', date) datemonth 
from emr_patient p, emr_encounter e, static_enc_type_lookup s
where p.id = e.patient_id 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
and center_id != '1'
and date >= '01-01-2010' 
and date <= now()::date  
and bmi is not null 
group by center_id, datemonth  
order by datemonth, center_id::int asc;

--# of encounters where temp is populated
drop table IF EXISTS temp_hastemp;
create table temp_hastemp as 
select count(*) counttemp, center_id, date_trunc('month', date) datemonth 
from emr_patient p, emr_encounter e, static_enc_type_lookup s
where p.id = e.patient_id 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
and center_id != '1'
and date >= '01-01-2010' 
and date <= now()::date  
and temperature is not null 
group by center_id, datemonth  
order by datemonth, center_id::int asc;

--# of patients who have non null tobacco use records in a month
drop table IF EXISTS temp_patientswithtobuse;
create table temp_patientswithtobuse as select count(distinct patient_id) countpatientstobuse, center_id, date_trunc('month',date) datemonth from emr_patient, emr_socialhistory
where emr_patient.id = emr_socialhistory.patient_id 
and center_id != '1'
and date >= '01-01-2010' 
and date <= now()::date
and tobacco_use is not null  group by center_id, datemonth  order by datemonth, center_id::int asc;

--# of distinct patients patients with >=1 encounter during the month
-- Just a join against encounter but count patients not encounters
--Filter out missed visits!
drop table IF EXISTS temp_patientswithvisits;
create table temp_patientswithvisits as 
select count(distinct patient_id) countpatients, center_id, date_trunc('month',date) datemonth 
from emr_patient p, emr_encounter e, static_enc_type_lookup s
where p.id = e.patient_id 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
and center_id != '1'
and date >= '01-01-2010' 
and date <= now()::date  
group by center_id, datemonth  order by datemonth, center_id::int asc;


--# of distinct patients patients with >=1 encounter during the month that are MEDICAL!!!!!!!!!!!!!!!!!!!!!!!
-- Just a join against encounter but count patients not encounters
--Filter out missed visits!  ONLY DO MEDICAL ENCOUNTERS
-- drop table IF EXISTS temp_patientswithmedicalvisits;
-- create table temp_patientswithmedicalvisits as select count(distinct patient_id) countpatientswithmedenc, center_id, date_trunc('month',date) datemonth from emr_patient, emr_encounter 
-- where emr_patient.id = emr_encounter.patient_id 
-- and date >= '01-01-2010' 
-- and (raw_encounter_type != 'NO SHOW' or raw_encounter_type != 'APPT CANCELLED')
-- and substring(raw_encounter_type from 1 for 1) = 'Y'
-- and date <= now()::date  group by center_id, datemonth  order by datemonth, center_id::int asc;

drop table IF EXISTS qc_denominator_data;
create table qc_denominator_data as 
select enc.center_id, enc.datemonth::date, countenc, 
--countmedenc, countmissvis, 
countlabres, countrx, countweight, countheight, countbmi, counttemp, countpatientstobuse, countpatients
--countpatientswithmedenc
from temp_allencounters enc
-- left join temp_allmedencounters encmed
-- on encmed.datemonth = enc.datemonth and encmed.center_id = enc.center_id
left join temp_alllabresults lab 
on lab.datemonth = enc.datemonth and lab.center_id = enc.center_id
left join temp_allrx rx 
on rx.datemonth = enc.datemonth and rx.center_id = enc.center_id
left join temp_hasweight wt
on wt.datemonth = enc.datemonth and wt.center_id = enc.center_id
left join temp_hasheight ht
on ht.datemonth = enc.datemonth and ht.center_id = enc.center_id
left join temp_hasbmi bmi
on bmi.datemonth = enc.datemonth and bmi.center_id = enc.center_id
left join temp_hastemp temp
on temp.datemonth = enc.datemonth and temp.center_id = enc.center_id 
left join temp_patientswithtobuse tobuse
on tobuse.datemonth = enc.datemonth and tobuse.center_id = enc.center_id
left join temp_patientswithvisits patwithvis
on patwithvis.datemonth = enc.datemonth and patwithvis.center_id = enc.center_id
-- left join temp_patientswithmedicalvisits patwithmedvis
-- on patwithmedvis.datemonth = enc.datemonth and patwithmedvis.center_id = enc.center_id
-- left join temp_missedvisits missedvisits on 
-- missedvisits.datemonth = enc.datemonth and missedvisits.center_id = enc.center_id
order by enc.center_id::int, datemonth;

drop table IF EXISTS temp_allencounters;
drop table IF EXISTS temp_alllabresults;
drop table IF EXISTS temp_allrx;
drop table IF EXISTS temp_hasweight;
drop table IF EXISTS temp_hasheight;
drop table IF EXISTS temp_hasbmi;
drop table IF EXISTS temp_hastemp;
drop table IF EXISTS temp_patientswithtobuse;
drop table IF EXISTS temp_patientswithvisits;
drop table IF EXISTS temp_missedvisits;

select * from qc_denominator_data;




