--
-- Script setup section 
--

DROP TABLE IF EXISTS mgtp_hef_w_lab_details CASCADE;
DROP TABLE IF EXISTS mgtp_cases_of_interest CASCADE;
DROP TABLE IF EXISTS mgtp_gon_events CASCADE;
DROP TABLE IF EXISTS mgtp_chlam_events CASCADE;
DROP TABLE IF EXISTS mgtp_syph_events CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_labs CASCADE;
DROP TABLE IF EXISTS mgtp_gon_counts CASCADE;
DROP TABLE IF EXISTS mgtp_chlam_counts CASCADE;
DROP TABLE IF EXISTS mgtp_syph_counts CASCADE;
DROP TABLE IF EXISTS mgtp_syph_cases CASCADE;
DROP TABLE IF EXISTS mgtp_chlam_cases CASCADE;
DROP TABLE IF EXISTS mgtp_gon_cases CASCADE;
DROP TABLE IF EXISTS mgtp_hepb_pos_dna CASCADE;
DROP TABLE IF EXISTS mgtp_cpts_of_interest CASCADE;
DROP TABLE IF EXISTS mgtp_dx_codes CASCADE;
DROP TABLE IF EXISTS mgtp_diag_fields_by_year CASCADE;
DROP TABLE IF EXISTS mgtp_diag_fields CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_counts CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_cases CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_meds CASCADE;
DROP TABLE IF EXISTS mgtp_truvada_array CASCADE;
DROP TABLE IF EXISTS mgtp_truvada_2mogap CASCADE;
DROP TABLE IF EXISTS mgtp_truvada_counts CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_all_details CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_all_details_values CASCADE;
DROP TABLE IF EXISTS mgtp_index_patients CASCADE;
DROP TABLE IF EXISTS mgtp_output_part_1 CASCADE;
DROP TABLE IF EXISTS mgtp_output_with_patient CASCADE;
DROP TABLE IF EXISTS mgtp_output_pat_and_enc CASCADE;
DROP TABLE IF EXISTS mgtp_ouput_pat_and_enc_counts CASCADE;

DROP TABLE IF EXISTS mgtp_truvada_2mogap_prep CASCADE;


DROP TABLE IF EXISTS mgtp_report_final_output CASCADE;


--
-- Script body 
--

-- BASE Join the hef events with the lab results
CREATE TABLE mgtp_hef_w_lab_details AS 
SELECT T1.id,T2.name,T2.patient_id,T2.date,T1.specimen_source,T2.object_id,T1.native_code, T1.native_name
FROM emr_labresult T1,
hef_event T2
WHERE T1.id = T2.object_id
AND T1.patient_id = T2.patient_id
AND T2.date >= '01-01-2010'
AND T2.date < '01-01-2018';

-- BASE - Get the cases we are interested in 
CREATE TABLE mgtp_cases_of_interest  AS 
SELECT T1.condition, T1.date, T1.patient_id, EXTRACT(YEAR FROM date) rpt_year 
FROM  nodis_case T1
WHERE  date >= '01-01-2010' 
AND date < '01-01-2018'
AND condition in ('syphilis', 'gonorrhea', 'chlamydia');

-- GONORRHEA - Gather up gonorrhea events
CREATE TABLE mgtp_gon_events  AS 
SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year,
CASE WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(PHARYNGEAL|THROAT)' or native_code ~* '(PHARYNGEAL|THROAT)')then 'THROAT'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(RECTAL|ANAL)' OR native_code ~* '(RECTAL|ANAL)') THEN 'RECTAL'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(GENITAL|URINE|URN|ENDOCX|THINPREP|THIN PREP|PAP)' OR native_code  ~* '(GENITAL|URINE|URN|URI|UR|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP)') THEN 'UROG'
ELSE specimen_source END specimen_source,
specimen_source specimen_source_orig,
native_code
FROM mgtp_hef_w_lab_details T1
WHERE  name like 'lx:gonorrhea%'
AND date >= '01-01-2010'
AND date < '01-01-2018';

-- CHLAMYDIA - Gather up chlamydia events
CREATE TABLE mgtp_chlam_events  AS 
SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year, specimen_source, native_code 
FROM mgtp_hef_w_lab_details
WHERE name like 'lx:chlamydia%'
AND date >= '01-01-2010'
AND date < '01-01-2018';

-- SYPHILLIS - Gather up syphillis events
CREATE TABLE mgtp_syph_events  AS 
SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year 
FROM mgtp_hef_w_lab_details 
WHERE  name ~ '^(lx:rpr|lx:vdrl|lx:tppa|lx:fta-abs|lx:tp-igg|lx:tp-igm)';

-- HEP B Gather up positive viral dna lab tests -- Any time before the end date
CREATE TABLE mgtp_hepb_pos_dna  AS 
SELECT T1.patient_id, 1::INT pos_hep_b_dna
FROM hef_event T1 
WHERE name = 'lx:hepatitis_b_viral_dna:positive'
AND date < '01-01-2018'
group by patient_id;


-- GONORRHEA - Counts & Column Creation
CREATE TABLE mgtp_gon_counts  AS 
SELECT T1.patient_id,count(*) t_gon_tests, 
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_gon_tests_10, count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_gon_tests_11, 
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_gon_tests_12, count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_gon_tests_13, 
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_gon_tests_14, count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_gon_tests_15, 
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_gon_tests_16, count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_gon_tests_17,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2010' THEN 1 END)  p_gon_tests_10, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2011' THEN 1 END)  p_gon_tests_11, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2012' THEN 1 END)  p_gon_tests_12, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2013' THEN 1 END)  p_gon_tests_13, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2014' THEN 1 END)  p_gon_tests_14, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2015' THEN 1 END)  p_gon_tests_15, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2016' THEN 1 END)  p_gon_tests_16, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2017' THEN 1 END)  p_gon_tests_17,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2010' THEN 1 END)  t_gon_tests_throat_10, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and  rpt_year = '2011' THEN 1 END)  t_gon_tests_throat_11, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and  rpt_year = '2012' THEN 1 END)  t_gon_tests_throat_12, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2013' THEN 1 END)  t_gon_tests_throat_13, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2014' THEN 1 END)  t_gon_tests_throat_14, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2015' THEN 1 END)  t_gon_tests_throat_15, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2016' THEN 1 END)  t_gon_tests_throat_16, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2017' THEN 1 END)  t_gon_tests_throat_17, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END)  p_gon_tests_throat_10, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END)  p_gon_tests_throat_11, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END)  p_gon_tests_throat_12, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END)  p_gon_tests_throat_13, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END)  p_gon_tests_throat_14, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END)  p_gon_tests_throat_15, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END)  p_gon_tests_throat_16, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END)  p_gon_tests_throat_17,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2010' THEN 1 END) t_gon_tests_rectal_10, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2011' THEN 1 END) t_gon_tests_rectal_11, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2012' THEN 1 END) t_gon_tests_rectal_12, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2013' THEN 1 END) t_gon_tests_rectal_13, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2014' THEN 1 END) t_gon_tests_rectal_14, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2015' THEN 1 END) t_gon_tests_rectal_15, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2016' THEN 1 END) t_gon_tests_rectal_16, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2017' THEN 1 END) t_gon_tests_rectal_17, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_gon_tests_rectal_10, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_gon_tests_rectal_11, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_gon_tests_rectal_12, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_gon_tests_rectal_13, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_gon_tests_rectal_14, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_gon_tests_rectal_15, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_gon_tests_rectal_16,  
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_gon_tests_rectal_17,
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') and rpt_year = '2010' THEN 1 END)  t_gon_tests_missing_10, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') and  rpt_year = '2011' THEN 1 END)  t_gon_tests_missing_11, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') and  rpt_year = '2012' THEN 1 END)  t_gon_tests_missing_12, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') and rpt_year = '2013' THEN 1 END)  t_gon_tests_missing_13, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') and rpt_year = '2014' THEN 1 END)  t_gon_tests_missing_14, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') and rpt_year = '2015' THEN 1 END)  t_gon_tests_missing_15, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') and rpt_year = '2016' THEN 1 END)  t_gon_tests_missing_16, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') and rpt_year = '2017' THEN 1 END)  t_gon_tests_missing_17, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') 
	and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END)  p_gon_tests_missing_10, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') 
	and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END)  p_gon_tests_missing_11, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') 
	and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END)  p_gon_tests_missing_12, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') 
	and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END)  p_gon_tests_missing_13, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') 
	and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END)  p_gon_tests_missing_14, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') 
	and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END)  p_gon_tests_missing_15, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') 
	and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END)  p_gon_tests_missing_16, 
count(CASE WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') 
	and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END)  p_gon_tests_missing_17,
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2010' THEN 1 END) t_gon_tests_urog_10, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2011' THEN 1 END) t_gon_tests_urog_11, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2012' THEN 1 END) t_gon_tests_urog_12, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2013' THEN 1 END) t_gon_tests_urog_13, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2014' THEN 1 END) t_gon_tests_urog_14, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2015' THEN 1 END) t_gon_tests_urog_15, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2016' THEN 1 END) t_gon_tests_urog_16, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2017' THEN 1 END) t_gon_tests_urog_17, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_gon_tests_urog_10, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_gon_tests_urog_11, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_gon_tests_urog_12, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_gon_tests_urog_13, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_gon_tests_urog_14, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_gon_tests_urog_15, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_gon_tests_urog_16,  
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_gon_tests_urog_17,
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and rpt_year = '2010' THEN 1 END) t_gon_tests_other_10, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and rpt_year = '2011' THEN 1 END) t_gon_tests_other_11, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and rpt_year = '2012' THEN 1 END) t_gon_tests_other_12, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and rpt_year = '2013' THEN 1 END) t_gon_tests_other_13, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and rpt_year = '2014' THEN 1 END) t_gon_tests_other_14, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and rpt_year = '2015' THEN 1 END) t_gon_tests_other_15, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and rpt_year = '2016' THEN 1 END) t_gon_tests_other_16, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and rpt_year = '2017' THEN 1 END) t_gon_tests_other_17, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_gon_tests_other_10, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_gon_tests_other_11, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_gon_tests_other_12, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_gon_tests_other_13, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_gon_tests_other_14, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_gon_tests_other_15, 
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_gon_tests_other_16,  
count(CASE WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') 
	and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_gon_tests_other_17
FROM  mgtp_gon_events T1   
GROUP BY T1.patient_id;

-- CHLAMYDIA - Counts & Column Creation
CREATE TABLE mgtp_chlam_counts  AS 
SELECT T1.patient_id,
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_chlam_10, count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_chlam_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_chlam_12, count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_chlam_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_chlam_14, count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_chlam_15, 
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_chlam_16, count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_chlam_17, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2010' THEN 1 END)  p_chlam_10, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2011' THEN 1 END)  p_chlam_11,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2012' THEN 1 END)  p_chlam_12, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2013' THEN 1 END)  p_chlam_13,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2014' THEN 1 END)  p_chlam_14, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2015' THEN 1 END)  p_chlam_15,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2016' THEN 1 END)  p_chlam_16, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2017' THEN 1 END)  p_chlam_17
FROM  mgtp_chlam_events T1   
GROUP BY T1.patient_id;

-- SYPHILLIS - Counts & Column Creation
CREATE TABLE mgtp_syph_counts  AS 
SELECT T1.patient_id,
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_syph_10, 
count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_syph_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_syph_12, 
count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_syph_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_syph_14, 
count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_syph_15,
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_syph_16,
count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_syph_17
FROM  mgtp_syph_events T1   
GROUP BY T1.patient_id;

-- SYPHILIS - Cases
CREATE TABLE mgtp_syph_cases  AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.rpt_year = '2010' then 1 end) syphilis_10,
count(CASE WHEN T1.rpt_year = '2011' then 1 end) syphilis_11,
count(CASE WHEN T1.rpt_year = '2012' then 1 end) syphilis_12, 
count(CASE WHEN T1.rpt_year = '2013' then 1 end) syphilis_13,
count(CASE WHEN T1.rpt_year = '2014' then 1 end) syphilis_14, 
count(CASE WHEN T1.rpt_year = '2015' then 1 end) syphilis_15, 
count(CASE WHEN T1.rpt_year = '2016' then 1 end) syphilis_16,
count(CASE WHEN T1.rpt_year = '2017' then 1 end) syphilis_17
FROM  mgtp_cases_of_interest T1   
WHERE condition = 'syphilis' GROUP BY T1.patient_id;

-- GONORRHEA - Cases
CREATE TABLE mgtp_gon_cases  AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.rpt_year = '2010' then 1 end) gon_10,
count(CASE WHEN T1.rpt_year = '2011' then 1 end) gon_11,
count(CASE WHEN T1.rpt_year = '2012' then 1 end) gon_12, 
count(CASE WHEN T1.rpt_year = '2013' then 1 end) gon_13,
count(CASE WHEN T1.rpt_year = '2014' then 1 end) gon_14, 
count(CASE WHEN T1.rpt_year = '2015' then 1 end) gon_15, 
count(CASE WHEN T1.rpt_year = '2016' then 1 end) gon_16,
count(CASE WHEN T1.rpt_year = '2017' then 1 end) gon_17
FROM  mgtp_cases_of_interest T1   
WHERE condition = 'gonorrhea' GROUP BY T1.patient_id;

-- CHLAMYIDA - Cases
CREATE TABLE mgtp_chlam_cases  AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.rpt_year = '2010' then 1 end) chlam_10,
count(CASE WHEN T1.rpt_year = '2011' then 1 end) chlam_11,
count(CASE WHEN T1.rpt_year = '2012' then 1 end) chlam_12, 
count(CASE WHEN T1.rpt_year = '2013' then 1 end) chlam_13,
count(CASE WHEN T1.rpt_year = '2014' then 1 end) chlam_14, 
count(CASE WHEN T1.rpt_year = '2015' then 1 end) chlam_15, 
count(CASE WHEN T1.rpt_year = '2016' then 1 end) chlam_16,
count(CASE WHEN T1.rpt_year = '2017' then 1 end) chlam_17
FROM  mgtp_cases_of_interest T1   
WHERE condition = 'chlamydia' GROUP BY T1.patient_id;


-- Anal Cytology Test - Counts & Column Creation
CREATE TABLE mgtp_cpts_of_interest  AS 
SELECT T1.patient_id, 
1::INT as anal_cytology_test_ever 
FROM  emr_labresult T1   
WHERE native_code like '88160%' 
GROUP BY T1.patient_id
-- Atrius Lab Order
UNION
SELECT T1.patient_id, 
1::INT AS anal_cytology_test_ever 
FROM emr_laborder T1
WHERE procedure_name in ( 'CYTOLOGY ANAL', 'CYTOLOGY THINPREP VIAL (ANAL)')
GROUP BY T1.patient_id
-- CHA Lab Order
UNION
SELECT T1.patient_id, 
1::INT AS anal_cytology_test_ever 
FROM emr_laborder T1
WHERE procedure_name in ('CYTOLOGY SPECIMEN', 'CYTOLOGY SPECIMEN (NON-GYN)') and specimen_source in ('ANAL', 'ANAL PAP')
GROUP BY T1.patient_id
-- MLCHC Native Name and Procedure Names
UNION
SELECT T1.patient_id, 
1::INT AS anal_cytology_test_ever 
FROM emr_labresult T1
WHERE procedure_name in ('P-CN, NON-GYN CYTOLOGY (ANAL PAP, URINE CYTOLOGY)', 'P-CN, NON-GYN CYTOLOGY (ANAL PAP, URINE CYTOLOGY) CMH') 
or native_name in ('Anal Pap-CYTOLOGY, NON-GYN', 'Anal Pap-NON-GYN CYTOLOGY', 'Anal Pap-NON GYNECOLOGIC CYTOLOGY (ANAL PAP, URINE CYTOLOGY)')
GROUP BY T1.patient_id;



-- DX Gather up dx_codes 
CREATE TABLE mgtp_dx_codes  AS 
SELECT * FROM emr_encounter_dx_codes 
WHERE dx_code_id ~'^(icd9:796.7)' --abnormal anal cytology, anal dysplasia, or anal carcinoma in situ
OR dx_code_id in (
'icd9:788.7',   --urethral discharge
'icd10:R36.0',  --urethral discharge
'icd10:R36.9',  --urethral discharge
'icd9:099.40',  --urethritis
'icd9:597.80',  --urethritis
'icd10:N34.1',  --urethritis 
'icd10:N34.2',  --urethritis
'icd9:616.10',  --vaginitis
'icd10:N76.0',  --vaginitis
'icd10:N76.1',  --vaginitis
'icd10:N76.2',  --vaginitis
'icd10:N76.3',   --vaginitis
'icd9:616.0',   --cervicitis
'icd10:N72',    --cervicitis
'icd9:623.5',   --vaginal leucorrhea
'icd10:N89.8',  --vaginal leucorrhea
'icd9:099.54',  --chlamydia
'icd10:A56.19', --chlamydia
'icd9:V74.5',   --screening for STIs
'icd10:Z11.3',  --screening for STIs
'icd9:569.44',  --abnormal anal cytology, anal dysplasia, or anal carcinoma in situ 
'icd9:230.5',   --abnormal anal cytology, anal dysplasia, or anal carcinoma in situ
'icd9:230.6',   --abnormal anal cytology, anal dysplasia, or anal carcinoma in situ
'icd10:D01.3',  --abnormal anal cytology, anal dysplasia, or anal carcinoma in situ
'icd9:V01.6',   --contact with or exposure to venereal disease
'icd10:Z20.2',  --contact with or exposure to venereal disease
'icd10:Z20.6',  --contact with or exposure to venereal disease
'icd9:V69.2',   --high risk sexual behavior
'icd10:Z72.5',  --high risk sexual behavior
'icd9:V65.44',  --HIV counseling
'icd10:Z71.7'   --HIV counseling
);

set enable_nestloop=off;

-- DX Create Fields

CREATE TABLE mgtp_diag_fields  AS 
SELECT T1.patient_id, 
EXTRACT(YEAR FROM date) rpt_year,
MAX(CASE WHEN T2.dx_code_id like 'icd9:796.7%' or T2.dx_code_id in ('icd9:569.44', 'icd9:230.5','icd9:230.6','icd10:D01.3') then 1 end) abnormal_anal_cytology,
MAX(CASE WHEN T2.dx_code_id in ('icd9:788.7', 'icd10:R36.0', 'icd10:R36.9') then 1 end) urethral_discharge,
MAX(CASE WHEN T2.dx_code_id in ('icd9:099.40', 'icd9:597.80', 'icd10:N34.1', 'icd10:N34.2') then 1 end) urethritis,
MAX(CASE WHEN T2.dx_code_id in ('icd9:616.10', 'icd10:N76.0', 'icd10:N76.1', 'icd10:N76.2', 'icd10:N76.3') then 1 end) vaginitis,
MAX(CASE WHEN T2.dx_code_id in ('icd9:616.0', 'icd10:N72') then 1 end) cervicitis,
MAX(CASE WHEN T2.dx_code_id in ('icd9:623.5', 'icd10:N89.8') then 1 end) vaginal_leucorrhea,
MAX(CASE WHEN T2.dx_code_id in ('icd9:099.54','icd10:A56.19') then 1 end) chlamydia,
MAX(CASE WHEN T2.dx_code_id in ('icd9:V74.5', 'icd10:Z11.3') then 1 end) screening_for_stis,
MAX(CASE WHEN T2.dx_code_id in ('icd9:V01.6', 'icd10:Z20.2','icd10:Z20.6') then 1 end)  cont_or_exp_to_vd,
MAX(CASE WHEN T2.dx_code_id in ('icd9:V69.2', 'icd10:Z72.5') then 1 end) high_risk_sexual_behavior,
MAX(CASE WHEN T2.dx_code_id in ('icd9:V65.44','icd10:Z71.7') then 1 end) hiv_counseling
FROM emr_encounter T1,
mgtp_dx_codes T2
WHERE T1.id = T2.encounter_id 
AND date >= '01-01-2010'
AND date < '01-01-2018'
GROUP BY T1.patient_id, EXTRACT(YEAR FROM date);

set enable_nestloop=on;

CREATE TABLE mgtp_diag_fields_by_year AS
SELECT patient_id,
MAX(CASE WHEN abnormal_anal_cytology = 1 and rpt_year = 2010 then 1 else 0 end) abnormal_anal_cytology_10,
MAX(CASE WHEN abnormal_anal_cytology = 1 and rpt_year = 2011 then 1 else 0 end) abnormal_anal_cytology_11,
MAX(CASE WHEN abnormal_anal_cytology = 1 and rpt_year = 2012 then 1 else 0 end) abnormal_anal_cytology_12,
MAX(CASE WHEN abnormal_anal_cytology = 1 and rpt_year = 2013 then 1 else 0 end) abnormal_anal_cytology_13,
MAX(CASE WHEN abnormal_anal_cytology = 1 and rpt_year = 2014 then 1 else 0 end) abnormal_anal_cytology_14,
MAX(CASE WHEN abnormal_anal_cytology = 1 and rpt_year = 2015 then 1 else 0 end) abnormal_anal_cytology_15,
MAX(CASE WHEN abnormal_anal_cytology = 1 and rpt_year = 2016 then 1 else 0 end) abnormal_anal_cytology_16,
MAX(CASE WHEN abnormal_anal_cytology = 1 and rpt_year = 2017 then 1 else 0 end) abnormal_anal_cytology_17,
MAX(CASE WHEN urethral_discharge = 1 and rpt_year = 2010 then 1 else 0 end) urethral_discharge_10,
MAX(CASE WHEN urethral_discharge = 1 and rpt_year = 2011 then 1 else 0 end) urethral_discharge_11,
MAX(CASE WHEN urethral_discharge = 1 and rpt_year = 2012 then 1 else 0 end) urethral_discharge_12,
MAX(CASE WHEN urethral_discharge = 1 and rpt_year = 2013 then 1 else 0 end) urethral_discharge_13,
MAX(CASE WHEN urethral_discharge = 1 and rpt_year = 2014 then 1 else 0 end) urethral_discharge_14,
MAX(CASE WHEN urethral_discharge = 1 and rpt_year = 2015 then 1 else 0 end) urethral_discharge_15,
MAX(CASE WHEN urethral_discharge = 1 and rpt_year = 2016 then 1 else 0 end) urethral_discharge_16,
MAX(CASE WHEN urethral_discharge = 1 and rpt_year = 2017 then 1 else 0 end) urethral_discharge_17,
MAX(CASE WHEN urethritis = 1 and rpt_year = 2010 then 1 else 0 end) urethritis_10,
MAX(CASE WHEN urethritis = 1 and rpt_year = 2011 then 1 else 0 end) urethritis_11,
MAX(CASE WHEN urethritis = 1 and rpt_year = 2012 then 1 else 0 end) urethritis_12,
MAX(CASE WHEN urethritis = 1 and rpt_year = 2013 then 1 else 0 end) urethritis_13,
MAX(CASE WHEN urethritis = 1 and rpt_year = 2014 then 1 else 0 end) urethritis_14,
MAX(CASE WHEN urethritis = 1 and rpt_year = 2015 then 1 else 0 end) urethritis_15,
MAX(CASE WHEN urethritis = 1 and rpt_year = 2016 then 1 else 0 end) urethritis_16,
MAX(CASE WHEN urethritis = 1 and rpt_year = 2017 then 1 else 0 end) urethritis_17,
MAX(CASE WHEN vaginitis = 1 and rpt_year = 2010 then 1 else 0 end) vaginitis_10,
MAX(CASE WHEN vaginitis = 1 and rpt_year = 2011 then 1 else 0 end) vaginitis_11,
MAX(CASE WHEN vaginitis = 1 and rpt_year = 2012 then 1 else 0 end) vaginitis_12,
MAX(CASE WHEN vaginitis = 1 and rpt_year = 2013 then 1 else 0 end) vaginitis_13,
MAX(CASE WHEN vaginitis = 1 and rpt_year = 2014 then 1 else 0 end) vaginitis_14,
MAX(CASE WHEN vaginitis = 1 and rpt_year = 2015 then 1 else 0 end) vaginitis_15,
MAX(CASE WHEN vaginitis = 1 and rpt_year = 2016 then 1 else 0 end) vaginitis_16,
MAX(CASE WHEN vaginitis = 1 and rpt_year = 2017 then 1 else 0 end) vaginitis_17,
MAX(CASE WHEN cervicitis = 1 and rpt_year = 2010 then 1 else 0 end) cervicitis_10,
MAX(CASE WHEN cervicitis = 1 and rpt_year = 2011 then 1 else 0 end) cervicitis_11,
MAX(CASE WHEN cervicitis = 1 and rpt_year = 2012 then 1 else 0 end) cervicitis_12,
MAX(CASE WHEN cervicitis = 1 and rpt_year = 2013 then 1 else 0 end) cervicitis_13,
MAX(CASE WHEN cervicitis = 1 and rpt_year = 2014 then 1 else 0 end) cervicitis_14,
MAX(CASE WHEN cervicitis = 1 and rpt_year = 2015 then 1 else 0 end) cervicitis_15,
MAX(CASE WHEN cervicitis = 1 and rpt_year = 2016 then 1 else 0 end) cervicitis_16,
MAX(CASE WHEN cervicitis = 1 and rpt_year = 2017 then 1 else 0 end) cervicitis_17,
MAX(CASE WHEN vaginal_leucorrhea = 1 and rpt_year = 2010 then 1 else 0 end) vaginal_leucorrhea_10,
MAX(CASE WHEN vaginal_leucorrhea = 1 and rpt_year = 2011 then 1 else 0 end) vaginal_leucorrhea_11,
MAX(CASE WHEN vaginal_leucorrhea = 1 and rpt_year = 2012 then 1 else 0 end) vaginal_leucorrhea_12,
MAX(CASE WHEN vaginal_leucorrhea = 1 and rpt_year = 2013 then 1 else 0 end) vaginal_leucorrhea_13,
MAX(CASE WHEN vaginal_leucorrhea = 1 and rpt_year = 2014 then 1 else 0 end) vaginal_leucorrhea_14,
MAX(CASE WHEN vaginal_leucorrhea = 1 and rpt_year = 2015 then 1 else 0 end) vaginal_leucorrhea_15,
MAX(CASE WHEN vaginal_leucorrhea = 1 and rpt_year = 2016 then 1 else 0 end) vaginal_leucorrhea_16,
MAX(CASE WHEN vaginal_leucorrhea = 1 and rpt_year = 2017 then 1 else 0 end) vaginal_leucorrhea_17,
MAX(CASE WHEN chlamydia = 1 and rpt_year = 2010 then 1 else 0 end) chlamydia_dx_10,
MAX(CASE WHEN chlamydia = 1 and rpt_year = 2011 then 1 else 0 end) chlamydia_dx_11,
MAX(CASE WHEN chlamydia = 1 and rpt_year = 2012 then 1 else 0 end) chlamydia_dx_12,
MAX(CASE WHEN chlamydia = 1 and rpt_year = 2013 then 1 else 0 end) chlamydia_dx_13,
MAX(CASE WHEN chlamydia = 1 and rpt_year = 2014 then 1 else 0 end) chlamydia_dx_14,
MAX(CASE WHEN chlamydia = 1 and rpt_year = 2015 then 1 else 0 end) chlamydia_dx_15,
MAX(CASE WHEN chlamydia = 1 and rpt_year = 2016 then 1 else 0 end) chlamydia_dx_16,
MAX(CASE WHEN chlamydia = 1 and rpt_year = 2017 then 1 else 0 end) chlamydia_dx_17,
MAX(CASE WHEN screening_for_stis = 1 and rpt_year = 2010 then 1 else 0 end) screening_for_stis_10,
MAX(CASE WHEN screening_for_stis = 1 and rpt_year = 2011 then 1 else 0 end) screening_for_stis_11,
MAX(CASE WHEN screening_for_stis = 1 and rpt_year = 2012 then 1 else 0 end) screening_for_stis_12,
MAX(CASE WHEN screening_for_stis = 1 and rpt_year = 2013 then 1 else 0 end) screening_for_stis_13,
MAX(CASE WHEN screening_for_stis = 1 and rpt_year = 2014 then 1 else 0 end) screening_for_stis_14,
MAX(CASE WHEN screening_for_stis = 1 and rpt_year = 2015 then 1 else 0 end) screening_for_stis_15,
MAX(CASE WHEN screening_for_stis = 1 and rpt_year = 2016 then 1 else 0 end) screening_for_stis_16,
MAX(CASE WHEN screening_for_stis = 1 and rpt_year = 2017 then 1 else 0 end) screening_for_stis_17,
MAX(CASE WHEN cont_or_exp_to_vd = 1 and rpt_year = 2010 then 1 else 0 end) cont_or_exp_to_vd_10,
MAX(CASE WHEN cont_or_exp_to_vd = 1 and rpt_year = 2011 then 1 else 0 end) cont_or_exp_to_vd_11,
MAX(CASE WHEN cont_or_exp_to_vd = 1 and rpt_year = 2012 then 1 else 0 end) cont_or_exp_to_vd_12,
MAX(CASE WHEN cont_or_exp_to_vd = 1 and rpt_year = 2013 then 1 else 0 end) cont_or_exp_to_vd_13,
MAX(CASE WHEN cont_or_exp_to_vd = 1 and rpt_year = 2014 then 1 else 0 end) cont_or_exp_to_vd_14,
MAX(CASE WHEN cont_or_exp_to_vd = 1 and rpt_year = 2015 then 1 else 0 end) cont_or_exp_to_vd_15,
MAX(CASE WHEN cont_or_exp_to_vd = 1 and rpt_year = 2016 then 1 else 0 end) cont_or_exp_to_vd_16,
MAX(CASE WHEN cont_or_exp_to_vd = 1 and rpt_year = 2017 then 1 else 0 end) cont_or_exp_to_vd_17,
MAX(CASE WHEN high_risk_sexual_behavior = 1 and rpt_year = 2010 then 1 else 0 end) high_risk_sexual_behavior_10,
MAX(CASE WHEN high_risk_sexual_behavior = 1 and rpt_year = 2011 then 1 else 0 end) high_risk_sexual_behavior_11,
MAX(CASE WHEN high_risk_sexual_behavior = 1 and rpt_year = 2012 then 1 else 0 end) high_risk_sexual_behavior_12,
MAX(CASE WHEN high_risk_sexual_behavior = 1 and rpt_year = 2013 then 1 else 0 end) high_risk_sexual_behavior_13,
MAX(CASE WHEN high_risk_sexual_behavior = 1 and rpt_year = 2014 then 1 else 0 end) high_risk_sexual_behavior_14,
MAX(CASE WHEN high_risk_sexual_behavior = 1 and rpt_year = 2015 then 1 else 0 end) high_risk_sexual_behavior_15,
MAX(CASE WHEN high_risk_sexual_behavior = 1 and rpt_year = 2016 then 1 else 0 end) high_risk_sexual_behavior_16,
MAX(CASE WHEN high_risk_sexual_behavior = 1 and rpt_year = 2017 then 1 else 0 end) high_risk_sexual_behavior_17,
MAX(CASE WHEN hiv_counseling = 1 and rpt_year = 2010 then 1 else 0 end) hiv_counseling_10,
MAX(CASE WHEN hiv_counseling = 1 and rpt_year = 2011 then 1 else 0 end) hiv_counseling_11,
MAX(CASE WHEN hiv_counseling = 1 and rpt_year = 2012 then 1 else 0 end) hiv_counseling_12,
MAX(CASE WHEN hiv_counseling = 1 and rpt_year = 2013 then 1 else 0 end) hiv_counseling_13,
MAX(CASE WHEN hiv_counseling = 1 and rpt_year = 2014 then 1 else 0 end) hiv_counseling_14,
MAX(CASE WHEN hiv_counseling = 1 and rpt_year = 2015 then 1 else 0 end) hiv_counseling_15,
MAX(CASE WHEN hiv_counseling = 1 and rpt_year = 2016 then 1 else 0 end) hiv_counseling_16,
MAX(CASE WHEN hiv_counseling = 1 and rpt_year = 2017 then 1 else 0 end) hiv_counseling_17
FROM mgtp_diag_fields
GROUP BY patient_id;



-- HIV - Gather Up Lab Tests 
CREATE TABLE mgtp_hiv_labs AS 
SELECT T1.patient_id, T2.test_name, EXTRACT(YEAR FROM date) rpt_year 
FROM emr_labresult T1, 
conf_labtestmap T2
WHERE T1.native_code = T2.native_code
AND test_name ilike 'hiv_%' 
AND date >= '01-01-2010'
AND date < '01-01-2018';

-- HIV - Lab Counts & Column Creation
CREATE TABLE mgtp_hiv_counts  AS 
SELECT T1.patient_id, count(*) total_hiv_tests 
FROM  mgtp_hiv_labs T1   
GROUP BY T1.patient_id;

-- HIV - Cases
CREATE TABLE mgtp_hiv_cases  AS 
SELECT T1.patient_id, 1::int hiv_per_esp, min(EXTRACT(YEAR FROM date)) hiv_per_esp_date, T1.date
FROM  nodis_case T1
WHERE condition = 'hiv' 
AND date < '01-01-2018'
GROUP BY T1.patient_id,T1.date;

-- HIV - Medications
CREATE TABLE mgtp_hiv_meds AS 
SELECT T1.patient_id,
T1.name,
split_part(T1.name, ':', 2) stripped_name,
T1.date,
case when T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end truvada_rx,
case when T1.name not in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end other_hiv_rx_non_truvada,
CASE WHEN refills~E'^\\d+$' THEN (refills::real +1)  ELSE 1 END  refills_mod,
1::int hiv_meds,
string_to_array(replace(T1.name, 'rx:hiv_',''), '-') test4,
string_to_array(replace(split_part(T1.name, ':', 2), 'hiv_',''), '-')::text test5,
string_to_array(replace(split_part(T1.name, ':', 2), 'hiv_',''), '-') med_array,
quantity,
quantity_float,
quantity_type,
EXTRACT(YEAR FROM T1.date)  rpt_year
FROM hef_event T1,
emr_prescription T2
WHERE T1.object_id = T2.id
AND T1.name ilike 'rx:hiv_%'; 

-- HIV - Truvada Array - For those with more than 2 rx (includes refills)
CREATE TABLE mgtp_truvada_array  AS 
SELECT T1.patient_id, T1.rpt_year, sum(T1.refills_mod)  total_truvada_rx,
array_agg(T1.date)  truvada_array 
FROM  mgtp_hiv_meds T1 
WHERE truvada_rx = 1  
GROUP BY T1.patient_id,T1.rpt_year 
HAVING sum(T1.refills_mod) >=2;

-- MLCHC Cannot Order Arrays -- special table here to unnest and do the sort and calculations
CREATE TABLE mgtp_truvada_2mogap_prep AS
SELECT
T2.patient_id,
T1.rpt_year,
max(T1.val) final_rx,
min(T1.val) first_rx,
max(T1.val) - min(T1.val) two_month_check
FROM 
(SELECT
    patient_id,rpt_year,
    UNNEST(truvada_array) AS val
	FROM mgtp_truvada_array_test
	GROUP BY rpt_year, patient_id, truvada_array
	order by val) T1,
mgtp_truvada_array_test T2
where T1.patient_id = T2.patient_id
GROUP BY T2.patient_id, T1.rpt_year;

-- HIV Truvada - Find those that have prescriptions 2 or more months apart in the same year
CREATE TABLE mgtp_truvada_2mogap  AS 
SELECT T1.patient_id, 
first_rx,
final_rx,
rpt_year,
two_month_check,
1 truvada_criteria_met 
FROM  mgtp_truvada_2mogap_prep T1   
WHERE two_month_check >= 60;

-- CREATE TABLE mgtp_truvada_2mogap  AS 
-- SELECT T1.patient_id, 
-- T1.truvada_array[1] first_rx,
-- truvada_array[array_length(truvada_array, 1)]   final_rx,
-- T1.rpt_year,
-- truvada_array[array_length(truvada_array,1)] - T1.truvada_array[1] two_month_check,
-- 1 truvada_criteria_met 
-- FROM  mgtp_truvada_array T1   
-- WHERE truvada_array[array_length(truvada_array,1)] - T1.truvada_array[1] >= 60;

-- HIV - Truvada Counts & Column Creation 
CREATE TABLE mgtp_truvada_counts  AS 
SELECT T1.patient_id,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2010 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null then 1 else 0 end) hiv_neg_truvada_10,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2011 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null  then 1 else 0 end) hiv_neg_truvada_11,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2012 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null then 1 else 0 end) hiv_neg_truvada_12,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2013 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null  then 1 else 0 end) hiv_neg_truvada_13,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2014 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null then 1 else 0 end) hiv_neg_truvada_14,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2015 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null then 1 else 0 end) hiv_neg_truvada_15,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2016 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null  then 1 else 0 end) hiv_neg_truvada_16,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2017 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null then 1 else 0 end) hiv_neg_truvada_17   
FROM mgtp_truvada_2mogap T1 
LEFT OUTER JOIN mgtp_hiv_cases T2 ON ((T1.patient_id = T2.patient_id))  
LEFT JOIN mgtp_hepb_pos_dna T3 ON ((T1.patient_id = T3.patient_id))
GROUP BY T1.patient_id;

-- HIV - Full Outer Join HIV Lab Events, Meds, and Cases - Derive Compound Values
CREATE TABLE mgtp_hiv_all_details  AS SELECT 
coalesce(lab.patient_id, cases.patient_id, truvada.patient_id) patient_id,
(case when total_hiv_tests is null and hiv_per_esp is null then 2
        when total_hiv_tests > 0 and hiv_per_esp is null then 0
        else hiv_per_esp end) as hiv_per_esp_final, 
coalesce(hiv_per_esp_date, null) as hiv_per_esp_date,
coalesce(hiv_neg_truvada_10,0) as hiv_neg_truvada_10,
coalesce(hiv_neg_truvada_11,0) as hiv_neg_truvada_11,
coalesce(hiv_neg_truvada_12,0) as hiv_neg_truvada_12,
coalesce(hiv_neg_truvada_13,0) as hiv_neg_truvada_13,
coalesce(hiv_neg_truvada_14,0) as hiv_neg_truvada_14,
coalesce(hiv_neg_truvada_15,0) as hiv_neg_truvada_15,
coalesce(hiv_neg_truvada_16,0) as hiv_neg_truvada_16,
coalesce(hiv_neg_truvada_17,0) as hiv_neg_truvada_17
FROM mgtp_hiv_counts lab
FULL OUTER JOIN mgtp_hiv_cases cases
    ON lab.patient_id=cases.patient_id
FULL OUTER JOIN mgtp_truvada_counts truvada
    ON truvada.patient_id = coalesce(lab.patient_id, cases.patient_id);
	
-- FILTER OUT PATIENTS THAT HAVE HIV LAB TESTS BUT NOTHING ELSE
-- AS THAT EXEMPTS THEM FROM THE REPORT
CREATE TABLE mgtp_hiv_all_details_values  AS
SELECT *
FROM mgtp_hiv_all_details 
WHERE  hiv_per_esp_final != 0
OR hiv_per_esp_date is NOT null
OR hiv_neg_truvada_10 != 0
OR hiv_neg_truvada_11 != 0
OR hiv_neg_truvada_12 != 0
OR hiv_neg_truvada_13 != 0
OR hiv_neg_truvada_14 != 0
OR hiv_neg_truvada_15 != 0
OR hiv_neg_truvada_16 != 0
OR hiv_neg_truvada_17 != 0;
	

-- BASE -- Create Index Patients Table	
CREATE TABLE mgtp_index_patients AS
SELECT patient_id FROM mgtp_gon_counts
UNION
SELECT patient_id FROM mgtp_gon_cases
UNION
SELECT patient_id FROM mgtp_chlam_events
UNION
SELECT patient_id FROM mgtp_chlam_cases
UNION
SELECT patient_id FROM mgtp_syph_counts
UNION
SELECT patient_id FROM mgtp_syph_cases
UNION
SELECT patient_id FROM mgtp_cpts_of_interest
UNION
SELECT patient_id FROM mgtp_hiv_all_details_values
UNION
SELECT patient_id FROM mgtp_diag_fields;


-- FULL OUTER JOIN ALL OF THE DATA TOGETHER
CREATE TABLE mgtp_output_part_1  AS SELECT 
--coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id) as master_patient_id,
T1.patient_id,
coalesce(t_gon_tests_10, 0) as total_gonorrhea_tests_10,
coalesce(t_gon_tests_11, 0) as total_gonorrhea_tests_11,
coalesce(t_gon_tests_12, 0) as total_gonorrhea_tests_12,
coalesce(t_gon_tests_13, 0) as total_gonorrhea_tests_13,
coalesce(t_gon_tests_14, 0) as total_gonorrhea_tests_14,
coalesce(t_gon_tests_15, 0) as total_gonorrhea_tests_15,
coalesce(t_gon_tests_16, 0) as total_gonorrhea_tests_16,
coalesce(t_gon_tests_17, 0) as total_gonorrhea_tests_17,
coalesce(p_gon_tests_10, 0) as positive_gonorrhea_tests_10,
coalesce(p_gon_tests_11, 0) as positive_gonorrhea_tests_11,
coalesce(p_gon_tests_12, 0) as positive_gonorrhea_tests_12,
coalesce(p_gon_tests_13, 0) as positive_gonorrhea_tests_13,
coalesce(p_gon_tests_14, 0) as positive_gonorrhea_tests_14,
coalesce(p_gon_tests_15, 0) as positive_gonorrhea_tests_15,
coalesce(p_gon_tests_16, 0) as positive_gonorrhea_tests_16,
coalesce(p_gon_tests_17, 0) as positive_gonorrhea_tests_17,
coalesce(gon_10, 0) as gonorrhea_per_esp_10,
coalesce(gon_11, 0) as gonorrhea_per_esp_11,
coalesce(gon_12, 0) as gonorrhea_per_esp_12,
coalesce(gon_13, 0) as gonorrhea_per_esp_13,
coalesce(gon_14, 0) as gonorrhea_per_esp_14,
coalesce(gon_15, 0) as gonorrhea_per_esp_15,
coalesce(gon_16, 0) as gonorrhea_per_esp_16,
coalesce(gon_17, 0) as gonorrhea_per_esp_17,
coalesce(t_gon_tests_urog_10, 0) as total_gonorrhea_tests_urog_10,
coalesce(t_gon_tests_urog_11, 0) as total_gonorrhea_tests_urog_11,
coalesce(t_gon_tests_urog_12, 0) as total_gonorrhea_tests_urog_12,
coalesce(t_gon_tests_urog_13, 0) as total_gonorrhea_tests_urog_13,
coalesce(t_gon_tests_urog_14, 0) as total_gonorrhea_tests_urog_14,
coalesce(t_gon_tests_urog_15, 0) as total_gonorrhea_tests_urog_15,
coalesce(t_gon_tests_urog_16, 0) as total_gonorrhea_tests_urog_16,
coalesce(t_gon_tests_urog_17, 0) as total_gonorrhea_tests_urog_17,
coalesce(p_gon_tests_urog_10, 0) as positive_gonorrhea_tests_urog_10,
coalesce(p_gon_tests_urog_11, 0) as positive_gonorrhea_tests_urog_11,
coalesce(p_gon_tests_urog_12, 0) as positive_gonorrhea_tests_urog_12,
coalesce(p_gon_tests_urog_13, 0) as positive_gonorrhea_tests_urog_13,
coalesce(p_gon_tests_urog_14, 0) as positive_gonorrhea_tests_urog_14,
coalesce(p_gon_tests_urog_15, 0) as positive_gonorrhea_tests_urog_15,
coalesce(p_gon_tests_urog_16, 0) as positive_gonorrhea_tests_urog_16,
coalesce(p_gon_tests_urog_17, 0) as positive_gonorrhea_tests_urog_17,
coalesce(t_gon_tests_rectal_10, 0) as total_gonorrhea_tests_rectal_10,
coalesce(t_gon_tests_rectal_11, 0) as total_gonorrhea_tests_rectal_11,
coalesce(t_gon_tests_rectal_12, 0) as total_gonorrhea_tests_rectal_12,
coalesce(t_gon_tests_rectal_13, 0) as total_gonorrhea_tests_rectal_13,
coalesce(t_gon_tests_rectal_14, 0) as total_gonorrhea_tests_rectal_14,
coalesce(t_gon_tests_rectal_15, 0) as total_gonorrhea_tests_rectal_15,
coalesce(t_gon_tests_rectal_16, 0) as total_gonorrhea_tests_rectal_16,
coalesce(t_gon_tests_rectal_17, 0) as total_gonorrhea_tests_rectal_17,
coalesce(p_gon_tests_rectal_10, 0) as positive_gonorrhea_tests_rectal_10,
coalesce(p_gon_tests_rectal_11, 0) as positive_gonorrhea_tests_rectal_11,
coalesce(p_gon_tests_rectal_12, 0) as positive_gonorrhea_tests_rectal_12,
coalesce(p_gon_tests_rectal_13, 0) as positive_gonorrhea_tests_rectal_13,
coalesce(p_gon_tests_rectal_14, 0) as positive_gonorrhea_tests_rectal_14,
coalesce(p_gon_tests_rectal_15, 0) as positive_gonorrhea_tests_rectal_15,
coalesce(p_gon_tests_rectal_16, 0) as positive_gonorrhea_tests_rectal_16,
coalesce(p_gon_tests_rectal_17, 0) as positive_gonorrhea_tests_rectal_17,
coalesce(t_gon_tests_throat_10, 0) as total_gonorrhea_tests_throat_10,
coalesce(t_gon_tests_throat_11, 0) as total_gonorrhea_tests_throat_11,
coalesce(t_gon_tests_throat_12, 0) as total_gonorrhea_tests_throat_12,
coalesce(t_gon_tests_throat_13, 0) as total_gonorrhea_tests_throat_13,
coalesce(t_gon_tests_throat_14, 0) as total_gonorrhea_tests_throat_14,
coalesce(t_gon_tests_throat_15, 0) as total_gonorrhea_tests_throat_15,
coalesce(t_gon_tests_throat_16, 0) as total_gonorrhea_tests_throat_16,
coalesce(t_gon_tests_throat_17, 0) as total_gonorrhea_tests_throat_17,
coalesce(p_gon_tests_throat_10, 0) as positive_gonorrhea_tests_throat_10,
coalesce(p_gon_tests_throat_11, 0) as positive_gonorrhea_tests_throat_11,
coalesce(p_gon_tests_throat_12, 0) as positive_gonorrhea_tests_throat_12,
coalesce(p_gon_tests_throat_13, 0) as positive_gonorrhea_tests_throat_13,
coalesce(p_gon_tests_throat_14, 0) as positive_gonorrhea_tests_throat_14,
coalesce(p_gon_tests_throat_15, 0) as positive_gonorrhea_tests_throat_15,
coalesce(p_gon_tests_throat_16, 0) as positive_gonorrhea_tests_throat_16,
coalesce(p_gon_tests_throat_17, 0) as positive_gonorrhea_tests_throat_17,
coalesce(t_gon_tests_other_10, 0) as total_gonorrhea_tests_other_10,
coalesce(t_gon_tests_other_11, 0) as total_gonorrhea_tests_other_11,
coalesce(t_gon_tests_other_12, 0) as total_gonorrhea_tests_other_12,
coalesce(t_gon_tests_other_13, 0) as total_gonorrhea_tests_other_13,
coalesce(t_gon_tests_other_14, 0) as total_gonorrhea_tests_other_14,
coalesce(t_gon_tests_other_15, 0) as total_gonorrhea_tests_other_15,
coalesce(t_gon_tests_other_16, 0) as total_gonorrhea_tests_other_16,
coalesce(t_gon_tests_other_17, 0) as total_gonorrhea_tests_other_17,
coalesce(p_gon_tests_other_10, 0) as positive_gonorrhea_tests_other_10,
coalesce(p_gon_tests_other_11, 0) as positive_gonorrhea_tests_other_11,
coalesce(p_gon_tests_other_12, 0) as positive_gonorrhea_tests_other_12,
coalesce(p_gon_tests_other_13, 0) as positive_gonorrhea_tests_other_13,
coalesce(p_gon_tests_other_14, 0) as positive_gonorrhea_tests_other_14,
coalesce(p_gon_tests_other_15, 0) as positive_gonorrhea_tests_other_15,
coalesce(p_gon_tests_other_16, 0) as positive_gonorrhea_tests_other_16,
coalesce(p_gon_tests_other_17, 0) as positive_gonorrhea_tests_other_17,
coalesce(t_gon_tests_missing_10, 0) as total_gonorrhea_tests_missing_10,
coalesce(t_gon_tests_missing_11, 0) as total_gonorrhea_tests_missing_11,
coalesce(t_gon_tests_missing_12, 0) as total_gonorrhea_tests_missing_12,
coalesce(t_gon_tests_missing_13, 0) as total_gonorrhea_tests_missing_13,
coalesce(t_gon_tests_missing_14, 0) as total_gonorrhea_tests_missing_14,
coalesce(t_gon_tests_missing_15, 0) as total_gonorrhea_tests_missing_15,
coalesce(t_gon_tests_missing_16, 0) as total_gonorrhea_tests_missing_16,
coalesce(t_gon_tests_missing_17, 0) as total_gonorrhea_tests_missing_17,
coalesce(p_gon_tests_missing_10, 0) as positive_gonorrhea_tests_missing_10,
coalesce(p_gon_tests_missing_11, 0) as positive_gonorrhea_tests_missing_11,
coalesce(p_gon_tests_missing_12, 0) as positive_gonorrhea_tests_missing_12,
coalesce(p_gon_tests_missing_13, 0) as positive_gonorrhea_tests_missing_13,
coalesce(p_gon_tests_missing_14, 0) as positive_gonorrhea_tests_missing_14,
coalesce(p_gon_tests_missing_15, 0) as positive_gonorrhea_tests_missing_15,
coalesce(p_gon_tests_missing_16, 0) as positive_gonorrhea_tests_missing_16,
coalesce(p_gon_tests_missing_17, 0) as positive_gonorrhea_tests_missing_17,
coalesce(t_chlam_10, 0) as total_chlamydia_tests_10,
coalesce(t_chlam_11, 0) as total_chlamydia_tests_11,
coalesce(t_chlam_12, 0) as total_chlamydia_tests_12,
coalesce(t_chlam_13, 0) as total_chlamydia_tests_13,
coalesce(t_chlam_14, 0) as total_chlamydia_tests_14,
coalesce(t_chlam_15, 0) as total_chlamydia_tests_15,
coalesce(t_chlam_16, 0) as total_chlamydia_tests_16,
coalesce(t_chlam_17, 0) as total_chlamydia_tests_17,
coalesce(p_chlam_10, 0) as positive_chlamydia_tests_10,
coalesce(p_chlam_11, 0) as positive_chlamydia_tests_11,
coalesce(p_chlam_12, 0) as positive_chlamydia_tests_12,
coalesce(p_chlam_13, 0) as positive_chlamydia_tests_13,
coalesce(p_chlam_14, 0) as positive_chlamydia_tests_14,
coalesce(p_chlam_15, 0) as positive_chlamydia_tests_15,
coalesce(p_chlam_16, 0) as positive_chlamydia_tests_16,
coalesce(p_chlam_17, 0) as positive_chlamydia_tests_17,
coalesce(chlam_10, 0) as chlamydia_per_esp_10,
coalesce(chlam_11, 0) as chlamydia_per_esp_11,
coalesce(chlam_12, 0) as chlamydia_per_esp_12,
coalesce(chlam_13, 0) as chlamydia_per_esp_13,
coalesce(chlam_14, 0) as chlamydia_per_esp_14,
coalesce(chlam_15, 0) as chlamydia_per_esp_15,
coalesce(chlam_16, 0) as chlamydia_per_esp_16,
coalesce(chlam_17, 0) as chlamydia_per_esp_17,
coalesce(t_syph_10, 0) as total_syphilis_tests_10,
coalesce(t_syph_11, 0) as total_syphilis_tests_11,
coalesce(t_syph_12, 0) as total_syphilis_tests_12,
coalesce(t_syph_13, 0) as total_syphilis_tests_13,
coalesce(t_syph_14, 0) as total_syphilis_tests_14,
coalesce(t_syph_15, 0) as total_syphilis_tests_15,
coalesce(t_syph_16, 0) as total_syphilis_tests_16,
coalesce(t_syph_17, 0) as total_syphilis_tests_17,
coalesce(syphilis_10, 0) as syphilis_per_esp_10,
coalesce(syphilis_11, 0) as syphilis_per_esp_11,
coalesce(syphilis_12, 0) as syphilis_per_esp_12,
coalesce(syphilis_13, 0) as syphilis_per_esp_13,
coalesce(syphilis_14, 0) as syphilis_per_esp_14,
coalesce(syphilis_15, 0) as syphilis_per_esp_15,
coalesce(syphilis_16, 0) as syphilis_per_esp_16,
coalesce(syphilis_17, 0) as syphilis_per_esp_17,
coalesce(anal_cytology_test_ever, 0) as anal_cytology_test_ever,
coalesce(hiv_per_esp_final, 2) as hiv_per_esp_spec,
coalesce(hiv_neg_truvada_10,0) as hiv_neg_truvada_10,
coalesce(hiv_neg_truvada_11,0) as hiv_neg_truvada_11,
coalesce(hiv_neg_truvada_12,0) as hiv_neg_truvada_12,
coalesce(hiv_neg_truvada_13,0) as hiv_neg_truvada_13,
coalesce(hiv_neg_truvada_14,0) as hiv_neg_truvada_14,
coalesce(hiv_neg_truvada_15,0) as hiv_neg_truvada_15,
coalesce(hiv_neg_truvada_16,0) as hiv_neg_truvada_16,
coalesce(hiv_neg_truvada_17,0) as hiv_neg_truvada_17,
coalesce(abnormal_anal_cytology_10, 0) as abnormal_anal_cytology_10,
coalesce(abnormal_anal_cytology_11, 0) as abnormal_anal_cytology_11,
coalesce(abnormal_anal_cytology_12, 0) as abnormal_anal_cytology_12,
coalesce(abnormal_anal_cytology_13, 0) as abnormal_anal_cytology_13,
coalesce(abnormal_anal_cytology_14, 0) as abnormal_anal_cytology_14,
coalesce(abnormal_anal_cytology_15, 0) as abnormal_anal_cytology_15,
coalesce(abnormal_anal_cytology_16, 0) as abnormal_anal_cytology_16,
coalesce(abnormal_anal_cytology_17, 0) as abnormal_anal_cytology_17,
coalesce(urethral_discharge_10, 0) as urethral_discharge_10,
coalesce(urethral_discharge_11, 0) as urethral_discharge_11,
coalesce(urethral_discharge_12, 0) as urethral_discharge_12,
coalesce(urethral_discharge_13, 0) as urethral_discharge_13,
coalesce(urethral_discharge_14, 0) as urethral_discharge_14,
coalesce(urethral_discharge_15, 0) as urethral_discharge_15,
coalesce(urethral_discharge_16, 0) as urethral_discharge_16,
coalesce(urethral_discharge_17, 0) as urethral_discharge_17,
coalesce(urethritis_10, 0) as urethritis_10,
coalesce(urethritis_11, 0) as urethritis_11,
coalesce(urethritis_12, 0) as urethritis_12,
coalesce(urethritis_13, 0) as urethritis_13,
coalesce(urethritis_14, 0) as urethritis_14,
coalesce(urethritis_15, 0) as urethritis_15,
coalesce(urethritis_16, 0) as urethritis_16,
coalesce(urethritis_17, 0) as urethritis_17,
coalesce(vaginitis_10, 0) as vaginitis_10,
coalesce(vaginitis_11, 0) as vaginitis_11,
coalesce(vaginitis_12, 0) as vaginitis_12,
coalesce(vaginitis_13, 0) as vaginitis_13,
coalesce(vaginitis_14, 0) as vaginitis_14,
coalesce(vaginitis_15, 0) as vaginitis_15,
coalesce(vaginitis_16, 0) as vaginitis_16,
coalesce(vaginitis_17, 0) as vaginitis_17,
coalesce(cervicitis_10, 0) as cervicitis_10,
coalesce(cervicitis_11, 0) as cervicitis_11,
coalesce(cervicitis_12, 0) as cervicitis_12,
coalesce(cervicitis_13, 0) as cervicitis_13,
coalesce(cervicitis_14, 0) as cervicitis_14,
coalesce(cervicitis_15, 0) as cervicitis_15,
coalesce(cervicitis_16, 0) as cervicitis_16,
coalesce(cervicitis_17, 0) as cervicitis_17,
coalesce(vaginal_leucorrhea_10, 0) as vaginal_leucorrhea_10,
coalesce(vaginal_leucorrhea_11, 0) as vaginal_leucorrhea_11,
coalesce(vaginal_leucorrhea_12, 0) as vaginal_leucorrhea_12,
coalesce(vaginal_leucorrhea_13, 0) as vaginal_leucorrhea_13,
coalesce(vaginal_leucorrhea_14, 0) as vaginal_leucorrhea_14,
coalesce(vaginal_leucorrhea_15, 0) as vaginal_leucorrhea_15,
coalesce(vaginal_leucorrhea_16, 0) as vaginal_leucorrhea_16,
coalesce(vaginal_leucorrhea_17, 0) as vaginal_leucorrhea_17,
coalesce(chlamydia_dx_10, 0) as chlamydia_dx_10,
coalesce(chlamydia_dx_11, 0) as chlamydia_dx_11,
coalesce(chlamydia_dx_12, 0) as chlamydia_dx_12,
coalesce(chlamydia_dx_13, 0) as chlamydia_dx_13,
coalesce(chlamydia_dx_14, 0) as chlamydia_dx_14,
coalesce(chlamydia_dx_15, 0) as chlamydia_dx_15,
coalesce(chlamydia_dx_16, 0) as chlamydia_dx_16,
coalesce(chlamydia_dx_17, 0) as chlamydia_dx_17,
coalesce(screening_for_stis_10, 0) as screening_for_stis_10,
coalesce(screening_for_stis_11, 0) as screening_for_stis_11,
coalesce(screening_for_stis_12, 0) as screening_for_stis_12,
coalesce(screening_for_stis_13, 0) as screening_for_stis_13,
coalesce(screening_for_stis_14, 0) as screening_for_stis_14,
coalesce(screening_for_stis_15, 0) as screening_for_stis_15,
coalesce(screening_for_stis_16, 0) as screening_for_stis_16,
coalesce(screening_for_stis_17, 0) as screening_for_stis_17,
coalesce(cont_or_exp_to_vd_10, 0) as cont_or_exp_to_vd_10,
coalesce(cont_or_exp_to_vd_11, 0) as cont_or_exp_to_vd_11,
coalesce(cont_or_exp_to_vd_12, 0) as cont_or_exp_to_vd_12,
coalesce(cont_or_exp_to_vd_13, 0) as cont_or_exp_to_vd_13,
coalesce(cont_or_exp_to_vd_14, 0) as cont_or_exp_to_vd_14,
coalesce(cont_or_exp_to_vd_15, 0) as cont_or_exp_to_vd_15,
coalesce(cont_or_exp_to_vd_16, 0) as cont_or_exp_to_vd_16,
coalesce(cont_or_exp_to_vd_17, 0) as cont_or_exp_to_vd_17,
coalesce(high_risk_sexual_behavior_10, 0) as high_risk_sexual_behavior_10,
coalesce(high_risk_sexual_behavior_11, 0) as high_risk_sexual_behavior_11,
coalesce(high_risk_sexual_behavior_12, 0) as high_risk_sexual_behavior_12,
coalesce(high_risk_sexual_behavior_13, 0) as high_risk_sexual_behavior_13,
coalesce(high_risk_sexual_behavior_14, 0) as high_risk_sexual_behavior_14,
coalesce(high_risk_sexual_behavior_15, 0) as high_risk_sexual_behavior_15,
coalesce(high_risk_sexual_behavior_16, 0) as high_risk_sexual_behavior_16,
coalesce(high_risk_sexual_behavior_17, 0) as high_risk_sexual_behavior_17,
coalesce(hiv_counseling_10, 0) as hiv_counseling_10,
coalesce(hiv_counseling_11, 0) as hiv_counseling_11,
coalesce(hiv_counseling_12, 0) as hiv_counseling_12,
coalesce(hiv_counseling_13, 0) as hiv_counseling_13,
coalesce(hiv_counseling_14, 0) as hiv_counseling_14,
coalesce(hiv_counseling_15, 0) as hiv_counseling_15,
coalesce(hiv_counseling_16, 0) as hiv_counseling_16,
coalesce(hiv_counseling_17, 0) as hiv_counseling_17
FROM mgtp_index_patients T1
LEFT JOIN mgtp_gon_counts T2 ON T1.patient_id = T2.patient_id
LEFT JOIN mgtp_gon_cases T3  ON T1.patient_id = T3.patient_id
LEFT JOIN mgtp_chlam_counts T4 ON T1.patient_id = T4.patient_id
LEFT JOIN mgtp_chlam_cases T5 ON T1.patient_id = T5.patient_id
LEFT JOIN mgtp_syph_counts T6 ON T1.patient_id = T6.patient_id
LEFT JOIN mgtp_syph_cases T7 ON T1.patient_id = T7.patient_id
LEFT JOIN mgtp_cpts_of_interest T8 ON T1.patient_id = T8.patient_id
LEFT JOIN mgtp_hiv_all_details T9 ON T1.patient_id = T9.patient_id
LEFT JOIN mgtp_diag_fields_by_year T10 ON T1.patient_id = T10.patient_id;


-- JOIN TO PATIENT TABLE
-- Restrict to patients age ≥15.
CREATE TABLE mgtp_output_with_patient  AS
SELECT date_part('year',age(date_of_birth)) today_age,
date_part('year', age('2017-12-31', date_of_birth)) age_end_of_2017,
T1.gender,
T1.race,
T2.*
FROM emr_patient T1 
INNER JOIN mgtp_output_part_1 T2 ON ((T1.id = T2.patient_id))
WHERE date_part('year', age('2017-12-31', date_of_birth)) >= 15
AND (center_id not in ('1') or center_id IS NULL);


-- ENCOUNTERS - Gather up encounters
-- Add center filtering here
CREATE TABLE mgtp_output_pat_and_enc  AS 
SELECT T2.patient_id, EXTRACT(YEAR FROM date)  rpt_year, T1.id as enc_id  
FROM emr_encounter T1, 
mgtp_output_with_patient T2,
static_enc_type_lookup T3
WHERE T1.patient_id = T2.patient_id  
AND T1.raw_encounter_type = T3.raw_encounter_type 
AND (T1.raw_encounter_type is NULL or T3.ambulatory = 1)
AND  T1.date >= '01-01-2010' 
AND T1.date < '01-01-2018';

-- ENCOUNTERS - Counts & Column Creation
CREATE TABLE mgtp_ouput_pat_and_enc_counts  AS 
SELECT T1.patient_id,
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_encounters_10,
count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_encounters_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_encounters_12,
count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_encounters_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_encounters_14,
count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_encounters_15, 
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_encounters_16,
count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_encounters_17
FROM mgtp_output_pat_and_enc T1 
INNER JOIN mgtp_output_with_patient T2 ON ((T1.patient_id = T2.patient_id))  
GROUP BY T1.patient_id;


-- JOIN TO PREVIOUS DATA WITH ENCOUNTERS FOR FULL REPORT
CREATE TABLE mgtp_report_final_output  AS SELECT 
T1.patient_id,
age_end_of_2017,
gender,
race,
coalesce(t_encounters_10, 0) t_encounters_10,
coalesce(t_encounters_11, 0) t_encounters_11,
coalesce(t_encounters_12, 0) t_encounters_12,
coalesce(t_encounters_13, 0) t_encounters_13,
coalesce(t_encounters_14, 0) t_encounters_14,
coalesce(t_encounters_15, 0) t_encounters_15,
coalesce(t_encounters_16, 0) t_encounters_16,
coalesce(t_encounters_17, 0) t_encounters_17,
total_gonorrhea_tests_10,
total_gonorrhea_tests_11,
total_gonorrhea_tests_12,
total_gonorrhea_tests_13,
total_gonorrhea_tests_14,
total_gonorrhea_tests_15,
total_gonorrhea_tests_16,
total_gonorrhea_tests_17,
positive_gonorrhea_tests_10,
positive_gonorrhea_tests_11,
positive_gonorrhea_tests_12,
positive_gonorrhea_tests_13,
positive_gonorrhea_tests_14,
positive_gonorrhea_tests_15,
positive_gonorrhea_tests_16,
positive_gonorrhea_tests_17,
gonorrhea_per_esp_10,
gonorrhea_per_esp_11,
gonorrhea_per_esp_12,
gonorrhea_per_esp_13,
gonorrhea_per_esp_14,
gonorrhea_per_esp_15,
gonorrhea_per_esp_16,
gonorrhea_per_esp_17,
total_gonorrhea_tests_urog_10,
total_gonorrhea_tests_urog_11,
total_gonorrhea_tests_urog_12,
total_gonorrhea_tests_urog_13,
total_gonorrhea_tests_urog_14,
total_gonorrhea_tests_urog_15,
total_gonorrhea_tests_urog_16,
total_gonorrhea_tests_urog_17,
positive_gonorrhea_tests_urog_10,
positive_gonorrhea_tests_urog_11,
positive_gonorrhea_tests_urog_12,
positive_gonorrhea_tests_urog_13,
positive_gonorrhea_tests_urog_14,
positive_gonorrhea_tests_urog_15,
positive_gonorrhea_tests_urog_16,
positive_gonorrhea_tests_urog_17,
total_gonorrhea_tests_rectal_10,
total_gonorrhea_tests_rectal_11,
total_gonorrhea_tests_rectal_12,
total_gonorrhea_tests_rectal_13,
total_gonorrhea_tests_rectal_14,
total_gonorrhea_tests_rectal_15,
total_gonorrhea_tests_rectal_16,
total_gonorrhea_tests_rectal_17,
positive_gonorrhea_tests_rectal_10,
positive_gonorrhea_tests_rectal_11,
positive_gonorrhea_tests_rectal_12,
positive_gonorrhea_tests_rectal_13,
positive_gonorrhea_tests_rectal_14,
positive_gonorrhea_tests_rectal_15,
positive_gonorrhea_tests_rectal_16,
positive_gonorrhea_tests_rectal_17,
total_gonorrhea_tests_throat_10,
total_gonorrhea_tests_throat_11,
total_gonorrhea_tests_throat_12,
total_gonorrhea_tests_throat_13,
total_gonorrhea_tests_throat_14,
total_gonorrhea_tests_throat_15,
total_gonorrhea_tests_throat_16,
total_gonorrhea_tests_throat_17,
positive_gonorrhea_tests_throat_10,
positive_gonorrhea_tests_throat_11,
positive_gonorrhea_tests_throat_12,
positive_gonorrhea_tests_throat_13,
positive_gonorrhea_tests_throat_14,
positive_gonorrhea_tests_throat_15,
positive_gonorrhea_tests_throat_16,
positive_gonorrhea_tests_throat_17,
total_gonorrhea_tests_other_10,
total_gonorrhea_tests_other_11,
total_gonorrhea_tests_other_12,
total_gonorrhea_tests_other_13,
total_gonorrhea_tests_other_14,
total_gonorrhea_tests_other_15,
total_gonorrhea_tests_other_16,
total_gonorrhea_tests_other_17,
positive_gonorrhea_tests_other_10,
positive_gonorrhea_tests_other_11,
positive_gonorrhea_tests_other_12,
positive_gonorrhea_tests_other_13,
positive_gonorrhea_tests_other_14,
positive_gonorrhea_tests_other_15,
positive_gonorrhea_tests_other_16,
positive_gonorrhea_tests_other_17,
total_gonorrhea_tests_missing_10,
total_gonorrhea_tests_missing_11,
total_gonorrhea_tests_missing_12,
total_gonorrhea_tests_missing_13,
total_gonorrhea_tests_missing_14,
total_gonorrhea_tests_missing_15,
total_gonorrhea_tests_missing_16,
total_gonorrhea_tests_missing_17,
positive_gonorrhea_tests_missing_10,
positive_gonorrhea_tests_missing_11,
positive_gonorrhea_tests_missing_12,
positive_gonorrhea_tests_missing_13,
positive_gonorrhea_tests_missing_14,
positive_gonorrhea_tests_missing_15,
positive_gonorrhea_tests_missing_16,
positive_gonorrhea_tests_missing_17,
total_chlamydia_tests_10,
total_chlamydia_tests_11,
total_chlamydia_tests_12,
total_chlamydia_tests_13,
total_chlamydia_tests_14,
total_chlamydia_tests_15,
total_chlamydia_tests_16,
total_chlamydia_tests_17,
positive_chlamydia_tests_10,
positive_chlamydia_tests_11,
positive_chlamydia_tests_12,
positive_chlamydia_tests_13,
positive_chlamydia_tests_14,
positive_chlamydia_tests_15,
positive_chlamydia_tests_16,
positive_chlamydia_tests_17,
chlamydia_per_esp_10,
chlamydia_per_esp_11,
chlamydia_per_esp_12,
chlamydia_per_esp_13,
chlamydia_per_esp_14,
chlamydia_per_esp_15,
chlamydia_per_esp_16,
chlamydia_per_esp_17,
total_syphilis_tests_10,
total_syphilis_tests_11,
total_syphilis_tests_12,
total_syphilis_tests_13,
total_syphilis_tests_14,
total_syphilis_tests_15,
total_syphilis_tests_16,
total_syphilis_tests_17,
syphilis_per_esp_10,
syphilis_per_esp_11,
syphilis_per_esp_12,
syphilis_per_esp_13,
syphilis_per_esp_14,
syphilis_per_esp_15,
syphilis_per_esp_16,
syphilis_per_esp_17,
anal_cytology_test_ever,
hiv_per_esp_spec,
hiv_neg_truvada_10,
hiv_neg_truvada_11,
hiv_neg_truvada_12,
hiv_neg_truvada_13,
hiv_neg_truvada_14,
hiv_neg_truvada_15,
hiv_neg_truvada_16,
hiv_neg_truvada_17,
abnormal_anal_cytology_10,
abnormal_anal_cytology_11,
abnormal_anal_cytology_12,
abnormal_anal_cytology_13,
abnormal_anal_cytology_14,
abnormal_anal_cytology_15,
abnormal_anal_cytology_16,
abnormal_anal_cytology_17,
urethral_discharge_10,
urethral_discharge_11,
urethral_discharge_12,
urethral_discharge_13,
urethral_discharge_14,
urethral_discharge_15,
urethral_discharge_16,
urethral_discharge_17,
urethritis_10,
urethritis_11,
urethritis_12,
urethritis_13,
urethritis_14,
urethritis_15,
urethritis_16,
urethritis_17,
vaginitis_10,
vaginitis_11,
vaginitis_12,
vaginitis_13,
vaginitis_14,
vaginitis_15,
vaginitis_16,
vaginitis_17,
cervicitis_10,
cervicitis_11,
cervicitis_12,
cervicitis_13,
cervicitis_14,
cervicitis_15,
cervicitis_16,
cervicitis_17,
vaginal_leucorrhea_10,
vaginal_leucorrhea_11,
vaginal_leucorrhea_12,
vaginal_leucorrhea_13,
vaginal_leucorrhea_14,
vaginal_leucorrhea_15,
vaginal_leucorrhea_16,
vaginal_leucorrhea_17,
chlamydia_dx_10,
chlamydia_dx_11,
chlamydia_dx_12,
chlamydia_dx_13,
chlamydia_dx_14,
chlamydia_dx_15,
chlamydia_dx_16,
chlamydia_dx_17,
screening_for_stis_10,
screening_for_stis_11,
screening_for_stis_12,
screening_for_stis_13,
screening_for_stis_14,
screening_for_stis_15,
screening_for_stis_16,
screening_for_stis_17,
cont_or_exp_to_vd_10,
cont_or_exp_to_vd_11,
cont_or_exp_to_vd_12,
cont_or_exp_to_vd_13,
cont_or_exp_to_vd_14,
cont_or_exp_to_vd_15,
cont_or_exp_to_vd_16,
cont_or_exp_to_vd_17,
high_risk_sexual_behavior_10,
high_risk_sexual_behavior_11,
high_risk_sexual_behavior_12,
high_risk_sexual_behavior_13,
high_risk_sexual_behavior_14,
high_risk_sexual_behavior_15,
high_risk_sexual_behavior_16,
high_risk_sexual_behavior_17,
hiv_counseling_10,
hiv_counseling_11,
hiv_counseling_12,
hiv_counseling_13,
hiv_counseling_14,
hiv_counseling_15,
hiv_counseling_16,
hiv_counseling_17
FROM mgtp_output_with_patient T1
LEFT JOIN mgtp_ouput_pat_and_enc_counts T2
	ON T1.patient_id = T2.patient_id;
	
	

-- CREATE TABLE mgtp_report_final_output_agetestpat_filter AS
-- SELECT * FROM mgtp_report_final_output
-- WHERE age >= 15 
-- AND master_patient_id not in 
-- (4856539,
-- 2388755,
-- 20021289,
-- 2622639,
-- 7393799,
-- 7405902,
-- 19086,
-- 2245972,
-- 9345414,
-- 1121717,
-- 1120095,
-- 5092053,
-- 1119941,
-- 15810936,
-- 2801547,
-- 7395105,
-- 6808599,
-- 467766,
-- 465618,
-- 9345404,
-- 24120559,
-- 20257460,
-- 6840308,
-- 467872,
-- 4512833,
-- 454136,
-- 8429720,
-- 8000774,
-- 377556,
-- 456070,
-- 8012787,
-- 7441382,
-- 7997968,
-- 3268327,
-- 3268319,
-- 3268323,
-- 8726972,
-- 9645683,
-- 456988,
-- 7395153,
-- 4476409,
-- 1400172,
-- 269278,
-- 5044824,
-- 5092208,
-- 269284,
-- 269622,
-- 269290,
-- 5689686,
-- 5651871,
-- 269282,
-- 269276,
-- 5042048,
-- 6272750,
-- 299112,
-- 269286,
-- 269288,
-- 5044808,
-- 291608,
-- 2733840,
-- 269280,
-- 4476411,
-- 8425284,
-- 7394534,
-- 2733842,
-- 5092285,
-- 7394532,
-- 3906359,
-- 1629211,
-- 7394546,
-- 7394548,
-- 5638755,
-- 3069021,
-- 7394544,
-- 16697245,
-- 16697270,
-- 16700804,
-- 16803981);

--
-- Script shutdown section 
--

-- DROP TABLE IF EXISTS mgtp_hef_w_lab_details CASCADE;
-- DROP TABLE IF EXISTS mgtp_cases_of_interest CASCADE;
-- DROP TABLE IF EXISTS mgtp_gon_events CASCADE;
-- DROP TABLE IF EXISTS mgtp_chlam_events CASCADE;
-- DROP TABLE IF EXISTS mgtp_syph_events CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_labs CASCADE;
-- DROP TABLE IF EXISTS mgtp_gon_counts CASCADE;
-- DROP TABLE IF EXISTS mgtp_chlam_counts CASCADE;
-- DROP TABLE IF EXISTS mgtp_syph_counts CASCADE;
-- DROP TABLE IF EXISTS mgtp_syph_cases CASCADE;
-- DROP TABLE IF EXISTS mgtp_chlam_cases CASCADE;
-- DROP TABLE IF EXISTS mgtp_gon_cases CASCADE;
-- DROP TABLE IF EXISTS mgtp_hepb_pos_dna CASCADE;
-- DROP TABLE IF EXISTS mgtp_cpts_of_interest CASCADE;
-- DROP TABLE IF EXISTS mgtp_dx_codes CASCADE;
-- DROP TABLE IF EXISTS mgtp_diag_fields_by_year CASCADE;
-- DROP TABLE IF EXISTS mgtp_diag_fields CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_counts CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_cases CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_meds CASCADE;
-- DROP TABLE IF EXISTS mgtp_truvada_array CASCADE;
-- DROP TABLE IF EXISTS mgtp_truvada_2mogap CASCADE;
-- DROP TABLE IF EXISTS mgtp_truvada_counts CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_all_details CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_all_details_values CASCADE;
-- DROP TABLE IF EXISTS mgtp_index_patients CASCADE;
-- DROP TABLE IF EXISTS mgtp_output_part_1 CASCADE;
-- DROP TABLE IF EXISTS mgtp_output_with_patient CASCADE;
-- DROP TABLE IF EXISTS mgtp_output_pat_and_enc CASCADE;
-- DROP TABLE IF EXISTS mgtp_ouput_pat_and_enc_counts CASCADE;

