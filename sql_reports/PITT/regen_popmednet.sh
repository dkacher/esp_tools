#!/bin/bash

#This will run the POPMEDNET regeneration scripts.  
#Modify the following script variables as appropriate.
#This is the name of the esp database
ESPDB=esp
#This is the name of the esp_mdphnet schema owner (authorized user)
ESPUSR=esp_popmednet
#This is the path to the sql script
scriptpath=/srv/esp/esp_tools/sql_reports/PITT
logpath=/srv/esp/logs
runtime=`date -u +%Y%m%dT%H%M%SZ`
#The rest of the file doesn't need to be modified
#psql -d $ESPDB -U $ESPUSR -f $scriptpath/generate_popmednet_functions.psql && \
#psql -d $ESPDB -U $ESPUSR -f $scriptpath/regen_popmednet.pg.sql -v ON_ERROR_STOP=1 > $logpath/regen_popmednet.log.$runtime 2>&1
psql -d $ESPDB -U $ESPUSR -f $scriptpath/regen_popmednet.pg.sql -v ON_ERROR_STOP=1 > $logpath/regen_popmednet.log.$runtime 2>&1

