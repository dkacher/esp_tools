#!/bin/bash
LOGS=/srv/esp/logs
RUNTIME=`date -u +%Y-%m-%dT%H:%M:%S`
LOGFILE=$LOGS/opioid_weekly.log.$RUNTIME
OPIOID_SURV_SCRIPT=/srv/esp/esp_tools/opioid_popmednet_surveil.sql
RECIPIENTS="jmiller@commoninf.com"
SUBJECT="Missing opioid and benzo RX Check"
FROM="opioid-rx-check@pitt.edu"


exec 5>&1 6>&2 >>$LOGFILE 2>&1

SURV_REPORT_UPDATE=$(psql -Uesp esp -f $OPIOID_SURV_SCRIPT)

OPIOID_RX=$(psql -d esp -c "select distinct name from emr_prescription
where (name ilike '%Codeine%' or name ilike '%Tylenol #3%'
     or name ilike '%Fentanyl%' or name ilike '%Duragesic%' or name ilike '%Oralet%' or name ilike '%Actiq%' or name ilike '%Sublimaze%' or name ilike '%Innovar%'
     or name ilike '%Hydromorphone%' or name ilike '%Dilaudid%'
     or name ilike '%Meperidine%' or name ilike '%Demerol%'
     or name ilike '%Morphine%' or name ilike '%MS Contin%' or name ilike '%Roxanol%' or name ilike '%Oramorph%' or name ilike '%RMS%' or name ilike '%MSIR%' or name ilike '%Avinza%' or name ilike '%Kadian%'
     or name ilike '%Oxycodone%' or name ilike '%OxyContin%' or name ilike '%Percocet%' or name ilike '%Endocet%' or name ilike '%Roxicodone' or name ilike '%Roxicet%' or name ilike '%Percodan%'
     or name ilike '%Levorphanol%'
     or name ilike 'Opium%' or name ilike '%\-Opium%' or name ilike '%\(Opium%%'
     or name ilike '%Methadone%' or name ilike '%Dolophine%'
     or name ilike '%Hydrocodone%' or name ilike '%Dihydrocodeinone%' or name ilike '%Vicodin%' or name ilike '%Lortab%' or name ilike '%Lorcet%' or name ilike '%Hycomine%' or name ilike '%Vicoprofen%' or name ilike '%Norco%'
     or name ilike '%Oxymorphone%' or name ilike '%Numorphan%'
     or name ilike '%Pentazocine%' or name ilike '%Talwin%' or name ilike '%Talacen%'
     or name ilike '%Butorphanol%' or name ilike '%Stadol%')  
and name not in(select name from static_rx_lookup)
and name not in('RAISED TOILET SEAT/LOCK & ARMS MISC');" -t)

BENZO_RX=$(psql -d esp -c "select distinct name from emr_prescription
 where (name ilike '%Alprazolam%' or name ilike '%Xanax%' or name ilike '%Niravam%'
     or name ilike '%Chlordiazepoxide%' or name ilike '%Librium%' 
     or name ilike '%Clonazepam%' or name ilike '%Klonopin%' or name ilike '%Clonapam%' 
     or name ilike '%Clorazepate%' or name ilike '%Tranxene%' 
     or name ilike '%Diazepam%' or name ilike '%Valium%' or name ilike '%Diastat%' or name ilike '%E Pam%' or name ilike '%Diazemuls%'
     or name ilike '%Estazolam%' or name ilike '%ProSom%' 
     or name ilike '%Flurazepam%' or name ilike '%Dalmane%' 
     or name ilike '%Lorazepam%' or name ilike '%Ativan%'
     or name ilike '%Oxazepam%' or name ilike '%Serax%' 
     or name ilike '%Prazepam%' or name ilike '%Centrax%' 
     or name ilike '%Quazepam%' or name ilike '%Doral%' or name ilike '%Dormalin%'
     or name ilike '%Temazepam%' or name ilike '%Restoril%'
     or name ilike '%Triazolam%' or name ilike '%Halcion%')
 and name not in(select name from static_rx_lookup)
 and name not ilike '%HYDROXYZINE PAMOATE%';" -t)


/usr/bin/sendmail -t  <<EOF
to:$RECIPIENTS
subject:$SUBJECT
from:$FROM
MISSING OPIOIDS
--------------
"${OPIOID_RX}"

MISSING BENZOS
--------------
"${BENZO_RX}"
EOF

