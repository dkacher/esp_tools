

--Jan 2016
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 1/1/2016
drop table if exists temp_hivreport_haslabtestresults_01012016;
create table temp_hivreport_haslabtestresults_01012016 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-JAN-2015' and date < '01-JAN-2016';
select count(*) from temp_hivreport_haslabtestresults_01012016;
--23888

--Jan 2016
-- 2. Create the table of patients who have HIV prior to 1/1/2016
drop table if exists temp_hivreport_hashivcase_01012016;
create table temp_hivreport_hashivcase_01012016 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-JAN-2016';
select count(*) from temp_hivreport_hashivcase_01012016;
--3873

--Jan 2016-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 1/1/2016
-- But are NOT in the list of patients who have HIV results 12 months prior to 1/1/2016
-- and are NOT in the list of patients who have HIV before 1/1/2016
drop table if exists temp_hiv_report2_firstsubsetpats_01012016;
create table temp_hiv_report2_firstsubsetpats_01012016 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-JAN-2015'    
and emr_encounter.date < '01-JAN-2016'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_01012016) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_01012016)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_01012016;
--309,614

--Jan 2016
--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_01012016;
create table temp_hiv_report2_masterpats_01012016 as select temp_hiv_report2_firstsubsetpats_01012016.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_01012016, emr_encounter
where temp_hiv_report2_firstsubsetpats_01012016.id = emr_encounter.patient_id
and emr_encounter.date >= '01-JAN-2016' 
and emr_encounter.date < '01-Feb-2016'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_01012016;
--95,332

--Jan 2016
--4.  BUILD COLUMN 1 2016
drop table if exists temp_hiv_report2_col1_01012016;
create table temp_hiv_report2_col1_01012016 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_01012016
group by center_id, dategroup order by center_id, dategroup;

 

--Jan 2016
-- 5. Prep COLUMN 2 01012016
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_01012016;
create table temp_hiv_report2_col2_masterpats_01012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_01012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_01012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JAN-2016'   
and emr_labresult.date < '01-FEB-2016'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_01012016;
--1326

--Jan 2016
--6. Column 2 Report 2 0102016 
drop table if exists temp_hiv_report2_col2_01012016;
create table temp_hiv_report2_col2_01012016 
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_01012016
group by center_id, dategroup;


--Jan 2016
--7. Prep Column 3 01012016
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_01012016;
create table temp_hiv_report2_col3_haselisaagab_01012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_01012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_01012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JAN-2016'   
and emr_labresult.date < '01-FEB-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_01012016;
--1309


--Jan 2016
--8. Create master table for column 3 01012016
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_01012016;
create table temp_hiv_report2_col3_masterpats_01012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_01012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_01012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JAN-2016'   
and emr_labresult.date < '01-FEB-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_01012016);
select count(*) from temp_hiv_report2_col3_masterpats_01012016;
--80


--Jan 2016-- 9. Column 3 Report 2 For Jan 2016
drop table if exists temp_hiv_report2_col3_01012016;
create table temp_hiv_report2_col3_01012016 
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_01012016
group by center_id, dategroup;


--Jan 2016
--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_01012016;
create table temp_hiv_report2_col4_masterpats_01012016 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_01012016, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_01012016.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-JAN-2016'
and hef_event.date < '01-FEB-2016';
select count(*) from temp_hiv_report2_col4_masterpats_01012016;
--7


--Jan 2016-- 11. Column 4 Report 2 for Jan 2016
drop table if exists temp_hiv_report2_col4_01012016;
create table temp_hiv_report2_col4_01012016 
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_01012016
group by center_id, dategroup;


--Jan 2016
---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--Jan 2016
--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_01012016
where temp_hiv_report2_masterpats_01012016.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-JAN-2016'
and hef_event.date < '31-JAN-2016'::date + interval '60 days';

--Jan 2016
--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_01012016
where temp_hiv_report2_masterpats_01012016.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-JAN-2016'
and hef_event.date < '31-JAN-2016'::date + interval '60 days';


--Jan 2016
--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_01012016, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_01012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JAN-2016'
and emr_labresult.date < '31-JAN-2016'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
--Jan 2016
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_01012016
where temp_hiv_report2_masterpats_01012016.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-JAN-2016'
and hef_event.date < '31-JAN-2016'::date + interval '60 days';


--Jan 2016--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_01012016
where temp_hiv_report2_masterpats_01012016.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-JAN-2016'
and hef_event.date < '31-JAN-2016'::date + interval '60 days';


--Jan 2016
--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS AGAB
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--Jan 2016
--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--Jan 2016
--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--Jan 2016
--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--Jan 2016
--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_01012016;
create table temp_hiv_report2_col5_source_01012016 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--Jan 2016
--22.  Refine column 5 data to get minimum/first date
--Make sure the dates are within 60 days of each other
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_01012016
where datea-dateb<=60;

--Jan 2016
--23  Put into final form
drop table temp_hiv_report2_col5_01012016;
create table temp_hiv_report2_col5_01012016 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-JAN-2016'
group by center_id, dategroup;


--REPORT 2:
drop table if exists temp_hiv_report2_01012016;
create table temp_hiv_report2_01012016 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_01012016 a
left join temp_hiv_report2_col2_01012016 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_01012016 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_01012016 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_01012016 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

---------------------------------------------------------------------------

--Feb 2016
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 2/1/2016
drop table if exists temp_hivreport_haslabtestresults_02012016;
create table temp_hivreport_haslabtestresults_02012016 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-FEB-2015' and date < '01-FEB-2016';
select count(*) from temp_hivreport_haslabtestresults_02012016;
--24822

-- 2. Create the table of patients who have HIV prior to 2/1/2016
drop table if exists temp_hivreport_hashivcase_02012016;
create table temp_hivreport_hashivcase_02012016 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-FEB-2016';
select count(*) from temp_hivreport_hashivcase_02012016;
--3910

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 2/1/2016
-- But are NOT in the list of patients who have HIV results 12 months prior to 2/1/2016
-- and are NOT in the list of patients who have HIV before 2/1/2016
drop table if exists temp_hiv_report2_firstsubsetpats_02012016;
create table temp_hiv_report2_firstsubsetpats_02012016 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-FEB-2015'    
and emr_encounter.date < '01-FEB-2016'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_02012016) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_02012016)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_02012016;
--303,363

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--FEB 2016
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_02012016;
create table temp_hiv_report2_masterpats_02012016 as 
select temp_hiv_report2_firstsubsetpats_02012016.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_02012016, emr_encounter
where temp_hiv_report2_firstsubsetpats_02012016.id = emr_encounter.patient_id
and emr_encounter.date >= '01-FEB-2016' 
and emr_encounter.date < '01-MAR-2016'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_02012016;
--93,799

--4.  BUILD COLUMN 1 Feb 2016
drop table if exists temp_hiv_report2_col1_02012016;
create table temp_hiv_report2_col1_02012016 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_02012016
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 02012016
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_02012016;
create table temp_hiv_report2_col2_masterpats_02012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_02012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_02012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-FEB-2016'   
and emr_labresult.date < '01-MAR-2016'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_02012016;
--1488

--6. Column 2 Report 2 0202016 
drop table if exists temp_hiv_report2_col2_02012016;
create table temp_hiv_report2_col2_02012016
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_02012016
group by center_id, dategroup;

--7. Prep Column 3 02012016
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_02012016;
create table temp_hiv_report2_col3_haselisaagab_02012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_02012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_02012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-FEB-2016'   
and emr_labresult.date < '01-MAR-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_02012016;
--1475


--8. Create master table for column 3 02012016
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_02012016;
create table temp_hiv_report2_col3_masterpats_02012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_02012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_02012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-FEB-2016'   
and emr_labresult.date < '01-MAR-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_02012016);
select count(*) from temp_hiv_report2_col3_masterpats_02012016;
--84

-- 9. Column 3 Report 2 For Feb 2016
drop table if exists temp_hiv_report2_col3_02012016;
create table temp_hiv_report2_col3_02012016
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_02012016
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_02012016;
create table temp_hiv_report2_col4_masterpats_02012016 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_02012016, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_02012016.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-FEB-2016'
and hef_event.date < '01-MAR-2016';
select count(*) from temp_hiv_report2_col4_masterpats_02012016;
--100

-- 11. Column 4 Report 2 for Feb 2016
drop table if exists temp_hiv_report2_col4_02012016;
create table temp_hiv_report2_col4_02012016
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_02012016
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_02012016
where temp_hiv_report2_masterpats_02012016.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-FEB-2016'
and hef_event.date < '29-FEB-2016'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_02012016
where temp_hiv_report2_masterpats_02012016.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-FEB-2016'
and hef_event.date < '29-FEB-2016'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_02012016, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_02012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-FEB-2016'
and emr_labresult.date < '29-FEB-2016'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_02012016
where temp_hiv_report2_masterpats_02012016.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-FEB-2016'
and hef_event.date < '29-FEB-2016'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_02012016
where temp_hiv_report2_masterpats_02012016.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-FEB-2016'
and hef_event.date < '29-FEB-2016'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_02012016;
create table temp_hiv_report2_col5_source_02012016 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_02012016
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_02012016;
create table temp_hiv_report2_col5_02012016 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-FEB-2016'
group by center_id, dategroup;


--REPORT 2:  Feb 2016
drop table if exists temp_hiv_report2_02012016;
create table temp_hiv_report2_02012016 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_02012016 a
left join temp_hiv_report2_col2_02012016 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_02012016 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_02012016 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_02012016 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

---------------------------------------------------------------------------

--MAR 2016
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 3/1/2016
drop table if exists temp_hivreport_haslabtestresults_03012016;
create table temp_hivreport_haslabtestresults_03012016 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-MAR-2015' and date < '01-MAR-2016';
select count(*) from temp_hivreport_haslabtestresults_03012016;
--26106

-- 2. Create the table of patients who have HIV prior to 3/1/2016
drop table if exists temp_hivreport_hashivcase_03012016;
create table temp_hivreport_hashivcase_03012016 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-MAR-2016';
select count(*) from temp_hivreport_hashivcase_03012016;
--3938

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 3/1/2016
-- But are NOT in the list of patients who have HIV results 12 months prior to 3/1/2016
-- and are NOT in the list of patients who have HIV before 3/1/2016
drop table if exists temp_hiv_report2_firstsubsetpats_03012016;
create table temp_hiv_report2_firstsubsetpats_03012016 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-MAR-2015'    
and emr_encounter.date < '01-MAR-2016'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_03012016) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_03012016)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_03012016;
--304,481

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--MAR 2016
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_03012016;
create table temp_hiv_report2_masterpats_03012016 as 
select temp_hiv_report2_firstsubsetpats_03012016.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_03012016, emr_encounter
where temp_hiv_report2_firstsubsetpats_03012016.id = emr_encounter.patient_id
and emr_encounter.date >= '01-MAR-2016' 
and emr_encounter.date < '01-APR-2016'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_03012016;
--109,497

--4.  BUILD COLUMN 1 MAR 2016
drop table if exists temp_hiv_report2_col1_03012016;
create table temp_hiv_report2_col1_03012016 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_03012016
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 03012016
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_03012016;
create table temp_hiv_report2_col2_masterpats_03012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_03012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_03012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAR-2016'   
and emr_labresult.date < '01-APR-2016'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_03012016;
--1565

--6. Column 2 Report 2 0302016 
drop table if exists temp_hiv_report2_col2_03012016;
create table temp_hiv_report2_col2_03012016
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_03012016
group by center_id, dategroup;

--7. Prep Column 3 03012016
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_03012016;
create table temp_hiv_report2_col3_haselisaagab_03012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_03012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_03012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAR-2016'   
and emr_labresult.date < '01-APR-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_03012016;
--1550


--8. Create master table for column 3 03012016
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_03012016;
create table temp_hiv_report2_col3_masterpats_03012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_03012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_03012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAR-2016'   
and emr_labresult.date < '01-APR-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_03012016);
select count(*) from temp_hiv_report2_col3_masterpats_03012016;
--67

-- 9. Column 3 Report 2 For MAR 2016
drop table if exists temp_hiv_report2_col3_03012016;
create table temp_hiv_report2_col3_03012016
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_03012016
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_03012016;
create table temp_hiv_report2_col4_masterpats_03012016 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_03012016, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_03012016.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-MAR-2016'
and hef_event.date < '01-APR-2016';
select count(*) from temp_hiv_report2_col4_masterpats_03012016;
--48

-- 11. Column 4 Report 2 for MAR 2016
drop table if exists temp_hiv_report2_col4_03012016;
create table temp_hiv_report2_col4_03012016
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_03012016
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_03012016
where temp_hiv_report2_masterpats_03012016.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-MAR-2016'
and hef_event.date < '31-MAR-2016'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_03012016
where temp_hiv_report2_masterpats_03012016.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-MAR-2016'
and hef_event.date < '31-MAR-2016'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_03012016, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_03012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAR-2016'
and emr_labresult.date < '31-MAR-2016'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_03012016
where temp_hiv_report2_masterpats_03012016.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-MAR-2016'
and hef_event.date < '31-MAR-2016'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_03012016
where temp_hiv_report2_masterpats_03012016.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-MAR-2016'
and hef_event.date < '31-MAR-2016'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_03012016;
create table temp_hiv_report2_col5_source_03012016 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_03012016
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_03012016;
create table temp_hiv_report2_col5_03012016 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-MAR-2016'
group by center_id, dategroup;


--REPORT 2:  MAR 2016
drop table if exists temp_hiv_report2_03012016;
create table temp_hiv_report2_03012016 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_03012016 a
left join temp_hiv_report2_col2_03012016 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_03012016 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_03012016 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_03012016 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------

--APRIL 2016
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 4/1/2016
drop table if exists temp_hivreport_haslabtestresults_04012016;
create table temp_hivreport_haslabtestresults_04012016 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-APR-2015' and date < '01-APR-2016';
select count(*) from temp_hivreport_haslabtestresults_04012016;
--27344

-- 2. Create the table of patients who have HIV prior to 4/1/2016
drop table if exists temp_hivreport_hashivcase_04012016;
create table temp_hivreport_hashivcase_04012016 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-APR-2016';
select count(*) from temp_hivreport_hashivcase_04012016;
--3968

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 4/1/2016
-- But are NOT in the list of patients who have HIV results 12 months prior to 4/1/2016
-- and are NOT in the list of patients who have HIV before 4/1/2016
drop table if exists temp_hiv_report2_firstsubsetpats_04012016;
create table temp_hiv_report2_firstsubsetpats_04012016 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-APR-2015'    
and emr_encounter.date < '01-APR-2016'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_04012016) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_04012016)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_04012016;
--298280

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--APR 2016
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_04012016;
create table temp_hiv_report2_masterpats_04012016 as 
select temp_hiv_report2_firstsubsetpats_04012016.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_04012016, emr_encounter
where temp_hiv_report2_firstsubsetpats_04012016.id = emr_encounter.patient_id
and emr_encounter.date >= '01-APR-2016' 
and emr_encounter.date < '01-MAY-2016'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_04012016;
--96,203

--4.  BUILD COLUMN 1 APR 2016
drop table if exists temp_hiv_report2_col1_04012016;
create table temp_hiv_report2_col1_04012016 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_04012016
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 04012016
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_04012016;
create table temp_hiv_report2_col2_masterpats_04012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_04012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_04012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-APR-2016'   
and emr_labresult.date < '01-MAY-2016'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_04012016;
--1358

--6. Column 2 Report 2 0302016 
drop table if exists temp_hiv_report2_col2_04012016;
create table temp_hiv_report2_col2_04012016
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_04012016
group by center_id, dategroup;

--7. Prep Column 3 04012016
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_04012016;
create table temp_hiv_report2_col3_haselisaagab_04012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_04012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_04012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-APR-2016'   
and emr_labresult.date < '01-MAY-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_04012016;
--1339

--8. Create master table for column 3 04012016
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_04012016;
create table temp_hiv_report2_col3_masterpats_04012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_04012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_04012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-APR-2016'   
and emr_labresult.date < '01-MAY-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_04012016);
select count(*) from temp_hiv_report2_col3_masterpats_04012016;
--90

-- 9. Column 3 Report 2 For APR 2016
drop table if exists temp_hiv_report2_col3_04012016;
create table temp_hiv_report2_col3_04012016
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_04012016
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_04012016;
create table temp_hiv_report2_col4_masterpats_04012016 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_04012016, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_04012016.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-APR-2016'
and hef_event.date < '01-MAY-2016';
select count(*) from temp_hiv_report2_col4_masterpats_04012016;
--45

-- 11. Column 4 Report 2 for APR 2016
drop table if exists temp_hiv_report2_col4_04012016;
create table temp_hiv_report2_col4_04012016
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_04012016
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_04012016
where temp_hiv_report2_masterpats_04012016.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-APR-2016'
and hef_event.date < '30-APR-2016'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_04012016
where temp_hiv_report2_masterpats_04012016.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-APR-2016'
and hef_event.date < '30-APR-2016'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_04012016, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_04012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-APR-2016'
and emr_labresult.date < '30-APR-2016'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_04012016
where temp_hiv_report2_masterpats_04012016.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-APR-2016'
and hef_event.date < '30-APR-2016'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_04012016
where temp_hiv_report2_masterpats_04012016.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-APR-2016'
and hef_event.date < '30-APR-2016'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_04012016;
create table temp_hiv_report2_col5_source_04012016 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_04012016
where datea-dateb<=60;

--23  Put into final form
drop table if exists temp_hiv_report2_col5_04012016;
create table temp_hiv_report2_col5_04012016 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-APR-2016'
group by center_id, dategroup;


--REPORT 2:  APR 2016
drop table if exists temp_hiv_report2_04012016;
create table temp_hiv_report2_04012016 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_04012016 a
left join temp_hiv_report2_col2_04012016 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_04012016 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_04012016 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_04012016 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------



--MAY 2016
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 5/1/2016
drop table if exists temp_hivreport_haslabtestresults_05012016;
create table temp_hivreport_haslabtestresults_05012016 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-MAY-2015' and date < '01-MAY-2016';
select count(*) from temp_hivreport_haslabtestresults_05012016;
--28281

-- 2. Create the table of patients who have HIV prior to 5/1/2016
drop table if exists temp_hivreport_hashivcase_05012016;
create table temp_hivreport_hashivcase_05012016 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-MAY-2016';
select count(*) from temp_hivreport_hashivcase_05012016;
--3997

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 5/1/2016
-- But are NOT in the list of patients who have HIV results 12 months prior to 5/1/2016
-- and are NOT in the list of patients who have HIV before /1/2016
drop table if exists temp_hiv_report2_firstsubsetpats_05012016;
create table temp_hiv_report2_firstsubsetpats_05012016 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-MAY-2015'    
and emr_encounter.date < '01-MAY-2016'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_05012016) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_05012016)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_05012016;
--291,344

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--MAY 2016
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_05012016;
create table temp_hiv_report2_masterpats_05012016 as 
select temp_hiv_report2_firstsubsetpats_05012016.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_05012016, emr_encounter
where temp_hiv_report2_firstsubsetpats_05012016.id = emr_encounter.patient_id
and emr_encounter.date >= '01-MAY-2016' 
and emr_encounter.date < '01-JUN-2016'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_05012016;
--93,313

--4.  BUILD COLUMN 1 MAY 2016
drop table if exists temp_hiv_report2_col1_05012016;
create table temp_hiv_report2_col1_05012016 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_05012016
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 05012016
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_05012016;
create table temp_hiv_report2_col2_masterpats_05012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_05012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_05012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAY-2016'   
and emr_labresult.date < '01-JUN-2016'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_05012016;
--1467

--6. Column 2 Report 2 0302016 
drop table if exists temp_hiv_report2_col2_05012016;
create table temp_hiv_report2_col2_05012016
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_05012016
group by center_id, dategroup;

--7. Prep Column 3 05012016
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_05012016;
create table temp_hiv_report2_col3_haselisaagab_05012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_05012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_05012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAY-2016'   
and emr_labresult.date < '01-JUN-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_05012016;
--1455


--8. Create master table for column 3 05012016
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_05012016;
create table temp_hiv_report2_col3_masterpats_05012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_05012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_05012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAY-2016'   
and emr_labresult.date < '01-JUN-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_05012016);
select count(*) from temp_hiv_report2_col3_masterpats_05012016;
--74

-- 9. Column 3 Report 2 For MAY 2016
drop table if exists temp_hiv_report2_col3_05012016;
create table temp_hiv_report2_col3_05012016
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_05012016
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_05012016;
create table temp_hiv_report2_col4_masterpats_05012016 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_05012016, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_05012016.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-MAY-2016'
and hef_event.date < '01-JUN-2016';
select count(*) from temp_hiv_report2_col4_masterpats_05012016;
--36

-- 11. Column 4 Report 2 for MAY 2016
drop table if exists temp_hiv_report2_col4_05012016;
create table temp_hiv_report2_col4_05012016
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_05012016
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_05012016
where temp_hiv_report2_masterpats_05012016.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-MAY-2016'
and hef_event.date < '31-MAY-2016'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_05012016
where temp_hiv_report2_masterpats_05012016.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-MAY-2016'
and hef_event.date < '31-MAY-2016'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_05012016, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_05012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAY-2016'
and emr_labresult.date < '31-MAY-2016'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_05012016
where temp_hiv_report2_masterpats_05012016.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-MAY-2016'
and hef_event.date < '31-MAY-2016'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_05012016
where temp_hiv_report2_masterpats_05012016.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-MAY-2016'
and hef_event.date < '31-MAY-2016'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_05012016;
create table temp_hiv_report2_col5_source_05012016 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_05012016
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_05012016;
create table temp_hiv_report2_col5_05012016 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-MAY-2016'
group by center_id, dategroup;


--REPORT 2:  MAY 2016
drop table if exists temp_hiv_report2_05012016;
create table temp_hiv_report2_05012016 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_05012016 a
left join temp_hiv_report2_col2_05012016 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_05012016 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_05012016 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_05012016 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------



--JUNE 2016
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 6/1/2016
drop table if exists temp_hivreport_haslabtestresults_06012016;
create table temp_hivreport_haslabtestresults_06012016 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-JUN-2015' and date < '01-JUN-2016';
select count(*) from temp_hivreport_haslabtestresults_06012016;
--29513

-- 2. Create the table of patients who have HIV prior to 6/1/2016
drop table if exists temp_hivreport_hashivcase_06012016;
create table temp_hivreport_hashivcase_06012016 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-JUN-2016';
select count(*) from temp_hivreport_hashivcase_06012016;
--4019

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 6/1/2016
-- But are NOT in the list of patients who have HIV results 12 months prior to 6/1/2016
-- and are NOT in the list of patients who have HIV before 6/1/2016
drop table if exists temp_hiv_report2_firstsubsetpats_06012016;
create table temp_hiv_report2_firstsubsetpats_06012016 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-JUN-2015'    
and emr_encounter.date < '01-JUN-2016'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_06012016) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_06012016)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_06012016;
--285237

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--JUNE 2016
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_06012016;
create table temp_hiv_report2_masterpats_06012016 as 
select temp_hiv_report2_firstsubsetpats_06012016.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_06012016, emr_encounter
where temp_hiv_report2_firstsubsetpats_06012016.id = emr_encounter.patient_id
and emr_encounter.date >= '01-JUN-2016' 
and emr_encounter.date < '01-JUL-2016'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_06012016;
--93,799

--4.  BUILD COLUMN 1 JUN 2016
drop table if exists temp_hiv_report2_col1_06012016;
create table temp_hiv_report2_col1_06012016 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_06012016
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 06012016
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_06012016;
create table temp_hiv_report2_col2_masterpats_06012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_06012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_06012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUN-2016'   
and emr_labresult.date < '01-JUL-2016'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_06012016;
--1488

--6. Column 2 Report 2 06012016 
drop table if exists temp_hiv_report2_col2_06012016;
create table temp_hiv_report2_col2_06012016
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_06012016
group by center_id, dategroup;

--7. Prep Column 3 06012016
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_06012016;
create table temp_hiv_report2_col3_haselisaagab_06012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_06012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_06012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUN-2016'   
and emr_labresult.date < '01-JUL-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_06012016;
--1475


--8. Create master table for column 3 06012016
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_06012016;
create table temp_hiv_report2_col3_masterpats_06012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_06012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_06012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUN-2016'   
and emr_labresult.date < '01-JUL-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_06012016);
select count(*) from temp_hiv_report2_col3_masterpats_06012016;
--84

-- 9. Column 3 Report 2 For JUN 2016
drop table if exists temp_hiv_report2_col3_06012016;
create table temp_hiv_report2_col3_06012016
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_06012016
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_06012016;
create table temp_hiv_report2_col4_masterpats_06012016 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_06012016, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_06012016.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-JUN-2016'
and hef_event.date < '01-JUL-2016';
select count(*) from temp_hiv_report2_col4_masterpats_06012016;
--100

-- 11. Column 4 Report 2 for JUN 2016
drop table if exists temp_hiv_report2_col4_06012016;
create table temp_hiv_report2_col4_06012016
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_06012016
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_06012016
where temp_hiv_report2_masterpats_06012016.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-JUN-2016'
and hef_event.date < '30-JUN-2016'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_06012016
where temp_hiv_report2_masterpats_06012016.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-JUN-2016'
and hef_event.date < '30-JUN-2016'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_06012016, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_06012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUN-2016'
and emr_labresult.date < '30-JUN-2016'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_06012016
where temp_hiv_report2_masterpats_06012016.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-JUN-2016'
and hef_event.date < '30-JUN-2016'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_06012016
where temp_hiv_report2_masterpats_06012016.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-JUN-2016'
and hef_event.date < '30-JUN-2016'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_06012016;
create table temp_hiv_report2_col5_source_06012016 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_06012016
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_06012016;
create table temp_hiv_report2_col5_06012016 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-JUN-2016'
group by center_id, dategroup;


--REPORT 2:  JUN 2016
drop table if exists temp_hiv_report2_06012016;
create table temp_hiv_report2_06012016 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_06012016 a
left join temp_hiv_report2_col2_06012016 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_06012016 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_06012016 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_06012016 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------

--JULY 2016
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 7/1/2016
drop table if exists temp_hivreport_haslabtestresults_07012016;
create table temp_hivreport_haslabtestresults_07012016 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-JUL-2015' and date < '01-JUL-2016';
select count(*) from temp_hivreport_haslabtestresults_07012016;
--28281

-- 2. Create the table of patients who have HIV prior to 7/1/2016
drop table if exists temp_hivreport_hashivcase_07012016;
create table temp_hivreport_hashivcase_07012016 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-JUL-2016';
select count(*) from temp_hivreport_hashivcase_07012016;
--3997

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 7/1/2016
-- But are NOT in the list of patients who have HIV results 12 months prior to 6/1/2016
-- and are NOT in the list of patients who have HIV before 6/1/2016
drop table if exists temp_hiv_report2_firstsubsetpats_07012016;
create table temp_hiv_report2_firstsubsetpats_07012016 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-JUL-2015'    
and emr_encounter.date < '01-AUG-2016'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_07012016) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_07012016)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_07012016;
--303,363

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--JUL 2016
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_07012016;
create table temp_hiv_report2_masterpats_07012016 as 
select temp_hiv_report2_firstsubsetpats_07012016.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_07012016, emr_encounter
where temp_hiv_report2_firstsubsetpats_07012016.id = emr_encounter.patient_id
and emr_encounter.date >= '01-JUL-2016' 
and emr_encounter.date < '01-AUG-2016'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_07012016;
--93,799

--4.  BUILD COLUMN 1 JUL 2016
drop table if exists temp_hiv_report2_col1_07012016;
create table temp_hiv_report2_col1_07012016 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_07012016
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 07012016
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_07012016;
create table temp_hiv_report2_col2_masterpats_07012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_07012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_07012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUL-2016'   
and emr_labresult.date < '01-AUG-2016'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_07012016;
--1488

--6. Column 2 Report 2 07012016 
drop table if exists temp_hiv_report2_col2_07012016;
create table temp_hiv_report2_col2_07012016
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_07012016
group by center_id, dategroup;

--7. Prep Column 3 07012016
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_07012016;
create table temp_hiv_report2_col3_haselisaagab_07012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_07012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_07012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUL-2016'   
and emr_labresult.date < '01-AUG-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_07012016;
--1475


--8. Create master table for column 3 07012016
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_07012016;
create table temp_hiv_report2_col3_masterpats_07012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_07012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_07012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUL-2016'   
and emr_labresult.date < '01-AUG-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_07012016);
select count(*) from temp_hiv_report2_col3_masterpats_07012016;
--84

-- 9. Column 3 Report 2 For JUL 2016
drop table if exists temp_hiv_report2_col3_07012016;
create table temp_hiv_report2_col3_07012016
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_07012016
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_07012016;
create table temp_hiv_report2_col4_masterpats_07012016 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_07012016, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_07012016.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-JUL-2016'
and hef_event.date < '01-AUG-2016';
select count(*) from temp_hiv_report2_col4_masterpats_07012016;
--100

-- 11. Column 4 Report 2 for JUL 2016
drop table if exists temp_hiv_report2_col4_07012016;
create table temp_hiv_report2_col4_07012016
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_07012016
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_07012016
where temp_hiv_report2_masterpats_07012016.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-JUL-2016'
and hef_event.date < '31-JUL-2016'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_07012016
where temp_hiv_report2_masterpats_07012016.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-JUL-2016'
and hef_event.date < '31-JUL-2016'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_07012016, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_07012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUL-2016'
and emr_labresult.date < '31-JUL-2016'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_07012016
where temp_hiv_report2_masterpats_07012016.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-JUL-2016'
and hef_event.date < '31-JUL-2016'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_07012016
where temp_hiv_report2_masterpats_07012016.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-JUL-2016'
and hef_event.date < '31-JUL-2016'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_07012016;
create table temp_hiv_report2_col5_source_07012016 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_07012016
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_07012016;
create table temp_hiv_report2_col5_07012016 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-JUL-2016'
group by center_id, dategroup;


--REPORT 2:  JUL 2016
drop table if exists temp_hiv_report2_07012016;
create table temp_hiv_report2_07012016 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_07012016 a
left join temp_hiv_report2_col2_07012016 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_07012016 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_07012016 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_07012016 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

--AUG 2016
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 8/1/2016
drop table if exists temp_hivreport_haslabtestresults_08012016;
create table temp_hivreport_haslabtestresults_08012016 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-AUG-2015' and date < '01-AUG-2016';
select count(*) from temp_hivreport_haslabtestresults_08012016;
--28281

-- 2. Create the table of patients who have HIV prior to 7/1/2016
drop table if exists temp_hivreport_hashivcase_08012016;
create table temp_hivreport_hashivcase_08012016 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-AUG-2016';
select count(*) from temp_hivreport_hashivcase_08012016;
--3997

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 7/1/2016
-- But are NOT in the list of patients who have HIV results 12 months prior to 6/1/2016
-- and are NOT in the list of patients who have HIV before 6/1/2016
drop table if exists temp_hiv_report2_firstsubsetpats_08012016;
create table temp_hiv_report2_firstsubsetpats_08012016 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-AUG-2015'    
and emr_encounter.date < '01-SEP-2016'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_08012016) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_08012016)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_08012016;
--303,363

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--AUG 2016
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_08012016;
create table temp_hiv_report2_masterpats_08012016 as 
select temp_hiv_report2_firstsubsetpats_08012016.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_08012016, emr_encounter
where temp_hiv_report2_firstsubsetpats_08012016.id = emr_encounter.patient_id
and emr_encounter.date >= '01-AUG-2016' 
and emr_encounter.date < '01-SEP-2016'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_08012016;
--93,799

--4.  BUILD COLUMN 1 AUG 2016
drop table if exists temp_hiv_report2_col1_08012016;
create table temp_hiv_report2_col1_08012016 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_08012016
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 08012016
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_08012016;
create table temp_hiv_report2_col2_masterpats_08012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_08012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_08012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-AUG-2016'   
and emr_labresult.date < '01-SEP-2016'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_08012016;
--1488

--6. Column 2 Report 2 08012016 
drop table if exists temp_hiv_report2_col2_08012016;
create table temp_hiv_report2_col2_08012016
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_08012016
group by center_id, dategroup;

--7. Prep Column 3 08012016
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_08012016;
create table temp_hiv_report2_col3_haselisaagab_08012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_08012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_08012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-AUG-2016'   
and emr_labresult.date < '01-SEP-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_08012016;
--1475


--8. Create master table for column 3 08012016
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_08012016;
create table temp_hiv_report2_col3_masterpats_08012016 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_08012016, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_08012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-AUG-2016'   
and emr_labresult.date < '01-SEP-2016'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_08012016);
select count(*) from temp_hiv_report2_col3_masterpats_08012016;
--84

-- 9. Column 3 Report 2 For AUG 2016
drop table if exists temp_hiv_report2_col3_08012016;
create table temp_hiv_report2_col3_08012016
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_08012016
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_08012016;
create table temp_hiv_report2_col4_masterpats_08012016 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_08012016, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_08012016.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-AUG-2016'
and hef_event.date < '01-SEP-2016';
select count(*) from temp_hiv_report2_col4_masterpats_08012016;
--100

-- 11. Column 4 Report 2 for AUG 2016
drop table if exists temp_hiv_report2_col4_08012016;
create table temp_hiv_report2_col4_08012016
as select count(distinct patid) as numpatswithposelisaoragaborviral, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_08012016
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_08012016
where temp_hiv_report2_masterpats_08012016.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-AUG-2016'
and hef_event.date < '31-AUG-2016'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_08012016
where temp_hiv_report2_masterpats_08012016.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-AUG-2016'
and hef_event.date < '31-AUG-2016'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_08012016, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_08012016.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-AUG-2016'
and emr_labresult.date < '31-AUG-2016'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_08012016
where temp_hiv_report2_masterpats_08012016.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-AUG-2016'
and hef_event.date < '31-AUG-2016'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_08012016
where temp_hiv_report2_masterpats_08012016.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-AUG-2016'
and hef_event.date < '31-AUG-2016'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_08012016;
create table temp_hiv_report2_col5_source_08012016 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_08012016
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_08012016;
create table temp_hiv_report2_col5_08012016 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-AUG-2016'
group by center_id, dategroup;


--REPORT 2:  AUG 2016
drop table if exists temp_hiv_report2_08012016;
create table temp_hiv_report2_08012016 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragaborviral,
confirmedtests
from temp_hiv_report2_col1_08012016 a
left join temp_hiv_report2_col2_08012016 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_08012016 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_08012016 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_08012016 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------


--FINAL UNION
--REPORT 2 all MONTHS
drop table if exists temp_hiv_report2;
create table temp_hiv_report2 as
(select * from temp_hiv_report2_01012016)
union 
(select * from temp_hiv_report2_02012016)
union 
(select * from temp_hiv_report2_03012016)
union
(select * from temp_hiv_report2_04012016)
union
(select * from temp_hiv_report2_05012016)
union
(select * from temp_hiv_report2_06012016)
union
(select * from temp_hiv_report2_07012016)
union
(select * from temp_hiv_report2_08012016)
;
select * from temp_hiv_report2 order by center_id::int, monthyear;



--Clean up
/*
drop table temp_hiv_report2_col1_01012016;
drop table temp_hiv_report2_col2_01012016;
drop table temp_hiv_report2_col3_01012016;
drop table temp_hiv_report2_col4_01012016;
drop table temp_hiv_report2_col5_01012016;
drop table temp_hiv_report2_col1_02012016;
drop table temp_hiv_report2_col2_02012016;
drop table temp_hiv_report2_col3_02012016;
drop table temp_hiv_report2_col4_02012016;
drop table temp_hiv_report2_col5_02012016;
drop table temp_hiv_report2_col1_03012016;
drop table temp_hiv_report2_col2_03012016;
drop table temp_hiv_report2_col3_03012016;
drop table temp_hiv_report2_col4_03012016;
drop table temp_hiv_report2_col5_03012016;
drop table temp_hiv_report2_col1_04012016;
drop table temp_hiv_report2_col2_04012016;
drop table temp_hiv_report2_col3_04012016;
drop table temp_hiv_report2_col4_04012016;
drop table temp_hiv_report2_col5_04012016;
drop table temp_hiv_report2_col1_05012016;
drop table temp_hiv_report2_col2_05012016;
drop table temp_hiv_report2_col3_05012016;
drop table temp_hiv_report2_col4_05012016;
drop table temp_hiv_report2_col5_05012016;
drop table temp_hiv_report2_col1_06012016;
drop table temp_hiv_report2_col2_06012016;
drop table temp_hiv_report2_col3_06012016;
drop table temp_hiv_report2_col4_06012016;
drop table temp_hiv_report2_col5_06012016;
drop table temp_hiv_report2_col1_07012016;
drop table temp_hiv_report2_col2_07012016;
drop table temp_hiv_report2_col3_07012016;
drop table temp_hiv_report2_col4_07012016;
drop table temp_hiv_report2_col5_07012016;
drop table temp_hiv_report2_col1_08012016;
drop table temp_hiv_report2_col2_08012016;
drop table temp_hiv_report2_col3_08012016;
drop table temp_hiv_report2_col4_08012016;
drop table temp_hiv_report2_col5_08012016;

drop table temp_hiv_report2_masterpats_01012016;
drop table temp_hiv_report2_col2_masterpats_01012016;
drop table temp_hiv_report2_col3_masterpats_01012016;
drop table temp_hiv_report2_col3_haselisaagab_01012016;
drop table temp_hiv_report2_col4_masterpats_01012016;

drop table temp_hiv_report2_masterpats_02012016;
drop table temp_hiv_report2_col2_masterpats_02012016;
drop table temp_hiv_report2_col3_masterpats_02012016;
drop table temp_hiv_report2_col3_haselisaagab_02012016;
drop table temp_hiv_report2_col4_masterpats_02012016;

drop table temp_hiv_report2_masterpats_03012016;
drop table temp_hiv_report2_col2_masterpats_03012016;
drop table temp_hiv_report2_col3_masterpats_03012016;
drop table temp_hiv_report2_col3_haselisaagab_03012016;
drop table temp_hiv_report2_col4_masterpats_03012016;

drop table temp_hiv_report2_masterpats_04012016;
drop table temp_hiv_report2_col2_masterpats_04012016;
drop table temp_hiv_report2_col3_masterpats_04012016;
drop table temp_hiv_report2_col3_haselisaagab_04012016;
drop table temp_hiv_report2_col4_masterpats_04012016;

drop table temp_hiv_report2_masterpats_05012016;
drop table temp_hiv_report2_col2_masterpats_05012016;
drop table temp_hiv_report2_col3_masterpats_05012016;
drop table temp_hiv_report2_col3_haselisaagab_05012016;
drop table temp_hiv_report2_col4_masterpats_05012016;

drop table temp_hiv_report2_masterpats_06012016;
drop table temp_hiv_report2_col2_masterpats_06012016;
drop table temp_hiv_report2_col3_masterpats_06012016;
drop table temp_hiv_report2_col3_haselisaagab_06012016;
drop table temp_hiv_report2_col4_masterpats_06012016;


drop table temp_hiv_report2_masterpats_07012016;
drop table temp_hiv_report2_col2_masterpats_07012016;
drop table temp_hiv_report2_col3_masterpats_07012016;
drop table temp_hiv_report2_col3_haselisaagab_07012016;
drop table temp_hiv_report2_col4_masterpats_07012016;


drop table temp_hiv_report2_masterpats_08012016;
drop table temp_hiv_report2_col2_masterpats_08012016;
drop table temp_hiv_report2_col3_masterpats_08012016;
drop table temp_hiv_report2_col3_haselisaagab_08012016;
drop table temp_hiv_report2_col4_masterpats_08012016;

drop table temp_hiv_report2_01012016;
drop table temp_hiv_report2_firstsubsetpats_01012016;
drop table temp_hiv_report2_haslabtestresults_01012016;
drop table temp_hiv_report2_hashivcase_01012016;

drop table temp_hiv_report2_02012016;
drop table temp_hiv_report2_firstsubsetpats_02012016;
drop table temp_hiv_report2_haslabtestresults_02012016;
drop table temp_hiv_report2_hashivcase_02012016;

drop table temp_hiv_report2_03012016;
drop table temp_hiv_report2_firstsubsetpats_03012016;
drop table temp_hiv_report2_haslabtestresults_03012016;
drop table temp_hiv_report2_hashivcase_03012016;

drop table temp_hiv_report2_04012016;
drop table temp_hiv_report2_firstsubsetpats_04012016;
drop table temp_hiv_report2_haslabtestresults_04012016;
drop table temp_hiv_report2_hashivcase_04012016;

drop table temp_hiv_report2_05012016;
drop table temp_hiv_report2_firstsubsetpats_05012016;
drop table temp_hiv_report2_haslabtestresults_05012016;
drop table temp_hiv_report2_hashivcase_05012016;

drop table temp_hiv_report2_06012016;
drop table temp_hiv_report2_firstsubsetpats_06012016;
drop table temp_hiv_report2_haslabtestresults_06012016;
drop table temp_hiv_report2_hashivcase_06012016;

drop table temp_hiv_report2_07012016;
drop table temp_hiv_report2_firstsubsetpats_07012016;
drop table temp_hiv_report2_haslabtestresults_07012016;
drop table temp_hiv_report2_hashivcase_07012016;

drop table temp_hiv_report2_08012016;
drop table temp_hiv_report2_firstsubsetpats_08012016;
drop table temp_hiv_report2_haslabtestresults_08012016;
drop table temp_hiv_report2_hashivcase_08012016;



drop table temp_hiv_report2_col5_poselisaoragab;
drop table temp_hiv_report2_col5_poselisa;
drop table temp_hiv_report2_col5_poswb;
drop table temp_hiv_report2_col5_posviralload;
drop table temp_hiv_report2_col5_pospcr;
drop table temp_hiv_report2_col5_posagab;
drop table temp_hiv_report2_col5_poselisaoragab_earliest_date;
drop table temp_hiv_report2_col5_poswborviralorpcr;
drop table temp_hiv_report2_col5_almost;
drop table temp_hiv_report2_col5_poswborviralorpcr_earliest_date;

drop table temp_hiv_report2_col1_01012016;
drop table temp_hiv_report2_col1_02012016;
drop table temp_hiv_report2_col1_03012016;
drop table temp_hiv_report2_col1_04012016;
drop table temp_hiv_report2_col1_05012016;
drop table temp_hiv_report2_col1_06012016;
drop table temp_hiv_report2_col1_07012016;
drop table temp_hiv_report2_col1_08012016;


drop table temp_hiv_report2_col2_01012016;
drop table temp_hiv_report2_col2_02012016;
drop table temp_hiv_report2_col2_03012016;
drop table temp_hiv_report2_col2_04012016;
drop table temp_hiv_report2_col2_05012016;
drop table temp_hiv_report2_col2_06012016;
drop table temp_hiv_report2_col2_07012016;
drop table temp_hiv_report2_col2_08012016;

drop table temp_hiv_report2_col3_01012016;
drop table temp_hiv_report2_col3_02012016;
drop table temp_hiv_report2_col3_03012016;
drop table temp_hiv_report2_col3_04012016;
drop table temp_hiv_report2_col3_05012016;
drop table temp_hiv_report2_col3_06012016;
drop table temp_hiv_report2_col3_07012016;
drop table temp_hiv_report2_col3_08012016;

drop table temp_hiv_report2_col4_01012016;
drop table temp_hiv_report2_col4_02012016;
drop table temp_hiv_report2_col4_03012016;
drop table temp_hiv_report2_col4_04012016;
drop table temp_hiv_report2_col4_05012016;
drop table temp_hiv_report2_col4_06012016;
drop table temp_hiv_report2_col4_07012016;
drop table temp_hiv_report2_col4_08012016;

drop table temp_hiv_report2_col5_01012016;
drop table temp_hiv_report2_col5_02012016;
drop table temp_hiv_report2_col5_03012016;
drop table temp_hiv_report2_col5_04012016;
drop table temp_hiv_report2_col5_05012016;
drop table temp_hiv_report2_col5_06012016;
drop table temp_hiv_report2_col5_07012016;
drop table temp_hiv_report2_col5_08012016;

drop table temp_hiv_report2_col5_source_01012016;
drop table temp_hiv_report2_col5_source_02012016;
drop table temp_hiv_report2_col5_source_03012016;
drop table temp_hiv_report2_col5_source_04012016;
drop table temp_hiv_report2_col5_source_05012016;
drop table temp_hiv_report2_col5_source_06012016;
drop table temp_hiv_report2_col5_source_07012016;
drop table temp_hiv_report2_col5_source_08012016;


drop table temp_hivreport_haslabtestresults_01012016;
drop table temp_hivreport_haslabtestresults_02012016;
drop table temp_hivreport_haslabtestresults_03012016;
drop table temp_hivreport_haslabtestresults_04012016;
drop table temp_hivreport_haslabtestresults_05012016;
drop table temp_hivreport_haslabtestresults_06012016;
drop table temp_hivreport_haslabtestresults_07012016;
drop table temp_hivreport_haslabtestresults_08012016;


drop table temp_hivreport_hashivcase_01012016;
drop table temp_hivreport_hashivcase_02012016;
drop table temp_hivreport_hashivcase_03012016;
drop table temp_hivreport_hashivcase_04012016;
drop table temp_hivreport_hashivcase_05012016;
drop table temp_hivreport_hashivcase_06012016;
drop table temp_hivreport_hashivcase_07012016;
drop table temp_hivreport_hashivcase_08012016;

*/


