--here are all the hef events we want for pos chlamydia;
-- ~30 seconds
drop table if exists temp_hef_poschlamyd;
CREATE TABLE temp_hef_poschlamyd as
  select distinct patient_id, provider_id, date from hef_event where name = 'lx:chlamydia:positive' ;
alter table temp_hef_poschlamyd add primary key (patient_id,provider_id,date);
create index temp_hef_poschlamyd_patient_id on temp_hef_poschlamyd (patient_id);
create index temp_hef_poschlamyd_provider_id on temp_hef_poschlamyd (provider_id);
create index temp_hef_poschlamyd_date on temp_hef_poschlamyd (date);
analyze temp_hef_poschlamyd;
--select * from temp_hef_poschlamyd

--I'm going to get the list of patients with chlamydia episodes -- it gets used a few times.
-- less than a second
drop table if exists temp_chlamyd_pats;
CREATE TABLE temp_chlamyd_pats as
select distinct patient_id from temp_hef_poschlamyd;
alter table temp_chlamyd_pats add primary key (patient_id);
analyze temp_chlamyd_pats;
-- select * from temp_chlamyd_pats

--here are the ALL chlamyd tests.  If there are multiple results from the same day, I take the max of pos=1, neg=0, indet=-1
-- ~10 seconds
drop table if exists temp_hef_chlamyd;
CREATE TABLE temp_hef_chlamyd as
  select patient_id,provider_id, date, 
    max(case when substr(name,14)='positive' then 1 when substr(name,14)='negative' then 0 else -1 end) as result
  from hef_event 
  where name like 'lx:chlamydia%'
  group by patient_id, provider_id, date;
alter table temp_hef_chlamyd add primary key (patient_id,provider_id,date);
create index temp_hef_chlamyd_date on temp_hef_chlamyd (date);
analyze temp_hef_chlamyd;
--select * from temp_hef_chlamyd

--Gonorrhea results
-- less than a second
drop table if exists temp_hef_gonpos;
CREATE TABLE temp_hef_gonpos as
  select distinct patient_id,provider_id, date from hef_event where name = 'lx:gonorrhea:positive' ;
alter table temp_hef_gonpos add primary key (patient_id,provider_id,date);
create index temp_hef_gonpos_date on temp_hef_gonpos (date);
analyze temp_hef_gonpos;
--select * from temp_hef_gonpos

--here is the preg results.
-- less than a second
--I think we need start AND end date, to determine if a patient is pregnant within 14 days of chlamydia pos result.
--Have to goose end_date where it's missing.  
--I am assuming from what little I know that missing end date can either mean ongoing pregnancy, or no info about termination/delivery
drop table if exists temp_preg;
CREATE table temp_preg as
select distinct patient_id, start_date, 
  case
    when end_date is not null then end_date
    when end_date is null and start_date + interval '46 weeks' > now() then greatest(now()::date, start_date + interval '40 weeks') --40 is average gestation + 6 weeks is 3 StDevs. 
    else null
  end as end_date
from hef_timespan ht
where name='pregnancy' and exists (select null from temp_chlamyd_pats tcp where tcp.patient_id=ht.patient_id)
   and case
    when end_date is not null then end_date
    when end_date is null and start_date + interval '46 weeks' > now() then now()::date
    else null
  end is not null; 
alter table temp_preg add primary key (patient_id, start_date, end_date);
analyze temp_preg;
--select* from temp_preg where end_date > now();

--here are ICD9 results
--this is a perfect example of the utility of temp tables.  
--The encounter table and dx tables are HUGE, but by limiting this temp table to just the columns and rows we need,
--and by imposing a single restriction that uses a normal to low-cardinaltiy index on the one table and joins via a 
--foreign key relationship, the result is generated relatively quickly and is then much more efficient to use subsequently 
--I'm not bothering to join with the temp chalmydia pats, since that would add to this query's complexity and the vast
--majority of these patients will already be in the chlamyd pats table.  The result here is small enough that a 
--subsequent join to the chlamydia pats table will be very fast.  As is, this query currently takes ~15 min.
--If we only need one of these dx codes (per your original script) it's way faster.  If you tried to embed this in a larger
--query, it would become less and less efficient as you added complexity.
drop table if exists temp_dx;
create table temp_dx as
select enc.patient_id, enc.provider_id, enc.date 
from emr_encounter enc 
  join emr_encounter_dx_codes dx on enc.id=dx.encounter_id 
  where dx.dx_code_id in ('icd9:078.88', 'icd9:079.88', 'icd9:079.98', 'icd9:099.5', 'icd9:099.50' ,'icd9:099.51','icd9:099.52','icd9:099.53','icd9:099.54','icd9:099.55','icd9:099.56','icd9:099.59')
and drvs_service_line != 'Behavioral Health'
and drvs_service_line != 'Dental'
and drvs_service_line != 'Optometry'
and raw_encounter_type != 'NO SHOW'
and raw_encounter_type != 'APPT CANCELLED';
--Another trick -- deleting duplicates here (on a small table) is way faster than imposing a distinct condition in prior query
--Careful with this.  If the result is large, or the result is a substantial portion of the original table, the distinct condition --is faster.
delete from temp_dx dx
where dx.ctid <> (select min(dups.ctid) from temp_dx dups where dx.patient_id=dups.patient_id and dx.provider_id=dups.provider_id and dx.date=dups.date);
alter table temp_dx add primary key (patient_id, provider_id, date);
vacuum 
analyze temp_dx;
--select * from temp_dx;

--now doing the similar for chlamydia reportable dx codes
--this takes about 20 minutes.  
drop table if exists temp_rep_dx;
create table temp_rep_dx as
select distinct enc.patient_id, enc.date, dx.dx_code_id 
from emr_encounter enc 
  join emr_encounter_dx_codes dx on enc.id=dx.encounter_id 
  join conf_reportabledx_code rep on dx.dx_code_id=rep.dx_code_id 
  join temp_chlamyd_pats tcp on enc.patient_id=tcp.patient_id
  where rep.condition_id='chlamydia'
and drvs_service_line != 'Behavioral Health'
and drvs_service_line != 'Dental'
and drvs_service_line != 'Optometry'
and raw_encounter_type != 'NO SHOW'
and raw_encounter_type != 'APPT CANCELLED';

alter table temp_rep_dx add primary key (patient_id, date, dx_code_id);
create index temp_rep_dx_date on temp_rep_dx (date);
analyze temp_rep_dx;
--select * from temp_rep_dx;

--here is rx
--this one takes about 12 minutes.
drop table if exists temp_rx;
create table temp_rx as
select pre.patient_id, pre.provider_id, pre.date from emr_prescription pre join temp_chlamyd_pats cpat on cpat.patient_id=pre.patient_id  
where lower(pre.name) like '%azithromycin%' and lower(pre.name) like '%chlamydia%';
delete from temp_rx rx
where rx.ctid <> (select min(dups.ctid) from temp_rx dups where rx.patient_id=dups.patient_id and rx.provider_id=dups.provider_id and rx.date=dups.date);
alter table temp_rx add primary key (patient_id, provider_id, date);
vacuum 
analyze temp_rx;
--select * from temp_rx

--here is lb for EPT order extension plus the lab results for medication type. 
--I put all the info in one row to simplify things. 
--If the order_extension table gets big this query will become inefficient.
-- ~ 20 seconds, 
-- change result date to be the encounter date
--lb.result_date is not currently provided, or is provided with incorrect values.
drop table if exists temp_ept_lb;
CREATE table temp_ept_lb as
  select ept0.patient_id,ept0.provider_id,ept0.date, null::date as result_date,
      array_to_string(array_agg(lb.native_code),',') as native_code
      from emr_order_extension ept0 
      left join emr_labresult lb on ept0.order_natural_key=lb.order_natural_key and ept0.patient_id=lb.patient_id
      group by ept0.patient_id,ept0.provider_id,ept0.date;
alter table temp_ept_lb add primary key (patient_id, provider_id, date);
analyze temp_ept_lb;
--select * from temp_ept_lb;

--Now I build most of the episode table, using the parts and pieces I assembled above.
--This would be terribly inefficient without the temp tables (small, compact, quickly queryable).
--This runs in approx ~10 seconds
--I use an anonymous block, which permits you to drop in plpgsql function/procedure language into sql code. 
--I don't know a better way to get patient-specific, dynamic temporal windows, but maybe there is. 
drop table if exists temp_chlamyd_episode;
create table temp_chlamyd_episode 
( patient_id integer
  ,provider_id integer
  ,date date
  ,pstart_date date
  ,pend_date date
  ,dx_prov_id integer
  ,dx_date date
  ,rx_prov_id integer
  ,rx_date date
  ,ept_yn varchar(3)
  ,ept_date date
  ,ept_prvd varchar(3)
  ,ept_type varchar(50)
  ,ept_time varchar(20)
 ); 


create or replace function temp_chlamyd_block() 
returns void as $$
-- Wrote as stored procedure for MLCHC
--here's the anonymous block.  You could write this as a stored procedure, but unless you're going to call
-- the procedure a number of times, I prefer to see it in-line -- you don't have to go look at it elsewhere to see what it's doing
  declare
  pat_id int;
  insrtxt text;
  epi_date date;
  dx_date date;
  cursrow record;
  pregcursrow record;
  dxcursrow record;
  dx_ept_found boolean;
  eptcursrow record;
  rxcursrow record;
  epttxt text;
  foundit integer;
  begin
    for pat_id in select patient_id from temp_chlamyd_pats 
    -- these have to match what's in temp_hef_poschlamyd, because we just built the list from there
    loop
      epi_date:=null;
      dx_date:=null;
      for cursrow in execute 'select distinct date, provider_id from temp_hef_poschlamyd where patient_id=' || pat_id || ' order by date'
      loop
      
        if epi_date is null or cursrow.date > epi_date + interval '1 year' then
          dx_ept_found:=FALSE; -- set this to false just in case it could get carried over from prior loop iteration
          epi_date:=cursrow.date;
          insrtxt:='insert into temp_chlamyd_episode (patient_id, provider_id, date, pstart_date, pend_date, dx_prov_id, dx_date, rx_prov_id, rx_date, ept_yn, ept_date, ept_prvd, ept_type, ept_time) values (' ||
             pat_id || ',' || cursrow.provider_id || ',''' || to_char(epi_date,'yyyy-mm-dd') || '''::date, ';
          execute 'select patient_id, start_date, end_date from temp_preg prg where ''' ||
                  to_char(epi_date,'yyyy-mm-dd') || '''::date between start_date - interval ''60 days'' and end_date + interval ''60 days''' ||
              ' and patient_id=' || pat_id || ' order by start_date limit 1' into pregcursrow;
          get diagnostics foundit = ROW_COUNT;
          if foundit<>1 then
            insrtxt:=insrtxt || 'null::date, null::date, ';
          else
            insrtxt:=insrtxt || '''' || to_char(pregcursrow.start_date,'yyyy-mm-dd') || '''::date, ''' || 
                to_char(pregcursrow.end_date,'yyyy-mm-dd') || '''::date, ';  
            if insrtxt is null then raise notice '%', 'check line 188'; end if;
          end if;
          execute 'select provider_id, date from temp_dx where date between ''' || 
              to_char(epi_date,'yyyy-mm-dd') || '''::date and ''' || to_char(epi_date,'yyyy-mm-dd') || '''::date + interval ''365 days''' ||
              ' and patient_id=' || pat_id || ' order by date limit 1' into dxcursrow; 
          get diagnostics foundit = ROW_COUNT;
          if foundit<>1 then
            insrtxt:=insrtxt || 'null, null::date, ';
          else
            insrtxt:=insrtxt || dxcursrow.provider_id || ', ''' || to_char(dxcursrow.date,'yyyy-mm-dd') || '''::date, ';
            if insrtxt is null then raise notice '%', 'check line 198'; end if;
            --ept can be associated with dx or rx. Check for dx first.  Use plus or minus one day.  Is that OK?
            execute 'select patient_id, date,  native_code, result_date from temp_ept_lb where patient_id=' || pat_id || ' and date between ''' || to_char(dxcursrow.date,'yyyy-mm-dd') || 
                     '''::date - interval ''1 days'' and ''' || to_char(dxcursrow.date,'yyyy-mm-dd') || '''::date + interval ''1 days''' into eptcursrow;
            get diagnostics foundit = ROW_COUNT;
            if foundit<>1 then
              dx_ept_found:=FALSE;
            else
              dx_ept_found:=TRUE; 
--the ept fields get inserted after the rx fields in any case, so I just need a boolian here to --let me know I have the ept already
            end if;
          end if;
          execute 'select provider_id, date from temp_rx where date between ''' || 
              to_char(epi_date,'yyyy-mm-dd') || '''::date and ''' || to_char(epi_date,'yyyy-mm-dd') || '''::date + interval ''365 days''' ||
              ' and patient_id=' || pat_id || ' order by date limit 1' into rxcursrow; 
          get diagnostics foundit = ROW_COUNT;
          if foundit<>1 then
            insrtxt:=insrtxt || 'null, null::date, ';
            if dx_ept_found then
              if eptcursrow.native_code='355805--'  then epttxt:='1'', ''Floor Meds Dispensed'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355806--' then epttxt:='1'', ''Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355805--,355806--' then epttxt:='1'', ''Floor Meds Dispensed and Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355804--' then epttxt:='0'', ''Declined'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              else epttxt:='0'', null, null)';
              end if;
              insrtxt:=insrtxt || '''1'', ''' || to_char(eptcursrow.date,'yyyy-mm-dd') || '''::date, ''' || epttxt;
              if insrtxt is null then raise notice '%', 'check line 223'; end if;
            else
              execute 'select patient_id, date,  native_code, result_date from temp_ept_lb where patient_id=' || pat_id || 'and date between ''' ||
                to_char(epi_date,'yyyy-mm-dd') || '''::date and ''' || to_char(epi_date,'yyyy-mm-dd') || '''::date + interval ''28 days''' --chlamydia heuristic recurrance interval
                 into eptcursrow; 
              get diagnostics foundit = ROW_COUNT;
              if foundit<>1 then
                insrtxt:=insrtxt || '''0'', null::date, null, null, null)';
              else
                if eptcursrow.native_code='355805--'  then epttxt:='1'', ''Floor Meds Dispensed'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355806--' then epttxt:='1'', ''Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355805--,355806--'  then epttxt:='1'', ''Floor Meds Dispensed and Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355804--' then epttxt:='0'', ''Declined'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                else epttxt:='0'', null, null)';
                end if;
                insrtxt:=insrtxt || '''0'', ''' || to_char(eptcursrow.date,'yyyy-mm-dd') || '''::date, ''' || epttxt;
              end if;
            end if;
          else
            insrtxt:=insrtxt || rxcursrow.provider_id || ', ''' || to_char(rxcursrow.date,'yyyy-mm-dd') || '''::date, ';
            if insrtxt is null then raise notice '%', 'check line 229'; end if;
            if dx_ept_found then
              if eptcursrow.native_code='355805--'  then epttxt:='1'', ''Floor Meds Dispensed'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355806--' then epttxt:='1'', ''Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355805--,355806--'  then epttxt:='1'', ''Floor Meds Dispensed and Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355804--' then epttxt:='0'', ''Declined'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              else epttxt:='0'', null, null)';
              end if;
              insrtxt:=insrtxt || '''1'', ''' || to_char(eptcursrow.date,'yyyy-mm-dd') || '''::date, ''' || epttxt;
              if insrtxt is null then raise notice 'check line 237: %, %',  date_part('hour',eptcursrow.result_date); end if;
            else
              execute 'select patient_id, date,  native_code, result_date from temp_ept_lb where patient_id=' || pat_id || 'and date between ''' ||
                 to_char(rxcursrow.date,'yyyy-mm-dd') || '''::date - interval ''1 days'' and ''' || to_char(rxcursrow.date,'yyyy-mm-dd') || '''::date + interval ''1 days'''
                 into eptcursrow;
              get diagnostics foundit = ROW_COUNT;
              if foundit<>1 then
                insrtxt:=insrtxt || '0, null::date, null, null, null)';
              else
                if eptcursrow.native_code='355805--'  then epttxt:='1'', ''Floor Meds Dispensed'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355806--' then epttxt:='1'', ''Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355805--,355806--'  then epttxt:='1'', ''Floor Meds Dispensed and Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355804--' then epttxt:='0'', ''Declined'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              else epttxt:='0'', null, null)';
                end if;
                insrtxt:=insrtxt || '''1'', ''' || to_char(eptcursrow.date,'yyyy-mm-dd') || '''::date, ''' || epttxt;
                if insrtxt is null then raise notice '%', 'check line 252'; end if;
              end if;
            end if;
          end if;
          execute insrtxt;
        end if;
      end loop;
    end loop;
  end;
$$ language plpgsql;

--Execute stored procedure
select * from temp_chlamyd_block();



alter table temp_chlamyd_episode add primary key (patient_id, date);
analyze temp_chlamyd_episode;
--select * from temp_chlamyd_episode where patient_id in (373490,1724046,2215954)

drop table if exists temp_post_pos_chlamyd_encs;
create table temp_post_pos_chlamyd_encs as
select t0.patient_id, t0.date as enc_date, t1.date as chlam_date 
from emr_encounter t0
join temp_chlamyd_episode t1 on t1.patient_id=t0.patient_id
where t1.date<t0.date
and drvs_service_line != 'Behavioral Health'
and drvs_service_line != 'Dental'
and drvs_service_line != 'Optometry'
and raw_encounter_type != 'NO SHOW'
and raw_encounter_type != 'APPT CANCELLED';

create index temp_post_pos_chlamyd_encs_pat_idx
  on temp_post_pos_chlamyd_encs (patient_id);

analyze temp_post_pos_chlamyd_encs;

drop table if exists temp_post_pos_chlamyd_enc_keydates;
create table temp_post_pos_chlamyd_enc_keydates as
select distinct t0.patient_id, t0.chlam_date, t1.next_test_date,
   min(t0.enc_date) over (partition by t0.patient_id) as first_enc_date,
   max(case
           when t0.enc_date between t0.chlam_date + interval '3 days' 
                             and t0.chlam_date + interval '28 days'  then 1
           else 0
        end) over (partition by t0.patient_id) as enc_3d_4w,
   max(case
           when t0.enc_date between t0.chlam_date + interval '28 days' 
                             and t0.chlam_date + interval '3 months'  then 1
           else 0
        end) over (partition by t0.patient_id) as enc_4w_3m,
   max(case
           when t0.enc_date between t0.chlam_date + interval '3 months' 
                             and t0.chlam_date + interval '6 months'  then 1
           else 0
        end) over (partition by t0.patient_id) as enc_3m_6m,
   max(case
           when t0.enc_date between t0.chlam_date + interval '6 months' 
                             and t0.chlam_date + interval '9 months'  then 1
           else 0
        end) over (partition by t0.patient_id) as enc_6m_9m,
   max(case
           when t0.enc_date between t0.chlam_date + interval '9 months' 
                             and t0.chlam_date + interval '12 months'  then 1
           else 0
        end) over (partition by t0.patient_id) as enc_9m_12m,
   sum(case
          when t0.enc_date < t1.next_test_date then 1 
          when t1.next_test_date is null and t0.enc_date < t0.chlam_date + interval '1 year' then 1
          else 0
        end) over (partition by t0.patient_id) as n_encs
from temp_post_pos_chlamyd_encs t0
      left join (select t00.patient_id, min(t00.date) as next_test_date
                 from temp_hef_chlamyd t00 
                   join temp_chlamyd_episode t01 on t01.patient_id=t00.patient_id
                 where t00.date between t01.date + interval '14 days'
                                and t01.date + interval '1 year'
                 group by t00.patient_id) t1 on t0.patient_id = t1.patient_id;
Alter table temp_post_pos_chlamyd_enc_keydates 
add primary key (patient_id, chlam_date);

drop table if exists temp_post_rx; 
create table temp_post_rx as
select t0.patient_id, t0.name, t0.date, t0.directions, t0.quantity, t0.refills, t1.date as chlmyd_date
from emr_prescription t0
  join temp_chlamyd_episode t1 on t1.patient_id=t0.patient_id  
    and t0.date between t1.date and t1.date + interval '30 days';

drop table if exists temp_post_rx_azithro;
CREATE TABLE temp_post_rx_azithro AS
select * from 
(SELECT patient_id, chlmyd_date,
       '1'::varchar as azithro_given,
       case 
         when quantity is not null and refills is not null then
           name||', quant: '||quantity||', refills: '||refills
         else name
       end as azithro_dose,
       date as azithro_date,
       row_number() over (partition by patient_id, chlmyd_date order by date) as rnum
FROM temp_post_rx 
where ((name ilike '%zithromycin%' or name ilike '%zithromax%' or name ilike '%zmax%') 
     and not name ilike '%eye %' and not name ilike '%ear %' and not name ilike '%otic%' 
     and not name ilike '%opht%')) t0
where t0.rnum=1;

drop table if exists temp_post_rx_doxy;
CREATE TABLE temp_post_rx_doxy AS
select * from 
(SELECT patient_id, chlmyd_date,
       '1'::varchar as doxy_given,
       case 
         when quantity is not null and refills is not null then
           name||', quant: '||quantity||', refills: '||refills
         else name
       end as doxy_dose,
       date as doxy_date,
       row_number() over (partition by patient_id, chlmyd_date order by date) as rnum
FROM temp_post_rx 
where ((name ilike '%doxyc%' or name ilike '%adoxa%' or name ilike '%doryx%' or name ilike '%monodox%' 
       or name ilike '%oracea%' or name ilike '%periostat%' or name ilike '%targadox%' 
       or name ilike '%vibramycin%') 
       and not name ilike '%eye %' and not name ilike '%ear %' and not name ilike '%otic%' 
       and not name ilike '%opht%')
) t0
where t0.rnum=1;

drop table if exists temp_post_rx_erythro;
CREATE TABLE temp_post_rx_erythro AS
select * from 
(SELECT patient_id, chlmyd_date,
       '1'::varchar as erythro_given,
       case 
         when quantity is not null and refills is not null then
           name||', quant: '||quantity||', refills: '||refills
         else name
       end as erythro_dose,
       date as erythro_date,
       row_number() over (partition by patient_id, chlmyd_date order by date) as rnum
FROM temp_post_rx 
where ((name ilike '%erythromycin%' or name ilike '%e-mycin%' or name ilike '%eryped%'
        or name ilike '%ery-tab%' or name ilike '%erythrocin%')
  and not name ilike '%eye %' and not name ilike '%ear %' and not name ilike '%otic%' 
  and not name ilike '%opht%' and not name ilike '%top%' and not name ilike '%ointment%')) t0
where t0.rnum=1;

drop table if exists temp_post_rx_levoflox;
CREATE TABLE temp_post_rx_levoflox AS
select * from 
(SELECT patient_id, chlmyd_date,
       '1'::varchar as levoflox_given,
       case 
         when quantity is not null and refills is not null then
           name||', quant: '||quantity||', refills: '||refills
         else name
       end as levoflox_dose,
       date as levoflox_date,
       row_number() over (partition by patient_id, chlmyd_date order by date) as rnum
FROM temp_post_rx 
where ((name ilike '%levoflox%' or name ilike '%levaquin%')
    and not name ilike '%eye %' and not name ilike '%ear %' and not name ilike '%otic%' and not name ilike '%opht%')
) t0
where t0.rnum=1;

drop table if exists temp_post_rx_ciproflox;
CREATE table temp_post_rx_ciproflox AS
select * from 
(SELECT patient_id, chlmyd_date,
       '1'::varchar as ciproflox_given,
       case 
         when quantity is not null and refills is not null then
           name||', quant: '||quantity||', refills: '||refills
         else name
       end as ciproflox_dose,
       date as ciproflox_date,
       row_number() over (partition by patient_id, chlmyd_date order by date) as rnum
FROM temp_post_rx 
where ((name ilike '%cipro%' or name ilike '%proquin%' ) 
     and not name ilike '%eye %' and not name ilike '%ear %' and not name ilike '%otic%' 
     and not name ilike '%opht%')
) t0
where t0.rnum=1;

drop table if exists temp_treat;
create table temp_treat as
select t0.patient_id, t0.date,
  t1.azithro_given, t1.azithro_dose, t1.azithro_date,
  t2.doxy_given, t2.doxy_dose, t2.doxy_date,
  t3.erythro_given, t3.erythro_dose, t3.erythro_date,
  t4.levoflox_given, t4.levoflox_dose, t4.levoflox_date,
  t5.ciproflox_given, t5.ciproflox_dose, t5.ciproflox_date,
  case 
    when t1.azithro_given is null and t2.doxy_given is null and t3.erythro_given is null 
         and t4.levoflox_given is null and t5.ciproflox_given is null then
	(select array_to_string(array_agg(name),',') from temp_post_rx t00 
          where t00.patient_id=t0.patient_id and t00.date=t0.date 
          group by t0.patient_id)::text 
    else null::text
  end as other_rx
from temp_chlamyd_episode t0
  left join temp_post_rx_azithro t1 on t1.patient_id=t0.patient_id and t0.date=t1.chlmyd_date
  left join temp_post_rx_doxy t2 on t2.patient_id=t0.patient_id and t0.date=t2.chlmyd_date
  left join temp_post_rx_erythro t3 on t3.patient_id=t0.patient_id and t0.date=t3.chlmyd_date
  left join temp_post_rx_levoflox t4 on t4.patient_id=t0.patient_id and t0.date=t4.chlmyd_date
  left join temp_post_rx_ciproflox t5 on t5.patient_id=t0.patient_id and t0.date=t5.chlmyd_date;
update temp_treat
set azithro_given=0 where azithro_given is null;
update temp_treat
set doxy_given=0 where doxy_given is null;
update temp_treat
set erythro_given=0 where erythro_given is null;
update temp_treat
set levoflox_given=0 where levoflox_given is null;
update temp_treat
set ciproflox_given=0 where ciproflox_given is null;
alter table temp_treat add primary key (patient_id, date);
analyze temp_treat;

--now build the report query from epi and the patient and provider tables.
drop table if exists temp_chlamydia_reinfection_report;
create table temp_chlamydia_reinfection_report as
select  pat.id as ESP_patid,
	pat.center_id as center,
        pat.mrn as MLCHC_patid,
       epi.date as dateposchlamydia, 
       date_part('year', age(epi.date,pat.date_of_birth)) as ageatchlamydiapos,
       pat.gender, 
       pat.race, 
       case 
         when lower(pat.ethnicity) like '%hispanic%' and lower(pat.ethnicity) not ilike '%non%' then 'HISPANIC' 
         when lower(pat.ethnicity) like '%hispanic%' and  lower(pat.ethnicity)  ilike '%non%' then 'NON-HISPANIC' 
         else '' 
       end as ethnicityvalues, 
       case when epi.pstart_date is not null then '1'
       else '0'
       end as pregnantwhenposchla,
       epi.pstart_date as sop,
       epi.pend_date as edd,
       epi.provider_id  as providerposchla,
       pro1.dept as providerlocposchla,
       epi.dx_date as datedxchla,
       epi.dx_prov_id as providerdxchla,
       pro2.dept as providerlocdxchla,
       epi.rx_date as daterxchla,
       epi.rx_prov_id as providerrxchla,
       pro3.dept as providerlocrxchla,
       epi.ept_yn as eptdata, 
       epi.ept_prvd as eptprovided,
       epi.ept_type as typeept, 
       epi.ept_date as dateept,
  --REMOVED 10/16/2015     'not available',--epi.ept_time as timeofdayept,
       case when gon.patient_id is not null then '1' else '0' end as positivegonorrhea, 
       null as hashiv, --place holder until condition is completed
       null as hivtest, --place holder until condition is completed
       case when rep.patient_id is not null then '1' else '0' end as repeatchlamydia,
       rep.date as daterepeatchla,
       case when rep.result=1 then 'Positive' when rep.result=0 then 'Negative' when rep.result=-1 then 'Indeterminate' else null end as resultrepeatchla,
       case when reppos.patient_id is not null then '1' else '0' end as repeatposchla,
       reppos.date as daterepeatposchla ,
       rep2.date as scndrep_date,
       case when rep2.result=1 then 'Positive' when rep2.result=0 then 'Negative' when rep2.result=-1 then 'Indeterminate' else null end as scndrep_res,
       case when rep2.edate is not null then 1 when rep2.date is not null then 0 end as scndrep_ept,
       rep2.edate as scndrep_eptdate,
       rep2.ept_type as scndrep_type,
       rep3.date as thrdrep_date,
       case when rep3.result=1 then 'Positive' when rep3.result=0 then 'Negative' when rep3.result=-1 then 'Indeterminate' else null end as thrdrep_res,
       case when rep3.edate is not null then 1  when rep3.date is not null then 0 end as thrdrep_ept,
       rep3.edate as thrdrep_eptdate,
       rep3.ept_type as thrdrep_type,
       case when symp.icd9_099_40 is null then 0 else symp.icd9_099_40 end icd9_099_40,
       case when symp.icd9_597_80 is null then 0 else symp.icd9_597_80 end icd9_597_80,
       case when symp.icd9_616_0 is null then 0 else symp.icd9_616_0 end icd9_616_0,
       case when symp.icd9_616_10 is null then 0 else symp.icd9_616_10 end icd9_616_10,
       case when symp.icd9_623_5 is null then 0 else  symp.icd9_623_5 end  icd9_623_5,
       case when symp.icd9_780_6 is null then 0 else symp.icd9_780_6 end icd9_780_6,
       case when symp.icd9_780_60 is null then 0 else symp.icd9_780_60 end icd9_780_60,
       case when symp.icd9_788_7 is null then 0 else symp.icd9_788_7 end icd9_788_7,
       case when symp.icd9_789_00 is null then 0 else symp.icd9_789_00 end icd9_789_00,
       case when symp.icd9_789_03 is null then 0 else symp.icd9_789_03 end icd9_789_03,
       case when symp.icd9_789_04 is null then 0 else symp.icd9_789_04 end icd9_789_04,
       case when symp.icd9_789_07 is null then 0 else symp.icd9_789_07 end icd9_789_07,
       case when symp.icd9_789_09 is null then 0 else symp.icd9_789_09 end icd9_789_09,
       tppcek.first_enc_date, tppcek.enc_3d_4w, tppcek.enc_4w_3m, tppcek.enc_3m_6m, 
       tppcek.enc_6m_9m, tppcek.enc_9m_12m, tppcek.n_encs,
       trt.azithro_given, trt.azithro_dose, trt.azithro_date,
       trt.doxy_given, trt.doxy_dose, trt.doxy_date,
       trt.erythro_given, trt.erythro_dose, trt.erythro_date,
       trt.levoflox_given, trt.levoflox_dose, trt.levoflox_date,
       trt.ciproflox_given, trt.ciproflox_dose, trt.ciproflox_date,
       trt.other_rx
 from emr_patient pat 
      join temp_chlamyd_pats tcp on tcp.patient_id=pat.id
      join temp_chlamyd_episode epi on tcp.patient_id=epi.patient_id
      join temp_treat trt on trt.patient_id=epi.patient_id and trt.date=epi.date
      join emr_provider pro1 on epi.provider_id=pro1.id 
      left join temp_post_pos_chlamyd_enc_keydates tppcek on tppcek.patient_id=epi.patient_id
        and tppcek.chlam_date=epi.date
      left join emr_provider pro2 on epi.dx_prov_id=pro2.id
      left join emr_provider pro3 on epi.rx_prov_id=pro3.id
      left join (select distinct gon.patient_id, epi.date as edate from temp_hef_gonpos gon 
                  join temp_chlamyd_episode epi on gon.patient_id=epi.patient_id 
                  where gon.date between epi.date - interval '14 days' and epi.date + interval '14 days') gon 
                  on epi.patient_id=gon.patient_id and epi.date=gon.edate
      left join (with row_sumry as 
                 (select thc.patient_id, thc.date, thc.result, epi.date as edate, 
                  row_number() over (partition by epi.patient_id, epi.date order by thc.date) as rn
                  from temp_hef_chlamyd thc 
                  join temp_chlamyd_episode epi on thc.patient_id=epi.patient_id 
                  where thc.date between epi.date + interval '14 days' and epi.date + interval '365 days'
                 ) select rs.patient_id, rs.date, rs.result, rs.edate from row_sumry rs where rs.rn=1  
--order by rs.patient_id, rs.date
                ) rep on rep.patient_id=epi.patient_id and rep.edate=epi.date
      left join (with row_sumry as
                 (select reppos.patient_id, reppos.date, epi.date as edate, 
                  row_number() over (partition by epi.patient_id, epi.date order by reppos.date) as rn
                  from temp_hef_poschlamyd reppos
                  join temp_chlamyd_episode epi on reppos.patient_id=epi.patient_id
                 where reppos.date between epi.date + interval '14 days' and epi.date + interval '365 days')
                 select rs.patient_id, rs.date, rs.edate from row_sumry rs where rs.rn=1
                 ) reppos on reppos.patient_id=epi.patient_id and reppos.edate=epi.date
       left join (with row_sumry as 
                 (select thc.patient_id, thc.date, thc.result, ept.date as edate, epi.date as epidate,
                  case
                    when ept.native_code='355805--'  then 'Floor Meds Dispensed'
                    when ept.native_code='355806--' then 'Print Rx Manual Fax'
                    when ept.native_code='355805--,355806--' then 'Floor Meds Dispensed and Print Rx Manual Fax'
                    when ept.native_code='355804--' then 'Declined'
                  end as ept_type,
                  row_number() over (partition by epi.patient_id, epi.date order by thc.date) as rn
                  from temp_hef_chlamyd thc 
                  join temp_chlamyd_episode epi on thc.patient_id=epi.patient_id
             left join temp_ept_lb ept on thc.patient_id=ept.patient_id and epi.date between ept.date - interval '1 days' and ept.date + interval '1 days'                   
                  where thc.date between epi.date + interval '14 days' and epi.date + interval '365 days'
                 ) select rs.patient_id, rs.date, rs.result, rs.edate, rs.ept_type, rs.epidate from row_sumry rs where rs.rn=2  
                ) rep2 on rep2.patient_id=epi.patient_id and rep2.epidate=epi.date
       left join (with row_sumry as 
                 (select thc.patient_id, thc.date, thc.result, ept.date as edate, epi.date as epidate, 
                  case
                    when ept.native_code='355805--'  then 'Floor Meds Dispensed'
                    when ept.native_code='355806--' then 'Print Rx Manual Fax'
                    when ept.native_code='355805--,355806--' then 'Floor Meds Dispensed and Print Rx Manual Fax'
                    when ept.native_code='355804--' then 'Declined'
                  end as ept_type,
                  row_number() over (partition by epi.patient_id, epi.date order by thc.date) as rn
                  from temp_hef_chlamyd thc 
                  join temp_chlamyd_episode epi on thc.patient_id=epi.patient_id
             left join temp_ept_lb ept on thc.patient_id=ept.patient_id and epi.date between ept.date - interval '1 days' and ept.date + interval '1 days'                   
                  where thc.date between epi.date + interval '14 days' and epi.date + interval '365 days'
                 ) select rs.patient_id, rs.date, rs.result, rs.edate, rs.ept_type, rs.epidate from row_sumry rs where rs.rn=3  
                ) rep3 on rep3.patient_id=epi.patient_id and rep3.epidate=epi.date
       left join (select trd.patient_id, epi.date as edate,
                    max(case when trd.dx_code_id='icd9:099.40' then 1 else 0 end) as icd9_099_40,
                    max(case when trd.dx_code_id='icd9:597.80' then 1 else 0 end) as icd9_597_80,
                    max(case when trd.dx_code_id='icd9:616.0' then 1 else 0 end) as icd9_616_0,
                    max(case when trd.dx_code_id='icd9:616.10' then 1 else 0 end) as icd9_616_10,
                    max(case when trd.dx_code_id='icd9:623.5' then 1 else 0 end) as icd9_623_5,
                    max(case when trd.dx_code_id='icd9:780.6' then 1 else 0 end) as icd9_780_6,
                    max(case when trd.dx_code_id='icd9:780.60' then 1 else 0 end) as icd9_780_60,
                    max(case when trd.dx_code_id='icd9:788.7' then 1 else 0 end) as icd9_788_7,
                    max(case when trd.dx_code_id='icd9:789.00' then 1 else 0 end) as icd9_789_00,
                    max(case when trd.dx_code_id='icd9:789.03' then 1 else 0 end) as icd9_789_03,
                    max(case when trd.dx_code_id='icd9:789.04' then 1 else 0 end) as icd9_789_04,
                    max(case when trd.dx_code_id='icd9:789.07' then 1 else 0 end) as icd9_789_07,
                    max(case when trd.dx_code_id='icd9:789.09' then 1 else 0 end) as icd9_789_09
                  from temp_rep_dx trd
                  join temp_chlamyd_episode epi on trd.patient_id=epi.patient_id 
                 where trd.date between epi.date - interval '14 days' and epi.date + interval '14 days' --from conf_conditionconfig
                 group by trd.patient_id, epi.date  order by trd.patient_id, epi.date ) symp on symp.patient_id=epi.patient_id and symp.edate=epi.date
where epi.date >= '01-01-2010'  
 order by epi.date;

\copy temp_chlamydia_reinfection_report to '/tmp/2016_09_11_chlamydia_reinfection.csv' csv header

--now build the report NO PHI query from epi and the patient and provider tables.
drop table if exists temp_chlamydia_reinfection_report_nophi;
create table temp_chlamydia_reinfection_report_nophi as
select  pat.id as ESP_patid,
	pat.center_id as center,
--PHI
        'MRN' as MLCHC_patid,
       epi.date as dateposchlamydia, 
       date_part('year', age(epi.date,pat.date_of_birth)) as ageatchlamydiapos,
       pat.gender, 
       pat.race, 
       case 
         when lower(pat.ethnicity) like '%hispanic%' and lower(pat.ethnicity) not ilike '%non%' then 'HISPANIC' 
         when lower(pat.ethnicity) like '%hispanic%' and  lower(pat.ethnicity)  ilike '%non%' then 'NON-HISPANIC' 
         else '' 
       end as ethnicityvalues, 
       case when epi.pstart_date is not null then '1'
       else '0'
       end as pregnantwhenposchla,

       epi.pstart_date as sop,
       epi.pend_date as edd,
       epi.provider_id  as providerposchla,
       pro1.dept as providerlocposchla,
       epi.dx_date as datedxchla,
       epi.dx_prov_id as providerdxchla,
       pro2.dept as providerlocdxchla,
       epi.rx_date as daterxchla,
       epi.rx_prov_id as providerrxchla,
       pro3.dept as providerlocrxchla,
       epi.ept_yn as eptdata, 
       epi.ept_prvd as eptprovided,
       epi.ept_type as typeept, 
       epi.ept_date as dateept,
  --REMOVED 10/16/2015     'not available',--epi.ept_time as timeofdayept,
       case when gon.patient_id is not null then '1' else '0' end as positivegonorrhea, 
       null as hashiv, --place holder until condition is completed
       null as hivtest, --place holder until condition is completed
       case when rep.patient_id is not null then '1' else '0' end as repeatchlamydia,
       rep.date as daterepeatchla,
       case when rep.result=1 then 'Positive' when rep.result=0 then 'Negative' when rep.result=-1 then 'Indeterminate' else null end as resultrepeatchla,
       case when reppos.patient_id is not null then '1' else '0' end as repeatposchla,
       reppos.date as daterepeatposchla,
       rep2.date as scndrep_date,
       case when rep2.result=1 then 'Positive' when rep2.result=0 then 'Negative' when rep2.result=-1 then 'Indeterminate' else null end as scndrep_res,
       case when rep2.edate is not null then 1 when rep2.date is not null then 0 end as scndrep_ept,
       rep2.edate as scndrep_eptdate,
       rep2.ept_type as scndrep_type,
       rep3.date as thrdrep_date,
       case when rep3.result=1 then 'Positive' when rep3.result=0 then 'Negative' when rep3.result=-1 then 'Indeterminate' else null end as thrdrep_res,
       case when rep3.edate is not null then 1  when rep3.date is not null then 0 end as thrdrep_ept,
       rep3.edate as thrdrep_eptdate,
       rep3.ept_type as thrdrep_type,
       case when symp.icd9_099_40 is null then 0 else symp.icd9_099_40 end icd9_099_40,
       case when symp.icd9_597_80 is null then 0 else symp.icd9_597_80 end icd9_597_80,
       case when symp.icd9_616_0 is null then 0 else symp.icd9_616_0 end icd9_616_0,
       case when symp.icd9_616_10 is null then 0 else symp.icd9_616_10 end icd9_616_10,
       case when symp.icd9_623_5 is null then 0 else  symp.icd9_623_5 end  icd9_623_5,
       case when symp.icd9_780_6 is null then 0 else symp.icd9_780_6 end icd9_780_6,
       case when symp.icd9_780_60 is null then 0 else symp.icd9_780_60 end icd9_780_60,
       case when symp.icd9_788_7 is null then 0 else symp.icd9_788_7 end icd9_788_7,
       case when symp.icd9_789_00 is null then 0 else symp.icd9_789_00 end icd9_789_00,
       case when symp.icd9_789_03 is null then 0 else symp.icd9_789_03 end icd9_789_03,
       case when symp.icd9_789_04 is null then 0 else symp.icd9_789_04 end icd9_789_04,
       case when symp.icd9_789_07 is null then 0 else symp.icd9_789_07 end icd9_789_07,
       case when symp.icd9_789_09 is null then 0 else symp.icd9_789_09 end icd9_789_09,
       tppcek.first_enc_date, 
       tppcek.enc_3d_4w, tppcek.enc_4w_3m, tppcek.enc_3m_6m, 
       tppcek.enc_6m_9m, tppcek.enc_9m_12m, tppcek.n_encs,
       trt.azithro_given, trt.azithro_dose, 
       trt.azithro_date,
       trt.doxy_given, trt.doxy_dose, 
       trt.doxy_date,
       trt.erythro_given, trt.erythro_dose, 
       trt.erythro_date,
       trt.levoflox_given, trt.levoflox_dose, 
       trt.levoflox_date,
       trt.ciproflox_given, trt.ciproflox_dose, 
       trt.ciproflox_date,
       trt.other_rx
 from emr_patient pat 
      join temp_chlamyd_pats tcp on tcp.patient_id=pat.id
      join temp_chlamyd_episode epi on tcp.patient_id=epi.patient_id
      join temp_treat trt on trt.patient_id=epi.patient_id and trt.date=epi.date
      join emr_provider pro1 on epi.provider_id=pro1.id 
      left join temp_post_pos_chlamyd_enc_keydates tppcek on tppcek.patient_id=epi.patient_id
        and tppcek.chlam_date=epi.date
      left join emr_provider pro2 on epi.dx_prov_id=pro2.id
      left join emr_provider pro3 on epi.rx_prov_id=pro3.id
      left join (select distinct gon.patient_id, epi.date as edate from temp_hef_gonpos gon 
                  join temp_chlamyd_episode epi on gon.patient_id=epi.patient_id 
                  where gon.date between epi.date - interval '14 days' and epi.date + interval '14 days') gon 
                  on epi.patient_id=gon.patient_id and epi.date=gon.edate
      left join (with row_sumry as 
                 (select thc.patient_id, thc.date, thc.result, epi.date as edate, 
                  row_number() over (partition by epi.patient_id, epi.date order by thc.date) as rn
                  from temp_hef_chlamyd thc 
                  join temp_chlamyd_episode epi on thc.patient_id=epi.patient_id 
                  where thc.date between epi.date + interval '14 days' and epi.date + interval '365 days'
                 ) select rs.patient_id, rs.date, rs.result, rs.edate from row_sumry rs where rs.rn=1  
--order by rs.patient_id, rs.date
                ) rep on rep.patient_id=epi.patient_id and rep.edate=epi.date
      left join (with row_sumry as
                 (select reppos.patient_id, reppos.date, epi.date as edate, 
                  row_number() over (partition by epi.patient_id, epi.date order by reppos.date) as rn
                  from temp_hef_poschlamyd reppos
                  join temp_chlamyd_episode epi on reppos.patient_id=epi.patient_id
                 where reppos.date between epi.date + interval '14 days' and epi.date + interval '365 days')
                 select rs.patient_id, rs.date, rs.edate from row_sumry rs where rs.rn=1
                 ) reppos on reppos.patient_id=epi.patient_id and reppos.edate=epi.date
       left join (with row_sumry as 
                 (select thc.patient_id, thc.date, thc.result, ept.date as edate, epi.date as epidate,
                  case
                    when ept.native_code='355805--'  then 'Floor Meds Dispensed'
                    when ept.native_code='355806--' then 'Print Rx Manual Fax'
                    when ept.native_code='355805--,355806--' then 'Floor Meds Dispensed and Print Rx Manual Fax'
                    when ept.native_code='355804--' then 'Declined'
                  end as ept_type,
                  row_number() over (partition by epi.patient_id, epi.date order by thc.date) as rn
                  from temp_hef_chlamyd thc 
                  join temp_chlamyd_episode epi on thc.patient_id=epi.patient_id
             left join temp_ept_lb ept on thc.patient_id=ept.patient_id and epi.date between ept.date - interval '1 days' and ept.date + interval '1 days'                   
                  where thc.date between epi.date + interval '14 days' and epi.date + interval '365 days'
                 ) select rs.patient_id, rs.date, rs.result, rs.edate, rs.ept_type, rs.epidate from row_sumry rs where rs.rn=2  
                ) rep2 on rep2.patient_id=epi.patient_id and rep2.epidate=epi.date
       left join (with row_sumry as 
                 (select thc.patient_id, thc.date, thc.result, ept.date as edate, epi.date as epidate, 
                  case
                    when ept.native_code='355805--'  then 'Floor Meds Dispensed'
                    when ept.native_code='355806--' then 'Print Rx Manual Fax'
                    when ept.native_code='355805--,355806--' then 'Floor Meds Dispensed and Print Rx Manual Fax'
                    when ept.native_code='355804--' then 'Declined'
                  end as ept_type,
                  row_number() over (partition by epi.patient_id, epi.date order by thc.date) as rn
                  from temp_hef_chlamyd thc 
                  join temp_chlamyd_episode epi on thc.patient_id=epi.patient_id
             left join temp_ept_lb ept on thc.patient_id=ept.patient_id and epi.date between ept.date - interval '1 days' and ept.date + interval '1 days'                   
                  where thc.date between epi.date + interval '14 days' and epi.date + interval '365 days'
                 ) select rs.patient_id, rs.date, rs.result, rs.edate, rs.ept_type, rs.epidate from row_sumry rs where rs.rn=3  
                ) rep3 on rep3.patient_id=epi.patient_id and rep3.epidate=epi.date
       left join (select trd.patient_id, epi.date as edate,
                    max(case when trd.dx_code_id='icd9:099.40' then 1 else 0 end) as icd9_099_40,
                    max(case when trd.dx_code_id='icd9:597.80' then 1 else 0 end) as icd9_597_80,
                    max(case when trd.dx_code_id='icd9:616.0' then 1 else 0 end) as icd9_616_0,
                    max(case when trd.dx_code_id='icd9:616.10' then 1 else 0 end) as icd9_616_10,
                    max(case when trd.dx_code_id='icd9:623.5' then 1 else 0 end) as icd9_623_5,
                    max(case when trd.dx_code_id='icd9:780.6' then 1 else 0 end) as icd9_780_6,
                    max(case when trd.dx_code_id='icd9:780.60' then 1 else 0 end) as icd9_780_60,
                    max(case when trd.dx_code_id='icd9:788.7' then 1 else 0 end) as icd9_788_7,
                    max(case when trd.dx_code_id='icd9:789.00' then 1 else 0 end) as icd9_789_00,
                    max(case when trd.dx_code_id='icd9:789.03' then 1 else 0 end) as icd9_789_03,
                    max(case when trd.dx_code_id='icd9:789.04' then 1 else 0 end) as icd9_789_04,
                    max(case when trd.dx_code_id='icd9:789.07' then 1 else 0 end) as icd9_789_07,
                    max(case when trd.dx_code_id='icd9:789.09' then 1 else 0 end) as icd9_789_09
                  from temp_rep_dx trd
                  join temp_chlamyd_episode epi on trd.patient_id=epi.patient_id 
                 where trd.date between epi.date - interval '14 days' and epi.date + interval '14 days' --from conf_conditionconfig
                 group by trd.patient_id, epi.date  order by trd.patient_id, epi.date ) symp on symp.patient_id=epi.patient_id and symp.edate=epi.date
where epi.date >= '01-01-2010'  
 order by epi.date;

\copy temp_chlamydia_reinfection_report_nophi to '/tmp/2016_09_11_chlamydia_reinfection_nophi.csv' csv header
