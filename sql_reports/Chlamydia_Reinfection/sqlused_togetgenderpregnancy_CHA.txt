select count(distinct patient_id),
case
when gender = 'MALE' and pregstartdate is null then 'M'
when gender = 'MALE' and pregstartdate is not null THEN '!!MALE-PREGNANT!!'
when gender = 'O' then 'O'
when gender = 'FEMALE' and pregstartdate is not null then 'F-PREG'
when gender = 'FEMALE' and pregstartdate is null then 'F-NOTPREG'
WHEN (gender is NULL or gender = '' or gender = 'UNKNOWN') and pregstartdate is null then 'U-NOTPREG'
WHEN (gender is NULL or gender = '' or gender = 'UNKNOWN') and pregstartdate is NOT null then 'U-PREG'
else gender
end as gender_pregstatus,
encdate
from
(
            select emr_encounter.patient_id, gender, date_trunc('year', date)::date as encdate, date_trunc('year', hef_timespan.start_date)::date as pregstartdate
            from emr_encounter
            join emr_patient on emr_patient.id = emr_encounter.patient_id
            left join hef_timespan on emr_encounter.patient_id = hef_timespan.patient_id 
            and date_trunc('year', date)::date = date_trunc('year', hef_timespan.start_date)::date) a
group by encdate, gender_pregstatus
order by encdate, gender_pregstatus;
