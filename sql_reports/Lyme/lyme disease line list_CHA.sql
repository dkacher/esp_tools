-- meet at least one of the proposed ESP surveillance algorithm criteria components between January 1, 2017 and December 31, 2018 

--
-- pick up encounters who have tick bite icd (ALL)
--
drop table if exists tick_dx_enc;
CREATE TABLE tick_dx_enc AS 
SELECT encounter_id, dx_code_id AS tick_dx
FROM emr_encounter_dx_codes dx 
WHERE dx.dx_code_id IN ('icd9:919.4', 'icd9:E906.4', 'icd9:911.4', 'icd9:916.4', 'icd10:W57.XXXA')
OR dx_code_id ilike 'icd10:T14.8%';

-- select the first reported date for tick bite ICD 
-- 2017
drop table if exists tick_dx_enc_idx;
CREATE TABLE tick_dx_enc_idx AS
SELECT enc.patient_id, tick_dx, enc.date, min(enc.date) over (partition by enc.patient_id) AS tick_idx_date
FROM emr_encounter enc
JOIN tick_dx_enc dx ON enc.id = dx.encounter_id
WHERE enc.date BETWEEN '01-01-2017' AND '12-31-2017';

-- 2018
drop table if exists tick_dx_enc_idx_18;
CREATE TABLE tick_dx_enc_idx_18 AS
SELECT enc.patient_id, tick_dx, enc.date, min(enc.date) over (partition by enc.patient_id) AS tick_idx_date
FROM emr_encounter enc
JOIN tick_dx_enc dx ON enc.id = dx.encounter_id
WHERE enc.date BETWEEN '01-01-2018' AND '12-31-2018';

--
-- pick up Lyme ICD code (ALL)
--
drop table if exists lyme_dx_enc;
CREATE TABLE lyme_dx_enc AS 
SELECT encounter_id, dx_code_id AS lyme_dx
FROM emr_encounter_dx_codes dx 
WHERE dx.dx_code_id IN ('icd9:088.81', 'icd10:A69.20', 'icd10:A69.21', 'icd10:A69.22', 'icd10:A69.23', 'icd10:A69.29');

-- select the first reported date for lyme ICD
-- 2017
drop table if exists lyme_dx_enc_idx;
CREATE TABLE lyme_dx_enc_idx AS 
SELECT enc.patient_id, lyme_dx, enc.date, min(enc.date) over (partition by enc.patient_id) AS lyme_idx_date
FROM emr_encounter enc
JOIN lyme_dx_enc dx ON enc.id = dx.encounter_id
WHERE enc.date BETWEEN '01-01-2017' AND '12-31-2017';

-- 2018
drop table if exists lyme_dx_enc_idx_18;
CREATE TABLE lyme_dx_enc_idx_18 AS 
SELECT enc.patient_id, lyme_dx, enc.date, min(enc.date) over (partition by enc.patient_id) AS lyme_idx_date
FROM emr_encounter enc
JOIN lyme_dx_enc dx ON enc.id = dx.encounter_id
WHERE enc.date BETWEEN '01-01-2018' AND '12-31-2018';

--
-- pick up EIA/ELISA/IFA+ lab tests 
-- Q1: NEED TO have string lyme in it or not
-- Q2: can't find IFA
-- Q3: positive results (some were numeric values)

-- 2017 
drop table if exists lyme_lt1;
CREATE TABLE lyme_lt1 AS
SELECT l.patient_id, l.native_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt1_idx_date
FROM emr_labresult l
JOIN conf_labtestmap tm ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia') 
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');

-- 2018 
drop table if exists lyme_lt1_18;
CREATE TABLE lyme_lt1_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt1_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE (tm.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia') 
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');

--
-- WB/PCR (ALL)
-- 2017
/*
drop table if exists lyme_lt2;
CREATE TABLE lyme_lt2 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt2_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igm_wb', 'lyme_igg_wb', 'lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');

-- 2018
drop table if exists lyme_lt2_18;
CREATE TABLE lyme_lt2_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt2_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igm_wb', 'lyme_igg_wb', 'lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');
*/


--
-- IgG WB 
-- Specific to CHA pick up additional igg wb in CHA
drop table if exists lt_igg_wb_code;
CREATE TABLE lt_igg_wb_code AS
select distinct native_name, native_code from emr_labtestconcordance 
where native_name in (
'LYME AB WESTERN BLOT CSF-- P18 AB IGG CSF',
'LYME AB WESTERN BLOT CSF-- P23 AB IGG CSF',
'LYME AB WESTERN BLOT CSF-- P28 AB IGG CSF',
'LYME AB WESTERN BLOT CSF-- P30 AB IGG CSF',
'LYME AB WESTERN BLOT CSF-- P39 AB IGG CSF',
'LYME AB WESTERN BLOT CSF-- P41 AB IGG CSF',
'LYME AB WESTERN BLOT CSF-- P45 AB IGG CSF',
'LYME AB WESTERN BLOT CSF-- P58 AB IGG CSF',
'LYME AB WESTERN BLOT CSF-- P66 AB IGG CSF',
'LYME AB WESTERN BLOT CSF-- P93 AB IGG CSF',
'LYME AB WESTERN BLOT SYN FL-- P18 AB IGG SYNOVIAL FLUID',
'LYME AB WESTERN BLOT SYN FL-- P23 AB IGG SYNOVIAL FLUID',
'LYME AB WESTERN BLOT SYN FL-- P28 AB IGG SYNOVIAL FLUID',
'LYME AB WESTERN BLOT SYN FL-- P30 AB IGG SYNOVIAL FLUID',
'LYME AB WESTERN BLOT SYN FL-- P39 AB IGG SYNOVIAL FLUID',
'LYME AB WESTERN BLOT SYN FL-- P41 AB IGG SYNOVIAL FLUID',
'LYME AB WESTERN BLOT SYN FL-- P45 AB IGG SYNOVIAL FLUID',
'LYME AB WESTERN BLOT SYN FL-- P58 AB IGG SYNOVIAL FLUID',
'LYME AB WESTERN BLOT SYN FL-- P66 AB IGG SYNOVIAL FLUID',
'LYME AB WESTERN BLOT SYN FL-- P93 AB IGG SYNOVIAL FLUID',
'LYME ANTIBODY WESTERN BLOT CSF-- P18 AB IGG CSF',
'LYME ANTIBODY WESTERN BLOT CSF-- P23 AB IGG CSF',
'LYME ANTIBODY WESTERN BLOT CSF-- P28 AB IGG CSF',
'LYME ANTIBODY WESTERN BLOT CSF-- P30 AB IGG CSF',
'LYME ANTIBODY WESTERN BLOT CSF-- P39 AB IGG CSF',
'LYME ANTIBODY WESTERN BLOT CSF-- P41 AB IGG CSF',
'LYME ANTIBODY WESTERN BLOT CSF-- P45 AB IGG CSF',
'LYME ANTIBODY WESTERN BLOT CSF-- P58 AB IGG CSF',
'LYME ANTIBODY WESTERN BLOT CSF-- P66 AB IGG CSF',
'LYME ANTIBODY WESTERN BLOT CSF-- P93 AB IGG CSF',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P18 AB IGG',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P23 AB IGG',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P28 AB IGG',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P30 AB IGG',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P39 AB IGG',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P41 AB IGG',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P45 AB IGG',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P58 AB IGG',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P66 AB IGG',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P93 AB IGG',
'LYME IGG/IGM RFLX WESTERN BLOT-- P18 AB IGG',
'LYME IGG/IGM RFLX WESTERN BLOT-- P23 AB IGG',
'LYME IGG/IGM RFLX WESTERN BLOT-- P28 AB IGG',
'LYME IGG/IGM RFLX WESTERN BLOT-- P30 AB IGG',
'LYME IGG/IGM RFLX WESTERN BLOT-- P39 AB IGG',
'LYME IGG/IGM RFLX WESTERN BLOT-- P41 AB IGG',
'LYME IGG/IGM RFLX WESTERN BLOT-- P45 AB IGG',
'LYME IGG/IGM RFLX WESTERN BLOT-- P58 AB IGG',
'LYME IGG/IGM RFLX WESTERN BLOT-- P66 AB IGG',
'LYME IGG/IGM RFLX WESTERN BLOT-- P93 AB IGG',
'LYME IGM AB WESTERN BLOT REFLX-- P18 AB IGG',
'LYME IGM AB WESTERN BLOT REFLX-- P23 AB IGG',
'LYME IGM AB WESTERN BLOT REFLX-- P28 AB IGG',
'LYME IGM AB WESTERN BLOT REFLX-- P30 AB IGG',
'LYME IGM AB WESTERN BLOT REFLX-- P39 AB IGG',
'LYME IGM AB WESTERN BLOT REFLX-- P41 AB IGG',
'LYME IGM AB WESTERN BLOT REFLX-- P45 AB IGG',
'LYME IGM AB WESTERN BLOT REFLX-- P58 AB IGG',
'LYME IGM AB WESTERN BLOT REFLX-- P66 AB IGG',
'LYME IGM AB WESTERN BLOT REFLX-- P93 AB IGG')

-- IGG 2017
drop table if exists lt_igg_wb_temp_17;
CREATE TABLE lt_igg_wb_temp_17 AS
SELECT l.patient_id, tm.native_name, l.date, result_string, result_float
FROM lt_igg_wb_code tm
JOIN emr_labresult l ON tm.native_code=l.native_code
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 

-- additional positive wb_igg
drop table if exists lt_igg_wb_temp1_17;
CREATE TABLE lt_igg_wb_temp1_17 AS
select t0.*, case when t1.patient_id is not null and native_name ilike '%LYME AB WESTERN BLOT CSF%' then t0.patient_id end as pos_1, 
case when t2.patient_id is not null and native_name ilike '%LYME AB WESTERN BLOT SYN FL%' then t0.patient_id end as pos_2,
case when t3.patient_id is not null and native_name ilike '%LYME ANTIBODY WESTERN BLOT CSF%' then t0.patient_id end as pos_3,
case when t4.patient_id is not null and native_name ilike '%LYME IGG/IGM REFLEX WESTERN BLOT%' then t0.patient_id end as pos_4, 
case when t5.patient_id is not null and native_name ilike '%LYME IGG/IGM RFLX WESTERN BLOT%' then t0.patient_id end as pos_5,
case when t6.patient_id is not null and native_name ilike '%LYME IGM AB WESTERN BLOT REFLX%' then t0.patient_id end as pos_6
from lt_igg_wb_temp_17 t0
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME AB WESTERN BLOT CSF%'
group by patient_id having count(distinct native_name)>=5) t1 on t0.patient_id = t1.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME AB WESTERN BLOT SYN FL%'
group by patient_id having count(distinct native_name)>=5) t2 on t0.patient_id = t2.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME ANTIBODY WESTERN BLOT CSF%'
group by patient_id having count(distinct native_name)>=5) t3 on t0.patient_id = t3.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME IGG/IGM REFLEX WESTERN BLOT%'
group by patient_id having count(distinct native_name)>=5) t4 on t0.patient_id = t4.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME IGG/IGM RFLX WESTERN BLOT%'
group by patient_id having count(distinct native_name)>=5) t5 on t0.patient_id = t5.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME IGM AB WESTERN BLOT REFLX%'
group by patient_id having count(distinct native_name)>=5) t6 on t0.patient_id = t6.patient_id 
where result_string ilike '%present%' and COALESCE(t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id, t5.patient_id, t6.patient_id) is not null 
order by t0.patient_id

-- combine with mapped data (CHA)
drop table if exists lt_igg_wb;
CREATE TABLE lt_igg_wb AS
SELECT t0.patient_id, t0.test_name, t0.date, t0.result_string, min(t0.date) over (partition by t0.patient_id) AS igg_wb_idx_date
FROM (SELECT l.patient_id, tm.test_name, l.date, result_string
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%')
UNION
SELECT patient_id, native_name, date, result_string 
FROM lt_igg_wb_temp1_17
) t0
;
/* (old)
drop table if exists lt_igg_wb;
CREATE TABLE lt_igg_wb AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS igg_wb_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');
*/

-- IGG 2018
drop table if exists lt_igg_wb_temp_18;
CREATE TABLE lt_igg_wb_temp_18 AS
SELECT l.patient_id, tm.native_name, l.date, result_string, result_float
FROM lt_igg_wb_code tm
JOIN emr_labresult l ON tm.native_code=l.native_code
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 

-- additional positive wb_igg
drop table if exists lt_igg_wb_temp1_18;
CREATE TABLE lt_igg_wb_temp1_18 AS
select t0.*, case when t1.patient_id is not null and native_name ilike '%LYME AB WESTERN BLOT CSF%' then t0.patient_id end as pos_1, 
case when t2.patient_id is not null and native_name ilike '%LYME AB WESTERN BLOT SYN FL%' then t0.patient_id end as pos_2,
case when t3.patient_id is not null and native_name ilike '%LYME ANTIBODY WESTERN BLOT CSF%' then t0.patient_id end as pos_3,
case when t4.patient_id is not null and native_name ilike '%LYME IGG/IGM REFLEX WESTERN BLOT%' then t0.patient_id end as pos_4, 
case when t5.patient_id is not null and native_name ilike '%LYME IGG/IGM RFLX WESTERN BLOT%' then t0.patient_id end as pos_5,
case when t6.patient_id is not null and native_name ilike '%LYME IGM AB WESTERN BLOT REFLX%' then t0.patient_id end as pos_6
from lt_igg_wb_temp_18 t0
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME AB WESTERN BLOT CSF%'
group by patient_id having count(distinct native_name)>=5) t1 on t0.patient_id = t1.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME AB WESTERN BLOT SYN FL%'
group by patient_id having count(distinct native_name)>=5) t2 on t0.patient_id = t2.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME ANTIBODY WESTERN BLOT CSF%'
group by patient_id having count(distinct native_name)>=5) t3 on t0.patient_id = t3.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME IGG/IGM REFLEX WESTERN BLOT%'
group by patient_id having count(distinct native_name)>=5) t4 on t0.patient_id = t4.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME IGG/IGM RFLX WESTERN BLOT%'
group by patient_id having count(distinct native_name)>=5) t5 on t0.patient_id = t5.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igg_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME IGM AB WESTERN BLOT REFLX%'
group by patient_id having count(distinct native_name)>=5) t6 on t0.patient_id = t6.patient_id 
where result_string ilike '%present%' and COALESCE(t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id, t5.patient_id, t6.patient_id) is not null 
order by t0.patient_id

-- combine with mapped data (CHA)
drop table if exists lt_igg_wb_18;
CREATE TABLE lt_igg_wb_18 AS
SELECT t0.patient_id, t0.test_name, t0.date, t0.result_string, min(t0.date) over (partition by t0.patient_id) AS igg_wb_idx_date
FROM (SELECT l.patient_id, tm.test_name, l.date, result_string
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb')
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%')
UNION
SELECT patient_id, native_name, date, result_string 
FROM lt_igg_wb_temp1_18
) t0

-- 2018 (old)
/*
drop table if exists lt_igg_wb_18;
CREATE TABLE lt_igg_wb_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS igg_wb_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb')
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');
*/

--
-- IgM WB 
--

-- Specific to CHA  pick up additional lab tests related to igm_wb 
drop table if exists lt_igm_wb_code;
CREATE TABLE lt_igm_wb_code AS
select distinct native_name, native_code from emr_labtestconcordance 
where native_name in (
'LYME AB WESTERN BLOT CSF-- LYME P23 AB IGM CSF',
'LYME AB WESTERN BLOT CSF-- LYME P39 AB IGM CSF',
'LYME AB WESTERN BLOT CSF-- LYME P41 AB IGM CSF',
'LYME AB WESTERN BLOT SYN FL-- P23 AB IGM SYNOVIAL FLUID',
'LYME AB WESTERN BLOT SYN FL-- P39 AB IGM SYNOVIAL FLUID',
'LYME AB WESTERN BLOT SYN FL-- P41 AB IGM SYNOVIAL FLUID',
'LYME ANTIBODY WESTERN BLOT CSF-- LYME P23 AB IGM CSF',
'LYME ANTIBODY WESTERN BLOT CSF-- LYME P39 AB IGM CSF',
'LYME ANTIBODY WESTERN BLOT CSF-- LYME P41 AB IGM CSF',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P23 AB IGM',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P39 AB IGM',
'LYME IGG/IGM REFLEX WESTERN BLOT-- P41 AB IGM',
'LYME IGG/IGM RFLX WESTERN BLOT-- P23 AB IGM',
'LYME IGG/IGM RFLX WESTERN BLOT-- P39 AB IGM',
'LYME IGG/IGM RFLX WESTERN BLOT-- P41 AB IGM',
'LYME IGM AB WESTERN BLOT REFLX-- P23 AB IGM',
'LYME IGM AB WESTERN BLOT REFLX-- P39 AB IGM',
'LYME IGM AB WESTERN BLOT REFLX-- P41 AB IGM'
)

--2017 CHA
drop table if exists lt_igm_wb_temp_17;
CREATE TABLE lt_igm_wb_temp_17 AS
SELECT l.patient_id, tm.native_name, l.date, result_string, result_float
FROM lt_igm_wb_code tm
JOIN emr_labresult l ON tm.native_code=l.native_code
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 

-- additional positive wb_igm
drop table if exists lt_igm_wb_temp1_17;
CREATE TABLE lt_igm_wb_temp1_17 AS
select t0.*, case when t1.patient_id is not null and native_name ilike '%LYME AB WESTERN BLOT CSF%' then t0.patient_id end as pos_1, 
case when t2.patient_id is not null and native_name ilike '%LYME AB WESTERN BLOT SYN FL%' then t0.patient_id end as pos_2,
case when t3.patient_id is not null and native_name ilike '%LYME ANTIBODY WESTERN BLOT CSF%' then t0.patient_id end as pos_3,
case when t4.patient_id is not null and native_name ilike '%LYME IGG/IGM REFLEX WESTERN BLOT%' then t0.patient_id end as pos_4, 
case when t5.patient_id is not null and native_name ilike '%LYME IGG/IGM RFLX WESTERN BLOT%' then t0.patient_id end as pos_5,
case when t6.patient_id is not null and native_name ilike '%LYME IGM AB WESTERN BLOT REFLX-%' then t0.patient_id end as pos_6
from lt_igm_wb_temp_17 t0
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME AB WESTERN BLOT CSF%'
group by patient_id having count(distinct native_name)>=2) t1 on t0.patient_id = t1.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME AB WESTERN BLOT SYN FL%'
group by patient_id having count(distinct native_name)>=2) t2 on t0.patient_id = t2.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME ANTIBODY WESTERN BLOT CSF%'
group by patient_id having count(distinct native_name)>=2) t3 on t0.patient_id = t3.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME IGG/IGM REFLEX WESTERN BLOT%'
group by patient_id having count(distinct native_name)>=2) t4 on t0.patient_id = t4.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME IGG/IGM RFLX WESTERN BLOT%'
group by patient_id having count(distinct native_name)>=2) t5 on t0.patient_id = t5.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_17
where result_string ilike '%present%' and native_name ilike '%LYME IGM AB WESTERN BLOT REFLX-%'
group by patient_id having count(distinct native_name)>=2) t6 on t0.patient_id = t6.patient_id 
where result_string ilike '%present%' and COALESCE(t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id, t5.patient_id, t6.patient_id) is not null 
order by t0.patient_id

-- combine with mapped data (CHA)
drop table if exists lt_igm_wb;
CREATE TABLE lt_igm_wb AS
SELECT t0.patient_id, t0.test_name, t0.date, t0.result_string, min(t0.date) over (partition by t0.patient_id) AS igm_wb_idx_date
FROM (SELECT l.patient_id, tm.test_name, l.date, result_string
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igm_wb')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%')
UNION
SELECT patient_id, native_name, date, result_string 
FROM lt_igm_wb_temp1_17
) t0
;

-- 2017 (old)
/*
drop table if exists lt_igm_wb;
CREATE TABLE lt_igm_wb AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS igm_wb_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igm_wb')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');
*/

--2018 CHA
drop table if exists lt_igm_wb_temp_18;
CREATE TABLE lt_igm_wb_temp_18 AS
SELECT l.patient_id, tm.native_name, l.date, result_string, result_float
FROM lt_igm_wb_code tm
JOIN emr_labresult l ON tm.native_code=l.native_code
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 

drop table if exists lt_igm_wb_temp1_18;
CREATE TABLE lt_igm_wb_temp1_18 AS
select t0.*, case when t1.patient_id is not null and native_name ilike '%LYME AB WESTERN BLOT CSF%' then t0.patient_id end as pos_1, 
case when t2.patient_id is not null and native_name ilike '%LYME AB WESTERN BLOT SYN FL%' then t0.patient_id end as pos_2,
case when t3.patient_id is not null and native_name ilike '%LYME ANTIBODY WESTERN BLOT CSF%' then t0.patient_id end as pos_3,
case when t4.patient_id is not null and native_name ilike '%LYME IGG/IGM REFLEX WESTERN BLOT%' then t0.patient_id end as pos_4, 
case when t5.patient_id is not null and native_name ilike '%LYME IGG/IGM RFLX WESTERN BLOT%' then t0.patient_id end as pos_5,
case when t6.patient_id is not null and native_name ilike '%LYME IGM AB WESTERN BLOT REFLX-%' then t0.patient_id end as pos_6
from lt_igm_wb_temp_18 t0
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME AB WESTERN BLOT CSF%'
group by patient_id having count(distinct native_name)>=2) t1 on t0.patient_id = t1.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME AB WESTERN BLOT SYN FL%'
group by patient_id having count(distinct native_name)>=2) t2 on t0.patient_id = t2.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME ANTIBODY WESTERN BLOT CSF%'
group by patient_id having count(distinct native_name)>=2) t3 on t0.patient_id = t3.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME IGG/IGM REFLEX WESTERN BLOT%'
group by patient_id having count(distinct native_name)>=2) t4 on t0.patient_id = t4.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME IGG/IGM RFLX WESTERN BLOT%'
group by patient_id having count(distinct native_name)>=2) t5 on t0.patient_id = t5.patient_id 
left join (select count(distinct native_name) as cnt, patient_id from lt_igm_wb_temp_18
where result_string ilike '%present%' and native_name ilike '%LYME IGM AB WESTERN BLOT REFLX-%'
group by patient_id having count(distinct native_name)>=2) t6 on t0.patient_id = t6.patient_id 
where result_string ilike '%present%' and COALESCE(t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id, t5.patient_id, t6.patient_id) is not null 
order by t0.patient_id

drop table if exists lt_igm_wb_18;
CREATE TABLE lt_igm_wb_18 AS
SELECT t0.patient_id, t0.test_name, t0.date, t0.result_string, min(t0.date) over (partition by t0.patient_id) AS igm_wb_idx_date
FROM (SELECT l.patient_id, tm.test_name, l.date, result_string
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igm_wb')
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%')
UNION
SELECT patient_id, native_name, date, result_string 
FROM lt_igm_wb_temp1_18
) t0
;
-- 2018 (old)
/*
drop table if exists lt_igm_wb_18;
CREATE TABLE lt_igm_wb_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS igm_wb_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igm_wb')
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');
*/

-- pcr 
-- 2017
drop table if exists lt_pcr;
CREATE TABLE lt_pcr AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS pcr_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');

-- 2018
drop table if exists lt_pcr_18;
CREATE TABLE lt_pcr_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS pcr_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');

-- REF: pick up any result just in case
drop table if exists lyme_lt1_all_result;
CREATE TABLE lyme_lt1_all_result AS
SELECT l.patient_id, test_name, h.name, l.date, result_string, result_float
FROM emr_labresult l
JOIN conf_labtestmap tm ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia', 'lyme_igg_imwb', 
'lyme_igm_wb', 'lyme_igg_wb', 'lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2017' AND '12-31-2018' 
order by patient_id

-- additional lab tests
/*
drop table if exists lyme_lt1_all_result_tmp;
CREATE TABLE lyme_lt1_all_result_tmp AS
SELECT l.patient_id, tm.native_name, l.date, result_string, result_float
FROM emr_labresult l
JOIN emr_labtestconcordance tm ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.native_code not in (select distinct native_code from conf_labtestmap where test_name ilike '%lyme%') and
(tm.native_name ilike '%lyme%' or tm.native_name ilike '%borr%' or tm.native_name ilike '%burg%') 
AND l.date BETWEEN '01-01-2017' AND '12-31-2018' 
order by patient_id

SELECT distinct l.native_name, l.native_code, result_string, result_float
FROM emr_labresult l
WHERE l.native_code in ('86318--5353',
'87476--926',
'LAB1340--21035',
'MR0001--4464',
'87168--926',
'87798--21010',
'LAB687--21306',
'86318--4464',
'86618--4464') order by native_name

SELECT l.patient_id, tm.native_name, l.date, result_string, result_float, name
FROM emr_labresult l
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE l.native_code in ('86318--5353',
'87476--926',
'LAB1340--21035',
'MR0001--4464',
'87168--926',
'87798--21010',
'LAB687--21306',
'86318--4464',
'86618--4464') order by native_name

-- check the unmapped tests
select native_name, native_code
from emr_labtestconcordance
where native_code not in (select distinct native_code from conf_labtestmap where test_name ilike '%lyme%') and
(native_name ilike '%lyme%' or native_name ilike '%borr%' or native_name ilike '%burg%');
*/

--
-- All lt2
--
-- for CHA (above query is slow)
-- select lab test first
drop table if exists lyme_lt2_temp1;
CREATE TABLE lyme_lt2_temp1 AS
SELECT *
FROM conf_labtestmap tm
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igm_wb', 'lyme_igg_wb', 'lyme_pcr', 'lyme_pcr_csf');

-- pick positive results 
-- 2017 
/* (old)
drop table if exists lyme_lt2;
CREATE TABLE lyme_lt2 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt2_idx_date
FROM lyme_lt2_temp1 tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igm_wb', 'lyme_igg_wb', 'lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');
*/

drop table if exists lyme_lt2;
CREATE TABLE lyme_lt2 AS
SELECT t0.patient_id, t0.test_name, t0.date, t0.result_string, min(t0.date) over (partition by t0.patient_id) AS lt2_idx_date
FROM (
select patient_id, test_name, date, result_string
from lt_igg_wb
union
select patient_id, test_name, date, result_string
from lt_igm_wb
union
select patient_id, test_name, date, result_string
from lt_pcr) t0;

-- 2018
/*
drop table if exists lyme_lt2_18;
CREATE TABLE lyme_lt2_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt2_idx_date
FROM lyme_lt2_temp1 tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igm_wb', 'lyme_igg_wb', 'lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');
*/

drop table if exists lyme_lt2_18;
CREATE TABLE lyme_lt2_18 AS
SELECT t0.patient_id, t0.test_name, t0.date, t0.result_string, min(t0.date) over (partition by t0.patient_id) AS lt2_idx_date
FROM (
select patient_id, test_name, date, result_string
from lt_igg_wb_18
union
select patient_id, test_name, date, result_string
from lt_igm_wb_18
union
select patient_id, test_name, date, result_string
from lt_pcr_18) t0;

--
-- antibiotics prescription
-- temp table pick up brand name
drop table if exists lyme_abx_rx;
CREATE TABLE lyme_abx_rx AS
SELECT DISTINCT rx.patient_id, rx.date as rx_date, rx.name as rx_name, rx.directions, rx.quantity, ds.generic_name, rx.quantity_float, rx.start_date, rx.end_date
FROM emr_prescription rx
JOIN static_drugsynonym ds on rx.name ilike '%' || ds.other_name || '%'  
WHERE (generic_name ilike '%Doxycycline%' or generic_name ilike '%Amoxicillin%' or generic_name  ilike '%Cefuroxime%' or
generic_name  ilike '%Ceftriaxone%' or generic_name  ilike '%Cefotaxime%' or generic_name  ilike '%Azithromycin%' or
generic_name  ilike '%Tetracycline%' )
AND rx.date >= '01-01-2017';

-- create lyme_abx_rx_2017
drop table if exists lyme_abx_rx_17;
CREATE TABLE lyme_abx_rx_17 AS 
SELECT * FROM lyme_abx_rx
WHERE rx_date BETWEEN '01-01-2017' and '12-31-2017'

-- create lyme_abx_rx_2018
drop table if exists lyme_abx_rx_18;
CREATE TABLE lyme_abx_rx_18 AS 
SELECT * FROM lyme_abx_rx
WHERE rx_date BETWEEN '01-01-2018' and '12-31-2018'

/* 1. pick up medication from hef_event 
drop table if exists lyme_abx;
CREATE TABLE lyme_abx AS
SELECT DISTINCT ON (rx.id) h.patient_id, h.date abx_dt, h.name, rx.name med_name, rx.quantity
FROM hef_event h 
LEFT JOIN emr_prescription rx ON h.patient_id = rx.patient_id AND rx.id = h.object_id 
WHERE h.name ilike '%Doxycycline%' or h.name ilike '%Amoxicillin%' or h.name ilike '%Cefuroxime%' or
 h.name ilike '%Ceftriaxone%' or h.name ilike '%Cefotaxime%' or h.name ilike '%Azithromycin%' or
 h.name ilike '%Tetracycline%' 
ORDER BY rx.id, rx.name;

-- Ref: checking static_drugsynonym amd emr_prescription

select * from static_drugsynonym
where generic_name ilike '%Doxycycline%' or generic_name ilike '%Amoxicillin%' or generic_name  ilike '%Cefuroxime%' or
generic_name  ilike '%Ceftriaxone%' or generic_name  ilike '%Cefotaxime%' or generic_name  ilike '%Azithromycin%' or
generic_name  ilike '%Tetracycline%' 

select distinct h.name from emr_prescription h
WHERE h.name ilike '%Doxycycline%' or h.name ilike '%Amoxicillin%' or h.name ilike '%Cefuroxime%' or
 h.name ilike '%Ceftriaxone%' or h.name ilike '%Cefotaxime%' or h.name ilike '%Azithromycin%' or
 h.name ilike '%Tetracycline%' 
*/

-- 
-- Lyme ICD and Abx w/in 14 days of each other and select the first qualified date 
-- 2017
drop table if exists lyme_rx_qualified;
CREATE TABLE lyme_rx_qualified AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) AS lyme_dx_abx_14, lyme_idx_date, rx_date, rx_name 
FROM lyme_abx_rx_17 t0
JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id
AND t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
Order BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 2018
drop table if exists lyme_rx_qualified_18;
CREATE TABLE lyme_rx_qualified_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) AS lyme_dx_abx_14, lyme_idx_date, rx_date, rx_name 
FROM lyme_abx_rx_18 t0
JOIN lyme_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id
AND t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
Order BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

--
-- Lyme EIA/ELISA/IFA+ and Abx w/in 14 days and 30 days of any test date, select the first 
-- 14 days of any lab test1 dates and pick the first date 
-- 2017
drop table if exists ll1_rx_q14;
CREATE TABLE ll1_rx_q14 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt1_abx_14, lt1_idx_date, rx_date, rx_name
FROM lyme_abx_rx_17 t0
JOIN lyme_lt1 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;


--Lyme EIA/ELISA/IFA+ and Abx w/in 14 days and 30 days of any test date, select the first 
-- 14 days of any lab test1 dates and pick the first date 
-- 2018
drop table if exists ll1_rx_q14_18;
CREATE TABLE ll1_rx_q14_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt1_abx_14, lt1_idx_date, rx_date, rx_name
FROM lyme_abx_rx_18 t0
JOIN lyme_lt1_18 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 30 days of any lab test1 dates 
-- 2017
drop table if exists ll1_rx_q30;
CREATE TABLE ll1_rx_q30 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt1_abx_30, lt1_idx_date, rx_date, rx_name
FROM lyme_abx_rx_17 t0
JOIN lyme_lt1 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '30 days') and (t1.date + INTERVAL '30 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 2018
drop table if exists ll1_rx_q30_18;
CREATE TABLE ll1_rx_q30_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt1_abx_30, lt1_idx_date, rx_date, rx_name
FROM lyme_abx_rx_18 t0
JOIN lyme_lt1_18 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '30 days') and (t1.date + INTERVAL '30 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

--Lyme WB/PCR and Abx w/in 14d of each other 
-- 2017
drop table if exists ll2_rx_q14;
CREATE TABLE ll2_rx_q14 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt2_abx_14, lt2_idx_date, rx_date, rx_name
FROM lyme_abx_rx_17 t0
JOIN lyme_lt2 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 2018
drop table if exists ll2_rx_q14_18;
CREATE TABLE ll2_rx_q14_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt2_abx_14, lt2_idx_date, rx_date, rx_name
FROM lyme_abx_rx_18 t0
JOIN lyme_lt2_18 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 30 days of any lab test2 dates 
-- 2017
drop table if exists ll2_rx_q30;
CREATE TABLE ll2_rx_q30 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt2_abx_30, lt2_idx_date, rx_date, rx_name
FROM lyme_abx_rx_17 t0
JOIN lyme_lt2 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '30 days') and (t1.date + INTERVAL '30 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 2018
drop table if exists ll2_rx_q30_18;
CREATE TABLE ll2_rx_q30_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt2_abx_30, lt2_idx_date, rx_date, rx_name
FROM lyme_abx_rx_18 t0
JOIN lyme_lt2_18 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '30 days') and (t1.date + INTERVAL '30 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- Lyme ICD + Single dose Doxy Same Day (one-time and single dose)
-- CHA 2017 
drop table if exists lyme_doxy_rx_qualified;
CREATE TABLE lyme_doxy_rx_qualified AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.lyme_idx_date, t1.date AS lyme_dx_doxy_same_dt, t0.rx_name
FROM lyme_abx_rx_17 t0
JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) or (end_date-start_date <=1)) and (directions ilike '%1 dose%' or quantity = '2 capsule')
order by t0.patient_id, t1.date asc 

--CHA 2018
drop table if exists lyme_doxy_rx_qualified_18;
CREATE TABLE lyme_doxy_rx_qualified_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.lyme_idx_date, t1.date AS lyme_dx_doxy_same_dt, t0.rx_name
FROM lyme_abx_rx_18 t0
JOIN lyme_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) and (end_date-start_date <=1)) 
order by t0.patient_id, t1.date asc 

/*
-- check CHA unique directions
SELECT distinct directions, start_date, end_date, (end_date - start_date) as dur, quantity, quantity_float
FROM lyme_abx_rx_18 t0
JOIN lyme_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' order by quantity_float

-- to check (atrius)
SELECT t0.*
FROM lyme_abx_rx_17 t0
JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) or (end_date-start_date <=1)) and (directions ilike '%1 dose%' or quantity = '2 capsule')

--2018
SELECT distinct directions, quantity, quantity_float, (end_date - start_date) as dur
FROM lyme_abx_rx_18 t0
JOIN lyme_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) or (end_date-start_date <=1)) 

-- invest one-time, single dose doxy on the same day as lyme icd
-- pick up doxy rx

drop table if exists lyme_dox_rx;
CREATE TABLE lyme_dox_rx AS
SELECT DISTINCT *
FROM emr_prescription rx
JOIN static_drugsynonym ds on rx.name ilike '%' || ds.other_name || '%'  
WHERE generic_name ilike '%Doxycycline%' AND rx.date >= '01-01-2017';

drop table if exists lyme_doxy_rx_inv;
CREATE TABLE lyme_doxy_rx_inv AS
SELECT t0.*
FROM lyme_dox_rx t0
JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.date = t1.date 
where t0.generic_name ilike 'doxycycline';

select distinct directions, (end_date - start_date) as dur, quantity, quantity_float from lyme_doxy_rx_inv 
where (quantity_float <=2) or (end_date-start_date <=1)
*/

--Tick Bite ICD + Doxy Same Day (need single dose)
--CHA 2017 
drop table if exists tick_doxy_rx_qualified;
CREATE TABLE tick_doxy_rx_qualified AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.tick_idx_date, t1.date AS tick_dx_doxy_same_dt, t0.rx_name
FROM lyme_abx_rx_17 t0
JOIN tick_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) or (end_date-start_date <=1)) and 
(directions ilike '%once%' or quantity_float = 2)
order by t0.patient_id, date asc;

-- CHA 2018
drop table if exists tick_doxy_rx_qualified_18;
CREATE TABLE tick_doxy_rx_qualified_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.tick_idx_date, t1.date AS tick_dx_doxy_same_dt, t0.rx_name
FROM lyme_abx_rx_18 t0
JOIN tick_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) or (end_date-start_date <=1)) and quantity_float <> 210.
order by t0.patient_id, date asc;

/*
SELECT distinct directions, quantity, quantity_float, (end_date - start_date) as dur
FROM lyme_abx_rx_18 t0
JOIN tick_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) or (end_date-start_date <=1)) and (directions not ilike '%days%' and quantity not in ('60 tablet', '28 capsule'))
*/

-- Tick Bite ICD + Other Lyme Abx Same Day 
-- 2017
drop table if exists tick_other_rx_qualified;
CREATE TABLE tick_other_rx_qualified AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.tick_idx_date, t1.date AS tick_dx_other_same_dt, t0.rx_name
FROM lyme_abx_rx_17 t0
JOIN tick_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name not ilike 'doxycycline' 
order by t0.patient_id, date asc;

-- 2018
drop table if exists tick_other_rx_qualified_18;
CREATE TABLE tick_other_rx_qualified_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.tick_idx_date, t1.date AS tick_dx_other_same_dt, t0.rx_name
FROM lyme_abx_rx_18 t0
JOIN tick_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name not ilike 'doxycycline' 
order by t0.patient_id, date asc;

-- join icd, lab test tables
/*
drop table if exists lyme_idx_patient ;
CREATE TABLE lyme_idx_patient AS
SELECT distinct COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id) as patient_id,
t0.tick_idx_date, t1.lyme_idx_date, t2.lt1_idx_date, t3.lt2_idx_date
FROM tick_dx_enc_idx t0
FULL JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
FULL JOIN lyme_lt1 t2 on t2.patient_id = COALESCE(t0.patient_id, t1.patient_id)
FULL JOIN lyme_lt2 t3 on t3.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id) 
order by patient_id;
*/

-- updated to separate out lt2 tests
-- 2017
drop table if exists lyme_idx_patient ;
CREATE TABLE lyme_idx_patient AS
SELECT distinct COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id, t5.patient_id) as patient_id,
t0.tick_idx_date, t1.lyme_idx_date, t2.lt1_idx_date, t3.igg_wb_idx_date, t4.igm_wb_idx_date, t5.pcr_idx_date
FROM tick_dx_enc_idx t0
FULL JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
FULL JOIN lyme_lt1 t2 on t2.patient_id = COALESCE(t0.patient_id, t1.patient_id)
FULL JOIN lt_igg_wb t3 on t3.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id) 
FULL JOIN lt_igm_wb t4 on t4.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id) 
FULL JOIN lt_pcr t5 on t5.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id) 
order by patient_id;

-- 2018
drop table if exists lyme_idx_patient_18 ;
CREATE TABLE lyme_idx_patient_18 AS
SELECT distinct COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id, t5.patient_id) as patient_id,
t0.tick_idx_date, t1.lyme_idx_date, t2.lt1_idx_date, t3.igg_wb_idx_date, t4.igm_wb_idx_date, t5.pcr_idx_date
FROM tick_dx_enc_idx_18 t0
FULL JOIN lyme_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
FULL JOIN lyme_lt1_18 t2 on t2.patient_id = COALESCE(t0.patient_id, t1.patient_id)
FULL JOIN lt_igg_wb_18 t3 on t3.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id) 
FULL JOIN lt_igm_wb_18 t4 on t4.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id) 
FULL JOIN lt_pcr_18 t5 on t5.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id) 
order by patient_id;

-- pick up rest of the stuff
-- 2017
drop table if exists lyme_idx_patient_all ;
CREATE TABLE lyme_idx_patient_all AS
SELECT distinct t0.*, t1.lyme_dx_abx_14, t2.lt1_abx_14, t3.lt1_abx_30, t4.lt2_abx_14, t5.lt2_abx_30,
       t6.lyme_dx_doxy_same_dt, t7.tick_dx_doxy_same_dt, t8.tick_dx_other_same_dt
FROM lyme_idx_patient t0
LEFT JOIN lyme_rx_qualified t1 on t0.patient_id = t1.patient_id
LEFT JOIN ll1_rx_q14 t2 on t0.patient_id = t2.patient_id
LEFT JOIN ll1_rx_q30 t3 on t0.patient_id = t3.patient_id
LEFT JOIN ll2_rx_q14 t4 on t0.patient_id = t4.patient_id
LEFT JOIN ll2_rx_q30 t5 on t0.patient_id = t5.patient_id
LEFT JOIN lyme_doxy_rx_qualified t6 on t0.patient_id = t6.patient_id
LEFT JOIN tick_doxy_rx_qualified t7 on t0.patient_id = t7.patient_id
LEFT JOIN tick_other_rx_qualified t8 on t0.patient_id = t8.patient_id
 
-- 2018
 drop table if exists lyme_idx_patient_all_18 ;
CREATE TABLE lyme_idx_patient_all_18 AS
SELECT distinct t0.*, t1.lyme_dx_abx_14, t2.lt1_abx_14, t3.lt1_abx_30, t4.lt2_abx_14, t5.lt2_abx_30,
       t6.lyme_dx_doxy_same_dt, t7.tick_dx_doxy_same_dt, t8.tick_dx_other_same_dt
FROM lyme_idx_patient_18 t0
LEFT JOIN lyme_rx_qualified_18 t1 on t0.patient_id = t1.patient_id
LEFT JOIN ll1_rx_q14_18 t2 on t0.patient_id = t2.patient_id
LEFT JOIN ll1_rx_q30_18 t3 on t0.patient_id = t3.patient_id
LEFT JOIN ll2_rx_q14_18 t4 on t0.patient_id = t4.patient_id
LEFT JOIN ll2_rx_q30_18 t5 on t0.patient_id = t5.patient_id
LEFT JOIN lyme_doxy_rx_qualified_18 t6 on t0.patient_id = t6.patient_id
LEFT JOIN tick_doxy_rx_qualified_18 t7 on t0.patient_id = t7.patient_id
LEFT JOIN tick_other_rx_qualified_18 t8 on t0.patient_id = t8.patient_id

-- output run this for 2017 and 2018
-- 2017
drop table if exists lyme_ll_output_17;
CREATE TABLE lyme_ll_output_17 AS
SELECT 
concat(T1.last_name, ', ', T1.first_name) as name,
T1.mrn,
date_of_birth as dob,
concat(T2.last_name, ', ', T2.first_name, ' ', T2.title) as pcp_name,
T2.dept as pcp_site,
gender as sex,
t5.*
FROM emr_patient T1
LEFT JOIN emr_provider T2 on (T1.pcp_id = T2.id)
JOIN lyme_idx_patient_all T5 on t1.id = t5.patient_id
WHERE substr(t1.gender,1,1) in ('M', 'F')
AND T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%';

-- 2018
drop table if exists lyme_ll_output_18;
CREATE TABLE lyme_ll_output_18 AS
SELECT 
concat(T1.last_name, ', ', T1.first_name) as name,
T1.mrn,
date_of_birth as dob,
concat(T2.last_name, ', ', T2.first_name, ' ', T2.title) as pcp_name,
T2.dept as pcp_site,
gender as sex,
t5.*
FROM emr_patient T1
LEFT JOIN emr_provider T2 on (T1.pcp_id = T2.id)
JOIN lyme_idx_patient_all_18 T5 on t1.id = t5.patient_id
WHERE substr(t1.gender,1,1) in ('M', 'F')
AND T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%';

--final output (updated)
drop table if exists lyme_output_17;
CREATE TABLE lyme_output_17 AS
SELECT 
name,
mrn,
dob,
sex,
pcp_name,
pcp_site,
tick_idx_date,
lyme_idx_date,
lt1_idx_date,
igg_wb_idx_date, 
igm_wb_idx_date, 
pcr_idx_date,
lyme_dx_abx_14,
lt1_abx_14,
lt2_abx_14,
lt1_abx_30,
lt2_abx_30,
lyme_dx_doxy_same_dt,
tick_dx_doxy_same_dt,
tick_dx_other_same_dt
FROM lyme_ll_output_17;

--2018
drop table if exists lyme_output_18;
CREATE TABLE lyme_output_18 AS
SELECT 
name,
mrn,
dob,
sex,
pcp_name,
pcp_site,
tick_idx_date,
lyme_idx_date,
lt1_idx_date,
igg_wb_idx_date, 
igm_wb_idx_date, 
pcr_idx_date,
lyme_dx_abx_14,
lt1_abx_14,
lt2_abx_14,
lt1_abx_30,
lt2_abx_30,
lyme_dx_doxy_same_dt,
tick_dx_doxy_same_dt,
tick_dx_other_same_dt
FROM lyme_ll_output_18;


--count

select count(*) as total, 
sum(case when tick_idx_date is not null then 1 else 0 end) as tick_dx, 
sum(case when lyme_idx_date is not null then 1 else 0 end) as lyme_dx, 
sum(case when lt1_idx_date is not null then 1 else 0 end) as lt1, 
sum(case when igg_wb_idx_date is not null then 1 else 0 end) as igg, 
sum(case when igm_wb_idx_date is not null then 1 else 0 end) as igm, 
sum(case when pcr_idx_date is not null then 1 else 0 end) as prc, 
sum(case when lyme_dx_abx_14 is not null then 1 else 0 end) as lyme_dose, 
sum(case when lt1_abx_14 is not null then 1 else 0 end) as lt1_14, 
sum(case when lt2_abx_14 is not null then 1 else 0 end) as lt2_14, 
sum(case when lt1_abx_30 is not null then 1 else 0 end) as lt1_30, 
sum(case when lt2_abx_30 is not null then 1 else 0 end) as lt2_30, 
sum(case when lyme_dx_doxy_same_dt is not null then 1 else 0 end) as lyme_same_dose, 
sum(case when tick_dx_doxy_same_dt is not null then 1 else 0 end) as tick_same_dose,
sum(case when tick_dx_other_same_dt is not null then 1 else 0 end) as tick_other_dose
from lyme_ll_output_17




-- look at the results for those not mapped

/*
--alternatively
drop table if exists lyme_ll_output_17;
CREATE TABLE lyme_ll_output_17 AS
SELECT 
concat(T1.last_name, ', ', T1.first_name) as name,
T1.mrn,
date_of_birth as dob,
concat(T2.last_name, ', ', T2.first_name, ' ', T2.title) as pcp_name,
T2.dept as pcp_site,
gender as sex,
t5.*
FROM emr_patient T1
LEFT JOIN emr_provider T2 on (T1.pcp_id = T2.id)
--JOIN emr_encounter T3 on (T1.id = T3.patient_id)
--LEFT JOIN static_enc_type_lookup T4 on (T3.raw_encounter_type = T4.raw_encounter_type)
JOIN lyme_idx_patient_all T5 on t1.id = t5.patient_id
WHERE gender in ('M', 'MALE', 'FEMALE', 'F')
-- must have an ambulatory encounter in the past 2 years
-- AND T4.ambulatory = 1
-- filter out test patients
AND T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%'
AND T1.last_name not ilike 'XYZ%'
AND T1.last_name not ilike 'ZZ%'
AND T1.last_name not ilike 'YY%'
AND T1.last_name not ilike 'test'
AND T1.last_name not ilike 'esp'
AND T1.first_name NOT ILIKE 'test'
AND T1.first_name not ilike 'yytest%'
AND T1.first_name not ILIKE 'zztest%';


-- invest number of pt with doxcycline
drop table if exists lyme_dox_rx;
CREATE TABLE lyme_dox_rx AS
SELECT DISTINCT *
FROM emr_prescription rx
JOIN static_drugsynonym ds on rx.name ilike '%' || ds.other_name || '%'  
WHERE generic_name ilike '%Doxycycline%' AND rx.date >= '01-01-2017';

drop table if exists tick_doxy_rx_inv;
CREATE TABLE tick_doxy_rx_inv AS
SELECT t0.* 
FROM lyme_dox_rx t0
JOIN tick_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.date = t1.date 
where t0.generic_name ilike 'doxycycline';

drop table if exists lyme_doxy_rx_inv;
CREATE TABLE lyme_doxy_rx_inv AS
SELECT t0.*
FROM lyme_dox_rx t0
JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.date = t1.date 
where t0.generic_name ilike 'doxycycline';

drop table if exists doxy_rx_inv_all;
CREATE TABLE doxy_rx_inv_all AS
select * from lyme_doxy_rx_inv
union
select * from tick_doxy_rx_inv
union 
select * from lyme_doxy_rx_inv_18
union 
select * from tick_doxy_rx_inv_18


select distinct directions, (end_date - start_date) as dur, quantity, quantity_float from doxy_rx_inv_all where true = case when quantity is null then end_date - start_date <=1 
when end_date is null or start_date is null then quantity_float <=2 else (quantity_float <=2 and end_date-start_date <=1) end

select count(distinct patient_id), 
sum(case when quantity_float is null and quantity is null then 1 else 0 end) as no_quantity, 
sum(case when end_date is null then 1 else 0 end) as no_enddt,
sum(case when start_date is null or end_date is null then 1 else 0 end) as no_dur,
sum(case when start_date is null and end_date is null and quantity_float is null and quantity is null then 1 else 0 end) as no_all
from  tick_doxy_rx_inv


select count(*) as total, sum(case when lyme_dx_doxy_same_dt is not null then 1 else 0 end) as lyme_dose, 
sum(case when tick_dx_doxy_same_dt is not null then 1 else 0 end) as tick_dose
from lyme_ll_output_17






*/