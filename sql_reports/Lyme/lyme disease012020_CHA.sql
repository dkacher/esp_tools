-- patients who have a positive Lyme disease enzyme immunoassay (EIA) test result between January 1, 2010 and June 30, 2019. 
--look into another source of  positive results - western to follow

--Specimen collection date for positive Lyme EIA 
--Specimen collection date for positive Lyme ELISA
--Result of Lyme EIA (positive/)
--Result of Lyme ELISA (positive/WesternToFollow/For results see Western Blot/see Western Blot/see Western Blot for result(s)/See Western Blot result(s)/Western Pending)
-- found 2 set of result associated with ELISA on the same date (one IGM, another total)

-- add order natural key to distinguish different test 
-- each row represent all the result assocaited with the test on the same date with the same order key 
/*
-- provider ID
SELECT T1.patient_id,T1.date,T1.name,T1.id hef_id,T1.object_id lab_result_id,T1.provider_id,T2.specimen_source
 FROM public.hef_event T1 INNER JOIN public.emr_labresult T2 ON ((T1.object_id = T2.id) AND (T1.patient_id = T2.patient_id)) 
 WHERE T1.date >= '01-01-2014'  and name in ('lx:gonorrhea:positive', 'lx:gonorrhea:negative') ;

-- provider location
SELECT DISTINCT ON (T1.patient_id) T1.patient_id,T1.date as first_pos_date,T1.lab_result_id first_pos_lab_result_id,
T1.hef_id first_pos_hef_id,T1.specimen_source,
T2.natural_key as provider_id,
T2.dept as provider_location 
FROM kregonrein_a_100027_s_9 T1, public.emr_provider T2
WHERE (T1.provider_id = T2.id) 
and  T1.name =  'lx:gonorrhea:positive' 
ORDER BY T1.patient_id, T1.date, T1.hef_id;
*/

-- patients who have positive screening results (result_date)
drop table if exists pos_screen_tmp0;
CREATE TABLE pos_screen_tmp0 AS
SELECT distinct l.patient_id, l.provider_id, tm.test_name as screen_test, result_string as screen_result, collection_date as screen_specimen_date, 
h.name as hef_event_name, l.native_name, l.result_date,
case when l.native_name ilike '%total%' then 'Total: '||result_string else 'IgM: '||result_string end as test_label, content_type_id,
 order_natural_key
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
WHERE tm.test_name IN ('lyme_igm_eia', 'lyme_igg_eia', 'lyme_elisa') 
AND l.result_date BETWEEN '01-01-2010' AND '12-31-2019' 
AND (((result_string ilike '%positive%' or h.name ilike '%positive%') and h.name not ilike '%any%') or (result_string ilike '%western%'));

-- pick up provider location as well (1/21/20)
drop table if exists pos_screen;
CREATE TABLE pos_screen AS
SELECT distinct t0.patient_id, t0.provider_id, p.dept, t0.screen_test, hef_event_name, screen_result, test_label, screen_specimen_date, 
order_natural_key, result_date
FROM pos_screen_tmp0 t0
JOIN django_content_type d on d.id = t0.content_type_id and d.model = 'labresult'
JOIN emr_provider p on t0.provider_id = p.id 

-- aggregate results
/*
drop table if exists pos_screen_agg;
CREATE TABLE pos_screen_agg AS
select patient_id, screen_test, screen_specimen_date, order_natural_key, string_agg(hef_event_name, '|' order by updated_timestamp) as hef_event_name_screen, 
string_agg(test_label, '|' order by updated_timestamp) as screen_result_list
from pos_screen
group by patient_id, screen_test, screen_specimen_date, order_natural_key
*/

drop table if exists pos_screen_agg; 
CREATE TABLE pos_screen_agg AS
select patient_id, screen_test, provider_id, dept, screen_specimen_date, order_natural_key, 
string_agg(hef_event_name, '|' ) as hef_event_name_screen, 
string_agg(distinct test_label, '|' ) as screen_result_list, max(result_date) as result_date
from pos_screen
group by patient_id, screen_test, provider_id, dept, screen_specimen_date, order_natural_key


-- For Lyme IgG and IgM Western Blot specimens collected up to 30 days after the specimen collection date for positive Lyme EIA please provide
-- a.Specimen collection date for Lyme IgG Western Blot 
-- b.Result of Lyme IgG Western Blot test
-- c.Specimen collection date for Lyme IgM Western Blot
-- d.Result of Lyme IgM Western Blot
-- **  and result_string not in ('tnp','TNP','Not tested','Test not done')

drop table if exists lyme_wb_t1;
CREATE TABLE lyme_wb_t1 AS
SELECT distinct t0.patient_id, t0.screen_test, screen_specimen_date, hef_event_name_screen, t0.provider_id, t0.dept,
t0.screen_result_list, tm.test_name, h.name as hef_event_name_wb, l.result_string, 
l.collection_date as wb_specimen_date, content_type_id, l.order_natural_key
FROM pos_screen_agg t0
JOIN emr_labresult l ON t0.patient_id = l.patient_id
JOIN conf_labtestmap tm on tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb', 'lyme_igm_wb') and 
l.collection_date between t0.screen_specimen_date and (t0.screen_specimen_date+ INTERVAL '30 days')
and result_string not in ('tnp','TNP','Not tested','Test not done') and h.name is not null;

drop table if exists lyme_wb;
CREATE TABLE lyme_wb AS
SELECT distinct t0.patient_id, t0.screen_test, t0.screen_specimen_date, hef_event_name_screen, t0.provider_id, t0.dept,
t0.screen_result_list, t0.test_name, hef_event_name_wb, order_natural_key,
t0.result_string, wb_specimen_date
FROM lyme_wb_t1 t0
JOIN django_content_type t1 on t1.id = t0.content_type_id and t1.model = 'labresult';

-- aggregate result based on order key

drop table if exists lyme_wb_agg;
CREATE TABLE lyme_wb_agg AS
select patient_id, screen_test, screen_specimen_date, hef_event_name_screen, screen_result_list, provider_id, dept,
order_natural_key, test_name, wb_specimen_date,
string_agg(hef_event_name_wb, '|') as hef_event_name_wb,
string_agg(distinct result_string, '|') as result_string
from lyme_wb
group by patient_id, screen_test, screen_specimen_date, hef_event_name_screen, screen_result_list, provider_id, dept,
order_natural_key, test_name, wb_specimen_date


/*
drop table if exists lyme_wb_rnk;
CREATE TABLE lyme_wb_rnk AS
SELECT t0.*,ROW_NUMBER() OVER ( PARTITION BY t0.patient_id, t0.screen_specimen_date, t0.screen_test, t0.screen_result_list ORDER BY t0.wb_specimen_date, t0.test_name) rnk
FROM lyme_wb t0 
*/

drop table if exists lyme_wb_rnk;
CREATE TABLE lyme_wb_rnk AS
SELECT t0.patient_id, t0.screen_test, date(t0.screen_specimen_date) as screen_specimen_date , hef_event_name_screen, t0.screen_result_list, 
t0.test_name, hef_event_name_wb, t0.result_string, date(wb_specimen_date) as wb_specimen_date , 
ROW_NUMBER() OVER ( PARTITION BY t0.patient_id, t0.screen_specimen_date, t0.screen_test, t0.screen_result_list ORDER BY t0.wb_specimen_date, t0.test_name) rnk
FROM lyme_wb_agg t0 ;

-- transpose to columns 
drop table if exists lyme_wb_t;
CREATE TABLE lyme_wb_t AS
SELECT patient_id, screen_specimen_date, screen_test, screen_result_list,
MAX(CASE WHEN rnk ='1' THEN test_name ELSE NULL END) wb1_name,
MAX(CASE WHEN rnk ='1' THEN wb_specimen_date ELSE NULL END) wb1_specimen_date,
MAX(CASE WHEN rnk ='1' THEN hef_event_name_wb ELSE NULL END) wb1_hef_event_name,
MAX(CASE WHEN rnk ='1' THEN result_string ELSE NULL END) wb1_result,
MAX(CASE WHEN rnk ='2' THEN test_name ELSE NULL END) wb2_name,
MAX(CASE WHEN rnk ='2' THEN wb_specimen_date ELSE NULL END) wb2_specimen_date,
MAX(CASE WHEN rnk ='2' THEN hef_event_name_wb ELSE NULL END) wb2_hef_event_name,
MAX(CASE WHEN rnk ='2' THEN result_string ELSE NULL END) wb2_result,
MAX(CASE WHEN rnk ='3' THEN test_name ELSE NULL END) wb3_name,
MAX(CASE WHEN rnk ='3' THEN wb_specimen_date ELSE NULL END) wb3_specimen_date,
MAX(CASE WHEN rnk ='3' THEN hef_event_name_wb ELSE NULL END) wb3_hef_event_name,
MAX(CASE WHEN rnk ='3' THEN result_string ELSE NULL END) wb3_result,
MAX(CASE WHEN rnk ='4' THEN test_name ELSE NULL END) wb4_name,
MAX(CASE WHEN rnk ='4' THEN wb_specimen_date ELSE NULL END) wb4_specimen_date,
MAX(CASE WHEN rnk ='4' THEN hef_event_name_wb ELSE NULL END) wb4_hef_event_name,
MAX(CASE WHEN rnk ='4' THEN result_string ELSE NULL END) wb4_result,
MAX(CASE WHEN rnk ='5' THEN test_name ELSE NULL END) wb5_name,
MAX(CASE WHEN rnk ='5' THEN wb_specimen_date ELSE NULL END) wb5_specimen_date,
MAX(CASE WHEN rnk ='5' THEN hef_event_name_wb ELSE NULL END) wb5_hef_event_name,
MAX(CASE WHEN rnk ='5' THEN result_string ELSE NULL END) wb5_result,
MAX(CASE WHEN rnk ='6' THEN test_name ELSE NULL END) wb6_name,
MAX(CASE WHEN rnk ='6' THEN wb_specimen_date ELSE NULL END) wb6_specimen_date,
MAX(CASE WHEN rnk ='6' THEN hef_event_name_wb ELSE NULL END) wb6_hef_event_name,
MAX(CASE WHEN rnk ='6' THEN result_string ELSE NULL END) wb6_result
FROM lyme_wb_rnk 
GROUP BY patient_id, screen_specimen_date, screen_test, screen_result_list ORDER BY patient_id, screen_specimen_date 

drop table if exists lyme_lab;
CREATE TABLE lyme_lab AS
SELECT distinct t0.patient_id, t0.provider_id, t0.dept, t0.screen_test, date(t0.screen_specimen_date) as screen_specimen_date, 
t0.hef_event_name_screen, t0.screen_result_list, date(t0.result_date) as result_date,
wb1_name,
wb1_specimen_date,
wb1_hef_event_name,
wb1_result,
wb2_name,
wb2_specimen_date,
wb2_hef_event_name,
wb2_result,
wb3_name,
wb3_specimen_date,
wb3_hef_event_name,
wb3_result,
wb4_name,
wb4_specimen_date,
wb4_hef_event_name,
wb4_result,
wb5_name,
wb5_specimen_date,
wb5_hef_event_name,
wb5_result,
wb6_name,
wb6_specimen_date,
wb6_hef_event_name,
wb6_result
FROM pos_screen_agg t0
LEFT JOIN lyme_wb_t t1 on t0.patient_id = t1.patient_id and date(t0.screen_specimen_date)=t1.screen_specimen_date and t0.screen_test = t1.screen_test and t0.screen_result_list = t1.screen_result_list


--Lyme disease antibiotics ordered within 30 days of positive/WesternToFollow Lyme EIA:
--antibiotics prescription, pick up rx info
--Date of Doxycycline order
--Number of days of Doxycycline ordered
--Date of Amoxicillin order
--Number of days of Amoxicillin ordered
--Date of Ceftriaxone order
--Number of days of Ceftriaxone ordered
--Date of Cefuroxime order
--Number of days of Cefuroxime ordered
--Date of Cefotaxime order
--Number of days of Cefotaxime ordered
--Date of Azithromycin order
--Number of days of Azithromycin ordered
--Date of Tetracycline order
--Number of days of Tetracycline ordered

drop table if exists lyme_rx;
CREATE TABLE lyme_rx AS
SELECT DISTINCT t0.patient_id, t0.screen_specimen_date, t0.screen_test, t0.screen_result_list, rx.date as rx_date, ds.generic_name, (rx.end_date - rx.start_date) as rx_dur
FROM lyme_lab t0
JOIN emr_prescription rx on t0.patient_id = rx.patient_id
JOIN static_drugsynonym ds on rx.name ilike '%' || ds.other_name || '%'  
WHERE (generic_name ilike '%Doxycycline%' or generic_name ilike '%Amoxicillin%' or generic_name  ilike '%Cefuroxime%' or
generic_name  ilike '%Ceftriaxone%' or generic_name  ilike '%Cefotaxime%' or generic_name  ilike '%Azithromycin%' or
generic_name  ilike '%Tetracycline%' ) and rx.date between (t0.result_date - INTERVAL '30 days') and (t0.result_date + INTERVAL '30 days');


drop table if exists lyme_rx_rnk;
CREATE TABLE lyme_rx_rnk AS
SELECT t0.*,ROW_NUMBER() OVER ( PARTITION BY t0.patient_id, t0.screen_specimen_date, t0.screen_test, t0.screen_result_list ORDER BY t0.rx_date, t0.generic_name, rx_dur) rnk
FROM lyme_rx t0 

drop table if exists lyme_rx_t;
CREATE TABLE lyme_rx_t AS
SELECT patient_id, screen_specimen_date, screen_test, screen_result_list,
MAX(CASE WHEN rnk ='1' THEN generic_name ELSE NULL END) rx1,
MAX(CASE WHEN rnk ='1' THEN rx_date ELSE NULL END) rx1_order_date,
MAX(CASE WHEN rnk ='1' THEN rx_dur ELSE NULL END) rx1_duration,
MAX(CASE WHEN rnk ='2' THEN generic_name ELSE NULL END) rx2,
MAX(CASE WHEN rnk ='2' THEN rx_date ELSE NULL END) rx2_order_date,
MAX(CASE WHEN rnk ='2' THEN rx_dur ELSE NULL END) rx2_duration,
MAX(CASE WHEN rnk ='3' THEN generic_name ELSE NULL END) rx3,
MAX(CASE WHEN rnk ='3' THEN rx_date ELSE NULL END) rx3_order_date,
MAX(CASE WHEN rnk ='3' THEN rx_dur ELSE NULL END) rx3_duration,
MAX(CASE WHEN rnk ='4' THEN generic_name ELSE NULL END) rx4,
MAX(CASE WHEN rnk ='4' THEN rx_date ELSE NULL END) rx4_order_date,
MAX(CASE WHEN rnk ='4' THEN rx_dur ELSE NULL END) rx4_duration,
MAX(CASE WHEN rnk ='5' THEN generic_name ELSE NULL END) rx5,
MAX(CASE WHEN rnk ='5' THEN rx_date ELSE NULL END) rx5_order_date,
MAX(CASE WHEN rnk ='5' THEN rx_dur ELSE NULL END) rx5_duration,
MAX(CASE WHEN rnk ='6' THEN generic_name ELSE NULL END) rx6,
MAX(CASE WHEN rnk ='6' THEN rx_date ELSE NULL END) rx6_order_date,
MAX(CASE WHEN rnk ='6' THEN rx_dur ELSE NULL END) rx6_duration,
MAX(CASE WHEN rnk ='7' THEN generic_name ELSE NULL END) rx7,
MAX(CASE WHEN rnk ='7' THEN rx_date ELSE NULL END) rx7_order_date,
MAX(CASE WHEN rnk ='7' THEN rx_dur ELSE NULL END) rx7_duration,
MAX(CASE WHEN rnk ='8' THEN generic_name ELSE NULL END) rx8,
MAX(CASE WHEN rnk ='8' THEN rx_date ELSE NULL END) rx8_order_date,
MAX(CASE WHEN rnk ='8' THEN rx_dur ELSE NULL END) rx8_duration,
MAX(CASE WHEN rnk ='9' THEN generic_name ELSE NULL END) rx9,
MAX(CASE WHEN rnk ='9' THEN rx_date ELSE NULL END) rx9_order_date,
MAX(CASE WHEN rnk ='9' THEN rx_dur ELSE NULL END) rx9_duration,
MAX(CASE WHEN rnk ='10' THEN generic_name ELSE NULL END) rx10,
MAX(CASE WHEN rnk ='10' THEN rx_date ELSE NULL END) rx10_order_date,
MAX(CASE WHEN rnk ='10' THEN rx_dur ELSE NULL END) rx10_duration,
MAX(CASE WHEN rnk ='11' THEN generic_name ELSE NULL END) rx11,
MAX(CASE WHEN rnk ='11' THEN rx_date ELSE NULL END) rx11_order_date,
MAX(CASE WHEN rnk ='11' THEN rx_dur ELSE NULL END) rx11_duration,
MAX(CASE WHEN rnk ='12' THEN generic_name ELSE NULL END) rx12,
MAX(CASE WHEN rnk ='12' THEN rx_date ELSE NULL END) rx12_order_date,
MAX(CASE WHEN rnk ='12' THEN rx_dur ELSE NULL END) rx12_duration,
MAX(CASE WHEN rnk ='13' THEN generic_name ELSE NULL END) rx13,
MAX(CASE WHEN rnk ='13' THEN rx_date ELSE NULL END) rx13_order_date,
MAX(CASE WHEN rnk ='13' THEN rx_dur ELSE NULL END) rx13_duration,
MAX(CASE WHEN rnk ='14' THEN generic_name ELSE NULL END) rx14,
MAX(CASE WHEN rnk ='14' THEN rx_date ELSE NULL END) rx14_order_date,
MAX(CASE WHEN rnk ='14' THEN rx_dur ELSE NULL END) rx14_duration,
MAX(CASE WHEN rnk ='15' THEN generic_name ELSE NULL END) rx15,
MAX(CASE WHEN rnk ='15' THEN rx_date ELSE NULL END) rx15_order_date,
MAX(CASE WHEN rnk ='15' THEN rx_dur ELSE NULL END) rx15_duration,
MAX(CASE WHEN rnk ='16' THEN generic_name ELSE NULL END) rx16,
MAX(CASE WHEN rnk ='16' THEN rx_date ELSE NULL END) rx16_order_date,
MAX(CASE WHEN rnk ='16' THEN rx_dur ELSE NULL END) rx16_duration,
MAX(CASE WHEN rnk ='17' THEN generic_name ELSE NULL END) rx17,
MAX(CASE WHEN rnk ='17' THEN rx_date ELSE NULL END) rx17_order_date,
MAX(CASE WHEN rnk ='17' THEN rx_dur ELSE NULL END) rx17_duration,
MAX(CASE WHEN rnk ='18' THEN generic_name ELSE NULL END) rx18,
MAX(CASE WHEN rnk ='18' THEN rx_date ELSE NULL END) rx18_order_date,
MAX(CASE WHEN rnk ='18' THEN rx_dur ELSE NULL END) rx18_duration,
MAX(CASE WHEN rnk ='19' THEN generic_name ELSE NULL END) rx19,
MAX(CASE WHEN rnk ='19' THEN rx_date ELSE NULL END) rx19_order_date,
MAX(CASE WHEN rnk ='19' THEN rx_dur ELSE NULL END) rx19_duration,
MAX(CASE WHEN rnk ='20' THEN generic_name ELSE NULL END) rx20,
MAX(CASE WHEN rnk ='20' THEN rx_date ELSE NULL END) rx20_order_date,
MAX(CASE WHEN rnk ='20' THEN rx_dur ELSE NULL END) rx20_duration
FROM lyme_rx_rnk 
GROUP BY patient_id, screen_specimen_date, screen_test, screen_result_list ORDER BY patient_id, screen_specimen_date 

drop table if exists lyme_rx_t1;
CREATE TABLE lyme_rx_t1 AS
SELECT distinct t0.*,
 rx1, rx1_order_date, rx1_duration,
 rx2, rx2_order_date, rx2_duration,
 rx3, rx3_order_date, rx3_duration,
 rx4, rx4_order_date, rx4_duration,
 rx5, rx5_order_date, rx5_duration,
 rx6, rx6_order_date, rx6_duration,
 rx7, rx7_order_date, rx7_duration,
 rx8, rx8_order_date, rx8_duration,
 rx9, rx9_order_date, rx9_duration,
 rx10, rx10_order_date, rx10_duration,
 rx11, rx11_order_date, rx11_duration,
 rx12, rx12_order_date, rx12_duration,
 rx13, rx13_order_date, rx13_duration,
 rx14, rx14_order_date, rx14_duration,
 rx15, rx15_order_date, rx15_duration,
 rx16, rx16_order_date, rx16_duration,
 rx17, rx17_order_date, rx17_duration,
 rx18, rx18_order_date, rx18_duration,
 rx19, rx19_order_date, rx19_duration,
 rx20, rx20_order_date, rx20_duration
FROM lyme_lab t0
LEFT JOIN lyme_rx_t t1 on t0.patient_id = t1.patient_id and t0.screen_specimen_date=t1.screen_specimen_date and t0.screen_test = t1.screen_test and t0.screen_result_list = t1.screen_result_list


-- Demographics:
-- ESP internal identifier (not MRN)
-- Patient’s age in years (calculate as date of birth – date of positive EIA test)
-- Sex
-- Race
-- Ethnicity
-- 012120 wait till early Feb to complete the following

drop table if exists lyme_cha_020420;
CREATE TABLE lyme_cha_020420 AS
SELECT distinct t0.id ptid, date_part('year', age(result_date, date_of_birth)) as age, gender as sex, date_of_birth,
 case 
     when (select target_value from gen_pop_tools.tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=t0.ethnicity) is not null
	   then (select target_value from gen_pop_tools.tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=t0.ethnicity)
	 else (select target_value from gen_pop_tools.tt_racemap t00 where t00.source_field='race' and t00.source_value=t0.race)
  end as race, t0.ethnicity, t1.*
FROM emr_patient t0
JOIN lyme_rx_t1 t1 on t0.id = t1.patient_id
WHERE substr(t0.gender,1,1) in ('M', 'F')
AND T0.last_name not in ('TEST', 'TEST**')
AND T0.last_name not ilike '%ssmctest%' 
AND T0.last_name not ilike '% test%' 
AND T0.last_name not ilike 'XB%' 
AND T0.last_name not ilike 'XX%';

ALTER TABLE lyme_CHA_020420 RENAME COLUMN dept TO clinic;

ALTER TABLE lyme_cha_020420 
DROP COLUMN patient_id,
DROP COLUMN date_of_birth,
DROP COLUMN result_date,
DROP COLUMN provider_id;


-- copy to tmp folder

copy (select * 
  from lyme_cha_020420) 
  to '/tmp/lyme_cha_020420.csv' with CSV delimiter ',' header;




-- for testing
select count(*), ptid, screen_specimen_date, screen_test from lyme_cha_073019
group by ptid, screen_specimen_date, screen_test having count(*) > 1

select * from lyme_cha_073019 where hef_event_name_screen ilike '%|%' and screen_result_list not ilike '%|%'

select * from lyme_cha_073019 where wb6_name is not null or rx20 is not null

select * from lyme_wb_agg where hef_event_name_wb ilike '%|%' and result_string not ilike '%|%'

select distinct test_name, result_string, hef_event_name_wb, wb_specimen_date, order_natural_key
from lyme_wb_t1 where patient_id in (16166, 33746) order by patient_id, wb_specimen_date


-- checking reflex wb

set search_path to hcc_report, public;
drop table if exists lymetmp0;
CREATE TABLE lymetmp0 AS
select distinct l.patient_id, tm.test_name as screen_test, result_string as screen_result, collection_date as screen_specimen_date, 
h.name as hef_event_name, l.native_name,  content_type_id,
 order_natural_key, updated_timestamp
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
WHERE l.native_name ilike '%rflx western%' or  l.native_name ilike '%reflex western%'
AND l.date BETWEEN '01-01-2010' AND '06-30-2019' 


drop table if exists lymetmp1;
CREATE TABLE lymetmp1 AS
SELECT distinct patient_id, screen_test, screen_result, screen_specimen_date, 
hef_event_name, t0.native_name, specimen_num, t0.content_type_id, t0.order_natural_key, t0.updated_timestamp
FROM pos_screen_tmp0 t0
join emr_labresult t1 on t0.patient_id = t1.patient_id and t0.screen_result = t1.result_string and t0.native_name = t1.native_name and t0.screen_specimen_date = t1.collection_date

drop table if exists lymetmp2;
CREATE TABLE lymetmp2 AS
Select distinct t0.*, 
tm.test_name, h.name as hef_event_name_wb, l.result_string, l.specimen_num as wb_spec,
l.collection_date as wb_specimen_date, content_type_id, l.order_natural_key, updated_timestamp
FROM lymetmp1 t0
JOIN emr_labresult l ON t0.patient_id = l.patient_id
JOIN conf_labtestmap tm on tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb', 'lyme_igm_wb') and l.collection_date between t0.screen_specimen_date and (t0.screen_specimen_date+ INTERVAL '30 days')
and result_string not in ('tnp','TNP','Not tested','Test not done') and h.name is not null;


/*
drop table if exists lyme_wb_tmp0;
CREATE TABLE lyme_wb_tmp0 AS
SELECT distinct t0.patient_id, t0.screen_test, hef_event_name_screen,
t0.screen_result_list, t0.screen_specimen_date, 
tm.test_name, h.name as hef_event_name_wb, l.result_string, date(l.collection_date) as wb_specimen_date, content_type_id 
FROM pos_screen_agg t0
JOIN emr_labresult l ON t0.patient_id = l.patient_id
JOIN conf_labtestmap tm on tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb', 'lyme_igm_wb') and date(l.collection_date) between t0.screen_specimen_date and (t0.screen_specimen_date+ INTERVAL '30 days')
and result_string not in ('tnp','TNP','Not tested','Test not done') and h.name is not null;


-- testing to check whether patient have multiple result assocaited with the same order
drop table if exists lyme_wb_tmp1;
CREATE TABLE lyme_wb_tmp1 AS
SELECT distinct l.patient_id, tm.test_name as screen_test, result_string as screen_result, collection_date as screen_specimen_date, 
l.order_natural_key, l.native_name, l.specimen_num, l.procedure_name, l.date, updated_timestamp 
FROM pos_screen_agg t0
JOIN emr_labresult l ON t0.patient_id = l.patient_id
JOIN conf_labtestmap tm on tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb', 'lyme_igm_wb') and date(l.collection_date) between t0.screen_specimen_date and (t0.screen_specimen_date+ INTERVAL '30 days')
and result_string not in ('tnp','TNP','Not tested','Test not done') and h.name is not null;

select count(*), patient_id, screen_test, order_natural_key, date from pos_screen_1 
group by patient_id, screen_test, order_natural_key, date having count(*) > 1

---
--alternatively

drop table if exists lyme_wb;
CREATE TABLE lyme_wb AS
SELECT distinct t0.patient_id, t0.screen_test, hef_event_name_screen, t0.screen_result_list, t0.screen_specimen_date, 
t0.test_name, t0.hef_event_name_wb, t0.result_string, t0.wb_specimen_date
FROM lyme_wb_tmp0 t0
JOIN django_content_type d on d.id = t0.content_type_id and d.model = 'labresult'


drop table if exists lyme_wb_agg;
CREATE TABLE lyme_wb_agg AS
select patient_id, screen_test, screen_specimen_date, hef_event_name_screen, screen_result_list, 
order_natural_key, test_name, wb_specimen_date,
string_agg(hef_event_name_wb, '|' order by updated_timestamp) as hef_event_name_wb,
string_agg(result_string, '|' order by updated_timestamp) as result_string
from lyme_wb
group by patient_id, screen_test, screen_specimen_date, hef_event_name_screen, screen_result_list, 
order_natural_key, test_name, wb_specimen_date


drop table if exists lyme_cha_061819;
CREATE TABLE lyme_cha_061819 AS
SELECT distinct t0.id ptid, date_part('year', age(screen_specimen_date, date_of_birth)) as age, gender as sex, date_of_birth,
 case 
     when (select target_value from gen_pop_tools.tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=t0.ethnicity) is not null
	   then (select target_value from gen_pop_tools.tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=t0.ethnicity)
	 else (select target_value from gen_pop_tools.tt_racemap t00 where t00.source_field='race' and t00.source_value=t0.race)
  end as race, t0.ethnicity, t1.*
FROM emr_patient t0
JOIN lyme_rx_t1 t1 on t0.id = t1.patient_id
WHERE substr(t0.gender,1,1) in ('M', 'F')
AND T0.last_name not in ('TEST', 'TEST**')
AND T0.last_name not ilike '%ssmctest%' 
AND T0.last_name not ilike '% test%' 
AND T0.last_name not ilike 'XB%' 
AND T0.last_name not ilike 'XX%';
*/