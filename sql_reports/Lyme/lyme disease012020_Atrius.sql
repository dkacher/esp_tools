-- patients who have a positive Lyme disease enzyme immunoassay (EIA) test result between January 1, 2010 and June 30, 2019. 
--look into another source of  positive results - western to follow

--Specimen collection date for positive Lyme EIA 
--Specimen collection date for positive Lyme ELISA
--Result of Lyme EIA (positive/)
--Result of Lyme ELISA (positive/WesternToFollow/For results see Western Blot/see Western Blot/see Western Blot for result(s)/See Western Blot result(s)/Western Pending)

-- For Lyme IgG and IgM Western Blot specimens collected up to 30 days after the specimen collection date for positive Lyme EIA please provide
-- a.Specimen collection date for Lyme IgG Western Blot 
-- b.Result of Lyme IgG Western Blot test
-- c.Specimen collection date for Lyme IgM Western Blot
-- d.Result of Lyme IgM Western Blot

-- **  and result_string not in ('tnp','TNP','Not tested','Test not done')
-- patient_id : 1862372, some result string doesn't seem to be very meaningful -- filter out this unmapped ones for now

-- For Atrius
-- seems to have two of the same results associated with the same patient (same order natural key, specimen_num, procedure name) one is numeric result, another is 'interpreted' result
-- pick interpreted result, otherwise numeric value

-- patients who have positive screening results (result_date)
drop table if exists pos_screen;
CREATE TABLE pos_screen AS
SELECT distinct l.patient_id, l.provider_id, p.dept, tm.test_name as screen_test, h.name as hef_event_name, l.date, result_string as screen_result, 
collection_date as screen_specimen_date, order_natural_key, l.result_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
JOIN emr_provider p on l.provider_id = p.id
WHERE tm.test_name IN ('lyme_igm_eia', 'lyme_igg_eia', 'lyme_elisa') 
AND l.result_date BETWEEN '01-01-2010' AND '12-31-2019' 
AND (((result_string ilike '%positive%' or h.name ilike '%positive%') and h.name not ilike '%any%') or (result_string ilike '%western%'))

-- aggregate the results associated with the same test/order natural id/ the same date, select the latest result_date (for 16607274)
drop table if exists pos_screen_agg;
CREATE TABLE pos_screen_agg AS
select patient_id, screen_test, screen_specimen_date, provider_id, dept, order_natural_key, 
string_agg(hef_event_name, '|') as hef_event_name_screen, string_agg(distinct screen_result, '|') as screen_result_list, max(result_date) as result_date
from pos_screen
group by patient_id, screen_test, screen_specimen_date, provider_id, dept, order_natural_key

-- follow up WB test
drop table if exists lyme_wb_t1;
CREATE TABLE lyme_wb_t1 AS
SELECT distinct t0.patient_id, t0.screen_test, t0.screen_specimen_date, hef_event_name_screen, t0.screen_result_list, tm.test_name, h.name as hef_event_name_wb, 
l.result_string, l.collection_date as wb_specimen_date, content_type_id, l.order_natural_key
FROM pos_screen_agg t0
JOIN emr_labresult l ON t0.patient_id = l.patient_id
JOIN conf_labtestmap tm on tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb', 'lyme_igm_wb') 
and l.collection_date between t0.screen_specimen_date and (t0.screen_specimen_date+ INTERVAL '30 days')
and result_string not in ('tnp','TNP','Not tested','Test not done') and h.name is not null;

drop table if exists lyme_wb;
CREATE TABLE lyme_wb AS
SELECT distinct t0.patient_id, t0.screen_test, t0.screen_specimen_date, hef_event_name_screen, 
t0.screen_result_list, t0.test_name, hef_event_name_wb, order_natural_key,
t0.result_string, wb_specimen_date
FROM lyme_wb_t1 t0
JOIN django_content_type t1 on t1.id = t0.content_type_id and t1.model = 'labresult';

-- aggregate result 
drop table if exists lyme_wb_agg;
CREATE TABLE lyme_wb_agg AS
select patient_id, screen_test, screen_specimen_date, hef_event_name_screen, screen_result_list, 
order_natural_key, test_name, wb_specimen_date,
string_agg(hef_event_name_wb, '|') as hef_event_name_wb,
string_agg(distinct result_string, '|') as result_string
from lyme_wb
group by patient_id, screen_test, screen_specimen_date, hef_event_name_screen, screen_result_list, 
order_natural_key, test_name, wb_specimen_date

drop table if exists lyme_wb_rnk;
CREATE TABLE lyme_wb_rnk AS
SELECT t0.patient_id, t0.screen_test, date(t0.screen_specimen_date) as screen_specimen_date, hef_event_name_screen, t0.screen_result_list, 
t0.test_name, hef_event_name_wb, t0.result_string, date(wb_specimen_date) as wb_specimen_date , 
ROW_NUMBER() OVER ( PARTITION BY t0.patient_id, t0.screen_specimen_date, t0.screen_test, t0.screen_result_list ORDER BY t0.wb_specimen_date, t0.test_name) rnk
FROM lyme_wb_agg t0 ;

-- transpose to columns (need to look back and confirm for CHA as well to see if there are duplicated labs)
drop table if exists lyme_wb_t;
CREATE TABLE lyme_wb_t AS
SELECT distinct patient_id, screen_specimen_date, screen_test, screen_result_list,
MAX(CASE WHEN rnk ='1' THEN test_name ELSE NULL END) wb1_name,
MAX(CASE WHEN rnk ='1' THEN wb_specimen_date ELSE NULL END) wb1_specimen_date,
MAX(CASE WHEN rnk ='1' THEN hef_event_name_wb ELSE NULL END) wb1_hef_event_name,
MAX(CASE WHEN rnk ='1' THEN result_string ELSE NULL END) wb1_result,
MAX(CASE WHEN rnk ='2' THEN test_name ELSE NULL END) wb2_name,
MAX(CASE WHEN rnk ='2' THEN wb_specimen_date ELSE NULL END) wb2_specimen_date,
MAX(CASE WHEN rnk ='2' THEN hef_event_name_wb ELSE NULL END) wb2_hef_event_name,
MAX(CASE WHEN rnk ='2' THEN result_string ELSE NULL END) wb2_result,
MAX(CASE WHEN rnk ='3' THEN test_name ELSE NULL END) wb3_name,
MAX(CASE WHEN rnk ='3' THEN wb_specimen_date ELSE NULL END) wb3_specimen_date,
MAX(CASE WHEN rnk ='3' THEN hef_event_name_wb ELSE NULL END) wb3_hef_event_name,
MAX(CASE WHEN rnk ='3' THEN result_string ELSE NULL END) wb3_result,
MAX(CASE WHEN rnk ='4' THEN test_name ELSE NULL END) wb4_name,
MAX(CASE WHEN rnk ='4' THEN wb_specimen_date ELSE NULL END) wb4_specimen_date,
MAX(CASE WHEN rnk ='4' THEN hef_event_name_wb ELSE NULL END) wb4_hef_event_name,
MAX(CASE WHEN rnk ='4' THEN result_string ELSE NULL END) wb4_result,
MAX(CASE WHEN rnk ='5' THEN test_name ELSE NULL END) wb5_name,
MAX(CASE WHEN rnk ='5' THEN wb_specimen_date ELSE NULL END) wb5_specimen_date,
MAX(CASE WHEN rnk ='5' THEN hef_event_name_wb ELSE NULL END) wb5_hef_event_name,
MAX(CASE WHEN rnk ='5' THEN result_string ELSE NULL END) wb5_result,
MAX(CASE WHEN rnk ='6' THEN test_name ELSE NULL END) wb6_name,
MAX(CASE WHEN rnk ='6' THEN wb_specimen_date ELSE NULL END) wb6_specimen_date,
MAX(CASE WHEN rnk ='6' THEN hef_event_name_wb ELSE NULL END) wb6_hef_event_name,
MAX(CASE WHEN rnk ='6' THEN result_string ELSE NULL END) wb6_result
FROM lyme_wb_rnk 
GROUP BY patient_id, screen_specimen_date, screen_test, screen_result_list ORDER BY patient_id, screen_specimen_date ;

-- patient id 16607274 has two diff result_date, use the latest one as result date
drop table if exists lyme_lab;
CREATE TABLE lyme_lab AS
SELECT distinct t0.patient_id, t0.screen_test, t0.provider_id, t0.dept, date(t0.screen_specimen_date) as screen_specimen_date , hef_event_name_screen, t0.screen_result_list, date(t0.result_date) as result_date,
wb1_name,
wb1_specimen_date,
wb1_hef_event_name,
wb1_result,
wb2_name,
wb2_specimen_date,
wb2_hef_event_name,
wb2_result,
wb3_name,
wb3_specimen_date,
wb3_hef_event_name,
wb3_result,
wb4_name,
wb4_specimen_date,
wb4_hef_event_name,
wb4_result,
wb5_name,
wb5_specimen_date,
wb5_hef_event_name,
wb5_result,
wb6_name,
wb6_specimen_date,
wb6_hef_event_name,
wb6_result
FROM pos_screen_agg t0
LEFT JOIN lyme_wb_t t1 on t0.patient_id = t1.patient_id and date(t0.screen_specimen_date)=t1.screen_specimen_date 
and t0.screen_test = t1.screen_test and t0.screen_result_list = t1.screen_result_list;


--Lyme disease antibiotics ordered within 30 days of positive/WesternToFollow Lyme EIA:
--antibiotics prescription, pick up rx info
--Date of Doxycycline order
--Number of days of Doxycycline ordered
--Date of Amoxicillin order
--Number of days of Amoxicillin orde
--Date of Ceftriaxone order
--Number of days of Ceftriaxone ordered
--Date of Cefuroxime order
--Number of days of Cefuroxime ordered
--Date of Cefotaxime order
--Number of days of Cefotaxime ordered
--Date of Azithromycin order
--Number of days of Azithromycin ordered
--Date of Tetracycline order
--Number of days of Tetracycline ordered

-- remove rx_name; Lyme disease antibiotics ordered within 30 days of positive Lyme ELISA or positive EIA (use result_date)
drop table if exists lyme_rx;
CREATE TABLE lyme_rx AS
SELECT DISTINCT t0.patient_id, t0.screen_specimen_date, t0.screen_test, t0.screen_result_list, rx.date as rx_date, 
ds.generic_name, (rx.end_date - rx.start_date) as rx_dur
FROM lyme_lab t0
JOIN emr_prescription rx on t0.patient_id = rx.patient_id
JOIN static_drugsynonym ds on rx.name ilike '%' || ds.other_name || '%'  
WHERE (generic_name ilike '%Doxycycline%' or generic_name ilike '%Amoxicillin%' or generic_name  ilike '%Cefuroxime%' or
generic_name  ilike '%Ceftriaxone%' or generic_name  ilike '%Cefotaxime%' or generic_name  ilike '%Azithromycin%' or
generic_name  ilike '%Tetracycline%' ) and rx.date between (t0.result_date - INTERVAL '30 days') and (t0.result_date + INTERVAL '30 days');


drop table if exists lyme_rx_rnk;
CREATE TABLE lyme_rx_rnk AS
SELECT t0.*,ROW_NUMBER() OVER ( PARTITION BY t0.patient_id, t0.screen_specimen_date, t0.screen_test, t0.screen_result_list ORDER BY t0.rx_date, t0.generic_name, rx_dur) rnk
FROM lyme_rx t0 ;

drop table if exists lyme_rx_t;
CREATE TABLE lyme_rx_t AS
SELECT patient_id, screen_specimen_date, screen_test, screen_result_list,
MAX(CASE WHEN rnk ='1' THEN generic_name ELSE NULL END) rx1,
MAX(CASE WHEN rnk ='1' THEN rx_date ELSE NULL END) rx1_order_date,
MAX(CASE WHEN rnk ='1' THEN rx_dur ELSE NULL END) rx1_duration,
MAX(CASE WHEN rnk ='2' THEN generic_name ELSE NULL END) rx2,
MAX(CASE WHEN rnk ='2' THEN rx_date ELSE NULL END) rx2_order_date,
MAX(CASE WHEN rnk ='2' THEN rx_dur ELSE NULL END) rx2_duration,
MAX(CASE WHEN rnk ='3' THEN generic_name ELSE NULL END) rx3,
MAX(CASE WHEN rnk ='3' THEN rx_date ELSE NULL END) rx3_order_date,
MAX(CASE WHEN rnk ='3' THEN rx_dur ELSE NULL END) rx3_duration,
MAX(CASE WHEN rnk ='4' THEN generic_name ELSE NULL END) rx4,
MAX(CASE WHEN rnk ='4' THEN rx_date ELSE NULL END) rx4_order_date,
MAX(CASE WHEN rnk ='4' THEN rx_dur ELSE NULL END) rx4_duration,
MAX(CASE WHEN rnk ='5' THEN generic_name ELSE NULL END) rx5,
MAX(CASE WHEN rnk ='5' THEN rx_date ELSE NULL END) rx5_order_date,
MAX(CASE WHEN rnk ='5' THEN rx_dur ELSE NULL END) rx5_duration,
MAX(CASE WHEN rnk ='6' THEN generic_name ELSE NULL END) rx6,
MAX(CASE WHEN rnk ='6' THEN rx_date ELSE NULL END) rx6_order_date,
MAX(CASE WHEN rnk ='6' THEN rx_dur ELSE NULL END) rx6_duration,
MAX(CASE WHEN rnk ='7' THEN generic_name ELSE NULL END) rx7,
MAX(CASE WHEN rnk ='7' THEN rx_date ELSE NULL END) rx7_order_date,
MAX(CASE WHEN rnk ='7' THEN rx_dur ELSE NULL END) rx7_duration,
MAX(CASE WHEN rnk ='8' THEN generic_name ELSE NULL END) rx8,
MAX(CASE WHEN rnk ='8' THEN rx_date ELSE NULL END) rx8_order_date,
MAX(CASE WHEN rnk ='8' THEN rx_dur ELSE NULL END) rx8_duration,
MAX(CASE WHEN rnk ='9' THEN generic_name ELSE NULL END) rx9,
MAX(CASE WHEN rnk ='9' THEN rx_date ELSE NULL END) rx9_order_date,
MAX(CASE WHEN rnk ='9' THEN rx_dur ELSE NULL END) rx9_duration,
MAX(CASE WHEN rnk ='10' THEN generic_name ELSE NULL END) rx10,
MAX(CASE WHEN rnk ='10' THEN rx_date ELSE NULL END) rx10_order_date,
MAX(CASE WHEN rnk ='10' THEN rx_dur ELSE NULL END) rx10_duration,
MAX(CASE WHEN rnk ='11' THEN generic_name ELSE NULL END) rx11,
MAX(CASE WHEN rnk ='11' THEN rx_date ELSE NULL END) rx11_order_date,
MAX(CASE WHEN rnk ='11' THEN rx_dur ELSE NULL END) rx11_duration,
MAX(CASE WHEN rnk ='12' THEN generic_name ELSE NULL END) rx12,
MAX(CASE WHEN rnk ='12' THEN rx_date ELSE NULL END) rx12_order_date,
MAX(CASE WHEN rnk ='12' THEN rx_dur ELSE NULL END) rx12_duration,
MAX(CASE WHEN rnk ='13' THEN generic_name ELSE NULL END) rx13,
MAX(CASE WHEN rnk ='13' THEN rx_date ELSE NULL END) rx13_order_date,
MAX(CASE WHEN rnk ='13' THEN rx_dur ELSE NULL END) rx13_duration,
MAX(CASE WHEN rnk ='14' THEN generic_name ELSE NULL END) rx14,
MAX(CASE WHEN rnk ='14' THEN rx_date ELSE NULL END) rx14_order_date,
MAX(CASE WHEN rnk ='14' THEN rx_dur ELSE NULL END) rx14_duration
FROM lyme_rx_rnk 
GROUP BY patient_id, screen_specimen_date, screen_test, screen_result_list ORDER BY patient_id, screen_specimen_date ;

drop table if exists lyme_rx_t1;
CREATE TABLE lyme_rx_t1 AS
SELECT distinct t0.*,
 rx1, rx1_order_date, rx1_duration,
 rx2, rx2_order_date, rx2_duration,
 rx3, rx3_order_date, rx3_duration,
 rx4, rx4_order_date, rx4_duration,
 rx5, rx5_order_date, rx5_duration,
 rx6, rx6_order_date, rx6_duration,
 rx7, rx7_order_date, rx7_duration,
 rx8, rx8_order_date, rx8_duration,
 rx9, rx9_order_date, rx9_duration,
 rx10, rx10_order_date, rx10_duration,
 rx11, rx11_order_date, rx11_duration,
 rx12, rx12_order_date, rx12_duration,
 rx13, rx13_order_date, rx13_duration,
 rx14, rx14_order_date, rx14_duration
FROM lyme_lab t0
LEFT JOIN lyme_rx_t t1 on t0.patient_id = t1.patient_id and t0.screen_specimen_date=t1.screen_specimen_date and
 t0.screen_test = t1.screen_test and t0.screen_result_list = t1.screen_result_list;

--** 012120 create the following after early Feb

-- Demographics:
-- ESP internal identifier (not MRN)
-- Patient’s age in years (calculate as date of birth – date of positive EIA test)
-- Sex
-- Race
-- Ethnicity


drop table if exists lyme_Atrius_020420;
CREATE TABLE lyme_atrius_020420 AS
SELECT distinct t0.id ptid, date_part('year', age(result_date, date_of_birth)) as age, gender as sex, date_of_birth, 
 case 
     when (select target_value from gen_pop_tools.tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=t0.ethnicity) is not null
	   then (select target_value from gen_pop_tools.tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=t0.ethnicity)
	 else (select target_value from gen_pop_tools.tt_racemap t00 where t00.source_field='race' and t00.source_value=t0.race)
  end as race, t0.ethnicity, t1.*
FROM emr_patient t0
JOIN lyme_rx_t1 t1 on t0.id = t1.patient_id
WHERE substr(t0.gender,1,1) in ('M', 'F')
AND T0.last_name not in ('TEST', 'TEST**')
AND T0.last_name not ilike '%ssmctest%' 
AND T0.last_name not ilike '% test%' 
AND T0.last_name not ilike 'XB%' 
AND T0.last_name not ilike 'XX%';

ALTER TABLE lyme_Atrius_020420 RENAME COLUMN dept TO clinic;

ALTER TABLE lyme_Atrius_020420 
DROP COLUMN patient_id,
DROP COLUMN date_of_birth;
DROP COLUMN result_date;
DROP COLUMN provider_id;

/*

select count(*), ptid, screen_specimen_date, screen_test from lyme_atrius_073019 
group by ptid, screen_specimen_date, screen_test having count(*) > 1

-- for testing
drop table if exists pos_screen_1;
CREATE TABLE pos_screen_1 AS
SELECT distinct l.patient_id, tm.test_name as screen_test, result_string as screen_result, collection_date as screen_specimen_date, 
l.order_natural_key, l.native_name, l.specimen_num, l.procedure_name, l.date, updated_timestamp 
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igm_eia', 'lyme_igg_eia', 'lyme_elisa') 
AND l.date BETWEEN '01-01-2010' AND '06-30-2019' 
AND (((result_string ilike '%positive%' or h.name ilike '%positive%') and h.name not ilike '%any%') or (result_string ilike '%western%'))

select count(distinct specimen_num), order_natural_key from pos_screen_1 group by order_natural_key
having count(distinct specimen_num) > 1

select * from pos_screen_1 where patient_id in 
(3281181,
1461559,
946555,
921674,
962888,
358570,
3283917,
680174,
5098095,
5017195,
357626,
5669603,
1595634,
371000,
15847831) order by patient_id, date
---
*/

/*
-- rank result (patient_id = 16607274; 50340; 358570) -- comeback later 
drop table if exists pos_screen_rnk;
CREATE TABLE pos_screen_rnk AS
SELECT t0.*,ROW_NUMBER() OVER ( PARTITION BY t0.patient_id, t0.screen_test, t0.screen_specimen_date ORDER BY updated_timestamp desc, screen_result desc) rnk
FROM pos_screen t0 ;
*/

/*
-- for testing
drop table if exists pos_screen_t0;
CREATE TABLE pos_screen_t0 AS
select distinct patient_id, date from pos_screen where name ilike '%positive%'

drop table if exists pos_screen_t1;
CREATE TABLE pos_screen_t1 AS
select distinct t0.*, t1.name as name1, t1.collection_date as co_1, t1.screen_result as result1, t1.native_name as nn1, t1.order_natural_key as order_1,
t1.specimen_num as spec_1, t1.procedure_name as p1, t1.date as d1
from pos_screen t0
join pos_screen t1 on t0.patient_id =t1.patient_id and t0.screen_test=t1.screen_test and t0.name = t1.name and t0.date =t1.date
where t0.screen_result ilike '%positive%' and t1.screen_result not ilike '%positive%'

SELECT distinct l.patient_id, tm.test_name, tm.native_code, native_name, created_timestamp, updated_timestamp, l.date, order_natural_key, result_date, collection_date, status,
order_type, patient_class, patient_status, result_float, result_string, specimen_num, specimen_source, comment,
procedure_name, ref_high_string, ref_low_string, ref_high_float, ref_low_float, ref_unit 
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igm_eia', 'lyme_igg_eia', 'lyme_elisa') 
AND l.date BETWEEN '01-01-2010' AND '06-30-2019' 
AND (result_string ilike '%positive%' or result_string ilike '%western%' or h.name ilike '%positive%') and h.name not ilike '%any%' and l.patient_id in (342, 578)
													order by l.patient_id, l.date


drop table if exists lyme_rx;
CREATE TABLE lyme_rx AS
SELECT DISTINCT t0.patient_id, t0.screen_specimen_date, t0.screen_test, t0.screen_result_list, rx.date as rx_date, rx.name as rx_name, ds.generic_name, (rx.end_date - rx.start_date) as rx_dur
FROM lyme_lab t0
JOIN emr_prescription rx on t0.patient_id = rx.patient_id
JOIN static_drugsynonym ds on rx.name ilike '%' || ds.other_name || '%'  
WHERE (generic_name ilike '%Doxycycline%' or generic_name ilike '%Amoxicillin%' or generic_name  ilike '%Cefuroxime%' or
generic_name  ilike '%Ceftriaxone%' or generic_name  ilike '%Cefotaxime%' or generic_name  ilike '%Azithromycin%' or
generic_name  ilike '%Tetracycline%' ) and rx.date between (t0.screen_specimen_date - INTERVAL '30 days') and (t0.screen_specimen_date + INTERVAL '30 days');

drop table if exists age_tmp;
CREATE TABLE age_tmp AS
select patient_id, date_part('year', age(screen_specimen_date, date_of_birth)) as age_spe, 
					  date_part('year', age(date, date_of_birth)) as age_date
FROM emr_patient t0
JOIN pos_screen t1 on t0.id = t1.patient_id


select count(*) , patient_id, screen_test, screen_specimen_date , hef_event_name_screen, screen_result_list
from (select patient_id, screen_test, screen_specimen_date , hef_event_name_screen, screen_result_list from lyme_lab)t0
group by patient_id, screen_test, screen_specimen_date , hef_event_name_screen, screen_result_list
except 
select count(*), patient_id, screen_test, screen_specimen_date , hef_event_name_screen, screen_result_list from pos_screen_agg
group by patient_id, screen_test, screen_specimen_date , hef_event_name_screen, screen_result_list
*/




