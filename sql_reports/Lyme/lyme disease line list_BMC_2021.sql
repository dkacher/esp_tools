/* Sarah Willis - DPM - 07/20/2021 As part of our chart review activity to develop a new Lyme algorithm, I need an additional group of BMC patients to sample from. I’m looking for BMC patients in 2017 and in 2018 who have a Lyme disease test ordered (Lyme ELISA/EIA/IFA, Western Blot, or PCR) but do not have a Lyme antibiotic ordered within 30 days or a Lyme related ICD-10 code within 30 days. I’m not sure that BMC has lab orders available – if they don’t, then I’m looking for anyone with a positive, negative, indeterminate, or no result for one of the Lyme related labs (and no antibiotic and no ICD-10 code_ */

-- BMC patients in 2017 and in 2018 who have a Lyme disease test
-- Find all patients that had any type of Lyme test
-- HEF event not required

DROP TABLE IF EXISTS lyme_rpt.lyme_lt1_all_result_21;
CREATE TABLE lyme_rpt.lyme_lt1_all_result_21 AS
SELECT l.patient_id, test_name, h.name, l.date as lab_date, result_string, result_float
FROM emr_labresult l
JOIN conf_labtestmap tm ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
WHERE tm.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia', 'lyme_igg_imwb', 
'lyme_igm_wb', 'lyme_igg_wb', 'lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2017' AND '12-31-2018' 
order by patient_id;


-- Lyme Antibiotics
-- extend time period for before and after lab test
DROP TABLE IF EXISTS lyme_rpt.lyme_abx_rx_21;
CREATE TABLE lyme_rpt.lyme_abx_rx_21 AS
SELECT DISTINCT rx.patient_id, rx.date as rx_date, rx.name as rx_name, rx.directions, rx.quantity, ds.generic_name, rx.quantity_float, rx.start_date, rx.end_date
FROM emr_prescription rx
JOIN static_drugsynonym ds on rx.name ilike '%' || ds.other_name || '%'  
WHERE 
(generic_name ilike '%Doxycycline%' or generic_name ilike '%Amoxicillin%' or generic_name  ilike '%Cefuroxime%' or
generic_name  ilike '%Ceftriaxone%' or generic_name  ilike '%Cefotaxime%' or generic_name  ilike '%Azithromycin%' or
generic_name  ilike '%Tetracycline%' )
AND rx.date BETWEEN '12-01-2016' AND '01-31-2019';


-- Lyme Diagnosis Codes
DROP TABLE IF EXISTS lyme_rpt.lyme_dx_enc_21;
CREATE TABLE lyme_rpt.lyme_dx_enc_21 AS 
SELECT patient_id, date as enc_date, encounter_id, dx_code_id AS lyme_dx
FROM emr_encounter_dx_codes dx 
JOIN emr_encounter enc ON (dx.encounter_id = enc.id)
WHERE dx.dx_code_id IN ('icd9:088.81', 'icd10:A69.20', 'icd10:A69.21', 'icd10:A69.22', 'icd10:A69.23', 'icd10:A69.29')
AND date BETWEEN '12-01-2016' AND '01-31-2019'; 

-- Patients that do not have a med within 30 days of the lab AND do not have a dx within 30 days of the lab
DROP TABLE IF EXISTS lyme_rpt.lyme_no_dx_or_med_21;
CREATE TABLE lyme_rpt.lyme_no_dx_or_med_21 AS 
SELECT lab.patient_id, lab_date, rx_date, rx_name, lab_date - rx_date as lab_rx_datediff, enc_date, lab_date - enc_date as lab_dx_datediff
FROM lyme_rpt.lyme_lt1_all_result_21 lab
LEFT JOIN lyme_rpt.lyme_abx_rx_21 rx ON (lab.patient_id = rx.patient_id)
LEFT JOIN lyme_rpt.lyme_dx_enc_21 dx ON (lab.patient_id = dx.patient_id)
WHERE (rx_date IS NULL OR abs(rx_date - lab_date) > 30)
AND (enc_date IS NULL OR abs(enc_date - lab_date) > 30)
ORDER BY enc_date;

-- Get the distinct patients and lab dates
DROP TABLE IF EXISTS lyme_rpt.lyme_pat_labs_of_int_21;
CREATE TABLE lyme_rpt.lyme_pat_labs_of_int_21 AS
SELECT patient_id, lab_date
FROM lyme_rpt.lyme_no_dx_or_med_21
GROUP BY patient_id, lab_date;

-- Create the output
DROP TABLE IF EXISTS lyme_rpt.lyme_output_21;
CREATE TABLE lyme_rpt.lyme_output_21 AS
SELECT 
concat(pat.last_name, ', ', pat.first_name) as patient_name,
pat.mrn,
pat.date_of_birth::date as dob,
concat(pro.last_name, ', ', pro.first_name, ' ', pro.title) as pcp_name,
pro.dept as pcp_site,
--list.patient_id, 
array_agg(distinct(list.lab_date)) as lab_dates, 
array_agg(distinct(test_name)) as lab_names, 
array_agg(distinct(name)) as event_names
FROM lyme_rpt.lyme_pat_labs_of_int_21 list
INNER JOIN lyme_rpt.lyme_lt1_all_result_21 lab ON (list.patient_id = lab.patient_id and list.lab_date = lab.lab_date)
INNER JOIN emr_patient pat ON (list.patient_id = pat.id)
LEFT JOIN emr_provider pro on (pat.pcp_id = pro.id)
GROUP BY list.patient_id, patient_name, mrn, dob, pcp_name, pcp_site
ORDER BY patient_name;


\COPY (select * from lyme_rpt.lyme_output_21) TO '/tmp/bmc_lymetest_no_dx_or_med.csv' with csv header;



