-- meet at least one of the proposed ESP surveillance algorithm criteria components between January 1, 2017 and December 31, 2018 

drop table if exists lyme_rpt.tick_dx_enc;
drop table if exists lyme_rpt.tick_dx_enc_idx;
drop table if exists lyme_rpt.tick_dx_enc_idx_18;
drop table if exists lyme_rpt.lyme_dx_enc;
drop table if exists lyme_rpt.lyme_dx_enc_idx;
drop table if exists lyme_rpt.lyme_dx_enc_idx_18;
drop table if exists lyme_rpt.lyme_lt1_all_result;
drop table if exists lyme_rpt.lyme_lt1;
drop table if exists lyme_rpt.lyme_lt1_18;
drop table if exists lyme_rpt.lyme_lt2;
drop table if exists lyme_rpt.lyme_lt2_18;
drop table if exists lyme_rpt.lt_igg_wb;
drop table if exists lyme_rpt.lt_igg_wb_18;
drop table if exists lyme_rpt.lt_igm_wb;
drop table if exists lyme_rpt.lt_igm_wb_18;
drop table if exists lyme_rpt.lt_pcr;
drop table if exists lyme_rpt.lt_pcr_18;
drop table if exists lyme_rpt.lyme_abx_rx;
drop table if exists lyme_rpt.lyme_abx_rx_17;
drop table if exists lyme_rpt.lyme_abx_rx_18;
drop table if exists lyme_rpt.lyme_rx_qualified;
drop table if exists lyme_rpt.lyme_rx_qualified_18;
drop table if exists lyme_rpt.ll1_rx_q14;
drop table if exists lyme_rpt.ll1_rx_q14_18;
drop table if exists lyme_rpt.ll1_rx_q30;
drop table if exists lyme_rpt.ll1_rx_q30_18;
drop table if exists lyme_rpt.ll2_rx_q14;
drop table if exists lyme_rpt.ll2_rx_q14_18;
drop table if exists lyme_rpt.ll2_rx_q30;
drop table if exists lyme_rpt.ll2_rx_q30_18;
drop table if exists lyme_rpt.lyme_doxy_rx_qualified;
drop table if exists lyme_rpt.lyme_doxy_rx_qualified_18;
drop table if exists lyme_rpt.tick_doxy_rx_qualified;
drop table if exists lyme_rpt.tick_doxy_rx_qualified_18;
drop table if exists lyme_rpt.tick_other_rx_qualified;
drop table if exists lyme_rpt.tick_other_rx_qualified_18;
drop table if exists lyme_rpt.lyme_idx_patient ;
drop table if exists lyme_rpt.lyme_idx_patient_18 ;
drop table if exists lyme_rpt.lyme_idx_patient_all ;
drop table if exists lyme_rpt.lyme_idx_patient_all_18 ;
drop table if exists lyme_rpt.lyme_ll_fin_17;
drop table if exists lyme_rpt.lyme_ll_fin_18;
drop table if exists lyme_rpt.lyme_output_17;
drop table if exists lyme_rpt.lyme_output_18;


SET search_path TO lyme_rpt, public;

--
-- pick up encounters who have tick bite icd (ALL)
--

CREATE TABLE lyme_rpt.tick_dx_enc AS 
SELECT encounter_id, dx_code_id AS tick_dx
FROM emr_encounter_dx_codes dx 
WHERE dx.dx_code_id IN ('icd9:919.4', 'icd9:E906.4', 'icd9:911.4', 'icd9:916.4', 'icd10:W57.XXXA')
OR dx_code_id ilike 'icd10:T14.8%';

-- select the first reported date for tick bite ICD 
-- 2017

CREATE TABLE lyme_rpt.tick_dx_enc_idx AS
SELECT enc.patient_id, tick_dx, enc.date, min(enc.date) over (partition by enc.patient_id) AS tick_idx_date
FROM emr_encounter enc
JOIN tick_dx_enc dx ON enc.id = dx.encounter_id
WHERE enc.date BETWEEN '01-01-2017' AND '12-31-2017';

-- 2018

CREATE TABLE lyme_rpt.tick_dx_enc_idx_18 AS
SELECT enc.patient_id, tick_dx, enc.date, min(enc.date) over (partition by enc.patient_id) AS tick_idx_date
FROM emr_encounter enc
JOIN tick_dx_enc dx ON enc.id = dx.encounter_id
WHERE enc.date BETWEEN '01-01-2018' AND '12-31-2018';

--
-- pick up Lyme ICD code (ALL)
--

CREATE TABLE lyme_rpt.lyme_dx_enc AS 
SELECT encounter_id, dx_code_id AS lyme_dx
FROM emr_encounter_dx_codes dx 
WHERE dx.dx_code_id IN ('icd9:088.81', 'icd10:A69.20', 'icd10:A69.21', 'icd10:A69.22', 'icd10:A69.23', 'icd10:A69.29');

-- select the first reported date for lyme ICD
-- 2017

CREATE TABLE lyme_rpt.lyme_dx_enc_idx AS 
SELECT enc.patient_id, lyme_dx, enc.date, min(enc.date) over (partition by enc.patient_id) AS lyme_idx_date
FROM emr_encounter enc
JOIN lyme_dx_enc dx ON enc.id = dx.encounter_id
WHERE enc.date BETWEEN '01-01-2017' AND '12-31-2017';

-- 2018

CREATE TABLE lyme_rpt.lyme_dx_enc_idx_18 AS 
SELECT enc.patient_id, lyme_dx, enc.date, min(enc.date) over (partition by enc.patient_id) AS lyme_idx_date
FROM emr_encounter enc
JOIN lyme_dx_enc dx ON enc.id = dx.encounter_id
WHERE enc.date BETWEEN '01-01-2018' AND '12-31-2018';

--
-- pick up EIA/ELISA/IFA+ lab tests 
-- Q1: NEED TO have string lyme in it or not
-- Q2: can't find IFA
-- Q3: positive results (some were numeric values)
/*
select native_name, native_code
from emr_labtestconcordance
where native_code not in (select distinct native_code from conf_labtestmap where test_name ilike '%lyme%') and
native_name in (
'LYME AB NUMBER',
'LYME AB SCREEN',
'LYME DISEASE C-6 AB'
);

-- no record found
select patient_id, native_name, date, result_string
FROM emr_labresult l
where native_code in ('LAB690--20718',
'86318--7745',
'86618--3996')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
*/

-- REF: pick up any result just in case

CREATE TABLE lyme_rpt.lyme_lt1_all_result AS
SELECT l.patient_id, test_name, h.name, l.date, result_string, result_float
FROM emr_labresult l
JOIN conf_labtestmap tm ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
--JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia', 'lyme_igg_imwb', 
'lyme_igm_wb', 'lyme_igg_wb', 'lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2017' AND '12-31-2018' 
order by patient_id;


-- 2017 Atrius (updated to include additional unmapped ELISA/EIA) -- did not change the results
/*drop table if exists lyme_lt1_tmp;
CREATE TABLE lyme_lt1_tmp AS
select *
from conf_labtestmap tm
where tm.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia') 


drop table if exists lyme_lt1;
CREATE TABLE lyme_lt1 AS
SELECT l.patient_id, l.native_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt1_idx_date
FROM emr_labresult l
JOIN lyme_lt1_tmp tm ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE (tm.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia') or l.native_code in ('LAB690--20718',
'86318--7745',
'86618--3996'))
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');
*/

--look into another source of  positive results - western to follow


CREATE TABLE lyme_rpt.lyme_lt1 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt1_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
--JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia') 
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or result_string ilike '%western%' or h.name ilike '%positive%');

-- 2018 Atrius (updated)

CREATE TABLE lyme_rpt.lyme_lt1_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt1_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
--JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia') 
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or result_string ilike '%western%' or h.name ilike '%positive%');


/*
drop table if exists lyme_lt1_18;
CREATE TABLE lyme_lt1_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt1_idx_date
FROM lyme_lt1_tmp tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE (tm.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia') or l.native_code in ('LAB690--20718',
'86318--7745',
'86618--3996'))
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');


--look into another source of  positive results (pending) -- western to follow

drop table if exists lyme_l_tmpt2;
CREATE TABLE lyme_l_tmpt2 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt1_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia') 
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or result_string ilike '%western%' or h.name ilike '%positive%');
*/

--
-- gather all results containing igGWB (Atrius) -- no records
--
/*
SELECT l.patient_id, l.native_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt1_idx_date
FROM emr_labresult l
JOIN conf_labtestmap tm ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE (tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb') or
 l.native_code in 
(select distinct native_code from emr_labtestconcordance where
native_name in ('B. BURGDOFERI 28 KD IGG',
'B. BURGDOFERI 30 KD IGG',
'B. BURGDOFERI 58 KD IGG',
'B. BURGDORFERI 18 KD IGG',
'B. BURGDORFERI 23 KD IGG',
'B. BURGDORFERI 39 KD IGG',
'B. BURGDORFERI 41 KD IGG',
'B. BURGDORFERI 45 KD IGG',
'B. BURGDORFERI 66 KD IGG',
'B. BURGDORFERI 93 KD IGG',
'LYME DISEASE 18 KD IGG',
'LYME DISEASE 23 KD IGG',
'LYME DISEASE 28 KD IGG',
'LYME DISEASE 30KD IGG',
'LYME DISEASE 39 KD IGG',
'LYME DISEASE 41 KD IGG',
'LYME DISEASE 45 KD IGG',
'LYME DISEASE 58 KD IGG',
'LYME DISEASE 66 KD IGG',
'LYME DISEASE 93 KD IGG'
)))
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 

select distinct native_name, native_code
from emr_labtestconcordance
where native_name in ('B. BURGDOFERI 28 KD IGG',
'B. BURGDOFERI 30 KD IGG',
'B. BURGDOFERI 58 KD IGG',
'B. BURGDORFERI 18 KD IGG',
'B. BURGDORFERI 23 KD IGG',
'B. BURGDORFERI 39 KD IGG',
'B. BURGDORFERI 41 KD IGG',
'B. BURGDORFERI 45 KD IGG',
'B. BURGDORFERI 66 KD IGG',
'B. BURGDORFERI 93 KD IGG',
'LYME DISEASE 18 KD IGG',
'LYME DISEASE 23 KD IGG',
'LYME DISEASE 28 KD IGG',
'LYME DISEASE 30KD IGG',
'LYME DISEASE 39 KD IGG',
'LYME DISEASE 41 KD IGG',
'LYME DISEASE 45 KD IGG',
'LYME DISEASE 58 KD IGG',
'LYME DISEASE 66 KD IGG',
'LYME DISEASE 93 KD IGG'
)

-- check igM (no records)

SELECT l.patient_id, l.native_name, l.date, result_string
FROM emr_labresult l
WHERE (
 l.native_code in 
(select distinct native_code from emr_labtestconcordance where
native_name in (
'B. BURGDOFERI 39 KD IGM',
'B. BURGDORFERI 23 KD IGM',
'B. BURGDORFERI 41 KD IGM',
'LYME DISEASE 23 KD IGM',
'LYME DISEASE 39 KD IGM',
'LYME DISEASE 41 KD IGM'
)))
AND l.date BETWEEN '01-01-2017' AND '12-31-2018' 

--pcr (2 new records)nothing seem to suggest it's positive
SELECT l.patient_id, l.native_name, l.date, result_string
FROM emr_labresult l
WHERE (
 l.native_code in 
(select distinct native_code from emr_labtestconcordance where
native_name in (
'LYME DISEASE DNA QL (UR)',
'LYME DISEASE DNA QL BLOOD'
)))
AND l.date BETWEEN '01-01-2017' AND '12-31-2018' 
*/

--
-- separate out WB or PCR+ 
-- 2017

CREATE TABLE lyme_rpt.lyme_lt2 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt2_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
--JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igm_wb', 'lyme_igg_wb', 'lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');

-- 2018

CREATE TABLE lyme_rpt.lyme_lt2_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS lt2_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
--JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igm_wb', 'lyme_igg_wb', 'lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');


-- IgG WB 
-- 2017

CREATE TABLE lyme_rpt.lt_igg_wb AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS igg_wb_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
--JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');

-- 2018

CREATE TABLE lyme_rpt.lt_igg_wb_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS igg_wb_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
--JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igg_imwb', 'lyme_igg_wb')
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');


--
-- IgM WB 
-- 2017


CREATE TABLE lyme_rpt.lt_igm_wb AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS igm_wb_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
--JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igm_wb')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');

-- 2018


CREATE TABLE lyme_rpt.lt_igm_wb_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS igm_wb_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
--JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_igm_wb')
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');


--
-- pcr 
-- 2017

CREATE TABLE lyme_rpt.lt_pcr AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS pcr_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
--JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2017' AND '12-31-2017' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');

-- 2018

CREATE TABLE lyme_rpt.lt_pcr_18 AS
SELECT l.patient_id, tm.test_name, h.name, l.date, result_string, min(l.date) over (partition by l.patient_id) AS pcr_idx_date
FROM conf_labtestmap tm
JOIN emr_labresult l ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
--JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.test_name IN ('lyme_pcr', 'lyme_pcr_csf')
AND l.date BETWEEN '01-01-2018' AND '12-31-2018' 
AND (result_string ilike '%positive%' or h.name ilike '%positive%');



SELECT t0.patient_id, t0.test_name, t0.name, t0.date, result_string, min(t0.date) over (partition by t0.patient_id) AS pcr_idx_date
from (select patient_id, test_name, name, result_string, date from lt_pcr
union 
select patient_id, test_name, name, result_string, date from lt_igm_wb
union
select patient_id, test_name, name, result_string, date from lt_igg_wb) t0





-- additional lab tests
/*
drop table if exists lyme_lt1_all_result_tmp;
CREATE TABLE lyme_lt1_all_result_tmp AS
SELECT l.patient_id, tm.native_name, l.date, result_string, result_float
FROM emr_labresult l
JOIN emr_labtestconcordance tm ON tm.native_code=l.native_code
LEFT JOIN hef_event h on ((l.id = h.object_id) and (l.patient_id = h.patient_id)) 
JOIN django_content_type d on d.id = h.content_type_id and d.model = 'labresult'
WHERE tm.native_code not in (select distinct native_code from conf_labtestmap where test_name ilike '%lyme%') and
(tm.native_name ilike '%lyme%' or tm.native_name ilike '%borr%' or tm.native_name ilike '%burg%') 
AND l.date BETWEEN '01-01-2017' AND '12-31-2018' 
order by patient_id


SELECT distinct l.native_name, l.native_code, result_string, result_float
FROM emr_labresult l
WHERE l.native_code in ('86318--5353',
'87476--926',
'LAB1340--21035',
'MR0001--4464',
'87168--926',
'87798--21010',
'LAB687--21306',
'86318--4464',
'86618--4464') order by native_name


-- check the unmapped tests
select native_name, native_code
from emr_labtestconcordance
where native_code not in (select distinct native_code from conf_labtestmap where test_name ilike '%lyme%') and
(native_name ilike '%lyme%' or native_name ilike '%borr%' or native_name ilike '%burg%');
*/


-- antibiotics prescription
-- temp table pick up brand name

CREATE TABLE lyme_rpt.lyme_abx_rx AS
SELECT DISTINCT rx.patient_id, rx.date as rx_date, rx.name as rx_name, rx.directions, rx.quantity, ds.generic_name, rx.quantity_float, rx.start_date, rx.end_date
FROM emr_prescription rx
JOIN static_drugsynonym ds on rx.name ilike '%' || ds.other_name || '%'  
WHERE (generic_name ilike '%Doxycycline%' or generic_name ilike '%Amoxicillin%' or generic_name  ilike '%Cefuroxime%' or
generic_name  ilike '%Ceftriaxone%' or generic_name  ilike '%Cefotaxime%' or generic_name  ilike '%Azithromycin%' or
generic_name  ilike '%Tetracycline%' )
AND rx.date >= '01-01-2017';

-- create lyme_abx_rx_2017

CREATE TABLE lyme_rpt.lyme_abx_rx_17 AS 
SELECT * FROM lyme_abx_rx
WHERE rx_date BETWEEN '01-01-2017' and '12-31-2017';

-- create lyme_abx_rx_2018

CREATE TABLE lyme_rpt.lyme_abx_rx_18 AS 
SELECT * FROM lyme_abx_rx
WHERE rx_date BETWEEN '01-01-2018' and '12-31-2018';

/* 1. pick up medication from hef_event 
drop table if exists lyme_abx;
CREATE TABLE lyme_abx AS
SELECT DISTINCT ON (rx.id) h.patient_id, h.date abx_dt, h.name, rx.name med_name, rx.quantity
FROM hef_event h 
LEFT JOIN emr_prescription rx ON h.patient_id = rx.patient_id AND rx.id = h.object_id 
WHERE h.name ilike '%Doxycycline%' or h.name ilike '%Amoxicillin%' or h.name ilike '%Cefuroxime%' or
 h.name ilike '%Ceftriaxone%' or h.name ilike '%Cefotaxime%' or h.name ilike '%Azithromycin%' or
 h.name ilike '%Tetracycline%' 
ORDER BY rx.id, rx.name;


-- Ref: checking static_drugsynonym amd emr_prescription

select * from static_drugsynonym
where generic_name ilike '%Doxycycline%' or generic_name ilike '%Amoxicillin%' or generic_name  ilike '%Cefuroxime%' or
generic_name  ilike '%Ceftriaxone%' or generic_name  ilike '%Cefotaxime%' or generic_name  ilike '%Azithromycin%' or
generic_name  ilike '%Tetracycline%' 

select distinct h.name from emr_prescription h
WHERE h.name ilike '%Doxycycline%' or h.name ilike '%Amoxicillin%' or h.name ilike '%Cefuroxime%' or
 h.name ilike '%Ceftriaxone%' or h.name ilike '%Cefotaxime%' or h.name ilike '%Azithromycin%' or
 h.name ilike '%Tetracycline%' 
*/
 
-- Lyme ICD and Abx w/in 14 days of each other and select the first qualified date 
-- 2017

CREATE TABLE lyme_rpt.lyme_rx_qualified AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) AS lyme_dx_abx_14, lyme_idx_date, rx_date, rx_name 
FROM lyme_abx_rx_17 t0
JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id
AND t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
Order BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 2018

CREATE TABLE lyme_rpt.lyme_rx_qualified_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) AS lyme_dx_abx_14, lyme_idx_date, rx_date, rx_name 
FROM lyme_abx_rx_18 t0
JOIN lyme_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id
AND t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
Order BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

--Lyme EIA/ELISA/IFA+ and Abx w/in 14 days and 30 days of any test date, select the first 
-- 14 days of any lab test1 dates and pick the first date 
-- 2017

CREATE TABLE lyme_rpt.ll1_rx_q14 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt1_abx_14, lt1_idx_date, rx_date, rx_name
FROM lyme_abx_rx_17 t0
JOIN lyme_lt1 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;


--Lyme EIA/ELISA/IFA+ and Abx w/in 14 days and 30 days of any test date, select the first 
-- 14 days of any lab test1 dates and pick the first date 
-- 2018

CREATE TABLE lyme_rpt.ll1_rx_q14_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt1_abx_14, lt1_idx_date, rx_date, rx_name
FROM lyme_abx_rx_18 t0
JOIN lyme_lt1_18 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 30 days of any lab test1 dates 
-- 2017

CREATE TABLE lyme_rpt.ll1_rx_q30 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt1_abx_30, lt1_idx_date, rx_date, rx_name
FROM lyme_abx_rx_17 t0
JOIN lyme_lt1 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '30 days') and (t1.date + INTERVAL '30 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 2018

CREATE TABLE lyme_rpt.ll1_rx_q30_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt1_abx_30, lt1_idx_date, rx_date, rx_name
FROM lyme_abx_rx_18 t0
JOIN lyme_lt1_18 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '30 days') and (t1.date + INTERVAL '30 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

--Lyme WB/PCR and Abx w/in 14d of each other 
-- 2017

CREATE TABLE lyme_rpt.ll2_rx_q14 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt2_abx_14, lt2_idx_date, rx_date, rx_name
FROM lyme_abx_rx_17 t0
JOIN lyme_lt2 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 2018

CREATE TABLE lyme_rpt.ll2_rx_q14_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt2_abx_14, lt2_idx_date, rx_date, rx_name
FROM lyme_abx_rx_18 t0
JOIN lyme_lt2_18 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '14 days') and (t1.date + INTERVAL '14 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 30 days of any lab test2 dates 
-- 2017

CREATE TABLE lyme_rpt.ll2_rx_q30 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt2_abx_30, lt2_idx_date, rx_date, rx_name
FROM lyme_abx_rx_17 t0
JOIN lyme_lt2 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '30 days') and (t1.date + INTERVAL '30 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- 2018

CREATE TABLE lyme_rpt.ll2_rx_q30_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, least(t0.rx_date, t1.date) as lt2_abx_30, lt2_idx_date, rx_date, rx_name
FROM lyme_abx_rx_18 t0
JOIN lyme_lt2_18 t1 on t0.patient_id = t1.patient_id and t0.rx_date between (t1.date - INTERVAL '30 days') and (t1.date + INTERVAL '30 days')
ORDER BY t0.patient_id, least(t0.rx_date, t1.date) ASC;

-- Lyme ICD + Single dose Doxy Same Day (one-time and single dose)
-- Atrius 2017 (updated based on Sarah's comment)

-- KRE MOD BELOW
-- CREATE TABLE lyme_rpt.lyme_doxy_rx_qualified AS
-- SELECT distinct on (t0.patient_id) t0.patient_id, t1.lyme_idx_date, t1.date AS lyme_dx_doxy_same_dt, t0.rx_name
-- FROM lyme_abx_rx_17 t0
-- JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
-- AND t0.rx_date = t1.date 
-- where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) or (end_date-start_date <=1)) and (directions ilike '%1 dose%' or quantity = '2 capsule') and (directions not ilike '125 mg%')
-- order by t0.patient_id, t1.date asc;


CREATE TABLE lyme_rpt.lyme_doxy_rx_qualified AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.lyme_idx_date, t1.date AS lyme_dx_doxy_same_dt, t0.rx_name
FROM lyme_abx_rx_17 t0
JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' 
and directions not ilike '125 mg%'
and ( quantity_float <= 2 or (quantity_float is null and (directions ilike '%1 dose%' or directions ilike '%lyme%' or directions ilike '%tick%')) )
order by t0.patient_id, t1.date asc;


-- Atrius 2018 

-- KRE MOD BELOW 
-- CREATE TABLE lyme_rpt.lyme_doxy_rx_qualified_18 AS
-- SELECT distinct on (t0.patient_id) t0.patient_id, t1.lyme_idx_date, t1.date AS lyme_dx_doxy_same_dt, t0.rx_name
-- FROM lyme_abx_rx_18 t0
-- JOIN lyme_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
-- AND t0.rx_date = t1.date 
-- where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) or (end_date-start_date <=1)) and (directions ilike '%1 dose%' or quantity = '2 capsule')
-- order by t0.patient_id, t1.date asc;


CREATE TABLE lyme_rpt.lyme_doxy_rx_qualified_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.lyme_idx_date, t1.date AS lyme_dx_doxy_same_dt, t0.rx_name
FROM lyme_abx_rx_18 t0
JOIN lyme_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' 
and directions not ilike '125 mg%'
and ( quantity_float <= 2 or (quantity_float is null and (directions ilike '%1 dose%' or directions ilike '%lyme%' or directions ilike '%tick%')) )
order by t0.patient_id, t1.date asc;


/*
-- check CHA unique directions
SELECT distinct directions, start_date, end_date, (end_date - start_date) as dur, quantity, quantity_float
FROM lyme_abx_rx_18 t0
JOIN lyme_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' order by quantity_float

-- to check (atrius)
SELECT t0.*
FROM lyme_abx_rx_17 t0
JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) or (end_date-start_date <=1)) and (directions ilike '%1 dose%' or quantity = '2 capsule')

--2018
SELECT distinct directions, quantity, quantity_float, (end_date - start_date) as dur
FROM lyme_abx_rx_18 t0
JOIN lyme_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) or (end_date-start_date <=1)) 

-- invest one-time, single dose doxy on the same day as lyme icd
-- pick up doxy rx

drop table if exists lyme_dox_rx;
CREATE TABLE lyme_dox_rx AS
SELECT DISTINCT *
FROM emr_prescription rx
JOIN static_drugsynonym ds on rx.name ilike '%' || ds.other_name || '%'  
WHERE generic_name ilike '%Doxycycline%' AND rx.date >= '01-01-2017';

drop table if exists lyme_doxy_rx_inv;
CREATE TABLE lyme_doxy_rx_inv AS
SELECT t0.*
FROM lyme_dox_rx t0
JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.date = t1.date 
where t0.generic_name ilike 'doxycycline';

select distinct directions, (end_date - start_date) as dur, quantity, quantity_float from lyme_doxy_rx_inv 
where (quantity_float <=2) or (end_date-start_date <=1)
*/

--Tick Bite ICD + Doxy Same Day (need single dose)
--Atrius 2017 (updated to include Sarah's comment)

 
CREATE TABLE lyme_rpt.tick_doxy_rx_qualified AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.tick_idx_date, t1.date AS tick_dx_doxy_same_dt, t0.rx_name
FROM lyme_abx_rx_17 t0
JOIN tick_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' 
and ( quantity_float <= 2 or (quantity_float is null and (directions ilike '%1 dose%' or directions ilike '%lyme%' or directions ilike '%tick%')) ) 
order by t0.patient_id, date asc;


-- Atrius 2018 (updated to include Sarah's comment)

CREATE TABLE lyme_rpt.tick_doxy_rx_qualified_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.tick_idx_date, t1.date AS tick_dx_doxy_same_dt, t0.rx_name
FROM lyme_abx_rx_18 t0
JOIN tick_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline'
and ( quantity_float <= 2 or (quantity_float is null and (directions ilike '%1 dose%' or directions ilike '%lyme%' or directions ilike '%tick%')) ) 
order by t0.patient_id, date asc;


/*
SELECT distinct directions, quantity, quantity_float, (end_date - start_date) as dur
FROM lyme_abx_rx_18 t0
JOIN tick_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name ilike 'doxycycline' and ((quantity_float <=2) or (end_date-start_date <=1)) and (directions not ilike '%days%' and quantity not in ('60 tablet', '28 capsule'))
*/

-- Tick Bite ICD + Other Lyme Abx Same Day 
-- 2017

CREATE TABLE lyme_rpt.tick_other_rx_qualified AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.tick_idx_date, t1.date AS tick_dx_other_same_dt, t0.rx_name
FROM lyme_abx_rx_17 t0
JOIN tick_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name not ilike 'doxycycline' 
order by t0.patient_id, date asc;

-- 2018

CREATE TABLE lyme_rpt.tick_other_rx_qualified_18 AS
SELECT distinct on (t0.patient_id) t0.patient_id, t1.tick_idx_date, t1.date AS tick_dx_other_same_dt, t0.rx_name
FROM lyme_abx_rx_18 t0
JOIN tick_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
AND t0.rx_date = t1.date 
where t0.generic_name not ilike 'doxycycline' 
order by t0.patient_id, date asc;

-- join icd, lab test tables
/*
drop table if exists lyme_idx_patient ;
CREATE TABLE lyme_idx_patient AS
SELECT distinct COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id) as patient_id,
t0.tick_idx_date, t1.lyme_idx_date, t2.lt1_idx_date, t3.lt2_idx_date
FROM tick_dx_enc_idx t0
FULL JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
FULL JOIN lyme_lt1 t2 on t2.patient_id = COALESCE(t0.patient_id, t1.patient_id)
FULL JOIN lyme_lt2 t3 on t3.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id) 
order by patient_id;
*/

-- updated to separate out lt2 tests
-- 2017

CREATE TABLE lyme_rpt.lyme_idx_patient AS
SELECT distinct COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id, t5.patient_id) as patient_id,
t0.tick_idx_date, t1.lyme_idx_date, t2.lt1_idx_date, t3.igg_wb_idx_date, t4.igm_wb_idx_date, t5.pcr_idx_date
FROM tick_dx_enc_idx t0
FULL JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
FULL JOIN lyme_lt1 t2 on t2.patient_id = COALESCE(t0.patient_id, t1.patient_id)
FULL JOIN lt_igg_wb t3 on t3.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id) 
FULL JOIN lt_igm_wb t4 on t4.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id) 
FULL JOIN lt_pcr t5 on t5.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id) 
order by patient_id;

-- 2018

CREATE TABLE lyme_rpt.lyme_idx_patient_18 AS
SELECT distinct COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id, t5.patient_id) as patient_id,
t0.tick_idx_date, t1.lyme_idx_date, t2.lt1_idx_date, t3.igg_wb_idx_date, t4.igm_wb_idx_date, t5.pcr_idx_date
FROM tick_dx_enc_idx_18 t0
FULL JOIN lyme_dx_enc_idx_18 t1 on t0.patient_id = t1.patient_id 
FULL JOIN lyme_lt1_18 t2 on t2.patient_id = COALESCE(t0.patient_id, t1.patient_id)
FULL JOIN lt_igg_wb_18 t3 on t3.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id) 
FULL JOIN lt_igm_wb_18 t4 on t4.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id) 
FULL JOIN lt_pcr_18 t5 on t5.patient_id = COALESCE(t0.patient_id, t1.patient_id, t2.patient_id, t3.patient_id, t4.patient_id) 
order by patient_id;

-- pick up rest of the stuff
-- 2017

CREATE TABLE lyme_rpt.lyme_idx_patient_all AS
SELECT distinct t0.*, t1.lyme_dx_abx_14, t2.lt1_abx_14, t3.lt1_abx_30, t4.lt2_abx_14, t5.lt2_abx_30,
       t6.lyme_dx_doxy_same_dt, t7.tick_dx_doxy_same_dt, t8.tick_dx_other_same_dt
FROM lyme_idx_patient t0
LEFT JOIN lyme_rx_qualified t1 on t0.patient_id = t1.patient_id
LEFT JOIN ll1_rx_q14 t2 on t0.patient_id = t2.patient_id
LEFT JOIN ll1_rx_q30 t3 on t0.patient_id = t3.patient_id
LEFT JOIN ll2_rx_q14 t4 on t0.patient_id = t4.patient_id
LEFT JOIN ll2_rx_q30 t5 on t0.patient_id = t5.patient_id
LEFT JOIN lyme_doxy_rx_qualified t6 on t0.patient_id = t6.patient_id
LEFT JOIN tick_doxy_rx_qualified t7 on t0.patient_id = t7.patient_id
LEFT JOIN tick_other_rx_qualified t8 on t0.patient_id = t8.patient_id;
 
-- 2018
 
CREATE TABLE lyme_rpt.lyme_idx_patient_all_18 AS
SELECT distinct t0.*, t1.lyme_dx_abx_14, t2.lt1_abx_14, t3.lt1_abx_30, t4.lt2_abx_14, t5.lt2_abx_30,
       t6.lyme_dx_doxy_same_dt, t7.tick_dx_doxy_same_dt, t8.tick_dx_other_same_dt
FROM lyme_idx_patient_18 t0
LEFT JOIN lyme_rx_qualified_18 t1 on t0.patient_id = t1.patient_id
LEFT JOIN ll1_rx_q14_18 t2 on t0.patient_id = t2.patient_id
LEFT JOIN ll1_rx_q30_18 t3 on t0.patient_id = t3.patient_id
LEFT JOIN ll2_rx_q14_18 t4 on t0.patient_id = t4.patient_id
LEFT JOIN ll2_rx_q30_18 t5 on t0.patient_id = t5.patient_id
LEFT JOIN lyme_doxy_rx_qualified_18 t6 on t0.patient_id = t6.patient_id
LEFT JOIN tick_doxy_rx_qualified_18 t7 on t0.patient_id = t7.patient_id
LEFT JOIN tick_other_rx_qualified_18 t8 on t0.patient_id = t8.patient_id;

-- output run this for 2017 and 2018
-- 2017

CREATE TABLE lyme_rpt.lyme_ll_fin_17 AS
SELECT 
concat(T1.last_name, ', ', T1.first_name) as name,
T1.mrn,
date_of_birth as dob,
concat(T2.last_name, ', ', T2.first_name, ' ', T2.title) as pcp_name,
T2.dept as pcp_site,
gender as sex,
t5.*
FROM emr_patient T1
LEFT JOIN emr_provider T2 on (T1.pcp_id = T2.id)
JOIN lyme_idx_patient_all T5 on t1.id = t5.patient_id
WHERE substr(t1.gender,1,1) in ('M', 'F')
AND T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%';

-- 2018

CREATE TABLE lyme_rpt.lyme_ll_fin_18 AS
SELECT 
concat(T1.last_name, ', ', T1.first_name) as name,
T1.mrn,
date_of_birth as dob,
concat(T2.last_name, ', ', T2.first_name, ' ', T2.title) as pcp_name,
T2.dept as pcp_site,
gender as sex,
t5.*
FROM emr_patient T1
LEFT JOIN emr_provider T2 on (T1.pcp_id = T2.id)
JOIN lyme_idx_patient_all_18 T5 on t1.id = t5.patient_id
WHERE substr(t1.gender,1,1) in ('M', 'F')
AND T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%';

--final output (updated)

CREATE TABLE lyme_rpt.lyme_output_17 AS
SELECT 
name,
mrn,
dob,
sex,
pcp_name,
pcp_site,
tick_idx_date,
lyme_idx_date,
lt1_idx_date,
igg_wb_idx_date, 
igm_wb_idx_date, 
pcr_idx_date,
lyme_dx_abx_14,
lt1_abx_14,
lt2_abx_14,
lt1_abx_30,
lt2_abx_30,
lyme_dx_doxy_same_dt,
tick_dx_doxy_same_dt,
tick_dx_other_same_dt
FROM lyme_ll_fin_17;

--2018

CREATE TABLE lyme_rpt.lyme_output_18 AS
SELECT 
name,
mrn,
dob,
sex,
pcp_name,
pcp_site,
tick_idx_date,
lyme_idx_date,
lt1_idx_date,
igg_wb_idx_date, 
igm_wb_idx_date, 
pcr_idx_date,
lyme_dx_abx_14,
lt1_abx_14,
lt2_abx_14,
lt1_abx_30,
lt2_abx_30,
lyme_dx_doxy_same_dt,
tick_dx_doxy_same_dt,
tick_dx_other_same_dt
FROM lyme_ll_fin_18;


--count

select count(*) as total, 
sum(case when tick_idx_date is not null then 1 else 0 end) as tick_dx, 
sum(case when lyme_idx_date is not null then 1 else 0 end) as lyme_dx, 
sum(case when lt1_idx_date is not null then 1 else 0 end) as lt1, 
sum(case when igg_wb_idx_date is not null then 1 else 0 end) as igg, 
sum(case when igm_wb_idx_date is not null then 1 else 0 end) as igm, 
sum(case when pcr_idx_date is not null then 1 else 0 end) as prc, 
sum(case when lyme_dx_abx_14 is not null then 1 else 0 end) as lyme_dose, 
sum(case when lt1_abx_14 is not null then 1 else 0 end) as lt1_14, 
sum(case when lt2_abx_14 is not null then 1 else 0 end) as lt2_14, 
sum(case when lt1_abx_30 is not null then 1 else 0 end) as lt1_30, 
sum(case when lt2_abx_30 is not null then 1 else 0 end) as lt2_30, 
sum(case when lyme_dx_doxy_same_dt is not null then 1 else 0 end) as lyme_same_dose, 
sum(case when tick_dx_doxy_same_dt is not null then 1 else 0 end) as tick_same_dose,
sum(case when tick_dx_other_same_dt is not null then 1 else 0 end) as tick_other_dose
from lyme_rpt.lyme_output_17;


\copy (select * from lyme_rpt.lyme_output_17) to '/home/dph/lyme_linelist_2017_bmc_08202020.csv' with CSV delimiter ',' header;
  
\copy (select * from lyme_rpt.lyme_output_18) to '/home/dph/lyme_linelist_2018_bmc_08202020.csv' with CSV delimiter ',' header;



-- look at the results for those not mapped
/*
--alternatively
drop table if exists lyme_ll_output_17;
CREATE TABLE lyme_ll_output_17 AS
SELECT 
concat(T1.last_name, ', ', T1.first_name) as name,
T1.mrn,
date_of_birth as dob,
concat(T2.last_name, ', ', T2.first_name, ' ', T2.title) as pcp_name,
T2.dept as pcp_site,
gender as sex,
t5.*
FROM emr_patient T1
LEFT JOIN emr_provider T2 on (T1.pcp_id = T2.id)
--JOIN emr_encounter T3 on (T1.id = T3.patient_id)
--LEFT JOIN static_enc_type_lookup T4 on (T3.raw_encounter_type = T4.raw_encounter_type)
JOIN lyme_idx_patient_all T5 on t1.id = t5.patient_id
WHERE gender in ('M', 'MALE', 'FEMALE', 'F')
-- must have an ambulatory encounter in the past 2 years
-- AND T4.ambulatory = 1
-- filter out test patients
AND T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%'
AND T1.last_name not ilike 'XYZ%'
AND T1.last_name not ilike 'ZZ%'
AND T1.last_name not ilike 'YY%'
AND T1.last_name not ilike 'test'
AND T1.last_name not ilike 'esp'
AND T1.first_name NOT ILIKE 'test'
AND T1.first_name not ilike 'yytest%'
AND T1.first_name not ILIKE 'zztest%';


-- invest number of pt with doxcycline
drop table if exists lyme_dox_rx;
CREATE TABLE lyme_dox_rx AS
SELECT DISTINCT *
FROM emr_prescription rx
JOIN static_drugsynonym ds on rx.name ilike '%' || ds.other_name || '%'  
WHERE generic_name ilike '%Doxycycline%' AND rx.date >= '01-01-2017';

drop table if exists tick_doxy_rx_inv;
CREATE TABLE tick_doxy_rx_inv AS
SELECT t0.* 
FROM lyme_dox_rx t0
JOIN tick_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.date = t1.date 
where t0.generic_name ilike 'doxycycline';

drop table if exists lyme_doxy_rx_inv;
CREATE TABLE lyme_doxy_rx_inv AS
SELECT t0.*
FROM lyme_dox_rx t0
JOIN lyme_dx_enc_idx t1 on t0.patient_id = t1.patient_id 
AND t0.date = t1.date 
where t0.generic_name ilike 'doxycycline';

drop table if exists doxy_rx_inv_all;
CREATE TABLE doxy_rx_inv_all AS
select * from lyme_doxy_rx_inv
union
select * from tick_doxy_rx_inv
union 
select * from lyme_doxy_rx_inv_18
union 
select * from tick_doxy_rx_inv_18


select distinct directions, (end_date - start_date) as dur, quantity, quantity_float from doxy_rx_inv_all where true = case when quantity is null then end_date - start_date <=1 
when end_date is null or start_date is null then quantity_float <=2 else (quantity_float <=2 and end_date-start_date <=1) end

select count(distinct patient_id), 
sum(case when quantity_float is null and quantity is null then 1 else 0 end) as no_quantity, 
sum(case when end_date is null then 1 else 0 end) as no_enddt,
sum(case when start_date is null or end_date is null then 1 else 0 end) as no_dur,
sum(case when start_date is null and end_date is null and quantity_float is null and quantity is null then 1 else 0 end) as no_all
from  tick_doxy_rx_inv


select count(*) as total, sum(case when lyme_dx_doxy_same_dt is not null then 1 else 0 end) as lyme_dose, 
sum(case when tick_dx_doxy_same_dt is not null then 1 else 0 end) as tick_dose
from lyme_ll_output_17






*/