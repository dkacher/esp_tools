
-- Total STI encounters
SELECT count(*)
FROM emr_stiencounterextended T1,
emr_encounter T2
WHERE T1.natural_key = T2.natural_key 
AND raw_encounter_type ilike '%VISIT%'
AND date BETWEEN '04-01-2017' AND '03-31-2018';


-- Total STI encounter patients
SELECT count(distinct(patient_id))
FROM emr_stiencounterextended T1,
emr_encounter T2
WHERE T1.natural_key = T2.natural_key 
AND raw_encounter_type ilike '%VISIT%'
AND date BETWEEN '04-01-2017' AND '03-31-2018';



-- count of distinct STI patients by facility
SELECT count(distinct(T1.patient_id)), 
CASE WHEN T2.site_natural_key in ('05F19D3B-E18F-4971-AB29-32A5904E67FE', '6') then 'PPLM Marlborough' 
	 when T2.site_natural_key in ('22DF4A2A-45F5-4D1B-A279-134E3F6168F1', '3') then 'PPLM Worcester'
	 when T2.site_natural_key = '588A5941-A016-462D-ADB1-8563ABD0F013' then 'PPLM Milford' 
	 when T2.site_natural_key in ('76E86AFC-EB87-44C7-A400-BC8FF6E15654', '4') then 'PPLM Springfield' 
	 when T2.site_natural_key in ('9D971E61-2B5A-4504-9016-7FD863790EE2', '2') then 'PPLM Boston'
	 when T2.site_natural_key in ('DE857862-F1A8-4985-997E-7C03D0CC8081', '5') then 'PPLM Fitchburg' 
	 when T2.site_natural_key = 'F32D324A-6AAB-4718-8310-132B5A223482' then 'PPLM Som Express' 
	 else 'PPLM - OTHER'
	 END facility
FROM emr_stiencounterextended T1, emr_encounter T2
WHERE T1.natural_key = T2.natural_key
AND raw_encounter_type ilike '%VISIT%'
AND date BETWEEN '04-01-2017' AND '03-31-2018'
GROUP BY facility;



-- count of encounters by facility
SELECT count(*), 
CASE WHEN T2.site_natural_key in ('05F19D3B-E18F-4971-AB29-32A5904E67FE', '6') then 'PPLM Marlborough' 
	 when T2.site_natural_key in ('22DF4A2A-45F5-4D1B-A279-134E3F6168F1', '3') then 'PPLM Worcester'
	 when T2.site_natural_key = '588A5941-A016-462D-ADB1-8563ABD0F013' then 'PPLM Milford' 
	 when T2.site_natural_key in ('76E86AFC-EB87-44C7-A400-BC8FF6E15654', '4') then 'PPLM Springfield' 
	 when T2.site_natural_key in ('9D971E61-2B5A-4504-9016-7FD863790EE2', '2') then 'PPLM Boston'
	 when T2.site_natural_key in ('DE857862-F1A8-4985-997E-7C03D0CC8081', '5') then 'PPLM Fitchburg' 
	 when T2.site_natural_key = 'F32D324A-6AAB-4718-8310-132B5A223482' then 'PPLM Som Express' 
	 else 'PPLM - OTHER'
	 END facility
FROM emr_stiencounterextended T1, emr_encounter T2
WHERE T1.natural_key = T2.natural_key
AND raw_encounter_type ilike '%VISIT%'
AND date BETWEEN '04-01-2017' AND '03-31-2018'
GROUP BY facility;

-- Clinic visits missing gender
(SELECT count(patient_id) missing_gender_count
FROM emr_stiencounterextended sti
INNER JOIN emr_patient p ON (sti.patient_id = p.id)
INNER JOIN emr_encounter e on ON (sti.natural_key = e.natural_key AND p.id = e.patient_id) 
WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
AND (gender in ('U' ,'UNKNOWN','', ' ') or gender is null)
AND raw_encounter_type ilike '%VISIT%';

-- Clinic visits missing race
(SELECT count(patient_id) missing_race_count
FROM emr_stiencounterextended sti
INNER JOIN emr_patient p ON (sti.patient_id = p.id)
INNER JOIN emr_encounter e on ON (sti.natural_key = e.natural_key AND p.id = e.patient_id) 
WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
AND (race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported', 'Patient Declined', '') or race is null)
AND raw_encounter_type ilike '%VISIT%';


-- Clinic visits missing insurance status
(SELECT count(patient_id) missing_insurance_count
FROM emr_stiencounterextended sti
INNER JOIN emr_patient p ON (sti.patient_id = p.id)
INNER JOIN emr_encounter e on ON (sti.natural_key = e.natural_key AND p.id = e.patient_id) 
WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
AND (insurance_status = '' or insurance_status is null)
AND raw_encounter_type ilike '%VISIT%';






