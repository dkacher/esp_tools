-- DIAGS, LABS, MEDS are also limited to only VISIT encounter types. This may need to be modified based on the output.
-- LABS do new pregnancy tests need to be mapped?
-- psql -d esp -f pplm-sti-mappings-gather.sql > pplm-ouput-for-karen.txt

select count(*), raw_encounter_type from emr_encounter
group by raw_encounter_type;

select count(*), site_natural_key from emr_encounter
group by site_natural_key;

select count(*), sti_symptoms_yn from emr_stiencounterextended
group by sti_symptoms_yn;

select count(*), sti_exposure_yn from emr_stiencounterextended
group by sti_exposure_yn;

select count(*), pelvic_exam_yn from emr_stiencounterextended
group by pelvic_exam_yn;

select count(*), num_male_partners from emr_stiencounterextended
group by num_male_partners;

select count(*), num_female_partners from emr_stiencounterextended
group by num_female_partners;

select count(*), total_partners from emr_stiencounterextended
group by total_partners;

select count(*), sex_mwb from emr_stiencounterextended
group by sex_mwb;

select count(*), sexuality from emr_stiencounterextended
group by sexuality;

select count(*), new_partners_yn from emr_stiencounterextended
group by new_partners_yn;

select count(*), anal_sex_yn from emr_stiencounterextended
group by anal_sex_yn;

select count(*), oral_sex_yn from emr_stiencounterextended
group by oral_sex_yn;

select count(*), ept_accepted_yn from emr_stiencounterextended
group by ept_accepted_yn;

select count(*), hiv_test_ever_yn from emr_stiencounterextended
group by hiv_test_ever_yn;

select count(*), hiv_test_date from emr_stiencounterextended
group by hiv_test_date;

select count(*), hiv_result from emr_stiencounterextended
group by hiv_result;

select count(*), hiv_test_refuse_yn from emr_stiencounterextended
group by hiv_test_refuse_yn;

select count(*), hpv_vac_yn from emr_stiencounterextended
group by hpv_vac_yn;

select count(*), ab_pain_rptd from emr_stiencounterextended
group by ab_pain_rptd;

select count(*), dysuria_rptd from emr_stiencounterextended
group by dysuria_rptd;

select count(*), discharge_rptd from emr_stiencounterextended
group by discharge_rptd;

select count(*), gen_lesion_rptd from emr_stiencounterextended
group by gen_lesion_rptd;

select count(*), vag_discharge_on_exam from emr_stiencounterextended
group by vag_discharge_on_exam;

select count(*), lower_ab_pain_on_exam from emr_stiencounterextended
group by lower_ab_pain_on_exam;

select count(*), cerv_tend_on_exam from emr_stiencounterextended
group by cerv_tend_on_exam;

select count(*), adnexal_tend_on_exam from emr_stiencounterextended
group by adnexal_tend_on_exam;

select 'BEGIN LABS SECTION'; 

select l.native_code, test_name, native_name, procedure_name
from emr_labresult l, conf_labtestmap c
where l.native_code = c.native_code
and test_name in ('gonorrhea', 'chlamydia', 'hiv', 'pregnancy')
group by l.native_code, test_name, native_name, procedure_name;

select count(*), result_string
from emr_labresult l, conf_labtestmap c
where l.native_code = c.native_code
and test_name in ('gonorrhea', 'chlamydia', 'hiv', 'pregnancy')
group by result_string;

select count(*), specimen_source
from emr_labresult l, conf_labtestmap c
where l.native_code = c.native_code
and test_name in ('gonorrhea', 'chlamydia', 'hiv', 'pregnancy')
group by specimen_source;

select 'BEGIN MED SECTION';

select count(*), name from emr_prescription t where 
t.name not ilike '%amoxicillin%' 
and t.name not ilike '%augmentin%' 
and t.name not ilike  '%AMOX TR-POTASSIUM CLAVULANATE%' 
and t.name not ilike '%ampicillin%' 
and t.name not ilike '%AMPICIL TRIHYD/PROBENECID%' 
and t.name not ilike '%azithromycin%' 
and t.name not ilike '%zithromax%' 
and t.name not ilike '%cefdinir%' 
and t.name not ilike '%omincef%' 
and t.name not ilike '%cipro%'
and t.name not ilike '%clindamycin%' 
and t.name not ilike '%cleocin%'
and t.name not ilike '%CLINDESSE%' 
and t.name not ilike '%LEVOFLOX%' 
and t.name != 'LEVAQUIN' 
and t.name not ilike '%MOXIFLOX%'
and t.name not ilike '%CEFIXIME%'
and t.name != 'SUPRAX' 
and t.name not ilike '%cefotaxime%'
and t.name not ilike '%cefoxitin%' 
and t.name not ilike '%cefuroxime%' 
and t.name not ilike '%doxycycline%' 
and t.name not ilike '%VIBRAMYCIN%'
and t.name not ilike '%DORYX%' 
and t.name not ilike '%ORAXYL%' 
and t.name not ilike '%VIBRA-TABS%' 
and t.name not ilike '%erythromycin base%' 
and t.name != '%GATIFLOXACIN%'
and t.name not ilike '%gentamicin%' 
and t.name not ilike '%GENTAM SULF/SODIUM CHLORIDE%' 
and t.name not ilike '%FACTIVE%' 
and t.name not ilike '%GEMIFLOXACIN MESYLATE%' 
and t.name not ilike '%metronidazole%' 
and t.name not ilike '%FLAGYL%' 
and t.name not ilike '%metrogel%' 
and t.name not ilike '%CEFTIBUTEN%' 
and t.name not ilike '%ceftriaxone%' 
and t.name not ilike '%cefpodoxime%'
and t.name not ilike '%tinidazole%' 
and t.name not ilike '%TINDAMAX%' 
and t.name not ilike '%OFLOXACIN%' 
and t.name not ilike '%truvada%' 
and t.name not ilike '%EMTRICITABINE/TENOFOVIR%'
group by t.name
order by t.name;


select count(*), dose from emr_prescription t 
where t.dose != '100 mg' 
and t.dose != '125 mg/5 mL' 
and t.dose != '150 mg' 
and t.dose != '150 mg/' 
and t.dose != '150 mg/5 mL' 
and t.dose != '200 mg' 
and t.dose != '200 mg/5 mL' 
and t.dose != '250 mg'
and t.dose != '300 mg'
and t.dose != '320 mg'
and t.dose != '400 mg' 
and t.dose != '400 mg/mL' 
and t.dose not ilike '400 mg/20 mL%'
and t.dose != '500 mg'
and t.dose != '600 mg'
and t.dose != '800 mg'
and t.dose != '1 gram' 
and t.dose != '1 gram/10 mL'
group by t.dose
order by t.dose;




































