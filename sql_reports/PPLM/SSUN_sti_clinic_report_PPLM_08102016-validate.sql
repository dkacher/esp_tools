﻿-- Analysis script generated on Tuesday, 01 Mar 2016 16:37:05 EST
-- Analysis name: pplm - STI Report - Clinic Variables
-- Analysis description: pplm - STI Report - Clinic Variables
-- Script generated for database: POSTGRESQL

--
-- Script setup section 
--

set client_min_messages to error;


DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_3 CASCADE;
DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_3 CASCADE;

DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_4 CASCADE;
DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_4 CASCADE;

DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_5 CASCADE;
DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_5 CASCADE;

--
-- Script body 
--


-- Step 3: Access - public.emr_encounter
CREATE VIEW public.pplm_sti_clinic_a_100037_s_3 AS SELECT * FROM public.emr_encounter where raw_encounter_type ilike '%VISIT%';

-- Step 4: Join - Join extended table against encounter to get the encounter id for the report. Create/transform all fields to values
CREATE TABLE public.pplm_sti_clinic_a_100037_s_4  AS SELECT 
case when T2.site_natural_key in ('05F19D3B-E18F-4971-AB29-32A5904E67FE', '6') then '2' --Pplm Marlborough 
	 when T2.site_natural_key in ('22DF4A2A-45F5-4D1B-A279-134E3F6168F1', '3') then '3' --Pplm Worcester
	 when T2.site_natural_key in ('588A5941-A016-462D-ADB1-8563ABD0F013')  then '4' --Pplm Milford     
	 when T2.site_natural_key in ('76E86AFC-EB87-44C7-A400-BC8FF6E15654', '4') then '5' --Pplm Springfield 
	 when T2.site_natural_key in ('9D971E61-2B5A-4504-9016-7FD863790EE2', '2') then '6' --Pplm Boston 
	 when T2.site_natural_key in ('DE857862-F1A8-4985-997E-7C03D0CC8081', '5') then '7' --Pplm Fitchburg   
	 when T2.site_natural_key = 'F32D324A-6AAB-4718-8310-132B5A223482' then '8' --Pplm Som Express 
	 else concat('99999:site_natural_key: ', site_natural_key)
	 end f1_facilityid,
'MA' f1_siteid,
T2.patient_id f1_patientid,
T2.date f1_visdate,
T2.id f1_eventid,
null f1_gisp_yrmo,
null f1_gisp_number,
case when T1.insurance_status in ('Medicaid', 'AB Funding', 'Abortion Fund', 'Medicare', 'Public', 'Medicare A', 'BMC Healthnet Plan', 'Celticare', 'Fallon', 'Health New England', 'Neighborhood Health Plan', 'Tufts Health Public Plans') then '1'
	when T1.insurance_status in ('United Healthcare', 'Harvard Pilgrim Health Care', 'Minuteman', 'Cigna', 'Aetna', 'Tufts', 'Blue Cross Blue Shield', 'Student Plans', 'Pharmacy', 'Auto-Renew', 'Pharmacy Billing', 'Private') then '2' 
	when T1.insurance_status in ('Non-Participating', 'Self-Pay') then '5' 
	when T1.insurance_status is null then null
	else concat('99999:insurance_status: ', insurance_status)
	end f1_insurance, 
case when T2.raw_encounter_type in ('GYN VISIT', 'OFFICE VISIT', 'CONSULT', 'OFFICE VISIT - GYN', 'PREVENTIVE MEDICINE-GYN', 'VISIT') then '1'
	when T2.raw_encounter_type in ('CHART UPDATE', 'PATIENT COMMUNICATION', 'CHART ABSTRACTION', 'CHART UPDATE - GYN') then '8'
	when T2.raw_encounter_type is null then null
	else concat('99999:raw_encounter_type: ', raw_encounter_type)
	end f1_visit_type,
case when T1.primary_purpose in ('Abdominal Pain', 'Abnormal Vaginal Bleeding', 'Absent Menses', 'Breast Mass/Lump', 'Breast Pain', 'Foreign Body Removal (F)', 'Genital Lesion (F)', 'Genital Lesion (M)', 'Genital Pain (F)', 'Genital Pain (M)', 'Irregular Menses', 'Nipple Discharge', 'Pelvic Pain', 'Sexual Problems (F)', 'STI Symptoms (F)', 'Urinary Symptoms (F)', 'Vaginal Infection Symptoms', 'Menopausal Symptoms', 'Miscarriage Management', 'Perimenopausal Symptoms', 'BREAST PROBLEM', 'PROBLEM', 'PROBLEM AB', 'Problem Abortion Post', 'URINARY TRACT INFECTION', 'Genital Lesion (F)', 'Genital Lesion (M)', 'Genital Lesions', 'Genital Pain (F)', 'Genital Symptoms (Other)', 'Painful Intercourse', 'Peri/Menopausal Symptoms', 'Sexual Problems (F)', 'STI Symptoms (F)', 'Urinary Symptoms', 'Urinary Symptoms (F)', 'Vulvar/ Vaginal Symptoms', 'Breast Problem') then '1'
	when  T1.primary_purpose in ('Molluscum Treatment', 'STI Treatment', 'STI Treatment (M)', 'Wart Treatment (F)', 'PrEP ESTABLISHED', 'PrEP NEW/RESTART', 'WART/MOLLUSCUM TREATMENT', 'PrEP Initiate/Restart', 'Wart Treatment', 'Wart Treatment (F)') then '2'
	when T1.primary_purpose in ('Follow-up/repeat Pap', 'Abortion Post', 'Abortion Post (Problem)', 'Medication Abortion Post', 'Post Med AB Lab Only', 'Post Colposcopy Testing', 'Post Med AB Lab Only', 'Medication Abortion Post (Phone)', 'Post LEEP Testing', 'EPL Follow-up', 'EPL Post (Medication)', 'Follow-up', 'PID Follow Up', 'PID Follow-Up', 'ABNORMAL PAP FOLLOW UP', 'POST ABORTION VISIT', 'Post Med AB', 'Post Med AB IUD/Implant', 'Abnormal Pap Follow-up', 'Abortion (Post)', 'Abortion Post (Problem)', 'Abortion (problem)', 'EPL Post (Medication)', 'Followup: Abnormal cervical Papanicolaou smear', 'Followup: Active immunization', 'Followup: Cervical intraepithelial neoplasia', 'Followup: Elective termination of pregnancy', 'Followup: Elevated blood pressure', 'Followup: Family planning surveillance', 'Followup: Female pelvic inflammatory disease', 'Followup: Follow-up encounter', 'Followup: Follow-up visit', 'Followup: Genital herpes simplex', 'Followup: Gynecologic examination', 'Followup: Herpetic ulceration of vulva', 'Followup: Initial prescription of oral contraception', 'Followup: Initiation of depot contraception done', 'Followup: Oral contraceptive prescribed', 'Followup: Prescription of contraception', 'Followup: Surveillance of contraception', 'Followup: Surveillance of depot contraception done', 'Followup: Syphilis', 'Followup: Vaginitis and vulvovaginitis', 'Followup: Venereal disease screening', 'Medication Abortion Post (Phone)', 'PrEP Follow-up', 'Followup: Complete legal termination of pregnancy', 'Followup: Complete abortion', 'Followup: Oral contraception', 'Followup: Molluscum contagiosum infection', 'Followup: Pregnancy test negative', 'Followup: Breast lump') then '3'
	when  T1.primary_purpose in ('Abortion', 'DMPA Injection Visit', 'EC', 'EPL', 'Essure Pre-Screen', 'Essure Procedure', 'Family Planning Counseling', 'Hormonal Contraception', 'HPV Repeat Injection', 'Implant check', 'Implant Insertion', 'Implant Removal', 'IUC Check', 'IUC Insertion', 'IUC Removal', 'Medication Abortion', 'Pregnancy Test', 'Prescription Barrier Method', 'AB 1ST OR MAB', 'ABORTION 1ST TRI/AB PREP', 'ABORTION 1ST TRIMESTER', 'ABORTION 2ND TRIMESTER', 'AB WITH PREP', 'CERVICAL DILATOR INSERTION', 'CERVICAL DILATOR INSERT LATE', 'CONTRACEPTION', 'CONTRACEPTIVE SALES', 'DEPO INJECTION', 'Depo Injection RN', 'EARLY PREGNANCY LOSS', 'ECS ONLY', 'Emergency Contraception', 'ESSURE', 'ESSURE PRESCREEN', 'HCA Pregnancy Test', 'HCA Lab Work (HCG)', 'IMPLANT INSERTION-20', 'IMPLANT REMOVAL-20', 'IMPLANTREMOVAL/IUDINSERTION-20', 'IMPLANT REMOVAL/REINSERTION-20', 'INJECTION VISIT', 'IUD CHECK', 'IUD INSERTION-20', 'IUD REMOVAL', 'IUDREMOVAL/IMPLANTINSERTION-20', 'IUD REMOVAL REINSERTION-20', 'LATE 2ND TRIMESTER', 'PRESCRIPTION BARRIER', 'LATE 2ND TRIMESTER', 'WALK IN AB', 'AB IUC Insertion', 'Abortion (Medication)', 'Abortion Post/ Implant Insertion', 'Abortion Post/ IUC Insertion', 'Abortion Surgical 1st Trimester', 'Abortion Surgical 1st Trimester/ Cervical Prep', 'Abortion Surgical 1st Trimester/ Same Day Cervical Prep', 'Abortion Surgical 2nd Trimester/ Cervical Prep', 'Abortion Surgical 2nd Trimester Day 1', 'Abortion Surgical 2nd Trimester Day 2', 'Abortion Surgical 2nd Trimester/Same Day Cervical Prep', 'Abortion(Surgical)-Aspiration procedure after failed AB or post AB with symptoms', 'Early Pregnancy Loss (Expectant)', 'Early Pregnancy Loss (Medication)', 'Early Pregnancy Loss (Surgical)', 'Implant Removal/ IUC Insertion', 'Implant Removal/ Reinsertion', 'IUC Removal/ Implant Insertion', 'IUC Removal/ Reinsertion', 'Pregnancy of Unknown Location', 'Contraception', 'Contraceptive Sales', 'Essure Prescreen') then '4'
	when  T1.primary_purpose in ('STI Testing', 'STI Testing (M)', 'HIV Results', 'STI TESTING', 'STI Screening', 'STI Testing (M)') then '5'
	when  T1.primary_purpose in ('Counseling/Education', 'Counseling/Education (F)', 'Immunization', 'Inconclusive Ultrasound', 'Other GYN Visit', 'LEEP', 'Well Woman Visit', 'Colposcopy', 'Lab Only', 'Endometrial Biopsy', 'Vulvar Biopsy', 'Ultrasound', 'Bartholin''s Cyst I & D', 'Polyp Removal', 'refill pick-up only', 'ACCESS', 'ANNUAL-20', 'ANY 10', 'ANY 20', 'CLOTHES ON', 'COLPOSCOPY', 'CS Notification #3', 'ENDOMETRIAL BIOPSY', 'HCA BP Check', 'HCA Lab Work', 'HPV IMMUNIZATION', 'HPV IMMUNIZATION RN', 'INFECTION CHECK', 'LAB RESULTS', 'MENOPAUSE MANAGEMENT', 'NON-CLINICIAN', 'RESEARCH', 'RESEARCH FOLLOW-UP', 'SAME DAY', 'WALK IN', 'WALK IN CLOTHES ON', 'WALK IN GYN EXAM', 'WALK IN GYN PROCEDURE', 'Abortion Counseling', 'Access (Ultrasound only)', 'Blood Pressure Check', 'Cervical Polyp', 'Counseling/Education (F)', 'Foreign Body Removal', 'Foreign Body Removal (F)', 'Lab Only-HCG', 'Lab Only-Other', 'Lab Results', 'Other', 'Preventive Visit', 'Repeat Misoprostol post MAB', 'Research IUC Check', 'Research IUC Insertion', 'Research IUC Screening/ Insertion', 'Suction procedure post AB', 'condoms', 'Access') then '8'
	when  T1.primary_purpose is null then null 
	else concat('99999:primary_purpose: ', primary_purpose)
	end f1_reason_visit,
case when T1.pregnant_today_yn = '1' then '1'
	when T1.pregnant_today_yn = '2'  then '2'
	when T1.pregnant_today_yn = '3'  then '3'
	when T1.pregnant_today_yn = '9'  then '9'
	when T1.pregnant_today_yn is null then null
	else concat('99999:pregnant_today_yn: ', pregnant_today_yn)
	end f1_pregnant,	
case when T1.primary_contraception in ('Injection', 'injections (DMPA)', 'Oral (CHC)', 'Oral (POP)', 'oral contraceptives', 'Patch', 'patch', 'DMPA', 'EC', 'EC and condoms', 'EC and implant at f/u', 'injections (DMPA), EC', 'IUS (Levonorgestrel)', 'Oral (POP)', 'oral contraceptives, Ella', 'oral contraceptives, IUC Miren', 'POP''s', 'Progestin-Only Pills (POPs)', 'POPs', 'patch, EC', 'emergency contracption', 'oral contraceptives/condoms', 'Oral Contraceptives', 'Oral (CHC)- from PCP', 'patch, Ella', 'emergency contraception', 'emergency contraceptive', 'POP', 'Oral (COC)') then '1'
	when T1.primary_contraception in ('Implant', 'implant', ' implant', 'IUC', 'IUC, Liletta', 'IUC, Mirena', 'IUC, other', 'IUC, Paragard', 'ring', 'Ring', 'diaphragm', 'IUC (Copper)', 'IUC, skyla', 'IUD', 'IUD, Skyla', 'IUC, Paragard/condoms', 'Mirena', 'IUC, Mirena thru PCP', 'Skyla', 'Paragard/OCP', 'Diaphragm', 'IUS (Levonorgestrel) Skyla', 'Paragard', 'ring or depo', 'will RTC for Mirena', 'IUC/Liletta', 'IUC/Other', 'IUC/Paragard') then '2'
	when T1.primary_contraception in ('condoms and withdrawal', 'condoms, female', 'condoms, male', 'Male Condom', 'condoms, male, Plan B', 'condoms, male/same sex partner', 'condoms/withdrawal', 'male condoms/spermicide', 'spermicide', 'sponge', 'vaginal film', 'cervical cap', 'Female Condom', 'condoms/cycle tracking', 'condoms, male, EC', 'condoms/same sex partners', 'femcap', 'condoms, male/withdrawal', 'spermicide; condoms, male', 'Sponge', 'condoms/female', 'condoms/male', 'condoms') then '3'
	when T1.primary_contraception in ('none, desires pregnancy', 'none, other reason', 'None-pregnant', 'seeking pregnancy', 'None - Other Reason', 'none, will RTC for BCM', 'Pregnant', 'none', 'Seeking Pregnancy', 'none other reason', 'planning a pregnancy', 'menopausal', 'relying on partner - vascetomy', 'pregnant', 'currently pregnant', '.none, other reason', 'desires pregnancy', 'none, unplanned pregnancy', 'None', 'none - other reason', 'Menopause', 'None', 'none, desires pregnancy', 'none/desires pregnancy', 'none/other reasons', 'None-same sex partner') then '4'
	when T1.primary_contraception in ('abstinence', 'Not Sexually Active / Abstinent', 'relying on partner', 'withdrawal', 'withdrawl', 'cycle tracking', 'has never been sexually active', 'not sexually active', 'FAM/NFP', ' Patient reports partner withdrewwithdrawal', 'ithdrawal', 'Natural Family Planning', 'Withdrawal' ) then '5'
	when T1.primary_contraception in ('Partner Sterilized', 'same sex partner', 'tubal ligation', 'vasectomy', 'essure', 'hysterectomy', 'hysteroscopic tubal occlusion', 'Essure', 'Other', 'ESSURE', 'Female Sterilization') then '8'
	when T1.primary_contraception in ('unplanned pregnancy', 'Unplanned Pregnancy', ' ', 'declined', 'uknown', 'unknown', 'Unsure', 'unsure', 'abortion', 'declined, thinking options', 'undecided', 'Unknown') then null
	when T1.primary_contraception is null then null
	else concat('99999:primary_contraception: ', primary_contraception)
	end f1_contraception,
case when  T1.sti_symptoms_yn in ('Yes') then '1'
	when  T1.sti_symptoms_yn in ('No') then '2'
	when  T1.sti_symptoms_yn in ('Unknown') then null 
	when  T1.sti_symptoms_yn is null then null
	else concat('99999:sti_symptoms_yn: ', sti_symptoms_yn)
	end f1_sympt,
case when  T1.sti_exposure_yn  in ('Y') then '1'
	when  T1.sti_exposure_yn  in ('N') then '2'
	when  T1.sti_exposure_yn  in ('U') then null 
	when  T1.sti_exposure_yn  is null then null 
	else concat('99999:sti_exposure_yn: ', sti_exposure_yn)
	end  f1_contact_std,
--added
case when T1.pelvic_exam_yn in ('Y') then '1'
	when T1.pelvic_exam_yn in ('N') then '2'
	when T1.pelvic_exam_yn is null then null
	else concat('99999:pelvic_exam_yn: ', pelvic_exam_yn)
	end f1_pelvic_exam, 
null f1_mensex, 
null f1_femsex,
case when sex_mwb in ('Cis Men', 'male') then '1'
	 when sex_mwb in ('Cis Women', 'female') then '2'
	 when sex_mwb in ('choose not to disclose') or sex_mwb is null then '4'
	 when sex_mwb in ('transmale', 'TransMen', 'Other', 'TransWomen', 'transfemale', 'gender queer', 'Genderqueer/ Non-Binary') then null
	 else concat('99999:sex_mwb: ', sex_mwb)
	 end f1_sexor3,
-- number sex partners is over greater time period than 3 months
T1.total_partners as f1_numsex3,
case when sexuality in ('Homosexual', 'Lesbian or gay or homosexual') then '1'
	 when sexuality in ('Heterosexual', 'Straight or heterosexual') then '2'
	 when sexuality in ('Bisexual') then '3'
	 when sexuality in ('Something else') then '4'
	 when sexuality in ('Don''t know', 'Choose not to disclose') or sexuality is null then null
	 else concat('99999:sexuality: ', sexuality)
	 end f1_sexuality,
--added
case when T1.new_partners_yn  in ('Y') then '1'
	when T1.new_partners_yn  in ('N') then '2'
	when T1.new_partners_yn  is null then null
	else concat('99999:new_partners_yn: ', new_partners_yn)
	end f1_newsex,
case when anal_sex_yn in ('1') then '1'
     when anal_sex_yn is null then null
	 else concat('99999:anal_sex: ', anal_sex_yn)
	 end f1_rectal_exposure,
case when oral_sex_yn in ('1') then '1'
     when oral_sex_yn is null then null
	 else concat('99999:oral_sex: ', oral_sex_yn)
	 end f1_pharynx_exposure,
case when ept_accepted_yn in ('Y') then '1'
	 when ept_accepted_yn is null then null
	 else concat('99999:ept_accepted_yn: ', ept_accepted_yn)
	 end f1_partner_tx,
null f1_gisp_travel,
null f1_gisp_sex_work,
null f1_gisp_antibiotic,
null f1_gisp_idu,
null f1_gisp_non_idu,
null f1_gisp_gc_12,
null f1_gisp_gc_ever,
-- added
case when T1.hiv_test_ever_yn  in ('Y') then '1'
	when T1.hiv_test_ever_yn  in ('N') then '2'
	when T1.hiv_test_ever_yn  is null then '9'
	else concat('99999:hiv_test_ever_yn: ', hiv_test_ever_yn)
	end f1_hivtest,
		
-- to_char(hiv_test_date, 'MM/YYYY') f1_hivtestdate,
-- 2015-01-10
case when T1.hiv_test_date::text ~ '^[0-9]{4}-[0-9]+-[0-9]{2}$' then concat( split_part(hiv_test_date, '-', 2), '/', split_part(hiv_test_date, '-', 1) )
	-- 20150110
	when T1.hiv_test_date ~ '^[0-9]{8}$' then concat(substr(hiv_test_date, 5, 2), '/', substr(hiv_test_date, 1, 4))
	when T1.hiv_test_date ~ '^[0-9]{2}/[0-9]{4}' then substr(hiv_test_date, 1, 7)
	when T1.hiv_test_date ~ '^[0-9]{2}/[0-9]{2}/[0-9]{4}' then concat (substr(hiv_test_date, 1, 3), substring(hiv_test_date from 7 for 4) ) 
	when T1.hiv_test_date ~ '^[0-9]{2}/[0-9]{2}' then concat (substr(hiv_test_date, 1, 3), '20', substring(hiv_test_date from 7 for 2) )
	when T1.hiv_test_date ~ '^[0-9]{1}/[0-9]{2}/[0-9]{4}' then concat ('0',substring(hiv_test_date from 1 for 2), substring(hiv_test_date from 6 for 4) )
	when T1.hiv_test_date ~ '^[0-9]{1}/[0-9]{2}/[0-9]{2}' then concat ('0',substring(hiv_test_date from 1 for 2), '20',substring(hiv_test_date from 6 for 2) ) 
	when T1.hiv_test_date ~ '^[0-9]{1}/[0-9]{4}' then concat ('0',substring(hiv_test_date from 1 for 2), substring(hiv_test_date from 3 for 4) ) 
	when T1.hiv_test_date ~ '^[0-9]{1}/[0-9]{2}' then concat ('0',substring(hiv_test_date from 1 for 2), '20', substring(hiv_test_date from 3 for 2) ) 
	when T1.hiv_test_date ~ '^[0-9]{2}-[0-9]{2}-[0-9]{4}' then concat (substring(hiv_test_date from 1 for 2), '/', substring(hiv_test_date from 7 for 4) )
	when T1.hiv_test_date ~ '^[0-9]{2}/[0-9]{1}/[0-9]{4}' then concat (substring(hiv_test_date from 1 for 2), substring(hiv_test_date from 6 for 4) )
	when T1.hiv_test_date ~ '^[0-9]{1}/[0-9]{1}/[0-9]{4}' then concat ('0', substring(hiv_test_date from 1 for 2), substring(hiv_test_date from 5 for 4) ) 
	when T1.hiv_test_date ~ '^[0-9]{1}/[0-9]{1}/[0-9]{2}' then concat ('0', substring(hiv_test_date from 1 for 2), '20', substring(hiv_test_date from 5 for 2) )
	when T1.hiv_test_date ~ '[0-9]{2}/[0-9]{4}' then substring(hiv_test_date from '[0-9]{2}/[0-9]{4}') 
	when T1.hiv_test_date ~ '[0-9]{1}/[0-9]{4}' then concat('0', substring(hiv_test_date from '[0-9]{1}/[0-9]{4}')) 
	when T1.hiv_test_date ~ '[0-9]{2}/[0-9]{2}/[0-9]{2}' then concat( substr((substring(hiv_test_date from '[0-9]{2}/[0-9]{2}/[0-9]{2}')), 1,2), '/20',substr((substring(hiv_test_date from '[0-9]{2}/[0-9]{2}/[0-9]{2}')), 7,2)  )
	when T1.hiv_test_date ~ '[0-9]{1}/[0-9]{1}/[0-9]{2}' then concat( '0', substr((substring(hiv_test_date from '[0-9]{1}/[0-9]{1}/[0-9]{2}')), 1,2), '20', substr((substring(hiv_test_date from '[0-9]{1}/[0-9]{1}/[0-9]{2}')), 5,2))
	when T1.hiv_test_date ~ '[0-9]{1}\.[0-9]{2}\.[0-9]{2}' then concat( '0', substr((substring(hiv_test_date from '[0-9]{1}\.[0-9]{2}\.[0-9]{2}')), 1,1), '/20', substr((substring(hiv_test_date from '[0-9]{1}\.[0-9]{2}\.[0-9]{2}')), 6,2)) 
	 when T1.hiv_test_date ~ '[0-9]{2}-[0-9]{2}-[0-9]{2}' then concat( substr((substring(hiv_test_date from '[0-9]{2}-[0-9]{2}-[0-9]{2}')), 1,2), '/20', substr((substring(hiv_test_date from '[0-9]{2}-[0-9]{2}-[0-9]{2}')), 7,2)) 
	when T1.hiv_test_date ilike '%september 2016%' then '09/2016' 
	when T1.hiv_test_date ilike '%march 2016%' then '03/2016'
	when T1.hiv_test_date ilike '%Mar-17%' then '03/2017'
	when T1.hiv_test_date ilike '%Jul-17%' then '07/2017'
	when T1.hiv_test_date ilike '%June 2017%' then '06/2017'
	when T1.hiv_test_date ilike '%sept 2017%' then '09/2017'
	when T1.hiv_test_date = 'sep,18,17' then '09/2017'
	when T1.hiv_test_date ~ '^[0-9]+$' then null
	when T1.hiv_test_date ilike '%approx%' or T1.hiv_test_date ilike '%neg%' or T1.hiv_test_date ilike '%first%' or T1.hiv_test_date ilike '%ago%' or T1.hiv_test_date ilike '%106%' then null
	when T1.hiv_test_date in ('27-Dec', '17-Mar', 'Jan-18') then null
	when T1.hiv_test_date is null then null
	else concat('99999:hiv_test_date: ', hiv_test_date)
	end f1_hivtestdate,
case when T1.hiv_result in ('negative', '+negative', ' negative') or T1.hiv_result ilike '%neg%' then '0'
	when T1.hiv_result in ('positive') then '1'
	when T1.hiv_result in ('indeterminate') then '2'
	when T1.hiv_result in ('unknown') then '3'
	when T1.hiv_result ~ '(\d+)' or T1.hiv_result ilike '%first%' then null
	when T1.hiv_result is null then '9'
	else concat('99999:hiv_result: ', hiv_result)
	end f1_hivresultlast,	
	-- added the following	
case when T1.hiv_test_refuse_yn  in ('Y', 'declined') then '1'
	when T1.hiv_test_refuse_yn  in ('N', 'accepted') then '2'
	when T1.hiv_test_refuse_yn in ('n/a') then null
	when T1.hiv_test_refuse_yn is null then null
	else concat('99999:hiv_test_refuse_yn: ', hiv_test_refuse_yn)
	end f1_hivtest_refuse,	
case when T1.hpv_vac_yn  in ('Y') then '1'
	when T1.hpv_vac_yn  in ('N') then '2'
	when T1.hpv_vac_yn is null then null
	else concat('99999:hpv_vac_yn: ', hpv_vac_yn)
	end f1_hpvvaxadmin,	
case when T1.ab_pain_rptd  in ('Y') then '1'
	when T1.ab_pain_rptd  in ('N') then '2'
	when T1.ab_pain_rptd is null then null
	else concat('99999:ab_pain_rptd: ', ab_pain_rptd)
	end f1_sxabdomen,	
case when T1.dysuria_rptd  in ('Y') then '1'
	when T1.dysuria_rptd  in ('N') then '2'
	when T1.dysuria_rptd is null then null
	else concat('99999:dysuria_rptd: ', dysuria_rptd)
	end f1_sxdysuria,	
case when T1.discharge_rptd  in ('Y') then '1'
	when T1.discharge_rptd  in ('N') then '2'
	when T1.discharge_rptd = '' or T1.discharge_rptd is null then null
	else concat('99999:discharge_rptd: ', discharge_rptd)
	end f1_sxdischarge,	
case when T1.gen_lesion_rptd  in ('Y') then '1'
	when T1.gen_lesion_rptd  in ('N') then '2'
	when T1.gen_lesion_rptd = '' or T1.gen_lesion_rptd is null then null
	else concat('99999:gen_lesion_rptd: ', gen_lesion_rptd)
	end f1_sxlesion,	
case when T1.vag_discharge_on_exam  in ('Y') then '1'
	when T1.vag_discharge_on_exam  in ('N') then '2'
	when T1.vag_discharge_on_exam = '' or T1.vag_discharge_on_exam is null then null
	else concat('99999:vag_discharge_on_exam: ', vag_discharge_on_exam)
	end f1_pedischarge,	
case when T1.lower_ab_pain_on_exam  in ('Y') then '1'
	when T1.lower_ab_pain_on_exam  in ('N') then '2'
	when T1.lower_ab_pain_on_exam is null then null
	else concat('99999:lower_ab_pain_on_exam: ', lower_ab_pain_on_exam)
	end f1_peabdomen,	
case when T1.cerv_tend_on_exam  in ('Y') then '1'
	when T1.cerv_tend_on_exam  in ('N') then '2'
	when T1.cerv_tend_on_exam is null then null
	else concat('99999:cerv_tend_on_exam: ', cerv_tend_on_exam)
	end f1_cmt,
case when T1.adnexal_tend_on_exam  in ('Y') then '1'
	when T1.adnexal_tend_on_exam  in ('N') then '2'
	when T1.adnexal_tend_on_exam is null then null
	else concat('99999:adnexal_tend_on_exam: ', adnexal_tend_on_exam)
	end f1_adnexal
FROM public.emr_stiencounterextended T1 
INNER JOIN public.pplm_sti_clinic_a_100037_s_3 T2 ON ((T1.natural_key = T2.natural_key)) 
WHERE  T2.date >= :start_date and T2.date < :end_date;

-- Step 5: Join - Join to patient table to populate and transform additional fields
CREATE TABLE public.pplm_sti_clinic_a_100037_s_5  AS SELECT 
T2.f1_facilityid,
T2.f1_siteid,
T2.f1_patientid,
T2.f1_visdate,
T2.f1_eventid,
T2.f1_gisp_yrmo,
T2.f1_gisp_number,
case when  T1.gender in ('M') then '1' 
	when  T1.gender in ('F') then '2' 
	when  T1.gender in ('T') then '5' 
	when  T1.gender in ('U' ,'UNKNOWN' ) then null 
	when  T1.gender in (' ') then null
	when  T1.gender in ('') then null
	when  T1.gender is null then null 
	else concat('99999:gender: ', gender) 
	end f1_gender,
date_part('year',age(date_of_birth)) f1_age,
case when T1.ethnicity in ('Hispanic or Latino', 'HISPANIC OR LATINO', 'Central American Indian', 'Venezuelan') then '1' 
	when  T1.race in ('Hispanic Or Latino (All Races)', 'Hispanic', 'Spanish American Indian', 'Venezuelan') then '1' 
	when  T1.ethnicity in ('Not Hispanic or Latino', 'Unknown', 'Unknown / Not Reported', 'NOT HISPANIC OR LATINO', 'NOT REPORTED') then '2' 
	when  T1.ethnicity in ('Declined to specify') then '9'
	when  T1.ethnicity is null then '9' 
	else concat('99999:ethnicity: ', ethnicity) 
	end f1_hisp,
case when  T1.race in ('American Indian or Alaska Native', 'AMERICAN INDIAN/ALASKAN NATIVE', 'Native American', 'Southeastern Indians', 'Native American Indian', 'Modoc') then '1' 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'African American (Black), Other', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian', 'ASIAN', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American', 'BLACK', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'Spanish American Indian', 'More than one race', 'Multiracial', 'Multi-racial', 'Native Hawaiian or Other Pacific Islander', 'Native Hawaiian or Other Pacific Islander, Other', 'Other', 'OTHER', 'Other Race', 'Other, White', 'Unknown, White', 'White', 'WHITE', 'White (Not Hispanic / Latino)', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander', 'PACIFIC ISLANDER/HAWAIIAN', 'White, White (Not Hispanic / Latino)', 'Other, White (Not Hispanic / Latino)', 'African American (Black), White', 'Native American, Other', 'African American (Black), White', 'American Indian or Alaska Native, Black or African American, Other', 'African American (Black), Black or African American, Unknown/Not Reported', 'Native Hawaiian or Other Pacific Islander, Other, White', 'American Indian or Alaska Native, Asian', 'American Indian or Alaska Native, Other', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander', 'More than one race, White', 'Asian, Native Hawaiian or Other Pacific Islander, White', 'Multi-racial, Other', 'Hawaiian', 'African American (Black), Native Hawaiian or Other Pacific Islander', 'Asian, Black or African American, White', 'Multiracial, Other', 'Asian, Unknown/Not Reported', 'Armenian', 'Barbadian', 'Maldivian', 'Polish', 'Laotian') then '2' 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported', 'Patient Declined') then '9' 
	when T1.race is null then '9' 
	else concat('99999:race: ', race)
	end f1_aian,
case when  T1.race in ('Asian', 'ASIAN', 'Asian, Unknown/Not Reported', 'Laotian') then '1' 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'African American (Black), Other', 'American Indian or Alaska Native', 'AMERICAN INDIAN/ALASKAN NATIVE', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American', 'BLACK', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'Spanish American Indian', 'More than one race', 'Multiracial', 'Multi-racial', 'Native American', 'Southeastern Indians', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander', 'Native Hawaiian or Other Pacific Islander, Other', 'Other', 'OTHER', 'Other Race', 'Other, White', 'Unknown, White', 'White', 'WHITE', 'White (Not Hispanic / Latino)', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander', 'PACIFIC ISLANDER/HAWAIIAN', 'White, White (Not Hispanic / Latino)', 'Other, White (Not Hispanic / Latino)', 'African American (Black), White', 'Native American, Other', 'African American (Black), White', 'American Indian or Alaska Native, Black or African American, Other', 'African American (Black), Black or African American, Unknown/Not Reported', 'Native Hawaiian or Other Pacific Islander, Other, White', 'American Indian or Alaska Native, Asian', 'American Indian or Alaska Native, Other', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander', 'More than one race, White', 'Asian, Native Hawaiian or Other Pacific Islander, White', 'Multi-racial, Other', 'Hawaiian', 'African American (Black), Native Hawaiian or Other Pacific Islander', 'Asian, Black or African American, White', 'Multiracial, Other', 'Armenian', 'Barbadian', 'Maldivian', 'Polish', 'Modoc' ) then '2' 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported', 'Patient Declined') then '9'  
	when  T1.race is null then '9' 
	else concat('99999:race: ', race)
	end f1_asian,
case when  T1.race in ('Native Hawaiian or Other Pacific Islander', 'Pacific Islander', 'PACIFIC ISLANDER/HAWAIIAN', 'Hawaiian') then '1' 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'African American (Black), Other', 'American Indian or Alaska Native', 'AMERICAN INDIAN/ALASKAN NATIVE', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian', 'ASIAN', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American', 'BLACK','Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'Spanish American Indian', 'More than one race', 'Multiracial', 'Multi-racial', 'Native American', 'Southeastern Indians', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander, Other', 'Other', 'OTHER', 'Other Race', 'Other, White', 'Unknown, White', 'White', 'WHITE', 'White (Not Hispanic / Latino)', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'White, White (Not Hispanic / Latino)' , 'Other, White (Not Hispanic / Latino)', 'African American (Black), White', 'Native American, Other', 'African American (Black), White', 'American Indian or Alaska Native, Black or African American, Other', 'African American (Black), Black or African American, Unknown/Not Reported', 'Native Hawaiian or Other Pacific Islander, Other, White', 'American Indian or Alaska Native, Asian', 'American Indian or Alaska Native, Other', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander', 'More than one race, White', 'Asian, Native Hawaiian or Other Pacific Islander, White', 'Multi-racial, Other', 'African American (Black), Native Hawaiian or Other Pacific Islander', 'Asian, Black or African American, White', 'Multiracial, Other', 'Asian, Unknown/Not Reported', 'Armenian', 'Barbadian', 'Maldivian', 'Polish', 'Modoc', 'Laotian') then '2' 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported', 'Patient Declined') then '9' 
	when  T1.race is null then '9' 
	else concat('99999:race: ', race)
	end f1_pih,
case when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'Black or African American', 'BLACK','Black/African American (Not Hispanic)', 'African American (Black), Black or African American, Unknown/Not Reported') then '1' 
	when  T1.race in ('African American (Black), Other', 'American Indian or Alaska Native', 'AMERICAN INDIAN/ALASKAN NATIVE', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian', 'ASIAN', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Hispanic', 'Hispanic Or Latino (All Races)', 'Spanish American Indian', 'More than one race', 'Multiracial', 'Multi-racial', 'Native American', 'Southeastern Indians', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander','Native Hawaiian or Other Pacific Islander, Other', 'Other', 'OTHER', 'Other Race', 'Other, White', 'Unknown, White', 'White', 'WHITE', 'White (Not Hispanic / Latino)', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander', 'PACIFIC ISLANDER/HAWAIIAN', 'White, White (Not Hispanic / Latino)', 'Other, White (Not Hispanic / Latino)', 'African American (Black), White', 'Native American, Other', 'African American (Black), White', 'American Indian or Alaska Native, Black or African American, Other', 'Native Hawaiian or Other Pacific Islander, Other, White', 'American Indian or Alaska Native, Asian', 'American Indian or Alaska Native, Other', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander', 'More than one race, White', 'Asian, Native Hawaiian or Other Pacific Islander, White', 'Multi-racial, Other', 'Hawaiian', 'African American (Black), Native Hawaiian or Other Pacific Islander', 'Asian, Black or African American, White', 'Multiracial, Other', 'Asian, Unknown/Not Reported', 'Armenian', 'Barbadian', 'Maldivian', 'Polish', 'Modoc', 'Laotian') then '2' 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported', 'Patient Declined') then '9' 
	when  T1.race is null then '9' 
	else concat('99999:race: ', race)
	end f1_black,
case when  T1.race in ('White', 'WHITE', 'White (Not Hispanic / Latino)', 'White, White (Not Hispanic / Latino)', 'Other, White (Not Hispanic / Latino)') then '1' 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'African American (Black), Other', 'American Indian or Alaska Native', 'AMERICAN INDIAN/ALASKAN NATIVE', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian', 'ASIAN', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American', 'BLACK', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'Spanish American Indian', 'More than one race', 'Multiracial', 'Multi-racial', 'Native American', 'Southeastern Indians', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander', 'Native Hawaiian or Other Pacific Islander, Other', 'Other', 'OTHER', 'Other Race', 'Other, White', 'Unknown, White', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander', 'PACIFIC ISLANDER/HAWAIIAN', 'African American (Black), White', 'Native American, Other', 'African American (Black), White', 'American Indian or Alaska Native, Black or African American, Other', 'African American (Black), Black or African American, Unknown/Not Reported', 'Native Hawaiian or Other Pacific Islander, Other, White', 'American Indian or Alaska Native, Asian', 'American Indian or Alaska Native, Other', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander', 'More than one race, White', 'Asian, Native Hawaiian or Other Pacific Islander, White', 'Multi-racial, Other', 'Hawaiian', 'African American (Black), Native Hawaiian or Other Pacific Islander', 'Asian, Black or African American, White', 'Multiracial, Other', 'Asian, Unknown/Not Reported', 'Armenian', 'Barbadian', 'Maldivian', 'Polish', 'Modoc', 'Laotian' ) then '2' 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported', 'Patient Declined' ) then '9' 
	when  T1.race is null then '9' 
	else concat('99999:race: ', race)
	end f1_white,
case when  T1.race in ('African American (Black), Other', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'More than one race', 'Multiracial', 'Multi-racial', 'Native Hawaiian or Other Pacific Islander, Other', 'Other, White', 'Unknown, White', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander',
'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander', 'PACIFIC ISLANDER/HAWAIIAN', 'African American (Black), White', 'Native American, Other', 'African American (Black), White', 'American Indian or Alaska Native, Black or African American, Other', 'Native Hawaiian or Other Pacific Islander, Other, White', 'American Indian or Alaska Native, Asian', 'American Indian or Alaska Native, Other', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander', 'More than one race, White', 'Asian, Native Hawaiian or Other Pacific Islander, White', 'Multi-racial, Other', 'African American (Black), Native Hawaiian or Other Pacific Islander', 'Asian, Black or African American, White', 'Multiracial, Other') then '1' 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'American Indian or Alaska Native', 'AMERICAN INDIAN/ALASKAN NATIVE', 'Asian', 'ASIAN', 'Black or African American', 'BLACK', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'Spanish American Indian', 'Native American', 'Southeastern Indians', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander', 'Other', 'OTHER', 'Other Race', 'White', 'WHITE', 'White (Not Hispanic / Latino)', 'White, White (Not Hispanic / Latino)', 'Other, White (Not Hispanic / Latino)', 'African American (Black), Black or African American, Unknown/Not Reported', 'Hawaiian', 'Asian, Unknown/Not Reported', 'Armenian', 'Barbadian', 'Maldivian', 'Polish', 'Modoc', 'Laotian') then '2' 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported', 'Patient Declined') then '9' 
	when  T1.race is null then '9' 
	else concat('99999:race: ', race) 
	end f1_multirace,
case when  T1.race in ('Other', 'OTHER', 'Other Race', 'Armenian', 'Barbadian', 'Maldivian', 'Polish') then '1' 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'African American (Black), Other', 'American Indian or Alaska Native', 'AMERICAN INDIAN/ALASKAN NATIVE', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian', 'ASIAN', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American', 'BLACK', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'Spanish American Indian', 'More than one race', 'Multiracial', 'Multi-racial', 'Native Hawaiian or Other Pacific Islander', 'Native American', 'Southeastern Indians', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander, Other', 'Other, White', 'Unknown, White', 'White', 'WHITE', 'White (Not Hispanic / Latino)', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander', 'PACIFIC ISLANDER/HAWAIIAN', 'White, White (Not Hispanic / Latino)', 'Other, White (Not Hispanic / Latino)', 'African American (Black), White', 'Native American, Other', 'African American (Black), White', 'American Indian or Alaska Native, Black or African American, Other', 'African American (Black), Black or African American, Unknown/Not Reported', 'Native Hawaiian or Other Pacific Islander, Other, White', 'American Indian or Alaska Native, Asian', 'American Indian or Alaska Native, Other', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander', 'More than one race, White', 'Asian, Native Hawaiian or Other Pacific Islander, White', 'Multi-racial, Other', 'Hawaiian', 'African American (Black), Native Hawaiian or Other Pacific Islander', 'Asian, Black or African American, White', 'Multiracial, Other', 'Asian, Unknown/Not Reported', 'Modoc', 'Laotian') then '2' 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported', 'Patient Declined') then '9' 
	when  T1.race is null then '9' 
	else concat('99999:race: ', race)
	end f1_otherrace,
T2.f1_insurance,
T2.f1_visit_type,
T2.f1_reason_visit,
T2.f1_pregnant,
T2.f1_contraception,
T2.f1_sympt,
T2.f1_contact_std,
T2.f1_pelvic_exam,
T2.f1_mensex,
T2.f1_femsex,
T2.f1_sexor3,
T2.f1_numsex3,
T2.f1_sexuality,
T2.f1_newsex,
T2.f1_rectal_exposure,
T2.f1_pharynx_exposure,
T2.f1_partner_tx,
T2.f1_gisp_travel,
T2.f1_gisp_sex_work,
T2.f1_gisp_antibiotic,
T2.f1_gisp_idu,
T2.f1_gisp_non_idu,
T2.f1_gisp_gc_12,
T2.f1_gisp_gc_ever,
T2.f1_hivtest,
T2.f1_hivtestdate,
T2.f1_hivresultlast,
T2.f1_hivtest_refuse,
T2.f1_hpvvaxadmin,
T2.f1_sxabdomen,
T2.f1_sxdysuria,
T2.f1_sxdischarge,
T2.f1_sxlesion,
T2.f1_pedischarge,
T2.f1_peabdomen,
T2.f1_cmt,
T2.f1_adnexal 
FROM public.emr_patient T1 
INNER JOIN public.pplm_sti_clinic_a_100037_s_4 T2 ON ((T1.id = T2.f1_patientid)) ;

\copy pplm_sti_clinic_a_100037_s_5 to '/tmp/SSUN_clinic.csv' csv header

--
-- Script shutdown section 
--


	
DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_3 CASCADE;

DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_4 CASCADE;

DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_5 CASCADE;


