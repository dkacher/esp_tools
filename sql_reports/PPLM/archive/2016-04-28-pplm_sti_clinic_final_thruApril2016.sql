﻿-- Analysis script generated on Tuesday, 01 Mar 2016 16:37:05 EST
-- Analysis name: pplm - STI Report - Clinic Variables
-- Analysis description: pplm - STI Report - Clinic Variables
-- Script generated for database: POSTGRESQL

--
-- Script setup section 
--

DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_1 CASCADE;
DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_1 CASCADE;

DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_2 CASCADE;
DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_2 CASCADE;

DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_3 CASCADE;
DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_3 CASCADE;

DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_4 CASCADE;
DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_4 CASCADE;

DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_5 CASCADE;
DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_5 CASCADE;

--
-- Script body 
--

-- Step 1: Access - public.emr_stiencounterextended
CREATE VIEW public.pplm_sti_clinic_a_100037_s_1 AS SELECT * FROM public.emr_stiencounterextended;

-- Step 2: Access - public.emr_patient
CREATE VIEW public.pplm_sti_clinic_a_100037_s_2 AS SELECT * FROM public.emr_patient;

-- Step 3: Access - public.emr_encounter
CREATE VIEW public.pplm_sti_clinic_a_100037_s_3 AS SELECT * FROM public.emr_encounter;

-- Step 4: Join - Join extended table against encounter to get the encounter id for the report. Create/transform all fields to values
CREATE TABLE public.pplm_sti_clinic_a_100037_s_4  AS SELECT 
case when T2.site_natural_key = '05F19D3B-E18F-4971-AB29-32A5904E67FE' then 2 --Pplm Marlborough 
	 when T2.site_natural_key = '22DF4A2A-45F5-4D1B-A279-134E3F6168F1' then 3 --Pplm Worcester
	 when T2.site_natural_key = '588A5941-A016-462D-ADB1-8563ABD0F013' then 4 --Pplm Milford     
	 when T2.site_natural_key = '76E86AFC-EB87-44C7-A400-BC8FF6E15654' then 5 --Pplm Springfield 
	 when T2.site_natural_key = '9D971E61-2B5A-4504-9016-7FD863790EE2' then 6 --Pplm Boston 
	 when T2.site_natural_key = 'DE857862-F1A8-4985-997E-7C03D0CC8081' then 7 --Pplm Fitchburg   
	 when T2.site_natural_key = 'F32D324A-6AAB-4718-8310-132B5A223482' then 8 --Pplm Som Express 
	 else 99999 
	 end f1_facilityid,
'MA' f1_siteid,
T2.patient_id f1_patientid,
T2.date f1_visdate,
T2.id f1_eventid,
null f1_gisp_yrmo,
null f1_gisp_number,
case when T1.insurance_status in ('Medicaid', 'AB Funding', 'Medicare') then 1
	when T1.insurance_status in ('United Healthcare', 'Harvard Pilgrim Health Care', 'Health New England', 'Fallon', 'BMC Healthnet Plan', 'Tufts Health Public Plans', 'Neighborhood Health Plan', 'Cigna', 'Celticare', 'Aetna', 'Tufts', 'Blue Cross Blue Shield', 'Student Plans' ) then 2 
	when T1.insurance_status in ('Non-Participating', 'Self-Pay') then 5 
	when T1.insurance_status is null then null
	else 99999
	end f1_insurance, 
case when T2.raw_encounter_type in ('GYN VISIT', 'OFFICE VISIT') then 1
	when T2.raw_encounter_type in ('CHART UPDATE', 'PATIENT COMMUNICATION') then 8
	when T2.raw_encounter_type is null then null
	else 99999
	end f1_visit_type,
case when T1.primary_purpose in ('Abdominal Pain', 'Abnormal Vaginal Bleeding', 'Absent Menses', 'Breast Mass/Lump', 'Breast Pain', 'Foreign Body Removal (F)', 'Genital Lesion (F)', 'Genital Lesion (M)', 'Genital Pain (F)', 'Genital Pain (M)', 'Irregular Menses', 'Nipple Discharge', 'Pelvic Pain', 'Sexual Problems (F)', 'STI Symptoms (F)', 'Urinary Symptoms (F)', 'Vaginal Infection Symptoms') then 1
	when  T1.primary_purpose in ('Molluscum Treatment', 'STI Treatment', 'STI Treatment (M)', 'Wart Treatment (F)') then 2
	when T1.primary_purpose in ('Follow-up/repeat Pap', 'Abortion Post', 'Abortion Post (Problem)', 'Medication Abortion Post') then 3
	when  T1.primary_purpose in ('Abortion', 'DMPA Injection Visit', 'EC', 'EPL', 'Essure Pre-Screen', 'Family Planning Counseling', 'Hormonal Contraception', 'HPV Repeat Injection', 'Implant check', 'Implant Insertion', 'Implant Removal', 'IUC Check', 'IUC Insertion', 'IUC Removal', 'Medication Abortion', 'Pregnancy Test', 'Prescription Barrier Method') then 4
	when  T1.primary_purpose in ('STI Testing', 'STI Testing (M)') then 5
	when  T1.primary_purpose in ('Counseling/Education', 'Counseling/Education (F)', 'Immunization', 'Inconclusive Ultrasound', 'Other GYN Visit', 'LEEP', 'Well Woman Visit', 'Colposcopy', 'Lab Only') then 8
	when  T1.primary_purpose is null then null 
	else 99999 
	end f1_reason_visit,
9 f1_pregnant,
case when T1.primary_contraception in ('Injection', 'injections (DMPA)', 'Oral (CHC)', 'Oral (POP)', 'oral contraceptives', 'Patch', 'patch', 'DMPA', 'EC', 'EC and condoms', 'EC and implant at f/u', 'injections (DMPA), EC', 'IUS (Levonorgestrel)',
'Oral (POP)', 'oral contraceptives, Ella', 'oral contraceptives, IUC Miren', 'POP''s',
'Progestin-Only Pills (POPs)', 'POPs', 'patch, EC', 'emergency contracption', 'oral contraceptives/condoms', 'Oral Contraceptives') then 1
	when T1.primary_contraception in ('Implant', 'implant', 'IUC', 'IUC, Liletta', 'IUC, Mirena', 'IUC, other', 'IUC, Paragard', 'ring', 'Ring', 'diaphragm', 'IUC (Copper)', 'IUC, skyla', 'IUD', 'IUD, Skyla', 'IUC, Paragard/condoms', 'Mirena') then 2
	when T1.primary_contraception in ('condoms and withdrawal', 'condoms, female', 'condoms, male', 'Male Condom', 'condoms, male, Plan B', 'condoms, male/same sex partner', 'condoms/withdrawal', 'male condoms/spermicide', 'spermicide', 'sponge', 'vaginal film', 'cervical cap', 'Female Condom', 'condoms/cycle tracking', 'condoms, male, EC', 'condoms/same sex partners') then 3
	when T1.primary_contraception in ('none, desires pregnancy', 'none, other reason', 'None-pregnant', 'seeking pregnancy', 'None - Other Reason', 'none, will RTC for BCM', 'Pregnant', 'none', 'Seeking Pregnancy') then 4
	when T1.primary_contraception in ('abstinence', 'Not Sexually Active / Abstinent', 'relying on partner', 'withdrawal', 'cycle tracking', 'has never been sexually active', 'not sexually active', 'FAM/NFP') then 5
	when T1.primary_contraception in ('Partner Sterilized', 'same sex partner', 'tubal ligation', 'vasectomy', 'essure', 'hysterectomy', 'hysteroscopic tubal occlusion', 'Essure') then 8
	when T1.primary_contraception in ('unplanned pregnancy', 'Unplanned Pregnancy', ' ', 'declined', 'uknown', 'unknown', 'Unsure', 'unsure') then null
	when T1.primary_contraception is null then null
	else 99999
	end f1_contraception,
case when  T1.sti_symptoms_yn in ('Yes') then 1
	when  T1.sti_symptoms_yn in ('No') then 2
	when  T1.sti_symptoms_yn in ('Unknown') then null 
	when  T1.sti_symptoms_yn is null then null
	else 99999 
	end f1_sympt,
case when  T1.sti_exposure_yn  in ('Y') then 1
	when  T1.sti_exposure_yn  in ('N') then 2
	when  T1.sti_exposure_yn  in ('U') then null 
	when  T1.sti_exposure_yn  is null then null 
	else 99999 
	end  f1_contact_std,
--added
case when T1.pelvic_exam_yn in ('Y') then 1
	when T1.pelvic_exam_yn in ('N') then 2
	when T1.pelvic_exam_yn is null then null
	else 99999
	end f1_pelvic_exam, 
null f1_mensex, 
null f1_femsex,
null f1_sexor3,
null f1_numsex3,
null f1_sexuality,
--added
case when T1.new_partners_yn  in ('Y') then 1
	when T1.new_partners_yn  in ('N') then 2
	when T1.new_partners_yn  is null then null
	else 99999
	end f1_newsex,
9 f1_rectal_exposure,
9 f1_pharynx_exposure,
9 f1_partner_tx,
null f1_gisp_travel,
null f1_gisp_sex_work,
null f1_gisp_antibiotic,
null f1_gisp_idu,
null f1_gisp_non_idu,
null f1_gisp_gc_12,
null f1_gisp_gc_ever,
-- added
case when T1.hiv_test_ever_yn  in ('Y') then 1
	when T1.hiv_test_ever_yn  in ('N') then 2
	when T1.hiv_test_ever_yn  is null then 9
	else 99999 
	end f1_hivtest,
		
-- added
to_char(hiv_test_date, 'MM/YYYY') f1_hivtestdate,

case when T1.hiv_result in ('negative', '+negative', ' negative') then 0
	when T1.hiv_result in ('positive') then 1
	when T1.hiv_result in ('indeterminate') then 2
	when T1.hiv_result in ('unknown') then 3
	when T1.hiv_result is null then 9
	else 99999
	end f1_hivresultlast,	
-- added the following	
case when T1.hiv_test_refuse_yn  in ('Y') then 1
	when T1.hiv_test_refuse_yn  in ('N') then 2
	when T1.hiv_test_refuse_yn is null then null
	else 99999 
	end f1_hivtest_refuse,	
case when T1.hpv_vac_yn  in ('Y') then 1
	when T1.hpv_vac_yn  in ('N') then 2
	when T1.hpv_vac_yn is null then null
	else 99999 
	end f1_hpvvaxadmin,	
case when T1.ab_pain_rptd  in ('Y') then 1
	when T1.ab_pain_rptd  in ('N') then 2
	when T1.ab_pain_rptd is null then null
	else 99999 
	end f1_sxabdomen,	
case when T1.dysuria_rptd  in ('Y') then 1
	when T1.dysuria_rptd  in ('N') then 2
	when T1.dysuria_rptd is null then null
	else 99999 
	end f1_sxdysuria,	
case when T1.discharge_rptd  in ('Y') then 1
	when T1.discharge_rptd  in ('N') then 2
	when T1.discharge_rptd is null then null
	else 99999 
	end f1_sxdischarge,	
case when T1.gen_lesion_rptd  in ('Y') then 1
	when T1.gen_lesion_rptd  in ('N') then 2
	when T1.gen_lesion_rptd is null then null
	else 99999 
	end f1_sxlesion,	
case when T1.vag_discharge_on_exam  in ('Y') then 1
	when T1.vag_discharge_on_exam  in ('N') then 2
	when T1.vag_discharge_on_exam is null then null
	else 99999 
	end f1_pedischarge,	
case when T1.lower_ab_pain_on_exam  in ('Y') then 1
	when T1.lower_ab_pain_on_exam  in ('N') then 2
	when T1.lower_ab_pain_on_exam is null then null
	else 99999 
	end f1_peabdomen,	
case when T1.cerv_tend_on_exam  in ('Y') then 1
	when T1.cerv_tend_on_exam  in ('N') then 2
	when T1.cerv_tend_on_exam is null then null
	else 99999 
	end f1_cmt,
case when T1.adnexal_tend_on_exam  in ('Y') then 1
	when T1.adnexal_tend_on_exam  in ('N') then 2
	when T1.adnexal_tend_on_exam is null then null
	else 99999 
	end f1_adnexal
FROM public.pplm_sti_clinic_a_100037_s_1 T1 
INNER JOIN public.pplm_sti_clinic_a_100037_s_3 T2 ON ((T1.natural_key = T2.natural_key)) 
WHERE  T2.date >= '01-01-2016' and T2.date < '05-01-2016';

-- Step 5: Join - Join to patient table to populate and transform additional fields
CREATE TABLE public.pplm_sti_clinic_a_100037_s_5  AS SELECT 
T2.f1_facilityid,
T2.f1_siteid,
T2.f1_patientid,
T2.f1_visdate,
T2.f1_eventid,
T2.f1_gisp_yrmo,
T2.f1_gisp_number,
case when  T1.gender in ('M') then 1 
	when  T1.gender in ('F') then 2 
	when  T1.gender in ('T') then 5 
	when  T1.gender in ('U' ,'UNKNOWN' ) then null 
	when  T1.gender is null then null 
	else 99999 
	end f1_gender,
date_part('year',age(date_of_birth)) f1_age,
case when T1.ethnicity in ('Hispanic or Latino') then 1 
	when  T1.race in ('Hispanic Or Latino (All Races)', 'Hispanic') then 1 
	when  T1.ethnicity in ('Not Hispanic or Latino', 'Unknown', 'Unknown / Not Reported') then 2 
	when  T1.ethnicity in ('Declined to specify') then 9
	when  T1.ethnicity is null then 9 
	else 99999 end f1_hisp,
case when  T1.race in ('American Indian or Alaska Native', 'Native American', 'Native American Indian' ) then 1 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'African American (Black), Other', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'More than one race', 'Multiracial', 'Multi-racial', 'Native Hawaiian or Other Pacific Islander', 'Native Hawaiian or Other Pacific Islander, Other', 'Other', 'Other Race', 'Other, White', 'Unknown, White', 'White', 'White (Not Hispanic / Latino)', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander', 'White, White (Not Hispanic / Latino)') then 2 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported') then 9 
	when T1.race is null then 9 
	else 99999 
	end f1_aian,
case when  T1.race in ('Asian') then 1 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'African American (Black), Other', 'American Indian or Alaska Native', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'More than one race', 'Multiracial', 'Multi-racial', 'Native American', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander', 'Native Hawaiian or Other Pacific Islander, Other', 'Other', 'Other Race', 'Other, White', 'Unknown, White', 'White', 'White (Not Hispanic / Latino)', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander', 'White, White (Not Hispanic / Latino)' ) then 2 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported') then 9  
	when  T1.race is null then 9 
	else 99999 
	end f1_asian,
case when  T1.race in ('Native Hawaiian or Other Pacific Islander', 'Pacific Islander') then 1 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'African American (Black), Other', 'American Indian or Alaska Native', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'More than one race', 'Multiracial', 'Multi-racial', 'Native American', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander, Other', 'Other', 'Other Race', 'Other, White', 'Unknown, White', 'White', 'White (Not Hispanic / Latino)', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'White, White (Not Hispanic / Latino)' ) then 2 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported') then 9 
	when  T1.race is null then 9 
	else 99999 
	end f1_pih,
case when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'Black or African American', 'Black/African American (Not Hispanic)') then 1 
	when  T1.race in ('African American (Black), Other', 'American Indian or Alaska Native', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Hispanic', 'Hispanic Or Latino (All Races)', 'More than one race', 'Multiracial', 'Multi-racial', 'Native American', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander','Native Hawaiian or Other Pacific Islander, Other', 'Other', 'Other Race', 'Other, White', 'Unknown, White', 'White', 'White (Not Hispanic / Latino)', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander', 'White, White (Not Hispanic / Latino)') then 2 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported') then 9 
	when  T1.race is null then 9 
	else 99999 
	end f1_black,
case when  T1.race in ('White', 'White (Not Hispanic / Latino)', 'White, White (Not Hispanic / Latino)') then 1 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'African American (Black), Other', 'American Indian or Alaska Native', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'More than one race', 'Multiracial', 'Multi-racial', 'Native American', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander', 'Native Hawaiian or Other Pacific Islander, Other', 'Other', 'Other Race', 'Other, White', 'Unknown, White', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander' ) then 2 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported') then 9 
	when  T1.race is null then 9 
	else 99999 
	end f1_white,
case when  T1.race in ('African American (Black), Other', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'More than one race', 'Multiracial', 'Multi-racial', 'Native Hawaiian or Other Pacific Islander, Other', 'Other, White', 'Unknown, White', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander',
'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander') then 1 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'American Indian or Alaska Native', 'Asian', 'Black or African American', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'Native American', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander', 'Other', 'Other Race', 'White', 'White (Not Hispanic / Latino)', 'White, White (Not Hispanic / Latino)') then 2 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported') then 9 
	when  T1.race is null then 9 
	else 99999 
	end f1_multirace,
case when  T1.race in ('Other', 'Other Race') then 1 
	when  T1.race in ('African American (Black)', 'African American (Black), Black or African American', 'African American (Black), Other', 'American Indian or Alaska Native', 'American Indian or Alaska Native, Black or African American, Other, White', 'Asian', 'Asian, Black or African American', 'Asian, Other, White', 'Asian, White', 'Black or African American', 'Black or African American, Native Hawaiian or Other Pacific Islander', 'Black or African American, Other', 'Black or African American, White', 'Black/African American (Not Hispanic)', 'Hispanic', 'Hispanic Or Latino (All Races)', 'More than one race', 'Multiracial', 'Multi-racial', 'Native Hawaiian or Other Pacific Islander', 'Native American', 'Native American Indian', 'Native Hawaiian or Other Pacific Islander, Other', 'Other, White', 'Unknown, White', 'White', 'White (Not Hispanic / Latino)', 'American Indian or Alaska Native, Black or African American', 'American Indian or Alaska Native, Black or African American, White', 'American Indian or Alaska Native, Native Hawaiian or Other Pacific Islander, White', 'American Indian or Alaska Native, White', 'Asian, Black or African American, Other', 'Asian, Native Hawaiian or Other Pacific Islander', 'Asian, Other', 'Black or African American, Native Hawaiian or Other Pacific Islander, White', 'Black or African American, Other, White', 'Native Hawaiian or Other Pacific Islander, White', 'Pacific Islander', 'White, White (Not Hispanic / Latino)') then 2 
	when  T1.race in ('Declined to specify', 'Unknown', 'UNKNOWN', 'Unknown/Not Reported', 'Other, Unknown', 'Other, Unknown/Not Reported') then 9 
	when  T1.race is null then 9 
	else 99999 
	end f1_otherrace,
T2.f1_insurance,
T2.f1_visit_type,
T2.f1_reason_visit,
T2.f1_pregnant,
T2.f1_contraception,
T2.f1_sympt,
T2.f1_contact_std,
T2.f1_pelvic_exam,
T2.f1_mensex,
T2.f1_femsex,
T2.f1_sexor3,
T2.f1_numsex3,
T2.f1_sexuality,
T2.f1_newsex,
T2.f1_rectal_exposure,
T2.f1_pharynx_exposure,
T2.f1_partner_tx,
T2.f1_gisp_travel,
T2.f1_gisp_sex_work,
T2.f1_gisp_antibiotic,
T2.f1_gisp_idu,
T2.f1_gisp_non_idu,
T2.f1_gisp_gc_12,
T2.f1_gisp_gc_ever,
T2.f1_hivtest,
T2.f1_hivtestdate,
T2.f1_hivresultlast,
T2.f1_hivtest_refuse,
T2.f1_hpvvaxadmin,
T2.f1_sxabdomen,
T2.f1_sxdysuria,
T2.f1_sxdischarge,
T2.f1_sxlesion,
T2.f1_pedischarge,
T2.f1_peabdomen,
T2.f1_cmt,
T2.f1_adnexal 
FROM public.pplm_sti_clinic_a_100037_s_2 T1 
INNER JOIN public.pplm_sti_clinic_a_100037_s_4 T2 ON ((T1.id = T2.f1_patientid)) ;


\copy pplm_sti_clinic_a_100037_s_5 to '/tmp/pplm_SSUN_clinic_thruApril2016.csv' csv header

--
-- Script shutdown section 
--

DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_1 CASCADE;

DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_2 CASCADE;
	
DROP VIEW IF EXISTS public.pplm_sti_clinic_a_100037_s_3 CASCADE;

DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_4 CASCADE;

DROP TABLE IF EXISTS public.pplm_sti_clinic_a_100037_s_5 CASCADE;


