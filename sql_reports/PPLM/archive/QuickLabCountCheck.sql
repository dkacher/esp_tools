--Get labs of interest for selected patients who have facility records
drop table if exists temp_patient_labs_first;
create table temp_patient_labs_first as select emr_labresult.order_natural_key, emr_labresult.updated_timestamp, emr_labresult.patient_id, emr_labresult.date, emr_labresult.native_code, native_name, test_name, specimen_source, result_string from conf_labtestmap, emr_labresult, emr_stiencounterextended
where (test_name = 'gonorrhea' or test_name = 'chlamydia' or test_name = 'hivcdc' or test_name = 'pregnancy') 
and emr_labresult.native_code = conf_labtestmap.native_code
and emr_labresult.patient_id = emr_stiencounterextended.patient_id
--exclude the incorrect natural keys with extra stuff in them
and emr_labresult.natural_key not like '%Chlam%'
and emr_labresult.natural_key not like '%GC%';

drop table if exists temp_patient_labs;
create table temp_patient_labs as select * from 
(select order_natural_key, updated_timestamp, patient_id, date, native_code, native_name, test_name, specimen_source, result_string, row_number() over (partition by temp_patient_labs_first.order_natural_key order by updated_timestamp) as rowno 
from temp_patient_labs_first)x 
where x.rowno = 1;


--Join extended table against encounter to get the encounter id for the report
drop table if exists temp_patient_stivisits;
create table temp_patient_stivisits as select sti.patient_id, enc.id as encounter_id, sti.date as encounter_date from emr_stiencounterextended sti
join emr_encounter enc on
sti.natural_key = enc.natural_key and enc.date >= '01-01-2015' and enc.date < '01-01-2016';


-- Build up the data needed for the report from joining the two tables on date
drop table if exists temp_patient_stivisits_labs;
create table temp_patient_stivisits_labs as 
select stivis.patient_id, stivis.encounter_id, stivis.encounter_date, native_code, native_name, test_name, result_string, specimen_source
from temp_patient_stivisits stivis
join temp_patient_labs on
temp_patient_labs.patient_id = stivis.patient_id
and
temp_patient_labs.date = stivis.encounter_date;

-- Test set of labs included
Select count(*) from temp_patient_stivisits_labs;

