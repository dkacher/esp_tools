
set client_min_messages to error;

--Get labs of interest for selected patients who have facility records
drop table if exists temp_patient_labs_first;
create table temp_patient_labs_first as select emr_labresult.order_natural_key, emr_labresult.updated_timestamp, emr_labresult.patient_id, emr_labresult.date, emr_labresult.native_code, native_name, test_name, specimen_source, result_string from conf_labtestmap, emr_labresult
where (test_name = 'gonorrhea' or test_name = 'chlamydia' or test_name = 'hiv' or test_name = 'pregnancy') 
and emr_labresult.native_code = conf_labtestmap.native_code
--exclude the incorrect natural keys with extra stuff in them
--and emr_labresult.natural_key not like '%Chlam%'
--and emr_labresult.natural_key not like '%GC%'
and emr_labresult.patient_id in (select distinct patient_id from emr_stiencounterextended);

-- Grab only the latest by updated timestamp of records where there are duplicate
-- records with the same order_natural_key
-- Some duplicates loaded by mistake due to incorrect natural_keys in the load files
drop table if exists temp_patient_labs;
create table temp_patient_labs as select * from 
(select order_natural_key, updated_timestamp, patient_id, date, native_code, native_name, test_name, result_string, specimen_source, row_number() over (partition by temp_patient_labs_first.order_natural_key order by updated_timestamp) as rowno 
from temp_patient_labs_first)x 
where x.rowno = 1;


--Join extended table against encounter to get the encounter id for the report
drop table if exists temp_patient_stivisits;
create table temp_patient_stivisits as select sti.patient_id, enc.id as encounter_id, sti.date as encounter_date from emr_stiencounterextended sti
join emr_encounter enc on
sti.natural_key = enc.natural_key 
and enc.date >= :start_date 
and enc.date < :end_date
and raw_encounter_type ilike '%VISIT%';


-- Build up the data needed for the report from joining the two tables on date
drop table if exists temp_patient_stivisits_labs;
create table temp_patient_stivisits_labs as 
select stivis.patient_id, stivis.encounter_id, stivis.encounter_date, native_code, native_name, test_name, result_string, specimen_source
from temp_patient_stivisits stivis
join temp_patient_labs on
temp_patient_labs.patient_id = stivis.patient_id
and
temp_patient_labs.date = stivis.encounter_date;

-- Test set of labs included
Select distinct native_code from temp_patient_stivisits_labs;
select distinct native_name from temp_patient_stivisits_labs;
select distinct test_name from temp_patient_stivisits_labs;
select distinct result_string from temp_patient_stivisits_labs;
select distinct specimen_source from temp_patient_stivisits_labs;

--Report
drop table if exists temp_ssun_labs_report;
create table temp_ssun_labs_report as select t.patient_id as "F3_PatientID", t.encounter_id as "F3_EventID", to_char(encounter_date, 'mm/dd/yyyy') as "F3_Visdate",
case 
	when t.test_name = 'gonorrhea' then '2'
	when t.test_name = 'chlamydia' then '3'
	when t.test_name = 'hiv' then '6'
	when t.test_name = 'pregnancy' then '20'
	else concat('99999:test_name: ', test_name)
end as "F3_Condtested",

case 
	--Culture
	--NAAT
	when t.native_code = '17303 Chlamydia--17305' then '2'
	when t.native_code = '17304 GC--17305' then '2'
	when t.native_code = '17303 Chlamydia--70049' then '2'
	when t.native_code = '17304 GC--70049' then '2'
	when t.native_code in ('CHLAMYDIA/GC--17303', 'CHLAMYDIA/GC--17304', 'CHLAMYDIA/GC--70043800', 'CHLAMYDIA/GC--70043900', 'CHLAMYDIA/GC--86005811', 'CHLAMYDIA/GC--86005814', 'CHLAMYDIA/GC--86005815', 'CHLAMYDIA/GC--43305-2') then '2'
    --HIV tests, there are 5 different tests
	when t.native_code = '86009052 HIV AG/AB, 4TH GEN--91431' then '14'
    when t.native_code = '86009052 HIV AG/AB, 4TH GEN--19728' then '14'  
	when t.native_code = '86009052 HIV AG/AB, 4TH GEN--37363' then '14'
	when t.native_code = '70055100 HIV 1/2 EIA AB SCREEN--19728' then '11'
    when t.native_code = '--Rapid HIV' then '11'
	when t.native_code = 'HIV--86009052' then '14'
	when t.native_code = 'HIV--OFFICE_LAB' then '11'
	
	--pregnancy
	when native_code = '--High Sensitivity Urine Pregnancy Test' then '40'
	when native_code = 'HCG QUALITATIVE--OFFICE_LAB' then '40'
	--Probe
	--Gram Stain
	--HIV Nucleic acid
	-- HIV rapid HIV-1 or HIV-1/2 antibody (Ab) test
	--HIV-1 Immunoassay (IA)
	-- HIV-1/2 IA
	--HIV-1/2 Ag/Ab IA
	--WB
	--IFA	
	--HIV-1/HIV-2 differentiation IA
	--HIV pooled RNA
	--Pregnancy
	--Other HIV-2
	--Else OTHER
	--CHANGE THIS TO 88 after validation.. make sure the OTHERS are OK
	else concat('99999:native_code: ', native_code)
end as "F3_Test_Type",

case
    when t.result_string ilike 'Negative' then '0'
    when t.result_string ilike 'Positive' then '1'
	when t.result_string ilike 'Not Detected' then '0'
	when t.result_string = 'Detected' then '1'
	when t.result_string = 'DETECTED' then '1'
    when t.result_string ilike 'NON-REACTIVE%' then '2'
	when t.result_string like 'Reactive%' then '3'
	when t.result_string = 'Inconclusive' then '4'
	when t.result_string ilike '%EQUIV%' then '4'
	--when t.result_string = 'Undetermined' then 4
	--when t.result_string = 'Weakly Reactive' then 5
	when t.result_string ilike 'QNS%' then '6'
	when t.result_string ilike 'Inverted swab' then '6'
	when t.result_string ilike 'Wrong swab type' then '6'
	when t.result_string = 'Not within liquid level parameters for testing' then '6'
	when t.result_string = 'Test Not Performed' 
		or t.result_string = 'Swab not present' 
		or t.result_string ilike '%Invalid%'  then '8'
    when t.result_string = '' 
		or t.result_string is null then '9'
	else concat('99999:result_string: ', t.result_string)
end as "F3_Qualres",

case 
	when t.specimen_source ilike'%Urethral%' then '1'
	when t.specimen_source ilike '%endoc%' or t.specimen_source ilike '%swab%'
	or t.specimen_source ilike '%cerv%' or t.specimen_source ilike '%vagin%' then '2' 
	when t.specimen_source ilike '%urine%' then '3'
	when t.specimen_source ilike '%rectal%' or t.specimen_source ilike '%rectum%' then '4'
	when t.specimen_source ilike '%throat%' then '5' 
	when t.specimen_source ilike '%blood%' then '6'
	when t.specimen_source ilike 'other' then '8'
	when t.test_name = 'hiv' then '6'
	when t.specimen_source = null then null
	when trim(t.specimen_source) is null then null
	else concat('99999:t.specimen_source: ', t.specimen_source)
end as "F3_Anatsite"

from temp_patient_stivisits_labs t;
select count(*) from temp_ssun_labs_report;

\copy temp_ssun_labs_report to '/tmp/SSUN_labs.csv' csv header


