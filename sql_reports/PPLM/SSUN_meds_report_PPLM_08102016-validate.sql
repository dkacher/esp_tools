set client_min_messages to error;

--Get meds for selected patients who have sti records
drop table if exists temp_patient_meds;
create table temp_patient_meds as select emr_prescription.patient_id, emr_prescription.date, name, dose, quantity, frequency, start_date, end_date 
from emr_prescription where 
patient_id in (select distinct patient_id from emr_stiencounterextended);


--Join extended table against encounter to get the encounter id for the report
drop table if exists temp_patient_stivisits;
create table temp_patient_stivisits as select sti.patient_id, enc.id as encounter_id, sti.date as encounter_date from emr_stiencounterextended sti
join emr_encounter enc on
sti.natural_key = enc.natural_key 
and enc.date >= :start_date 
and enc.date < :end_date
and raw_encounter_type ilike '%VISIT%';


-- Build up the data needed for the report from joining the two tables on date
drop table if exists temp_patient_stivisits_meds;
create table temp_patient_stivisits_meds as 
select stivis.patient_id, stivis.encounter_id, stivis.encounter_date, name, dose, quantity, frequency, start_date, end_date
from temp_patient_stivisits stivis
join temp_patient_meds on
temp_patient_meds.date = stivis.encounter_date
and temp_patient_meds.patient_id = stivis.patient_id;



drop table if exists temp_ssun_meds_report;
create table temp_ssun_meds_report as select t.patient_id as "F4_PatientID", t.encounter_id as "F4_EventID", to_char(encounter_date, 'mm/dd/yyyy') as "F4_Visdate",
case 
	when t.name ilike '%amoxicillin%' or t.name ilike '%augmentin%' 
		or t.name ilike  '%AMOX TR-POTASSIUM CLAVULANATE%' then '10'
	when t.name ilike '%ampicillin%' 
		or t.name ilike '%AMPICIL TRIHYD/PROBENECID%' then '11'
	when t.name ilike '%azithromycin%' 
		or t.name ilike '%zithromax%' then '20'
	when t.name ilike '%cefdinir%' 
		or t.name ilike '%omincef%' then '36'
	when t.name ilike '%cipro%' then '40'
	when t.name ilike '%clindamycin%' 
		or t.name ilike '%cleocin%'
		or t.name ilike '%CLINDESSE%' then '22'
	when t.name ilike '%LEVOFLOX%' 
		or t.name = 'LEVAQUIN' then '41'
	when t.name ilike '%MOXIFLOX%' then '42'
	when t.name ilike '%CEFIXIME%' 
		or t.name = 'SUPRAX' then '30'
	when t.name ilike '%cefotaxime%'then '32'
	when t.name ilike '%cefoxitin%' then '33'
	when t.name ilike '%cefuroxime%' then '38'
	when t.name ilike '%doxycycline%' 
		or t.name ilike '%VIBRAMYCIN%'
		or t.name ilike '%DORYX%' 
		or t.name ilike '%ORAXYL%' 
		or t.name ilike '%VIBRA-TABS%' then '50'
	when t.name ilike '%erythromycin base%' then '21'
	when t.name = '%GATIFLOXACIN%' then '88'
	when t.name ilike '%gentamicin%' 
		or t.name ilike '%GENTAM SULF/SODIUM CHLORIDE%' then '23'
	when t.name ilike '%FACTIVE%' 
		or t.name ilike '%GEMIFLOXACIN MESYLATE%' then '44'
	when t.name ilike '%metronidazole%' 
		or t.name ilike '%FLAGYL%' 
		or t.name ilike '%metrogel%' then  '60'
	when t.name ilike '%CEFTIBUTEN%' then '35'
	when t.name ilike '%ceftriaxone%' then '37'
	when t.name ilike '%cefpodoxime%' then '34'
	when t.name ilike '%tinidazole%' 
		or t.name ilike '%TINDAMAX%' then '61'
	when t.name ilike '%OFLOXACIN%' then '43'
	when t.name ilike '%truvada%' or t.name ilike '%EMTRICITABINE/TENOFOVIR%' then '70'
	--else concat('99999:med_name: ', name)
        else '88'
end as "F4_Medication",

null "F4_Medication_other",

	case
	when t.dose = '100 mg' 
		or t.dose = '100 mg/mL' 
		or t.dose = '500 mg/5 mL' 
		or t.dose = '800 mg/8 mL (100 mg/mL)' then '1'
	when t.dose = '125 mg/5 mL' then '2'
	when t.dose = '150 mg' 
		or t.dose = '150 mg/' 
		or t.dose = '150 mg/mL'
		or t.dose = '150' then '3'
	when t.dose = '200 mg' 
		or t.dose = '200 mg/5 mL' then '4'
	when t.dose = '250 mg' then '6'
	when t.dose = '300 mg' then '7'
    when t.dose = '320 mg' then '8'
	when t.dose = '400 mg' 
		or t.dose = '400 mg/mL' then '9'
	when t.dose = '500 mg' then '10'
	when t.dose = '600 mg' then '11'
	when t.dose = '800 mg' then '13'
	when t.dose = '1 gram' 
		or t.dose = '1,000 mg' then '14'

	--else concat('99999:dose ', dose)
        else '88'
end as "F4_Dosage",


null "F4_Number_doses",


null  "F4_Dose_freq",

	case
	when (t.end_date - t.start_date <1 ) or (t.end_date - t.start_date >100) then null
	when (t.end_date - t.start_date) = 1 then 1
	when (t.end_date - t.start_date) = 3 then 2
	when (t.end_date - t.start_date) = 5 then 3
	when (t.end_date - t.start_date) = 7 then 4
	when (t.end_date - t.start_date) = 10 then 5
	when (t	.end_date - t.start_date) = 14 then 6
	when (t.end_date is null or t.start_date is null) then 9
	else 8
end as "F4_Duration"

from temp_patient_stivisits_meds t;

\copy temp_ssun_meds_report to '/tmp/SSUN_meds.csv' csv header

