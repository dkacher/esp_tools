#!/bin/bash

####SITE SPECIFIC VARIABLES
# Enter Email Address Separated By Semicolons
RECIPIENTS="keberhardt@commoninf.com"
SITE_NAME="SITE_NAME"
#CREATE A 3 LETTER SITE ABBREVIATION. INCLUDE THE DASH (-) AT END
SITE_ABBR="SITE-"
FROM="esp-no-reply@test.org"
DBNAME="esp"
ESP_LOG_DIR="/srv/esp/logs"
REPORT_DIR="/srv/esp/data/covid_suspect_surv_reports"
ZIP="/usr/bin/zip"
KEYCHAIN="/srv/esp/keychain-2.8.5/keychain"
SFTP="/usr/bin/sftp"
DPH_CERT="/srv/esp/.ssh/test-edss-dph.key"
DPH_SFTP_USERNAME="test-edss"
DPH_SFTP_SERVER="sftp-site.gov"


### PLEASE DO NOT MODIFY BELOW THIS LINE UNLESS INSTRUCTED TO DO SO ###

#WEEKLY_COVID_REPORT_SCRIPT="/srv/esp/esp_tools/sql_reports/COVID/suspect_surv/covid-surveillance-report.sql"
SUBJECT="${SITE_NAME}_Historical_COVID_Suspect_Surveillance_Report_Uploaded"
EMAIL_UTILITY="/srv/esp/esp_tools/send_attached_file.py"
RUN_TIME=`date -u +%Y%m%dT%H:%M:%SZ`
LOGFILE_NAME=historical-covid-suspect-surv-report.$RUN_TIME.log
LOGFILE=$ESP_LOG_DIR/$LOGFILE_NAME


exec 5>&1 6>&2 >$LOGFILE 2>&1


# You can pass in the "week end date" for which the report should be run
# This date must be a Saturday
# Date format to pass in should be 2020-03-21 or 20200321
# If no date is passed in, then the previous Saturday to the current
# date will be used

if [ -z "$1" ]
then
   WEEK_END_DATE=`date +"%Y-%m-%d" -d "last saturday"`
else
   WEEK_END_DATE=$1
   WEEK_END_DATE=`date "+%Y-%m-%d" -d "$WEEK_END_DATE"`
   DOW=$(date -d "$WEEK_END_DATE" +%u)
   if [ $DOW -ne "6" ];
   then
      echo "ERROR: Invalid Week End Date. Date Must be a SATURDAY"
      exit
   fi
fi

echo
echo "Historical Script Started at:" $RUN_TIME "for:" $WEEK_END_DATE

#COVID_WEEKLY_SQL=$(psql $DB_NAME -f "$WEEKLY_COVID_REPORT_SCRIPT" -v run_date="'$WEEK_END_DATE'" -v site_abbr="'$SITE_ABBR'" -t)

#echo "SQL Complete. Now preparing files."

#CREATE A SEPARATE FILE FOR EACH
DENOM_FILE="historical-denom-covid-surv-"$SITE_NAME"-"$WEEK_END_DATE".csv"
DEMOG_FILE="historical-demog-covid-surv-"$SITE_NAME"-"$WEEK_END_DATE".csv"
LABRES_FILE="historical-labres-covid-surv-"$SITE_NAME"-"$WEEK_END_DATE".csv"
LABORD_FILE="historical-labord-covid-surv-"$SITE_NAME"-"$WEEK_END_DATE".csv"
ENC_FILE="historical-enc-covid-surv-"$SITE_NAME"-"$WEEK_END_DATE".csv"
MED_FILE="historical-med-covid-surv-"$SITE_NAME"-"$WEEK_END_DATE".csv"
HLTHCOND_FILE="historical-hlthcond-covid-surv-"$SITE_NAME"-"$WEEK_END_DATE".csv"
ZIP_FILE="historical-covid-surv-"$SITE_NAME"-"$WEEK_END_DATE".zip"


DENOM_QUERY="SELECT * FROM covid19_report.surv_rpt_denom_output ORDER BY week_start_date"
DEMOG_QUERY="SELECT * FROM covid19_report.surv_demog_ouput ORDER BY masked_patient_id"
LABRES_QUERY="SELECT * FROM covid19_report.surv_labresult_ouput ORDER BY masked_patient_id"
LABORD_QUERY="SELECT * FROM covid19_report.surv_laborder_ouput ORDER BY masked_patient_id"
ENC_QUERY="SELECT * FROM covid19_report.surv_enc_ouput ORDER BY masked_patient_id"
MED_QUERY="SELECT * FROM covid19_report.surv_med_ouput ORDER BY masked_patient_id"
HLTHCOND_QUERY="SELECT * FROM covid19_report.surv_hlthcond_ouput ORDER BY masked_patient_id"


DENOM_FILE_CREATE=$(psql --dbname=$DBNAME -c "\copy ($DENOM_QUERY) to '$REPORT_DIR/$DENOM_FILE' with csv header;" -t)
DEMOG_FILE_CREATE=$(psql --dbname=$DBNAME -c "\copy ($DEMOG_QUERY) to '$REPORT_DIR/$DEMOG_FILE' with csv header;" -t)
LABRES_FILE_CREATE=$(psql --dbname=$DBNAME -c "\copy ($LABRES_QUERY) to '$REPORT_DIR/$LABRES_FILE' with csv header;" -t)
LABORD_FILE_CREATE=$(psql --dbname=$DBNAME -c "\copy ($LABORD_QUERY) to '$REPORT_DIR/$LABORD_FILE' with csv header;" -t)
ENC_FILE_CREATE=$(psql --dbname=$DBNAME -c "\copy ($ENC_QUERY) to '$REPORT_DIR/$ENC_FILE' with csv header;" -t)
MED_FILE_CREATE=$(psql --dbname=$DBNAME -c "\copy ($MED_QUERY) to '$REPORT_DIR/$MED_FILE' with csv header;" -t)
HLTHCOND_FILE_CREATE=$(psql --dbname=$DBNAME -c "\copy ($HLTHCOND_QUERY) to '$REPORT_DIR/$HLTHCOND_FILE' with csv header;" -t)

echo "Files Created. Now creating zip file."

$ZIP -j $REPORT_DIR/$ZIP_FILE $REPORT_DIR/$DENOM_FILE $REPORT_DIR/$DEMOG_FILE $REPORT_DIR/$LABRES_FILE $REPORT_DIR/$LABORD_FILE $REPORT_DIR/$ENC_FILE $REPORT_DIR/$MED_FILE $REPORT_DIR/$HLTHCOND_FILE


echo "Zip file created. Now uploading to DPH"

#INVOKE SSH-AGENT
eval `$KEYCHAIN -q --noask --agents ssh --eval $DPH_CERT`

#UPLOAD THE ZIP FILE
SFTP_RESULT=$($SFTP $DPH_SFTP_USERNAME"@"$DPH_SFTP_SERVER <<EOF
put $REPORT_DIR/$ZIP_FILE
exit
EOF
)


if [ -z "$SFTP_RESULT" ]
then 
     echo "ERROR: The SFTP Upload of the COVID Suspect Surveillance report to DPH Failed. Please contact CII for assistance. The RSA key may need to be re-authenticated."
else
     echo "SFTP Upload of $ZIP_FILE to DPH Succeeded"

fi

#REMOVE THE FILES - JUST LEAVE THE ZIP
rm $REPORT_DIR/$DENOM_FILE $REPORT_DIR/$DEMOG_FILE $REPORT_DIR/$LABRES_FILE $REPORT_DIR/$LABORD_FILE $REPORT_DIR/$ENC_FILE $REPORT_DIR/$MED_FILE $REPORT_DIR/$HLTHCOND_FILE

#UPDATING SENT TIMESTAMP
SET_SENT=$(psql --dbname=$DBNAME -c "update covid19_report.suspect_nodis_case T1 SET sent_timestamp = now() FROM (select patient_id, index_date from covid19_report.surv_index_pats) T2 WHERE T1.patient_id = T2.patient_id AND T1.date = T2.index_date;")

$EMAIL_UTILITY -f $FROM -r $RECIPIENTS -p $ESP_LOG_DIR -n $LOGFILE_NAME -s $SUBJECT


exec 1>&5 2>&6


