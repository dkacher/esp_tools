CREATE SCHEMA IF NOT EXISTS covid19_report;

DROP TABLE IF EXISTS covid19_report.suspect_nodis_case;
DROP SEQUENCE IF EXISTS covid19_report.suspect_nodis_case_id_seq;
DROP TABLE IF EXISTS covid19_report.surv_pats_masked_patients;
DROP SEQUENCE IF EXISTS covid19_report.surv_pats_masked_id_seq;

CREATE SEQUENCE covid19_report.suspect_nodis_case_id_seq;

CREATE TABLE covid19_report.suspect_nodis_case
(
    id integer NOT NULL DEFAULT nextval('covid19_report.suspect_nodis_case_id_seq'::regclass),
    condition character varying(100) COLLATE pg_catalog."default" NOT NULL,
    date date NOT NULL,
    patient_id integer NOT NULL,
    criteria character varying(2000) COLLATE pg_catalog."default",
    source character varying(255) COLLATE pg_catalog."default" NOT NULL,
    status character varying(32) COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    created_timestamp timestamp with time zone NOT NULL,
    updated_timestamp timestamp with time zone NOT NULL,
    sent_timestamp timestamp with time zone,
    CONSTRAINT suspect_nodis_case_pkey PRIMARY KEY (id),
    CONSTRAINT suspect_nodis_case_patient_id_key UNIQUE (patient_id, condition, date)
);

-- NEEDED FOR REPORTING
CREATE SEQUENCE covid19_report.surv_pats_masked_id_seq;

CREATE TABLE covid19_report.surv_pats_masked_patients
(
    patient_id integer,
    masked_patient_id character varying(128)
);
