drop table IF EXISTS covid19_report.algo_analysis_wk_counts;
create table covid19_report.algo_analysis_wk_counts
as
select :run_date::date as week_end_date, covid19_report.mmwrwkn(:run_date::date) as week_number, count(distinct(patient_id)) criteria_count, 'cases_total'::text criteria
from nodis_case
where condition = 'covid19'
and date >= (:run_date::date - INTERVAL '6 days')::date
and date <= :run_date::date
UNION
select :run_date::date::date as week_end_date, covid19_report.mmwrwkn(:run_date::date) as week_number, count(distinct(patient_id)) criteria_count, criteria
from nodis_case
where condition = 'covid19'
and date >= (:run_date::date - INTERVAL '6 days')::date
and date <= :run_date::date
group by criteria
UNION 
select :run_date::date::date as week_end_date, covid19_report.mmwrwkn(:run_date::date) as week_number, count(distinct(patient_id)) criteria_count, 
'cat2_added_if_fever_dropped'::text as criteria
from hef_event
where name in (
'dx:pneumonia','dx:bronchitis','dx:lri','dx:ards'
)
and date >= (:run_date::date - INTERVAL '6 days')::date
and date <= :run_date::date
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' and date >= '2020-02-01' and date <= :run_date::date)
UNION
-- CATEGORY 3 THAT WOULD GET PICKED UP IN THE WEEK IF WE DROPPED FEVER 
select :run_date::date as week_end_date, covid19_report.mmwrwkn(:run_date::date) as week_number, count(distinct(patient_id)) criteria_count,
'cat3_added_if_fever_dropped'::text as criteria
from hef_event
where name in (
'dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath'
)
and date >= (:run_date::date - INTERVAL '6 days')::date
and date <= :run_date::date
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' and date >= '2020-02-01' and date <= :run_date::date)
UNION
-- CATEGORY 3 THAT WOULD GET PICKED UP IF WE DROPPED FEVER FROM BOTH CAT2 and CAT3
-- ELIMINATES THOSE THAT WOULD NOW GET PICKED UP BY CAT2 FIRST
--1452
select :run_date::date as week_end_date, covid19_report.mmwrwkn(:run_date::date) as week_number, count(distinct(patient_id)) criteria_count,
'cat3_added_if_fever_dropped_from_2_and_3'::text as criteria
from hef_event
where name in (
'dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath'
)
and date >= (:run_date::date - INTERVAL '6 days')::date
and date <= :run_date::date
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' and date >= '2020-02-01' and date <= :run_date::date)
and patient_id not in (
	select patient_id from hef_event
    where name in ('dx:pneumonia','dx:bronchitis','dx:lri','dx:ards') and date >= (:run_date::date - INTERVAL '6 days')::date and date <= :run_date::date)
UNION
-- CATEGORY 3 THAT WOULD GET PICKED UP IF WE INCLUDED COVID DIAGNOSIS CODE AS AN "OR" FOR FEVER
-- I AM JUST LOOKING FOR COVID DX IN THE SAME WEEK AS OTHER DIAG TO CUT DOWN ON OVERLAPPING PATIENTS BETWEEN WEEKS.
-- ALGORITHM CHANGE COULD POSSIBLY PICK UP MORE OR TRIGGER EARLIER AS IT WOULD BE LOOKING WITHIN 14 DAYS
-- 142
select :run_date::date::date as week_end_date, covid19_report.mmwrwkn(:run_date::date) as week_number, count(distinct(patient_id)) criteria_count,
'cat3_added_if_covid_dx_or_fever'::text as criteria
from hef_event
where name in (
'dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath'
)
and date >= (:run_date::date - INTERVAL '6 days')::date
and date <= :run_date::date
and patient_id in (select patient_id from hef_event where name = 'dx:covid19' and date >= (:run_date::date - INTERVAL '6 days')::date and date <= :run_date::date)
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' and date >= '2020-02-01' and date <= :run_date::date)
UNION
-- CATEGORY 2 CASES THAT WOULD GET DROPPED IF FLU WAS A NEGATING DIAG CODE
-- 3	
select :run_date::date as week_end_date, covid19_report.mmwrwkn(:run_date::date) as week_number, count(distinct(T1.patient_id)) criteria_count,
'cat2_dropped_by_flu_dx_negation'::text as criteria
from emr_encounter T1
JOIN emr_encounter_dx_codes T2 on T1.id = T2.encounter_id
JOIN nodis_case T3 on T1.patient_id = T3.patient_id
WHERE
dx_code_id ~ '^(icd10:J09|icd10:J10|icd10:J11|icd9:487.0|icd9:487.1|icd9:487.8|icd9:488.0|icd9:488.1|icd9:488.8)'
and T1.date >= (:run_date::date - INTERVAL '6 days')::date
and T1.date <= :run_date::date
and condition = 'covid19'
and criteria in ('Category 2')
-- case date after start period
and T3.date >= '2020-02-01'
-- case date before week end
and T3.date <= :run_date::date
-- flu dx should be within 14 days before case date
and T1.date >= T3.date - INTERVAL '14 days' 
-- flu dx should be on/before case date
and T1.date <= T3.date
UNION
-- CATEGORY 2 CASES THAT WOULD GET DROPPED IF FLU WAS A NEGATING DIAG CODE
-- 3	
--select distinct(T1.patient_id), dx_code_id, T1.date flu_dx_date, T3.date case_date, T3.criteria as category
select :run_date::date as week_end_date, covid19_report.mmwrwkn(:run_date::date) as week_number, count(distinct(T1.patient_id)) criteria_count,
'cat3_dropped_by_flu_dx_negation'::text as criteria
from emr_encounter T1
JOIN emr_encounter_dx_codes T2 on T1.id = T2.encounter_id
JOIN nodis_case T3 on T1.patient_id = T3.patient_id
WHERE
dx_code_id ~ '^(icd10:J09|icd10:J10|icd10:J11|icd9:487.0|icd9:487.1|icd9:487.8|icd9:488.0|icd9:488.1|icd9:488.8)'
and T1.date >= (:run_date::date - INTERVAL '6 days')::date
and T1.date <= :run_date::date
and condition = 'covid19'
and criteria in ('Category 3')
-- case date after start period
and T3.date >= '2020-02-01'
-- case date before week end
and T3.date <= :run_date::date
-- flu dx should be within 14 days before case date
and T1.date >= T3.date - INTERVAL '14 days' 
-- flu dx should be on/before case date
and T1.date <= T3.date;


	
INSERT INTO covid19_report.algo_analysis_wk_summary 
select
max(week_end_date) week_end_date,
max(week_number) week_number,
max(case when criteria = 'cases_total' then criteria_count end) cases_total,
max(case when criteria = 'Category 1' then criteria_count end) category_1,
max(case when criteria = 'Category 2' then criteria_count end) category_2,
max(case when criteria = 'Category 3' then criteria_count end) category_3,
max(case when criteria = 'cat2_added_if_fever_dropped' then criteria_count end) cat2_added_if_fever_dropped,
max(case when criteria = 'cat3_added_if_fever_dropped' then criteria_count end) cat3_added_if_fever_dropped,
max(case when criteria = 'cat3_added_if_fever_dropped_from_2_and_3' then criteria_count end) cat3_added_if_fever_dropped_from_2_and_3,
max(case when criteria = 'cat3_added_if_covid_dx_or_fever' then criteria_count end) cat3_added_if_covid_dx_or_fever,
max(case when criteria = 'cat2_dropped_by_flu_dx_negation' then criteria_count end) cat2_dropped_by_flu_dx_negation,
max(case when criteria = 'cat3_dropped_by_flu_dx_negation' then criteria_count end) cat3_dropped_by_flu_dx_negation
from covid19_report.algo_analysis_wk_counts;

select * from covid19_report.algo_analysis_wk_summary;