﻿DROP TABLE IF EXISTS tmp_incare_enc_2011;
DROP TABLE IF EXISTS tmp_incare_enc_2011_next;
DROP TABLE IF EXISTS tmp_incare_enc_2011_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2012;
DROP TABLE IF EXISTS tmp_incare_enc_2012_next;
DROP TABLE IF EXISTS tmp_incare_enc_2012_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2013;
DROP TABLE IF EXISTS tmp_incare_enc_2013_next;
DROP TABLE IF EXISTS tmp_incare_enc_2013_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2014;
DROP TABLE IF EXISTS tmp_incare_enc_2014_next;
DROP TABLE IF EXISTS tmp_incare_enc_2014_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2015;
DROP TABLE IF EXISTS tmp_incare_enc_2015_next;
DROP TABLE IF EXISTS tmp_incare_enc_2015_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2016;
DROP TABLE IF EXISTS tmp_incare_enc_2016_next;
DROP TABLE IF EXISTS tmp_incare_enc_2016_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2017;
DROP TABLE IF EXISTS tmp_incare_enc_2017_next;
DROP TABLE IF EXISTS tmp_incare_enc_2017_next_days;

DROP TABLE IF EXISTS tmp_incare_enc_all_year_counts;
DROP TABLE IF EXISTS tmp_incare_enc_2011_counts_and_pcts;



--2011
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2011 AS
SELECT patient_id, min(date) first_encounter
FROM gen_pop_tools.clin_enc e, emr_patient p
WHERE e.patient_id = p.id
--AND center_id not in ('1') 
AND date >= '2011-01-01' and date < '2012-01-01'
AND date_part('year', age('2011-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2011
CREATE TABLE tmp_incare_enc_2011_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2011 t
LEFT JOIN gen_pop_tools.clin_enc e ON (e.patient_id = t.patient_id)
WHERE e.date > t.first_encounter
GROUP BY t.patient_id;


--2011
CREATE TABLE tmp_incare_enc_2011_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2011'::text as rpt_year
FROM tmp_incare_enc_2011 t
LEFT JOIN tmp_incare_enc_2011_next n ON (n.patient_id = t.patient_id);

--2012
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2012 AS
select patient_id, min(date) first_encounter 
FROM gen_pop_tools.clin_enc e, emr_patient p
WHERE e.patient_id = p.id
--AND center_id not in ('1') 
AND date >= '2012-01-01' and date < '2013-01-01'
AND date_part('year', age('2012-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2012
CREATE TABLE tmp_incare_enc_2012_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2012 t
LEFT JOIN gen_pop_tools.clin_enc e ON (e.patient_id = t.patient_id)
WHERE e.date > t.first_encounter
GROUP BY t.patient_id;

--2012
CREATE TABLE tmp_incare_enc_2012_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2012'::text as rpt_year
FROM tmp_incare_enc_2012 t
LEFT JOIN tmp_incare_enc_2012_next n ON (n.patient_id = t.patient_id);

--2013
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2013 AS
select patient_id, min(date) first_encounter 
FROM gen_pop_tools.clin_enc e, emr_patient p
WHERE e.patient_id = p.id
--AND center_id not in ('1') 
AND date >= '2013-01-01' and date < '2014-01-01'
AND date_part('year', age('2013-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2013
CREATE TABLE tmp_incare_enc_2013_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2013 t
LEFT JOIN gen_pop_tools.clin_enc e ON (e.patient_id = t.patient_id)
WHERE e.date > t.first_encounter
GROUP BY t.patient_id;

--2013
CREATE TABLE tmp_incare_enc_2013_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2013'::text as rpt_year
FROM tmp_incare_enc_2013 t
LEFT JOIN tmp_incare_enc_2013_next n ON (n.patient_id = t.patient_id);

--2014
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2014 AS
select patient_id, min(date) first_encounter 
FROM gen_pop_tools.clin_enc e, emr_patient p
WHERE e.patient_id = p.id
--AND center_id not in ('1') 
AND date >= '2014-01-01' and date < '2015-01-01'
AND date_part('year', age('2014-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2014
CREATE TABLE tmp_incare_enc_2014_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2014 t
LEFT JOIN gen_pop_tools.clin_enc e ON (e.patient_id = t.patient_id)
WHERE e.date > t.first_encounter
GROUP BY t.patient_id;

--2014
CREATE TABLE tmp_incare_enc_2014_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2014'::text as rpt_year
FROM tmp_incare_enc_2014 t
LEFT JOIN tmp_incare_enc_2014_next n ON (n.patient_id = t.patient_id);

--2015
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2015 AS
select patient_id, min(date) first_encounter 
FROM gen_pop_tools.clin_enc e, emr_patient p
WHERE e.patient_id = p.id
--AND center_id not in ('1') 
AND date >= '2015-01-01' and date < '2016-01-01'
AND date_part('year', age('2015-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2015
CREATE TABLE tmp_incare_enc_2015_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2015 t
LEFT JOIN gen_pop_tools.clin_enc e ON (e.patient_id = t.patient_id)
WHERE e.date > t.first_encounter
GROUP BY t.patient_id;

--2015
CREATE TABLE tmp_incare_enc_2015_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2015'::text as rpt_year
FROM tmp_incare_enc_2015 t
LEFT JOIN tmp_incare_enc_2015_next n ON (n.patient_id = t.patient_id);

--2016
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2016 AS
select patient_id, min(date) first_encounter 
FROM gen_pop_tools.clin_enc e, emr_patient p
WHERE e.patient_id = p.id
--AND center_id not in ('1') 
AND date >= '2016-01-01' and date < '2017-01-01'
AND date_part('year', age('2016-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2016
CREATE TABLE tmp_incare_enc_2016_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2016 t
LEFT JOIN gen_pop_tools.clin_enc e ON (e.patient_id = t.patient_id)
WHERE e.date > t.first_encounter
GROUP BY t.patient_id;

--2016
CREATE TABLE tmp_incare_enc_2016_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2016'::text as rpt_year
FROM tmp_incare_enc_2016 t
LEFT JOIN tmp_incare_enc_2016_next n ON (n.patient_id = t.patient_id);

--2017
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2017 AS
select patient_id, min(date) first_encounter 
FROM gen_pop_tools.clin_enc e, emr_patient p
WHERE e.patient_id = p.id
--AND center_id not in ('1') 
AND date >= '2017-01-01' and date < '2018-01-01'
AND date_part('year', age('2017-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2017
CREATE TABLE tmp_incare_enc_2017_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2017 t
LEFT JOIN gen_pop_tools.clin_enc e ON (e.patient_id = t.patient_id)
WHERE e.date > t.first_encounter
GROUP BY t.patient_id;

--2017
CREATE TABLE tmp_incare_enc_2017_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2017'::text as rpt_year
FROM tmp_incare_enc_2017 t
LEFT JOIN tmp_incare_enc_2017_next n ON (n.patient_id = t.patient_id);


-- ALL YEARS
CREATE TABLE tmp_incare_enc_all_year_counts AS 
select rpt_year, count(*) patient_count, next_encounter_days from tmp_incare_enc_2011_next_days GROUP by next_encounter_days, rpt_year 
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2012_next_days GROUP by next_encounter_days, rpt_year
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2013_next_days GROUP by next_encounter_days, rpt_year
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2014_next_days GROUP by next_encounter_days, rpt_year
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2015_next_days GROUP by next_encounter_days, rpt_year
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2016_next_days GROUP by next_encounter_days, rpt_year
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2017_next_days GROUP by next_encounter_days, rpt_year;


-- For output
SELECT * from tmp_incare_enc_all_year_counts
order by rpt_year, next_encounter_days;


-- SPECIAL QUERIES FOR NOELLE
-- Using the first 2011 encounter among those with >=1 encounter in 2011:
-- identify the % with and without >=1 subsequent encounter within 5 years (1825 days)
-- what % of those with an enc in the subsequent 5 years have it within 1 year?; how many within 2 years?  
-- of those who do not have an enc within 365 days of their first 2011 enc, what % have an enc within 2 years, within 5 years

-- Total patients with >=1 enc in 2011
-- 99236
-- SELECT count(*) from tmp_incare_enc_2011_next_days;

-- -- with >=1 subsequent encounter within 5 years (1825 days)
-- SELECT count(*) from tmp_incare_enc_2011_next_days 
-- WHERE next_encounter_days > 0 and next_encounter_days <= 1825;

-- -- without >=1 subsequent encounter within 5 years (1825 days)
-- SELECT count(*) from tmp_incare_enc_2011_next_days 
-- WHERE next_encounter_days =0 or next_encounter_days > 1825;

-- -- within 1 year
-- SELECT count(*) from tmp_incare_enc_2011_next_days 
-- WHERE next_encounter_days > 0 and next_encounter_days <= 365;

-- -- within 2 years
-- SELECT count(*) from tmp_incare_enc_2011_next_days 
-- WHERE next_encounter_days > 0 and next_encounter_days <= 730;

-- -- total patients without an encounter within 365 days
-- SELECT count(*) from tmp_incare_enc_2011_next_days 
-- WHERE next_encounter_days = 0 or next_encounter_days > 365;

-- -- with those more than 1 year out, how many have one within 2 years
-- SELECT count(*) from tmp_incare_enc_2011_next_days 
-- WHERE next_encounter_days > 365 and next_encounter_days <= 730;

-- -- with those more than 1 year out, how many have one within 5 years
-- SELECT count(*) from tmp_incare_enc_2011_next_days 
-- WHERE next_encounter_days > 365 and next_encounter_days <= 1825;

CREATE TABLE tmp_incare_enc_2011_counts_and_pcts AS
SELECT pats_2011, enc_within_5yrs, no_enc_within_5yrs, 
enc_within_1yr,
enc_within_2yr,
no_enc_within_1yr,
enc_gt1yr_lte2yr,
enc_gt1yr_lte5yr,
round( ((enc_within_5yrs::numeric/pats_2011::numeric)*100), 2)::real pct_enc_within_5years, 
round( ((no_enc_within_5yrs::numeric/pats_2011::numeric)*100), 2)::real pct_no_enc_within_5years,
round( ((enc_within_1yr::numeric/enc_within_5yrs::numeric)*100), 2)::real pct_enc_within_1yr_5yrsubset,
round( ((enc_within_2yr::numeric/enc_within_5yrs::numeric)*100), 2)::real pct_enc_within_2yr_5yrsubset,
round( ((enc_gt1yr_lte2yr::numeric/no_enc_within_1yr::numeric)*100), 2)::real pct_enc_gt1yr_lte2yr_noenc1yrsubset,
round( ((enc_gt1yr_lte5yr::numeric/no_enc_within_1yr::numeric)*100), 2)::real pct_enc_gt1yr_lte5yr_noenc1yrsubset
FROM
(SELECT count(*) pats_2011 from tmp_incare_enc_2011_next_days) T1,
-- with >=1 subsequent encounter within 5 years (1825 days)
(SELECT count(*) enc_within_5yrs from tmp_incare_enc_2011_next_days 
WHERE next_encounter_days > 0 and next_encounter_days <= 1825) T2,
-- without >=1 subsequent encounter within 5 years (1825 days)
(SELECT count(*) no_enc_within_5yrs from tmp_incare_enc_2011_next_days 
WHERE next_encounter_days =0 or next_encounter_days > 1825) T3,
(-- within 1 year
SELECT count(*) enc_within_1yr from tmp_incare_enc_2011_next_days 
WHERE next_encounter_days > 0 and next_encounter_days <= 365) T4,
-- within 2 years
(SELECT count(*) enc_within_2yr from tmp_incare_enc_2011_next_days 
WHERE next_encounter_days > 0 and next_encounter_days <= 730) T5,
-- total patients without an encounter within 365 days
(SELECT count(*) no_enc_within_1yr from tmp_incare_enc_2011_next_days 
WHERE next_encounter_days = 0 or next_encounter_days > 365) T6,
-- with those more than 1 year out, how many have one within 2 years
(SELECT count(*) enc_gt1yr_lte2yr from tmp_incare_enc_2011_next_days 
WHERE next_encounter_days > 365 and next_encounter_days <= 730) T7,
-- with those more than 1 year out, how many have one within 5 years
(SELECT count(*) enc_gt1yr_lte5yr from tmp_incare_enc_2011_next_days 
WHERE next_encounter_days > 365 and next_encounter_days <= 1825) T8;

--For Output
SELECT * FROM tmp_incare_enc_2011_counts_and_pcts;




