--Jan. 1, 2012 through June 30, 2017 

--CREATE TABLE kre_iud_output AS 
-- -- IUD DX
-- WITH kre_iud_dx_enc AS (
	-- SELECT patient_id, 
	-- date_trunc('month', date)::date as index_date,
	-- 'iud_dx'::text as enc_type
	-- FROM emr_encounter e, emr_encounter_dx_codes d
	-- WHERE e.id = d.encounter_id
	-- AND date BETWEEN '01-01-2015' and '02-28-2017'
	-- AND dx_code_id in ('icd9:V25.11', 'icd9:V25.13', 'icd10:Z30.430', 'icd10: Z40.433')
	-- GROUP BY patient_id, index_date
	-- ORDER BY patient_id, index_date),
	
-- UPDATED	
-- IUD DX
WITH kre_iud_dx_enc AS (
	SELECT patient_id, 
	date_trunc('month', date)::date as index_date,
	'iud_dx'::text as enc_type
	FROM emr_encounter e, emr_encounter_dx_codes d, emr_patient p, static_enc_type_lookup s 
	WHERE e.id = d.encounter_id
	AND e.patient_id = p.id
	AND date BETWEEN '01-01-2012' and '06-30-2017'
	AND dx_code_id in ('icd9:V25.11', 'icd10:Z30.430')
	AND (center_id not in ('1', '34') or center_id IS NULL)
	AND e.raw_encounter_type = s.raw_encounter_type 
    AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
	GROUP BY patient_id, index_date
	ORDER BY patient_id, index_date),
	
	
	
-- -- IMPLANT DX
	-- kre_iud_implant_dx_enc AS (
	-- SELECT patient_id, 
	-- date_trunc('month', date)::date as index_date,
	-- 'implant_dx'::text as enc_type
	-- FROM emr_encounter e, emr_encounter_dx_codes d
	-- WHERE e.id = d.encounter_id
	-- AND date BETWEEN '01-01-2015' and '02-28-2017'
	-- AND dx_code_id in ('icd9:V25.5', 'icd10:Z30.017')
	-- GROUP BY patient_id, index_date
	-- ORDER BY patient_id, index_date),
	
	
-- -- CONTRACEPTION DX
	-- kre_iud_cm_dx_enc AS (
	-- SELECT patient_id, 
	-- date_trunc('month', date)::date as index_date,
	-- 'cm_dx'::text as enc_type
	-- FROM emr_encounter e, emr_encounter_dx_codes d
	-- WHERE e.id = d.encounter_id
	-- AND date BETWEEN '01-01-2015' and '02-28-2017'
	-- AND dx_code_id in ('icd9:V25.01', 'icd9:V25.04', 'icd9:V25.2', 'icd9:V25.02', 'icd10:Z30.013', 'icd10:Z30.011', 'icd10:Z30.02', 'icd10:Z30.2', 'icd10:Z30.015', 'icd10:Z30.016', 'icd10:Z30.018', 'icd10:Z30.019')
	-- GROUP BY patient_id, index_date
	-- ORDER BY patient_id, index_date),

-- UPDATED	
-- ALL ENCOUNTERS
	kre_iud_all_enc AS (
	SELECT patient_id, 
	date_trunc('month', date)::date as index_date,
	'encounter'::text as enc_type
	FROM emr_encounter e, emr_patient p, static_enc_type_lookup s 
	WHERE e.patient_id = p.id
	AND date BETWEEN '01-01-2012' and '06-30-2017'
	AND (center_id not in ('1', '34') or center_id IS NULL)
	AND e.raw_encounter_type = s.raw_encounter_type 
	AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
	GROUP BY patient_id, index_date
	ORDER BY patient_id, index_date),
	
-- ORAL RX	
	kre_iud_oral AS (
	SELECT patient_id,
	date_trunc('month', date)::date as index_date,
	'oral_rx'::text as enc_type
	FROM emr_prescription
	WHERE name ILIKE ANY(ARRAY[
	'Alesse%',
	'Altavera%',
	'Alyacen%',
	'Amethia%',
	'Amethyst%',
	'Apri %',
	'Aranelle%',
	'Ashlyna%',
	'Aubra%',
	'Aviane%',
	'Azurette%',
	'Balziva%',
	'Bekyree%',
	'Beyaz%',
	'Blisovi%',
	'Brevicon%',
	'Briellyn%',
	'Camila%',
	'Camrese%',
	'Caziant%',
	'Cesia%',
	'Chateal%',
	'Cryselle%',
	'Cyclafem%',
	'Cyclessa%',
	'Cyred%',
	'Dasetta%',
	'Daysee%',
	'Deblitane%',
	'Delyla%',
	'Demulen%',
	'Desogen%',
	'Elinest%',
	'Emoquette%',
	'Enpresse%',
	'Enskyce%',
	'Errin%',
	'Estarylla%',
	'Estrostep%',
	'Falmina%',
	'Femcon%',
	'Femynor%',
	'Generess%',
	'Gianvi%',
	'Gildagia%',
	'Gildess%',
	'Heather%',
	'Introvale%',
	'Jencycla%',
	'Jolessa%',
	'Jolivette%',
	'Juleber%',
	'Junel%',
	'Kaitlib%',
	'Kariva%',
	'Kelnor%',
	'Kimidess%',
	'Kurvelo%',
	'Larin%',
	'Larissia%',
	'Layolis%',
	'Leena%',
	'Lessina%',
	'Levlen%',
	'Levlite%',
	'Levonest%',
	'Levora%',
	'Lo Minastrin%',
	'Loestrin%',
	'Lo Loestrin%',
	'Lomedia%',
	'Lo%Ovral%',
	'Loryna%',
	'LoSeasonique%',
	'%Low-Ogestrel%',
	'Lutera%',
	'Lybrel%',
	'Lyza%',
	'Marlissa%',
	'Microgestin%',
	'Micronor%',
	'Minastrin%',
	'Mircette%',
	'Modicon%',
	'Mono-Linyah%',
	'Mononessa%',
	'Myzilra%',
	'Natazia%',
	'Necon%',
	'Nikki%',
	'Nora-BE%',
	'Nordette%',
	'Norinyl%',
	'Norlyroc%',
	'Nor-QD%',
	'%ortrel%',
	'Ocella%',
	'Ogestrel%',
	'Orsythia%',
	'Ortho Micronor%',
	'Ortho%Tri%Cyclen%',
	'Ortho-Cept%',
	'Ortho%Cyclen%',
	'Ortho-Novum%',
	'Ovcon%',
	'Ovral%',
	'Ovrette%',
	'Philith%',
	'Pimtrea%',
	'Pirmella%',
	'Portia%',
	'Previfem%',
	'Quartette%',
	'Quasense%',
	'Rajani%',
	'Reclipsen%',
	'Safyral%',
	'Seasonale%',
	'Seasonique%',
	'Setlakin%',
	'Sharobel%',
	'Solia%',
	'Sprintec%',
	'Sronyx%',
	'Syeda%',
	'Tarina%',
	'Taytulla%',
	'Tilia%',
	'Tri-Estarylla%',
	'Tri-Legest%',
	'Tri-Levlen%',
	'Tri-Linyah%',
	'Tri-Lo-Estarylla%',
	'Tri-Lo-Marzia%',
	'Tri-Lo-Sprintec%',
	'TriNessa%',
	'Tri-Norinyl%',
	'Triphasil%',
	'Tri-Previfem%',
	'Tri-Sprintec%',
	'Trivora%',
	'Velivet%',
	'Vestura%',
	'Vienva%',
	'Viorele%',
	'Vyfemla%',
	'Wera%',
	'Wymzya%',
	'Yasmin%',
	'YAZ%',
	'Zarah%',
	'Zenchent%',
	'ZenChent%',
	'Zeosa%',
	'Zovia%'])	
	OR (name ilike '%desog%' and name ilike '%est%')
	OR (name ilike '%estra%' and name ilike '%dien%')
	OR (name ilike '%drosp%' and name ilike '%est%')
	OR (name ilike '%eth%' and name ilike '%dia%' and name ilike '%estr%')
	OR (name ilike '%levon%')
	OR (name ilike '%noreth%')
	OR (name ilike '%norgest%')
	OR (name ilike '%ethyno%')
	AND date BETWEEN '01-01-2012' and '06-30-2017'
	GROUP BY patient_id, index_date
	ORDER BY patient_id, index_date),

-- -- IMPLANT RX
	-- kre_iud_implants AS (
	-- SELECT patient_id,
	-- date_trunc('month', date)::date as index_date,
	-- 'implant_rx'::text as enc_type
	-- FROM emr_prescription
	-- WHERE name ilike '%Nexplanon%'
    -- OR name ilike '%Implanon%'
    -- OR (name ilike '%ETONOG%' and name not ilike '%eth%')
	-- AND date BETWEEN '01-01-2015' and '02-28-2017'
	-- GROUP BY patient_id, index_date
	-- ORDER BY patient_id, index_date),	

-- -- INJECTABLE RX
	-- kre_iud_injectable AS (
	-- SELECT patient_id,
	-- date_trunc('month', date)::date as index_date,
	-- 'inject_rx'::text as enc_type
	-- FROM emr_prescription
	-- WHERE name ilike '%depo%prov%'
    -- OR (name ilike '%depo%subq%' and name not ilike '%SOMATULINE DEPOT SUBQ%')
    -- OR (name ilike '%medrox%' and name not ilike '%conj%')
    -- AND NAME NOT ILIKE '%tab%'
    -- AND NAME NOT ILIKE '%oral%'
	-- AND date BETWEEN '01-01-2015' and '02-28-2017'
	-- GROUP BY patient_id, index_date
	-- ORDER BY patient_id, index_date),

-- -- PATCH RX
	-- kre_iud_patch AS (
	-- SELECT patient_id,
	-- date_trunc('month', date)::date as index_date,
	-- 'patch_rx'::text as enc_type
	-- FROM emr_prescription
	-- WHERE name ilike '%NORELG%'
	-- OR name ilike '%Ortho%Evra%'
	-- OR name ilike '%Xulane%'
	-- AND date BETWEEN '01-01-2015' and '02-28-2017'
	-- GROUP BY patient_id, index_date
	-- ORDER BY patient_id, index_date),

-- -- RING RX	
	-- kre_iud_ring AS (
	-- SELECT patient_id,
	-- date_trunc('month', date)::date as index_date,
	-- 'ring_rx'::text as enc_type
	-- FROM emr_prescription
	-- WHERE name ilike '%nuvaring%'
    -- OR (NAME ilike '%ETONOG%' and name ilike '%eth%')
	-- AND date BETWEEN '01-01-2015' and '02-28-2017'
	-- GROUP BY patient_id, index_date
	-- ORDER BY patient_id, index_date),

-- UPDATED	
-- ALL RX
	kre_iud_all_rx AS (
	SELECT patient_id, 
	date_trunc('month', date)::date as index_date,
	'all_rx'::text as enc_type
	FROM emr_prescription
	WHERE date BETWEEN '01-01-2012' and '06-30-2017'
	GROUP BY patient_id, index_date
	ORDER BY patient_id, index_date),

	
-- UNION TOGETHER ALL DETAILS
	kre_iud_cond_union AS (
	SELECT * from kre_iud_dx_enc
	-- UNION
	-- SELECT * from kre_iud_implant_dx_enc
	-- UNION
	-- SELECT * from kre_iud_cm_dx_enc
	UNION
	SELECT * from kre_iud_all_enc
	UNION
	SELECT * from kre_iud_oral
	-- UNION 
	-- SELECT * from kre_iud_implants
	-- UNION
	-- SELECT * from kre_iud_injectable
	-- UNION
	-- SELECT * from kre_iud_patch
	-- UNION
	-- SELECT * from kre_iud_ring
	UNION
	SELECT * from kre_iud_all_rx),

-- ADD IN PATIENT PATIENT DETAILS
-- FILTER FOR AGE RANGE FOR REPORT
	kre_iud_pat_details AS (
	SELECT patient_id, 
	enc_type,
	index_date, 
	date_part('year', age(index_date, date_of_birth)) age,
	CASE   
	when date_part('year', age(index_date, date_of_birth)) <= 14 then '0-14'    
	when date_part('year', age(index_date, date_of_birth)) <= 19 then '15-19'  
	when date_part('year', age(index_date, date_of_birth)) <= 29 then '20-29' 
	when date_part('year', age(index_date, date_of_birth)) <= 39 then '30-39' 
	when date_part('year', age(index_date, date_of_birth)) <= 44 then '40-44'   
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '45-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 59 then '50-59' 
	when date_part('year', age(index_date, date_of_birth)) <= 69 then '60-69' 
	when date_part('year', age(index_date, date_of_birth)) <= 79 then '70-79'   
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>= 80' end age_group_10_yr,
	natural_key,
	sex,
	CASE when race_ethnicity = 6 then 'hispanic'  
	when race_ethnicity = 5 then 'white' 
	when race_ethnicity = 3 then 'black' 
	when race_ethnicity = 2 then 'asian' 
	when race_ethnicity = 1 then 'native_american' 
	when race_ethnicity = 0 then 'unknown' 
	end race
	FROM kre_iud_cond_union iud
	INNER JOIN public.emr_patient p ON (iud.patient_id = p.id )
	INNER JOIN esp_mdphnet.esp_demographic d on (p.natural_key = d.patid)
	WHERE date_part('year', age(index_date, date_of_birth)) BETWEEN 15 AND 44
	AND sex = 'F')
	
-- COUNT AND PREPARE FINAL OUTPUT
SELECT
sex, age_group_10_yr, race,
count(case when  index_date = '2012-01-01' and enc_type = 'encounter' then 1 end)  jan_12_encs,
count(case when  index_date = '2012-02-01' and enc_type = 'encounter' then 1 end)  feb_12_encs,
count(case when  index_date = '2012-03-01' and enc_type = 'encounter' then 1 end)  mar_12_encs,
count(case when  index_date = '2012-04-01' and enc_type = 'encounter' then 1 end)  apr_12_encs,
count(case when  index_date = '2012-05-01' and enc_type = 'encounter' then 1 end)  may_12_encs,
count(case when  index_date = '2012-06-01' and enc_type = 'encounter' then 1 end)  jun_12_encs,
count(case when  index_date = '2012-07-01' and enc_type = 'encounter' then 1 end)  jul_12_encs,
count(case when  index_date = '2012-08-01' and enc_type = 'encounter' then 1 end)  aug_12_encs,
count(case when  index_date = '2012-09-01' and enc_type = 'encounter' then 1 end)  sep_12_encs,
count(case when  index_date = '2012-10-01' and enc_type = 'encounter' then 1 end)  oct_12_encs,
count(case when  index_date = '2012-11-01' and enc_type = 'encounter' then 1 end)  nov_12_encs,
count(case when  index_date = '2012-12-01' and enc_type = 'encounter' then 1 end)  dec_12_encs,
count(case when  index_date = '2013-01-01' and enc_type = 'encounter' then 1 end)  jan_13_encs,
count(case when  index_date = '2013-02-01' and enc_type = 'encounter' then 1 end)  feb_13_encs,
count(case when  index_date = '2013-03-01' and enc_type = 'encounter' then 1 end)  mar_13_encs,
count(case when  index_date = '2013-04-01' and enc_type = 'encounter' then 1 end)  apr_13_encs,
count(case when  index_date = '2013-05-01' and enc_type = 'encounter' then 1 end)  may_13_encs,
count(case when  index_date = '2013-06-01' and enc_type = 'encounter' then 1 end)  jun_13_encs,
count(case when  index_date = '2013-07-01' and enc_type = 'encounter' then 1 end)  jul_13_encs,
count(case when  index_date = '2013-08-01' and enc_type = 'encounter' then 1 end)  aug_13_encs,
count(case when  index_date = '2013-09-01' and enc_type = 'encounter' then 1 end)  sep_13_encs,
count(case when  index_date = '2013-10-01' and enc_type = 'encounter' then 1 end)  oct_13_encs,
count(case when  index_date = '2013-11-01' and enc_type = 'encounter' then 1 end)  nov_13_encs,
count(case when  index_date = '2013-12-01' and enc_type = 'encounter' then 1 end)  dec_13_encs,
count(case when  index_date = '2014-01-01' and enc_type = 'encounter' then 1 end)  jan_14_encs,
count(case when  index_date = '2014-02-01' and enc_type = 'encounter' then 1 end)  feb_14_encs,
count(case when  index_date = '2014-03-01' and enc_type = 'encounter' then 1 end)  mar_14_encs,
count(case when  index_date = '2014-04-01' and enc_type = 'encounter' then 1 end)  apr_14_encs,
count(case when  index_date = '2014-05-01' and enc_type = 'encounter' then 1 end)  may_14_encs,
count(case when  index_date = '2014-06-01' and enc_type = 'encounter' then 1 end)  jun_14_encs,
count(case when  index_date = '2014-07-01' and enc_type = 'encounter' then 1 end)  jul_14_encs,
count(case when  index_date = '2014-08-01' and enc_type = 'encounter' then 1 end)  aug_14_encs,
count(case when  index_date = '2014-09-01' and enc_type = 'encounter' then 1 end)  sep_14_encs,
count(case when  index_date = '2014-10-01' and enc_type = 'encounter' then 1 end)  oct_14_encs,
count(case when  index_date = '2014-11-01' and enc_type = 'encounter' then 1 end)  nov_14_encs,
count(case when  index_date = '2014-12-01' and enc_type = 'encounter' then 1 end)  dec_14_encs,
count(case when  index_date = '2015-01-01' and enc_type = 'encounter' then 1 end)  jan_15_encs,
count(case when  index_date = '2015-02-01' and enc_type = 'encounter' then 1 end)  feb_15_encs,
count(case when  index_date = '2015-03-01' and enc_type = 'encounter' then 1 end)  mar_15_encs,
count(case when  index_date = '2015-04-01' and enc_type = 'encounter' then 1 end)  apr_15_encs,
count(case when  index_date = '2015-05-01' and enc_type = 'encounter' then 1 end)  may_15_encs,
count(case when  index_date = '2015-06-01' and enc_type = 'encounter' then 1 end)  jun_15_encs,
count(case when  index_date = '2015-07-01' and enc_type = 'encounter' then 1 end)  jul_15_encs,
count(case when  index_date = '2015-08-01' and enc_type = 'encounter' then 1 end)  aug_15_encs,
count(case when  index_date = '2015-09-01' and enc_type = 'encounter' then 1 end)  sep_15_encs,
count(case when  index_date = '2015-10-01' and enc_type = 'encounter' then 1 end)  oct_15_encs,
count(case when  index_date = '2015-11-01' and enc_type = 'encounter' then 1 end)  nov_15_encs,
count(case when  index_date = '2015-12-01' and enc_type = 'encounter' then 1 end)  dec_15_encs,
count(case when  index_date = '2016-01-01' and enc_type = 'encounter' then 1 end)  jan_16_encs,
count(case when  index_date = '2016-02-01' and enc_type = 'encounter' then 1 end)  feb_16_encs,
count(case when  index_date = '2016-03-01' and enc_type = 'encounter' then 1 end)  mar_16_encs,
count(case when  index_date = '2016-04-01' and enc_type = 'encounter' then 1 end)  apr_16_encs,
count(case when  index_date = '2016-05-01' and enc_type = 'encounter' then 1 end)  may_16_encs,
count(case when  index_date = '2016-06-01' and enc_type = 'encounter' then 1 end)  jun_16_encs,
count(case when  index_date = '2016-07-01' and enc_type = 'encounter' then 1 end)  jul_16_encs,
count(case when  index_date = '2016-08-01' and enc_type = 'encounter' then 1 end)  aug_16_encs,
count(case when  index_date = '2016-09-01' and enc_type = 'encounter' then 1 end)  sep_16_encs,
count(case when  index_date = '2016-10-01' and enc_type = 'encounter' then 1 end)  oct_16_encs,
count(case when  index_date = '2016-11-01' and enc_type = 'encounter' then 1 end)  nov_16_encs,
count(case when  index_date = '2016-12-01' and enc_type = 'encounter' then 1 end)  dec_16_encs,
count(case when  index_date = '2017-01-01' and enc_type = 'encounter' then 1 end)  jan_17_encs,
count(case when  index_date = '2017-02-01' and enc_type = 'encounter' then 1 end)  feb_17_encs,
count(case when  index_date = '2017-03-01' and enc_type = 'encounter' then 1 end)  mar_17_encs,
count(case when  index_date = '2017-04-01' and enc_type = 'encounter' then 1 end)  apr_17_encs,
count(case when  index_date = '2017-05-01' and enc_type = 'encounter' then 1 end)  may_17_encs,
count(case when  index_date = '2017-06-01' and enc_type = 'encounter' then 1 end)  jun_17_encs,
count(case when  index_date = '2012-01-01' and enc_type = 'iud_dx' then 1 end)  jan_12_iud_dx,
count(case when  index_date = '2012-02-01' and enc_type = 'iud_dx' then 1 end)  feb_12_iud_dx,
count(case when  index_date = '2012-03-01' and enc_type = 'iud_dx' then 1 end)  mar_12_iud_dx,
count(case when  index_date = '2012-04-01' and enc_type = 'iud_dx' then 1 end)  apr_12_iud_dx,
count(case when  index_date = '2012-05-01' and enc_type = 'iud_dx' then 1 end)  may_12_iud_dx,
count(case when  index_date = '2012-06-01' and enc_type = 'iud_dx' then 1 end)  jun_12_iud_dx,
count(case when  index_date = '2012-07-01' and enc_type = 'iud_dx' then 1 end)  jul_12_iud_dx,
count(case when  index_date = '2012-08-01' and enc_type = 'iud_dx' then 1 end)  aug_12_iud_dx,
count(case when  index_date = '2012-09-01' and enc_type = 'iud_dx' then 1 end)  sep_12_iud_dx,
count(case when  index_date = '2012-10-01' and enc_type = 'iud_dx' then 1 end)  oct_12_iud_dx,
count(case when  index_date = '2012-11-01' and enc_type = 'iud_dx' then 1 end)  nov_12_iud_dx,
count(case when  index_date = '2012-12-01' and enc_type = 'iud_dx' then 1 end)  dec_12_iud_dx,
count(case when  index_date = '2013-01-01' and enc_type = 'iud_dx' then 1 end)  jan_13_iud_dx,
count(case when  index_date = '2013-02-01' and enc_type = 'iud_dx' then 1 end)  feb_13_iud_dx,
count(case when  index_date = '2013-03-01' and enc_type = 'iud_dx' then 1 end)  mar_13_iud_dx,
count(case when  index_date = '2013-04-01' and enc_type = 'iud_dx' then 1 end)  apr_13_iud_dx,
count(case when  index_date = '2013-05-01' and enc_type = 'iud_dx' then 1 end)  may_13_iud_dx,
count(case when  index_date = '2013-06-01' and enc_type = 'iud_dx' then 1 end)  jun_13_iud_dx,
count(case when  index_date = '2013-07-01' and enc_type = 'iud_dx' then 1 end)  jul_13_iud_dx,
count(case when  index_date = '2013-08-01' and enc_type = 'iud_dx' then 1 end)  aug_13_iud_dx,
count(case when  index_date = '2013-09-01' and enc_type = 'iud_dx' then 1 end)  sep_13_iud_dx,
count(case when  index_date = '2013-10-01' and enc_type = 'iud_dx' then 1 end)  oct_13_iud_dx,
count(case when  index_date = '2013-11-01' and enc_type = 'iud_dx' then 1 end)  nov_13_iud_dx,
count(case when  index_date = '2013-12-01' and enc_type = 'iud_dx' then 1 end)  dec_13_iud_dx,
count(case when  index_date = '2014-01-01' and enc_type = 'iud_dx' then 1 end)  jan_14_iud_dx,
count(case when  index_date = '2014-02-01' and enc_type = 'iud_dx' then 1 end)  feb_14_iud_dx,
count(case when  index_date = '2014-03-01' and enc_type = 'iud_dx' then 1 end)  mar_14_iud_dx,
count(case when  index_date = '2014-04-01' and enc_type = 'iud_dx' then 1 end)  apr_14_iud_dx,
count(case when  index_date = '2014-05-01' and enc_type = 'iud_dx' then 1 end)  may_14_iud_dx,
count(case when  index_date = '2014-06-01' and enc_type = 'iud_dx' then 1 end)  jun_14_iud_dx,
count(case when  index_date = '2014-07-01' and enc_type = 'iud_dx' then 1 end)  jul_14_iud_dx,
count(case when  index_date = '2014-08-01' and enc_type = 'iud_dx' then 1 end)  aug_14_iud_dx,
count(case when  index_date = '2014-09-01' and enc_type = 'iud_dx' then 1 end)  sep_14_iud_dx,
count(case when  index_date = '2014-10-01' and enc_type = 'iud_dx' then 1 end)  oct_14_iud_dx,
count(case when  index_date = '2014-11-01' and enc_type = 'iud_dx' then 1 end)  nov_14_iud_dx,
count(case when  index_date = '2014-12-01' and enc_type = 'iud_dx' then 1 end)  dec_14_iud_dx,
count(case when  index_date = '2015-01-01' and enc_type = 'iud_dx' then 1 end)  jan_15_iud_dx,
count(case when  index_date = '2015-02-01' and enc_type = 'iud_dx' then 1 end)  feb_15_iud_dx,
count(case when  index_date = '2015-03-01' and enc_type = 'iud_dx' then 1 end)  mar_15_iud_dx,
count(case when  index_date = '2015-04-01' and enc_type = 'iud_dx' then 1 end)  apr_15_iud_dx,
count(case when  index_date = '2015-05-01' and enc_type = 'iud_dx' then 1 end)  may_15_iud_dx,
count(case when  index_date = '2015-06-01' and enc_type = 'iud_dx' then 1 end)  jun_15_iud_dx,
count(case when  index_date = '2015-07-01' and enc_type = 'iud_dx' then 1 end)  jul_15_iud_dx,
count(case when  index_date = '2015-08-01' and enc_type = 'iud_dx' then 1 end)  aug_15_iud_dx,
count(case when  index_date = '2015-09-01' and enc_type = 'iud_dx' then 1 end)  sep_15_iud_dx,
count(case when  index_date = '2015-10-01' and enc_type = 'iud_dx' then 1 end)  oct_15_iud_dx,
count(case when  index_date = '2015-11-01' and enc_type = 'iud_dx' then 1 end)  nov_15_iud_dx,
count(case when  index_date = '2015-12-01' and enc_type = 'iud_dx' then 1 end)  dec_15_iud_dx,
count(case when  index_date = '2016-01-01' and enc_type = 'iud_dx' then 1 end)  jan_16_iud_dx,
count(case when  index_date = '2016-02-01' and enc_type = 'iud_dx' then 1 end)  feb_16_iud_dx,
count(case when  index_date = '2016-03-01' and enc_type = 'iud_dx' then 1 end)  mar_16_iud_dx,
count(case when  index_date = '2016-04-01' and enc_type = 'iud_dx' then 1 end)  apr_16_iud_dx,
count(case when  index_date = '2016-05-01' and enc_type = 'iud_dx' then 1 end)  may_16_iud_dx,
count(case when  index_date = '2016-06-01' and enc_type = 'iud_dx' then 1 end)  jun_16_iud_dx,
count(case when  index_date = '2016-07-01' and enc_type = 'iud_dx' then 1 end)  jul_16_iud_dx,
count(case when  index_date = '2016-08-01' and enc_type = 'iud_dx' then 1 end)  aug_16_iud_dx,
count(case when  index_date = '2016-09-01' and enc_type = 'iud_dx' then 1 end)  sep_16_iud_dx,
count(case when  index_date = '2016-10-01' and enc_type = 'iud_dx' then 1 end)  oct_16_iud_dx,
count(case when  index_date = '2016-11-01' and enc_type = 'iud_dx' then 1 end)  nov_16_iud_dx,
count(case when  index_date = '2016-12-01' and enc_type = 'iud_dx' then 1 end)  dec_16_iud_dx,
count(case when  index_date = '2017-01-01' and enc_type = 'iud_dx' then 1 end)  jan_17_iud_dx,
count(case when  index_date = '2017-02-01' and enc_type = 'iud_dx' then 1 end)  feb_17_iud_dx,
count(case when  index_date = '2017-03-01' and enc_type = 'iud_dx' then 1 end)  mar_17_iud_dx,
count(case when  index_date = '2017-04-01' and enc_type = 'iud_dx' then 1 end)  apr_17_iud_dx,
count(case when  index_date = '2017-05-01' and enc_type = 'iud_dx' then 1 end)  may_17_iud_dx,
count(case when  index_date = '2017-06-01' and enc_type = 'iud_dx' then 1 end)  jun_17_iud_dx,


-- count(case when  index_date = '2015-01-01' and enc_type = 'implant_dx' then 1 end)  jan_15_implant_dx,
-- count(case when  index_date = '2015-02-01' and enc_type = 'implant_dx' then 1 end)  feb_15_implant_dx,
-- count(case when  index_date = '2015-03-01' and enc_type = 'implant_dx' then 1 end)  mar_15_implant_dx,
-- count(case when  index_date = '2015-04-01' and enc_type = 'implant_dx' then 1 end)  apr_15_implant_dx,
-- count(case when  index_date = '2015-05-01' and enc_type = 'implant_dx' then 1 end)  may_15_implant_dx,
-- count(case when  index_date = '2015-06-01' and enc_type = 'implant_dx' then 1 end)  jun_15_implant_dx,
-- count(case when  index_date = '2015-07-01' and enc_type = 'implant_dx' then 1 end)  jul_15_implant_dx,
-- count(case when  index_date = '2015-08-01' and enc_type = 'implant_dx' then 1 end)  aug_15_implant_dx,
-- count(case when  index_date = '2015-09-01' and enc_type = 'implant_dx' then 1 end)  sep_15_implant_dx,
-- count(case when  index_date = '2015-10-01' and enc_type = 'implant_dx' then 1 end)  oct_15_implant_dx,
-- count(case when  index_date = '2015-11-01' and enc_type = 'implant_dx' then 1 end)  nov_15_implant_dx,
-- count(case when  index_date = '2015-12-01' and enc_type = 'implant_dx' then 1 end)  dec_15_implant_dx,
-- count(case when  index_date = '2016-01-01' and enc_type = 'implant_dx' then 1 end)  jan_16_implant_dx,
-- count(case when  index_date = '2016-02-01' and enc_type = 'implant_dx' then 1 end)  feb_16_implant_dx,
-- count(case when  index_date = '2016-03-01' and enc_type = 'implant_dx' then 1 end)  mar_16_implant_dx,
-- count(case when  index_date = '2016-04-01' and enc_type = 'implant_dx' then 1 end)  apr_16_implant_dx,
-- count(case when  index_date = '2016-05-01' and enc_type = 'implant_dx' then 1 end)  may_16_implant_dx,
-- count(case when  index_date = '2016-06-01' and enc_type = 'implant_dx' then 1 end)  jun_16_implant_dx,
-- count(case when  index_date = '2016-07-01' and enc_type = 'implant_dx' then 1 end)  jul_16_implant_dx,
-- count(case when  index_date = '2016-08-01' and enc_type = 'implant_dx' then 1 end)  aug_16_implant_dx,
-- count(case when  index_date = '2016-09-01' and enc_type = 'implant_dx' then 1 end)  sep_16_implant_dx,
-- count(case when  index_date = '2016-10-01' and enc_type = 'implant_dx' then 1 end)  oct_16_implant_dx,
-- count(case when  index_date = '2016-11-01' and enc_type = 'implant_dx' then 1 end)  nov_16_implant_dx,
-- count(case when  index_date = '2016-12-01' and enc_type = 'implant_dx' then 1 end)  dec_16_implant_dx,
-- count(case when  index_date = '2017-01-01' and enc_type = 'implant_dx' then 1 end)  jan_17_implant_dx,
-- count(case when  index_date = '2017-02-01' and enc_type = 'implant_dx' then 1 end)  feb_17_implant_dx,
-- count(case when  index_date = '2015-01-01' and enc_type = 'cm_dx' then 1 end)  jan_15_cm_dx,
-- count(case when  index_date = '2015-02-01' and enc_type = 'cm_dx' then 1 end)  feb_15_cm_dx,
-- count(case when  index_date = '2015-03-01' and enc_type = 'cm_dx' then 1 end)  mar_15_cm_dx,
-- count(case when  index_date = '2015-04-01' and enc_type = 'cm_dx' then 1 end)  apr_15_cm_dx,
-- count(case when  index_date = '2015-05-01' and enc_type = 'cm_dx' then 1 end)  may_15_cm_dx,
-- count(case when  index_date = '2015-06-01' and enc_type = 'cm_dx' then 1 end)  jun_15_cm_dx,
-- count(case when  index_date = '2015-07-01' and enc_type = 'cm_dx' then 1 end)  jul_15_cm_dx,
-- count(case when  index_date = '2015-08-01' and enc_type = 'cm_dx' then 1 end)  aug_15_cm_dx,
-- count(case when  index_date = '2015-09-01' and enc_type = 'cm_dx' then 1 end)  sep_15_cm_dx,
-- count(case when  index_date = '2015-10-01' and enc_type = 'cm_dx' then 1 end)  oct_15_cm_dx,
-- count(case when  index_date = '2015-11-01' and enc_type = 'cm_dx' then 1 end)  nov_15_cm_dx,
-- count(case when  index_date = '2015-12-01' and enc_type = 'cm_dx' then 1 end)  dec_15_cm_dx,
-- count(case when  index_date = '2016-01-01' and enc_type = 'cm_dx' then 1 end)  jan_16_cm_dx,
-- count(case when  index_date = '2016-02-01' and enc_type = 'cm_dx' then 1 end)  feb_16_cm_dx,
-- count(case when  index_date = '2016-03-01' and enc_type = 'cm_dx' then 1 end)  mar_16_cm_dx,
-- count(case when  index_date = '2016-04-01' and enc_type = 'cm_dx' then 1 end)  apr_16_cm_dx,
-- count(case when  index_date = '2016-05-01' and enc_type = 'cm_dx' then 1 end)  may_16_cm_dx,
-- count(case when  index_date = '2016-06-01' and enc_type = 'cm_dx' then 1 end)  jun_16_cm_dx,
-- count(case when  index_date = '2016-07-01' and enc_type = 'cm_dx' then 1 end)  jul_16_cm_dx,
-- count(case when  index_date = '2016-08-01' and enc_type = 'cm_dx' then 1 end)  aug_16_cm_dx,
-- count(case when  index_date = '2016-09-01' and enc_type = 'cm_dx' then 1 end)  sep_16_cm_dx,
-- count(case when  index_date = '2016-10-01' and enc_type = 'cm_dx' then 1 end)  oct_16_cm_dx,
-- count(case when  index_date = '2016-11-01' and enc_type = 'cm_dx' then 1 end)  nov_16_cm_dx,
-- count(case when  index_date = '2016-12-01' and enc_type = 'cm_dx' then 1 end)  dec_16_cm_dx,
-- count(case when  index_date = '2017-01-01' and enc_type = 'cm_dx' then 1 end)  jan_17_cm_dx,
-- count(case when  index_date = '2017-02-01' and enc_type = 'cm_dx' then 1 end)  feb_17_cm_dx,

count(case when  index_date = '2012-01-01' and enc_type = 'all_rx' then 1 end)  jan_12_all_rx,
count(case when  index_date = '2012-02-01' and enc_type = 'all_rx' then 1 end)  feb_12_all_rx,
count(case when  index_date = '2012-03-01' and enc_type = 'all_rx' then 1 end)  mar_12_all_rx,
count(case when  index_date = '2012-04-01' and enc_type = 'all_rx' then 1 end)  apr_12_all_rx,
count(case when  index_date = '2012-05-01' and enc_type = 'all_rx' then 1 end)  may_12_all_rx,
count(case when  index_date = '2012-06-01' and enc_type = 'all_rx' then 1 end)  jun_12_all_rx,
count(case when  index_date = '2012-07-01' and enc_type = 'all_rx' then 1 end)  jul_12_all_rx,
count(case when  index_date = '2012-08-01' and enc_type = 'all_rx' then 1 end)  aug_12_all_rx,
count(case when  index_date = '2012-09-01' and enc_type = 'all_rx' then 1 end)  sep_12_all_rx,
count(case when  index_date = '2012-10-01' and enc_type = 'all_rx' then 1 end)  oct_12_all_rx,
count(case when  index_date = '2012-11-01' and enc_type = 'all_rx' then 1 end)  nov_12_all_rx,
count(case when  index_date = '2012-12-01' and enc_type = 'all_rx' then 1 end)  dec_12_all_rx,
count(case when  index_date = '2013-01-01' and enc_type = 'all_rx' then 1 end)  jan_13_all_rx,
count(case when  index_date = '2013-02-01' and enc_type = 'all_rx' then 1 end)  feb_13_all_rx,
count(case when  index_date = '2013-03-01' and enc_type = 'all_rx' then 1 end)  mar_13_all_rx,
count(case when  index_date = '2013-04-01' and enc_type = 'all_rx' then 1 end)  apr_13_all_rx,
count(case when  index_date = '2013-05-01' and enc_type = 'all_rx' then 1 end)  may_13_all_rx,
count(case when  index_date = '2013-06-01' and enc_type = 'all_rx' then 1 end)  jun_13_all_rx,
count(case when  index_date = '2013-07-01' and enc_type = 'all_rx' then 1 end)  jul_13_all_rx,
count(case when  index_date = '2013-08-01' and enc_type = 'all_rx' then 1 end)  aug_13_all_rx,
count(case when  index_date = '2013-09-01' and enc_type = 'all_rx' then 1 end)  sep_13_all_rx,
count(case when  index_date = '2013-10-01' and enc_type = 'all_rx' then 1 end)  oct_13_all_rx,
count(case when  index_date = '2013-11-01' and enc_type = 'all_rx' then 1 end)  nov_13_all_rx,
count(case when  index_date = '2013-12-01' and enc_type = 'all_rx' then 1 end)  dec_13_all_rx,
count(case when  index_date = '2014-01-01' and enc_type = 'all_rx' then 1 end)  jan_14_all_rx,
count(case when  index_date = '2014-02-01' and enc_type = 'all_rx' then 1 end)  feb_14_all_rx,
count(case when  index_date = '2014-03-01' and enc_type = 'all_rx' then 1 end)  mar_14_all_rx,
count(case when  index_date = '2014-04-01' and enc_type = 'all_rx' then 1 end)  apr_14_all_rx,
count(case when  index_date = '2014-05-01' and enc_type = 'all_rx' then 1 end)  may_14_all_rx,
count(case when  index_date = '2014-06-01' and enc_type = 'all_rx' then 1 end)  jun_14_all_rx,
count(case when  index_date = '2014-07-01' and enc_type = 'all_rx' then 1 end)  jul_14_all_rx,
count(case when  index_date = '2014-08-01' and enc_type = 'all_rx' then 1 end)  aug_14_all_rx,
count(case when  index_date = '2014-09-01' and enc_type = 'all_rx' then 1 end)  sep_14_all_rx,
count(case when  index_date = '2014-10-01' and enc_type = 'all_rx' then 1 end)  oct_14_all_rx,
count(case when  index_date = '2014-11-01' and enc_type = 'all_rx' then 1 end)  nov_14_all_rx,
count(case when  index_date = '2014-12-01' and enc_type = 'all_rx' then 1 end)  dec_14_all_rx,
count(case when  index_date = '2015-01-01' and enc_type = 'all_rx' then 1 end)  jan_15_all_rx,
count(case when  index_date = '2015-02-01' and enc_type = 'all_rx' then 1 end)  feb_15_all_rx,
count(case when  index_date = '2015-03-01' and enc_type = 'all_rx' then 1 end)  mar_15_all_rx,
count(case when  index_date = '2015-04-01' and enc_type = 'all_rx' then 1 end)  apr_15_all_rx,
count(case when  index_date = '2015-05-01' and enc_type = 'all_rx' then 1 end)  may_15_all_rx,
count(case when  index_date = '2015-06-01' and enc_type = 'all_rx' then 1 end)  jun_15_all_rx,
count(case when  index_date = '2015-07-01' and enc_type = 'all_rx' then 1 end)  jul_15_all_rx,
count(case when  index_date = '2015-08-01' and enc_type = 'all_rx' then 1 end)  aug_15_all_rx,
count(case when  index_date = '2015-09-01' and enc_type = 'all_rx' then 1 end)  sep_15_all_rx,
count(case when  index_date = '2015-10-01' and enc_type = 'all_rx' then 1 end)  oct_15_all_rx,
count(case when  index_date = '2015-11-01' and enc_type = 'all_rx' then 1 end)  nov_15_all_rx,
count(case when  index_date = '2015-12-01' and enc_type = 'all_rx' then 1 end)  dec_15_all_rx,
count(case when  index_date = '2016-01-01' and enc_type = 'all_rx' then 1 end)  jan_16_all_rx,
count(case when  index_date = '2016-02-01' and enc_type = 'all_rx' then 1 end)  feb_16_all_rx,
count(case when  index_date = '2016-03-01' and enc_type = 'all_rx' then 1 end)  mar_16_all_rx,
count(case when  index_date = '2016-04-01' and enc_type = 'all_rx' then 1 end)  apr_16_all_rx,
count(case when  index_date = '2016-05-01' and enc_type = 'all_rx' then 1 end)  may_16_all_rx,
count(case when  index_date = '2016-06-01' and enc_type = 'all_rx' then 1 end)  jun_16_all_rx,
count(case when  index_date = '2016-07-01' and enc_type = 'all_rx' then 1 end)  jul_16_all_rx,
count(case when  index_date = '2016-08-01' and enc_type = 'all_rx' then 1 end)  aug_16_all_rx,
count(case when  index_date = '2016-09-01' and enc_type = 'all_rx' then 1 end)  sep_16_all_rx,
count(case when  index_date = '2016-10-01' and enc_type = 'all_rx' then 1 end)  oct_16_all_rx,
count(case when  index_date = '2016-11-01' and enc_type = 'all_rx' then 1 end)  nov_16_all_rx,
count(case when  index_date = '2016-12-01' and enc_type = 'all_rx' then 1 end)  dec_16_all_rx,
count(case when  index_date = '2017-01-01' and enc_type = 'all_rx' then 1 end)  jan_17_all_rx,
count(case when  index_date = '2017-02-01' and enc_type = 'all_rx' then 1 end)  feb_17_all_rx,
count(case when  index_date = '2017-03-01' and enc_type = 'all_rx' then 1 end)  mar_17_all_rx,
count(case when  index_date = '2017-04-01' and enc_type = 'all_rx' then 1 end)  apr_17_all_rx,
count(case when  index_date = '2017-05-01' and enc_type = 'all_rx' then 1 end)  may_17_all_rx,
count(case when  index_date = '2017-06-01' and enc_type = 'all_rx' then 1 end)  jun_17_all_rx,

count(case when  index_date = '2012-01-01' and enc_type = 'oral_rx' then 1 end)  jan_12_oral_rx,
count(case when  index_date = '2012-02-01' and enc_type = 'oral_rx' then 1 end)  feb_12_oral_rx,
count(case when  index_date = '2012-03-01' and enc_type = 'oral_rx' then 1 end)  mar_12_oral_rx,
count(case when  index_date = '2012-04-01' and enc_type = 'oral_rx' then 1 end)  apr_12_oral_rx,
count(case when  index_date = '2012-05-01' and enc_type = 'oral_rx' then 1 end)  may_12_oral_rx,
count(case when  index_date = '2012-06-01' and enc_type = 'oral_rx' then 1 end)  jun_12_oral_rx,
count(case when  index_date = '2012-07-01' and enc_type = 'oral_rx' then 1 end)  jul_12_oral_rx,
count(case when  index_date = '2012-08-01' and enc_type = 'oral_rx' then 1 end)  aug_12_oral_rx,
count(case when  index_date = '2012-09-01' and enc_type = 'oral_rx' then 1 end)  sep_12_oral_rx,
count(case when  index_date = '2012-10-01' and enc_type = 'oral_rx' then 1 end)  oct_12_oral_rx,
count(case when  index_date = '2012-11-01' and enc_type = 'oral_rx' then 1 end)  nov_12_oral_rx,
count(case when  index_date = '2012-12-01' and enc_type = 'oral_rx' then 1 end)  dec_12_oral_rx,
count(case when  index_date = '2013-01-01' and enc_type = 'oral_rx' then 1 end)  jan_13_oral_rx,
count(case when  index_date = '2013-02-01' and enc_type = 'oral_rx' then 1 end)  feb_13_oral_rx,
count(case when  index_date = '2013-03-01' and enc_type = 'oral_rx' then 1 end)  mar_13_oral_rx,
count(case when  index_date = '2013-04-01' and enc_type = 'oral_rx' then 1 end)  apr_13_oral_rx,
count(case when  index_date = '2013-05-01' and enc_type = 'oral_rx' then 1 end)  may_13_oral_rx,
count(case when  index_date = '2013-06-01' and enc_type = 'oral_rx' then 1 end)  jun_13_oral_rx,
count(case when  index_date = '2013-07-01' and enc_type = 'oral_rx' then 1 end)  jul_13_oral_rx,
count(case when  index_date = '2013-08-01' and enc_type = 'oral_rx' then 1 end)  aug_13_oral_rx,
count(case when  index_date = '2013-09-01' and enc_type = 'oral_rx' then 1 end)  sep_13_oral_rx,
count(case when  index_date = '2013-10-01' and enc_type = 'oral_rx' then 1 end)  oct_13_oral_rx,
count(case when  index_date = '2013-11-01' and enc_type = 'oral_rx' then 1 end)  nov_13_oral_rx,
count(case when  index_date = '2013-12-01' and enc_type = 'oral_rx' then 1 end)  dec_13_oral_rx,
count(case when  index_date = '2014-01-01' and enc_type = 'oral_rx' then 1 end)  jan_14_oral_rx,
count(case when  index_date = '2014-02-01' and enc_type = 'oral_rx' then 1 end)  feb_14_oral_rx,
count(case when  index_date = '2014-03-01' and enc_type = 'oral_rx' then 1 end)  mar_14_oral_rx,
count(case when  index_date = '2014-04-01' and enc_type = 'oral_rx' then 1 end)  apr_14_oral_rx,
count(case when  index_date = '2014-05-01' and enc_type = 'oral_rx' then 1 end)  may_14_oral_rx,
count(case when  index_date = '2014-06-01' and enc_type = 'oral_rx' then 1 end)  jun_14_oral_rx,
count(case when  index_date = '2014-07-01' and enc_type = 'oral_rx' then 1 end)  jul_14_oral_rx,
count(case when  index_date = '2014-08-01' and enc_type = 'oral_rx' then 1 end)  aug_14_oral_rx,
count(case when  index_date = '2014-09-01' and enc_type = 'oral_rx' then 1 end)  sep_14_oral_rx,
count(case when  index_date = '2014-10-01' and enc_type = 'oral_rx' then 1 end)  oct_14_oral_rx,
count(case when  index_date = '2014-11-01' and enc_type = 'oral_rx' then 1 end)  nov_14_oral_rx,
count(case when  index_date = '2014-12-01' and enc_type = 'oral_rx' then 1 end)  dec_14_oral_rx,
count(case when  index_date = '2015-01-01' and enc_type = 'oral_rx' then 1 end)  jan_15_oral_rx,
count(case when  index_date = '2015-02-01' and enc_type = 'oral_rx' then 1 end)  feb_15_oral_rx,
count(case when  index_date = '2015-03-01' and enc_type = 'oral_rx' then 1 end)  mar_15_oral_rx,
count(case when  index_date = '2015-04-01' and enc_type = 'oral_rx' then 1 end)  apr_15_oral_rx,
count(case when  index_date = '2015-05-01' and enc_type = 'oral_rx' then 1 end)  may_15_oral_rx,
count(case when  index_date = '2015-06-01' and enc_type = 'oral_rx' then 1 end)  jun_15_oral_rx,
count(case when  index_date = '2015-07-01' and enc_type = 'oral_rx' then 1 end)  jul_15_oral_rx,
count(case when  index_date = '2015-08-01' and enc_type = 'oral_rx' then 1 end)  aug_15_oral_rx,
count(case when  index_date = '2015-09-01' and enc_type = 'oral_rx' then 1 end)  sep_15_oral_rx,
count(case when  index_date = '2015-10-01' and enc_type = 'oral_rx' then 1 end)  oct_15_oral_rx,
count(case when  index_date = '2015-11-01' and enc_type = 'oral_rx' then 1 end)  nov_15_oral_rx,
count(case when  index_date = '2015-12-01' and enc_type = 'oral_rx' then 1 end)  dec_15_oral_rx,
count(case when  index_date = '2016-01-01' and enc_type = 'oral_rx' then 1 end)  jan_16_oral_rx,
count(case when  index_date = '2016-02-01' and enc_type = 'oral_rx' then 1 end)  feb_16_oral_rx,
count(case when  index_date = '2016-03-01' and enc_type = 'oral_rx' then 1 end)  mar_16_oral_rx,
count(case when  index_date = '2016-04-01' and enc_type = 'oral_rx' then 1 end)  apr_16_oral_rx,
count(case when  index_date = '2016-05-01' and enc_type = 'oral_rx' then 1 end)  may_16_oral_rx,
count(case when  index_date = '2016-06-01' and enc_type = 'oral_rx' then 1 end)  jun_16_oral_rx,
count(case when  index_date = '2016-07-01' and enc_type = 'oral_rx' then 1 end)  jul_16_oral_rx,
count(case when  index_date = '2016-08-01' and enc_type = 'oral_rx' then 1 end)  aug_16_oral_rx,
count(case when  index_date = '2016-09-01' and enc_type = 'oral_rx' then 1 end)  sep_16_oral_rx,
count(case when  index_date = '2016-10-01' and enc_type = 'oral_rx' then 1 end)  oct_16_oral_rx,
count(case when  index_date = '2016-11-01' and enc_type = 'oral_rx' then 1 end)  nov_16_oral_rx,
count(case when  index_date = '2016-12-01' and enc_type = 'oral_rx' then 1 end)  dec_16_oral_rx,
count(case when  index_date = '2017-01-01' and enc_type = 'oral_rx' then 1 end)  jan_17_oral_rx,
count(case when  index_date = '2017-02-01' and enc_type = 'oral_rx' then 1 end)  feb_17_oral_rx,
count(case when  index_date = '2017-03-01' and enc_type = 'oral_rx' then 1 end)  mar_17_oral_rx,
count(case when  index_date = '2017-04-01' and enc_type = 'oral_rx' then 1 end)  apr_17_oral_rx,
count(case when  index_date = '2017-05-01' and enc_type = 'oral_rx' then 1 end)  may_17_oral_rx,
count(case when  index_date = '2017-06-01' and enc_type = 'oral_rx' then 1 end)  jun_17_oral_rx
-- count(case when  index_date = '2015-01-01' and enc_type = 'implant_rx' then 1 end)  jan_15_implant_rx,
-- count(case when  index_date = '2015-02-01' and enc_type = 'implant_rx' then 1 end)  feb_15_implant_rx,
-- count(case when  index_date = '2015-03-01' and enc_type = 'implant_rx' then 1 end)  mar_15_implant_rx,
-- count(case when  index_date = '2015-04-01' and enc_type = 'implant_rx' then 1 end)  apr_15_implant_rx,
-- count(case when  index_date = '2015-05-01' and enc_type = 'implant_rx' then 1 end)  may_15_implant_rx,
-- count(case when  index_date = '2015-06-01' and enc_type = 'implant_rx' then 1 end)  jun_15_implant_rx,
-- count(case when  index_date = '2015-07-01' and enc_type = 'implant_rx' then 1 end)  jul_15_implant_rx,
-- count(case when  index_date = '2015-08-01' and enc_type = 'implant_rx' then 1 end)  aug_15_implant_rx,
-- count(case when  index_date = '2015-09-01' and enc_type = 'implant_rx' then 1 end)  sep_15_implant_rx,
-- count(case when  index_date = '2015-10-01' and enc_type = 'implant_rx' then 1 end)  oct_15_implant_rx,
-- count(case when  index_date = '2015-11-01' and enc_type = 'implant_rx' then 1 end)  nov_15_implant_rx,
-- count(case when  index_date = '2015-12-01' and enc_type = 'implant_rx' then 1 end)  dec_15_implant_rx,
-- count(case when  index_date = '2016-01-01' and enc_type = 'implant_rx' then 1 end)  jan_16_implant_rx,
-- count(case when  index_date = '2016-02-01' and enc_type = 'implant_rx' then 1 end)  feb_16_implant_rx,
-- count(case when  index_date = '2016-03-01' and enc_type = 'implant_rx' then 1 end)  mar_16_implant_rx,
-- count(case when  index_date = '2016-04-01' and enc_type = 'implant_rx' then 1 end)  apr_16_implant_rx,
-- count(case when  index_date = '2016-05-01' and enc_type = 'implant_rx' then 1 end)  may_16_implant_rx,
-- count(case when  index_date = '2016-06-01' and enc_type = 'implant_rx' then 1 end)  jun_16_implant_rx,
-- count(case when  index_date = '2016-07-01' and enc_type = 'implant_rx' then 1 end)  jul_16_implant_rx,
-- count(case when  index_date = '2016-08-01' and enc_type = 'implant_rx' then 1 end)  aug_16_implant_rx,
-- count(case when  index_date = '2016-09-01' and enc_type = 'implant_rx' then 1 end)  sep_16_implant_rx,
-- count(case when  index_date = '2016-10-01' and enc_type = 'implant_rx' then 1 end)  oct_16_implant_rx,
-- count(case when  index_date = '2016-11-01' and enc_type = 'implant_rx' then 1 end)  nov_16_implant_rx,
-- count(case when  index_date = '2016-12-01' and enc_type = 'implant_rx' then 1 end)  dec_16_implant_rx,
-- count(case when  index_date = '2017-01-01' and enc_type = 'implant_rx' then 1 end)  jan_17_implant_rx,
-- count(case when  index_date = '2017-02-01' and enc_type = 'implant_rx' then 1 end)  feb_17_implant_rx,
-- count(case when  index_date = '2015-01-01' and enc_type = 'inject_rx' then 1 end)  jan_15_inject_rx,
-- count(case when  index_date = '2015-02-01' and enc_type = 'inject_rx' then 1 end)  feb_15_inject_rx,
-- count(case when  index_date = '2015-03-01' and enc_type = 'inject_rx' then 1 end)  mar_15_inject_rx,
-- count(case when  index_date = '2015-04-01' and enc_type = 'inject_rx' then 1 end)  apr_15_inject_rx,
-- count(case when  index_date = '2015-05-01' and enc_type = 'inject_rx' then 1 end)  may_15_inject_rx,
-- count(case when  index_date = '2015-06-01' and enc_type = 'inject_rx' then 1 end)  jun_15_inject_rx,
-- count(case when  index_date = '2015-07-01' and enc_type = 'inject_rx' then 1 end)  jul_15_inject_rx,
-- count(case when  index_date = '2015-08-01' and enc_type = 'inject_rx' then 1 end)  aug_15_inject_rx,
-- count(case when  index_date = '2015-09-01' and enc_type = 'inject_rx' then 1 end)  sep_15_inject_rx,
-- count(case when  index_date = '2015-10-01' and enc_type = 'inject_rx' then 1 end)  oct_15_inject_rx,
-- count(case when  index_date = '2015-11-01' and enc_type = 'inject_rx' then 1 end)  nov_15_inject_rx,
-- count(case when  index_date = '2015-12-01' and enc_type = 'inject_rx' then 1 end)  dec_15_inject_rx,
-- count(case when  index_date = '2016-01-01' and enc_type = 'inject_rx' then 1 end)  jan_16_inject_rx,
-- count(case when  index_date = '2016-02-01' and enc_type = 'inject_rx' then 1 end)  feb_16_inject_rx,
-- count(case when  index_date = '2016-03-01' and enc_type = 'inject_rx' then 1 end)  mar_16_inject_rx,
-- count(case when  index_date = '2016-04-01' and enc_type = 'inject_rx' then 1 end)  apr_16_inject_rx,
-- count(case when  index_date = '2016-05-01' and enc_type = 'inject_rx' then 1 end)  may_16_inject_rx,
-- count(case when  index_date = '2016-06-01' and enc_type = 'inject_rx' then 1 end)  jun_16_inject_rx,
-- count(case when  index_date = '2016-07-01' and enc_type = 'inject_rx' then 1 end)  jul_16_inject_rx,
-- count(case when  index_date = '2016-08-01' and enc_type = 'inject_rx' then 1 end)  aug_16_inject_rx,
-- count(case when  index_date = '2016-09-01' and enc_type = 'inject_rx' then 1 end)  sep_16_inject_rx,
-- count(case when  index_date = '2016-10-01' and enc_type = 'inject_rx' then 1 end)  oct_16_inject_rx,
-- count(case when  index_date = '2016-11-01' and enc_type = 'inject_rx' then 1 end)  nov_16_inject_rx,
-- count(case when  index_date = '2016-12-01' and enc_type = 'inject_rx' then 1 end)  dec_16_inject_rx,
-- count(case when  index_date = '2017-01-01' and enc_type = 'inject_rx' then 1 end)  jan_17_inject_rx,
-- count(case when  index_date = '2017-02-01' and enc_type = 'inject_rx' then 1 end)  feb_17_inject_rx,
-- count(case when  index_date = '2015-01-01' and enc_type = 'patch_rx' then 1 end)  jan_15_patch_rx,
-- count(case when  index_date = '2015-02-01' and enc_type = 'patch_rx' then 1 end)  feb_15_patch_rx,
-- count(case when  index_date = '2015-03-01' and enc_type = 'patch_rx' then 1 end)  mar_15_patch_rx,
-- count(case when  index_date = '2015-04-01' and enc_type = 'patch_rx' then 1 end)  apr_15_patch_rx,
-- count(case when  index_date = '2015-05-01' and enc_type = 'patch_rx' then 1 end)  may_15_patch_rx,
-- count(case when  index_date = '2015-06-01' and enc_type = 'patch_rx' then 1 end)  jun_15_patch_rx,
-- count(case when  index_date = '2015-07-01' and enc_type = 'patch_rx' then 1 end)  jul_15_patch_rx,
-- count(case when  index_date = '2015-08-01' and enc_type = 'patch_rx' then 1 end)  aug_15_patch_rx,
-- count(case when  index_date = '2015-09-01' and enc_type = 'patch_rx' then 1 end)  sep_15_patch_rx,
-- count(case when  index_date = '2015-10-01' and enc_type = 'patch_rx' then 1 end)  oct_15_patch_rx,
-- count(case when  index_date = '2015-11-01' and enc_type = 'patch_rx' then 1 end)  nov_15_patch_rx,
-- count(case when  index_date = '2015-12-01' and enc_type = 'patch_rx' then 1 end)  dec_15_patch_rx,
-- count(case when  index_date = '2016-01-01' and enc_type = 'patch_rx' then 1 end)  jan_16_patch_rx,
-- count(case when  index_date = '2016-02-01' and enc_type = 'patch_rx' then 1 end)  feb_16_patch_rx,
-- count(case when  index_date = '2016-03-01' and enc_type = 'patch_rx' then 1 end)  mar_16_patch_rx,
-- count(case when  index_date = '2016-04-01' and enc_type = 'patch_rx' then 1 end)  apr_16_patch_rx,
-- count(case when  index_date = '2016-05-01' and enc_type = 'patch_rx' then 1 end)  may_16_patch_rx,
-- count(case when  index_date = '2016-06-01' and enc_type = 'patch_rx' then 1 end)  jun_16_patch_rx,
-- count(case when  index_date = '2016-07-01' and enc_type = 'patch_rx' then 1 end)  jul_16_patch_rx,
-- count(case when  index_date = '2016-08-01' and enc_type = 'patch_rx' then 1 end)  aug_16_patch_rx,
-- count(case when  index_date = '2016-09-01' and enc_type = 'patch_rx' then 1 end)  sep_16_patch_rx,
-- count(case when  index_date = '2016-10-01' and enc_type = 'patch_rx' then 1 end)  oct_16_patch_rx,
-- count(case when  index_date = '2016-11-01' and enc_type = 'patch_rx' then 1 end)  nov_16_patch_rx,
-- count(case when  index_date = '2016-12-01' and enc_type = 'patch_rx' then 1 end)  dec_16_patch_rx,
-- count(case when  index_date = '2017-01-01' and enc_type = 'patch_rx' then 1 end)  jan_17_patch_rx,
-- count(case when  index_date = '2017-02-01' and enc_type = 'patch_rx' then 1 end)  feb_17_patch_rx,
-- count(case when  index_date = '2015-01-01' and enc_type = 'ring_rx' then 1 end)  jan_15_ring_rx,
-- count(case when  index_date = '2015-02-01' and enc_type = 'ring_rx' then 1 end)  feb_15_ring_rx,
-- count(case when  index_date = '2015-03-01' and enc_type = 'ring_rx' then 1 end)  mar_15_ring_rx,
-- count(case when  index_date = '2015-04-01' and enc_type = 'ring_rx' then 1 end)  apr_15_ring_rx,
-- count(case when  index_date = '2015-05-01' and enc_type = 'ring_rx' then 1 end)  may_15_ring_rx,
-- count(case when  index_date = '2015-06-01' and enc_type = 'ring_rx' then 1 end)  jun_15_ring_rx,
-- count(case when  index_date = '2015-07-01' and enc_type = 'ring_rx' then 1 end)  jul_15_ring_rx,
-- count(case when  index_date = '2015-08-01' and enc_type = 'ring_rx' then 1 end)  aug_15_ring_rx,
-- count(case when  index_date = '2015-09-01' and enc_type = 'ring_rx' then 1 end)  sep_15_ring_rx,
-- count(case when  index_date = '2015-10-01' and enc_type = 'ring_rx' then 1 end)  oct_15_ring_rx,
-- count(case when  index_date = '2015-11-01' and enc_type = 'ring_rx' then 1 end)  nov_15_ring_rx,
-- count(case when  index_date = '2015-12-01' and enc_type = 'ring_rx' then 1 end)  dec_15_ring_rx,
-- count(case when  index_date = '2016-01-01' and enc_type = 'ring_rx' then 1 end)  jan_16_ring_rx,
-- count(case when  index_date = '2016-02-01' and enc_type = 'ring_rx' then 1 end)  feb_16_ring_rx,
-- count(case when  index_date = '2016-03-01' and enc_type = 'ring_rx' then 1 end)  mar_16_ring_rx,
-- count(case when  index_date = '2016-04-01' and enc_type = 'ring_rx' then 1 end)  apr_16_ring_rx,
-- count(case when  index_date = '2016-05-01' and enc_type = 'ring_rx' then 1 end)  may_16_ring_rx,
-- count(case when  index_date = '2016-06-01' and enc_type = 'ring_rx' then 1 end)  jun_16_ring_rx,
-- count(case when  index_date = '2016-07-01' and enc_type = 'ring_rx' then 1 end)  jul_16_ring_rx,
-- count(case when  index_date = '2016-08-01' and enc_type = 'ring_rx' then 1 end)  aug_16_ring_rx,
-- count(case when  index_date = '2016-09-01' and enc_type = 'ring_rx' then 1 end)  sep_16_ring_rx,
-- count(case when  index_date = '2016-10-01' and enc_type = 'ring_rx' then 1 end)  oct_16_ring_rx,
-- count(case when  index_date = '2016-11-01' and enc_type = 'ring_rx' then 1 end)  nov_16_ring_rx,
-- count(case when  index_date = '2016-12-01' and enc_type = 'ring_rx' then 1 end)  dec_16_ring_rx,
-- count(case when  index_date = '2017-01-01' and enc_type = 'ring_rx' then 1 end)  jan_17_ring_rx,
-- count(case when  index_date = '2017-02-01' and enc_type = 'ring_rx' then 1 end)  feb_17_ring_rx
FROM kre_iud_pat_details
GROUP BY sex, age_group_10_yr, race
ORDER by sex, age_group_10_yr, race;


