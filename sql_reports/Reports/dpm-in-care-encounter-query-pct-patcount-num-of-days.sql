﻿DROP TABLE IF EXISTS tmp_incare_enc_percents;
DROP TABLE IF EXISTS tmp_incare_enc_percents_rt;
DROP TABLE IF EXISTS tmp_incare_enc_percents_output;


-- Compute the percentage of patients for each "next_encounter_days" within a given year
-- Excluding patients who did not have another encounter
CREATE TABLE tmp_incare_enc_percents AS
SELECT T1.rpt_year, max(patient_count) patient_count, 
case when next_encounter_days= 0 then 99999 else next_encounter_days end next_encounter_days, 
max(patient_count / yr_patient_count) as days_pct, 
max(yr_patient_count) total_yearly_patients
FROM   tmp_incare_enc_all_year_counts T1,
(select rpt_year, sum(patient_count) yr_patient_count from tmp_incare_enc_all_year_counts group by rpt_year) T2
WHERE T1.rpt_year = T2.rpt_year
--AND next_encounter_days != 0
GROUP BY T1.rpt_year, next_encounter_days
ORDER  BY T1.rpt_year, next_encounter_days;

-- Compute the running total of percentages 
CREATE TABLE tmp_incare_enc_percents_rt AS
SELECT rpt_year, patient_count, next_encounter_days, days_pct, total_yearly_patients
     , sum(days_pct) OVER (PARTITION BY rpt_year ORDER BY next_encounter_days) AS running_total
FROM   tmp_incare_enc_percents
WHERE next_encounter_days != 0
ORDER  BY rpt_year, next_encounter_days;

-- Determine the percentages that fall within the window
-- x percent of patients had a visit on that number of days or less

CREATE TABLE tmp_incare_enc_percents_output AS
select T1.rpt_year, num_days_for_1_pct, num_days_for_5_pct, num_days_for_10_pct, num_days_for_25_pct, num_days_for_50_pct, num_days_for_75_pct, num_days_for_90_pct, num_days_for_99_pct,
(ROUND( 100.0 * (zero_subsequent_count / total_yearly_patients))) as zero_subsequent_pct
FROM tmp_incare_enc_percents_rt T1,
	(--1st percentile
	select rpt_year, min(next_encounter_days)  num_days_for_1_pct
	from tmp_incare_enc_percents_rt
	WHERE running_total >=.01
	GROUP BY rpt_year) T2,
	(--5th percentile
	select rpt_year, min(next_encounter_days) num_days_for_5_pct
	from tmp_incare_enc_percents_rt
	WHERE running_total >=.05
	GROUP BY rpt_year) T3,
	(--10th percentile
	select rpt_year, min(next_encounter_days) num_days_for_10_pct
	from tmp_incare_enc_percents_rt
	WHERE running_total >=.10
	GROUP BY rpt_year) T4,
	(--25th percentile
	select rpt_year, min(next_encounter_days) num_days_for_25_pct
	from tmp_incare_enc_percents_rt
	WHERE running_total >=.25
	GROUP BY rpt_year) T5,
	(--50th percentile
	select rpt_year, min(next_encounter_days) num_days_for_50_pct
	from tmp_incare_enc_percents_rt
	WHERE running_total >=.50
	GROUP BY rpt_year) T6,
	(--75th percentile
	select rpt_year, min(next_encounter_days) num_days_for_75_pct
	from tmp_incare_enc_percents_rt
	WHERE running_total >=.75
	GROUP BY rpt_year) T7,
	(--90th percentile
	select rpt_year, min(next_encounter_days) num_days_for_90_pct
	from tmp_incare_enc_percents_rt
	WHERE running_total >=.90
	GROUP BY rpt_year) T8,
	(--99th percentile
	select rpt_year, min(next_encounter_days) num_days_for_99_pct
	from tmp_incare_enc_percents_rt
	WHERE running_total >=.99
	GROUP BY rpt_year) T9,	
	(--patients who did not have another encounter
	select rpt_year, sum(patient_count) as zero_subsequent_count from tmp_incare_enc_all_year_counts
	WHERE next_encounter_days =0
	GROUP BY rpt_year) T10
WHERE T1.rpt_year = T2.rpt_year
AND T1.rpt_year = T3.rpt_year
AND T1.rpt_year = T4.rpt_year
AND T1.rpt_year = T5.rpt_year
AND T1.rpt_year = T6.rpt_year
AND T1.rpt_year = T7.rpt_year
AND T1.rpt_year = T8.rpt_year
AND T1.rpt_year = T9.rpt_year
AND T1.rpt_year = T10.rpt_year
GROUP BY T1.rpt_year, num_days_for_1_pct, num_days_for_5_pct, num_days_for_10_pct, num_days_for_25_pct, num_days_for_50_pct, num_days_for_75_pct, num_days_for_90_pct, num_days_for_99_pct, zero_subsequent_pct
ORDER BY rpt_year;;

--select * from tmp_incare_enc_percents_output order by rpt_year;









