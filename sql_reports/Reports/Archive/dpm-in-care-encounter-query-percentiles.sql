﻿-- The distribution of number of days from first encounter in the calendar year of interest until the patients next encounter whenever it might be 
-- (even if beyond the calendar year of interest). Please provide the 1st, 5th, 10th, 25th, 50th, 75th, 90th, and 99th percentile values for time until next 
-- encounter as well as the percentage of patients with 0 subsequent encounters.  

DROP TABLE IF EXISTS tmp_incare_enc_percentile;

CREATE TABLE tmp_incare_enc_percentile AS 

select 
     '2011'::text AS rpt_year,
    max(case when ntile = 1 then next_encounter_days end) as percentile_1,
    max(case when ntile = 5 then next_encounter_days end) as percentile_5,
    max(case when ntile = 10 then next_encounter_days end) as percentile_10,
    max(case when ntile = 25 then next_encounter_days end) as percentile_25,
    max(case when ntile = 50 then next_encounter_days end) as percentile_50,
    max(case when ntile = 75 then next_encounter_days end) as percentile_75,
    max(case when ntile = 90 then next_encounter_days end) as percentile_90,
    max(case when ntile = 95 then next_encounter_days end) as percentile_95,
    max(case when ntile = 99 then next_encounter_days end) as percentile_99,
    max(case when ntile = 100 then next_encounter_days end) as percentile_100,
    max(ROUND( 100.0 * (zero_subsequent_count / all_patient_count))) as zero_subsequent_pct
from (
    select 
        rpt_year, next_encounter_days,
        NTILE(100) over (order by next_encounter_days asc) as ntile
    from tmp_incare_enc_all_year_counts
    where rpt_year = '2011' 
    AND next_encounter_days != 0
) as T1,
(select sum(patient_count) as all_patient_count from tmp_incare_enc_all_year_counts
where rpt_year = '2011') T2,
(select sum(patient_count) as zero_subsequent_count from tmp_incare_enc_all_year_counts
where rpt_year = '2011'
AND next_encounter_days =0) T3

UNION

select 
     '2012'::text AS rpt_year,
    max(case when ntile = 1 then next_encounter_days end) as percentile_1,
    max(case when ntile = 5 then next_encounter_days end) as percentile_5,
    max(case when ntile = 10 then next_encounter_days end) as percentile_10,
    max(case when ntile = 25 then next_encounter_days end) as percentile_25,
    max(case when ntile = 50 then next_encounter_days end) as percentile_50,
    max(case when ntile = 75 then next_encounter_days end) as percentile_75,
    max(case when ntile = 90 then next_encounter_days end) as percentile_90,
    max(case when ntile = 95 then next_encounter_days end) as percentile_95,
    max(case when ntile = 99 then next_encounter_days end) as percentile_99,
    max(case when ntile = 100 then next_encounter_days end) as percentile_100,
    max(ROUND( 100.0 * (zero_subsequent_count / all_patient_count))) as zero_subsequent_pct
from (
    select 
        rpt_year, next_encounter_days,
        NTILE(100) over (order by next_encounter_days asc) as ntile
    from tmp_incare_enc_all_year_counts
    where rpt_year = '2012' 
    AND next_encounter_days != 0
) as T1,
(select sum(patient_count) as all_patient_count from tmp_incare_enc_all_year_counts
where rpt_year = '2012') T2,
(select sum(patient_count) as zero_subsequent_count from tmp_incare_enc_all_year_counts
where rpt_year = '2012'
AND next_encounter_days =0) T3

UNION

select 
     '2013'::text AS rpt_year,
    max(case when ntile = 1 then next_encounter_days end) as percentile_1,
    max(case when ntile = 5 then next_encounter_days end) as percentile_5,
    max(case when ntile = 10 then next_encounter_days end) as percentile_10,
    max(case when ntile = 25 then next_encounter_days end) as percentile_25,
    max(case when ntile = 50 then next_encounter_days end) as percentile_50,
    max(case when ntile = 75 then next_encounter_days end) as percentile_75,
    max(case when ntile = 90 then next_encounter_days end) as percentile_90,
    max(case when ntile = 95 then next_encounter_days end) as percentile_95,
    max(case when ntile = 99 then next_encounter_days end) as percentile_99,
    max(case when ntile = 100 then next_encounter_days end) as percentile_100,
    max(ROUND( 100.0 * (zero_subsequent_count / all_patient_count))) as zero_subsequent_pct
from (
    select 
        rpt_year, next_encounter_days,
        NTILE(100) over (order by next_encounter_days asc) as ntile
    from tmp_incare_enc_all_year_counts
    where rpt_year = '2013' 
    AND next_encounter_days != 0
) as T1,
(select sum(patient_count) as all_patient_count from tmp_incare_enc_all_year_counts
where rpt_year = '2013') T2,
(select sum(patient_count) as zero_subsequent_count from tmp_incare_enc_all_year_counts
where rpt_year = '2013'
AND next_encounter_days =0) T3

UNION

select 
     '2014'::text AS rpt_year,
    max(case when ntile = 1 then next_encounter_days end) as percentile_1,
    max(case when ntile = 5 then next_encounter_days end) as percentile_5,
    max(case when ntile = 10 then next_encounter_days end) as percentile_10,
    max(case when ntile = 25 then next_encounter_days end) as percentile_25,
    max(case when ntile = 50 then next_encounter_days end) as percentile_50,
    max(case when ntile = 75 then next_encounter_days end) as percentile_75,
    max(case when ntile = 90 then next_encounter_days end) as percentile_90,
    max(case when ntile = 95 then next_encounter_days end) as percentile_95,
    max(case when ntile = 99 then next_encounter_days end) as percentile_99,
    max(case when ntile = 100 then next_encounter_days end) as percentile_100,
    max(ROUND( 100.0 * (zero_subsequent_count / all_patient_count))) as zero_subsequent_pct
from (
    select 
        rpt_year, next_encounter_days,
        NTILE(100) over (order by next_encounter_days asc) as ntile
    from tmp_incare_enc_all_year_counts
    where rpt_year = '2014' 
    AND next_encounter_days != 0
) as T1,
(select sum(patient_count) as all_patient_count from tmp_incare_enc_all_year_counts
where rpt_year = '2014') T2,
(select sum(patient_count) as zero_subsequent_count from tmp_incare_enc_all_year_counts
where rpt_year = '2014'
AND next_encounter_days =0) T3

UNION

select 
     '2015'::text AS rpt_year,
    max(case when ntile = 1 then next_encounter_days end) as percentile_1,
    max(case when ntile = 5 then next_encounter_days end) as percentile_5,
    max(case when ntile = 10 then next_encounter_days end) as percentile_10,
    max(case when ntile = 25 then next_encounter_days end) as percentile_25,
    max(case when ntile = 50 then next_encounter_days end) as percentile_50,
    max(case when ntile = 75 then next_encounter_days end) as percentile_75,
    max(case when ntile = 90 then next_encounter_days end) as percentile_90,
    max(case when ntile = 95 then next_encounter_days end) as percentile_95,
    max(case when ntile = 99 then next_encounter_days end) as percentile_99,
    max(case when ntile = 100 then next_encounter_days end) as percentile_100,
    max(ROUND( 100.0 * (zero_subsequent_count / all_patient_count))) as zero_subsequent_pct
from (
    select 
        rpt_year, next_encounter_days,
        NTILE(100) over (order by next_encounter_days asc) as ntile
    from tmp_incare_enc_all_year_counts
    where rpt_year = '2015' 
    AND next_encounter_days != 0
) as T1,
(select sum(patient_count) as all_patient_count from tmp_incare_enc_all_year_counts
where rpt_year = '2015') T2,
(select sum(patient_count) as zero_subsequent_count from tmp_incare_enc_all_year_counts
where rpt_year = '2015'
AND next_encounter_days =0) T3

UNION

select 
     '2016'::text AS rpt_year,
    max(case when ntile = 1 then next_encounter_days end) as percentile_1,
    max(case when ntile = 5 then next_encounter_days end) as percentile_5,
    max(case when ntile = 10 then next_encounter_days end) as percentile_10,
    max(case when ntile = 25 then next_encounter_days end) as percentile_25,
    max(case when ntile = 50 then next_encounter_days end) as percentile_50,
    max(case when ntile = 75 then next_encounter_days end) as percentile_75,
    max(case when ntile = 90 then next_encounter_days end) as percentile_90,
    max(case when ntile = 95 then next_encounter_days end) as percentile_95,
    max(case when ntile = 99 then next_encounter_days end) as percentile_99,
    max(case when ntile = 100 then next_encounter_days end) as percentile_100,
    max(ROUND( 100.0 * (zero_subsequent_count / all_patient_count))) as zero_subsequent_pct
from (
    select 
        rpt_year, next_encounter_days,
        NTILE(100) over (order by next_encounter_days asc) as ntile
    from tmp_incare_enc_all_year_counts
    where rpt_year = '2016' 
    AND next_encounter_days != 0
) as T1,
(select sum(patient_count) as all_patient_count from tmp_incare_enc_all_year_counts
where rpt_year = '2016') T2,
(select sum(patient_count) as zero_subsequent_count from tmp_incare_enc_all_year_counts
where rpt_year = '2016'
AND next_encounter_days =0) T3

UNION

select 
     '2017'::text AS rpt_year,
    max(case when ntile = 1 then next_encounter_days end) as percentile_1,
    max(case when ntile = 5 then next_encounter_days end) as percentile_5,
    max(case when ntile = 10 then next_encounter_days end) as percentile_10,
    max(case when ntile = 25 then next_encounter_days end) as percentile_25,
    max(case when ntile = 50 then next_encounter_days end) as percentile_50,
    max(case when ntile = 75 then next_encounter_days end) as percentile_75,
    max(case when ntile = 90 then next_encounter_days end) as percentile_90,
    max(case when ntile = 95 then next_encounter_days end) as percentile_95,
    max(case when ntile = 99 then next_encounter_days end) as percentile_99,
    max(case when ntile = 100 then next_encounter_days end) as percentile_100,
    max(ROUND( 100.0 * (zero_subsequent_count / all_patient_count))) as zero_subsequent_pct
from (
    select 
        rpt_year, next_encounter_days,
        NTILE(100) over (order by next_encounter_days asc) as ntile
    from tmp_incare_enc_all_year_counts
    where rpt_year = '2017' 
    AND next_encounter_days != 0
) as T1,
(select sum(patient_count) as all_patient_count from tmp_incare_enc_all_year_counts
where rpt_year = '2017') T2,
(select sum(patient_count) as zero_subsequent_count from tmp_incare_enc_all_year_counts
where rpt_year = '2017'
AND next_encounter_days =0) T3

order by rpt_year;

SELECT * FROM tmp_incare_enc_percentile;

--DROP TABLE IF EXISTS tmp_incare_enc_percentile;
