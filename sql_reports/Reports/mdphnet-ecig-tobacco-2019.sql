set search_path=esp_mdphnet, public;

WITH vap_denom AS (
SELECT patient_id, T2.natural_key as patient_natural_key, T1.id as enc_id, T1.date as enc_date
	FROM emr_encounter T1,
	emr_patient T2,
	esp_demographic T3,
	emr_encounter_dx_codes T4
	WHERE T1.date BETWEEN '01-01-2019' and '12-31-2019'
	AND T1.patient_id = T2.id 
	AND T3.patid = T2.natural_key
	AND T4.encounter_id = T1.id
	AND (T1.weight > 0 
	     OR T1.height > 0
		 OR T1.bp_systolic > 0
         OR T1.bp_diastolic > 0
		 OR T1.temperature > 0
		 OR T1.pregnant = TRUE
		 OR T1.edd is not null
		 OR exists (select null 
		            from emr_encounter_dx_codes dx 
	                where dx.encounter_id = T1.id 
	                and T4.dx_code_id<>'icd9:799.9'))
	),
	

vap_all_soc_for_pats as (
-- This is the patient's latest tobacco value BEFORE end of the study period
select T1.patient_id, T1.patient_natural_key, T2.date as enc_date, T2.tobacco_use, null as vaping_use
from vap_denom T1
	JOIN emr_socialhistory T2 on (T1.patient_id = T2.patient_id)
	JOIN (select max(date) as latest_tobacco_date, patient_id from emr_socialhistory 
	    --filter out not asked here to get the best answer because on 1 visit it may be current and a week later at another visit they don't ask
		where tobacco_use is not null and tobacco_use != '' and tobacco_use != 'NOT ASKED'
		and date < '2020-01-01'
		GROUP BY patient_id) T3 on (T3.patient_id = T1.patient_id)
WHERE T2.tobacco_use is not null and T2.tobacco_use != '' and tobacco_use != 'NOT ASKED'
AND date < '2020-01-01'
AND T3.latest_tobacco_date = T2.date
GROUP BY T1.patient_id, T1.patient_natural_key, T2.date, tobacco_use, vaping_use
UNION
-- This is the patient's latest vaping value BEFORE end of the study period
select T1.patient_id, T1.patient_natural_key, T2.date as enc_date, null as tobacco_use, vaping_use
from vap_denom T1
JOIN special_social_hist_vaping T2 on (T1.patient_natural_key = T2.pat_nat_key)
JOIN (select max(date) as latest_vaping_date, pat_nat_key from special_social_hist_vaping
		where vaping_use is not null and vaping_use != '' and vaping_use != 'Never Assessed'
		and date < '2020-01-01'
		GROUP BY pat_nat_key) T3 on (T3.pat_nat_key = T1.patient_natural_key)
WHERE T2.vaping_use is not null and T2.vaping_use != '' and vaping_use != 'Never Assessed'
AND T3.latest_vaping_date = T2.date
GROUP BY patient_id, patient_natural_key, T2.date, vaping_use),


vap_all_soc_for_pats_clean as (
select patient_id, patient_natural_key, max(tobacco_use) tobacco_use, max(vaping_use) vaping_use
from vap_all_soc_for_pats
group by patient_id, patient_natural_key),

-- normalize/categorize the data
vap_catergorize_vap_tobacco as (
select patient_id, patient_natural_key, 
case when tobacco_use in ('YES') then 'Current'
when tobacco_use in ('QUIT') then 'Former'
when tobacco_use in ('PASSIVE') then 'Passive'
when tobacco_use in ('NEVER') then 'Never'
else 'Unknown' end as tobacco_use,
case when vaping_use in ('Never Assessed', 'Unknown If Ever Used', NULL) then 'Unknown'
when vaping_use in ('User - Current Status Unknown','Current Every Day User','Current Some Day User') then 'Current'
when vaping_use in ('Former User') then 'Former'
when vaping_use in ('Never User') then 'Never'
else 'Unknown' end as vaping_use,
case when tobacco_use in ('PASSIVE', 'YES') and vaping_use in ('User - Current Status Unknown','Current Every Day User','Current Some Day User') then 'Yes'
else null end combo_use
from vap_all_soc_for_pats_clean),

-- breathing icd encounters for denom patients
vap_breathing_encounters as (
select patient_id, patient_natural_key, 'Yes'::text as breathing_icd 
from vap_denom T1,
emr_encounter_dx_codes T2
where dx_code_id ilike 'icd10:R06%'
AND T1.enc_id = T2.encounter_id
group by patient_id, patient_natural_key, breathing_icd),

-- bring info for patients together with denom data
vap_numerator_data as (
select T1.patient_id, T1.patient_natural_key, tobacco_use, vaping_use, combo_use, breathing_icd 
FROM  vap_denom T1
LEFT JOIN vap_catergorize_vap_tobacco T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN vap_breathing_encounters T3 ON (T1.patient_id = T3.patient_id)
GROUP BY T1.patient_id, T1.patient_natural_key, tobacco_use, vaping_use, combo_use, breathing_icd),

-- Add in patient age groups and details to prepare to stratify
vap_data_with_pat_info as (
SELECT T1.*, 
sex,
CASE   
	when date_part('year', age('2019-01-01'::date, date_of_birth)) <= 14 then '0-14'    
	when date_part('year', age('2019-01-01'::date, date_of_birth)) <= 15 then '15-19'  
	when date_part('year', age('2019-01-01'::date, date_of_birth)) <= 29 then '20-29' 
	when date_part('year', age('2019-01-01'::date, date_of_birth)) <= 39 then '30-39' 
	when date_part('year', age('2019-01-01'::date, date_of_birth)) <= 44 then '40-44'   
	when date_part('year', age('2019-01-01'::date, date_of_birth)) <= 49 then '45-49'  
	when date_part('year', age('2019-01-01'::date, date_of_birth)) <= 59 then '50-59' 
	when date_part('year', age('2019-01-01'::date, date_of_birth)) <= 69 then '60-69' 
	when date_part('year', age('2019-01-01'::date, date_of_birth)) <= 79 then '70-79'   
	when date_of_birth is null  then 'UNKNOWN AGE'
	else '>= 80' end age_group_10_yr,
CASE when race_ethnicity = 6 then 'hispanic'  
	when race_ethnicity = 5 then 'white' 
	when race_ethnicity = 3 then 'black' 
	when race_ethnicity = 2 then 'asian' 
	when race_ethnicity = 1 then 'native_american' 
	when race_ethnicity = 0 then 'unknown' 
	end race_ethnicity
FROM vap_numerator_data T1
JOIN emr_patient T2 on (T1.patient_id = T2.id)
JOIN esp_mdphnet.esp_demographic T3 on (T1.patient_natural_key = T3.patid) )

-- Do the counts for the output
SELECT
count(*) num_pats_with_enc,
race_ethnicity,
age_group_10_yr, 
sex, 
count(case when vaping_use = 'Unknown' or vaping_use is null then 1 end) as ecig_unknown,
count(case when vaping_use = 'Current' then 1 end) as ecig_current,
count(case when vaping_use = 'Former' then 1 end) as ecig_former,
count(case when vaping_use = 'Never' then 1 end) as ecig_never,
count(case when tobacco_use = 'Unknown' or vaping_use is null then 1 end) as tobacco_unknown,
count(case when tobacco_use = 'Current' then 1 end) as tobacco_user,
count(case when tobacco_use = 'Passive' then 1 end) as tobacco_passive,
count(case when tobacco_use = 'Former' then 1 end) as tobacco_former,
count(case when tobacco_use = 'Never' then 1 end) as tobacco_never,
count(case when combo_use = 'Yes' then 1 end) as combo_user,
count(case when breathing_icd = 'Yes' and vaping_use = 'Current' then 1 end) as breathing_ecig
from vap_data_with_pat_info
group by sex, age_group_10_yr, race_ethnicity
order by race_ethnicity, age_group_10_yr, sex