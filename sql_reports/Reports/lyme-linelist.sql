﻿DROP TABLE IF EXISTS lyme_ll_icds;
DROP TABLE IF EXISTS lyme_ll_labs_all;
DROP TABLE IF EXISTS lyme_ll_labs_filter;
DROP TABLE IF EXISTS lyme_ll_labs_no_hef;
DROP TABLE IF EXISTS lyme_ll_labs;
DROP TABLE IF EXISTS lyme_ll_indexpats;
DROP TABLE IF EXISTS lyme_ll_cases;
DROP TABLE IF EXISTS lyme_ll_indexdates;
DROP TABLE IF EXISTS lyme_ll_lab_elisa;
DROP TABLE IF EXISTS lyme_ll_lab_igg_wb;
DROP TABLE IF EXISTS lyme_ll_lab_igm_wb;
DROP TABLE IF EXISTS lyme_ll_lab_pcr;
DROP TABLE IF EXISTS lyme_ll_meds;
DROP TABLE IF EXISTS lyme_ll_doxy;
DROP TABLE IF EXISTS lyme_ll_antib;
DROP TABLE IF EXISTS lyme_ll_output;


-- Patients with ICD for Lyme
CREATE TABLE lyme_ll_icds AS
select patient_id, dx_code_id, date icd_date
FROM emr_encounter e,
emr_encounter_dx_codes dx
WHERE e.id = dx.encounter_id
AND dx.dx_code_id in ('icd9:088.81','icd10:A69.20','icd10:A69.23','icd10:A69.29','icd10:A69.21','icd10:A69.22')
AND e.date BETWEEN '01-01-2013' AND '12-31-2015';

-- Patients with a lab test for lyme
-- VIEW DATA FROM THIS TABLE TO CHECK FOR UNMAPPED RESULT STRINGS
-- CREATE TABLE lyme_ll_labs AS
-- select l.patient_id, native_name, test_name, l.date lab_date, h.name, h.date hef_date, l.result_string, l.specimen_source, l.order_natural_key, l.ref_high_float
-- FROM emr_labresult l
-- LEFT JOIN conf_labtestmap c ON l.native_code = c.native_code
-- LEFT JOIN hef_event h ON l.id = h.object_id AND l.patient_id = h.patient_id
-- WHERE c.test_name ilike '%lyme%'
-- AND (h.name not ilike 'lx:lyme%any-result' or h.name is NULL)
-- AND l.date BETWEEN '01-01-2013' AND '12-31-2015'
-- order by patient_id, lab_date, test_name, result_string;


-- Patients with a lab test for lyme
-- VIEW DATA FROM THIS TABLE TO CHECK FOR UNMAPPED RESULT STRINGS
-- CREATE TABLE lyme_ll_labs AS
-- select l.patient_id, native_name, test_name, l.date lab_date, l.result_string, l.specimen_source, l.order_natural_key, l.ref_high_float
-- FROM emr_labresult l
-- LEFT JOIN conf_labtestmap c ON l.native_code = c.native_code
-- WHERE c.test_name ilike '%lyme%'
-- AND l.date BETWEEN '01-01-2013' AND '12-31-2015'
-- order by patient_id, lab_date, test_name, result_string;

CREATE TABLE lyme_ll_labs_all AS
SELECT l.patient_id, l.id, native_name, test_name, l.date lab_date, l.result_string, l.specimen_source, l.order_natural_key, l.ref_high_float, h.name
FROM emr_labresult l
LEFT JOIN conf_labtestmap c ON l.native_code = c.native_code
LEFT JOIN hef_event h ON l.id = h.object_id
WHERE c.test_name ilike '%lyme%'
AND l.date BETWEEN '01-01-2013' AND '12-31-2015'
--AND (((h.name ilike 'lx:lyme%' and h.name != 'lx:lyme_elisa:any-result' and h.name != 'lx:lyme_igm_eia:any-result') or l.result_string ='') or h.name is null)
AND (h.content_type_id in (34,35) or h.content_type_id is null)
order by name, patient_id, lab_date, test_name, result_string;

-- PENDING NEW CODE
CREATE TABLE lyme_ll_labs_filter AS
SELECT * from lyme_ll_labs_all
WHERE name not ilike '%any-result%' or name is null;

-- LAB RESULTS WITHOUT POS/NEG HEF EVENTS
CREATE TABLE lyme_ll_labs_no_hef AS
SElECT patient_id, id, native_name, test_name, lab_date, result_string, specimen_source, order_natural_key, ref_high_float, null::text as name
FROM lyme_ll_labs_all
where id not in (select id from lyme_ll_labs_filter);

-- UNION FILTER AND MISSING
CREATE TABLE lyme_ll_labs AS
SELECT * FROM lyme_ll_labs_filter
UNION
SELECT * FROM lyme_ll_labs_no_hef;


-- Index patients
CREATE TABLE lyme_ll_indexpats AS
SELECT patient_id FROM lyme_ll_icds
UNION
SELECT patient_id FROM lyme_ll_labs;


-- Lyme Cases For Index Patients
CREATE TABLE lyme_ll_cases AS
SELECT i.patient_id,
min(date) esp_case_date,
case when max(c.id) IS NOT NULL then 1 ELSE 0 END esp_case
FROM nodis_case c
RIGHT JOIN lyme_ll_indexpats i ON c.patient_id = i.patient_id
AND c.condition = 'lyme'
GROUP BY i.patient_id;


-- Index date for report
CREATE TABLE lyme_ll_indexdates AS
SELECT i.patient_id,
CASE WHEN esp_case_date is NOT NULL then esp_case_date ELSE LEAST(min(icd_date), min(l.lab_date)) END index_date
FROM lyme_ll_indexpats i
LEFT JOIN lyme_ll_cases c ON i.patient_id = c.patient_id
LEFT JOIN lyme_ll_labs l ON i.patient_id = l.patient_id
LEFT JOIN lyme_ll_icds dx ON i.patient_id = dx.patient_id
GROUP BY i.patient_id, esp_case_date;


-- Elisa variables for the report
-- Many patients have multiple elisa tests on the same date.
-- One variable to indicated if an ELISA test was done (code 1 for yes, 0 for no)
-- A second variable for the date of first ELISA test
-- A third variable for the result of the first ELISA test

-- CREATE TABLE lyme_ll_lab_elisa AS
-- SELECT l.patient_id,
-- elisa_first_date,
-- 1::integer elisa_done,
-- array_agg(distinct(result_string)) elisa_first_result
-- FROM lyme_ll_indexpats i
-- LEFT JOIN lyme_ll_labs l ON l.patient_id = i.patient_id
-- LEFT JOIN (SELECT patient_id, min(lab_date) elisa_first_date from lyme_ll_labs WHERE test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia', 'lyme_ab_csf') GROUP BY patient_id) l2 ON l2.patient_id = i.patient_id AND l.patient_id = l2.patient_id
-- WHERE l.lab_date = l2.elisa_first_date
-- AND l.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia', 'lyme_ab_csf')
-- GROUP BY l.patient_id, elisa_first_date
-- ORDER BY l.patient_id;

-- PENDING NEW
CREATE TABLE lyme_ll_lab_elisa AS
SELECT l.patient_id,
elisa_first_date,
1::integer elisa_done,
array_agg(result_string) elisa_first_result,
array_agg(name) elisa_pos_neg_interp
FROM lyme_ll_indexpats i
LEFT JOIN lyme_ll_labs l ON l.patient_id = i.patient_id
LEFT JOIN (SELECT patient_id, min(lab_date) elisa_first_date from lyme_ll_labs WHERE test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia', 'lyme_ab_csf') GROUP BY patient_id) l2 ON l2.patient_id = i.patient_id AND l.patient_id = l2.patient_id
WHERE l.lab_date = l2.elisa_first_date
AND l.test_name IN ('lyme_elisa', 'lyme_igm_eia', 'lyme_igg_eia', 'lyme_ab_csf')
GROUP BY l.patient_id, elisa_first_date
ORDER BY l.patient_id;


-- IGG WB Variables for the report
-- One variable to indicate if an IGG WB test was done within 0 to 60 days of the first ELISA test (code 1 for yes, 0 for no)
-- A second variable for the date of the first IGG WB test performed within 0 to 60 days of the ELISA
-- A third variable for the result of the first IGG WB test performed within 0 to 60 days of the ELISA

-- CREATE TABLE lyme_ll_lab_igg_wb AS
-- SELECT l.patient_id,
-- 1::INTEGER igg_wb_done,
-- igg_wb_first_date,
-- array_agg(distinct(result_string)) igg_wb_first_result
-- FROM lyme_ll_labs l,
-- (select ll.patient_id, min(lab_date) igg_wb_first_date FROM lyme_ll_labs ll, lyme_ll_lab_elisa ee WHERE ll.patient_id = ee.patient_id
	-- AND ll.lab_date <= ee.elisa_first_date + INTERVAL '60 days'
	-- AND ll.lab_date >= ee.elisa_first_date
	-- AND ll.test_name = 'lyme_igg_wb'
	-- group by ll.patient_id) l2
-- WHERE l.patient_id = l2.patient_id
-- AND l.test_name = 'lyme_igg_wb'
-- AND l.lab_date = igg_wb_first_date
-- GROUP BY l.patient_id, igg_wb_first_date
-- ORDER BY patient_id;

-- PENDING NEW
CREATE TABLE lyme_ll_lab_igg_wb AS
SELECT l.patient_id,
1::INTEGER igg_wb_done,
igg_wb_first_date,
--array_agg(distinct(result_string)) igg_wb_first_result,
array_agg(result_string) igg_wb_first_result,
array_agg(name) igg_wb_pos_neg_interp 
FROM lyme_ll_labs l,
(select ll.patient_id, min(lab_date) igg_wb_first_date FROM lyme_ll_labs ll, lyme_ll_lab_elisa ee WHERE ll.patient_id = ee.patient_id
	AND ll.lab_date <= ee.elisa_first_date + INTERVAL '60 days'
	AND ll.lab_date >= ee.elisa_first_date
	AND ll.test_name = 'lyme_igg_wb'
	group by ll.patient_id) l2
WHERE l.patient_id = l2.patient_id
AND l.test_name = 'lyme_igg_wb'
AND l.lab_date = igg_wb_first_date
GROUP BY l.patient_id, igg_wb_first_date
ORDER BY patient_id;


-- IGM WB Variables for the report
-- One variable to indicate if an IGM WB test was done within 0 to 60 days of the first ELISA test (code 1 for yes, 0 for no)
-- A second variable for the date of the first IGM WB test performed within 0 to 60 days of the ELISA
-- A third variable for the result of the first IGM WB test performed within 0 to 60 days of the ELISA
-- CREATE TABLE lyme_ll_lab_igm_wb AS
-- SELECT l.patient_id,
-- 1::INTEGER igm_wb_done,
-- igm_wb_first_date,
-- array_agg(distinct(result_string)) igm_wb_first_result
-- FROM lyme_ll_labs l,
-- (select ll.patient_id, min(lab_date) igm_wb_first_date FROM lyme_ll_labs ll, lyme_ll_lab_elisa ee WHERE ll.patient_id = ee.patient_id
	-- AND ll.lab_date <= ee.elisa_first_date + INTERVAL '60 days'
	-- AND ll.lab_date >= ee.elisa_first_date
	-- AND ll.test_name = 'lyme_igm_wb'
	-- group by ll.patient_id) l2
-- WHERE l.patient_id = l2.patient_id
-- AND l.test_name = 'lyme_igm_wb'
-- AND l.lab_date = igm_wb_first_date
-- GROUP BY l.patient_id, igm_wb_first_date
-- ORDER BY patient_id;

-- PENDING NEW
CREATE TABLE lyme_ll_lab_igm_wb AS
SELECT l.patient_id,
1::INTEGER igm_wb_done,
igm_wb_first_date,
--array_agg(distinct(result_string)) igm_wb_first_result
array_agg(result_string) igm_wb_first_result,
array_agg(name) igm_wb_pos_neg_interp 
FROM lyme_ll_labs l,
(select ll.patient_id, min(lab_date) igm_wb_first_date FROM lyme_ll_labs ll, lyme_ll_lab_elisa ee WHERE ll.patient_id = ee.patient_id
	AND ll.lab_date <= ee.elisa_first_date + INTERVAL '60 days'
	AND ll.lab_date >= ee.elisa_first_date
	AND ll.test_name = 'lyme_igm_wb'
	group by ll.patient_id) l2
WHERE l.patient_id = l2.patient_id
AND l.test_name = 'lyme_igm_wb'
AND l.lab_date = igm_wb_first_date
GROUP BY l.patient_id, igm_wb_first_date
ORDER BY patient_id;


-- Lyme PCR
-- One variable to indicate if a Lyme PCR test was done (code 1 for yes, 0 for no)
-- A second variable for the date of the first PCR test performed
-- A third variable for the result of the first PCR test performed 
-- A fourth variable for the specimen source of the first PCR performed
-- CREATE TABLE lyme_ll_lab_pcr AS
-- SELECT l.patient_id,
-- 1::INTEGER pcr_done,
-- pcr_first_date,
-- array_agg(distinct(result_string)) pcr_first_result,
-- array_agg(DISTINCT(l.specimen_source)) pcr_specimen_source
-- FROM lyme_ll_indexpats i
-- LEFT JOIN lyme_ll_labs l ON l.patient_id = i.patient_id
-- LEFT JOIN (SELECT patient_id, min(lab_date) pcr_first_date from lyme_ll_labs WHERE test_name IN ('lyme_pcr', 'lyme_pcr_csf') GROUP BY patient_id) l2 ON l2.patient_id = i.patient_id AND l.patient_id = l2.patient_id
-- WHERE l.lab_date = l2.pcr_first_date
-- AND l.test_name IN ('lyme_pcr', 'lyme_pcr_csf')
-- GROUP BY l.patient_id, pcr_first_date
-- ORDER BY l.patient_id;

--PENDING NEW
CREATE TABLE lyme_ll_lab_pcr AS
SELECT l.patient_id,
1::INTEGER pcr_done,
pcr_first_date,
array_agg(result_string) pcr_first_result,
array_agg(name) pcr_pos_neg_interp ,
array_agg(DISTINCT(l.specimen_source)) pcr_specimen_source
FROM lyme_ll_indexpats i
LEFT JOIN lyme_ll_labs l ON l.patient_id = i.patient_id
LEFT JOIN (SELECT patient_id, min(lab_date) pcr_first_date from lyme_ll_labs WHERE test_name IN ('lyme_pcr', 'lyme_pcr_csf') GROUP BY patient_id) l2 ON l2.patient_id = i.patient_id AND l.patient_id = l2.patient_id
WHERE l.lab_date = l2.pcr_first_date
AND l.test_name IN ('lyme_pcr', 'lyme_pcr_csf')
GROUP BY l.patient_id, pcr_first_date
ORDER BY l.patient_id;


-- Treatment
-- One variable to indicate whether a patient has been prescribed doxycycline within 0 to 30 days of the index date (code as 1 for yes, 0 for no)
-- A second variable to indicate whether a patient has been prescribed amoxicillin, cefuroxime, ceftriaxone, or cefotaxime within 0 to 30 days of the index date (code as 1 for yes, 0 for no)
-- A third variable for which antibiotic was prescribed
-- A fourth variable for the quantity prescribed
CREATE TABLE lyme_ll_meds AS
SELECT DISTINCT ON (rx.id) i.patient_id, h.date med_date, h.name, rx.name med_name, rx.quantity
FROM  lyme_ll_indexpats i
LEFT JOIN hef_event h ON h.patient_id = i.patient_id
LEFT JOIN emr_prescription rx ON i.patient_id = rx.patient_id AND h.patient_id = rx.patient_id AND rx.id = h.object_id 
WHERE h.name in ('rx:lyme_other_antibiotics', 'rx:doxycycline', 'rx:doxycycline_7_day')
--and (rx.patient_class is null or rx.patient_class ilike '1.%')
order by rx.id, rx.name;

CREATE TABLE lyme_ll_doxy AS
SELECT m.patient_id,
doxy_first_date,
1::INTEGER doxy_given,
array_agg ((med_name) ORDER BY MED_NAME) doxy_first_name,
array_agg((quantity) ORDER BY MED_NAME) doxy_first_quantity
FROM lyme_ll_meds m,
(select rx.patient_id, min(med_date) doxy_first_date FROM lyme_ll_meds rx, lyme_ll_indexdates id WHERE rx.patient_id = id.patient_id
	AND rx.med_date <= id.index_date + INTERVAL '30 days'
	AND rx.med_date >= id.index_date
	AND rx.name in ('rx:doxycycline', 'rx:doxycycline_7_day')
	group by rx.patient_id) m2	
WHERE m.patient_id = m2.patient_id
AND m.med_date = doxy_first_date
AND m.name in ('rx:doxycycline', 'rx:doxycycline_7_day')
GROUP BY m.patient_id, doxy_first_date
ORDER BY patient_id;

CREATE TABLE lyme_ll_antib AS
SELECT m.patient_id,
other_antib_first_date,
1::INTEGER other_antib_given,
array_agg ((med_name) ORDER BY MED_NAME) other_antib_first_name,
array_agg((quantity) ORDER BY MED_NAME) other_antib_first_quantity
FROM lyme_ll_meds m,
(select rx.patient_id, min(med_date) other_antib_first_date FROM lyme_ll_meds rx, lyme_ll_indexdates id WHERE rx.patient_id = id.patient_id
	AND rx.med_date <= id.index_date + INTERVAL '30 days'
	AND rx.med_date >= id.index_date
	AND rx.name in ('rx:lyme_other_antibiotics')
	group by rx.patient_id) m2	
WHERE m.patient_id = m2.patient_id
AND m.med_date = other_antib_first_date
AND m.name in ('rx:lyme_other_antibiotics')
GROUP BY m.patient_id, other_antib_first_date
ORDER BY patient_id;

-- Prepare the output 
CREATE TABLE lyme_ll_output AS
SELECT
i.patient_id esp_id,
esp_case,
index_date,
date_part('year', age(index_date, date_of_birth)) age_at_index_date,
gender,
race,
COALESCE(elisa_done,0) elisa_done,
elisa_first_date,
elisa_first_result,
elisa_pos_neg_interp,
COALESCE(igg_wb_done, 0) igg_wb_done,
igg_wb_first_date,
igg_wb_first_result,
igg_wb_pos_neg_interp,
COALESCE(igm_wb_done, 0) igm_wb_done,
igm_wb_first_date,
igm_wb_first_result,
igm_wb_pos_neg_interp,
COALESCE(pcr_done, 0) pcr_done,
pcr_first_date,
pcr_first_result,
pcr_pos_neg_interp,
pcr_specimen_source,
COALESCE(doxy_given,0) doxy_given,
doxy_first_name,
doxy_first_quantity,
COALESCE(other_antib_given, 0) other_antib_given,
other_antib_first_name,
other_antib_first_quantity
FROM lyme_ll_indexpats i
LEFT JOIN lyme_ll_cases c ON c.patient_id = i.patient_id
LEFT JOIN lyme_ll_indexdates id ON id.patient_id = i.patient_id
LEFT JOIN emr_patient p ON p.id = i.patient_id
LEFT JOIN lyme_ll_lab_elisa e on e.patient_id = i.patient_id
LEFT JOIN lyme_ll_lab_igg_wb igg ON igg.patient_id = i.patient_id
LEFT JOIN lyme_ll_lab_igm_wb igm ON igm.patient_id = i.patient_id
LEFT JOIN lyme_ll_lab_pcr pcr ON pcr.patient_id = i.patient_id
LEFT JOIN lyme_ll_doxy doxy ON doxy.patient_id = i.patient_id
LEFT JOIN lyme_ll_antib ab ON ab.patient_id = i.patient_id
order by i.patient_id;

select * from lyme_ll_output;





-- 
-- 
-- 
-- 
-- --unmapped labs?
-- select distinct(native_code), native_name from emr_labresult where native_code not in (select native_code from conf_labtestmap where test_name ilike '%lyme%') and
-- native_name ilike '%lyme%';
-- 
-- 
-- select distinct(native_name), native_code from emr_labresult where native_code not in (select native_code from conf_labtestmap where test_name ilike '%lyme%') and
-- native_name ilike '%lyme%';
-- 
--  from lyme_ll_labs ll
-- where name is null
-- 
-- 

