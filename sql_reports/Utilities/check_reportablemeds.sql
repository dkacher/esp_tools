--REPORTABLE MEDS
drop table if exists tmpRepRx;
--create tmpRepRx table from conf_reportablemedication
--truncate table - leaves structure..
select * into table tmpRepRx from conf_reportablemedication;
truncate tmpRepRx;
--copy good source File to tmp DB Table
copy tmpRepRx (id,condition_id,drug_name,notes) FROM '/srv/esp/esp_tools/sql_reports/Utilities/data/ESPReportableRx.csv' delimiters ',' CSV header;  
--compare Get list of missing Meds to Add
select condition_id,lower(drug_name) as AddToReportableMeds from tmpreprx except  
select condition_id,lower(drug_name) from conf_reportablemedication order by condition_id;
--compare Get list of Extra Meds to Delete
select condition_id,lower(drug_name) as RemoveFromReportableMeds from conf_reportablemedication except 
select condition_id,lower(drug_name) from tmpreprx order by condition_id;

