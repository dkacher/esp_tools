--REPORTABLE LABS
drop table if exists tmpRepLx;
--create tmpRepLx table from conf_reportablelab
--truncate table - leaves structure..
select * into table tmpRepLx from conf_reportablelab;
truncate tmpRepLx;
--copy the good source File to tmp DB Table
copy tmpRepLx (condition_id,native_name) FROM '/srv/esp/esp_tools/sql_reports/Utilities/data/ESPReportableLx.csv' delimiters ',' CSV header;  
--compare Get list of missing Labs to Add
select condition_id,lower(native_name) as AddToReportableLabs from tmpreplx except  
select condition_id,lower(native_name) from conf_reportablelab order by condition_id;
--compare Get list of Extra Labs to Delete
select condition_id,native_name as RemoveFromReportableLabs from conf_reportablelab except 
select condition_id,native_name from tmpreplx order by condition_id;

