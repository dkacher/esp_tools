-- RUN THIS FIRST 
-- these are the unmapped labs
drop table if exists kre_report.kre_unmapped;
create table kre_report.kre_unmapped as
select distinct(native_code), native_name  from emr_labtestconcordance
where native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode)
and native_name ~* '(a1c|alk|alp|alt|aminotrans|ast|bili|bilirubin|borr|burg|ca|cd4|chlam|chol|ck|cpk|cr|creat|cult|diab|eo|feron|fta|flu|gad65|gc|
genotype|gluc|glyc|gon|haemog|hb|hbv|hcv|hdl|hemog|hep|hg|hiv|ica512|immuno|insulin|islet|k|kinase|ldl|leuc|leuk|lipoprotein|lyme|
lymph|na|neut|ogtt|pallidum|pcr|peptide|pert|plastin|plat|plt|pmn|poly|pot|ptt|rapid plasma reagin|rpr|serol|sgot|sgpt|sod|syph|
tbil|thromboc|tp|trac|trep|trig|t-spot|vdrl|wbc|white bl)';

-- RUN THIS TO GENERATE WORKING LIST 
-- run this to see if any of the unmapped labs map to labs that have already been mapped
-- match up on the codes where possible
select c.test_name, c.native_code already_mapped_native_code, c.reportable, c.output_code, c.snomed_pos, c.snomed_neg, c.snomed_ind, c.threshold, u.native_code as native_code_to_map, u.native_name
from conf_labtestmap c, 
kre_report.kre_unmapped u
where split_part(c.native_code, '--', 2) = split_part(u.native_code, '--', 2)
order by split_part(c.native_code, '--', 2);

-- ONLY RUN THIS IF YOU WANT TO REVIEW ALL UNMAPPED LABS
-- run this to see all unmapped labs (!!!may contain labs for which a plugin is not installed!!)
-- display list of unmapped labs
select * from kre_report.kre_unmapped;

-- RUN THIS IF PERFORMING IGNORED LABS VALIDATION
-- SCRIPTS EXISTS FOR BOTH CHA AND ATRIUS.
-- Run to this see if any ignored labs map to labs that have been mapped
-- This could alert one to the fact that a lab has been incorrectly ignored **OR** it could be that a lab has been incorrectly mapped.
-- **OR** it could be valid that the lab was ignored. If the lab has been verified as valid to ignore then the native_code should be added
-- as an exclusion in this script.

-- Atrius
SELECT c.test_name, c.native_code, c.reportable, c.output_code, c.snomed_pos, c.snomed_neg, c.snomed_ind, c.threshold, i.native_code ignored_native_code
from conf_labtestmap c, 
conf_ignoredcode i
where split_part(c.native_code, '--', 2) = split_part(i.native_code, '--', 2)
and test_name != 'rapid_flu'
and test_name != 'rubella'
and i.native_code not in (
'85027--1116', 
'83021--420',
'83020--420',
'82947--420',
'84080--5803',
'84075--9711',
'82947--178',
'80048--181',
'80053--181',
'80058--181',
'80076--181',
'82040--181',
'84075--181',
'84080--181',
'85027--181',
'LA0217--181',
'LA0297--181',
'84235--6315',
'82310--8791',
'82330--8791',
'83970--8791',
'82340--3892',
'82570--3892',
'82715--799',
'83719--7626',
'84460EXT--54',
'83719--4839',
'N1097--59',
'LAB721--7719',
'81002--1796',
'82545--59',
'83036EXT--59',
'LAB495--59',
'LAB865--59',
'84540--7719',
'82435--99',
'83718--99',
'84132EXT--3128',
'85007--3128',
'85027--3128',
'85027EXT--3128	',
'85999--3128',
'89051--3128',
'LA0256--3128',
'LAB110--3128',
'TA999--3128',
'85027EXT--3128',
'87086--2523',
'82947--35',
'81001--35',
'83020--7573',
'86803--4061',
'87902--4062',
'87389--1475',
'85613--8138',
'86320--8138',
'83020--8138',
'83715--8138',
'86140--8138',
'87476--926',
'86618--4464',
'86753--8138',
'86666--8138',
'83625--8138',
'84165--8138',
'86146--8138',
'86335--8138',
'85303--8138',
'86318--8160',
'82947--98',
'82947EXT--98',
'85025--594',
'LA0256--594',
'TA999--594',
'82947--97',
'N2444--97',
'85007--2979',
'85027--97',
'84132EXT--2979',
'82947--158',
'82947--159',
'N2185--2887',
'80061--64',
'83718--64',
'84300--4477',
'80061--60',
'84295--7426',
'84300--7426',
'84300--7426',
'81000--2842',
'81002--2842',
'N2880--2842',
'87210--5340',
'89051--18661',
'87491--4310',
'87070--328',
'81005--11',
'83970--11',
'81003--1419',
'81005--1419'
)
order by test_name;


-- CHA
SELECT c.test_name, c.native_code, c.reportable, c.output_code, c.snomed_pos, c.snomed_neg, c.snomed_ind, c.threshold, i.native_code ignored_native_code
from conf_labtestmap c, 
conf_ignoredcode i
where split_part(c.native_code, '--', 2) = split_part(i.native_code, '--', 2)
and test_name != 'clostridium_difficile_eia'
and test_name != 'giardiasis'
and i.native_code not in (
 'LAB370--5261--',
 '82565.3--2667--',
 '87389.1--6038--',
 '87389.1--6055--',
 '2001035--6468--',
 '2001036--6468--',
 '2001010--5552--',
 '87086.1--1366--',
 '87177.5--1366--',
 '87102--1366--',
 '87070.8--1366--',
 '87070.7--1366--',
 '87086--1366--',
 '2000098--1366--',
 '86403.3--1366--',
 '87641.1--1366--',
 '87205.2--1366--',
 '87070.4--1366--',
 '2000095--1366--',
 '87040.1--1366--',
 '87070.2--1366--',
 '87621.5--1366--',
 '87081.3--1366--',
 '87493.2--1366--',
 '87210.2--1366--',
 '86403.2--1366--',
 '2000147--1366--',
 '2000091--1366--',
 '2000092--1366--',
 '87177.3--1366--',
 '2000093--1366--',
 '87102.1--1366--',
 '87400.1--1366--',
 '87076.2--1366--',
 '2000097--1366--',
 '2000096--1366--',
 '87070.12--1366--',
 '87070.6--1366--',
 '87076.4--1366--',
 '87400--1366--',
 '87449.1--1366--',
 '2000099--1366--',
 '87076.5--1366--',
 '87040.3--1366--',
 '86403.1--1366--',
 '87070.4--1368--',
 '87177.2--1368--',
 '87070.3--1368--',
 '87070.8--1368--',
 '87070.12--1368--',
 '2000092--1368--',
 '87102--1368--',
 '87081.3--1368--',
 '87070.7--1368--',
 '87040.3--1368--',
 '87076.2--1368--',
 '2000091--1368--',
'2000093--1368--',
'87040.1--1368--',
'87070.2--1368--',
'87205.2--1368--',
'87086--1368--',
'87086.1--1368--',
'87070.6--1368--',
'2000098--1368--',
'87076.5--1368--',
'2000096--1368--',
'2000099--1368--',
'87102.1--1368--',
'87076.4--1368--',
'2000095--1368--',
'87070.3--1366--',
'2000097--1368--'
)
order by test_name;