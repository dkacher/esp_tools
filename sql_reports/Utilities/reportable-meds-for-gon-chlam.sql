-- Will show cases and med details for reportable meds for chlamydia and gonorrhea
-- Adjust date range as desired
-- Modify reporting windows as desired
-- Can be adjusted to use for other conditions 

select c.id as case_id, c.patient_id, c.date, c.condition, rx.name, rx.date, (rx.date - c.date) med_days_from_case, c.status
from nodis_case c
LEFT JOIN emr_prescription rx on (c.patient_id = rx.patient_id)
LEFT JOIN (select drug_name chlam_drug, condition_id from conf_reportablemedication where condition_id = 'chlamydia') chlam_rx ON (c.condition = chlam_rx.condition_id)
LEFT JOIN (select drug_name gon_drug, condition_id from conf_reportablemedication where condition_id = 'gonorrhea') gon_rx ON (c.condition = gon_rx.condition_id)
where condition in ('gonorrhea', 'chlamydia')
and c.date >= '2019-12-01'
and rx.date >= c.date - INTERVAL '7 days'
and rx.date <= c.date + INTERVAL '30 days'
and ((rx.name ILIKE '%' || chlam_drug || '%') or (rx.name ILIKE '%' || gon_drug || '%'))
group by case_id, c.patient_id, c.date, c.condition, rx.name, rx.date
order by case_id, c.patient_id, condition, name;