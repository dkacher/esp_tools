--SQL to Create a listing of labs useful for assigning output and snomed values in the conf_labtestmap table
-- update the list of tests for that condition in the line:
	-- AND test_name in ('chlamydia')
-- update the date of the file generated
-- currently set below for 2 conditions chlamydia and gonorrhea

--chlamydia
\COPY (select count(*), c.id as test_id, l.native_code, native_name, procedure_name, test_name, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('chlamydia') group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/labmappings-ESP-chlamydia-2019-10-16.csv' WITH CSV HEADER;

--gonorrhea
\COPY (select count(*), c.id as test_id, l.native_code, native_name, procedure_name, test_name, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('gonorrhea') group by c.id, l.native_code, native_name, procedure_name, test_name, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-gonorrhea-2019-10-16.csv' WITH CSV HEADER;

--syphilis
\COPY (select count(*), c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('rpr', 'vdrl', 'tppa', 'fta-abs', 'tp-igg', 'tp-cia', 'tp-igm', 'vdrl-csf', 'tppa-csf', 'fta-abs-csf') group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-syphilis-2020-01-09.csv' WITH CSV HEADER;


--tuberculosis
\COPY (select count(*), c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('tb_afb', 'tb_igra') group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-tuberculosis-2020-01-09.csv' WITH CSV HEADER;

--hepatits_a
\COPY (select count(*), c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('hepatitis_a_igm_antibody', 'alt', 'ast') group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-hepA-2020-01-09.csv' WITH CSV HEADER;

--hepatitis_b
\COPY (select count(*), c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and (test_name ilike 'hepatitis_b%' or test_name in ('ast', 'bilirubin_total', 'alt')) group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-hepB-2020-01-09.csv' WITH CSV HEADER;

--hepatitis_c
\COPY (select count(*), c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and (test_name ilike 'hepatitis_c%' or test_name in ('bilirubin_total', 'alt')) group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-hepC-2020-01-09.csv' WITH CSV HEADER;

--tuberculosis
\COPY (select count(*), c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('tb_afb', 'tb_igra') group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-tuberculosis-2020-01-09.csv' WITH CSV HEADER;
