DROP INDEX IF EXISTS emr_labresult_ts_code_high_float;
DROP INDEX IF EXISTS emr_labresult_patient_date_code_result;
DROP INDEX IF EXISTS emr_labresult_ts_native_code_and_result_float;
DROP INDEX IF EXISTS emr_labresult_ts_native_code_and_result_string;
DROP INDEX IF EXISTS emr_labresult_native_code_result_float;
DROP INDEX IF EXISTS emr_labresult_order_natural_key_like;
DROP INDEX IF EXISTS emr_labresult_CLIA_ID_id_like;
DROP INDEX IF EXISTS emr_labresult_CLIA_ID_id;
DROP INDEX IF EXISTS emr_labresult_order_natural_key;
DROP INDEX IF EXISTS emr_encounter_site_name_like;
DROP INDEX IF EXISTS emr_encounter_site_name;
DROP INDEX IF EXISTS emr_labresult_native_name;
DROP INDEX IF EXISTS emr_encounter_raw_encounter_type_like;
DROP INDEX IF EXISTS emr_labresult_result_string_like;
DROP INDEX IF EXISTS emr_encounter_site_natural_key_like;
DROP INDEX IF EXISTS emr_encounter_site_natural_key;
DROP INDEX IF EXISTS emr_labresult_abnormal_flag;
DROP INDEX IF EXISTS emr_labresult_abnormal_flag_like;
DROP INDEX IF EXISTS emr_labresult_ref_high_float;
DROP INDEX IF EXISTS emr_labresult_ref_low_float;
DROP INDEX IF EXISTS emr_labresult_collection_date;
DROP INDEX IF EXISTS emr_labresult_result_date;
DROP INDEX IF EXISTS emr_labresult_updated_timestamp;
DROP INDEX IF EXISTS emr_prescription_ts_name_like;
DROP INDEX IF EXISTS emr_encounter_dx_codes_dx_code_id_like;
DROP INDEX IF EXISTS emr_prescription_name;
DROP INDEX IF EXISTS emr_encounter_cpt;
DROP INDEX IF EXISTS emr_encounter_cpt_like;
DROP INDEX IF EXISTS emr_encounter_peak_flow;
DROP INDEX IF EXISTS emr_encounter_pregnant;
DROP INDEX IF EXISTS emr_encounter_status_like;
DROP INDEX IF EXISTS emr_encounter_o2_sat;
DROP INDEX IF EXISTS emr_encounter_priority;
DROP INDEX IF EXISTS emr_encounter_status;
DROP INDEX IF EXISTS emr_encounter_encounter_type_like;
DROP INDEX IF EXISTS emr_encounter_priority_like;
DROP INDEX IF EXISTS emr_encounter_weight;
DROP INDEX IF EXISTS emr_encounter_encounter_type;
DROP INDEX IF EXISTS emr_encounter_date_closed;
DROP INDEX IF EXISTS emr_encounter_hosp_dschrg_dt;
DROP INDEX IF EXISTS emr_encounter_hosp_admit_dt;
DROP INDEX IF EXISTS emr_encounter_updated_timestamp;
DROP INDEX IF EXISTS emr_prescription_order_natural_key;
DROP INDEX IF EXISTS emr_prescription_order_natural_key_like;
DROP INDEX IF EXISTS emr_prescription_provider_id;
DROP INDEX IF EXISTS emr_prescription_quantity_float;
DROP INDEX IF EXISTS emr_prescription_dose;
DROP INDEX IF EXISTS emr_prescription_dose_like;
DROP INDEX IF EXISTS emr_prescription_updated_timestamp;
DROP INDEX IF EXISTS emr_patient_ethnicity;
DROP INDEX IF EXISTS emr_patient_ethnicity_like;
DROP INDEX IF EXISTS emr_patient_race;
DROP INDEX IF EXISTS emr_patient_race_like;
DROP INDEX IF EXISTS emr_socialhistory_provider_id;
DROP INDEX IF EXISTS emr_socialhistory_alcohol_use;
DROP INDEX IF EXISTS emr_socialhistory_alcohol_use_like;
DROP INDEX IF EXISTS emr_socialhistory_date;
DROP INDEX IF EXISTS emr_socialhistory_tobacco_use;
DROP INDEX IF EXISTS emr_socialhistory_updated_timestamp;
DROP INDEX IF EXISTS emr_patient_center_id;
DROP INDEX IF EXISTS emr_patient_gender_like;
DROP INDEX IF EXISTS emr_patient_gender;
DROP INDEX IF EXISTS emr_patient_center_id_like;
DROP INDEX IF EXISTS emr_patient_zip5_like;
DROP INDEX IF EXISTS emr_patient_zip5;
DROP INDEX IF EXISTS emr_patient_zip;
DROP INDEX IF EXISTS emr_patient_zip_like;
DROP INDEX IF EXISTS emr_patient_updated_timestamp;
DROP INDEX IF EXISTS emr_patient_pcp_id;
DROP INDEX IF EXISTS emr_patient_mrn_like;
DROP INDEX IF EXISTS emr_patient_mrn;
DROP INDEX IF EXISTS hef_event_source;
DROP INDEX IF EXISTS hef_event_source_like;
DROP INDEX IF EXISTS emr_provider_center_id;
DROP INDEX IF EXISTS emr_provider_center_id_like;
DROP INDEX IF EXISTS emr_provider_updated_timestamp;
DROP INDEX IF EXISTS emr_labtestconcordance_native_name;
DROP INDEX IF EXISTS emr_labtestconcordance_native_name_like;
DROP INDEX IF EXISTS static_ndc_product_code;
DROP INDEX IF EXISTS static_ndc_label_code_like;
DROP INDEX IF EXISTS static_ndc_product_code_like;
DROP INDEX IF EXISTS static_ndc_label_code;
DROP INDEX IF EXISTS emr_labtestconcordance_native_code;
DROP INDEX IF EXISTS emr_labtestconcordance_native_code_like;
DROP INDEX IF EXISTS nodis_case_criteria;
DROP INDEX IF EXISTS emr_provenance_source;
DROP INDEX IF EXISTS nodis_reported_417f1b1c;
DROP INDEX IF EXISTS emr_provenance_status_like;
DROP INDEX IF EXISTS emr_provenance_status;
DROP INDEX IF EXISTS conf_reportabledx_code_condition_id;
DROP INDEX IF EXISTS conf_reportablelab_native_name_like;
DROP INDEX IF EXISTS emr_encountertypemap_mapping;
DROP INDEX IF EXISTS emr_encountertypemap_priority;
DROP INDEX IF EXISTS emr_encountertypemap_raw_encounter_type_like;
DROP INDEX IF EXISTS emr_laborder_order_type_like;
DROP INDEX IF EXISTS conf_reportablemedication_condition_id;
DROP INDEX IF EXISTS nodis_casestatushistory_timestamp;
DROP INDEX IF EXISTS ss_site_zip_code_like;
DROP INDEX IF EXISTS ss_site_zip_code;
DROP INDEX IF EXISTS emr_labinfo_zip5_like;
DROP INDEX IF EXISTS emr_labinfo_zip;
DROP INDEX IF EXISTS emr_labinfo_provenance_id;
DROP INDEX IF EXISTS emr_labinfo_zip_like;
DROP INDEX IF EXISTS emr_labinfo_zip5;
DROP INDEX IF EXISTS emr_laborder_updated_timestamp;
DROP INDEX IF EXISTS emr_laborder_specimen_id;
DROP INDEX IF EXISTS conf_reportablemedication_condition_id_like;
DROP INDEX IF EXISTS emr_laborder_cdate;
DROP INDEX IF EXISTS emr_laborder_specimen_id_like;
DROP INDEX IF EXISTS emr_laborder_provider_id;
DROP INDEX IF EXISTS emr_laborder_order_type;
DROP INDEX IF EXISTS emr_laborder_cdate_like;
DROP INDEX IF EXISTS emr_specimen_laborder_id;
DROP INDEX IF EXISTS emr_specimen_provenance_id;
DROP INDEX IF EXISTS emr_encountertypemap_mapping_like;
DROP INDEX IF EXISTS emr_encountertypemap_raw_encounter_type;
DROP INDEX IF EXISTS conf_reportablelab_condition_id;
DROP INDEX IF EXISTS conf_labtestmap_reportable;
DROP INDEX IF EXISTS conf_reportablelab_native_name;
DROP INDEX IF EXISTS conf_reportablelab_condition_id_like;
DROP INDEX IF EXISTS emr_patient_addr_patient_id;
DROP INDEX IF EXISTS emr_patient_addr_zip_like;
DROP INDEX IF EXISTS emr_patient_addr_zip5_like;
DROP INDEX IF EXISTS emr_patient_addr_zip5;
DROP INDEX IF EXISTS conf_hl7map_hl7_id;
DROP INDEX IF EXISTS emr_order_idinfo_provenance_id;
DROP INDEX IF EXISTS static_allergen_vaccines_vaccine_id;
DROP INDEX IF EXISTS static_allergen_vaccines_allergen_id_like;
DROP INDEX IF EXISTS static_allergen_vaccines_allergen_id;
DROP INDEX IF EXISTS hef_timespan_source_like;
DROP INDEX IF EXISTS hef_timespan_source;
DROP INDEX IF EXISTS hef_timespan_name_like;
DROP INDEX IF EXISTS hef_timespan_name;
DROP INDEX IF EXISTS emr_stiencounterextended_updated_timestamp;
DROP INDEX IF EXISTS emr_stiencounterextended_provider_id;
DROP INDEX IF EXISTS emr_stiencounterextended_provenance_id;
DROP INDEX IF EXISTS emr_stiencounterextended_patient_id;
DROP INDEX IF EXISTS emr_stiencounterextended_date;
DROP INDEX IF EXISTS nodis_validatorresult_cases_validatorresult_id;
DROP INDEX IF EXISTS vaers_report_sent_report_type_like;
DROP INDEX IF EXISTS vaers_report_sent_report_type;
DROP INDEX IF EXISTS vaers_report_sent_questionnaire_id;
DROP INDEX IF EXISTS vaers_report_sent_case_id;
DROP INDEX IF EXISTS auth_user_groups_group_id;
DROP INDEX IF EXISTS static_rx_lookup_type_id;
DROP INDEX IF EXISTS static_immunizationmanufacturer_vaccines_produced_vaccine_id;
DROP INDEX IF EXISTS static_immunizationmanufacturer_vaccines_produced_immunizatcfc9;
DROP INDEX IF EXISTS vaers_questionnaire_state_like;
DROP INDEX IF EXISTS vaers_questionnaire_state;
DROP INDEX IF EXISTS vaers_questionnaire_satisfaction_num_msg_like;
DROP INDEX IF EXISTS vaers_questionnaire_satisfaction_num_msg;
DROP INDEX IF EXISTS vaers_questionnaire_provider_id;
DROP INDEX IF EXISTS vaers_questionnaire_case_id;
DROP INDEX IF EXISTS vaers_sender_provider_id;
DROP INDEX IF EXISTS vaers_sender_date_added;
DROP INDEX IF EXISTS conf_vaccinemanufacturermap_canonical_code_id;
DROP INDEX IF EXISTS emr_provider_phones_provider_id;
DROP INDEX IF EXISTS emr_provider_phones_provenance_id;
DROP INDEX IF EXISTS static_site_group_id_like;
DROP INDEX IF EXISTS static_site_group_id;
DROP INDEX IF EXISTS vaers_case_patient_id;
DROP INDEX IF EXISTS vaers_case_last_update;
DROP INDEX IF EXISTS vaers_case_date;
DROP INDEX IF EXISTS emr_patient_extradata_updated_timestamp;
DROP INDEX IF EXISTS emr_patient_extradata_provenance_id;
DROP INDEX IF EXISTS emr_patient_extradata_patient_id;
DROP INDEX IF EXISTS nodis_validatorrun_list_id;
DROP INDEX IF EXISTS emr_immunization_provider_id;
DROP INDEX IF EXISTS emr_immunization_updated_timestamp;
DROP INDEX IF EXISTS conf_reportableextended_variables_abstract_ext_var_id_like;
DROP INDEX IF EXISTS conf_reportableextended_variables_condition_id;
DROP INDEX IF EXISTS conf_reportableextended_variables_abstract_ext_var_id;
DROP INDEX IF EXISTS emr_order_extension_updated_timestamp;
DROP INDEX IF EXISTS emr_order_extension_date;
DROP INDEX IF EXISTS auth_user_user_permissions_permission_id;
DROP INDEX IF EXISTS vaers_adverseevent_priority;
DROP INDEX IF EXISTS vaers_adverseevent_patient_id;
DROP INDEX IF EXISTS vaers_adverseevent_object_id;
DROP INDEX IF EXISTS vaers_adverseevent_content_type_id;
DROP INDEX IF EXISTS emr_surveyreports_datecancelled;
DROP INDEX IF EXISTS emr_labresult_details_labresult_id;
DROP INDEX IF EXISTS emr_labresult_details_provenance_id;
DROP INDEX IF EXISTS emr_allergy_updated_timestamp;
DROP INDEX IF EXISTS emr_allergy_status;
DROP INDEX IF EXISTS emr_allergy_name_like;
DROP INDEX IF EXISTS emr_allergy_date_noted;
DROP INDEX IF EXISTS emr_allergy_allergen_id_like;
DROP INDEX IF EXISTS emr_allergy_status_like;
DROP INDEX IF EXISTS emr_allergy_provider_id;
DROP INDEX IF EXISTS emr_allergy_patient_id;
DROP INDEX IF EXISTS emr_allergy_name;
DROP INDEX IF EXISTS emr_allergy_date;
DROP INDEX IF EXISTS emr_allergy_allergen_id;
DROP INDEX IF EXISTS nodis_validatorresult_events_validatorresult_id;
DROP INDEX IF EXISTS vaers_case_adverse_events_case_id;
DROP INDEX IF EXISTS vaers_case_adverse_events_adverseevent_id;
DROP INDEX IF EXISTS nodis_validatorresult_lab_results_validatorresult_id;
DROP INDEX IF EXISTS nodis_validatorresult_lab_results_labresult_id;
DROP INDEX IF EXISTS vaers_case_immunizations_immunization_id;
DROP INDEX IF EXISTS vaers_case_immunizations_case_id;
DROP INDEX IF EXISTS conf_labtestmap_extra_indeterminate_strings_resultstring_id;
DROP INDEX IF EXISTS vaers_diagnosticseventrule_heuristic_discarding_codes_dx_coa4c5;
DROP INDEX IF EXISTS vaers_diagnosticseventrule_heuristic_discarding_codes_dx_co219c;
DROP INDEX IF EXISTS vaers_diagnosticseventrule_heuristic_discarding_codes_diagn45de;
DROP INDEX IF EXISTS emr_specobs_specimen_id;
DROP INDEX IF EXISTS emr_specobs_provenance_id;
DROP INDEX IF EXISTS conf_vaccinecodemap_canonical_code_id;
DROP INDEX IF EXISTS nodis_referencecase_patient_id;
DROP INDEX IF EXISTS nodis_referencecase_list_id;
DROP INDEX IF EXISTS nodis_referencecase_ignore;
DROP INDEX IF EXISTS nodis_referencecase_date;
DROP INDEX IF EXISTS nodis_referencecase_condition_like;
DROP INDEX IF EXISTS nodis_referencecase_condition;
DROP INDEX IF EXISTS conf_labtestmap_donotsend_results_labtestmap_id;
DROP INDEX IF EXISTS conf_labtestmap_donotsend_results_resultstring_id;
DROP INDEX IF EXISTS emr_pregnancy_updated_timestamp;
DROP INDEX IF EXISTS emr_pregnancy_provider_id;
DROP INDEX IF EXISTS emr_pregnancy_patient_id;
DROP INDEX IF EXISTS emr_pregnancy_edd;
DROP INDEX IF EXISTS emr_pregnancy_date;
DROP INDEX IF EXISTS phit_monthlystatistic_name_like;
DROP INDEX IF EXISTS phit_monthlystatistic_name;
DROP INDEX IF EXISTS phit_monthlystatistic_month;
DROP INDEX IF EXISTS emr_surveyresponse_updated_timestamp;
DROP INDEX IF EXISTS emr_surveyresponse_provider_id;
DROP INDEX IF EXISTS emr_surveyresponse_provenance_id;
DROP INDEX IF EXISTS emr_surveyresponse_patient_id;
DROP INDEX IF EXISTS emr_surveyresponse_date;
DROP INDEX IF EXISTS emr_provider_idinfo_provider_id;
DROP INDEX IF EXISTS emr_provider_idinfo_provenance_id;
DROP INDEX IF EXISTS auth_group_permissions_permission_id;
DROP INDEX IF EXISTS auth_group_permissions_group_id;
DROP INDEX IF EXISTS nodis_validatorresult_encounters_validatorresult_id;
DROP INDEX IF EXISTS nodis_validatorresult_encounters_encounter_id;
DROP INDEX IF EXISTS emr_patient_guardian_zip_like;
DROP INDEX IF EXISTS emr_patient_guardian_zip5_like;
DROP INDEX IF EXISTS emr_patient_guardian_zip5;
DROP INDEX IF EXISTS emr_patient_guardian_zip;
DROP INDEX IF EXISTS emr_patient_guardian_updated_timestamp;
DROP INDEX IF EXISTS emr_patient_guardian_provenance_id;
DROP INDEX IF EXISTS emr_patient_guardian_patient_id;
DROP INDEX IF EXISTS nodis_caseactivehistory_object_id;
DROP INDEX IF EXISTS nodis_caseactivehistory_date;
DROP INDEX IF EXISTS nodis_caseactivehistory_content_type_id;
DROP INDEX IF EXISTS vaers_case_prior_immunizations_immunization_id;
DROP INDEX IF EXISTS vaers_case_prior_immunizations_case_id;
DROP INDEX IF EXISTS nodis_validatorresult_run_id;
DROP INDEX IF EXISTS nodis_validatorresult_ref_case_id;
DROP INDEX IF EXISTS nodis_validatorresult_date;
DROP INDEX IF EXISTS nodis_validatorresult_condition_like;
DROP INDEX IF EXISTS nodis_validatorresult_condition;
DROP INDEX IF EXISTS emr_hospital_problem_updated_timestamp;
DROP INDEX IF EXISTS emr_hospital_problem_status;
DROP INDEX IF EXISTS emr_hospital_problem_provenance_id;
DROP INDEX IF EXISTS emr_hospital_problem_priority;
DROP INDEX IF EXISTS emr_hospital_problem_principal_prob;
DROP INDEX IF EXISTS emr_hospital_problem_present_on_adm;
DROP INDEX IF EXISTS emr_hospital_problem_dx_code_id_like;
DROP INDEX IF EXISTS emr_hospital_problem_status_like;
DROP INDEX IF EXISTS emr_hospital_problem_provider_id;
DROP INDEX IF EXISTS emr_hospital_problem_priority_like;
DROP INDEX IF EXISTS emr_hospital_problem_principal_prob_like;
DROP INDEX IF EXISTS emr_hospital_problem_present_on_adm_like;
DROP INDEX IF EXISTS emr_hospital_problem_patient_id;
DROP INDEX IF EXISTS emr_hospital_problem_dx_code_id;
DROP INDEX IF EXISTS emr_hospital_problem_date;
DROP INDEX IF EXISTS conf_labtestmap_excluded_indeterminate_strings_resultstring_id;
DROP INDEX IF EXISTS emr_diagnosis_updated_timestamp;
DROP INDEX IF EXISTS emr_diagnosis_provenance_id;
DROP INDEX IF EXISTS emr_diagnosis_encounter_id;
DROP INDEX IF EXISTS emr_diagnosis_codeset_like;
DROP INDEX IF EXISTS emr_diagnosis_code_like;
DROP INDEX IF EXISTS emr_diagnosis_provider_id;
DROP INDEX IF EXISTS emr_diagnosis_patient_id;
DROP INDEX IF EXISTS emr_diagnosis_date;
DROP INDEX IF EXISTS emr_diagnosis_codeset;
DROP INDEX IF EXISTS emr_diagnosis_code;
DROP INDEX IF EXISTS emr_problem_updated_timestamp;
DROP INDEX IF EXISTS emr_problem_status_like;
DROP INDEX IF EXISTS emr_problem_status;
DROP INDEX IF EXISTS emr_problem_provider_id;
DROP INDEX IF EXISTS emr_problem_patient_id;
DROP INDEX IF EXISTS emr_problem_dx_code_id_like;
DROP INDEX IF EXISTS emr_problem_dx_code_id;
DROP INDEX IF EXISTS emr_problem_date;
DROP INDEX IF EXISTS conf_labtestmap_excluded_positive_strings_resultstring_id;
DROP INDEX IF EXISTS nodis_validatorresult_prescriptions_validatorresult_id;
DROP INDEX IF EXISTS nodis_validatorresult_prescriptions_prescription_id;
DROP INDEX IF EXISTS ss_nonspecialistvisitevent_reporting_site_id_like;
DROP INDEX IF EXISTS ss_nonspecialistvisitevent_reporting_site_id;
DROP INDEX IF EXISTS ss_nonspecialistvisitevent_patient_zip_code_like;
DROP INDEX IF EXISTS ss_nonspecialistvisitevent_patient_zip_code;
DROP INDEX IF EXISTS ss_nonspecialistvisitevent_encounter_id;
DROP INDEX IF EXISTS conf_labtestmap_excluded_negative_strings_resultstring_id;
DROP INDEX IF EXISTS vaers_diagnosticseventrule_heuristic_defining_codes_dx_codecb89;
DROP INDEX IF EXISTS vaers_diagnosticseventrule_heuristic_defining_codes_dx_code_id;
DROP INDEX IF EXISTS vaers_diagnosticseventrule_heuristic_defining_codes_diagnos5066;
DROP INDEX IF EXISTS emr_patient_addr_zip;
DROP INDEX IF EXISTS emr_patient_addr_updated_timestamp;
DROP INDEX IF EXISTS emr_patient_addr_provenance_id;
