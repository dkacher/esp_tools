-- SCRIPT TO FIND HL7 CASES WITH UNKNOWN PID

--CREATE SCHEMA kre_report
--AUTHORIZATION esp;

-- Get case and initial lab details for all cases since a given date
DROP TABLE IF EXISTS kre_report.unknown_resend_1;
CREATE TABLE kre_report.unknown_resend_1 AS
    SELECT case_id, initial_lab_id, c.date as case_date, order_natural_key
    FROM nodis_casereport nc, nodis_case c, emr_labresult l
    where  nc.case_id = c.id
    AND l.id = nc.initial_lab_id
    AND c.date >= '2019-01-01';

-- Filter for cases that contain the Unknown Search String that we are looking for
DROP TABLE IF EXISTS kre_report.unknown_resend_2;
CREATE TABLE kre_report.unknown_resend_2 AS
select u.case_id, case_date, filename, u.order_natural_key, initial_lab_id, regexp_split_to_table(message, '</ORU_R01>') AS split_report,
concat('Unknown.*<EI.1>(', order_natural_key, ')</EI.1>.*') as search_string
from nodis_report nr , 
nodis_report_cases nrc, 
kre_report.unknown_resend_1 u
WHERE nr.id = nrc.report_id
AND u.case_id = nrc.case_id
-- changed to like from ilike to match the way ESP populates Unknown vs UNKNOWN 
-- if a lot of results are returned check here first as it may be related to unknown providers
--AND message like '%<PID.5>%<XPN.1>%<FN.1>Unknown</FN.1>%';
AND message ~ '<PID.5>\s*<XPN.1>\s*<FN.1>Unknown</FN.1>';


-- Extract out the order_natural_key from the message
DROP TABLE IF EXISTS kre_report.unknown_resend_3;
CREATE TABLE kre_report.unknown_resend_3 AS
select *, regexp_matches(split_report, search_string) as sub_msg_new 
from kre_report.unknown_resend_2;


-- Match only where the order_natural_key matches the extract
-- This eliminates the other cases that were part of the same message but don't match our unknown value.
DROP TABLE IF EXISTS kre_report.unknown_resend_4;
CREATE TABLE kre_report.unknown_resend_4 AS
select case_id, case_date, condition, status as case_status, p.last_name, p.first_name, order_natural_key, initial_lab_id, filename 
from kre_report.unknown_resend_3 u,
nodis_case c,
emr_patient p
WHERE u.case_id = c.id
AND p.id = c.patient_id
AND sub_msg_new::text = concat('{', order_natural_key, '}')
GROUP BY case_id, case_date, order_natural_key, condition, last_name, first_name, status, initial_lab_id, filename;


-- Display the results
select case_id, case_date, condition, case_status,
        case when last_name is null or last_name = '' then 'UNKNOWN NAME' else 'MASKED' END as last_name,
        CASE WHEN first_name IS NULL or first_name = '' THEN 'UNKNOWN NAME' ELSE 'MASKED' END as first_name,
        initial_lab_id as esp_lab_id
FROM  kre_report.unknown_resend_4;

-- Alternate display
select case_id, case_date, condition, case_status, last_name, first_name, u.order_natural_key, initial_lab_id, mrn, l.date as lab_date
from kre_report.unknown_resend_4 u,
emr_labresult l
where initial_lab_id = l.id;