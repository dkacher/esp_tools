-- Analysis script generated on Tuesday, 01 Mar 2016 16:37:05 EST
-- Analysis name: Fenway - STI Report - Clinic Variables
-- Analysis description: Fenway - STI Report - Clinic Variables
-- Script generated for database: POSTGRESQL

--
-- Script setup section 
--

set client_min_messages to error;

DROP TABLE IF EXISTS public.fenway_sti_clinic_a_100037_s_1 CASCADE;
DROP VIEW IF EXISTS public.fenway_sti_clinic_a_100037_s_1 CASCADE;

DROP TABLE IF EXISTS public.fenway_sti_clinic_a_100037_s_2 CASCADE;
DROP VIEW IF EXISTS public.fenway_sti_clinic_a_100037_s_2 CASCADE;

DROP TABLE IF EXISTS public.fenway_sti_clinic_a_100037_s_3 CASCADE;
DROP VIEW IF EXISTS public.fenway_sti_clinic_a_100037_s_3 CASCADE;

DROP TABLE IF EXISTS public.fenway_sti_clinic_a_100037_s_4 CASCADE;
DROP VIEW IF EXISTS public.fenway_sti_clinic_a_100037_s_4 CASCADE;

DROP TABLE IF EXISTS public.fenway_sti_clinic_a_100037_s_5 CASCADE;
DROP VIEW IF EXISTS public.fenway_sti_clinic_a_100037_s_5 CASCADE;

--
-- Script body 
--

-- Step 1: Access - public.emr_stiencounterextended
CREATE VIEW public.fenway_sti_clinic_a_100037_s_1 AS SELECT * FROM public.emr_stiencounterextended;

-- Step 2: Access - public.emr_patient
CREATE VIEW public.fenway_sti_clinic_a_100037_s_2 AS SELECT * FROM public.emr_patient;

-- Step 3: Access - public.emr_encounter
CREATE VIEW public.fenway_sti_clinic_a_100037_s_3 AS SELECT * FROM public.emr_encounter;

-- Step 4: Join - Join extended table against encounter to get the encounter id for the report. Create/transform all fields to values
CREATE TABLE public.fenway_sti_clinic_a_100037_s_4  AS SELECT 
1 f1_facilityid,
'MA' f1_siteid,
T2.patient_id f1_patientid,
T2.date f1_visdate,
T2.id f1_eventid,
null f1_gisp_yrmo,
null f1_gisp_number,
case when T1.insurance_status in(
'ABCD GRANT' ,
'FREECARE A PRIMARY' , 
'FREECARE B SECONDARY', 
'FREECARE C PARTIAL', 
'MASSHEALTH' ,
'MEDICARE' ,
'RYAN WHITE MEDICAL', 
'SHC / DPH', 'Tricare Northern Region'
) then 1
	
when T1.insurance_status in (
'AETNA' , 
'Aetna Global', 
'ASSURANT HEALTH' , 
'BCBS Federal', 
'BCBS HMO' ,
'BCBS NORTHEASTERN' ,
'BCBS out of state' ,
 'BCBS POS', 
 'BCBS PPO' ,
 'BMC' ,
 'CELTICARE' ,
 'CIGNA' , 
 'Cigna Health', 
 'CONSOLIDATED HEALTH PLANS', 
 'Coventry Federal', 
 'Coventry PPO', 
 'Geisinger Health Plan' ,
 'HARVARD PILGRIM HMO' ,
 'HARVARD PILGRIM PPO/POS' ,
 'HEALTH NET' ,
 'HEALTH PLAN INC', 
 'MedCost',
 'Meritain Health', 
 'NHP' ,
 'Olympus Managed Healthcare' ,
 'OXFORD HEALTH PLAN' , 
 'Scott & White Health Plan', 
 'Tufts Health Plans', 
 'TUFTS HEALTH PUBLIC PLAN (NETWORK HEALTH)' , 
 'TUFTS HEALTH UNIFY (NOT CONTRACTED)', 
 'TUFTS HMO/POS' ,'TUFTS PPO' , 
 'UNICARE GIC COMMONWEALTH', 
 'United Health Care' ,
 'UNITED HEALTH CARE' ,
 'UNITED STUDENT RESOURCES' 
 ) then 2 
	when T1.insurance_status in (
	'SELF PAY' ,
	'Sliding Fee Scale' 
	) then 5 
	when T1.insurance_status is null then null
	else 99999
	end f1_insurance, 
	
case when T2.raw_encounter_type in ('STD PROVIDER VISIT', 'STD PROVIDER VISIT - SAME DAY') then 1
	when T2.raw_encounter_type in ('STD NURSING VISIT', 'STD NURSING VISIT - SAME DAY', 'STD NURSING VISIT - 45') then 2
	when T2.raw_encounter_type in ('STD CLINIC', 'STD CLINIC - SAME DAY') then null
	when T2.raw_encounter_type is null then null
	else 99999
	end f1_visit_type,
case when T1.primary_purpose in ('Symptomatic') then 1
	when T1.primary_purpose in ('Treatment') then 2
	when  T1.primary_purpose in ('Screening') then 5
	when  T1.primary_purpose in ('Other') then 8
	when  T1.primary_purpose is null then null 
	else 99999 
	end f1_reason_visit,
9 f1_pregnant,
9 f1_contraception,
case when  T1.sti_symptoms_yn in ('Yes') then 1
	when  T1.sti_symptoms_yn in ('No') then 2
	when  T1.sti_symptoms_yn in ('Unknown') then null 
	when  T1.sti_symptoms_yn is null then null
	else 99999 
	end f1_sympt,
case when  T1.sti_exposure_yn  in ('Yes') then 1
	when  T1.sti_exposure_yn  in ('No') then 2
	when  T1.sti_exposure_yn  in ('Unknown') then null 
	when  T1.sti_exposure_yn  is null then null 
	else 99999 
	end  f1_contact_std,
9 f1_pelvic_exam, 
T1.num_male_partners  f1_mensex, 
T1.num_female_partners  f1_femsex,
case when  T1.sex_mwb in ('m', 'mt', '''sex_w_men = Yessex_w_women = Didn''''t asksex_w_trans = Didn''''t ask''') then 1 
	when  T1.sex_mwb in ('w', '''sex_w_men = Didn''''t asksex_w_women = Yessex_w_trans = No''', 'wt') then 2 
	when  T1.sex_mwb in ('mw' ,'mwt') then 3 
	when  T1.sex_mwb in ('none','t') then 4 
	when  T1.sex_mwb in ('missing') then 9 
	when  T1.sex_mwb is null then null 
	else 99999 
	end f1_sexor3, 
T1.total_partners  f1_numsex3,
case when  T1.sexuality in ('Lesbian, Gay, or Homosexual') then 1 
	when  T1.sexuality in ('Straight or Heterosexual') then 2 
	when  T1.sexuality in ('Bisexual') then 3 
	when  T1.sexuality in ('''Don''''t Know''' ,'Something Else') then 4 
	when  T1.sexuality in ('Not Reported') then 9 
	when  T1.sexuality in ('--') then null 
	when  T1.sexuality is null then null 
	else 99999 
	end f1_sexuality,
9 f1_newsex,
case when T1.anal_sex_yn in ('Yes') then 1
	when T1.anal_sex_yn in ('No') then 2
	when T1.anal_sex_yn is null then 9
	else 99999
	end f1_rectal_exposure,
case when T1.oral_sex_yn in ('Yes') then 1
	when T1.oral_sex_yn in ('No') then 2
	when T1.oral_sex_yn is null then 9
	else 99999
	end f1_pharynx_exposure,
2 f1_partner_tx,
null f1_gisp_travel,
null f1_gisp_sex_work,
null f1_gisp_antibiotic,
null f1_gisp_idu,
null f1_gisp_non_idu,
null f1_gisp_gc_12,
null f1_gisp_gc_ever,
9 f1_hiv_test,
null f1_hivtestdate,
case when T1.hiv_result in ('Negative') then 0 
	when T1.hiv_result in ('Positive') then 1
	when T1.hiv_result is null then 9
	else 99999
    end f1_hivresultlast,
9 f1_hivtest_refuse,
9 f1_hpvvaxadmin,
9 f1_sxabdomen,
9 f1_sxdysuria,
9 f1_sxdischarge,
9 f1_sxlesion,
9 f1_pedischarge,
9 f1_peabdomen,
9 f1_cmt,
9 f1_adnexal 
FROM public.fenway_sti_clinic_a_100037_s_1 T1 
INNER JOIN public.fenway_sti_clinic_a_100037_s_3 T2 ON ((T1.natural_key = T2.natural_key)) 
WHERE  T2.date >= :start_date and T2.date < :end_date;

-- Step 5: Join - Join to patient table to populate and transform additional fields
CREATE TABLE public.fenway_sti_clinic_a_100037_s_5  AS SELECT 
T2.f1_facilityid,
T2.f1_siteid,
T2.f1_patientid,
T2.f1_visdate,
T2.f1_eventid,
T2.f1_gisp_yrmo,
T2.f1_gisp_number,
case when  T1.gender in ('M') then 1 
	when  T1.gender in ('F') then 2 
	when  T1.gender in ('T') then 5 
	when  T1.gender in ('U' ,'UNKNOWN','' ) then null 
	when  T1.gender is null then null 
	else 99999 
	end f1_gender,
date_part('year',age(date_of_birth)) f1_age,
case when T1.ethnicity in ('Hispanic or Latino') then 1 
	when  T1.race = 'Hispanic' then 1 
	when  T1.ethnicity in ('Non Hispanic or Latino' ,'Not Hispanic or Latino' ,'Not Reported' ,'Other or Undetermined' ,'Patient Declined' ,'State Prohibited' ) then 2 
	when  T1.ethnicity is null then 9 
	else 99999 end f1_hisp,
case when  T1.race in ('AMERICAN INDIAN/ALASKAN NATIVE' ,'American Indian/Alaskan Native' ) then 1 
	when  T1.race in ('Asian' ,'Black' ,'Black/African American' ,'Hispanic' ,'Multiracial' ,'Other' ,'Other Pacific Islander' ,'Pacific Islander' ,'PACIFIC ISLANDER/HAWAIIAN' ,'White' ,'White/Caucasian' ) then 2 
	when  T1.race in ('Patient Declined' ,'Undetermined/Not Reported' ,'Unknown' ,'UNKNOWN') then 9 
	when T1.race is null then 9 
	else 99999 
	end f1_aian,
case when  T1.race in ('Asian') then 1 when  T1.race in ('AMERICAN INDIAN/ALASKAN NATIVE' ,'American Indian/Alaskan Native' ,'Black' ,'Black/African American' ,'Hispanic' ,'Multiracial' ,'Other' ,'Other Pacific Islander' ,'Pacific Islander' ,'PACIFIC ISLANDER/HAWAIIAN' ,'White' ,'White/Caucasian' ) then 2 
	when  T1.race in ('Patient Declined' ,'Undetermined/Not Reported' ,'Unknown' ,'UNKNOWN' ) then 9 
	when  T1.race is null then 9 
	else 99999 
	end f1_asian,
case when  T1.race in ('Other Pacific Islander' ,'Pacific Islander' ,'PACIFIC ISLANDER/HAWAIIAN') then 1 
	when  T1.race in ('AMERICAN INDIAN/ALASKAN NATIVE' ,'American Indian/Alaskan Native' ,'Asian' ,'Black' ,'Black/African American' ,'Hispanic' ,'Multiracial' ,'Other' ,'White' ,'White/Caucasian') then 2 
	when  T1.race in ('Patient Declined' ,'Undetermined/Not Reported' ,'Unknown' ,'UNKNOWN' ) then 9 
	when  T1.race is null then 9 
	else 99999 
	end f1_pih,
case when  T1.race in ('Black' ,'Black/African American' ) then 1 
	when  T1.race in ('AMERICAN INDIAN/ALASKAN NATIVE' ,'American Indian/Alaskan Native' ,'Asian' ,'Hispanic' ,'Multiracial' ,'Other' ,'Other Pacific Islander' ,'Pacific Islander' ,'PACIFIC ISLANDER/HAWAIIAN' ,'White' ,'White/Caucasian' ) then 2 
	when  T1.race in ('Patient Declined' ,'Undetermined/Not Reported' ,'Unknown' ,'UNKNOWN' ) then 9 
	when  T1.race is null then 9 
	else 99999 
	end f1_black,
case when  T1.race in ('White' ,'White/Caucasian') then 1 
	when  T1.race in ('AMERICAN INDIAN/ALASKAN NATIVE' ,'American Indian/Alaskan Native' ,'Asian' ,'Black' ,'Black/African American' ,'Hispanic' ,'Multiracial' ,'Other' ,'Other Pacific Islander' ,'Pacific Islander' ,'PACIFIC ISLANDER/HAWAIIAN' ) then 2 
	when  T1.race in ('Patient Declined' ,'Undetermined/Not Reported' ,'Unknown' ,'UNKNOWN' ) then 9 
	when  T1.race is null then 9 
	else 99999 
	end f1_white,
case when  T1.race in ('Multiracial') then 1 
	when  T1.race in ('AMERICAN INDIAN/ALASKAN NATIVE' ,'American Indian/Alaskan Native' ,'Asian' ,'Black' ,'Black/African American' ,'Hispanic' ,'Other' ,'Other Pacific Islander' ,'Pacific Islander' ,'PACIFIC ISLANDER/HAWAIIAN' ,'White' ,'White/Caucasian' ) then 2 
	when  T1.race in ('Patient Declined' ,'Undetermined/Not Reported' ,'Unknown' ,'UNKNOWN' ) then 9 
	when  T1.race is null then 9 
	else 99999 
	end f1_multirace,
case when  T1.race in ('Other') then 1 
	when  T1.race in ('AMERICAN INDIAN/ALASKAN NATIVE' ,'American Indian/Alaskan Native' ,'Asian' ,'Black' ,'Black/African American' ,'Hispanic' ,'Multiracial' ,'Other Pacific Islander' ,'Pacific Islander' ,'PACIFIC ISLANDER/HAWAIIAN' ,'White' ,'White/Caucasian' ) then 2 
	when  T1.race in ('Patient Declined' ,'Undetermined/Not Reported' ,'Unknown' ,'UNKNOWN' ) then 9 
	when  T1.race is null then 9 
	else 99999 
	end f1_otherrace,
T2.f1_insurance,
T2.f1_visit_type,
T2.f1_reason_visit,
T2.f1_pregnant,
T2.f1_contraception,
T2.f1_sympt,
T2.f1_contact_std,
T2.f1_pelvic_exam,
T2.f1_mensex,
T2.f1_femsex,
T2.f1_sexor3,
T2.f1_numsex3,
T2.f1_sexuality,
T2.f1_newsex,
T2.f1_rectal_exposure,
T2.f1_pharynx_exposure,
T2.f1_partner_tx,
T2.f1_gisp_travel,
T2.f1_gisp_sex_work,
T2.f1_gisp_antibiotic,
T2.f1_gisp_idu,
T2.f1_gisp_non_idu,
T2.f1_gisp_gc_12,
T2.f1_gisp_gc_ever,
T2.f1_hivtest,
T2.f1_hivtestdate,
T2.f1_hivresultlast,
T2.f1_hivtest_refuse,
T2.f1_hpvvaxadmin,
T2.f1_sxabdomen,
T2.f1_sxdysuria,
T2.f1_sxdischarge,
T2.f1_sxlesion,
T2.f1_pedischarge,
T2.f1_peabdomen,
T2.f1_cmt,
T2.f1_adnexal 
FROM public.fenway_sti_clinic_a_100037_s_2 T1 
INNER JOIN public.fenway_sti_clinic_a_100037_s_4 T2 ON ((T1.id = T2.f1_patientid)) ;


\copy fenway_sti_clinic_a_100037_s_5 to '/tmp/SSUN_clinic.csv' csv header

--
-- Script shutdown section 
--

DROP VIEW IF EXISTS public.fenway_sti_clinic_a_100037_s_1 CASCADE;

DROP VIEW IF EXISTS public.fenway_sti_clinic_a_100037_s_2 CASCADE;

DROP VIEW IF EXISTS public.fenway_sti_clinic_a_100037_s_3 CASCADE;

DROP TABLE IF EXISTS public.fenway_sti_clinic_a_100037_s_4 CASCADE;

DROP TABLE IF EXISTS public.fenway_sti_clinic_a_100037_s_5 CASCADE;


