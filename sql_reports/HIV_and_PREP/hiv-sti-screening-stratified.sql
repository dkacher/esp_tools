-- Here is the background: CDC recommends (https://www.cdc.gov/std/tg2015/screening-recommendations.htm) that people with HIV who are sexually active be screened for GC, chlamydia, and syphilis “at first HIV evaluation” and annually after that. Kathleen Roosevelt explained that in MA there is a high rate of HIV co-infection among individuals diagnosed with infectious syphilis (~40%) – and that while it is not possible to assess sexual activity, she hopes to see annual STD screening for ≥75% HIV patients ages 19-49.
 
-- Here are the details. The only thing I am not 100% sure of is the years – what year is a good starting point for HIV data at Atrius?
 
-- ·         Per year, for 2014 through year-to-date 2017:
-- o   Denominator: HIV positive individuals ages 19-49 with ≥1 encounter in the calendar year
-- o   Numerators (3 separate):
-- §  # that received a chlamydia test
-- §  # that received a gonorrhea test
-- §   # that received a syphilis test
 
-- Actual results for the STIs can be ignored for this report. The test can occur any time in the calendar year (i.e. during or after their first encounter for the year).
 
-- Kathleen asked that individuals be deduplicated but that’s obviously not possible (Libby, let’s make sure we point out this limitation in subsequent discussions/demos of MDPHnet and RiskScape).

-- AGES
-- ENCOUNTER TYPES
-- CENTER FILTERING

--DROP TABLE IF EXISTS kre_hiv_sti;
--CREATE TABLE kre_hiv_sti AS
 
-- Get list of all HIV patients and case date
WITH hiv_sti_cases AS (
	SELECT patient_id, id as case_id, date as hiv_date
	FROM nodis_case
	WHERE condition = 'hiv'),
	

-- Get the years hiv patients had encounters (equal to or following hiv case date)
-- Only count the encounter if the patient was of the right age on the date of the encounter (19-49)
-- Get the date of the last encounter for the year to determine patient age group for the report
	hiv_sti_encounters AS (
		SELECT c.patient_id, date_trunc('year', date)::date as rpt_year, 'denom_encounter'::text enc_type, max(date) AS last_encdate_in_yr
		FROM emr_encounter e, hiv_sti_cases c, emr_patient p
		WHERE e.patient_id = c.patient_id
		AND e.patient_id = p.id
		AND c.patient_id = p.id
		AND e.date >= c.hiv_date
		AND e.raw_encounter_type NOT IN ('HISTORY')
		AND date_part('year', age(e.date, date_of_birth)) >= 19 
		AND date_part('year', age(e.date, date_of_birth)) <= 49 
		GROUP by c.patient_id, rpt_year),


-- Gather details on patients (with an enc in that year) that had a chlam test
	hiv_sti_all_chlam_tests AS (
		SELECT e.patient_id, date_trunc('year', date)::date as rpt_year, 'chlam_tests'::text enc_type, last_encdate_in_yr
		FROM hef_event h, hiv_sti_encounters e
		WHERE date_trunc('year', date)::date = e.rpt_year
		AND h.patient_id = e.patient_id
		AND h.name ilike 'lx:chlamydia%'
		GROUP BY e.patient_id, date_trunc('year', date)::date, last_encdate_in_yr),
	

-- Gather details on patients (with an enc in that year) that had a gonorrhea test
	hiv_sti_all_gon_tests AS (
		SELECT e.patient_id, date_trunc('year', date)::date as rpt_year, 'gon_tests'::text enc_type, last_encdate_in_yr
		FROM hef_event h, hiv_sti_encounters e
		WHERE date_trunc('year', date)::date = e.rpt_year
		AND h.patient_id = e.patient_id
		AND h.name ilike 'lx:gonorrhea%'
		GROUP BY e.patient_id, date_trunc('year', date)::date, last_encdate_in_yr),
		

-- Gather details on patients (with an enc in that year) that had a syphilis test
	hiv_sti_all_syph_tests AS (
		SELECT e.patient_id, date_trunc('year', date)::date as rpt_year, 'syph_tests'::text enc_type, last_encdate_in_yr
		FROM hef_event h, hiv_sti_encounters e
		WHERE date_trunc('year', date)::date = e.rpt_year
		AND h.patient_id = e.patient_id
		AND (h.name ilike 'lx:rpr:%'
		or h.name ilike 'lx:vdrl:%'
		or h.name ilike 'lx:tppa:%'
		or h.name ilike 'lx:fta-abs:%'
		or h.name ilike 'lx:tp-igg:%'
		or h.name ilike 'lx:vdrl-csf:%'
		or h.name ilike 'lx:fta-abs-csf:%'
		or h.name ilike 'lx:tppa-csf:%')
		GROUP BY e.patient_id, date_trunc('year', date)::date, last_encdate_in_yr),

		
-- UNION TOGETHER ALL DETAILS
	hiv_sti_union AS (
	SELECT * from hiv_sti_encounters
	UNION
	SELECT * from hiv_sti_all_chlam_tests
	UNION
	SELECT * from hiv_sti_all_gon_tests
	UNION
	SELECT * from hiv_sti_all_syph_tests),
	
-- ADD IN PATIENT PATIENT DETAILS
	hiv_sti_pat_details AS (
		SELECT patient_id, 
		rpt_year,
		enc_type, 
		date_part('year', age(last_encdate_in_yr, date_of_birth)) age,
		CASE   
		when date_part('year', age(last_encdate_in_yr, date_of_birth)) <= 18 then '0-18'    
		when date_part('year', age(last_encdate_in_yr, date_of_birth)) <= 29 then '19-29'  
		when date_part('year', age(last_encdate_in_yr, date_of_birth)) <= 39 then '30-39' 
		when date_part('year', age(last_encdate_in_yr, date_of_birth)) <= 49 then '40-49' 
		when date_part('year', age(last_encdate_in_yr, date_of_birth)) <= 79 then '50-79'   
		when date_of_birth is null then 'UNKNOWN AGE'
		else '>= 80' end age_group_10_yr,
		natural_key,
		sex,
		CASE when race_ethnicity = 6 then 'hispanic'  
		when race_ethnicity = 5 then 'white' 
		when race_ethnicity = 3 then 'black' 
		when race_ethnicity = 2 then 'asian' 
		when race_ethnicity = 1 then 'native_american' 
		when race_ethnicity = 0 then 'unknown' 
		end race
		FROM hiv_sti_union sti
		INNER JOIN public.emr_patient p ON (sti.patient_id = p.id )
		INNER JOIN esp_mdphnet.esp_demographic d on (p.natural_key = d.patid)
	)
		
-- COUNT AND PREPARE FINAL OUTPUT
SELECT
sex, age_group_10_yr, race,
count(case when  rpt_year = '2006-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_06,
count(case when  rpt_year = '2007-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_07,
count(case when  rpt_year = '2008-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_08,
count(case when  rpt_year = '2009-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_09,
count(case when  rpt_year = '2010-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_10,
count(case when  rpt_year = '2011-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_11,
count(case when  rpt_year = '2012-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_12,
count(case when  rpt_year = '2013-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_13,
count(case when  rpt_year = '2014-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_14,
count(case when  rpt_year = '2015-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_15,
count(case when  rpt_year = '2016-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_16,
count(case when  rpt_year = '2017-01-01' and enc_type = 'denom_encounter' then 1 end)  denom_encs_17,
count(case when  rpt_year = '2006-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_06,
count(case when  rpt_year = '2007-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_07,
count(case when  rpt_year = '2008-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_08,
count(case when  rpt_year = '2009-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_09,
count(case when  rpt_year = '2010-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_10,
count(case when  rpt_year = '2011-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_11,
count(case when  rpt_year = '2012-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_12,
count(case when  rpt_year = '2013-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_13,
count(case when  rpt_year = '2014-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_14,
count(case when  rpt_year = '2015-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_15,
count(case when  rpt_year = '2016-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_16,
count(case when  rpt_year = '2017-01-01' and enc_type = 'chlam_tests' then 1 end)  chlam_tests_17,
count(case when  rpt_year = '2006-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_06,
count(case when  rpt_year = '2007-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_07,
count(case when  rpt_year = '2008-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_08,
count(case when  rpt_year = '2009-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_09,
count(case when  rpt_year = '2010-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_10,
count(case when  rpt_year = '2011-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_11,
count(case when  rpt_year = '2012-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_12,
count(case when  rpt_year = '2013-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_13,
count(case when  rpt_year = '2014-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_14,
count(case when  rpt_year = '2015-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_15,
count(case when  rpt_year = '2016-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_16,
count(case when  rpt_year = '2017-01-01' and enc_type = 'gon_tests' then 1 end)  gon_tests_17,
count(case when  rpt_year = '2006-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_06,
count(case when  rpt_year = '2007-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_07,
count(case when  rpt_year = '2008-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_08,
count(case when  rpt_year = '2009-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_09,
count(case when  rpt_year = '2010-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_10,
count(case when  rpt_year = '2011-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_11,
count(case when  rpt_year = '2012-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_12,
count(case when  rpt_year = '2013-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_13,
count(case when  rpt_year = '2014-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_14,
count(case when  rpt_year = '2015-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_15,
count(case when  rpt_year = '2016-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_16,
count(case when  rpt_year = '2017-01-01' and enc_type = 'syph_tests' then 1 end)  syph_tests_17
FROM hiv_sti_pat_details
GROUP BY sex, age_group_10_yr, race
ORDER by sex, age_group_10_yr, race;



		


