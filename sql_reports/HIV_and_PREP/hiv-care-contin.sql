﻿DROP TABLE IF EXISTS hiv_ll_indexpats;
DROP TABLE IF EXISTS hiv_ll_labs;
DROP TABLE IF EXISTS hiv_ll_new_diag;
DROP TABLE IF EXISTS hiv_ll_viral_loads;
DROP TABLE IF EXISTS hiv_ll_cd4;
DROP TABLE IF EXISTS hiv_ll_meds;
DROP TABLE IF EXISTS hiv_ll_trmt;
DROP TABLE IF EXISTS hiv_ll_encounters_all;
DROP TABLE IF EXISTS hiv_ll_visits;
DROP TABLE IF EXISTS hiv_ll_enc_dx_codes;
DROP TABLE IF EXISTS hiv_ll_hiv_dx;
DROP TABLE IF EXISTS hiv_ll_oi;
DROP TABLE IF EXISTS hiv_ll_contin_of_care;

-- Index Patients
-- All patients with a HIV case
CREATE table hiv_ll_indexpats AS
SELECT patient_id, date case_date 
FROM nodis_case
WHERE condition = 'hiv';

-- HIV Labs For Index Patients
CREATE TABLE hiv_ll_labs AS
SELECT T1.patient_id, native_name, test_name, T1.date lab_date, T3.name, T3.date hef_date, T1.result_string, date_trunc('year', T1.date)::date lab_year, natural_key lab_id
FROM emr_labresult T1
LEFT JOIN conf_labtestmap T2 ON T1.native_code = T2.native_code
LEFT JOIN hef_event T3 ON T1.id = T3.object_id AND T1.patient_id = T3.patient_id
WHERE (T2.test_name ilike '%hiv%' or T2.test_name = 'cd4')
AND T1.patient_id in (SELECT patient_id FROM hiv_ll_indexpats)
ORDER BY patient_id, lab_date, test_name, result_string;


-- HIV - New Diagnosis Computation
-- Defined as (positive +Western Blot or +ELISA or +Ab_Ag) on or before the NODIS case date 
-- or (history of negative HIV ELISA or Ab_Ag within 2 years preceding the NODIS case date). Code as 1 for yes, 0 for no.
CREATE TABLE hiv_ll_new_diag AS 
SELECT T2.patient_id, 
MAX(case  when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive') and T1.hef_date <=  T2.case_date then 1  
          when T1.name IN ('lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative') and T1.hef_date >= (T2.case_date - INTERVAL '2 years') then 1 else 0 end) new_hiv_diagnosis 
FROM hiv_ll_labs T1 
RIGHT OUTER JOIN hiv_ll_indexpats T2 ON ((T1.patient_id = T2.patient_id))  
GROUP BY T2.patient_id;

-- Viral load assays
-- One variable to indicate the date of a patient’s first viral load following the case detection date. 
-- If he or she does not have any viral load results following case detection, then leave blank.  
-- For each calendar year, two variables should be created:
-- One variable to indicate the number of viral load assays with any result value a patient has had during the calendar year (if no viral load assays performed, then variable should equal 0).  
-- A second variable to indicate the value of the last viral load assay occurring during the calendar year. Include entire string, including ‘<’, ‘>’, ‘below’, ‘undetected’, ‘not-detected’, etc.

CREATE TABLE hiv_ll_viral_loads AS
SELECT T1.patient_id,
max(first_vl_date_after_case) first_vl_date_after_case,
count(case when  T2.lab_year = '2006-01-01' then 1 end)  total_vl_06,
max(case when T2.lab_year = '2006-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_06,
count(case when  T2.lab_year = '2007-01-01' then 1 end)  total_vl_07,
max(case when T2.lab_year = '2007-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_07,
count(case when  T2.lab_year = '2008-01-01' then 1 end)  total_vl_08,
max(case when T2.lab_year = '2008-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_08,
count(case when  T2.lab_year = '2009-01-01' then 1 end)  total_vl_09,
max(case when T2.lab_year = '2009-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_09,
count(case when  T2.lab_year = '2010-01-01' then 1 end)  total_vl_10,
max(case when T2.lab_year = '2010-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_10,
count(case when  T2.lab_year = '2011-01-01' then 1 end)  total_vl_11,
max(case when T2.lab_year = '2011-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_11,
count(case when  T2.lab_year = '2012-01-01' then 1 end)  total_vl_12,
max(case when T2.lab_year = '2012-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_12,
count(case when  T2.lab_year = '2013-01-01' then 1 end)  total_vl_13,
max(case when T2.lab_year = '2013-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_13,
count(case when  T2.lab_year = '2014-01-01' then 1 end)  total_vl_14,
max(case when T2.lab_year = '2014-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_14,
count(case when  T2.lab_year = '2015-01-01' then 1 end)  total_vl_15,
max(case when T2.lab_year = '2015-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_15,
count(case when  T2.lab_year = '2016-01-01' then 1 end)  total_vl_16,
max(case when T2.lab_year = '2016-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_16,
count(case when  T2.lab_year = '2017-01-01' then 1 end)  total_vl_17,
max(case when T2.lab_year = '2017-01-01' and T2.lab_date = max_vl_date_yr THEN last_vl_result_yr END) last_vl_result_17
FROM hiv_ll_indexpats T1
LEFT JOIN (select DISTINCT ON (lab_id) patient_id, native_name, test_name, lab_date, result_string, lab_year, lab_id
		 FROM hiv_ll_labs
		 WHERE test_name = 'hiv_rna_viral' )T2 ON T1.patient_id = T2.patient_id
LEFT JOIN (SELECT patient_id, max(lab_date) max_vl_date_yr, lab_year 
		 FROM hiv_ll_labs 
		 WHERE test_name = 'hiv_rna_viral' GROUP by patient_id, lab_year) T3 
		 ON T1.patient_id = T3.patient_id and T2.lab_year = T3.lab_year
LEFT JOIN (SELECT patient_id, array_agg(distinct(result_string)) last_vl_result_yr, lab_year, lab_date 
		 FROM hiv_ll_labs 
		 WHERE test_name = 'hiv_rna_viral' GROUP by patient_id, lab_year, lab_date) T4 
		 ON T1.patient_id = T4.patient_id AND T2.lab_date = T4.lab_date AND T2.lab_year = T4.lab_year AND T3.lab_year = T4.lab_year
LEFT JOIN (SELECT TT2.patient_id, min(lab_date) first_vl_date_after_case
		FROM hiv_ll_labs TT1, hiv_ll_indexpats TT2  
		WHERE test_name = 'hiv_rna_viral' AND lab_date > case_date AND TT1.patient_id = TT2.patient_id	GROUP by TT2.patient_id) T5
		ON T1.patient_id = T5. patient_id
GROUP BY T1.patient_id;


-- CD-4 Counts
-- One variable to indicate the date of a patient’s first CD4 count following the case detection date. If he or she does not have any CD4 results following case detection, then leave blank.  
-- For each calendar year, create one variable to indicate the number of CD4 counts with any result value a patient has had during the calendar year (if no CD4 counts performed, then variable should equal 0).  

CREATE TABLE hiv_ll_cd4 AS
SELECT T1.patient_id,
max(first_cd4_date_after_case) first_cd4_date_after_case,
count(case when  T2.lab_year = '2006-01-01' then 1 end)  total_cd4_06,
max(case when T2.lab_year = '2006-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_06,
count(case when  T2.lab_year = '2007-01-01' then 1 end)  total_cd4_07,
max(case when T2.lab_year = '2007-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_07,
count(case when  T2.lab_year = '2008-01-01' then 1 end)  total_cd4_08,
max(case when T2.lab_year = '2008-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_08,
count(case when  T2.lab_year = '2009-01-01' then 1 end)  total_cd4_09,
max(case when T2.lab_year = '2009-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_09,
count(case when  T2.lab_year = '2010-01-01' then 1 end)  total_cd4_10,
max(case when T2.lab_year = '2010-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_10,
count(case when  T2.lab_year = '2011-01-01' then 1 end)  total_cd4_11,
max(case when T2.lab_year = '2011-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_11,
count(case when  T2.lab_year = '2012-01-01' then 1 end)  total_cd4_12,
max(case when T2.lab_year = '2012-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_12,
count(case when  T2.lab_year = '2013-01-01' then 1 end)  total_cd4_13,
max(case when T2.lab_year = '2013-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_13,
count(case when  T2.lab_year = '2014-01-01' then 1 end)  total_cd4_14,
max(case when T2.lab_year = '2014-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_14,
count(case when  T2.lab_year = '2015-01-01' then 1 end)  total_cd4_15,
max(case when T2.lab_year = '2015-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_15,
count(case when  T2.lab_year = '2016-01-01' then 1 end)  total_cd4_16,
max(case when T2.lab_year = '2016-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_16,
count(case when  T2.lab_year = '2017-01-01' then 1 end)  total_cd4_17,
max(case when T2.lab_year = '2017-01-01' and T2.lab_date = max_cd4_date_yr THEN last_cd4_result_yr END) last_cd4_result_17
FROM hiv_ll_indexpats T1
LEFT JOIN (select DISTINCT ON (lab_id) patient_id, native_name, test_name, lab_date, result_string, lab_year, lab_id
		 FROM hiv_ll_labs
		 WHERE test_name = 'cd4' )T2 ON T1.patient_id = T2.patient_id
LEFT JOIN (SELECT patient_id, max(lab_date) max_cd4_date_yr, lab_year 
		 FROM hiv_ll_labs
		 WHERE test_name = 'cd4' GROUP by patient_id, lab_year) T3 
		 ON T1.patient_id = T3.patient_id and T2.lab_year = T3.lab_year
LEFT JOIN (SELECT patient_id, array_agg(distinct(result_string)) last_cd4_result_yr, lab_year, lab_date 
		 FROM hiv_ll_labs
		 WHERE test_name = 'cd4' GROUP by patient_id, lab_year, lab_date) T4 
		 ON T1.patient_id = T4.patient_id AND T2.lab_date = T4.lab_date AND T2.lab_year = T4.lab_year AND T3.lab_year = T4.lab_year
LEFT JOIN (SELECT TT2.patient_id, min(lab_date) first_cd4_date_after_case
		FROM hiv_ll_labs TT1, hiv_ll_indexpats TT2  
		WHERE test_name = 'cd4' AND lab_date > case_date AND TT1.patient_id = TT2.patient_id	GROUP by TT2.patient_id) T5
		ON T1.patient_id = T5. patient_id
GROUP BY T1.patient_id;



-- Treatment
-- Overall
-- One variable to indicate the date of the first medication listed in Table 1 that a patient receives following the case detection date. If he or she does not receive any treatment following case detection, then leave blank. 
-- For each calendar year, create: 
-- One variable to indicate whether or not a patient has been prescribed any of the medications listed in Table 1 during that year (code as 1 for yes, 0 for no).  
 
CREATE TABLE hiv_ll_meds AS
SELECT T2.patient_id, T1.date, name, date_trunc('year', T1.date)::date med_year
FROM hef_event T1,
hiv_ll_indexpats T2
WHERE T1.patient_id = T2.patient_id
AND name ilike 'rx:hiv%';

CREATE TABLE hiv_ll_trmt AS
SELECT T1.patient_id,
max(first_med_date_after_case) first_med_date_after_case,
max(case when med_year = '2006-01-01' THEN 1 ELSE 0 END) treatment_06,
max(case when med_year = '2007-01-01' THEN 1 ELSE 0 END) treatment_07,
max(case when med_year = '2008-01-01' THEN 1 ELSE 0 END) treatment_08,
max(case when med_year = '2009-01-01' THEN 1 ELSE 0 END) treatment_09,
max(case when med_year = '2010-01-01' THEN 1 ELSE 0 END) treatment_10,
max(case when med_year = '2011-01-01' THEN 1 ELSE 0 END) treatment_11,
max(case when med_year = '2012-01-01' THEN 1 ELSE 0 END) treatment_12,
max(case when med_year = '2013-01-01' THEN 1 ELSE 0 END) treatment_13,
max(case when med_year = '2014-01-01' THEN 1 ELSE 0 END) treatment_14,
max(case when med_year = '2015-01-01' THEN 1 ELSE 0 END) treatment_15,
max(case when med_year = '2016-01-01' THEN 1 ELSE 0 END) treatment_16,
max(case when med_year = '2017-01-01' THEN 1 ELSE 0 END) treatment_17
FROM hiv_ll_indexpats  T1
LEFT JOIN hiv_ll_meds T2 ON T1.patient_id = T2.patient_id
LEFT JOIN (SELECT TT2.patient_id, min(date) first_med_date_after_case
		FROM hiv_ll_meds TT1, hiv_ll_indexpats TT2  
		WHERE TT1.date > case_date AND TT1.patient_id = TT2.patient_id	GROUP by TT2.patient_id) T3
		ON T1.patient_id = T3. patient_id
GROUP BY T1.patient_id;


-- Outpatient visits
-- For each calendar year, three variables should be created:
-- One variable to indicate the number of outpatient visits a patient has had during the calendar year (if no visits, then variable should equal 0).  
-- A second variable to indicate the date of their first outpatient visit during the calendar year. 
-- A third variable to indicate if a patient had an outpatient visit by September 1st of that year and a second outpatient visit at least 90 days following the first visit (code as 1 for yes, 0 for no). 

CREATE TABLE hiv_ll_encounters_all AS
SELECT T1.patient_id, id encounter_id, date, date_trunc('year', T2.date)::date enc_year, raw_encounter_type
FROM hiv_ll_indexpats T1, emr_encounter T2
WHERE T1.patient_id = T2.patient_id
AND (raw_encounter_type != 'NO SHOW' or raw_encounter_type != 'APPT CANCELLED');


CREATE TABLE hiv_ll_visits AS
SELECT T1.patient_id,
count(case when  T2.enc_year = '2006-01-01' then 1 end)  total_enc_06,
min(CASE WHEN T2.enc_year = '2006-01-01' THEN T2.date END) first_enc_date_06,
max(CASE WHEN T2.enc_year = '2006-01-01' and T2.date <= '2006-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_06,
count(case when  T2.enc_year = '2007-01-01' then 1 end)  total_enc_07,
min(CASE WHEN T2.enc_year = '2007-01-01' THEN T2.date END) first_enc_date_07,
max(CASE WHEN T2.enc_year = '2007-01-01' and T2.date <= '2007-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_07,
count(case when  T2.enc_year = '2008-01-01' then 1 end)  total_enc_08,
min(CASE WHEN T2.enc_year = '2008-01-01' THEN T2.date END) first_enc_date_08,
max(CASE WHEN T2.enc_year = '2008-01-01' and T2.date <= '2008-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_08,
count(case when  T2.enc_year = '2009-01-01' then 1 end)  total_enc_09,
min(CASE WHEN T2.enc_year = '2009-01-01' THEN T2.date END) first_enc_date_09,
max(CASE WHEN T2.enc_year = '2009-01-01' and T2.date <= '2009-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_09,
count(case when  T2.enc_year = '2010-01-01' then 1 end)  total_enc_10,
min(CASE WHEN T2.enc_year = '2010-01-01' THEN T2.date END) first_enc_date_10,
max(CASE WHEN T2.enc_year = '2010-01-01' and T2.date <= '2010-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_10,
count(case when  T2.enc_year = '20011-01-01' then 1 end)  total_enc_11,
min(CASE WHEN T2.enc_year = '20011-01-01' THEN T2.date END) first_enc_date_11,
max(CASE WHEN T2.enc_year = '2011-01-01' and T2.date <= '2011-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_11,
count(case when  T2.enc_year = '2012-01-01' then 1 end)  total_enc_12,
min(CASE WHEN T2.enc_year = '2012-01-01' THEN T2.date END) first_enc_date_12,
max(CASE WHEN T2.enc_year = '2012-01-01' and T2.date <= '2012-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_12,
count(case when  T2.enc_year = '2013-01-01' then 1 end)  total_enc_13,
min(CASE WHEN T2.enc_year = '2013-01-01' THEN T2.date END) first_enc_date_13,
max(CASE WHEN T2.enc_year = '2013-01-01' and T2.date <= '2013-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_13,
count(case when  T2.enc_year = '2014-01-01' then 1 end)  total_enc_14,
min(CASE WHEN T2.enc_year = '2014-01-01' THEN T2.date END) first_enc_date_14,
max(CASE WHEN T2.enc_year = '2014-01-01' and T2.date <= '2014-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_14,
count(case when  T2.enc_year = '2015-01-01' then 1 end)  total_enc_15,
min(CASE WHEN T2.enc_year = '2015-01-01' THEN T2.date END) first_enc_date_15,
max(CASE WHEN T2.enc_year = '2015-01-01' and T2.date <= '2015-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_15,
count(case when  T2.enc_year = '2016-01-01' then 1 end)  total_enc_16,
min(CASE WHEN T2.enc_year = '2016-01-01' THEN T2.date END) first_enc_date_16,
max(CASE WHEN T2.enc_year = '2016-01-01' and T2.date <= '2016-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_16,
count(case when  T2.enc_year = '2017-01-01' then 1 end)  total_enc_17,
min(CASE WHEN T2.enc_year = '2017-01-01' THEN T2.date END) first_enc_date_17,
max(CASE WHEN T2.enc_year = '2017-01-01' and T2.date <= '2017-09-01'  AND enc_spread >= 90 THEN 1 ELSE 0 END) enc_by_sep_plus90_17
FROM hiv_ll_indexpats T1
LEFT JOIN hiv_ll_encounters_all T2 ON T1.patient_id = T2.patient_id
LEFT JOIN (SELECT patient_id, (max(date) - min(date)) enc_spread, enc_year 
	   FROM hiv_ll_encounters_all
	   GROUP BY patient_id, enc_year) T3 ON T1.patient_id = T3.patient_id AND T2.enc_year = T3.enc_year
GROUP BY T1.patient_id
ORDER BY T1.patient_id;

-- HIV diagnosis code
-- For each calendar year, create:
-- One variable to indicate the number of diagnosis codes for HIV (listed in Table 2) a patient had during the calendar year (if no HIV diagnoses, then variable should equal 0).  

CREATE TABLE hiv_ll_enc_dx_codes AS
SELECT * 
FROM emr_encounter_dx_codes 
WHERE dx_code_id ~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)'
OR dx_code_id ~ '^(icd9:130.|icd10:B58.|icd9:010|icd9:011|icd9:012|icd9:013|icd9:014|icd9:015|icd9:016|icd9:017|icd9:018|icd10:A15|icd10:A16|icd10:A17|icd10:A18|icd10:A19)'
OR dx_code_id in (
'icd9:136.3',
'icd10:B59',
'icd9:117.5',
'icd9:321.0',
'icd10:B45.0',
'icd10:B45.1',
'icd10:B45.7',
'icd10:B45.9',
'icd9:007.4',
'icd10:A07.2',
'icd9:031.2',
'icd10:A31.2',
'icd9:046.3',
'icd10:A81.2',
'icd9:112.0',
'icd9:112.4',
'icd9:112.5',
'icd9:112.84',
'icd10:B37.0',
'icd10:B37.1',
'icd10:B37.81',
'icd10:B37.83');


CREATE TABLE hiv_ll_hiv_dx AS
SELECT 
T1.patient_id, 
COUNT(case when  T2.enc_year = '2006-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_06,
COUNT(case when  T2.enc_year = '2007-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_07,
COUNT(case when  T2.enc_year = '2008-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_08,
COUNT(case when  T2.enc_year = '2009-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_09,
COUNT(case when  T2.enc_year = '2010-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_10,
COUNT(case when  T2.enc_year = '2011-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_11,
COUNT(case when  T2.enc_year = '2012-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_12,
COUNT(case when  T2.enc_year = '2013-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_13,
COUNT(case when  T2.enc_year = '2014-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_14,
COUNT(case when  T2.enc_year = '2015-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_15,
COUNT(case when  T2.enc_year = '2016-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_16,
COUNT(case when  T2.enc_year = '2017-01-01' AND T3.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then 1 END) total_hiv_dx_17
FROM  hiv_ll_indexpats T1
LEFT JOIN hiv_ll_encounters_all T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN hiv_ll_enc_dx_codes T3 ON (T2.encounter_id = T3.encounter_id)  
GROUP BY T1.patient_id;

-- Opportunistic infections
-- For each of the following opportunistic infections listed in Table 3, create one variable to indicate the first date on which a patient has a diagnosis code for that OI following case detection. 
-- If a patient does not have a diagnosis code, then variable should be left blank.

CREATE TABLE hiv_ll_oi AS
SELECT T1.patient_id,
min(case when T3.dx_code_id IN ('icd9:136.3','icd10:B59') AND T2.date > T1.case_date then T2.date END) pcp,
min(CASE WHEN T3.dx_code_id ~ '^icd9:130.|icd10:B58.' AND T2.date > T1.case_date THEN T2.date END) toxo,
min(CASE WHEN T3.dx_code_id IN ('icd9:117.5','icd9:321.0','icd10:B45.0','icd10:B45.1','icd10:B45.7','icd10:B45.9') AND T2.date > T1.case_date THEN T2.date END) cm,
min(CASE WHEN T3.dx_code_id IN ('icd9:007.4','icd10:A07.2') AND T2.date > T1.case_date THEN T2.date END) crypto,
min(CASE WHEN T3.dx_code_id ~ '^icd9:010|icd9:011|icd9:012|icd9:013|icd9:014|icd9:015|icd9:016|icd9:017|icd9:018|icd10:A15|icd10:A16|icd10:A17|icd10:A18|icd10:A19' AND T2.date > T1.case_date THEN T2.date END) tb,
min(CASE WHEN T3.dx_code_id IN ('icd9:112.0','icd9:112.4','icd9:112.5','icd9:112.84','icd10:B37.0','icd10:B37.1','icd10:B37.81','icd10:B37.83') AND T2.date > T1.case_date THEN T2.date END) mc,
min(CASE WHEN T3.dx_code_id IN ('icd9:031.2','icd10:A31.2') AND T2.date > T1.case_date THEN T2.date END) mac,
min(CASE WHEN T3.dx_code_id IN ('icd9:046.3','icd10:A81.2') AND T2.date > T1.case_date THEN T2.date END) pml
FROM  hiv_ll_indexpats T1
LEFT JOIN hiv_ll_encounters_all T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN hiv_ll_enc_dx_codes T3 ON (T2.encounter_id = T3.encounter_id)  
GROUP BY T1.patient_id;


-- Prepare the output
CREATE TABLE hiv_ll_contin_of_care AS
SELECT T1.patient_id,
T2.center_id,
T1.case_date,
date_part('year', age(case_date, date_of_birth)) age_at_detection,
gender,
race,
new_hiv_diagnosis,
first_vl_date_after_case,
total_vl_06,
last_vl_result_06, 
total_vl_07,
last_vl_result_07, 
total_vl_08,
last_vl_result_08, 
total_vl_09,
last_vl_result_09, 
total_vl_10,
last_vl_result_10, 
total_vl_11,
last_vl_result_11, 
total_vl_12,
last_vl_result_12, 
total_vl_13,
last_vl_result_13, 
total_vl_14,
last_vl_result_14, 
total_vl_15,
last_vl_result_15, 
total_vl_16,
last_vl_result_16, 
total_vl_17,
last_vl_result_17,
first_cd4_date_after_case,
total_cd4_06,
last_cd4_result_06, 
total_cd4_07,
last_cd4_result_07, 
total_cd4_08,
last_cd4_result_08, 
total_cd4_09,
last_cd4_result_09, 
total_cd4_10,
last_cd4_result_10, 
total_cd4_11,
last_cd4_result_11, 
total_cd4_12,
last_cd4_result_12, 
total_cd4_13,
last_cd4_result_13, 
total_cd4_14,
last_cd4_result_14, 
total_cd4_15,
last_cd4_result_15, 
total_cd4_16,
last_cd4_result_16, 
total_cd4_17,
last_cd4_result_17, 
first_med_date_after_case,
treatment_06,
treatment_07,
treatment_08,
treatment_09,
treatment_10,
treatment_11,
treatment_12,
treatment_13,
treatment_14,
treatment_15,
treatment_16,
treatment_17,
total_enc_06,
first_enc_date_06,
enc_by_sep_plus90_06,
total_enc_07,
first_enc_date_07,
enc_by_sep_plus90_07,
total_enc_08,
first_enc_date_08,
enc_by_sep_plus90_08,
total_enc_09,
first_enc_date_09,
enc_by_sep_plus90_09,
total_enc_10,
first_enc_date_10,
enc_by_sep_plus90_10,
total_enc_11,
first_enc_date_11,
enc_by_sep_plus90_11,
total_enc_12,
first_enc_date_12,
enc_by_sep_plus90_12,
total_enc_13,
first_enc_date_13,
enc_by_sep_plus90_13,
total_enc_14,
first_enc_date_14,
enc_by_sep_plus90_14,
total_enc_15,
first_enc_date_15,
enc_by_sep_plus90_15,
total_enc_16,
first_enc_date_16,
enc_by_sep_plus90_16,
total_enc_17,
first_enc_date_17,
enc_by_sep_plus90_17,
total_hiv_dx_06,
total_hiv_dx_07,
total_hiv_dx_08,
total_hiv_dx_09,
total_hiv_dx_10,
total_hiv_dx_11,
total_hiv_dx_12,
total_hiv_dx_13,
total_hiv_dx_14,
total_hiv_dx_15,
total_hiv_dx_16,
total_hiv_dx_17,
pcp,
toxo,
cm,
crypto,
tb,
mc,
mac,
pml
FROM hiv_ll_indexpats T1
LEFT JOIN emr_patient T2 ON (T1.patient_id = T2.id)
LEFT JOIN hiv_ll_new_diag T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN hiv_ll_viral_loads T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN hiv_ll_cd4 T5 ON (T1.patient_id = T5.patient_id)
LEFT JOIN hiv_ll_trmt T6 ON (T1.patient_id = T6.patient_id)
LEFT JOIN hiv_ll_visits T7 ON (T1.patient_id = T7.patient_id)
LEFT JOIN hiv_ll_hiv_dx T8 ON (T1.patient_id = T8.patient_id)
LEFT JOIN hiv_ll_oi T9 ON (T1.patient_id = T9.patient_id)
ORDER BY patient_id;

DROP TABLE IF EXISTS hiv_ll_indexpats;
DROP TABLE IF EXISTS hiv_ll_labs;
DROP TABLE IF EXISTS hiv_ll_new_diag;
DROP TABLE IF EXISTS hiv_ll_viral_loads;
DROP TABLE IF EXISTS hiv_ll_cd4;
DROP TABLE IF EXISTS hiv_ll_meds;
DROP TABLE IF EXISTS hiv_ll_trmt;
DROP TABLE IF EXISTS hiv_ll_encounters_all;
DROP TABLE IF EXISTS hiv_ll_visits;
DROP TABLE IF EXISTS hiv_ll_enc_dx_codes;
DROP TABLE IF EXISTS hiv_ll_hiv_dx;
DROP TABLE IF EXISTS hiv_ll_oi;