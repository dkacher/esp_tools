-- PREP R21
-- Population-Level Effects of Increasing PrEP Uptake on Incidence of HIV and Bacterial STIs

-- Person-time data set with one record per individual per month of follow-up. 

-- The following inclusion criteria at some point during 2012-2017: 
-- 1) Male; 
-- 2) aged ≥15
-- 3) an HIV-negative test. 
-- The first month of follow-up (month=0) for an individual will be the first month when all of these inclusion criteria are met. 

CREATE SCHEMA IF NOT EXISTS hiv_rpt;


SET SEARCH_PATH TO hiv_rpt, public;

DROP TABLE IF EXISTS hiv_rpt.r21_hiv_neg_labs;
DROP TABLE IF EXISTS hiv_rpt.r21_potential_pats;
DROP TABLE IF EXISTS hiv_rpt.r21_potential_pats_details;
DROP TABLE IF EXISTS hiv_rpt.r21_index_pats;

DROP TABLE IF EXISTS hiv_rpt.r21_month_intervals;

DROP TABLE IF EXISTS hiv_rpt.r21_hef_w_lab_details;

DROP TABLE IF EXISTS hiv_rpt.r21_gon_chlam_events;
DROP TABLE IF EXISTS hiv_rpt.r21_gon_chlam_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_syph_events;
DROP TABLE IF EXISTS hiv_rpt.r21_syph_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_sti_tst_episodes;
DROP TABLE IF EXISTS hiv_rpt.r21_gon_chlam_rectal_tst_episodes;
DROP TABLE IF EXISTS hiv_rpt.r21_gon_chlam_throat_tst_episodes;

DROP TABLE IF EXISTS hiv_rpt.r21_esp_cases;
DROP TABLE IF EXISTS hiv_rpt.r21_case_counts;
DROP TABLE IF EXISTS hiv_rpt.sti_case_encs;

DROP TABLE IF EXISTS hiv_rpt.r21_encounters;

DROP TABLE IF EXISTS hiv_rpt.r21_hiv_events;
DROP TABLE IF EXISTS hiv_rpt.r21_hiv_events_dist_by_date;
DROP TABLE IF EXISTS hiv_rpt.r21_hiv_counts_nohef;
DROP TABLE IF EXISTS hiv_rpt.r21_hiv_counts_hef;
DROP TABLE IF EXISTS hiv_rpt.r21_hiv_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_hep_events;
DROP TABLE IF EXISTS hiv_rpt.r21_hep_events_dist_by_date;
DROP TABLE IF EXISTS hiv_rpt.r21_hep_counts_nohef;
DROP TABLE IF EXISTS hiv_rpt.r21_hep_counts_hef;
DROP TABLE IF EXISTS hiv_rpt.r21_hep_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_hiv_meds;
DROP TABLE IF EXISTS hiv_rpt.r21_non_hiv_rx_of_interest;
DROP TABLE IF EXISTS hiv_rpt.r21_all_rx_of_interest;
DROP TABLE IF EXISTS hiv_rpt.r21_rx_of_int_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_dx_codes;
DROP TABLE IF EXISTS hiv_rpt.r21_dx_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_bmi_measurements;
DROP TABLE IF EXISTS hiv_rpt.r21_bmi_counts;


DROP TABLE IF EXISTS hiv_rpt.r21_baseline_table;
DROP TABLE IF EXISTS hiv_rpt.r21_output_table;

DROP SEQUENCE IF EXISTS hiv_rpt.r21_masked_id_seq;
DROP TABLE IF EXISTS hiv_rpt.r21_masked_patients;
DROP TABLE IF EXISTS hiv_rpt.r21_output_table_masked;


-------------------
------------------- IDENTIFY INDEX PATIENTS
-------------------

	
-- Generate Starting Potential Patients List Based on specific HIV Negative Tests
-- Exclude Patients Who Have Had a Positive HIV test Prior to Study Period
-- Exclude Patients Who Have An ESP Case Prior to Study Period
-- FOR NOW LEAVE OFF HIV CASE EXCLUSION
CREATE TABLE hiv_rpt.r21_hiv_neg_labs AS
SELECT T1.patient_id, T1.name, T1.date as hiv_hef_neg_date
FROM hef_event T1
WHERE T1.date >= '01-01-2012'
AND T1.date < '01-01-2018'
AND T1.name in ('lx:hiv_elisa:negative',
                 'lx:hiv_ag_ab:negative',
				 'lx:hiv_wb:negative',
				 'lx:hiv_geenius:negative',
				 'lx:hiv_multispot:negative')
AND patient_id NOT IN (SELECT T1.patient_id
    FROM hef_event T1
    WHERE T1.name ilike 'lx:hiv%:positive'
    AND T1.date <= '01-01-2012')
	;
--AND patient_id NOT IN (SELECT T1.patient_id
--    FROM nodis_case T1
--	WHERE T1.condition = 'hiv'
--	AND T1.date <= '01-01-2012');

-- All individuals that are male and at least 15 on 12/31/2017 
-- Include HIV lab date (when the patient was 15 on lab test date) 
CREATE TABLE hiv_rpt.r21_potential_pats AS
SELECT T1.id as master_patient_id,
date_of_birth::date,
min(hiv_hef_neg_date) abs_index_date,
(date_of_birth + INTERVAL '15 years')::date date_turned_15
FROM emr_patient T1,
hiv_rpt.r21_hiv_neg_labs T2
WHERE T1.id = T2.patient_id
-- only include males
AND gender in ('M', 'MALE')
-- exclude patients that are not 15 on last day of reporting year
AND date_part('year', age('2017-12-31', date_of_birth)) >= 15
-- patient must be 15 on the date of the neg_hiv_test_date
AND (date_of_birth + INTERVAL '15 years')::date <= hiv_hef_neg_date
GROUP by T1.id;

-- COMMENTING OUT UNTIL BMC HAS THE FIELDS
-- -- Index Month, Index Patients & Demographics
-- -- Exclude Patients with a Positive HIV test prior or on the same date as first negative test during the study period
-- -- FOR NOW LEAVE THIS OFF: Exclude Patients with HIV on the problem list prior to or on the same date as first negative test 
-- CREATE TABLE hiv_rpt.r21_potential_pats_details AS
-- SELECT master_patient_id,
-- -- first day of month representing month 0
-- (date_trunc('month', abs_index_date))::date month_0,
-- -- age on the last day of month 0
-- date_part('year', age((date_trunc('month', abs_index_date) + interval '1 month' - interval '1 day')::date, T2.date_of_birth)) age_month_0,
-- gender as sex,
-- race,
-- ethnicity,
-- home_language,
-- T3.female_partner_yn,
-- T3.male_partner_yn,
-- date_of_death
-- FROM hiv_rpt.r21_potential_pats T1
-- INNER JOIN emr_patient T2 on (T1.master_patient_id = T2.id)
-- --FOR ATRIUS ONLY
-- --LEFT JOIN kre_report.atrius_soc_history T3 ON (T3.pat_id = T2.natural_key)
-- LEFT JOIN hiv_rpt.static_soc_history T3 ON (T3.pat_id = T2.natural_key)
-- LEFT JOIN (SELECT patient_id, min(date) pos_test_date from hef_event where name ilike 'lx:hiv%positive' group by patient_id) T4 on (T4.patient_id = T1.master_patient_id) 
-- --LEFT JOIN (SELECT patient_id, min(date) hiv_prob_date from emr_problem where date <= '01-01-2012' and dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B97.35|icd10:Z21)' and status not in ('Deleted') group by patient_id) T5 on (T5.patient_id = T1.master_patient_id)
-- WHERE pos_test_date is null or abs_index_date < pos_test_date;
-- --AND hiv_prob_date is null or abs_index_date < hiv_prob_date;

-- TEMP WORKAROUND FOR TESTING - REPLACES TABLE DEF FROM ABOVE
-- Index Month, Index Patients & Demographics
-- Exclude Patients with a Positive HIV test prior or on the same date as first negative test during the study period
-- FOR NOW LEAVE THIS OFF: Exclude Patients with HIV on the problem list prior to or on the same date as first negative test 
CREATE TABLE hiv_rpt.r21_potential_pats_details AS
SELECT master_patient_id,
-- first day of month representing month 0
(date_trunc('month', abs_index_date))::date month_0,
-- age on the last day of month 0
date_part('year', age((date_trunc('month', abs_index_date) + interval '1 month' - interval '1 day')::date, T2.date_of_birth)) age_month_0,
gender as sex,
race,
ethnicity,
home_language,
null::text as female_partner_yn,
null::text as male_partner_yn,
date_of_death
FROM hiv_rpt.r21_potential_pats T1
INNER JOIN emr_patient T2 on (T1.master_patient_id = T2.id)
--FOR ATRIUS ONLY
--LEFT JOIN kre_report.atrius_soc_history T3 ON (T3.pat_id = T2.natural_key)
--LEFT JOIN hiv_rpt.static_soc_history T3 ON (T3.pat_id = T2.natural_key)
LEFT JOIN (SELECT patient_id, min(date) pos_test_date from hef_event where name ilike 'lx:hiv%positive' group by patient_id) T4 on (T4.patient_id = T1.master_patient_id) 
--LEFT JOIN (SELECT patient_id, min(date) hiv_prob_date from emr_problem where date <= '01-01-2012' and dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B97.35|icd10:Z21)' and status not in ('Deleted') group by patient_id) T5 on (T5.patient_id = T1.master_patient_id)
WHERE pos_test_date is null or abs_index_date < pos_test_date;
--AND hiv_prob_date is null or abs_index_date < hiv_prob_date;




-- Index Patient List with Details
-- Patient Death Ends Follow-Up
-- The month of first HIV case ends the follow_up
-- Need to eliminate patients that had a HIV case before month_0 (but after study period start)
-- Need to get end_of_followup_month for all other index patients
-- FOR NOW LEAVE OFF HIV CASE EXCLUSION -- MAINTAING AS A PLACEHOLDER FOR FOLLOW-UP END DATA IF WE HAVE IT
CREATE TABLE hiv_rpt.r21_index_pats AS
SELECT T1.*,
-- first day of month representing end of follow_up
case when date_of_death is not null then (date_trunc('month', date_of_death))::date else null END as end_of_followup_month
--null::date as end_of_followup_month
FROM hiv_rpt.r21_potential_pats_details T1
;
--LEFT JOIN (SELECT date, patient_id from nodis_case where condition = 'hiv') T2 on T1.master_patient_id = T2.patient_id
--WHERE date is null or (date_trunc('month', date))::date > T1.month_0;

-------------------
------------------- CREATE FOLLOW-UP TIME INTERVALS
-------------------
CREATE TABLE hiv_rpt.r21_month_intervals AS
SELECT T2 month_id, T1.master_patient_id,month_0, end_of_followup_month,
case when T2 = 0 then month_0::date
when T2 = 1 and ((month_0 + interval '1 month') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '1 month')::date
when T2 = 2 and ((month_0 + interval '2 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '2 months'):: date
when T2 = 3 and ((month_0 + interval '3 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '3 months'):: date
when T2 = 4 and ((month_0 + interval '4 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '4 months'):: date
when T2 = 5 and ((month_0 + interval '5 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '5 months'):: date
when T2 = 6 and ((month_0 + interval '6 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '6 months'):: date
when T2 = 7 and ((month_0 + interval '7 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '7 months'):: date
when T2 = 8 and ((month_0 + interval '8 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '8 months'):: date
when T2 = 9 and ((month_0 + interval '9 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '9 months'):: date
when T2 = 10 and ((month_0 + interval '10 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '10 months'):: date
when T2 = 11 and ((month_0 + interval '11 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '11 months'):: date
when T2 = 12 and ((month_0 + interval '12 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '12 months'):: date
when T2 = 13 and ((month_0 + interval '13 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '13 months'):: date
when T2 = 14 and ((month_0 + interval '14 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '14 months'):: date
when T2 = 15 and ((month_0 + interval '15 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '15 months'):: date
when T2 = 16 and ((month_0 + interval '16 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '16 months'):: date
when T2 = 17 and ((month_0 + interval '17 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '17 months'):: date
when T2 = 18 and ((month_0 + interval '18 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '18 months'):: date
when T2 = 19 and ((month_0 + interval '19 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '19 months'):: date
when T2 = 20 and ((month_0 + interval '20 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '20 months'):: date
when T2 = 21 and ((month_0 + interval '21 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '21 months'):: date
when T2 = 22 and ((month_0 + interval '22 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '22 months'):: date
when T2 = 23 and ((month_0 + interval '23 months') <= end_of_followup_month or end_of_followup_month is null) then (month_0 + interval '23 months'):: date
else null end month_start,
case when T2 = 0 then (month_0 + interval '1 month')::date
when T2 = 1 then (month_0 + interval '2 month')::date
when T2 = 2 then (month_0 + interval '3 months'):: date
when T2 = 3 then (month_0 + interval '4 months'):: date
when T2 = 4 then (month_0 + interval '5 months'):: date
when T2 = 5 then (month_0 + interval '6 months'):: date
when T2 = 6 then (month_0 + interval '7 months'):: date
when T2 = 7 then (month_0 + interval '8 months'):: date
when T2 = 8 then (month_0 + interval '9 months'):: date
when T2 = 9 then (month_0 + interval '10 months'):: date
when T2 = 10 then (month_0 + interval '11 months'):: date
when T2 = 11 then (month_0 + interval '12 months'):: date
when T2 = 12 then (month_0 + interval '13 months'):: date
when T2 = 13 then (month_0 + interval '14 months'):: date
when T2 = 14 then (month_0 + interval '15 months'):: date
when T2 = 15 then (month_0 + interval '16 months'):: date
when T2 = 16 then (month_0 + interval '17 months'):: date
when T2 = 17 then (month_0 + interval '18 months'):: date
when T2 = 18 then (month_0 + interval '19 months'):: date
when T2 = 19 then (month_0 + interval '20 months'):: date
when T2 = 20 then (month_0 + interval '21 months'):: date
when T2 = 21 then (month_0 + interval '22 months'):: date
when T2 = 22 then (month_0 + interval '23 months'):: date
when T2 = 23 then (month_0 + interval '24 months'):: date
else null end month_end
FROM generate_series(0,23) T2,
hiv_rpt.r21_index_pats T1;

-- IF FOLLOW-UP LESS THAN ONE MONTH THEN REMOVE ROWS
DELETE FROM hiv_rpt.r21_month_intervals where month_start is null;





-------------------
-------------------  LAB & HEF DATA
-------------------


-- All lab hef events for index patients
-- Start is 2006 to capture all hef events [needed for 1 demog variable for chlam/gon testing]
CREATE TABLE hiv_rpt.r21_hef_w_lab_details AS 
SELECT T1.id,T2.name,T2.patient_id,T2.date,T1.specimen_source,T2.object_id,T1.native_code, T1.native_name, T1.procedure_name
FROM emr_labresult T1,
hef_event T2, 
hiv_rpt.r21_index_pats T3
WHERE T1.id = T2.object_id
AND T1.patient_id = T2.patient_id
AND T1.patient_id = T3.master_patient_id
AND T3.master_patient_id = T2.patient_id
AND T2.date >= '01-01-2006'
AND T2.date < '01-01-2018';


-------------------
------------------- GONORRHEA & CHLAMYDIA
-------------------

-- GONORRHEA - Gather up gonorrhea events and normalize specimen sources
-- CHLAMYDIA - Gather up chlamydia events and normalize specimen sources
CREATE TABLE hiv_rpt.r21_gon_chlam_events AS
SELECT name, date, patient_id, object_id,
CASE WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(PHARYNGEAL|THROAT)' or native_code ~* '(PHARYNGEAL|THROAT)' or procedure_name ~* '(PHARYNGEAL|THROAT)')then 'THROAT'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(RECTAL|ANAL)' OR native_code ~* '(RECTAL|ANAL)' OR procedure_name ~* '(RECTAL|ANAL)') THEN 'RECTAL'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(GENITAL|URINE|URN|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)' OR native_code  ~* '(GENITAL|URINE|URN|URI|UR|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)' OR procedure_name  ~* '(GENITAL|URINE|URN|URI|UR|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)') THEN 'UROG'
WHEN specimen_source ~* '^(RECTUM|RECT|ANAL)' then 'RECTAL'
WHEN specimen_source ~* '^(THROAT)' then 'THROAT'
WHEN specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG|UREATHRA)' then 'UROG'
ELSE specimen_source END specimen_source,
specimen_source specimen_source_orig,
native_code
FROM hiv_rpt.r21_hef_w_lab_details T1
WHERE (name like 'lx:gonorrhea%' OR name like 'lx:chlamydia%');


CREATE TABLE hiv_rpt.r21_gon_chlam_counts as
select T1.patient_id, month_id, month_start,
-- find any chlam/gon test for rectal or throat ever before or during month 0
case when month_id = 0 then max(case when (name ilike 'lx:gonorrhea:%' or name ilike 'lx:chlamydia') and specimen_source in ('RECTAL', 'THROAT') and date <= month_end then 1 else 0 end) else null end prior_gon_chlam_rectal_throat_ever,
case when month_id = 0 then count(case when name = 'lx:gonorrhea:positive' and specimen_source = 'RECTAL' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_pos_gon_rectal,
case when month_id = 0 then count(case when name = 'lx:gonorrhea:positive' and specimen_source = 'THROAT' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_pos_gon_oropharynx,
case when month_id = 0 then count(case when name = 'lx:gonorrhea:positive' and specimen_source = 'UROG' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_pos_gon_urog,
max(case when name ilike 'lx:gonorrhea:%' and specimen_source = 'RECTAL' and date >= month_start and date < month_end then 1 end) as any_gon_rectal,
max(case when name = 'lx:gonorrhea:positive' and specimen_source = 'RECTAL' and date >= month_start and date < month_end then 1 end) as pos_gon_rectal,
max(case when name ilike 'lx:gonorrhea:%' and specimen_source = 'THROAT' and date >= month_start and date < month_end then 1 end) as any_gon_throat,
max(case when name = 'lx:gonorrhea:positive' and specimen_source = 'THROAT'  and date >= month_start and date < month_end then 1 end) as pos_gon_throat,
max(case when name ilike 'lx:gonorrhea:%' and specimen_source = 'UROG' and date >= month_start and date >= month_start and date < month_end then 1 end) as any_gon_urog,
max(case when name = 'lx:gonorrhea:positive' and specimen_source = 'UROG' and date >= month_start and date < month_end then 1 end) as pos_gon_urog,
case when month_id = 0 then count(case when name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_pos_chlam_rectal,
case when month_id = 0 then count(case when name = 'lx:chlamydia:positive' and specimen_source = 'THROAT' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_pos_chlam_oropharynx,
case when month_id = 0 then count(case when name = 'lx:chlamydia:positive' and specimen_source = 'UROG' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_pos_chlam_urog,
max(case when name ilike 'lx:chlamydia:%' and specimen_source = 'RECTAL' and date >= month_start and date < month_end then 1 end) as any_chlam_rectal,
max(case when name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' and date >= month_start and date < month_end then 1 end) as pos_chlam_rectal,
max(case when name ilike 'lx:chlamydia:%' and specimen_source = 'THROAT' and date >= month_start and date < month_end then 1 end) as any_chlam_throat,
max(case when name = 'lx:chlamydia:positive' and specimen_source = 'THROAT'  and date >= month_start and date < month_end then 1 end) as pos_chlam_throat,
max(case when name ilike 'lx:chlamydia:%' and specimen_source = 'UROG' and date >= month_start and date >= month_start and date < month_end then 1 end) as any_chlam_urog,
max(case when name = 'lx:chlamydia:positive' and specimen_source = 'UROG' and date >= month_start and date < month_end then 1 end) as pos_chlam_urog
FROM hiv_rpt.r21_gon_chlam_events T1,
hiv_rpt.r21_month_intervals T2
WHERE T1.patient_id = T2.master_patient_id
--AND T1.patient_id in (1688763, 38662886)
GROUP BY T1.patient_id, month_id, month_start
ORDER BY patient_id,month_id;


-------------------
------------------- SYPHILIS
-------------------

-- Gather up all syphilis related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
CREATE TABLE hiv_rpt.r21_syph_events AS 
SELECT T2.patient_id, T2.id, T3.test_name, T4.name as hef_name, T2.date, result_string 
FROM hiv_rpt.r21_index_pats T1
INNER JOIN emr_labresult T2 ON (T1.master_patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.master_patient_id = T4.patient_id))
WHERE T2.date >= '12-01-2010'
AND T2.date < '01-01-2018'
AND T3.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm', 'tp-cia')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined', 'Not Done', 'Not performed',  'Not tested', 'Not Tested', 'test', 'CANCELLED. DUPLICATE REQUEST.')
AND result_string !~ ('CANCELLED')
and result_string is not null;

-- Any test for syphilis (per the ESP syphilis spec, syphilis tests include RPR, VDRL, TPPA, FTA-ABS, TP-IGM, TP-CIA and TP-IGG)
CREATE TABLE hiv_rpt.r21_syph_counts as
SELECT T1.patient_id, month_id, month_start,
max(case when date >= month_start and date < month_end then 1 end) as any_syph
FROM hiv_rpt.r21_syph_events T1,
hiv_rpt.r21_month_intervals T2
WHERE T1.patient_id = T2.master_patient_id
--AND T1.patient_id in (1688763, 38662886)
GROUP BY T1.patient_id, month_id, month_start
ORDER BY patient_id,month_id;



-------------------
------------------- STI TESTING EPISODES
-------------------

-- Number of gonorrhea, chlamydia, or syphilis testing episodes (any anatomic site; multiple tests on same date count as 1 testing episode)
CREATE TABLE hiv_rpt.r21_sti_tst_episodes AS 
SELECT patient_id, count(*) gon_chlam_syph_tst_episodes 
FROM (
	SELECT T2.master_patient_id as patient_id, date as testing_date
	FROM hiv_rpt.r21_syph_events T1,
	hiv_rpt.r21_month_intervals T2
	WHERE T1.patient_id = T2.master_patient_id
	AND month_id = 0
	AND date < month_start and date >= month_start - interval '12 months'
	--AND T1.master_patient_id in (1688763, 38662886, 9614)
	GROUP BY T2.master_patient_id, date
	UNION
	SELECT patient_id, date as testing_date
	FROM hiv_rpt.r21_gon_chlam_events T1,
	hiv_rpt.r21_month_intervals T2
	WHERE T1.patient_id = T2.master_patient_id
	AND month_id = 0
	AND date < month_start and date >= month_start - interval '12 months'
	--AND patient_id in (1688763, 38662886, 9614)
	GROUP BY patient_id, date
	ORDER BY patient_id, testing_date) T1
GROUP BY patient_id;

CREATE TABLE hiv_rpt.r21_gon_chlam_rectal_tst_episodes AS 
SELECT patient_id, count(*) gon_chlam_rectal_tst_episodes 
FROM (
SELECT T1.patient_id as patient_id, date as testing_date
	FROM hiv_rpt.r21_gon_chlam_events T1,
	hiv_rpt.r21_month_intervals T2
	WHERE T1.patient_id = T2.master_patient_id
	AND month_id = 0
	AND date < month_start and date >= month_start - interval '12 months'
	AND specimen_source = 'RECTAL'
	--AND T1.patient_id in (1688763, 38662886, 9614)
	GROUP BY T1.patient_id, date) T1
GROUP BY patient_id;

CREATE TABLE hiv_rpt.r21_gon_chlam_throat_tst_episodes AS 
SELECT patient_id, count(*) gon_chlam_orpharynx_tst_episodes 
FROM (
SELECT T1.patient_id as patient_id, date as testing_date
	FROM hiv_rpt.r21_gon_chlam_events T1,
	hiv_rpt.r21_month_intervals T2
	WHERE T1.patient_id = T2.master_patient_id
	AND month_id = 0
	AND date < month_start and date >= month_start - interval '12 months'
	AND specimen_source = 'THROAT'
	--AND T1.patient_id in (1688763, 38662886, 9614)
	GROUP BY T1.patient_id, date) T1
GROUP BY patient_id;



-------------------
------------------- ESP CASES
-------------------

-- All gon, chlam, syph, and hep_b_acute cases for index patients
-- Add in HepC Acute Cases
CREATE TABLE hiv_rpt.r21_esp_cases AS 
SELECT T1.patient_id, T1.condition, T1.date
FROM nodis_case T1,
hiv_rpt.r21_index_pats T2
WHERE T1.patient_id = T2.master_patient_id
AND condition in ('gonorrhea', 'chlamydia', 'syphilis', 'hepatitis_b:acute')
AND T1.date >= '01-01-2010'
AND T1.date < '01-01-2018'

UNION

-- CAN ONLY GO FOR Undetermined to Acute (or Chronic) so we just need all Hep C Acute Cases
SELECT T1.patient_id, 'hepatitis_c:acute' as condition, T1.date
FROM nodis_case T1, 
nodis_caseactivehistory T2,
hiv_rpt.r21_index_pats T3
WHERE T1.condition = 'hepatitis_c'
AND T1.date >= '01-01-2010'
AND T1.date < '01-01-2018' 
and T2.status = 'HEP_C-A'
AND T1.id = T2.case_id 
AND T1. patient_id = T3.master_patient_id;


CREATE TABLE hiv_rpt.r21_case_counts as
SELECT T1.patient_id, month_id, month_start,
case when month_id = 0 then count(case when condition = 'gonorrhea' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_gon_cases,
case when month_id = 0 then count(case when condition = 'chlamydia' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_chlam_cases,
case when month_id = 0 then count(case when condition = 'syphilis' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_syph_cases,
case when month_id = 0 then count(case when condition = 'hepatitis_b:acute' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hep_b_acute_case,
case when month_id = 0 then count(case when condition = 'hepatitis_c:acute' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hep_c_acute_case,
max(case when condition = 'syphilis' and date >= month_start and date < month_end then 1 end) as esp_syph_case,
max(case when condition = 'hepatitis_b:acute' and date >= month_start and date < month_end then 1 end) as esp_hep_b_acute_case,
max(case when condition = 'hepatitis_c:acute' and date >= month_start and date < month_end then 1 end) as esp_hep_c_acute_case
FROM hiv_rpt.r21_esp_cases T1,
hiv_rpt.r21_month_intervals T2
WHERE T1.patient_id = T2.master_patient_id
--AND T1.patient_id in (1688763, 38662886)
GROUP BY T1.patient_id, month_id, month_start
ORDER BY patient_id,month_id;

-- Number of prior encounters that met ESP criteria for chlamydia, gonorrhea, syphilis (i.e., number of unique case dates for any of these 3 conditions; if patient met ESP criteria for multiple STIs on the same day then count as 1 STI encounter)
-- Restrict to specific conditions
CREATE TABLE hiv_rpt.sti_case_encs AS 
SELECT patient_id, count(*) sti_esp_case_encounters 
FROM (
SELECT T1.patient_id as patient_id, date as testing_date
	FROM hiv_rpt.r21_esp_cases T1,
	hiv_rpt.r21_month_intervals T2
	WHERE T1.patient_id = T2.master_patient_id
	AND month_id = 0
	AND date < month_start and date >= month_start - interval '12 months'
	AND condition in ('chlamydia', 'gonorrhea', 'syphilis')
	GROUP BY T1.patient_id, date) T1
GROUP BY patient_id;


-------------------
------------------- ENCOUNTERS
-------------------

CREATE TABLE hiv_rpt.r21_encounters as
SELECT T1.patient_id, month_id, month_start,
case when month_id = 0 then count(case when date < month_start and date >= month_start - interval '12 months' then 1 end) else null end number_of_encs,
max(case when date >= month_start and date < month_end then 1 end) as any_enc
FROM public.emr_encounter T1
JOIN hiv_rpt.r21_month_intervals T2 ON (T1.patient_id = T2.master_patient_id)
LEFT JOIN static_enc_type_lookup T3 on (T1.raw_encounter_type = T3.raw_encounter_type)
WHERE T3.ambulatory = 1
AND T1.date < '01-01-2018'
GROUP BY T1.patient_id, month_id, month_start
ORDER BY patient_id,month_id;

-------------------
------------------- HIV LABS
-------------------


-- Gather up all hiv related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS
-- ONLY PRINT RESULT STRING FOR TESTING. GROUP SO LOG RESULTS DON'T GET COUNTED TWICE
CREATE TABLE hiv_rpt.r21_hiv_events AS 
SELECT T2.patient_id, T3.test_name, T4.name as hef_name, 
--result_string,
T2.date
FROM hiv_rpt.r21_index_pats T1
INNER JOIN emr_labresult T2 ON (T1.master_patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.master_patient_id = T4.patient_id))
WHERE T2.date >= '12-01-2010'
AND T2.date < '01-01-2018'
AND T3.test_name ilike '%hiv%'
AND T3.test_name not in ('hiv not a test')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined', 'Not Done', 'Not performed',  'Not tested', 'Not Tested', 'test')
AND result_string not ilike '%cancelled%'
AND result_string is not null
GROUP BY T2.patient_id, T3.test_name, T4.name, T2.date;


-- Remove hef name so tests that don't have pos/neg don't get counted twice
CREATE TABLE hiv_rpt.r21_hiv_events_dist_by_date AS 
SELECT patient_id, test_name, date
FROM hiv_rpt.r21_hiv_events
GROUP BY patient_id, test_name, date;


CREATE TABLE hiv_rpt.r21_hiv_counts_nohef as
SELECT T1.patient_id, month_id, month_start,
case when month_id = 0 then count(case when test_name = 'hiv_elisa' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hiv_elisa,
case when month_id = 0 then count(case when test_name = 'hiv_wb' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hiv_wb,
case when month_id = 0 then count(case when test_name = 'hiv_rna_viral' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hiv_rna_viral,
case when month_id = 0 then count(case when test_name = 'hiv_pcr' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hiv_pcr,
case when month_id = 0 then count(case when test_name = 'hiv_ag_ab' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hiv_ag_ab,
max(case when test_name = 'hiv_elisa' and date >= month_start and date < month_end then 1 end) as any_hiv_elisa,
max(case when test_name = 'hiv_wb' and date >= month_start and date < month_end then 1 end) as any_hiv_wb,
max(case when test_name = 'hiv_rna_viral' and date >= month_start and date < month_end then 1 end) as any_hiv_rna_viral,
max(case when test_name = 'hiv_ag_ab' and date >= month_start and date < month_end then 1 end) as any_hiv_ag_ab,
max(case when test_name = 'hiv_pcr' and date >= month_start and date < month_end then 1 end) as any_hiv_pcr
from 
hiv_rpt.r21_hiv_events_dist_by_date T1,
hiv_rpt.r21_month_intervals T2
WHERE T1.patient_id = T2.master_patient_id
GROUP BY patient_id, month_id, month_start
ORDER BY patient_id,month_id;


CREATE TABLE hiv_rpt.r21_hiv_counts_hef as
SELECT T1.patient_id, month_id, month_start,
max(case when test_name = 'hiv_elisa' and hef_name ilike '%pos%' and date >= month_start and date < month_end then 1 end) as pos_hiv_elisa,
max(case when test_name = 'hiv_wb' and hef_name ilike '%pos%' and date >= month_start and date < month_end then 1 end) as pos_hiv_wb,
max(case when test_name = 'hiv_rna_viral' and (hef_name ='lx:hiv_rna_viral:threshold:gt:200' or hef_name = 'lx:hiv_rna_viral:positive')  and date >= month_start and date < month_end then 1 end) as pos_hiv_rna_viral,
max(case when test_name = 'hiv_ag_ab' and hef_name ilike '%pos%' and date >= month_start and date < month_end then 1 end) as pos_hiv_ag_ab,
max(case when test_name = 'hiv_pcr' and hef_name ilike '%pos%' and date >= month_start and date < month_end then 1 end) as pos_hiv_pcr
FROM hiv_rpt.r21_hiv_events T1,
hiv_rpt.r21_month_intervals T2
WHERE T1.patient_id = T2.master_patient_id
GROUP BY T1.patient_id, month_id, month_start
ORDER BY patient_id,month_id;

CREATE TABLE hiv_rpt.r21_hiv_counts as
SELECT T1.patient_id, T1.month_id, T1.month_start,
prior_hiv_elisa,
prior_hiv_wb,
prior_hiv_rna_viral,
prior_hiv_pcr,
prior_hiv_ag_ab,
any_hiv_elisa,
pos_hiv_elisa,
any_hiv_wb,
pos_hiv_wb,
any_hiv_rna_viral,
pos_hiv_rna_viral,
any_hiv_ag_ab,
pos_hiv_ag_ab,
any_hiv_pcr,
pos_hiv_pcr
FROM 
hiv_rpt.r21_hiv_counts_nohef T1,
hiv_rpt.r21_hiv_counts_hef T2
WHERE T1.patient_id = T2.patient_id
AND T1.month_id = T2.month_id
AND T1.month_start = T2.month_start;



-------------------
------------------- HEP LABS
-------------------


-- Gather up all hep related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS
-- ONLY PRINT RESULT STRING FOR TESTING. GROUP SO LOG RESULTS DON'T GET COUNTED TWICE
CREATE TABLE hiv_rpt.r21_hep_events AS 
SELECT T2.patient_id, T3.test_name, T4.name as hef_name, --result_string,
T2.date
FROM hiv_rpt.r21_index_pats T1
INNER JOIN emr_labresult T2 ON (T1.master_patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.master_patient_id = T4.patient_id))
WHERE T2.date >= '12-01-2010'
AND T2.date < '01-01-2018'
AND T3.test_name in ('hepatitis_c_elisa', 'hepatitis_c_rna', 'hepatitis_b_surface_antigen', 'hepatitis_b_viral_dna', 'hepatitis_b_core_antigen_igm_antibody')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined', 'Not Done', 'Not performed',  'Not tested', 'Not Tested', 'test')
AND result_string not ilike '%cancelled%'
AND result_string is not null
GROUP BY T2.patient_id, T3.test_name, T4.name, T2.date;


-- Remove hef name so tests that don't have pos/neg don't get counted twice
CREATE TABLE hiv_rpt.r21_hep_events_dist_by_date AS 
SELECT patient_id, test_name, date
FROM hiv_rpt.r21_hep_events
GROUP BY patient_id, test_name, date;


CREATE TABLE hiv_rpt.r21_hep_counts_nohef as
SELECT T1.patient_id, month_id, month_start,
case when month_id = 0 then count(case when test_name = 'hepatitis_c_elisa' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hepc_elisa,
case when month_id = 0 then count(case when test_name = 'hepatitis_c_rna' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hepc_rna,
case when month_id = 0 then count(case when test_name = 'hepatitis_b_surface_antigen' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hepb_surf_ant,
case when month_id = 0 then count(case when test_name = 'hepatitis_b_viral_dna' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hepb_viral_dna,
case when month_id = 0 then count(case when test_name = 'hepatitis_b_core_antigen_igm_antibody' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_hepb_core_ag_igm_ab,
max(case when test_name = 'hepatitis_c_elisa' and date >= month_start and date < month_end then 1 end) as any_hepc_elisa,
max(case when test_name = 'hepatitis_c_rna' and date >= month_start and date < month_end then 1 end) as any_hepc_rna,
max(case when test_name = 'hepatitis_b_surface_antigen' and date >= month_start and date < month_end then 1 end) as any_hepb_surf_ant,
max(case when test_name = 'hepatitis_b_viral_dna' and date >= month_start and date < month_end then 1 end) as any_hepb_viral_dna,
max(case when test_name = 'hepatitis_b_core_antigen_igm_antibody' and date >= month_start and date < month_end then 1 end) as any_hepb_core_ag_igm_ab
from 
hiv_rpt.r21_hep_events_dist_by_date T1,
hiv_rpt.r21_month_intervals T2
WHERE T1.patient_id = T2.master_patient_id
GROUP BY patient_id, month_id, month_start
ORDER BY patient_id,month_id;


CREATE TABLE hiv_rpt.r21_hep_counts_hef as
SELECT T1.patient_id, month_id, month_start,
case when month_id = 0 then count(case when test_name = 'hepatitis_c_elisa' and hef_name ilike '%pos%' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_pos_hepc_elisa,
case when month_id = 0 then count(case when test_name = 'hepatitis_c_rna' and hef_name ilike '%pos%' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_pos_hepc_rna,
case when month_id = 0 then count(case when test_name = 'hepatitis_b_core_antigen_igm_antibody' and hef_name ilike '%pos%' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_pos_hepb_core_ag_igm_ab,
case when month_id = 0 then max(case when (test_name = 'hepatitis_b_surface_antigen' or test_name = 'hepatitis_b_viral_dna') and hef_name ilike '%pos%' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_pos_hepb,
max(case when test_name = 'hepatitis_c_elisa' and hef_name ilike '%pos%' and date >= month_start and date < month_end then 1 end) as pos_hepc_elisa,
max(case when test_name = 'hepatitis_c_rna' and hef_name ilike '%pos%' and date >= month_start and date < month_end then 1 end) as pos_hepc_rna,
max(case when test_name = 'hepatitis_b_core_antigen_igm_antibody' and hef_name ilike '%pos%' and date >= month_start and date < month_end then 1 end) as pos_hepb_core_ag_igm_ab,
max(case when (test_name = 'hepatitis_b_surface_antigen' or test_name = 'hepatitis_b_viral_dna') and hef_name ilike '%pos%' and date >= month_start and date < month_end then 1 end) as pos_hepb
FROM hiv_rpt.r21_hep_events T1,
hiv_rpt.r21_month_intervals T2
WHERE T1.patient_id = T2.master_patient_id
--AND T1.patient_id in (1688763, 38662886)
GROUP BY T1.patient_id, month_id, month_start
ORDER BY patient_id,month_id;

CREATE TABLE hiv_rpt.r21_hep_counts as
SELECT T1.patient_id, T1.month_id, T1.month_start,
prior_hepc_elisa,
prior_hepc_rna,
prior_pos_hepc_elisa,
prior_pos_hepc_rna,
prior_hepb_surf_ant,
prior_hepb_viral_dna,
prior_hepb_core_ag_igm_ab,
prior_pos_hepb_core_ag_igm_ab,
prior_pos_hepb,
any_hepc_elisa,
any_hepc_rna,
pos_hepc_elisa,
pos_hepc_rna,
any_hepb_surf_ant,
any_hepb_viral_dna,
any_hepb_core_ag_igm_ab,
pos_hepb_core_ag_igm_ab,
pos_hepb
FROM 
hiv_rpt.r21_hep_counts_nohef T1,
hiv_rpt.r21_hep_counts_hef T2
WHERE T1.patient_id = T2.patient_id
AND T1.month_id = T2.month_id
AND T1.month_start = T2.month_start;


-------------------
------------------- PRESCRIPTIONS
-------------------

-- HIV MEDS

CREATE TABLE hiv_rpt.r21_hiv_meds as
SELECT T1.patient_id, T1.name, T3.name as hef_name, T1.date, T1.quantity, T1.quantity_float, T1.refills, T1.start_date, T1.end_date, 
CASE WHEN refills~E'^\\d+$' THEN ((refills::real +1) * quantity_float) 
     WHEN refills is null then quantity_float end as total_quantity_per_rx,
case when T3.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end rx_truvada,
case when T3.name not in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end other_hiv_rx_non_truvada
FROM emr_prescription T1
JOIN hiv_rpt.r21_index_pats T2 on (T1.patient_id = T2.master_patient_id)
JOIN hef_event T3 on (T1.patient_id = T3.patient_id and T1.id = T3.object_id)
WHERE T3.name ilike 'rx:hiv%'
AND T1.date >= '01-01-2010'
AND T1.date < '01-01-2018';



CREATE TABLE hiv_rpt.r21_non_hiv_rx_of_interest AS 
SELECT T1.patient_id,T1.name, T1.date, T1.quantity, T1.quantity_float, T1.refills, T1.start_date, T1.end_date, T1.dose,
CASE WHEN refills~E'^\\d+$' THEN ((refills::real +1) * quantity_float) 
     WHEN refills is null then quantity_float end as total_quantity_per_rx,
CASE WHEN T1.name in (
-- ATRIUS
'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)',
'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE',
'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
--CHA 
'BICILLIN L-A 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE & PROC 1200000 UNIT/2ML IM SUSP',
'PENICILLIN G IVPB IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G IVPB (MINIBAG-PLUS) 5 MILLION UNITS',
'PENICILLIN G IVPB MINIBAG PLUS 5 MILLION UNITS',
'PENICILLIN G POTASSIUM 20000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM 5000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM IN D5W 40000 UNIT/ML IV SOLN',
'PENICILLIN G POTASSIUM IN D5W 60000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 20000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 40000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 60000 UNIT/ML IV SOLN',
'PENICILLIN G PROCAINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 300000-900000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G SODIUM 5000000 IU IJ SOLR',
--BMC
'BICILLIN L-A 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
'PENICILLIN G IVPB  IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G POTASSIUM 20 MILLION UNIT SOLUTION FOR INJECTION',
'PENICILLIN G POTASSIUM 5 MILLION UNIT SOLUTION FOR INJECTION',
'PENICILLIN G POTASSIUM IV 5 MILLION UNITS MBP',
'PENICILLIN G SODIUM 5 MILLION UNIT SOLUTION FOR INJECTION')
-- ATRIUS 
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE', 'BICILLIN L-A DISP SYRIN 600000 U IM (PENICILLIN G BENZATHINE)', 'BICILLIN L-A DISP SYRIN 600MU/1ML IM (PENICILLIN G BENZATHINE)') and quantity_float >= 4)
-- CHA
OR (name in ('BICILLIN L-A 1200000 UNIT/2ML IM SUSP', 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600000 UNIT/ML IM SUSP', 'PENICILLIN G BENZATHINE 600000 UNIT/ML IM SUSP') and quantity_float >= 4)
-- BMC
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML INTRAMUSCULAR SYRINGE') and (dose = '2.4 Million Units' or dose = '2400000 Units'))
then 1 end rx_bicillin,	 
CASE WHEN T1.name in (
    -- ATRIUS
	'AZITHROMYCIN 1 GRAM ORAL PACKET', 
	'AZITHROMYCIN CHLAMYDIA RX (1 G POWDER PACKET)', 
	'ZITHROMAX 1 GRAM ORAL PACKET (AZITHROMYCIN)', 
	-- CHA
	'AZITHROMYCIN 1 G PO PACK',
	'ZITHROMAX 1 G PO PACK',
	'AZITHROMYCIN 2 G PO SUSR',
	-- BMC
	'AZITHROMYCIN 1 GRAM ORAL PACKET') then 1 end rx_azithromycin,
CASE WHEN T1.name in ( 
     -- ATRIUS
	'CEFTRIAXONE IM INJECTION <=250 MG', 
	'CEFTRIAXONE-LIDOCAINE IM INJECTION <=250 MG',
	'ROCEPHIN 250 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)', 
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION', 
	-- CHA
	'CEFTRIAXONE SODIUM 250 MG IJ SOLR',
	'ROCEPHIN 250 MG IJ SOLR',
	-- BMC
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION',
	'CEFTRIAXONE IM INJECTION <=250 MG') then 1 end rx_ceftriaxone,
CASE WHEN 
	T1.name ilike '%methadone%' 
	or T1.name ilike '%dolophine%' 
	or T1.name ilike '%methadose%' then 1 end rx_methadone,
CASE WHEN 
	T1.name ilike '%suboxone%' 
	or T1.name ilike 'buprenorphine%naloxone%' 
	or T1.name ilike '%bunavail%'
	or T1.name ilike '%cassipa%'
	or T1.name ilike '%zubsolv%' then 1 end rx_suboxone,
CASE WHEN 
	T1.name ilike '%tadalafil%' 
	or T1.name ilike '%cilais%' 
	or T1.name ilike '%vardenafil%' 
	or T1.name ilike '%staxyn%'
	or T1.name ilike '%levitra%' 
	or T1.name ilike '%sildenafil%' 
	or T1.name ilike '%viagra%' then 1 end rx_viagara_cilais_or_levitra 
FROM emr_prescription T1
JOIN hiv_rpt.r21_index_pats T2 on (T1.patient_id = T2.master_patient_id)
WHERE T1.date >= '01-01-2010'
AND T1.date < '01-01-2018'
AND ( name in (
     --ATRIUS
	'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)',
	'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)',
	'BICILLIN L-A DISP SYRIN 600000 U IM (PENICILLIN G BENZATHINE)',
	'BICILLIN L-A DISP SYRIN 600MU/1ML IM (PENICILLIN G BENZATHINE)',
	'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE',
	'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
	'BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 
	'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE'
	'BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 
	'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE'
	'AZITHROMYCIN 1 GRAM ORAL PACKET', 
	'AZITHROMYCIN CHLAMYDIA RX (1 G POWDER PACKET)', 
	'ZITHROMAX 1 GRAM ORAL PACKET (AZITHROMYCIN)', 
	'CEFTRIAXONE IM INJECTION <=250 MG', 
	'CEFTRIAXONE-LIDOCAINE IM INJECTION <=250 MG',
	'ROCEPHIN 250 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)', 
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION', 
	--CHA
	'BICILLIN L-A 1200000 UNIT/2ML IM SUSP',
	'BICILLIN L-A 2400000 UNIT/4ML IM SUSP',
	'BICILLIN L-A 600000 UNIT/ML IM SUSP',
	'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP',
	'PENICILLIN G BENZATHINE 2400000 UNIT/4ML IM SUSP',
	'PENICILLIN G BENZATHINE 600000 UNIT/ML IM SUSP',
	'PENICILLIN G BENZATHINE & PROC 1200000 UNIT/2ML IM SUSP',
	'PENICILLIN G IVPB IN 100 ML',
	'PENICILLIN G IVPB IN 50 ML',
	'PENICILLIN G IVPB (MINIBAG-PLUS) 5 MILLION UNITS',
	'PENICILLIN G IVPB MINIBAG PLUS 5 MILLION UNITS',
	'PENICILLIN G POTASSIUM 20000000 UNITS IJ SOLR',
	'PENICILLIN G POTASSIUM 5000000 UNITS IJ SOLR',
	'PENICILLIN G POTASSIUM IN D5W 40000 UNIT/ML IV SOLN',
	'PENICILLIN G POTASSIUM IN D5W 60000 UNIT/ML IV SOLN',
	'PENICILLIN G POT IN DEXTROSE 20000 UNIT/ML IV SOLN',
	'PENICILLIN G POT IN DEXTROSE 40000 UNIT/ML IV SOLN',
	'PENICILLIN G POT IN DEXTROSE 60000 UNIT/ML IV SOLN',
	'PENICILLIN G PROCAINE 600000 UNIT/ML IM SUSP',
	'PENICILLIN G PROC & BENZATHINE 300000-900000 UNIT/ML IM SUSP',
	'PENICILLIN G PROC & BENZATHINE 600000 UNIT/ML IM SUSP',
	'PENICILLIN G SODIUM 5000000 IU IJ SOLR',
	'AZITHROMYCIN 1 G PO PACK',
	'ZITHROMAX 1 G PO PACK',
	'AZITHROMYCIN 2 G PO SUSR',
	'CEFTRIAXONE SODIUM 250 MG IJ SOLR',
	'ROCEPHIN 250 MG IJ SOLR', 
	--BMC 
	'BICILLIN L-A 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
    'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
    'PENICILLIN G IVPB  IN 100 ML',
    'PENICILLIN G IVPB IN 50 ML',
    'PENICILLIN G POTASSIUM 20 MILLION UNIT SOLUTION FOR INJECTION',
    'PENICILLIN G POTASSIUM 5 MILLION UNIT SOLUTION FOR INJECTION',
    'PENICILLIN G POTASSIUM IV 5 MILLION UNITS MBP',
    'PENICILLIN G SODIUM 5 MILLION UNIT SOLUTION FOR INJECTION',
	'BICILLIN L-A 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE',
	'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE',
	'PENICILLIN G BENZATHINE 600,000 UNIT/ML INTRAMUSCULAR SYRINGE',
	'AZITHROMYCIN 1 GRAM ORAL PACKET',
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION',
	'CEFTRIAXONE IM INJECTION <=250 MG')
OR (name ilike '%methadone%' 
	or name ilike '%dolophine%' 
	or name ilike '%methadose%' 
	or name ilike '%suboxone%'  
	or name ilike 'buprenorphine%naloxone%'
	or name ilike '%zubsolv%' 
	or name ilike '%bunavail%' 
	or name ilike '%cassipa%'  
	or name ilike '%tadalafil%'  
	or name ilike '%cilais%'  
	or name ilike '%vardenafil%' 
	or name ilike '%staxyn%' 
	or name ilike '%levitra%' 
	or name ilike '%sildenafil%'  
	or name ilike '%viagra%') 
AND name not ilike '%revatio%'
AND name not ilike '%ANTIHYPERTENSIVE%'
AND name not ilike '%HYPERTENSION%'
AND name not ilike '%adcirca%'
AND name not ilike '%alyq%');
	
	
CREATE TABLE hiv_rpt.r21_all_rx_of_interest AS 
SELECT patient_id, date,  total_quantity_per_rx, rx_truvada, other_hiv_rx_non_truvada, null as rx_bicillin,	null as rx_azithromycin, null as rx_ceftriaxone, null as rx_methadone, null as rx_suboxone, null as rx_viagara_cilais_or_levitra 
FROM hiv_rpt.r21_hiv_meds
UNION
SELECT patient_id, date,  total_quantity_per_rx, null as rx_truvada, null as other_hiv_rx_non_truvada, rx_bicillin,	rx_azithromycin, rx_ceftriaxone, rx_methadone, rx_suboxone, rx_viagara_cilais_or_levitra
FROM hiv_rpt.r21_non_hiv_rx_of_interest
;

CREATE TABLE hiv_rpt.r21_rx_of_int_counts as
SELECT T1.master_patient_id as patient_id, month_id, month_start,
case when month_id = 0 then count(case when rx_truvada = 1 and T2.date < month_start and T2.date >= month_start - interval '3 months' then 1 end) else null end prior_rx_truvada_3m,
case when month_id = 0 then sum(case when rx_truvada = 1 and T2.date < month_start and T2.date >= month_start - interval '3 months' then T2.total_quantity_per_rx end) else null end prior_rx_truvada_3m_total_quantity,
case when month_id = 0 then count(case when rx_truvada = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then 1 end) else null end prior_rx_truvada_12m,
case when month_id = 0 then sum(case when rx_truvada = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_truvada_12m_total_quantity,
case when month_id = 0 then count(case when other_hiv_rx_non_truvada = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then 1 end) else null end prior_rx_hiv_other,
case when month_id = 0 then sum(case when other_hiv_rx_non_truvada = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_hiv_other_total_quantity,
case when month_id = 0 then count(case when rx_bicillin = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then 1 end) else null end prior_rx_bicillin,	
case when month_id = 0 then sum(case when rx_bicillin = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_bicillin_total_quantity,	
case when month_id = 0 then count(case when rx_azithromycin = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then 1 end) else null end prior_rx_azithromycin,	
case when month_id = 0 then sum(case when rx_azithromycin = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_azithromycin_total_quantity,	
case when month_id = 0 then count(case when rx_ceftriaxone = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then 1 end) else null end prior_rx_ceftriaxone,
case when month_id = 0 then sum(case when rx_ceftriaxone = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_ceftriaxone_total_quantity,		
case when month_id = 0 then count(case when rx_methadone = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then 1 end) else null end prior_rx_methadone,
case when month_id = 0 then sum(case when rx_methadone = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_methadone_total_quantity,	
case when month_id = 0 then count(case when rx_suboxone = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then 1 end) else null end prior_rx_suboxone,
case when month_id = 0 then sum(case when rx_suboxone = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_suboxone_total_quantity,
case when month_id = 0 then count(case when rx_viagara_cilais_or_levitra = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then 1 end) else null end prior_rx_viagara_cilais_or_levitra,
case when month_id = 0 then sum(case when rx_viagara_cilais_or_levitra = 1 and T2.date < month_start and T2.date >= month_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_viagara_cilais_or_levitra_total_quantity,
max(case when rx_truvada = 1 and T2.date >= month_start and T2.date < month_end then 1 end) as rx_truvada,
sum(case when rx_truvada = 1 and T2.date >= month_start and T2.date < month_end then T2.total_quantity_per_rx end) as rx_truvada_total_quantity,
max(case when other_hiv_rx_non_truvada = 1 and T2.date >= month_start and T2.date < month_end then 1 end) as rx_hiv_other,
sum(case when other_hiv_rx_non_truvada  = 1 and T2.date >= month_start and T2.date < month_end then T2.total_quantity_per_rx end) as rx_hiv_other_total_quantity,
max(case when rx_bicillin = 1 and T2.date >= month_start and T2.date < month_end then 1 end) as rx_bicillin,
sum(case when rx_bicillin = 1 and T2.date >= month_start and T2.date < month_end then T2.total_quantity_per_rx end) as rx_bicillin_total_quantity,
max(case when rx_azithromycin = 1 and T2.date >= month_start and T2.date < month_end then 1 end) as rx_azithromycin,
sum(case when rx_azithromycin = 1 and T2.date >= month_start and T2.date < month_end then T2.total_quantity_per_rx end) as rx_azithromycin_total_quantity,
max(case when rx_ceftriaxone = 1 and T2.date >= month_start and T2.date < month_end then 1 end) as rx_ceftriaxone,
sum(case when rx_ceftriaxone = 1 and T2.date >= month_start and T2.date < month_end then T2.total_quantity_per_rx end) as rx_ceftriaxone_total_quantity,
max(case when rx_methadone = 1 and T2.date >= month_start and T2.date < month_end then 1 end) as rx_methadone,
sum(case when rx_methadone = 1 and T2.date >= month_start and T2.date < month_end then T2.total_quantity_per_rx end) as rx_methadone_total_quantity,
max(case when rx_suboxone = 1 and T2.date >= month_start and T2.date < month_end then 1 end) as rx_suboxone,
sum(case when rx_suboxone = 1 and T2.date >= month_start and T2.date < month_end then T2.total_quantity_per_rx end) as rx_suboxone_total_quantity,
max(case when rx_viagara_cilais_or_levitra = 1 and T2.date >= month_start and T2.date < month_end then 1 end) as rx_viagara_cilais_or_levitra,
sum(case when rx_viagara_cilais_or_levitra = 1 and T2.date >= month_start and T2.date < month_end then T2.total_quantity_per_rx end) as rx_viagara_cilais_or_levitra_total_quantity
FROM hiv_rpt.r21_month_intervals T1
JOIN hiv_rpt.r21_all_rx_of_interest T2 on (T2.patient_id = T1.master_patient_id)
--AND T1.patient_id in (1688763, 38662886)
GROUP BY T1.master_patient_id, month_id, month_start
ORDER BY T1.master_patient_id,month_id;



-------------------
------------------- DIAGNOSIS CODES
-------------------

CREATE TABLE hiv_rpt.r21_dx_codes AS 
SELECT T3.*, T2.patient_id, T2.date
FROM 
hiv_rpt.r21_index_pats T1
JOIN emr_encounter T2 on (T1.master_patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 on (T3.encounter_id = T2.id)
WHERE T2.date >= '01-01-2010'
AND T2.date < '01-01-2018'
AND (dx_code_id ~
'^(icd10:B20|icd10:Z21|icd9:796.7|icd10:R85.61|icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.|icd9:099.4|icd9:054.1|icd10:A60.0|icd10:A63.|icd10:Z72.5|icd9:296.2|icd9:296.3|icd10:F32.|icd10:F33.|icd9:314.|icd10:F90.|icd9:649.0|icd10:F17.|icd9:305.0|icd9:304.0|icd9:305.5|icd10:F11.|icd9:304.1|icd9:305.4|icd10:F13.|icd9:304.2|icd9:305.6|icd10:F14.|icd9:304.4|icd9:305.7|icd10:F15.|icd9:304.9|icd9:292.8|icd10:F19.|icd10:F10.1|icd10:T18.5|icd10:F50.0|icd10:F50.8)'
OR dx_code_id in (
    'icd9:042',
    'icd9:V08',
	'icd9:079.53',
	'icd10:B97.35',
	'icd9:569.44',
	'icd10:K62.82',
	'icd9:230.5',
	'icd9:230.6',
	'icd10:D01.3',	
	'icd9:097.9',
	'icd9:097.1',
	'icd10:A53.0',
	'icd10:A53.9',
	'icd9:091.1',
	'icd10:A51.1',
	'icd9:098.7',
	'icd10:A54.6',
	'icd9:098.6',
	'icd10:A54.5', 
	'icd9:099.52', 
	'icd10:A56.3', 
	'icd9:099.51', 
	'icd10:A56.4', 
	'icd9:099.1', 
	'icd10:A55', 
	'icd9:099.0', 
	'icd10:A57', 
	'icd9:099.2', 
	'icd10:A58', 
	'icd10:N34.1', 
	'icd9:054.8', 
	'icd9:054.9', 
	'icd9:054.79', 
	'icd10:A60.1', 
	'icd10:A60.9', 
	'icd10:B00.9',
	'icd9:078.11', 
	'icd10:A63.0', 
	'icd9:569.41', 
	'icd10:K62.6'
	'icd9:099.8',
	'icd9:099.9',
	'icd10:A64', 
	'icd9:V01.6',
	'icd10:Z20.2', 
	'icd10:Z20.6', 
	'icd9:V69.2', 
	'icd9:V65.44', 
	'icd10:Z71.7', 
	'icd9:307.1', 
	'icd9:307.51', 
	'icd10:F50.2', 
	'icd9:307.50', 
	'icd10:F50.9', 
	'icd9:296.82',
	'icd9:300.4',
	'icd9:301.12',
	'icd9:308',
	'icd9:309',
	'icd9:309.1',
	'icd9:309.28',
	'icd9:311',
	'icd10:F34.1',
	'icd10:F43.21',
	'icd10:F43.23',
	'icd10:F06.31',
	'icd10:F06.32',
	'icd9:302.0',
	'icd9:302.6',
	'icd9:302.85',
	'icd10:F64.2',
	'icd10:F64.8',
	'icd10:F64.9',
	'icd10:F66.0',
	'icd9:302.5', 
	'icd9:302.50', 
	'icd9:302.51', 
	'icd9:302.52', 
	'icd9:302.53', 
	'icd10:F64.0',
	'icd10:F64.1',
	'icd10:F65.1',
	'icd9:V61.21',
	'icd10:Z61.4', 
	'icd10:Z61.5',
	'icd10:Z69.010',
	'icd9:937', 
	'icd9:305.1',
	'icd9:V15.82',
	'icd9:303.90', 
	'icd9:303.91', 
	'icd9:303.92', 
	'icd10:F10.2'
));



CREATE TABLE hiv_rpt.r21_dx_counts AS 
SELECT patient_id, month_id, month_start,
-- icd10:O98.7 removed as this is for males only
case when month_id = 0 then count(case when dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B97.35|icd10:Z21)' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_hiv,
case when month_id = 0 then count(case when dx_code_id ~ '^(icd9:796.7|icd10:R85.61)' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_abn_anal_cyt,
case when month_id = 0 then count(case when dx_code_id in ('icd9:569.44', 'icd10:K62.82') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_anal_dyspl,
case when month_id = 0 then count(case when dx_code_id in ('icd9:230.5', 'icd9:230.6', 'icd10:D01.3') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_anal_carc,
case when month_id = 0 then count(case when (dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1')) and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_syph,
case when month_id = 0 then count(case when dx_code_id in ('icd9:091.1', 'icd10:A51.1') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_anal_syph,
case when month_id = 0 then count(case when dx_code_id in ('icd9:098.7','icd10:A54.6') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_gonoc_inf_rectum,
case when month_id = 0 then count(case when dx_code_id in ('icd9:098.6','icd10:A54.5') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_gonoc_pharyn,
case when month_id = 0 then count(case when dx_code_id in ('icd9:099.52','icd10:A56.3') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_chlam_inf_rectum,
case when month_id = 0 then count(case when dx_code_id in ('icd9:099.51','icd10:A56.4') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_chlam_inf_pharyn,
case when month_id = 0 then count(case when dx_code_id in ('icd9:099.1','icd10:A55') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_lymphgranuloma_venereum,
case when month_id = 0 then count(case when dx_code_id in ('icd9:099.0','icd10:A57') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_chancroid,
case when month_id = 0 then count(case when dx_code_id in ('icd9:099.2', 'icd10:A58') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_granuloma_inguinale,
case when month_id = 0 then count(case when (dx_code_id like 'icd9:099.4%' or dx_code_id in ('icd10:N34.1')) and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_nongonococcal_urethritis,
case when month_id = 0 then count(case when dx_code_id in ('icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9', 'icd10:B00.9') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_herpes_simplex_w_complications,
case when month_id = 0 then count(case when dx_code_id ~ '^(icd9:054.1|icd10:A60.0)' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_genital_herpes,
case when month_id = 0 then count(case when dx_code_id in ('icd9:078.11', 'icd10:A63.0') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_anogenital_warts,
case when month_id = 0 then count(case when dx_code_id in ('icd9:569.41', 'icd10:K62.6') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_anorectal_ulcer,
case when month_id = 0 then count(case when (dx_code_id like 'icd10:A63.%' or dx_code_id in ('icd10:A64', 'icd9:099.8', 'icd9:099.9')) and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_unspecified_std,
case when month_id = 0 then count(case when dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_contact_with_or_exposure_to_venereal_disease,
case when month_id = 0 then count(case when (dx_code_id ~ '^(icd10:Z72.5)' or dx_code_id in ('icd9:V69.2')) and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_high_risk_sexual_behavior,
case when month_id = 0 then count(case when dx_code_id in ('icd9:V65.44','icd10:Z71.7') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_hiv_counseling,
case when month_id = 0 then count(case when (dx_code_id ~ '^(icd10:F50.0)' or dx_code_id in ('icd9:307.1')) and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_anorexia_nervosa,
case when month_id = 0 then count(case when dx_code_id in ('icd9:307.51','icd10:F50.2') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_bulimia_nervosa,
case when month_id = 0 then count(case when (dx_code_id ~ '^(icd10:F50.8)' or dx_code_id in ('icd9:307.50','icd10:F50.9')) and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_eating_disorder_nos,
case when month_id = 0 then count(case when (dx_code_id ~ '^(icd9:296.2|icd9:296.3|icd10:F32.|icd10:F33.)' or dx_code_id in ('icd9:296.82', 'icd9:300.4', 'icd9:301.12', 'icd9:308', 'icd9:309', 'icd9:309.1', 'icd9:309.28', 'icd9:311', 'icd10:F34.1', 'icd10:F43.21', 'icd10:F43.23', 'icd10:F06.31', 'icd10:F06.32')) and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_depression,
case when month_id = 0 then count(case when dx_code_id ~ '^(icd9:314.|icd10:F90.)' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_attn_def_disorder,
case when month_id = 0 then count(case when dx_code_id in ('icd9:302.0', 'icd9:302.6', 'icd9:302.85', 'icd10:F64.2', 'icd10:F64.8', 'icd10:F64.9', 'icd10:F66.0', 'icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F64.0','icd10:F64.1', 'icd10:F65.1', 'icd10:Z87.890') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_gend_iden_trans_sex_reassign,
case when month_id = 0 then count(case when dx_code_id in ('icd9:V61.21', 'icd10:Z61.4', 'icd10:Z61.5', 'icd10:Z69.010') and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_counseling_for_child_sexual_abuse,
case when month_id = 0 then count(case when (dx_code_id ~ '^(icd10:T18.5)' or dx_code_id in ('icd9:937')) and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_foreign_body_in_anus,
case when month_id = 0 then count(case when (dx_code_id ~ '^(icd9:649.0|icd10:F17.)' or dx_code_id in ('icd9:305.1', 'icd9:V15.82')) and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_smoking_tobacco_use,
case when month_id = 0 then count(case when (dx_code_id ~ '^(icd9:305.0|icd10:F10.1)' or dx_code_id in ('icd9:303.90', 'icd9:303.91', 'icd9:303.92', 'icd10:F10.2')) and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_alcohol_dependence_abuse,
case when month_id = 0 then count(case when dx_code_id ~ '^(icd9:304.0|icd10:F11.|icd9:305.5)' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_opioid_dependence_abuse,
case when month_id = 0 then count(case when dx_code_id ~ '^(icd9:304.1|icd9:305.4|icd10:F13.)' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_sed_hypn_anxio_depend_abuse,
case when month_id = 0 then count(case when dx_code_id ~ '^(icd9:304.2|icd9:305.6|icd10:F14.)' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_cocaine_dependence_abuse,
case when month_id = 0 then count(case when dx_code_id ~ '^(icd9:304.4|icd9:305.7|icd10:F15.)' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_amphet_stim_dependence_abuse,
case when month_id = 0 then count(case when dx_code_id ~ '^(icd9:304.9|icd9:292.8|icd10:F19)' and date < month_start and date >= month_start - interval '12 months' then 1 end) else null end prior_dx_oth_psycho_or_unspec_subs_depend_abuse,
max(case when dx_code_id ~ '^(icd9:796.7|icd10:R85.61)' and date >= month_start and date < month_end then 1 end) as dx_abn_anal_cyt,
max(case when dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B97.35|icd10:Z21)' and date >= month_start and date < month_end then 1 end) as dx_hiv,
max(case when dx_code_id in ('icd9:569.44', 'icd10:K62.82') and date >= month_start and date < month_end then 1 end) as dx_anal_dyspl,
max(case when dx_code_id in ('icd9:230.5', 'icd9:230.6', 'icd10:D01.3') and date >= month_start and date < month_end then 1 end) as dx_anal_carc,
max(case when (dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1')) and date >= month_start and date < month_end then 1 end) as dx_syph,
max(case when dx_code_id in ('icd9:091.1', 'icd10:A51.1') and date >= month_start and date < month_end then 1 end) as dx_anal_syph,
max(case when dx_code_id in ('icd9:098.7','icd10:A54.6') and date >= month_start and date < month_end then 1 end) as dx_gonoc_inf_rectum,
max(case when dx_code_id in ('icd9:098.6','icd10:A54.5') and date >= month_start and date < month_end then 1 end) as dx_gonoc_pharyn,
max(case when dx_code_id in ('icd9:099.52','icd10:A56.3') and date >= month_start and date < month_end then 1 end) as dx_chlam_inf_rectum,
max(case when dx_code_id in ('icd9:099.51','icd10:A56.4') and date >= month_start and date < month_end then 1 end) as dx_chlam_inf_pharyn,
max(case when dx_code_id in ('icd9:099.1','icd10:A55') and date >= month_start and date < month_end then 1 end) as dx_lymphgranuloma_venereum,
max(case when dx_code_id in ('icd9:099.0','icd10:A57') and date >= month_start and date < month_end then 1 end) as dx_chancroid,
max(case when dx_code_id in ('icd9:099.2', 'icd10:A58') and date >= month_start and date < month_end then 1 end) as dx_granuloma_inguinale,
max(case when (dx_code_id like 'icd9:099.4%' or dx_code_id in ('icd10:N34.1')) and date >= month_start and date < month_end then 1 end) as dx_nongonococcal_urethritis,
max(case when dx_code_id in ('icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9', 'icd10:B00.9') and date >= month_start and date < month_end then 1 end) as dx_herpes_simplex_w_complications,
max(case when dx_code_id ~ '^(icd9:054.1|icd10:A60.0)' and date >= month_start and date < month_end then 1 end) as dx_genital_herpes,
max(case when dx_code_id in ('icd9:078.11', 'icd10:A63.0') and date >= month_start and date < month_end then 1 end) as dx_anogenital_warts,
max(case when dx_code_id in ('icd9:569.41', 'icd10:K62.6') and date >= month_start and date < month_end then 1 end) as dx_anorectal_ulcer,
max(case when (dx_code_id like 'icd10:A63.%' or dx_code_id in ('icd10:A64', 'icd9:099.8', 'icd9:099.9')) and date >= month_start and date < month_end then 1 end) as dx_unspecified_std,
max(case when dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') and date >= month_start and date < month_end then 1 end) as dx_contact_with_or_exposure_to_venereal_disease,
max(case when (dx_code_id ~ '^(icd10:Z72.5)' or dx_code_id in ('icd9:V69.2')) and date >= month_start and date < month_end then 1 end) as dx_high_risk_sexual_behavior,
max(case when dx_code_id in ('icd9:V65.44','icd10:Z71.7') and date >= month_start and date < month_end then 1 end) as dx_hiv_counseling,
max(case when (dx_code_id ~ '^(icd10:F50.0)' or dx_code_id in ('icd9:307.1')) and date >= month_start and date < month_end then 1 end) as dx_anorexia_nervosa,
max(case when dx_code_id in ('icd9:307.51','icd10:F50.2') and date >= month_start and date < month_end then 1 end) as dx_bulimia_nervosa,
max(case when (dx_code_id ~ '^(icd10:F50.8)' or dx_code_id in ('icd9:307.50','icd10:F50.9')) and date >= month_start and date < month_end then 1 end) as dx_eating_disorder_nos,
max(case when (dx_code_id ~ '^(icd9:296.2|icd9:296.3|icd10:F32.|icd10:F33.)' or dx_code_id in ('icd9:296.82', 'icd9:300.4', 'icd9:301.12', 'icd9:308', 'icd9:309', 'icd9:309.1', 'icd9:309.28', 'icd9:311', 'icd10:F34.1', 'icd10:F43.21', 'icd10:F43.23', 'icd10:F06.31', 'icd10:F06.32')) and date >= month_start and date < month_end then 1 end) as dx_depression,
max(case when dx_code_id ~ '^(icd9:314.|icd10:F90.)' and date >= month_start and date < month_end then 1 end) as dx_attn_def_disorder,
max(case when dx_code_id in ('icd9:302.0', 'icd9:302.6', 'icd9:302.85', 'icd10:F64.2', 'icd10:F64.8', 'icd10:F64.9', 'icd10:F66.0', 'icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F64.0','icd10:F64.1', 'icd10:F65.1', 'icd10:Z87.890') and date >= month_start and date < month_end then 1 end) as dx_gend_iden_trans_sex_reassign,
max(case when dx_code_id in ('icd9:V61.21', 'icd10:Z61.4', 'icd10:Z61.5', 'icd10:Z69.010') and date >= month_start and date < month_end then 1 end) as dx_counseling_for_child_sexual_abuse,
max(case when (dx_code_id ~ '^(icd10:T18.5)' or dx_code_id in ('icd9:937')) and date >= month_start and date < month_end then 1 end) as dx_foreign_body_in_anus,
max(case when (dx_code_id ~ '^(icd9:649.0|icd10:F17.)' or dx_code_id in ('icd9:305.1', 'icd9:V15.82')) and date >= month_start and date < month_end then 1 end) as dx_smoking_tobacco_use,
max(case when (dx_code_id ~ '^(icd9:305.0|icd10:F10.1)' or dx_code_id in ('icd9:303.90', 'icd9:303.91', 'icd9:303.92', 'icd10:F10.2')) and date >= month_start and date < month_end then 1 end) as dx_alcohol_dependence_abuse,
max(case when dx_code_id ~ '^(icd9:304.0|icd10:F11.|icd9:305.5)' and date >= month_start and date < month_end then 1 end) as dx_opioid_dependence_abuse,
max(case when dx_code_id ~ '^(icd9:304.1|icd9:305.4|icd10:F13.)' and date >= month_start and date < month_end then 1 end) as dx_sed_hypn_anxio_depend_abuse,
max(case when dx_code_id ~ '^(icd9:304.2|icd9:305.6|icd10:F14.)' and date >= month_start and date < month_end then 1 end) as dx_cocaine_dependence_abuse,
max(case when dx_code_id ~ '^(icd9:304.4|icd9:305.7|icd10:F15.)' and date >= month_start and date < month_end then 1 end) as dx_amphet_stim_dependence_abuse,
max(case when dx_code_id ~ '^(icd9:304.9|icd9:292.8|icd10:F19)' and date >= month_start and date < month_end then 1 end) as dx_oth_psycho_or_unspec_subs_depend_abuse
FROM
hiv_rpt.r21_dx_codes T1,
hiv_rpt.r21_month_intervals T2
WHERE T1.patient_id = T2.master_patient_id
GROUP BY patient_id, month_id, month_start
ORDER BY patient_id,month_id;

-------------------
------------------- BMI
-------------------

CREATE TABLE hiv_rpt.r21_bmi_measurements as
SELECT T2.patient_id, T2.date, T2.bmi 
FROM hiv_rpt.r21_index_pats T1
INNER JOIN emr_encounter T2 ON (T1.master_patient_id = T2.patient_id)
WHERE T2.date >= '12-01-2010'
AND T2.date < '01-01-2018'
AND bmi is not null;


CREATE TABLE hiv_rpt.r21_bmi_counts as 
SELECT T1.master_patient_id as patient_id, T1.month_id, T1.month_start,
case when T1.month_id = 0 then max(case when date = max_prior_bmi_date then bmi end)else null end prior_bmi,
max(case when date = max_in_month_bmi_date then bmi end) as bmi
--max(case when T1.month_id = T4.month_id and T2.date = max_in_month_bmi_date then bmi else null end) bmi4
FROM hiv_rpt.r21_month_intervals T1
JOIN hiv_rpt.r21_bmi_measurements T2 ON (T1.master_patient_id = T2.patient_id)
LEFT JOIN (
	select patient_id, month_id, month_start,
	max(case when date < month_start and date >= month_start - interval '12 months' then date end) max_prior_bmi_date
	FROM hiv_rpt.r21_month_intervals T1
	JOIN hiv_rpt.r21_bmi_measurements T2 ON (T1.master_patient_id = T2.patient_id)
	WHERE month_id = 0
	--AND patient_id = 32
	GROUP BY patient_id, month_id, month_start) T3 on (T2.date = T3.max_prior_bmi_date and T1.master_patient_id = T3.patient_id)
LEFT JOIN (
        select patient_id, month_id, month_start,
	max(case when date >= month_start and date < month_end then date end) max_in_month_bmi_date
	FROM hiv_rpt.r21_month_intervals T1
	JOIN hiv_rpt.r21_bmi_measurements T2 ON (T1.master_patient_id = T2.patient_id)
	--AND patient_id = 32
        GROUP BY patient_id, month_id, month_start) T4 on (T2.date = T4.max_in_month_bmi_date and T1.master_patient_id = T4.patient_id and T1.month_id = T4.month_id)
--WHERE T1.master_patient_id = 32
GROUP BY T1.master_patient_id, T1.month_id, T1.month_start
order by month_id;


-------------------
------------------- HIV RISK SCORE
-------------------


-- HIV RISK SCORE FOR MONTH 0 IS COMPUTED VIA FUNCTION THAT IS CALLED FROM SHELL SCRIPT
-- THE FINAL OUTPUT TABLE IS THEN UPDATED WITH THE VALUE OF THE RISK SCORE

-------------------
------------------- BASELINE TABLE
-------------------

CREATE TABLE hiv_rpt.r21_baseline_table as
SELECT 
T1.master_patient_id,
T1.month_0,
CASE WHEN T1.age_month_0 > 89 then '90+' else T1.age_month_0::varchar END age_month_0,
T1.sex,
T1.race,
T1.ethnicity,
T1.home_language,
T1.female_partner_yn,
T1.male_partner_yn,
coalesce(T3.prior_gon_chlam_rectal_throat_ever, 0) as prior_gon_chlam_rectal_throat_ever,
null::numeric as risk_score_month_0,
coalesce(T7.prior_gon_cases, 0) as prior_gon_cases,
coalesce(T3.prior_pos_gon_rectal, 0) prior_pos_gon_rectal,
coalesce(T3.prior_pos_gon_oropharynx,0) prior_pos_gon_oropharynx,
coalesce(T3.prior_pos_gon_urog, 0) prior_pos_gon_urog,
coalesce(T7.prior_chlam_cases, 0) as prior_chlam_cases,
coalesce(T3.prior_pos_chlam_rectal, 0) prior_pos_chlam_rectal,
coalesce(T3.prior_pos_chlam_oropharynx, 0) prior_pos_chlam_oropharynx,
coalesce(T3.prior_pos_chlam_urog, 0) prior_pos_chlam_urog,
coalesce(T7.prior_syph_cases, 0) as prior_syph_cases,
coalesce(T8.sti_esp_case_encounters, 0) as sti_esp_case_encounters,
coalesce(T9.number_of_encs, 0) as number_of_encs,
coalesce(T10.prior_hiv_elisa, 0) prior_hiv_elisa,
coalesce(T10.prior_hiv_wb, 0) prior_hiv_wb,
coalesce(T10.prior_hiv_rna_viral, 0) prior_hiv_rna_viral,
coalesce(T10.prior_hiv_pcr, 0) prior_hiv_pcr,
coalesce(T10.prior_hiv_ag_ab, 0) prior_hiv_ag_ab,
coalesce(T4.gon_chlam_syph_tst_episodes, 0) as gon_chlam_syph_tst_episodes,
coalesce(T5.gon_chlam_rectal_tst_episodes, 0) as gon_chlam_rectal_tst_episodes,
coalesce(T6.gon_chlam_orpharynx_tst_episodes , 0) as gon_chlam_orpharynx_tst_episodes,
coalesce(T11.prior_hepc_elisa, 0) as prior_hepc_elisa,
coalesce(T11.prior_hepc_rna, 0) as prior_hepc_rna,
coalesce(T11.prior_pos_hepc_elisa, 0) as prior_pos_hepc_elisa,
coalesce(T11.prior_pos_hepc_rna, 0) as prior_pos_hepc_rna,
coalesce(T11.prior_hepb_surf_ant, 0) as prior_hepb_surf_ant,
coalesce(T11.prior_hepb_viral_dna, 0) as prior_hepb_viral_dna,
coalesce(T11.prior_hepb_core_ag_igm_ab, 0) as prior_hepb_core_ag_igm_ab,
coalesce(T11.prior_pos_hepb_core_ag_igm_ab, 0) as prior_pos_hepb_core_ag_igm_ab,
coalesce(T11.prior_pos_hepb, 0) as prior_pos_hepb,
coalesce(T12.prior_rx_truvada_3m, 0) as prior_rx_truvada_3m,
coalesce(T12.prior_rx_truvada_3m_total_quantity, 0) as prior_rx_truvada_3m_total_quantity,
coalesce(T12.prior_rx_truvada_12m, 0) as prior_rx_truvada_12m,
coalesce(T12.prior_rx_truvada_12m_total_quantity, 0) as prior_rx_truvada_12m_total_quantity,
coalesce(T12.prior_rx_hiv_other, 0) as prior_rx_hiv_other,
coalesce(T12.prior_rx_hiv_other_total_quantity, 0) as prior_rx_hiv_other_total_quantity,
coalesce(T12.prior_rx_bicillin, 0) as prior_rx_bicillin,
coalesce(T12.prior_rx_bicillin_total_quantity, 0) as prior_rx_bicillin_total_quantity,
coalesce(T12.prior_rx_azithromycin, 0) as prior_rx_azithromycin, 
coalesce(T12.prior_rx_azithromycin_total_quantity, 0) as prior_rx_azithromycin_total_quantity,
coalesce(T12.prior_rx_ceftriaxone, 0 ) as prior_rx_ceftriaxone, 
coalesce(T12.prior_rx_ceftriaxone_total_quantity, 0) as prior_rx_ceftriaxone_total_quantity,
coalesce(T12.prior_rx_methadone, 0) as prior_rx_methadone, 
coalesce(T12.prior_rx_methadone_total_quantity, 0) as prior_rx_methadone_total_quantity,
coalesce(T12.prior_rx_suboxone, 0 ) as prior_rx_suboxone, 
coalesce(T12.prior_rx_suboxone_total_quantity, 0) as prior_rx_suboxone_total_quantity,
coalesce(T12.prior_rx_viagara_cilais_or_levitra, 0) as prior_rx_viagara_cilais_or_levitra, 
coalesce(T12.prior_rx_viagara_cilais_or_levitra_total_quantity, 0) as prior_rx_viagara_cilais_or_levitra_total_quantity,
coalesce(T13.prior_dx_hiv, 0) as prior_dx_hiv,
coalesce(T13.prior_dx_abn_anal_cyt, 0) as prior_dx_abn_anal_cyt,
coalesce(T7.prior_hep_b_acute_case, 0) as prior_hep_b_acute_case,
coalesce(T7.prior_hep_c_acute_case, 0) as prior_hep_c_acute_case,
coalesce(T13.prior_dx_anal_dyspl, 0) as prior_dx_anal_dyspl,
coalesce(T13.prior_dx_anal_carc, 0) as prior_dx_anal_carc,
coalesce(T13.prior_dx_syph, 0) as prior_dx_syph,
coalesce(T13.prior_dx_anal_syph, 0) as prior_dx_anal_syph,
coalesce(T13.prior_dx_gonoc_inf_rectum, 0) as prior_dx_gonoc_inf_rectum,
coalesce(T13.prior_dx_gonoc_pharyn,0) as prior_dx_gonoc_pharyn,
coalesce(T13.prior_dx_chlam_inf_rectum, 0) as prior_dx_chlam_inf_rectum,
coalesce(T13.prior_dx_chlam_inf_pharyn, 0) as prior_dx_chlam_inf_pharyn,
coalesce(T13.prior_dx_lymphgranuloma_venereum, 0) as prior_dx_lymphgranuloma_venereum,
coalesce(T13.prior_dx_chancroid, 0) as prior_dx_chancroid,
coalesce(T13.prior_dx_granuloma_inguinale, 0) as prior_dx_granuloma_inguinale,
coalesce(T13.prior_dx_nongonococcal_urethritis, 0) as prior_dx_nongonococcal_urethritis,
coalesce(T13.prior_dx_herpes_simplex_w_complications, 0) as prior_dx_herpes_simplex_w_complications,
coalesce(T13.prior_dx_genital_herpes, 0) as prior_dx_genital_herpes,
coalesce(T13.prior_dx_anogenital_warts, 0) as prior_dx_anogenital_warts,
coalesce(T13.prior_dx_anorectal_ulcer, 0) as prior_dx_anorectal_ulcer,
coalesce(T13.prior_dx_unspecified_std, 0) as prior_dx_unspecified_std,
coalesce(T13.prior_dx_contact_with_or_exposure_to_venereal_disease, 0) as prior_dx_contact_with_or_exposure_to_venereal_disease,
coalesce(T13.prior_dx_high_risk_sexual_behavior, 0) as prior_dx_high_risk_sexual_behavior,
coalesce(T13.prior_dx_hiv_counseling, 0) as prior_dx_hiv_counseling,
coalesce(T13.prior_dx_anorexia_nervosa, 0) as prior_dx_anorexia_nervosa,
coalesce(T13.prior_dx_bulimia_nervosa, 0) as prior_dx_bulimia_nervosa,
coalesce(T13.prior_dx_eating_disorder_nos, 0) as prior_dx_eating_disorder_nos,
coalesce(T13.prior_dx_depression, 0) as prior_dx_depression,
coalesce(T13.prior_dx_attn_def_disorder, 0) as prior_dx_attn_def_disorder,
coalesce(T13.prior_dx_gend_iden_trans_sex_reassign, 0) as prior_dx_gend_iden_trans_sex_reassign,
--coalesce(T13.prior_dx_transsexualism, 0) as prior_dx_transsexualism,
coalesce(T13.prior_dx_counseling_for_child_sexual_abuse, 0) as prior_dx_counseling_for_child_sexual_abuse,
coalesce(T13.prior_dx_foreign_body_in_anus, 0) as prior_dx_foreign_body_in_anus,
coalesce(T13.prior_dx_smoking_tobacco_use, 0) as prior_dx_smoking_tobacco_use,
coalesce(T13.prior_dx_alcohol_dependence_abuse, 0) as prior_dx_alcohol_dependence_abuse,
coalesce(T13.prior_dx_opioid_dependence_abuse, 0) as prior_dx_opioid_dependence_abuse,
coalesce(T13.prior_dx_sed_hypn_anxio_depend_abuse, 0) as prior_dx_sed_hypn_anxio_depend_abuse,
coalesce(T13.prior_dx_cocaine_dependence_abuse, 0) as prior_dx_cocaine_dependence_abuse,
coalesce(T13.prior_dx_amphet_stim_dependence_abuse, 0) as prior_dx_amphet_stim_dependence_abuse,
coalesce(T13.prior_dx_oth_psycho_or_unspec_subs_depend_abuse, 0) as prior_dx_oth_psycho_or_unspec_subs_depend_abuse,
coalesce(T14.prior_bmi, 0) as prior_bmi
FROM hiv_rpt.r21_index_pats T1
JOIN hiv_rpt.r21_month_intervals T2 on (T1.master_patient_id = T2.master_patient_id and T2.month_id = 0)
LEFT JOIN hiv_rpt.r21_gon_chlam_counts T3 on (T1.master_patient_id = T3.patient_id and T3.month_id = T2.month_id)
LEFT JOIN hiv_rpt.r21_sti_tst_episodes T4 on (T1.master_patient_id = T4.patient_id)
LEFT JOIN hiv_rpt.r21_gon_chlam_rectal_tst_episodes T5 on (T1.master_patient_id = T5.patient_id)
LEFT JOIN hiv_rpt.r21_gon_chlam_throat_tst_episodes T6 on (T1.master_patient_id = T6.patient_id)
LEFT JOIN hiv_rpt.r21_case_counts T7 on (T1.master_patient_id = T7.patient_id and T7.month_id = T2.month_id)
LEFT JOIN hiv_rpt.sti_case_encs T8 on (T1.master_patient_id = T8.patient_id)
LEFT JOIN hiv_rpt.r21_encounters T9 on (T1.master_patient_id = T9.patient_id and T9.month_id = T2.month_id)
LEFT JOIN hiv_rpt.r21_hiv_counts T10 on (T1.master_patient_id = T10.patient_id and T10.month_id = T2.month_id)
LEFT JOIN hiv_rpt.r21_hep_counts T11 on (T1.master_patient_id = T11.patient_id and T11.month_id = T2.month_id)
LEFT JOIN hiv_rpt.r21_rx_of_int_counts T12 on (T1.master_patient_id = T12.patient_id and T12.month_id = T2.month_id)
LEFT JOIN hiv_rpt.r21_dx_counts T13 on (T1.master_patient_id = T13.patient_id and T13.month_id = T2.month_id)
LEFT JOIN hiv_rpt.r21_bmi_counts T14 on (T1.master_patient_id = T14.patient_id and T14.month_id = T2.month_id)
--WHERE T1.master_patient_id in (1688763, 38662886, 9614, 7028, 145366)
ORDER BY T1.master_patient_id, T2.month_id;


-------------------
------------------- OUTPUT TABLE
-------------------

CREATE TABLE hiv_rpt.r21_output_table as
SELECT T2.*,
case when T1.month_start = end_of_followup_month then 1 else null end outcome_and_censoring,
T1.month_id,
T1.month_start,
coalesce(T6.any_enc, 0) any_enc,
coalesce(T7.any_hiv_elisa, 0) any_hiv_elisa,
coalesce(T7.pos_hiv_elisa, 0) pos_hiv_elisa,
coalesce(T7.any_hiv_wb, 0) any_hiv_wb,
coalesce(T7.pos_hiv_wb, 0) pos_hiv_wb,
coalesce(T7.any_hiv_rna_viral, 0) any_hiv_rna_viral,
coalesce(T7.pos_hiv_rna_viral, 0) pos_hiv_rna_viral,
coalesce(T7.any_hiv_ag_ab, 0) any_hiv_ag_ab,
coalesce(T7.pos_hiv_ag_ab, 0) pos_hiv_ag_ab,
coalesce(T7.any_hiv_pcr, 0) any_hiv_pcr,
coalesce(T7.pos_hiv_pcr, 0) pos_hiv_pcr,
coalesce(T3.any_gon_rectal, 0) any_gon_rectal,
coalesce(T3.pos_gon_rectal, 0) pos_gon_rectal,
coalesce(T3.any_gon_throat, 0) any_gon_oropharynx,
coalesce(T3.pos_gon_throat, 0) pos_gon_oropharynx,
coalesce(T3.any_gon_urog, 0) any_gon_urog,
coalesce(T3.pos_gon_urog, 0) pos_gon_urog,
coalesce(T3.any_chlam_rectal, 0) any_chlam_rectal,
coalesce(T3.pos_chlam_rectal, 0) pos_chlam_rectal, 
coalesce(T3.any_chlam_throat, 0) any_chlam_oropharynx,
coalesce(T3.pos_chlam_throat, 0) pos_chlam_oropharynx, 
coalesce(T3.any_chlam_urog, 0) any_chlam_urog,
coalesce(T3.pos_chlam_urog, 0) pos_chlam_urog,
coalesce(T4.any_syph, 0) any_syph,
coalesce(T5.esp_syph_case, 0) esp_syph_case,
coalesce(T8.any_hepc_elisa, 0) any_hepc_elisa,
coalesce(T8.any_hepc_rna, 0) any_hepc_rna,
coalesce(T8.pos_hepc_elisa, 0) pos_hepc_elisa,
coalesce(T8.pos_hepc_rna, 0) pos_hepc_rna,
coalesce(T8.any_hepb_surf_ant, 0) any_hepb_surf_ant,
coalesce(T8.any_hepb_viral_dna, 0) any_hepb_viral_dna,
coalesce(T8.any_hepb_core_ag_igm_ab, 0) any_hepb_core_ag_igm_ab,
coalesce(T8.pos_hepb_core_ag_igm_ab, 0) pos_hepb_core_ag_igm_ab,
coalesce(T8.pos_hepb, 0) pos_hepb,
coalesce(T9.rx_truvada, 0) rx_truvada,
coalesce(T9.rx_truvada_total_quantity, 0) rx_truvada_total_quantity,
coalesce(T9.rx_hiv_other, 0) rx_hiv_other,
coalesce(T9.rx_hiv_other_total_quantity, 0) rx_hiv_other_total_quantity,
coalesce(T9.rx_bicillin, 0) as rx_bicillin, 
coalesce(T9.rx_bicillin_total_quantity, 0) as rx_bicillin_total_quantity,
coalesce(T9.rx_azithromycin, 0) as rx_azithromycin, 
coalesce(T9.rx_azithromycin_total_quantity, 0) as rx_azithromycin_total_quantity,
coalesce(T9.rx_ceftriaxone, 0 ) as rx_ceftriaxone, 
coalesce(T9.rx_ceftriaxone_total_quantity, 0) as rx_ceftriaxone_total_quantity,
coalesce(T9.rx_methadone, 0) as rx_methadone, 
coalesce(T9.rx_methadone_total_quantity, 0) as rx_methadone_total_quantity,
coalesce(T9.rx_suboxone, 0 ) as rx_suboxone, 
coalesce(T9.rx_suboxone_total_quantity, 0) as rx_suboxone_total_quantity,
coalesce(T9.rx_viagara_cilais_or_levitra, 0) as rx_viagara_cilais_or_levitra, 
coalesce(T9.rx_viagara_cilais_or_levitra_total_quantity, 0) as rx_viagara_cilais_or_levitra_total_quantity,
coalesce(T10.dx_hiv, 0) as dx_hiv,
coalesce(T10.dx_abn_anal_cyt, 0) as dx_abn_anal_cyt,
coalesce(T5.esp_hep_b_acute_case, 0) as esp_hep_b_acute_case,
coalesce(T5.esp_hep_c_acute_case, 0) as esp_hep_c_acute_case,
coalesce(T10.dx_anal_dyspl, 0) as dx_anal_dyspl,
coalesce(T10.dx_anal_carc, 0) as dx_anal_carc,
coalesce(T10.dx_syph, 0) as dx_syph,
coalesce(T10.dx_anal_syph, 0) as dx_anal_syph,
coalesce(T10.dx_gonoc_inf_rectum, 0) as dx_gonoc_inf_rectum,
coalesce(T10.dx_gonoc_pharyn,0) as dx_gonoc_pharyn,
coalesce(T10.dx_chlam_inf_rectum, 0) as dx_chlam_inf_rectum,
coalesce(T10.dx_chlam_inf_pharyn, 0) as dx_chlam_inf_pharyn,
coalesce(T10.dx_lymphgranuloma_venereum, 0) as dx_lymphgranuloma_venereum,
coalesce(T10.dx_chancroid, 0) as dx_chancroid,
coalesce(T10.dx_granuloma_inguinale, 0) as dx_granuloma_inguinale,
coalesce(T10.dx_nongonococcal_urethritis, 0) as dx_nongonococcal_urethritis,
coalesce(T10.dx_herpes_simplex_w_complications, 0) as dx_herpes_simplex_w_complications,
coalesce(T10.dx_genital_herpes, 0) as dx_genital_herpes,
coalesce(T10.dx_anogenital_warts, 0) as dx_anogenital_warts,
coalesce(T10.dx_anorectal_ulcer, 0) as dx_anorectal_ulcer,
coalesce(T10.dx_unspecified_std, 0) as dx_unspecified_std,
coalesce(T10.dx_contact_with_or_exposure_to_venereal_disease, 0) as dx_contact_with_or_exposure_to_venereal_disease,
coalesce(T10.dx_high_risk_sexual_behavior, 0) as dx_high_risk_sexual_behavior,
coalesce(T10.dx_hiv_counseling, 0) as dx_hiv_counseling,
coalesce(T10.dx_anorexia_nervosa, 0) as dx_anorexia_nervosa,
coalesce(T10.dx_bulimia_nervosa, 0) as dx_bulimia_nervosa,
coalesce(T10.dx_eating_disorder_nos, 0) as dx_eating_disorder_nos,
coalesce(T10.dx_depression, 0) as dx_depression,
coalesce(T10.dx_attn_def_disorder, 0) as dx_attn_def_disorder,
coalesce(T10.dx_gend_iden_trans_sex_reassign, 0) as dx_gend_iden_trans_sex_reassign,
--coalesce(T10.dx_transsexualism, 0) as dx_transsexualism,
coalesce(T10.dx_counseling_for_child_sexual_abuse, 0) as dx_counseling_for_child_sexual_abuse,
coalesce(T10.dx_foreign_body_in_anus, 0) as dx_foreign_body_in_anus,
coalesce(T10.dx_smoking_tobacco_use, 0) as dx_smoking_tobacco_use,
coalesce(T10.dx_alcohol_dependence_abuse, 0) as dx_alcohol_dependence_abuse,
coalesce(T10.dx_opioid_dependence_abuse, 0) as dx_opioid_dependence_abuse,
coalesce(T10.dx_sed_hypn_anxio_depend_abuse, 0) as dx_sed_hypn_anxio_depend_abuse,
coalesce(T10.dx_cocaine_dependence_abuse, 0) as dx_cocaine_dependence_abuse,
coalesce(T10.dx_amphet_stim_dependence_abuse, 0) as dx_amphet_stim_dependence_abuse,
coalesce(T10.dx_oth_psycho_or_unspec_subs_depend_abuse, 0) as dx_oth_psycho_or_unspec_subs_depend_abuse,
coalesce(T11.bmi, 0) as bmi
FROM
hiv_rpt.r21_month_intervals T1
JOIN hiv_rpt.r21_baseline_table T2 ON (T1.master_patient_id = T2.master_patient_id)
LEFT JOIN hiv_rpt.r21_gon_chlam_counts T3 ON (T1.master_patient_id = T3.patient_id and T3.month_id = T1.month_id)
LEFT JOIN hiv_rpt.r21_syph_counts T4 ON (T1.master_patient_id = T4.patient_id and T4.month_id = T1.month_id)
LEFT JOIN hiv_rpt.r21_case_counts T5 ON (T1.master_patient_id = T5.patient_id and T5.month_id = T1.month_id)
LEFT JOIN hiv_rpt.r21_encounters T6 on (T1.master_patient_id = T6.patient_id and T6.month_id = T1.month_id)
LEFT JOIN hiv_rpt.r21_hiv_counts T7 on (T1.master_patient_id = T7.patient_id and T7.month_id = T1.month_id)
LEFT JOIN hiv_rpt.r21_hep_counts T8 on (T1.master_patient_id = T8.patient_id and T8.month_id = T1.month_id)
LEFT JOIN hiv_rpt.r21_rx_of_int_counts T9 on (T1.master_patient_id = T9.patient_id and T9.month_id = T1.month_id)
LEFT JOIN hiv_rpt.r21_dx_counts T10 on (T1.master_patient_id = T10.patient_id and T10.month_id = T1.month_id)
LEFT JOIN hiv_rpt.r21_bmi_counts T11 on (T1.master_patient_id = T11.patient_id and T11.month_id = T1.month_id)
;

-- CREATE INDEX
DROP INDEX IF EXISTS hiv_rpt.r21_output_table_master_patient_id_month_0_idx;
CREATE INDEX r21_output_table_master_patient_id_month_0_idx
  ON hiv_rpt.r21_output_table
  USING btree
  (master_patient_id, month_0);

-- CREATE A SEQUENCE FOR MASKING PATIENT ID's
CREATE SEQUENCE hiv_rpt.r21_masked_id_seq;

-- GENERATE A MASKED ID FOR ALL OUTPUT PATIENTS
CREATE TABLE hiv_rpt.r21_masked_patients as
SELECT master_patient_id,
nextval(('hiv_rpt.r21_masked_id_seq'::text)::regclass) masked_patient_id
from hiv_rpt.r21_output_table
group by master_patient_id;

-- CREATE INDEX
DROP INDEX IF EXISTS hiv_rpt.r21_masked_patients_master_patient_id_masked_patient_id_idx;
CREATE INDEX r21_masked_patients_master_patient_id_masked_patient_id_idx
  ON hiv_rpt.r21_masked_patients
  USING btree
  (master_patient_id, masked_patient_id);



-- CREATE THE OUTPUT TABLE WITH MASKED ID VALUES
CREATE TABLE hiv_rpt.r21_output_table_masked as
SELECT
masked_patient_id,
month_0,
age_month_0,
sex,
race,
ethnicity,
home_language,
female_partner_yn,
male_partner_yn,
prior_gon_chlam_rectal_throat_ever,
risk_score_month_0,
prior_gon_cases,
prior_pos_gon_rectal,
prior_pos_gon_oropharynx,
prior_pos_gon_urog,
prior_chlam_cases,
prior_pos_chlam_rectal,
prior_pos_chlam_oropharynx,
prior_pos_chlam_urog,
prior_syph_cases,
sti_esp_case_encounters,
number_of_encs,
prior_hiv_elisa,
prior_hiv_wb,
prior_hiv_rna_viral,
prior_hiv_pcr,
prior_hiv_ag_ab,
gon_chlam_syph_tst_episodes,
gon_chlam_rectal_tst_episodes,
gon_chlam_orpharynx_tst_episodes,
prior_hepc_elisa,
prior_hepc_rna,
prior_pos_hepc_elisa,
prior_pos_hepc_rna,
prior_hepb_surf_ant,
prior_hepb_viral_dna,
prior_hepb_core_ag_igm_ab,
prior_pos_hepb_core_ag_igm_ab,
prior_pos_hepb,
prior_rx_truvada_3m,
prior_rx_truvada_3m_total_quantity,
prior_rx_truvada_12m,
prior_rx_truvada_12m_total_quantity,
prior_rx_hiv_other,
prior_rx_hiv_other_total_quantity,
prior_rx_bicillin,
prior_rx_bicillin_total_quantity,
prior_rx_azithromycin,
prior_rx_azithromycin_total_quantity,
prior_rx_ceftriaxone,
prior_rx_ceftriaxone_total_quantity,
prior_rx_methadone,
prior_rx_methadone_total_quantity,
prior_rx_suboxone,
prior_rx_suboxone_total_quantity,
prior_rx_viagara_cilais_or_levitra,
prior_rx_viagara_cilais_or_levitra_total_quantity,
prior_dx_hiv,
prior_dx_abn_anal_cyt,
prior_hep_b_acute_case,
prior_hep_c_acute_case,
prior_dx_anal_dyspl,
prior_dx_anal_carc,
prior_dx_syph,
prior_dx_anal_syph,
prior_dx_gonoc_inf_rectum,
prior_dx_gonoc_pharyn,
prior_dx_chlam_inf_rectum,
prior_dx_chlam_inf_pharyn,
prior_dx_lymphgranuloma_venereum,
prior_dx_chancroid,
prior_dx_granuloma_inguinale,
prior_dx_nongonococcal_urethritis,
prior_dx_herpes_simplex_w_complications,
prior_dx_genital_herpes,
prior_dx_anogenital_warts,
prior_dx_anorectal_ulcer,
prior_dx_unspecified_std,
prior_dx_contact_with_or_exposure_to_venereal_disease,
prior_dx_high_risk_sexual_behavior,
prior_dx_hiv_counseling,
prior_dx_anorexia_nervosa,
prior_dx_bulimia_nervosa,
prior_dx_eating_disorder_nos,
prior_dx_depression,
prior_dx_attn_def_disorder,
prior_dx_gend_iden_trans_sex_reassign,
prior_dx_counseling_for_child_sexual_abuse,
prior_dx_foreign_body_in_anus,
prior_dx_smoking_tobacco_use,
prior_dx_alcohol_dependence_abuse,
prior_dx_opioid_dependence_abuse,
prior_dx_sed_hypn_anxio_depend_abuse,
prior_dx_cocaine_dependence_abuse,
prior_dx_amphet_stim_dependence_abuse,
prior_dx_oth_psycho_or_unspec_subs_depend_abuse,
prior_bmi,
outcome_and_censoring,
month_id,
month_start,
any_enc,
any_hiv_elisa,
pos_hiv_elisa,
any_hiv_wb,
pos_hiv_wb,
any_hiv_rna_viral,
pos_hiv_rna_viral,
any_hiv_ag_ab,
pos_hiv_ag_ab,
any_hiv_pcr,
pos_hiv_pcr,
any_gon_rectal,
pos_gon_rectal,
any_gon_oropharynx,
pos_gon_oropharynx,
any_gon_urog,
pos_gon_urog,
any_chlam_rectal,
pos_chlam_rectal,
any_chlam_oropharynx,
pos_chlam_oropharynx,
any_chlam_urog,
pos_chlam_urog,
any_syph,
esp_syph_case,
any_hepc_elisa,
any_hepc_rna,
pos_hepc_elisa,
pos_hepc_rna,
any_hepb_surf_ant,
any_hepb_viral_dna,
any_hepb_core_ag_igm_ab,
pos_hepb_core_ag_igm_ab,
pos_hepb,
rx_truvada,
rx_truvada_total_quantity,
rx_hiv_other,
rx_hiv_other_total_quantity,
rx_bicillin,
rx_bicillin_total_quantity,
rx_azithromycin,
rx_azithromycin_total_quantity,
rx_ceftriaxone,
rx_ceftriaxone_total_quantity,
rx_methadone,
rx_methadone_total_quantity,
rx_suboxone,
rx_suboxone_total_quantity,
rx_viagara_cilais_or_levitra,
rx_viagara_cilais_or_levitra_total_quantity,
dx_hiv,
dx_abn_anal_cyt,
esp_hep_b_acute_case,
esp_hep_c_acute_case,
dx_anal_dyspl,
dx_anal_carc,
dx_syph,
dx_anal_syph,
dx_gonoc_inf_rectum,
dx_gonoc_pharyn,
dx_chlam_inf_rectum,
dx_chlam_inf_pharyn,
dx_lymphgranuloma_venereum,
dx_chancroid,
dx_granuloma_inguinale,
dx_nongonococcal_urethritis,
dx_herpes_simplex_w_complications,
dx_genital_herpes,
dx_anogenital_warts,
dx_anorectal_ulcer,
dx_unspecified_std,
dx_contact_with_or_exposure_to_venereal_disease,
dx_high_risk_sexual_behavior,
dx_hiv_counseling,
dx_anorexia_nervosa,
dx_bulimia_nervosa,
dx_eating_disorder_nos,
dx_depression,
dx_attn_def_disorder,
dx_gend_iden_trans_sex_reassign,
dx_counseling_for_child_sexual_abuse,
dx_foreign_body_in_anus,
dx_smoking_tobacco_use,
dx_alcohol_dependence_abuse,
dx_opioid_dependence_abuse,
dx_sed_hypn_anxio_depend_abuse,
dx_cocaine_dependence_abuse,
dx_amphet_stim_dependence_abuse,
dx_oth_psycho_or_unspec_subs_depend_abuse,
bmi
FROM hiv_rpt.r21_output_table T1,
hiv_rpt.r21_masked_patients T2
WHERE T1.master_patient_id = T2.master_patient_id;

--CREATE INDEX
DROP INDEX IF EXISTS hiv_rpt.r21_output_table_masked_masked_patient_id_month_0_idx;
CREATE INDEX r21_output_table_masked_masked_patient_id_month_0_idx
  ON hiv_rpt.r21_output_table_masked
  USING btree
  (masked_patient_id, month_0);



\COPY (select * from hiv_rpt.r21_output_table_masked order by masked_patient_id, month_id ) TO '/tmp/2020-03-02-cha-r21-output.csv' WITH CSV HEADER;















