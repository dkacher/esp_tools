-- Hi Kathy and Heather –

-- Circling back on this.

-- Confirming that if we develop this using ESP episode criteria that it will de-duplicate repeat positive tests in <30 days. 

-- It sounds to me like we should have Commonwealth Informatics send you two things:

-- A linelist of all cases of chlamydia, gonorrhea, syphilis using ESP criteria between July 28, 2014 and July 27, 2016 with the name, dob, case type (chlamydia, gonorrhea, or syphilis), and case date
-- A linelist of all the unique individuals above with the total count of cases of chlamydia, gonorrhea, and syphilis individually and collectively between July 28, 2014 and July 27, 2016.
-- I’m presuming that Heather or someone else at MDPH will then cross-reference the second list with your existing list of all STI repeaters to determine a) if there are any repeaters on the MDPH list that appear on the Atrius list of patients but do not have total case counts of ≥2 on the Atrius list, and b) the accuracy of repeat case counts on the Atrius list versus the MDPH list.

-- Sound right?

-- Mike.


--stirpt_mdph

DROP TABLE IF EXISTS stirpt_mdph_index_pats;
DROP TABLE IF EXISTS stirpt_mdph_cond_total;
DROP TABLE IF EXISTS stirpt_mdph_output;


-- INDEX PATIENTS 
-- All individuals with at least one case of Chlamydia, Gonorrhea, Syphillis, (July 28, 2014 - July 27, 2016
CREATE TABLE stirpt_mdph_index_pats AS
SELECT T1.patient_id,
concat(T2.last_name, ', ',T2.first_name) as name,
T2.date_of_birth::date,
T1.date as case_date,
T1.condition
FROM nodis_case T1,
emr_patient T2
WHERE T1.patient_id = T2.id
AND T1.condition in ('chlamydia', 'gonorrhea', 'syphilis')
AND T1.date >= '07-28-2014'
AND T1.date <= '07-27-2016';

-- Count cases 
CREATE TABLE stirpt_mdph_cond_total AS
SELECT patient_id,
COUNT(CASE WHEN condition ='chlamydia' then 1 END ) chlam_cases_t,
COUNT(CASE WHEN condition ='gonorrhea' then 1 END ) gon_cases_t,
COUNT(CASE WHEN condition ='syphilis' then 1 END )  syph_cases_t,
COUNT(*) all_cases_t
FROM stirpt_mdph_index_pats
GROUP BY patient_id;

-- Prepare output 
-- Duplicate data for totals for each patient
CREATE TABLE stirpt_mdph_output AS
SELECT T1.patient_id,
T1.name,
T1.date_of_birth,
T1.condition, 
T1.case_date,
coalesce(T2.chlam_cases_t, 0) as chlam_cases_t,
coalesce(T2.gon_cases_t, 0) as gon_cases_t,
coalesce(T2.syph_cases_t, 0) as syph_cases_t,
coalesce(T2.all_cases_t, 0) as all_cases_t
FROM stirpt_mdph_index_pats T1,
stirpt_mdph_cond_total T2
WHERE T1.patient_id = T2.patient_id
ORDER BY T1.patient_id, case_date;


-- Print the output 3 different ways.

-- combo output
SELECT * FROM stirpt_mdph_output;

-- case lineline only
SELECT * FROM stirpt_mdph_index_pats ORDER BY patient_id, case_date;

-- Patient count totals only
SELECT T1.patient_id,
T2.name,
T2.date_of_birth,
max(coalesce(T1.chlam_cases_t, 0)) as chlam_cases_t,
max(coalesce(T1.gon_cases_t, 0)) as gon_cases_t,
max(coalesce(T1.syph_cases_t, 0)) as syph_cases_t,
max(coalesce(T1.all_cases_t, 0)) as all_cases_t
FROM stirpt_mdph_cond_total T1
LEFT JOIN stirpt_mdph_index_pats T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id, T2.name, T2.date_of_birth
ORDER BY patient_id;


