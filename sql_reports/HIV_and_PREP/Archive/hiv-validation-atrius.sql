﻿-- Analysis script generated on Tuesday, 17 May 2016 16:46:42 EDT
-- CCDA Build #1.0.2303 (1014) Updated: Wednesday January 6, 2016 at 8:00am POSTGRESQL
-- Analysis name: HIV Validation
-- Analysis description: HIV Validation
-- Script generated for database: POSTGRESQL

--
-- Script setup section 
--
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_diag_codes CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_first_diag_and_count CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hef_labs CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_elisa_ag_ab_labs_all CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_pos_viral_loads CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_max_viral_load CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_elisa_wb_agab CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_rx CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_rx_combo_distinct CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_rx_final CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_truvada CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_test_365_since_truvada CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_truv_rx_30_after_neg_elisa_agab CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_rx_3_diff CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_mp_list CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_mp_details CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_vanilla_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_1day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_30day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_60day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_90day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_120day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_180day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_180day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_1day_w400_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_30day_w400_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_60day_w400_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_90day_w400_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_120day_w400_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_180day_w400_cases CASCADE;


DROP TABLE IF EXISTS kre_hiv_temp_a_hepb_testing CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_validtion_ll CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_validtion_ll_final CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_problem_list CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hepb_diags CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hepb_diags_2 CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hepb_dna_labs_all CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hepb_test_365_since_truvada CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_truvada_only_on_rx_date CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_may18_report CASCADE;

--
-- Script body 
--


-- Query - HIV Diagnosis Codes
CREATE TABLE kre_hiv_temp_a_hiv_diag_codes  WITHOUT OIDS  AS 
SELECT T1.id,T1.encounter_id,T1.dx_code_id 
FROM  emr_encounter_dx_codes T1   
WHERE  dx_code_id ~ '^(icd9:042)' or dx_code_id ~ '^(icd9:V08)' or dx_code_id ~ '^(icd10:B20)' or  dx_code_id ~ '^(icd10:B21)'
or  dx_code_id ~ '^(icd10:B22)'  or  dx_code_id ~ '^(icd10:B23)' or dx_code_id ~ '^(icd10:B24)' 
or dx_code_id ~ '^(icd10:B97.35)' or dx_code_id ~ '^(icd10:Z21)' ;


-- Query - Get Date of First HIV Diagnosis & Count of Encounters With HIV Diagnosis Codes
CREATE TABLE kre_hiv_temp_a_first_diag_and_count  WITHOUT OIDS  AS 
SELECT T2.patient_id,min(date) hiv_first_diag_date,1::int hiv_diag, count(distinct(encounter_id)) as hiv_enc_diag_count 
FROM kre_hiv_temp_a_hiv_diag_codes T1 
INNER JOIN emr_encounter T2 
ON ((T1.encounter_id = T2.id))  
GROUP BY T2.patient_id;

-- Query - HIV on the Problem List
CREATE TABLE kre_hiv_temp_a_problem_list  WITHOUT OIDS  AS 
SELECT DISTINCT ON(patient_id) T1.id,T1.patient_id,T2.first_date hiv_first_prob_date, T1.dx_code_id, 1::int hiv_prob 
FROM  emr_problem T1,
	(SELECT patient_id, min(date) as first_date from emr_problem WHERE  dx_code_id ~ '^(icd9:042)' or dx_code_id ~ '^(icd9:V08)' or dx_code_id ~ '^(icd10:B20)' or  dx_code_id ~ '^(icd10:B21)'
	or  dx_code_id ~ '^(icd10:B22)'  or  dx_code_id ~ '^(icd10:B23)' or dx_code_id ~ '^(icd10:B24)' 
	or dx_code_id ~ '^(icd10:B97.35)' or dx_code_id ~ '^(icd10:Z21)' group by patient_id) T2
WHERE  (dx_code_id ~ '^(icd9:042)' or dx_code_id ~ '^(icd9:V08)' or dx_code_id ~ '^(icd10:B20)' or  dx_code_id ~ '^(icd10:B21)'
or  dx_code_id ~ '^(icd10:B22)'  or  dx_code_id ~ '^(icd10:B23)' or dx_code_id ~ '^(icd10:B24)' 
or dx_code_id ~ '^(icd10:B97.35)' or dx_code_id ~ '^(icd10:Z21)')
AND T1.date = T2.first_date
AND T1.patient_id = T2.patient_id ;


-- Query - HIV Hef Positive Lab Events
CREATE TABLE kre_hiv_temp_a_hef_labs  WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.name,T1.date,T1.object_id 
FROM  hef_event T1   
WHERE name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_rna_viral:threshold:gt:0', 'lx:hiv_rna_viral:positive');

-- Query - HIV Hef All Elisa and AG/AB Lab Events
CREATE TABLE kre_hiv_temp_a_elisa_ag_ab_labs_all  WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.name,T1.date,T1.object_id 
FROM  hef_event T1   
WHERE name ilike 'lx:hiv_elisa:%'
OR NAME ILIKE  'lx:hiv_ag_ab:%';


-- Query - HIV Positive Viral Load Lab Results
CREATE TABLE kre_hiv_temp_a_hiv_pos_viral_loads  WITHOUT OIDS  AS 
SELECT T1.patient_id,T2.result_float,T1.date, T2.ref_unit
FROM kre_hiv_temp_a_hef_labs T1 
INNER JOIN emr_labresult T2 ON ((T1.object_id = T2.id) AND (T1.patient_id = T2.patient_id))  
WHERE  T1.name =  'lx:hiv_rna_viral:threshold:gt:0' ;

-- SQL - Max Viral Load, Date, & Units
CREATE TABLE kre_hiv_temp_a_max_viral_load  WITHOUT OIDS  AS 
SELECT T1.patient_id, date as max_viral_load_date, max_viral_load, ref_unit as max_viral_load_units
FROM kre_hiv_temp_a_hiv_pos_viral_loads T1
JOIN 
(
   SELECT patient_id, MAX(result_float) AS max_viral_load
   FROM kre_hiv_temp_a_hiv_pos_viral_loads T1
   GROUP BY patient_id
) T2
ON T1.patient_id = T2.patient_id
AND t1.result_float = T2.max_viral_load;

-- Query - First ELISA, WB, & AG_AB Dates
CREATE TABLE kre_hiv_temp_a_elisa_wb_agab  WITHOUT OIDS  AS 
SELECT T1.patient_id,
max(case when name = 'lx:hiv_elisa:positive' then 1 else 0 end) pos_elisa,
min(case when name = 'lx:hiv_elisa:positive' then date else null end) first_elisa_pos_date,
max(case when name = 'lx:hiv_wb:positive' then 1 else 0 end)  pos_wb,
min(case when name = 'lx:hiv_wb:positive' then date else null end)  first_wb_pos_date,
min(case when name = 'lx:hiv_ag_ab:positive' then date else null end) first_ag_ab_pos_date,
max(case when name = 'lx:hiv_ag_ab:positive' then 1 else 0 end) pos_ag_ab 
FROM  kre_hiv_temp_a_hef_labs T1   GROUP BY T1.patient_id;

-- Query - HIV Prescriptions
CREATE TABLE kre_hiv_temp_a_hiv_rx  WITHOUT OIDS  AS 
SELECT T1.patient_id,
T1.name,
split_part(T1.name, ':', 2) stripped_name,
T1.date,
case when T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end truvada_rx,
case when T1.name not in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end other_hiv_rx_non_truvada,
CASE WHEN refills~E'^\\d+$' THEN (refills::real +1)  ELSE 1 END  refills_mod,
1::int hiv_meds,
string_to_array(replace(T1.name, 'rx:hiv_',''), '-') test4,
string_to_array(replace(split_part(T1.name, ':', 2), 'hiv_',''), '-')::text test5,
string_to_array(replace(split_part(T1.name, ':', 2), 'hiv_',''), '-') med_array,
quantity,
quantity_float,
quantity_type 
FROM hef_event T1 
INNER JOIN emr_prescription T2 
ON ((T1.object_id = T2.id))  
WHERE T1.name ilike 'rx:hiv_%'; 

-- SQL - Break down the combo meds in to distinct individual meds.
CREATE TABLE kre_hiv_temp_a_hiv_rx_combo_distinct  WITHOUT OIDS  AS 
select distinct unnest(med_array) as distinct_med , 
patient_id 
from kre_hiv_temp_a_hiv_rx 
group by patient_id, med_array;

-- Query - HIV Rx Values For Table Population
CREATE TABLE kre_hiv_temp_a_hiv_rx_final  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.truvada_rx) truvada_rx,
max(T1.other_hiv_rx_non_truvada) other_hiv_rx_non_truvada,
max(T1.hiv_meds) hiv_meds 
FROM  kre_hiv_temp_a_hiv_rx T1   
GROUP BY T1.patient_id;

-- Query - Number of Truvada Prescriptions & First Truvada Rx Date & Total Doses
CREATE TABLE kre_hiv_temp_a_truvada  WITHOUT OIDS  AS 
SELECT T1.patient_id,
sum( refills_mod ) num_of_truvada_rx,
min(date) as first_truvada_rx_date,
sum(case when quantity_type in ('tab', 'tabs', '', 'tablet') or quantity_type is null then (quantity_float * refills_mod) else 0 end) truvada_doses,
count(CASE WHEN (quantity_type in ('tab', 'tabs', '', 'tablet') or quantity_type is null) and quantity_float >= 60 then 1 END) truvada_gte60_pills_in_rx
FROM  kre_hiv_temp_a_hiv_rx T1   
WHERE  truvada_rx = 1 
GROUP BY T1.patient_id;

--Rx for truvada within 30 days AFTER a negative HIV ELISA or Ab/Ag test
CREATE TABLE kre_hiv_temp_a_truv_rx_30_after_neg_elisa_agab  WITHOUT OIDS  AS 
select T1.patient_id, 1::INT truv_rx_30_after_neg_elisa_agab
FROM  kre_hiv_temp_a_hiv_rx T1,  kre_hiv_temp_a_elisa_ag_ab_labs_all T2  
WHERE  truvada_rx = 1
AND T1.patient_id = T2.patient_id
AND T2.name IN ('lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative')
AND T1.date <= T2.date + 30 
AND T1.date >= T2.date
group by T1.patient_id;

--Count of HIV tests (ELISA or Ab/Ag, any result) in the 365 days following first prescription for truvada 
CREATE TABLE kre_hiv_temp_a_hiv_test_365_since_truvada  WITHOUT OIDS  AS
SELECT T1.patient_id, count(*) hiv_test_count_365_since_truvada
FROM kre_hiv_temp_a_elisa_ag_ab_labs_all T1, kre_hiv_temp_a_truvada T2
WHERE (name ilike 'lx:hiv_elisa:%'
OR NAME ILIKE  'lx:hiv_ag_ab:%')
and T1.date <= T2.first_truvada_rx_date + 365
AND T1.date >= T2.first_truvada_rx_date
AND T1.patient_id = T2.patient_id
GROUP BY T1.patient_id;

-- Find patients where Truvada Rx was the only HIV drug ordered each time they received an Rx for Truvada
CREATE TABLE kre_hiv_temp_a_truvada_only_on_rx_date  WITHOUT OIDS  AS
SELECT patient_id, max_truvada, max_non, 1 as only_truvada_on_all_rx_dates FROM
	(SELECT patient_id, max(num_truvada) max_truvada, max(num_non_truvada) as max_non FROM
		(SELECT patient_id, date, count(*), sum(truvada_rx) num_truvada, sum(other_hiv_rx_non_truvada) num_non_truvada
		FROM kre_hiv_temp_a_hiv_rx GROUP BY patient_id, date) Ts1
	WHERE num_truvada >= 1 GROUP BY patient_id) T1
WHERE max_non = 0
ORDER BY patient_id;

-- Query - Rx for ≥3 different HIV meds 
CREATE TABLE kre_hiv_temp_a_rx_3_diff  WITHOUT OIDS  AS 
SELECT T1.patient_id,
case when count(distinct(distinct_med)) >= 3 then 1 else 0 end diff_hiv_med_count 
FROM  kre_hiv_temp_a_hiv_rx_combo_distinct T1   
GROUP BY T1.patient_id;


-- Union - MASTER PATIENT LIST Union together all the patient ids that have had HIV Diagnosis, HIV Meds, or Positive HIV Lab Test
CREATE TABLE kre_hiv_temp_a_mp_list  WITHOUT OIDS  AS 
(SELECT patient_id FROM kre_hiv_temp_a_first_diag_and_count) 
UNION (SELECT patient_id FROM kre_hiv_temp_a_hef_labs) 
UNION (SELECT patient_id FROM kre_hiv_temp_a_hiv_rx);

-- Query - MASTER PATIENT DETAILS
CREATE TABLE kre_hiv_temp_a_mp_details  WITHOUT OIDS  AS 
SELECT T1.patient_id,
T2.mrn,
T2.last_name,
T2.first_name,
T2.date_of_birth::date,
T2.gender,
T2.race 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN emr_patient T2 
ON ((T1.patient_id = T2.id)) ;

-- Query - HIV Cases - Vanilla 
CREATE TABLE kre_hiv_temp_a_vanilla_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_esp,
date hiv_per_esp_date 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_baseline_rxonly_30 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';

-- Query - HIV Cases - 1 day - SCNDWINDOWSTRTMAX=180
CREATE TABLE kre_hiv_temp_a_1day_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_1_day 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_nodis_rxonly_1 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';

-- Query - HIV Cases - 30 days - SCNDWINDOWSTRTMAX=180
CREATE TABLE kre_hiv_temp_a_30day_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_30_days 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_nodis_rxonly_30 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';

-- Query - HIV Cases - 60 days - SCNDWINDOWSTRTMAX=180
CREATE TABLE kre_hiv_temp_a_60day_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_60_days 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_nodis_rxonly_60 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';

-- Query - HIV Cases - 90 days - SCNDWINDOWSTRTMAX=180
CREATE TABLE kre_hiv_temp_a_90day_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_90_days 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_nodis_rxonly_90 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';

-- Query - HIV Cases - 120 days - SCNDWINDOWSTRTMAX=365
CREATE TABLE kre_hiv_temp_a_120day_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_120_days 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_nodis_rxonly_120 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';

-- Query - HIV Cases - 180 days - SCNDWINDOWSTRTMAX=365
CREATE TABLE kre_hiv_temp_a_180day_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_180_days 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_nodis_rxonly_180 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';


-- Query - HIV Cases - 1 day - SCNDWINDOWSTRTMAX=400
CREATE TABLE kre_hiv_temp_a_1day_w400_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_1_day_w400 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_nodis_rxonly_1_wmax400 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';


-- Query - HIV Cases - 30 days - SCNDWINDOWSTRTMAX=400
CREATE TABLE kre_hiv_temp_a_30day_w400_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_30_days_w400 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_nodis_rxonly_30_wmax400 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';

-- Query - HIV Cases - 60 days - SCNDWINDOWSTRTMAX=400
CREATE TABLE kre_hiv_temp_a_60day_w400_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_60_days_w400 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN  temp_hiv_nodis_rxonly_60_wmax400 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';


-- Query - HIV Cases - 90 days - SCNDWINDOWSTRTMAX=400
CREATE TABLE kre_hiv_temp_a_90day_w400_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_90_days_w400 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_nodis_rxonly_90_wmax400 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';

-- Query - HIV Cases - 120 days - SCNDWINDOWSTRTMAX=400
CREATE TABLE kre_hiv_temp_a_120day_w400_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_120_days_w400 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_nodis_rxonly_120_wmax400 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';

-- Query - HIV Cases - 180 days - SCNDWINDOWSTRTMAX=400
CREATE TABLE kre_hiv_temp_a_180day_w400_cases  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hiv_per_180_days_w400 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN temp_hiv_nodis_rxonly_180_wmax400 T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.condition = 'hiv';

-- Query -- Appeared on May 18th version of the report
CREATE TABLE kre_hiv_temp_a_may18_report AS
SELECT T1.esp_patient_id patient_id, 1::int on_may18_report
FROM hiv_validation_20160518 T1;

-- Query - HepB Testing
CREATE TABLE kre_hiv_temp_a_hepb_testing  WITHOUT OIDS  AS 
SELECT T1.patient_id,
1::int hist_of_hep_b 
FROM kre_hiv_temp_a_mp_list T1 
INNER JOIN hef_event T2 
ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.name in ('lx:hepatitis_b_surface_antigen:positive',
'lx:hepatitis_b_viral_dna:positive')
 GROUP BY T1.patient_id;
 
-- Query - HBV DNA tests (any result)
CREATE TABLE kre_hiv_temp_a_hepb_dna_labs_all  WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.name,T1.date,T1.object_id 
FROM  hef_event T1   
WHERE name ilike 'lx:hepatitis_b_viral_dna:%';


--Count of HBV DNA tests in the 365 days following first prescription for truvada 
CREATE TABLE kre_hiv_temp_a_hepb_test_365_since_truvada  WITHOUT OIDS  AS
SELECT T1.patient_id, count(*) hepb_dna_test_count_365_since_truvada
FROM kre_hiv_temp_a_hepb_dna_labs_all T1, kre_hiv_temp_a_truvada T2
WHERE name ilike 'lx:hepatitis_b_viral_dna:%'
and T1.date <= T2.first_truvada_rx_date + 365
AND T1.date > T2.first_truvada_rx_date
AND T1.patient_id = T2.patient_id
GROUP BY T1.patient_id;
 


-- Query - HepB Diagnosis Codes
CREATE UNLOGGED TABLE kre_hiv_temp_a_hepb_diags  WITHOUT OIDS  AS 
SELECT T1.id, T1.encounter_id,T1.dx_code_id 
FROM  emr_encounter_dx_codes T1
WHERE  
(dx_code_id ~ '^(icd9:070.2)' or dx_code_id ~ '^(icd9:070.3)' or dx_code_id ~ '^(icd10:B18.0)'
or  dx_code_id ~ '^(icd10:B18.1)'  or  dx_code_id ~ '^(icd10:B19.1)');

-- Query -- HepB Diagnosis Codes - Faster as 2 queries
CREATE TABLE kre_hiv_temp_a_hepb_diags_2  WITHOUT OIDS  AS
select patient_id, 1::int hist_of_hep_b
from kre_hiv_temp_a_hepb_diags T1,
emr_encounter T2
where T1.encounter_id = T2.id
group by patient_id;

-- SQL - Bring it all together
CREATE TABLE kre_hiv_temp_a_hiv_validtion_ll  WITHOUT OIDS  AS 
SELECT coalesce(mp.patient_id, hiv_esp.patient_id, mvl.patient_id, labs.patient_id, diag.patient_id, meds.patient_id, truv_count.patient_id, diff_med.patient_id, hep_b.patient_id ) as esp_patient_id ,
mp.mrn,
mp.last_name,
mp.first_name,
mp.date_of_birth,
mp.gender,
mp.race,
coalesce(hiv_per_esp, 0) as hiv_per_esp,
coalesce(hiv_per_esp_date, null) as hiv_per_esp_date, 
coalesce(mvl.max_viral_load, null) as max_viral_load,
COALESCE(mvl.max_viral_load_units, NULL) AS max_viral_load_units,
coalesce(mvl.max_viral_load_date, null) as max_viral_load_date,
coalesce(labs.pos_wb, 0) as pos_wb,
coalesce(labs.first_wb_pos_date, null) as first_pos_wb_date,
coalesce(labs.pos_elisa, 0) as pos_elisa,
coalesce(labs.first_elisa_pos_date, null) as first_elisa_pos_date,
coalesce(labs.pos_ag_ab, 0) as pos_ag_ab,
coalesce(labs.first_ag_ab_pos_date, null) as first_ag_ab_pos_date,
coalesce(diag.hiv_diag, 0) as hiv_diag,
COALESCE(diag.hiv_enc_diag_count, 0) AS hiv_enc_diag_count,
coalesce(hiv_first_diag_date, null) as hiv_first_diag_date,
COALESCE(problem.hiv_prob, 0) as hiv_prob,
COALESCE(hiv_first_prob_date, NULL) AS hiv_first_prob_date, 
coalesce(hiv_meds, 0) as hiv_meds,
coalesce(truvada_rx, 0) as truvada_rx,
coalesce(other_hiv_rx_non_truvada, 0) as other_hiv_rx_non_truvada,
coalesce(num_of_truvada_rx, 0) as num_of_truvada_rx,
coalesce(truvada_doses, 0) as truvada_doses,
coalesce(truv_rx_30_after_neg_elisa_agab, 0) as truv_rx_30_after_neg_elisa_agab,
coalesce(only_truvada_on_all_rx_dates, 0) as only_truvada_on_all_rx_dates,
coalesce(truvada_gte60_pills_in_rx, 0) as truvada_gte60_pills_in_rx,
coalesce(hiv_test_count_365_since_truvada, 0) as hiv_test_count_365_since_truvada,
coalesce(hepb_dna_test_count_365_since_truvada, 0) as hepb_dna_test_count_365_since_truvada,
coalesce(diff_hiv_med_count, 0) as rx_3diff_hiv_meds,
coalesce(hep_b.hist_of_hep_b, hep_b_diag.hist_of_hep_b, 0) as hist_of_heb_b,
coalesce(on_may18_report, 0) as on_may18_report
FROM kre_hiv_temp_a_mp_details mp
LEFT OUTER JOIN kre_hiv_temp_a_vanilla_cases hiv_esp
ON hiv_esp.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_max_viral_load mvl
ON mvl.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_elisa_wb_agab labs
ON labs.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_first_diag_and_count diag
ON diag.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_hiv_rx_final meds
ON meds.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_truvada truv_count
ON truv_count.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_rx_3_diff diff_med
ON diff_med.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_hepb_testing hep_b
ON hep_b.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_problem_list problem
ON problem.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_hepb_diags_2 hep_b_diag
ON hep_b_diag.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_hiv_test_365_since_truvada tests_since_truv
ON tests_since_truv.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_hepb_test_365_since_truvada hebp_tests_since_truv
ON hebp_tests_since_truv.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_truv_rx_30_after_neg_elisa_agab truv_rx_30_eagab
ON truv_rx_30_eagab.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_truvada_only_on_rx_date truv_only
ON truv_only.patient_id = mp.patient_id
LEFT OUTER JOIN kre_hiv_temp_a_may18_report may_report
ON may_report.patient_id = mp.patient_id
;

-- SQL - Add in HIV Case Detection Variants
CREATE TABLE kre_hiv_temp_a_hiv_validtion_ll_final  WITHOUT OIDS  AS 
select T1.* ,
coalesce(hiv_per_1_day,0) as hiv_per_1_day,
coalesce(hiv_per_30_days,0) as hiv_per_30_days,
coalesce(hiv_per_60_days,0) as hiv_per_60_days,
coalesce(hiv_per_90_days,0) as hiv_per_90_days,
coalesce(hiv_per_1_day_w400,0) as hiv_per_1_day_w400,
coalesce(hiv_per_30_days_w400,0) as hiv_per_30_days_w400,
coalesce(hiv_per_60_days_w400,0) as hiv_per_60_days_w400,
coalesce(hiv_per_90_days_w400,0) as hiv_per_90_days_w400,
coalesce(hiv_per_120_days_w400,0) as hiv_per_120_days_w400,
coalesce(hiv_per_180_days_w400,0) as hiv_per_180_days_w400
from kre_hiv_temp_a_hiv_validtion_ll T1
FULL OUTER JOIN kre_hiv_temp_a_1day_cases day1
ON day1.patient_id = T1.esp_patient_id
FULL OUTER JOIN kre_hiv_temp_a_30day_cases day30
ON day30.patient_id = T1.esp_patient_id
FULL OUTER JOIN kre_hiv_temp_a_60day_cases day60
ON day60.patient_id = T1.esp_patient_id
FULL OUTER JOIN kre_hiv_temp_a_90day_cases day90
ON day90.patient_id = T1.esp_patient_id
FULL OUTER JOIN kre_hiv_temp_a_1day_w400_cases day1w400
ON day1w400.patient_id = T1.esp_patient_id
FULL OUTER JOIN kre_hiv_temp_a_30day_w400_cases day30w400
ON day30w400.patient_id = T1.esp_patient_id
FULL OUTER JOIN kre_hiv_temp_a_60day_w400_cases day60w400
ON day60w400.patient_id = T1.esp_patient_id
FULL OUTER JOIN kre_hiv_temp_a_90day_w400_cases day90w400
ON day90w400.patient_id = T1.esp_patient_id
FULL OUTER JOIN kre_hiv_temp_a_120day_w400_cases day120w400
ON day120w400.patient_id = T1.esp_patient_id
FULL OUTER JOIN kre_hiv_temp_a_180day_w400_cases day180w400
ON day180w400.patient_id = T1.esp_patient_id;

--
-- Script shutdown section 
--
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_diag_codes CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_first_diag_and_count CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hef_labs CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_elisa_ag_ab_labs_all CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_pos_viral_loads CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_max_viral_load CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_elisa_wb_agab CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_rx CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_rx_combo_distinct CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_rx_final CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_truvada CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_test_365_since_truvada CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_truv_rx_30_after_neg_elisa_agab CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_rx_3_diff CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_mp_list CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_mp_details CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_vanilla_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_1day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_30day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_60day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_90day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_120day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_180day_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_1day_w400_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_30day_w400_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_60day_w400_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_90day_w400_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_120day_w400_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_180day_w400_cases CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hepb_testing CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_validtion_ll CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_problem_list CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hepb_diags CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hepb_diags_2 CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hepb_dna_labs_all CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_hepb_test_365_since_truvada CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_truvada_only_on_rx_date CASCADE;
DROP TABLE IF EXISTS kre_hiv_temp_a_may18_report CASCADE;



--DROP TABLE IF EXISTS kre_hiv_temp_a_hiv_validtion_ll_final CASCADE;

-- DON'T FORGET TO FILTER OUT TEST PATIENT IDs

-- select * from kre_hiv_temp_a_hiv_validtion_ll_final
-- where esp_patient_id not in (
-- 4856539,
-- 2388755,
-- 20021289,
-- 2622639,
-- 7393799,
-- 7405902,
-- 19086,
-- 2245972,
-- 9345414,
-- 1121717,
-- 1120095,
-- 5092053,
-- 1119941,
-- 15810936,
-- 2801547,
-- 7395105,
-- 6808599,
-- 467766,
-- 465618,
-- 9345404,
-- 24120559,
-- 20257460,
-- 6840308,
-- 467872,
-- 4512833,
-- 454136,
-- 8429720,
-- 8000774,
-- 377556,
-- 456070,
-- 8012787,
-- 7441382,
-- 7997968,
-- 3268327,
-- 3268319,
-- 3268323,
-- 8726972,
-- 9645683,
-- 456988,
-- 7395153,
-- 4476409,
-- 1400172,
-- 269278,
-- 5044824,
-- 5092208,
-- 269284,
-- 269622,
-- 269290,
-- 5689686,
-- 5651871,
-- 269282,
-- 269276,
-- 5042048,
-- 6272750,
-- 299112,
-- 269286,
-- 269288,
-- 5044808,
-- 291608,
-- 2733840,
-- 269280,
-- 4476411,
-- 8425284,
-- 7394534,
-- 2733842,
-- 5092285,
-- 7394532,
-- 3906359,
-- 1629211,
-- 7394546,
-- 7394548,
-- 5638755,
-- 3069021,
-- 7394544,
-- 16697245,
-- 16697270,
-- 16700804,
-- 16803981)
