-- generate an Excel-based linelist on demand with one row per patient that includes 
-- name, 
-- MRN, 
-- age, 
-- risk score, 
-- PCP name, 
-- PCP’s site of practice, 
-- the value for each variable in the dataset, 
-- last creatinine value and date, 
-- last Chlamydia test date + result, 
-- last Gonorrhea test date + result, 
-- last HIV test date + result, 
-- and last syphilis test date + result, 


-- done done # sex	-10.01112 if male, -12.4031 if female	1.00000000
-- done done # race_black	1 if race is recorded as black, 0 otherwise	1.06186066
-- done done # race_caucasian	1 if race is recorded as caucasian, 0 otherwise	-0.6609999
-- done done # english_lang	1 if English is primary language, 0 otherwise	-0.4213635
-- done done # has_language_data	1 if language is recorded, 0 if missing	-0.0784053
-- done done # years_of_data	length of available EHR history in years	-0.0660395
-- done done # t_hiv_rna_1_year	number of HIV RNA tests in the previous year	0.14676104
-- done done # rx_bicillin_1_year	number of prescriptions for bicillin in previous year	1.35516827
-- done done # has_1yr_data	1 if EHR history exists for previous year, 0 otherwise	-0.6311702
-- done done # has_2yr_data	1 if EHR history exists for previous 2 years, 0 otherwise	-0.4035768
-- done done # t_pos_gon_test_2yr	total number of positive gonorrhea tests in past 2 years	3.07004984
-- done done # t_hiv_test_2yr	total number of HIV tests in past 2 years (regardless of test result)	0.23349732
-- done done # t_hiv_test_2yr	total number of HIV tests in past 2 years (regardless of test result)	0.23349732
-- done done # rx_bicillin_2yr	total number of prescriptions for bicillin in past 2 years	0.20795003
-- done done # rx_suboxone_2yr	total number of prescriptions for suboxone in past 2 years	0.19721102
-- done done # t_chla_test_t_e	total number of chlamydia tests ever	-0.154315
-- done done # t_hiv_test_e		total number of HIV tests ever (ELISA or Ab/Ag or RNA)	0.12024552
-- done done # t_hiv_elisa_e	total number of HIV ELISA or Ab/Ag tests ever	0.16006917
-- done done # rx_bicillin_e	total number of prescriptions for bicillin ever	1.79771045
-- done # syphilis_any_site_state_x_late_e	1 if syphilis ever diagnosed, 0 otherwise	0.9997348
-- done # contact_w_or_expo_venereal_dis_e	1 if ever had contact or exposure to venereal disease, 0 otherwise (ICD9=V01.6 or ICD10=Z20.2 or Z20.6 ever)	0.28886449
-- done done # acute_hiv_test_2yr	1 if had an HIV RNA test in past 2 years, 0 otherwise	0.15925514
-- done done # acute_hiv_test_e	1 if had an HIV RNA test ever, 0 otherwise	1.82082955
-- done # hiv_counseling_2yr	1 if counseled for HIV in past 2 years, 0 otherwise (ICD9=V65.44 or ICD10=Z71.7)	1.09544347

DROP TABLE IF EXISTS hrp2016_coefficients;
DROP TABLE IF EXISTS hrp2016_index_patients;
DROP TABLE IF EXISTS hrp2016_pat_encs;
DROP TABLE IF EXISTS hrp2016_hiv_labs;
DROP TABLE IF EXISTS hrp2016_hiv_lab_values;
DROP TABLE IF EXISTS hrp2016_gon_chlam_events;
DROP TABLE IF EXISTS hrp2016_gon_chlam_values;
DROP TABLE IF EXISTS hrp2016_syph_events;
DROP TABLE IF EXISTS hrp2016_diags;
DROP TABLE IF EXISTS hrp2016_diags_values;
DROP TABLE IF EXISTS hrp2016_bicillin;
DROP TABLE IF EXISTS hrp2016_bicillin_values;
DROP TABLE IF EXISTS hrp2016_suboxone;
DROP TABLE IF EXISTS hrp2016_suboxone_values;
DROP TABLE IF EXISTS hrp2016_creatinine_most_recent;
DROP TABLE IF EXISTS hrp2016_chlam_gon_syph_max_dates;
DROP TABLE IF EXISTS hrp2016_chlam_most_recent;
DROP TABLE IF EXISTS hrp2016_gon_most_recent;
DROP TABLE IF EXISTS hrp2016_syph_max_dates;
DROP TABLE IF EXISTS hrp2016_syph_most_recent;
DROP TABLE IF EXISTS hrp2016_hiv_max_dates;
DROP TABLE IF EXISTS hrp2016_hiv_most_recent;
DROP TABLE IF EXISTS hrp2016_most_recent_weight;
DROP TABLE IF EXISTS hrp2016_pat_sum_x_value;
DROP TABLE IF EXISTS hrp2016_index_data;
DROP TABLE IF EXISTS hrp2016_risk_scores;

DROP TABLE IF EXISTS hrp2016_data_output;


CREATE TABLE hrp2016_coefficients AS
SELECT 1.00000000::real sex,
1.06186066::real race_black,
-0.6609999::real race_caucasian,
-0.4213635::real english_lang,
-0.0784053::real has_language_data,
-0.0660395::real years_of_data,
-0.6311702::real has_1yr_data,
-0.4035768::real has_2yr_data,
0.14676104::real t_hiv_rna_1_yr,
0.23349732::real t_hiv_test_2yr,
0.12024552::real t_hiv_test_e,
0.16006917::real t_hiv_elisa_e,
1.82082955::real acute_hiv_test_e,
0.15925514::real acute_hiv_test_2yr,
3.07004984::real t_pos_gon_test_2yr,
-0.154315::real t_chla_test_t_e,
1.35516827::real rx_bicillin_1_yr,
0.20795003::real rx_bicillin_2yr,
1.79771045::real rx_bicillin_e,
0.19721102::real rx_suboxone_2yr,
0.9997348::real syphilis_any_site_state_x_late_e,
0.28886449::real contact_w_or_expo_venereal_dis_e,
1.09544347::real hiv_counseling_2yr;


-- CREATE TABLE hrp2016_index_patients AS
-- SELECT T1.id as patient_id,
-- concat(T1.last_name, ', ', T1.first_name) as name,
-- mrn,
-- date_part('year',age(date_of_birth)) current_age,
-- concat(T2.last_name, ', ', T2.first_name, ' ', T2.title) as pcp_name,
-- T2.dept as pcp_site,
-- gender as sex_raw,
-- CASE WHEN gender = 'F' then -12.4031
     -- WHEN gender = 'M' then -10.01112
     -- END sex,
-- race as race_raw,
-- CASE WHEN race = 'BLACK' then 1 ELSE 0 END race_black,
-- CASE WHEN race = 'CAUCASIAN' then 1 ELSE 0 END race_caucasian,
-- home_language as home_language_raw,
-- CASE WHEN home_language = 'ENGLISH' then 1 ELSE 0 END english_lang,
-- CASE WHEN home_language not in ('DECLINED', 'UNKNOWN', '') and home_language is not null then 1 else 0 END has_language_data
-- FROM emr_patient T1
-- LEFT JOIN emr_provider T2 on (T1.pcp_id = T2.id)
-- WHERE gender in ('M', 'F')
-- -- must have an encounter in the past 2 years
-- AND EXISTS
    -- (SELECT 1
     -- FROM gen_pop_tools.clin_enc T3
     -- WHERE T3.patient_id = T1.id
     -- AND T3.date >= (now()::date - interval '2 years'))
-- -- must be at least age 15
-- AND date_part('year',age(date_of_birth)) >= 15
-- -- don't compute for known hiv patients
-- AND T1.id NOT IN (select patient_id from nodis_case where condition = 'hiv')
-- -- filter out test patients
-- AND T1.last_name not in ('TEST', 'TEST**')
-- AND T1.last_name not ilike '%ssmctest%' 
-- AND T1.last_name not ilike '% test%' 
-- AND T1.last_name not ilike 'XB%' 
-- AND T1.last_name not ilike 'XX%';

CREATE TABLE hrp2016_index_patients AS
SELECT T1.id as patient_id,
concat(T1.last_name, ', ', T1.first_name) as name,
T1.mrn,
date_part('year',age(date_of_birth)) current_age,
concat(T2.last_name, ', ', T2.first_name, ' ', T2.title) as pcp_name,
T2.dept as pcp_site,
gender as sex_raw,
CASE WHEN gender = 'F' then -12.4031
     WHEN gender = 'M' then -10.01112
     END sex,
race as race_raw,
CASE WHEN race = 'BLACK' then 1 ELSE 0 END race_black,
CASE WHEN race = 'CAUCASIAN' then 1 ELSE 0 END race_caucasian,
home_language as home_language_raw,
CASE WHEN home_language = 'ENGLISH' then 1 ELSE 0 END english_lang,
CASE WHEN home_language not in ('DECLINED', 'UNKNOWN', '') and home_language is not null then 1 else 0 END has_language_data
FROM emr_patient T1
LEFT JOIN emr_provider T2 on (T1.pcp_id = T2.id)
JOIN emr_encounter T3 on (T1.id = T3.patient_id)
LEFT JOIN static_enc_type_lookup T4 on (T3.raw_encounter_type = T4.raw_encounter_type)
WHERE gender in ('M', 'F')
-- must have an ambulatory encounter in the past 2 years
AND T3.date >= ('2016-12-31'::date - interval '2 years')
-- must have an ambulatory encounter before the "run" date
AND T3.date <= '2016-12-31'
AND T4.ambulatory = 1
-- must be at least age 15
AND date_part('year', age('2016-12-31'::date, date_of_birth)) >= 15
-- don't compute for known hiv patients
AND T1.id NOT IN (select patient_id from nodis_case where condition = 'hiv' and date <= '2016-12-31')
-- filter out test patients
AND T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%'
GROUP BY T1.id, name, T1.mrn, current_age, pcp_name, pcp_site, sex_raw, sex, race_raw, race_black, race_caucasian, home_language_raw, english_lang, has_language_data;


-- years_of_data 
-- length of available EHR history in years (based on encounter of specific type)
-- # has_1yr_data	1 if EHR history exists for previous year, 0 otherwise	-0.6311702
-- # has_2yr_data	1 if EHR history exists for previous 2 years, 0 otherwise	-0.4035768
-- CREATE TABLE hrp2016_pat_encs AS 
-- SELECT patient_id, (max(abs(date_part('year', age(date, now()::date))) ) + 1) years_of_data,
-- max(CASE WHEN abs(date_part('year', age(date, now()::date))) = 0 then 1 END) has_1yr_data,
-- max(CASE WHEN abs(date_part('year', age(date, now()::date))) = 1 then 1 END) has_2yr_data
-- FROM emr_encounter 
-- WHERE date >= '01-01-2006' 
-- AND raw_encounter_type in ('CONSULTATION', 'DIALYSIS', 'ED ADMISSION', 'EMERGENCY RO', 'ENDO', 'EXTENDED CAR', 'HOME VISIT', 'HOSPITAL', 'HOSPITAL ADM', 'HOSPITAL OB', 'INPATIENT -', 'INPT ROUND', 'MTM OFFICE', 'OB VISIT', 'OUTPT DEPT', 'POST DISCHAR', 'PRE-OP', 'PROCEDURE', 'REHAB', 'REG', 'SHARED PP', 'SHARED RETUR', 'SMAG OFFICE', 'SMA OFFICE', 'URGENT CARE', 'VISIT')
-- GROUP BY patient_id;

CREATE TABLE hrp2016_pat_encs AS 
SELECT patient_id, (max(abs(date_part('year', age(date, '2016-12-31'::date))) ) + 1) years_of_data,
max(CASE WHEN abs(date_part('year', age(date, '2016-12-31'::date))) = 0 then 1 END) has_1yr_data,
max(CASE WHEN abs(date_part('year', age(date, '2016-12-31'::date))) = 1 then 1 END) has_2yr_data
FROM emr_encounter T1,
static_enc_type_lookup T2
WHERE T1.raw_encounter_type = T2.raw_encounter_type
AND T2.ambulatory = 1
AND date >= '01-01-2006' 
AND date <= '2016-12-31'
GROUP BY patient_id;

-- Gather up all of the HIV lab tests
-- (ELISA or Ab/Ag or RNA) 
-- NEED TO LIMIT TO THESE TEST TYPES TO ELISA, HIV_RNA_VIRAL, HIV_AG_AB
-- NEED TO GRAB ALL TESTS NOT JUST THOSE WITH A HEF EVENT OR ELSE YOU WILL MISS VIRAL LOAD TESTS
CREATE TABLE hrp2016_hiv_labs AS 
SELECT T1.patient_id, T2.test_name, EXTRACT(YEAR FROM T1.date) rpt_year, abs(date_part('year', age(T1.date, '2016-12-31'::date))) yrs_since_test, T1.date, result_string, name as hef_name, concat(test_name, ':' , result_string) as result_data
FROM emr_labresult T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
LEFT JOIN hef_event T3 ON (T1.id = T3.object_id and T1.patient_id = T3.patient_id)
WHERE test_name in ('hiv_elisa', 'hiv_rna_viral', 'hiv_ag_ab')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined')
AND T1.date >= '01-01-2006' 
AND T1.date <= '2016-12-31'
AND result_string is not null;

-- Compute HIV test values
-- number of HIV RNA tests in the previous year
-- total number of HIV tests in past 2 years (regardless of test result)
-- total number of HIV tests ever (ELISA or Ab/Ag or RNA)
-- total number of HIV ELISA or Ab/Ag tests ever
-- 1 if had an HIV RNA test in past 2 years, 0 otherwise
-- 1 if had an HIV RNA test ever, 0 otherwise

CREATE TABLE hrp2016_hiv_lab_values AS
SELECT
patient_id,
count(CASE WHEN yrs_since_test = 0 AND test_name = 'hiv_rna_viral' THEN 1 END) t_hiv_rna_1_yr,
count(CASE WHEN yrs_since_test <= 1 THEN 1 END) t_hiv_test_2yr,
count(*) t_hiv_test_e,
count(CASE WHEN test_name in ('hiv_elisa', 'hiv_ag_ab') THEN 1 END) t_hiv_elisa_e,
max(CASE WHEN yrs_since_test <= 1 AND test_name = 'hiv_rna_viral' THEN 1 END) acute_hiv_test_2yr,
max(CASE WHEN test_name = 'hiv_rna_viral' THEN 1 END) acute_hiv_test_e
FROM hrp2016_hiv_labs
GROUP BY patient_id;

-- Gather up labs hef events for chlamydia and gonorrhea 
CREATE TABLE hrp2016_gon_chlam_events AS 
SELECT T1.id,T2.test_name as condition, T3.name as hef_name,T1.patient_id,T1.date, abs(date_part('year', age(T1.date, '2016-12-31'::date))) yrs_since_test, result_string
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
INNER JOIN hef_event T3 ON ((T1.id = T3.object_id) AND (T1.patient_id = T3.patient_id))
WHERE T1.date >= '01-01-2006'
AND T1.date <= '2016-12-31'
AND T2.test_name in ('chlamydia', 'gonorrhea');


-- Gather up all syphilis related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
CREATE TABLE hrp2016_syph_events AS 
SELECT T1.id,T2.test_name as condition, T3.name as hef_name,T1.patient_id,T1.date, abs(date_part('year', age(T1.date, '2016-12-31'::date))) yrs_since_test, result_string, concat(test_name, ':' , result_string) as result_data
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
LEFT JOIN hef_event T3 ON ((T1.id = T3.object_id) AND (T1.patient_id = T3.patient_id))
WHERE T1.date >= '01-01-2006'
AND T1.date <= '2016-12-31'
AND T2.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm')
AND result_string not in ('','Not Done','Not performed', 'Not tested', 'Not Tested', 'TNP', 'test')
and result_string is not null;


-- Compute Chlamydia and Gonorrhea values
-- # t_pos_gon_test_2yr	total number of positive gonorrhea tests in past 2 years
-- # t_chla_test_t_e	total number of chlamydia tests ever

CREATE TABLE hrp2016_gon_chlam_values AS
SELECT 
patient_id,
count(CASE WHEN hef_name = 'lx:gonorrhea:positive' AND yrs_since_test <= 1 THEN 1 END) t_pos_gon_test_2yr,
count(CASE WHEN condition = 'chlamydia' THEN 1 END) t_chla_test_t_e
FROM hrp2016_gon_chlam_events
GROUP BY patient_id;

-- Gather up diagnosis codes of interest
-- # syphilis_any_site_state_x_late_e	1 if syphilis ever diagnosed, 0 otherwise (icd9:091.*, icd9:092.*, icd9:093.*, icd9:094.*, icd9:095.*, icd10:A51.*, icd9:097.9, icd10:A52.0, icd10:A52.7) 
-- # contact_w_or_expo_venereal_dis_e	1 if ever had contact or exposure to venereal disease, 0 otherwise (ICD9=V01.6 or ICD10=Z20.2 or Z20.6 ever)	
-- # hiv_counseling_2yr	1 if counseled for HIV in past 2 years, 0 otherwise (ICD9=V65.44 or ICD10=Z71.7)
CREATE TABLE hrp2016_diags AS 
SELECT T2.patient_id, T1.id, T1.encounter_id, T1.dx_code_id, abs(date_part('year', age(T2.date, '2016-12-31'::date))) yrs_since_diag
FROM emr_encounter_dx_codes T1, 
emr_encounter T2
WHERE 
T1.encounter_id = T2.id
AND (dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.)' 
or dx_code_id in ('icd9:097.9', 'icd10:A52.0', 'icd10:A52.7', 'icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6', 'icd9:V65.44','icd10:Z71.7'))
AND T2.date <= '2016-12-31';

CREATE TABLE hrp2016_diags_values AS
SELECT T1.patient_id,
MAX(CASE WHEN dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.)' or dx_code_id in ('icd9:097.9', 'icd10:A52.0', 'icd10:A52.7') then 1 end) syphilis_any_site_state_x_late_e,
MAX(CASE WHEN dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') then  1 end) contact_w_or_expo_venereal_dis_e,
MAX(CASE WHEN dx_code_id in ('icd9:V65.44','icd10:Z71.7') and yrs_since_diag <=1  then 1 end) hiv_counseling_2yr
FROM hrp2016_diags T1
GROUP BY patient_id;


-- # rx_bicillin_1_year	number of prescriptions for bicillin in previous year
-- # rx_bicillin_2yr	total number of prescriptions for bicillin in past 2 years
-- # rx_bicillin_e	total number of prescriptions for bicillin ever	1.79771045
-- Only count 1 rx per date/ per name

CREATE TABLE hrp2016_bicillin AS
SELECT patient_id, name, date, abs(date_part('year', age(date, '2016-12-31'::date))) yrs_since_rx
FROM emr_prescription 
WHERE (
(name in (
'BICILLIN C-R 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
'BICILLIN C-R 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)',
'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)',
'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)',
'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE',
'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)'))
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE') and quantity_float = 2)
OR (name in ('BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE') and quantity_float = 4)
)
AND date <= '2016-12-31'
GROUP BY patient_id, name, date;


CREATE TABLE hrp2016_bicillin_values AS
SELECT T1.patient_id,
COUNT(CASE WHEN yrs_since_rx = 0 THEN 1 END) rx_bicillin_1_yr,
COUNT(CASE WHEN yrs_since_rx <= 1 THEN 1 END )rx_bicillin_2yr,
count(*) rx_bicillin_e
FROM hrp2016_bicillin T1
GROUP BY patient_id;

-- # rx_suboxone_2yr	total number of prescriptions for suboxone in past 2 years

CREATE TABLE hrp2016_suboxone AS
SELECT patient_id, name, date, abs(date_part('year', age(date, '2016-12-31'::date))) yrs_since_rx
FROM emr_prescription T1
WHERE (T1.name ilike '%suboxone%' 
or T1.name ilike 'buprenorphine%naloxone%' 
or T1.name ilike '%zubsolv%')
AND date <= '2016-12-31'
GROUP BY patient_id, name, date;

CREATE TABLE hrp2016_suboxone_values AS
SELECT T1.patient_id,
COUNT(CASE WHEN yrs_since_rx <= 1 THEN 1 END )rx_suboxone_2yr
FROM hrp2016_suboxone T1
GROUP BY patient_id;


-- Gather up all of creatine tests and last value
CREATE TABLE hrp2016_creatinine_most_recent AS 
SELECT T1.patient_id, T2.test_name, max_creat_date as creat_recent_date, array_agg(distinct(result_string)) creat_recent_result
FROM emr_labresult T1, 
conf_labtestmap T2,
(select patient_id, max(date) max_creat_date from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name = 'creatinine' group by patient_id) T3
WHERE T1.native_code = T2.native_code
AND T1.patient_id = T3.patient_id
AND test_name = 'creatinine'
AND T1.date = T3.max_creat_date
AND date >= '01-01-2006'
AND date <= '2016-12-31'
GROUP BY T1.patient_id, T2.test_name, max_creat_date;


-- Get date of most recent chlam and/or gon event for each patient
CREATE TABLE hrp2016_chlam_gon_syph_max_dates AS 
SELECT T1.patient_id, T2.max_chlam_date, T3.max_gon_date
FROM hrp2016_gon_chlam_events T1
LEFT JOIN (select patient_id, max(date) max_chlam_date from hrp2016_gon_chlam_events where condition = 'chlamydia' group by patient_id) T2 on (T1.patient_id = T2.patient_id)
LEFT JOIN (select patient_id, max(date) max_gon_date from hrp2016_gon_chlam_events where condition = 'gonorrhea' group by patient_id) T3 on (T1.patient_id = T3.patient_id)
GROUP BY T1.patient_id, T2.max_chlam_date, T3.max_gon_date;

-- Get most recent chlamydia result. Multiple results may appear on the same date so put these in an array.
CREATE TABLE hrp2016_chlam_most_recent AS 
SELECT T1.patient_id, T1.max_chlam_date chlam_recent_date, array_agg(distinct(T2.hef_name)) chlam_recent_result
FROM hrp2016_chlam_gon_syph_max_dates T1,
hrp2016_gon_chlam_events T2
WHERE T1.patient_id = T2.patient_id
AND condition = 'chlamydia'
AND hef_name ilike '%chlam%'
AND T1.max_chlam_date = T2.date
GROUP BY T1.patient_id, T1.max_chlam_date;

-- Get most recent gonorrhea result. Multiple results may appear on the same date so put these in an array.
CREATE TABLE hrp2016_gon_most_recent AS 
SELECT T1.patient_id, T1.max_gon_date gon_recent_date, array_agg(distinct(T2.hef_name)) gon_recent_result
FROM hrp2016_chlam_gon_syph_max_dates T1,
hrp2016_gon_chlam_events T2
WHERE T1.patient_id = T2.patient_id
AND condition = 'gonorrhea'
AND hef_name ilike '%gon%'
AND T1.max_gon_date = T2.date
GROUP BY T1.patient_id, T1.max_gon_date;

-- Get date of most recent syph event for each patient
CREATE TABLE hrp2016_syph_max_dates AS 
SELECT T1.patient_id, max(date) as max_syph_date
FROM hrp2016_syph_events T1
GROUP BY T1.patient_id;

-- Get most recent syphilis result. Multiple results may appear on the same date so put these in an array.
CREATE TABLE hrp2016_syph_most_recent AS 
SELECT T1.patient_id, T1.max_syph_date syph_recent_date, array_agg(distinct(T2.result_data)) syph_recent_result
FROM hrp2016_syph_max_dates T1,
hrp2016_syph_events T2
WHERE T1.patient_id = T2.patient_id
AND T1.max_syph_date = T2.date
GROUP BY T1.patient_id, T1.max_syph_date;

-- Get date of most recent hiv lab date for each patient
CREATE TABLE hrp2016_hiv_max_dates AS 
SELECT T1.patient_id, max(date) as max_hiv_date
FROM hrp2016_hiv_labs T1
GROUP BY patient_id;

-- Get most recent hiv result. Multiple results may appear on the same date so put these in an array.
CREATE TABLE hrp2016_hiv_most_recent AS 
SELECT T1.patient_id, T1.max_hiv_date hiv_recent_date, array_agg(distinct(T2.result_data)) hiv_recent_result
FROM hrp2016_hiv_max_dates T1,
hrp2016_hiv_labs T2
WHERE T1.patient_id = T2.patient_id
AND T1.max_hiv_date = T2.date
GROUP BY T1.patient_id, T1.max_hiv_date;

-- Get the most recent weight for the patient
-- Get the most recent weight for the patient
CREATE TABLE hrp2016_most_recent_weight AS 
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, round(weight::numeric, 2) most_recent_weight, raw_weight, max_weight_date from
emr_encounter T1,
(select patient_id, max(date) max_weight_date from emr_encounter where weight is not null group by patient_id) T2,
hrp2016_index_patients T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.patient_id = T3.patient_id
AND T1.date = max_weight_date
AND T1.weight is not null
ORDER BY T1.patient_id;



-- Bring together all of the fields and values
CREATE TABLE hrp2016_index_data AS
SELECT T1.patient_id,
name,
mrn,
current_age,
pcp_name,
pcp_site,
sex_raw,
sex,
race_raw,
race_black,
race_caucasian,
home_language_raw,
english_lang,
has_language_data,
coalesce(years_of_data, 0) years_of_data,
coalesce(has_1yr_data, 0) has_1yr_data,
coalesce(has_2yr_data, 0) has_2yr_data,
coalesce(t_hiv_rna_1_yr, 0) t_hiv_rna_1_yr,
coalesce(t_hiv_test_2yr, 0) t_hiv_test_2yr,
coalesce(t_hiv_test_e, 0) t_hiv_test_e,
coalesce(t_hiv_elisa_e, 0) t_hiv_elisa_e,
coalesce(acute_hiv_test_2yr, 0) acute_hiv_test_2yr,
coalesce(acute_hiv_test_e, 0) acute_hiv_test_e,
coalesce(rx_bicillin_1_yr, 0) rx_bicillin_1_yr,
coalesce(rx_bicillin_2yr, 0) rx_bicillin_2yr,
coalesce(rx_bicillin_e, 0) rx_bicillin_e,
coalesce(rx_suboxone_2yr, 0) rx_suboxone_2yr,
coalesce(t_pos_gon_test_2yr, 0) t_pos_gon_test_2yr,
coalesce(t_chla_test_t_e, 0) t_chla_test_t_e,
coalesce(syphilis_any_site_state_x_late_e, 0) syphilis_any_site_state_x_late_e,
coalesce(contact_w_or_expo_venereal_dis_e, 0) contact_w_or_expo_venereal_dis_e,
coalesce(hiv_counseling_2yr, 0) hiv_counseling_2yr,
coalesce(creat_recent_date, null) creat_last_date,
coalesce(creat_recent_result, null) creat_last_value,
coalesce(chlam_recent_date, null) chlam_last_date,
coalesce(chlam_recent_result, null) chlam_last_result,
coalesce(gon_recent_date, null) gon_last_date,
coalesce(gon_recent_result, null) gon_last_result,
coalesce(syph_recent_date, null) syph_last_date,
coalesce(syph_recent_result, null) syph_last_result,
coalesce(hiv_recent_date, null) hiv_last_date,
coalesce(hiv_recent_result, null) hiv_last_result,
coalesce(most_recent_weight, null) most_recent_weight
FROM hrp2016_index_patients T1
LEFT JOIN hrp2016_pat_encs T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN hrp2016_hiv_lab_values T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN hrp2016_bicillin_values T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN hrp2016_suboxone_values T5 ON (T1.patient_id = T5.patient_id)
LEFT JOIN hrp2016_gon_chlam_values T6 ON (T1.patient_id = T6.patient_id)
LEFT JOIN hrp2016_diags_values T7 ON (T1.patient_id = T7.patient_id)
LEFT JOIN hrp2016_creatinine_most_recent T8 ON (T1.patient_id = T8.patient_id)
LEFT JOIN hrp2016_chlam_most_recent T9 ON (T1.patient_id = T9.patient_id)
LEFT JOIN hrp2016_gon_most_recent T10 ON (T1.patient_id = T10.patient_id)
LEFT JOIN hrp2016_syph_most_recent T11 ON (T1.patient_id = T11.patient_id)
LEFT JOIN hrp2016_hiv_most_recent T12 ON (T1.patient_id = T12.patient_id)
LEFT JOIN hrp2016_most_recent_weight T13 ON (T1.patient_id = T13.patient_id)
;

-- Compute the sum to be used in the risk score calculation
CREATE TABLE hrp2016_pat_sum_x_value AS
SELECT patient_id, 
(
(i.sex * c.sex) +
(i.race_black * c.race_black) +
(i.race_caucasian * c.race_caucasian) +
(i.english_lang * c.english_lang) +
(i.has_language_data * c.has_language_data) +
(i.years_of_data * c.years_of_data) +
(i.has_1yr_data * c.has_1yr_data) +
(i.has_2yr_data * c.has_2yr_data) +
(i.t_hiv_rna_1_yr * c.t_hiv_rna_1_yr) + 
(i.t_hiv_test_2yr * c.t_hiv_test_2yr) +
(i.t_hiv_test_e * c.t_hiv_test_e) +
(i.t_hiv_elisa_e * c.t_hiv_elisa_e) +
(i.acute_hiv_test_e * c.acute_hiv_test_e) +
(i.acute_hiv_test_2yr * c.acute_hiv_test_2yr) +
(i.t_pos_gon_test_2yr * c.t_pos_gon_test_2yr) +
(i.t_chla_test_t_e * c.t_chla_test_t_e) +
(i.rx_bicillin_1_yr * c.rx_bicillin_1_yr) +
(i.rx_bicillin_2yr * c.rx_bicillin_2yr) +
(i.rx_bicillin_e * c.rx_bicillin_e) +
(i.rx_suboxone_2yr * c.rx_suboxone_2yr) +
(i.syphilis_any_site_state_x_late_e * c.syphilis_any_site_state_x_late_e) +
(i.contact_w_or_expo_venereal_dis_e * c.contact_w_or_expo_venereal_dis_e) +
(i.hiv_counseling_2yr * c.hiv_counseling_2yr)
) as pat_sum_x_value
FROM hrp2016_index_data i,
hrp2016_coefficients c;

CREATE TABLE hrp2016_risk_scores AS
SELECT patient_id, (1 / (1 + exp(-(pat_sum_x_value)))) hiv_risk_score
FROM hrp2016_pat_sum_x_value;

CREATE TABLE hrp2016_data_output AS
SELECT round(hiv_risk_score::numeric, 6) hiv_risk_score, pat_sum_x_value, T3.*
FROM hrp2016_pat_sum_x_value T1,
hrp2016_risk_scores T2,
hrp2016_index_data T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.patient_id = T3.patient_id;




-- Questions
-- ---------
-- - When gender is unknown or not male or female, do you want to exclude or use a different value?
	-- - Being Investigated By DPM

-- - What should be the basis for having EHR data? (any of the following: encounter, lab_test, or prescription? If encounter, does it need to be an encounter with vitals or dx?)
	-- - Encounter of the type for PREP report. I need to point out to them that this is a very limited listed of encounter types!

-- - For HIV RNA tests, only include those mapped to hiv_viral_dna or include hiv_pcr as well?
	-- - 'hiv_rna_viral' only

-- - Should the HIV tests be counted based on type and date? For example, an RNA test may return results in quant and quant log. These are received as 2 results. Should this be counted as 2 tests or only 1 test per type per day? So if we have 2 RNA results on the same date, count as 1 test? Same for chlamydia and gonorrhea...only count 1 test per date per condition?

	-- - Do not condense. Compare values to PREP output values.


-- - In the PREP report, we limited bicillin prescriptions to a specific set of prescriptions. Should they be limited for this report as well or include any bicillin rx. Here is the list from the previous report:
	-- 'BICILLIN C-R 1,200,000 UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	-- 'BICILLIN C-R 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	-- 'BICILLIN C-R 600,000 UNIT/ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)' , 
	-- 'BICILLIN C-R 900,000 UNIT-300K UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	-- 'BICILLIN C-R DISP SYRIN 1.2MMU/2ML IM (PEN G BENZ/PEN G PROCAINE)', 
	-- 'BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 
	-- 'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)' , 
	-- 'BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 
	-- 'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)', 
	-- 'BICILLIN L-A DISP SYRIN 600000 U IM (PENICILLIN G BENZATHINE)', 
	-- 'BICILLIN L-A DISP SYRIN 600MU/1ML IM (PENICILLIN G BENZATHINE)', 
	-- 'BICILLIN L-A IM', 
	-- 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE', 
	-- 'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE', 
	-- 'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE', 
	-- 'PENICILLIN G BENZATHINE&PROCAIN 1,200,000 UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	-- 'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	-- 'PENICILLIN G BENZATHINE&PROCAIN 600,000 UNIT/ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	-- 'PENICILLIN G BENZATHINE&PROCAIN 900,000 UNIT-300K UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)'
	
	-- - Waiting for feedback from DPM
	
-- - In regard to syphilis_any_site_state_x_late_e, should this be based on an ESP syphilis case or based on ICD codes (icd9:091.*, icd9:092.*, icd9:093.*, icd9:094.*, icd9:095.*, icd10:A51.*, icd9:097.9, icd10:A52.0, icd10:A52.7) or either?
	-- - Use ICD codes

-- ====================================================
-- - EHR history years	
-- Still need to clarify if a patient must have an encounter in each of the 2 previous years to be counted?
-- Also ensure they are on board with the encounter types
	
-- -- Need to clarify that counts for total hiv tests ever should only include AG/AB, RNA VIRAL, and ELISA? Do not include PCR and WB
-- -- What about for the t_hiv_test_2yr tests?








