CREATE TABLE hiv_rpt.high_risk_pats
(
    patient_id integer NOT NULL,
    index_date date NOT NULL,
    index_score numeric NOT NULL,
    latest_date date,
    latest_score numeric,
    created_timestamp timestamp with time zone NOT NULL,
    updated_timestamp timestamp with time zone,
    sent_timestamp timestamp with time zone,
    active boolean,
    CONSTRAINT "hiv_rpt.high_risk_pats_pkey" PRIMARY KEY (patient_id)
	);

-- INSERT PATIENTS WHO DO NOT ALREADY HAVE A SCORE
INSERT INTO hiv_rpt.high_risk_pats
SELECT 
DISTINCT patient_id,
'2020-05-31'::date as index_date,
hiv_risk_score as index_score,
'2020-05-31'::date as latest_date,
hiv_risk_score as latest_score,
now() as created_timestamp,
now() as updated_timestamp,
null::timestamp as sent_timestamp,
true as active
FROM gen_pop_tools.cc_hiv_risk_score
WHERE patient_id not in (select patient_id from hiv_rpt.high_risk_pats)
AND hiv_risk_score >= 0.05
AND rpt_year = extract(year from '2020-05-31'::date)::text;

-- UPDATE PATIENTS THAT ALREADY HAVE A SCORE

SELECT T1.patient_id,
'2020-05-31'::date as latest_date,
hiv_risk_score as latest_score,
now() as updated_timestamp
FROM gen_pop_tools.cc_hiv_risk_score T1
JOIN hiv_rpt.high_risk_pats T2 on (T1.patient_id = T2.patient_id)
WHERE T2.created_timestamp::date < '2020-05-31'::date
AND T1.rpt_year = extract(year from '2020-05-31'::date)::text;

UPDATE hiv_rpt.high_risk_pats T1
SET latest_date = '2020-05-31',
latest_score = hiv_risk_score,
updated_timestamp = now(),
active = true 
FROM gen_pop_tools.cc_hiv_risk_score T2
WHERE T1.patient_id = T2.patient_id
AND T1.latest_date::date < '2020-05-31'::date
AND T2.rpt_year = extract(year from '2020-05-31'::date)::text;

-- INACTIVATE PATIENTS WHO DO NOT HAVE A RISK SCORE
UPDATE hiv_rpt.high_risk_pats T1
SET active = false
WHERE patient_id not in 
	(SELECT patient_id from gen_pop_tools.cc_hiv_risk_score 
	 WHERE rpt_year = extract(year from '2020-05-31'::date)::text);



-- patients that drop from high_risk_pats (meaning they do not have a qualifying encounter in the last 2 years, are diagnosed with HIV, etc)
-- maybe add an active flag?

-- ATRIUS
-- ASSIGN A PERCENTILE TO HIGH RISK PATIENTS (BUT ONLY IF THEY ARE ASSOCIATED WITH AN ATRIUS SITE/PROVIDER )
CREATE TABLE kre_report.hrp_high_risk_percentile_with_provider as 
select ntile(100) over (order by hiv_risk_score desc) as percentile, * 
from kre_report.hrp_data_output
where pcp_site is not null 
and pcp_site not ilike 'EXTERNAL%'
and pcp_site not ilike 'UNKNOWN';

 


