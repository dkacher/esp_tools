#!/srv/esp/prod/bin/python


# module to access PostgreSQL databases
import psycopg2
import numpy as np
from itertools import chain

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt 

# Variables for the Script
esp_db = 'esp30'
score_cutoff = .01
site_name = 'ATR'

# connect to ESP database
conn = psycopg2.connect(database=esp_db)
# prepare a cursor
cur = conn.cursor()


# Get scores for patients for the cutoff value supplied
query = ( """
select hiv_risk_score::float from kre_report.dph_prep_gte01_data
where hiv_risk_score >= %s
order by hiv_risk_score""" % score_cutoff)

# execute the query
cur.execute(query)
# retrieve the whole result set
data = cur.fetchall()

# close cursor and connection
cur.close()

# unpack data
risk_score = list(zip(*data))

num_scores = len(list(chain(*risk_score))) 

# Get count of all patients with a score
cur = conn.cursor()
query = ( """
select count(*) from kre_report.dph_prep_gte01_data
""")

# execute the query
cur.execute(query)
num_all_scores = cur.fetchone()

# close cursor and connection
cur.close()

num_all_scores = int(*num_all_scores)

# Graphs and Charts Start Here

fig = matplotlib.pyplot.gcf()
fig.set_size_inches(20, 10)

# setting the ranges and no. of intervals
range = (0, 1)

bins=[0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.4, 0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49, 0.5, 0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59, 0.6, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.7, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79, 0.8, 0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.9, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.0]


plt.grid(color='#DCDCDC', linestyle='-', linewidth=.5)


# plotting a histogram
plt.hist(risk_score, bins, range, color = '#4799b3',
        histtype = 'bar', rwidth = 0.8)


locs, labels = plt.xticks()  # Get the current locations and labels.


plt.xticks([0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.4, 0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49, 0.5, 0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59, 0.6, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.7, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79, 0.8, 0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.9, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.0], ['0', '.01', '', '.03' ,'', '.05','' ,'.07', '', '.09', '', '.11', '', '.13', '', '.15', '', '.17', '', '.19', '', '.21', '', '.23', '', '.25', '', '.27', '', '.29', '', '.31', '', '.33', '', '.35', '', '.37', '', '.39', '', '.41', '', '.43', '', '.45', '', '.47', '', '.49', '', '.51', '', '.53', '', '.55', '', '.57', '', '.59', '', '.61', '', '.63', '', '.65', '', '.67', '', '.69', '', '.71', '', '.73', '', '.75', '', '.77', '', '.79', '', '.81', '', '.83', '', '.85', '', '.87', '', '.89', '', '.91', '', '.93', '', '.95', '', '.97', '', '.99', '1.0'], rotation=70)


locs, labels = plt.yticks()  # Get the current locations and labels.
plt.yticks(np.arange(0, 200, step=5))

  
  
# x-axis label 
plt.xlabel('Risk Score') 
# frequency label 
plt.ylabel('No. of patients') 
# plot title 
plt.suptitle(site_name +' - High Risk Patients - Risk Score >= ' + str(score_cutoff))  
plt.title('Total Patients: {}'.format(num_scores))

  
# function to show the plot 
#plt.show() 

plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-score-histogram.png')

plt.clf()


#HISTOGRAM OF SCORES FOR TRUVDA EVER

cur = conn.cursor()

query = ( """
select hiv_risk_score::float from kre_report.dph_prep_gte01_data 
--where rpt_year = '2020'
where hiv_risk_score >= %s
and truvada_ever = 'Y'
order by hiv_risk_score""" % score_cutoff)

# execute the query
cur.execute(query)
# retrieve the whole result set
data = cur.fetchall()

# close cursor and connection
cur.close()

# unpack data
risk_score = list(zip(*data))

num_truv_ever_scores = len(list(chain(*risk_score)))

fig = matplotlib.pyplot.gcf()
fig.set_size_inches(20, 10)

# setting the ranges and no. of intervals
range = (0, 1)

bins=[0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.4, 0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49, 0.5, 0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59, 0.6, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.7, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79, 0.8, 0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.9, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.0]


plt.grid(color='#DCDCDC', linestyle='-', linewidth=.5)


# plotting a histogram
plt.hist(risk_score, bins, range, color = '#4799b3',
        histtype = 'bar', rwidth = 0.8)


locs, labels = plt.xticks()  # Get the current locations and labels.


plt.xticks([0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.4, 0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49, 0.5, 0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59, 0.6, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.7, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79, 0.8, 0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.9, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.0], ['0', '.01', '', '.03' ,'', '.05','' ,'.07', '', '.09', '', '.11', '', '.13', '', '.15', '', '.17', '', '.19', '', '.21', '', '.23', '', '.25', '', '.27', '', '.29', '', '.31', '', '.33', '', '.35', '', '.37', '', '.39', '', '.41', '', '.43', '', '.45', '', '.47', '', '.49', '', '.51', '', '.53', '', '.55', '', '.57', '', '.59', '', '.61', '', '.63', '', '.65', '', '.67', '', '.69', '', '.71', '', '.73', '', '.75', '', '.77', '', '.79', '', '.81', '', '.83', '', '.85', '', '.87', '', '.89', '', '.91', '', '.93', '', '.95', '', '.97', '', '.99', '1.0'], rotation=70)


locs, labels = plt.yticks()  # Get the current locations and labels.
plt.yticks(np.arange(0, 50, step=5))

# x-axis label
plt.xlabel('Risk Score')
# frequency label
plt.ylabel('No. of patients')
# plot title
plt.suptitle(site_name +' - Truvada Ever - High Risk Patients - Risk Score >= ' + str(score_cutoff))
plt.title('Total Patients: {}'.format(num_truv_ever_scores))


# function to show the plot
#plt.show()

plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-score-truv-ever-histogram.png')

plt.clf()


#HISTOGRAM OF SCORES FOR TRUVDA RX 2020

cur = conn.cursor()

query = ( """
select hiv_risk_score::float from kre_report.dph_prep_gte01_data
--where rpt_year = '2020'
where hiv_risk_score >= %s
and truvada_rx_2020 = 'Y'
order by hiv_risk_score""" % score_cutoff)

# execute the query
cur.execute(query)
# retrieve the whole result set
data = cur.fetchall()

# close cursor and connection
cur.close()

# unpack data
risk_score = list(zip(*data))

num_truv_2020_scores = len(list(chain(*risk_score)))

fig = matplotlib.pyplot.gcf()
fig.set_size_inches(20, 10)

# setting the ranges and no. of intervals
range = (0, 1)

bins=[0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.4, 0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49, 0.5, 0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59, 0.6, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.7, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79, 0.8, 0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.9, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.0]


plt.grid(color='#DCDCDC', linestyle='-', linewidth=.5)


# plotting a histogram
plt.hist(risk_score, bins, range, color = '#4799b3',
        histtype = 'bar', rwidth = 0.8)


locs, labels = plt.xticks()  # Get the current locations and labels.


plt.xticks([0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.4, 0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49, 0.5, 0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59, 0.6, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.7, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79, 0.8, 0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.9, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.0], ['0', '.01', '', '.03' ,'', '.05','' ,'.07', '', '.09', '', '.11', '', '.13', '', '.15', '', '.17', '', '.19', '', '.21', '', '.23', '', '.25', '', '.27', '', '.29', '', '.31', '', '.33', '', '.35', '', '.37', '', '.39', '', '.41', '', '.43', '', '.45', '', '.47', '', '.49', '', '.51', '', '.53', '', '.55', '', '.57', '', '.59', '', '.61', '', '.63', '', '.65', '', '.67', '', '.69', '', '.71', '', '.73', '', '.75', '', '.77', '', '.79', '', '.81', '', '.83', '', '.85', '', '.87', '', '.89', '', '.91', '', '.93', '', '.95', '', '.97', '', '.99', '1.0'], rotation=70)


locs, labels = plt.yticks()  # Get the current locations and labels.
plt.yticks(np.arange(0, 50, step=5))

# x-axis label
plt.xlabel('Risk Score')
# frequency label
plt.ylabel('No. of patients')
# plot title
plt.suptitle(site_name +' - Truvada RX 2020 - High Risk Patients - Risk Score >= ' + str(score_cutoff))
plt.title('Total Patients: {}'.format(num_truv_2020_scores))


# function to show the plot
#plt.show()

plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-score-truv-2020-histogram.png')

plt.clf()





races = ['ASIAN', 'BLACK', 'NAT AMERICAN', 'OTHER', 'UNKNOWN', 'WHITE']
colors = ['#8fd2a4', '#4799b3', '#8a7ec0','#dcf19f','#fdf5ad', '#fdc676', '#f47c4d','#cd364b','#4d66ac','#58afad','#a9dda2', '#ebf7a6', '#feeb9f']


cur = conn.cursor()
itemlist =[]

for c in races:
    itemlist.append(c) #append each item to the list


mylist = []#create an empty list that is able to be appended
for items in races: #repeat the below commands in the loop for each list member
    cur.execute("select count(race) from kre_report.dph_prep_gte01_data where race='%s'" % (items))
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        mylist.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
cur.close()

outside_counts = list(chain(*mylist))

cur = conn.cursor()

mylist2 = []#create an empty list that is able to be appended
for items in races: #repeat the below commands in the loop for each list member
    cur.execute("select count(race) from kre_report.dph_prep_gte01_data where race='%s' and hiv_risk_score>=%s" % (items, score_cutoff))
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        mylist2.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
cur.close()

inside_counts = list(chain(*mylist2))


labels=itemlist

def pie(ax, values, **kwargs):
    total = sum(values)
    autopct='%1.1f%%'
    wedges, _, labels = ax.pie(values, autopct='%1.1f%%', **kwargs)
    return wedges

fig, ax = plt.subplots(figsize=(9,9)) #set the size of the plot

ax.axis('equal')

width = .35
kwargs = dict(colors=colors, startangle=90)

outside = pie(ax, outside_counts, radius=1, pctdistance=1-width/2, **kwargs)

inside = pie(ax, inside_counts, radius=1-width,
             pctdistance=1 - (width/2) / (1-width), **kwargs)


plt.setp(inside + outside, width=width, edgecolor='white')


plt.legend(inside[::1], labels, frameon=False)


#plt.legend(labels,loc='upper center', bbox_to_anchor=(0.5, 1.13),
#         ncol=3, fancybox=True, shadow=True)


plt.suptitle(site_name +' - Race - Risk Score >= ' + str(score_cutoff))
plt.title('Outer Ring: Total Site Population with a Risk Score')
plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-race-pie-chart.png')


plt.clf()

age_groups = ['15-24','25-34','35-44','45-54','55-64','65-74','85+']
colors = ['#8fd2a4', '#4799b3', '#8a7ec0','#dcf19f','#fdf5ad', '#fdc676', '#f47c4d','#cd364b','#4d66ac','#58afad','#a9dda2', '#ebf7a6', '#feeb9f']


cur = conn.cursor()
itemlist =[]

for c in age_groups:
    itemlist.append(c) #append each item to the list


mylist = []#create an empty list that is able to be appended
for items in age_groups: #repeat the below commands in the loop for each list member
    cur.execute("select count(age_group) from kre_report.dph_prep_gte01_data where age_group='%s'" % (items)) #query the database
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        mylist.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
cur.close()

outside_counts = list(chain(*mylist))

cur = conn.cursor()

mylist2 = []#create an empty list that is able to be appended
for items in age_groups: #repeat the below commands in the loop for each list member
    cur.execute("select count(age_group) from kre_report.dph_prep_gte01_data where age_group='%s' and hiv_risk_score >=%s" % (items, score_cutoff)) #query the database
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        mylist2.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
cur.close()

inside_counts = list(chain(*mylist2))
labels=itemlist

def pie(ax, values, **kwargs):
    total = sum(values)
    autopct='%1.1f%%'
    wedges, _, labels = ax.pie(values, autopct='%1.1f%%', **kwargs)
    return wedges

fig, ax = plt.subplots(figsize=(9,9)) #set the size of the plot

ax.axis('equal')

width = .35
explode = (0, 0, 0, 0, 0, 0, .1)
kwargs = dict(colors=colors, startangle=90)
inkwargs = dict(colors=colors, startangle=90, explode=explode)

outside = pie(ax, outside_counts, radius=1, pctdistance=1-width/2, **kwargs)

inside = pie(ax, inside_counts, radius=1-width,
             pctdistance=1 - (width/2) / (1-width), **inkwargs)


plt.setp(inside + outside, width=width, edgecolor='white')
plt.legend(inside[::1], labels, frameon=False)
#plt.legend(labels,loc='upper center', bbox_to_anchor=(0.5, 1.13),
#         ncol=3, fancybox=True, shadow=True)


plt.suptitle(site_name +' - Age Group - Risk Score >= ' + str(score_cutoff))
plt.title('Outer Ring: Total Site Population with a Risk Score')
plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-age-group-pie-chart.png')


plt.clf()

genders = ['MALE', 'FEMALE']
colors = ['#8fd2a4', '#4799b3', '#8a7ec0','#dcf19f','#fdf5ad', '#fdc676', '#f47c4d','#cd364b','#4d66ac','#58afad','#a9dda2', '#ebf7a6', '#feeb9f']


cur = conn.cursor()
itemlist =[]

for c in genders:
    itemlist.append(c) #append each item to the list


mylist = []#create an empty list that is able to be appended
for items in genders: #repeat the below commands in the loop for each list member
    cur.execute("select count(gender) from kre_report.dph_prep_gte01_data where gender='%s'" % (items)) #query the database
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        mylist.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
cur.close()

outside_counts = list(chain(*mylist))

cur = conn.cursor()

mylist2 = []#create an empty list that is able to be appended
for items in genders: #repeat the below commands in the loop for each list member
    cur.execute("select count(gender) from kre_report.dph_prep_gte01_data where gender='%s' and hiv_risk_score>=%s" % (items, score_cutoff)) #query the database
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        mylist2.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
cur.close()

inside_counts = list(chain(*mylist2))

labels=itemlist

def pie(ax, values, **kwargs):
    total = sum(values)
    autopct='%1.1f%%'
    wedges, _, labels = ax.pie(values, autopct='%1.1f%%', **kwargs)
    return wedges

fig, ax = plt.subplots(figsize=(9,9)) #set the size of the plot

ax.axis('equal')

width = .35
kwargs = dict(colors=colors, startangle=90)

outside = pie(ax, outside_counts, radius=1, pctdistance=1-width/2, **kwargs)

inside = pie(ax, inside_counts, radius=1-width,
             pctdistance=1 - (width/2) / (1-width), **kwargs)


plt.setp(inside + outside, width=width, edgecolor='white')


plt.legend(inside[::1], labels, frameon=False)


#plt.legend(labels,loc='upper center', bbox_to_anchor=(0.5, 1.13),
#         ncol=3, fancybox=True, shadow=True)


plt.suptitle(site_name +' - Gender - Risk Score >= ' + str(score_cutoff))
plt.title('Outer Ring: Total Site Population with a Risk Score')
plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-gender-pie-chart.png')

plt.clf()
plt.close()

#reset the view
fig, ax = plt.subplots()
#fig, ax = plt.subplots(figsize=(15,15))
fig = matplotlib.pyplot.gcf()
fig.set_size_inches(20.5, 10.5)

#labels for bars and query parameters
variables = ['truvada_ever', 'truvada_rx_2020', 'hiv_test_2yr', 'hiv_tst_ever', 'syph_diag_ever', 'bicillin_1yr', 'bicillin_2yr', 'pos_gon_2yr', 'chlam_tst_ever', 'suboxone_2yr', 'vd_cont_dx_ever', 'hiv_couns_2yr', 'english_lang']

cur = conn.cursor()
itemlist =[] #create an empty list that is able to be appended
for c in variables: #repeat the command for each item in list. Write them to the variable itemlist. they will be called later to get the labels for the plot
    itemlist.append(c) #append each item to the list
mylist = []#create an empty list that is able to be appended


for items in variables: #repeat the below commands in the loop for each list member
    cur.execute("select count(*) from kre_report.dph_prep_gte01_data where %s = 'Y' and hiv_risk_score>=%s" % (items, score_cutoff)) #query the database
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        #compute % to use as the value to display
        l[0]=(100 * int(l[0]) / num_scores)
        mylist.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
cur.close()


counts = mylist #the results of each sql query count

#heights of bars are the counts
height = list(chain(*counts))

#temp putting in %
#height = [34, 23, 66]

# x-coordinates
x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]


tick_label = itemlist

bars = plt.bar(x, height, tick_label = tick_label,
        width = 0.4, color = ['#8fd2a4', '#4799b3', '#5e4fa2','#dcf19f','#fdf5ad', '#fdc676', '#f47c4d','#cd364b','#4d66ac','#58afad','#a9dda2', '#ebf7a6', '#feeb9f'])


#assign the labels to to the top of the bars
for bar in bars:
    height = bar.get_height()
    plt.text(bar.get_x() + bar.get_width()/2., 0.99*height,
            '%d' % int(height) + "%", ha='center', va='bottom')


#plt.xticks(rotation='vertical')



# naming the x-axis
#plt.xlabel('x - axis')
# naming the y-axis
plt.ylabel('Percentage')


plt.title(site_name +' - Variables - Risk Score >= ' + str(score_cutoff))
plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-variables.png')


plt.clf()
plt.close()


# VARIABLES ALL -- Compare High Risk to Total Population

#reset the view
fig, ax = plt.subplots()
#fig, ax = plt.subplots(figsize=(15,15))
fig = matplotlib.pyplot.gcf()
fig.set_size_inches(20.5, 10.5)

#labels for bars and query parameters
variables = ['truvada_ever', 'truvada_rx_2020', 'hiv_test_2yr', 'hiv_tst_ever', 'syph_diag_ever', 'bicillin_1yr', 'bicillin_2yr', 'pos_gon_2yr', 'chlam_tst_ever', 'suboxone_2yr', 'vd_cont_dx_ever', 'hiv_couns_2yr', 'english_lang']

cur = conn.cursor()
itemlist =[] #create an empty list that is able to be appended
for c in variables: #repeat the command for each item in list. Write them to the variable itemlist. they will be called later to get the labels for the plot
    itemlist.append(c) #append each item to the list
mylist = []#create an empty list that is able to be appended


for items in variables: #repeat the below commands in the loop for each list member
    cur.execute("select count(*) from kre_report.dph_prep_gte01_data where %s = 'Y' and hiv_risk_score <%s" % (items, score_cutoff)) #query the database
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        #compute % to use as the value to display
        l[0]=(100 * int(l[0]) / num_all_scores)
        mylist.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
cur.close()

counts = mylist #the results of each sql query count

#heights of bars are the counts
height1 = list(chain(*counts))

mylist = [] #reset mylist

cur = conn.cursor()
for items in variables: #repeat the below commands in the loop for each list member
    cur.execute("select count(*) from kre_report.dph_prep_gte01_data where %s = 'Y' and hiv_risk_score>=%s" % (items, score_cutoff)) #query the database
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        #compute % to use as the value to display
        l[0]=(100 * int(l[0]) / num_scores)
        mylist.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
cur.close()

counts = mylist #the results of each sql query count
#heights of bars are the counts
height2 = list(chain(*counts))


barWidth = 0.25
color = ['#feeb9f']
color2 = ['#6fc1a8']


# Set position of bar on X axis
r1 = np.arange(len(height1))
r2 = [x + barWidth for x in r1]

tick_label = itemlist

bars = plt.bar(r1, height1, tick_label = tick_label,
        width = barWidth, color = color, label='Below Threshold')

#assign the labels to to the top of the bars
for bar in bars:
    height = bar.get_height()
    plt.text(bar.get_x() + bar.get_width()/2., 0.99*height,
            '%d' % int(height) + "%", ha='center', va='bottom')

bars = plt.bar(r2, height2, tick_label = tick_label,
        width = barWidth, color = color2, label='High Risk')

#assign the labels to to the top of the bars
for bar in bars:
    height = bar.get_height()
    plt.text(bar.get_x() + bar.get_width()/2., 0.99*height,
            '%d' % int(height) + "%", ha='center', va='bottom')

# naming the y-axis
plt.ylabel('Percentage')
plt.legend()

plt.suptitle(site_name +' - Compare Variables - Risk Score >= ' + str(score_cutoff))
plt.title('Patients below risk score threshold versus at/above score threshold')
plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-compare-variables.png')

plt.clf()
plt.close()


# HISTOGRAM OF DAYS SUPPLY OF TRUVADA

cur = conn.cursor()

query = ("""
select truvada_time from kre_report.dph_prep_gte01_data
where total_truvada_days > 0 
and hiv_risk_score >= %s
""" % score_cutoff)


# execute the query
cur.execute(query)
# retrieve the whole result set
data = cur.fetchall()

# close cursor and connection
cur.close()
#conn.close()

# unpack data
truvada_days = list(zip(*data))

num_rx = len(list(chain(*truvada_days)))

fig = matplotlib.pyplot.gcf()
fig.set_size_inches(20, 10)

# setting the ranges and no. of intervals
range = (1, 12)
bins = 11 


# plotting a histogram
plt.hist(truvada_days, bins, range, color = '#8fd2a4',
        histtype = 'bar', rwidth = 0.8)

plt.grid(color='#DCDCDC', linestyle='-', linewidth=.5)


locs, labels = plt.xticks()  # Get the current locations and labels.

plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], ['0 mo', '1 mo', '3 mo', '6 mo', '1 yr', '2 yrs', '3 yrs', '4 yrs', '5 yrs', '6 yrs', '7 yrs', 'Beyond'])

locs, labels = plt.yticks()  # Get the current locations and labels.
plt.yticks(np.arange(0, 40, step=2))



# x-axis label
plt.xlabel('Days Supply')
# frequency label
plt.ylabel('# of patients')

plt.suptitle(site_name +' - Truvada Duration - Risk Score >= ' + str(score_cutoff))
plt.title('Total Patients: {}'.format(num_rx))

plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-truvada-days-historgram.png')

plt.clf()
plt.close()


# HISTOGRAM OF NUMBER OF RX RECORDS
# UNIQUE BY DATE

cur = conn.cursor()

query = ("""
select truvada_rx_num from kre_report.dph_prep_gte01_data
where truvada_rx_num > 0
and hiv_risk_score >= %s
""" % score_cutoff)


# execute the query
cur.execute(query)
# retrieve the whole result set
data = cur.fetchall()

# close cursor and connection
cur.close()
#conn.close()

# unpack data
truvada_rx_num = list(zip(*data))

num_rx = len(list(chain(*truvada_rx_num)))

fig = matplotlib.pyplot.gcf()
fig.set_size_inches(20, 10)

# setting the ranges and no. of intervals
range = (1, 50)
bins = 49 


# plotting a histogram
plt.hist(truvada_rx_num, bins, range, color = '#5e4fa2',
        histtype = 'bar', rwidth = 0.8)

plt.grid(color='#DCDCDC', linestyle='-', linewidth=.5)


locs, labels = plt.xticks()  # Get the current locations and labels.

plt.xticks([0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50])

locs, labels = plt.yticks()  # Get the current locations and labels.
plt.yticks(np.arange(0, 30, step=2))

# x-axis label
plt.xlabel('# of RX Records')
# frequency label
plt.ylabel('# of patients')

plt.suptitle(site_name +' - Truvada Rx Records - Risk Score >= ' + str(score_cutoff))
plt.title('Total Patients: {}'.format(num_rx))

plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-truvada-rxnum-histogram.png')

plt.clf()
plt.close()


# x-axis label
plt.xlabel('# of RX Records')
# frequency label
plt.ylabel('# of patients')

plt.suptitle(site_name +' - Truvada RX Records - Risk Score >= ' + str(score_cutoff))
plt.title('Total Patients: {}'.format(num_rx))



#reset the view
fig, ax = plt.subplots()
#fig, ax = plt.subplots(figsize=(15,15))
fig = matplotlib.pyplot.gcf()
fig.set_size_inches(10, 10)

#labels for bars and query parameters
variables = ['gon_case', 'chlam_case', 'syph_case', 'gon_and_chlam_case']

cur = conn.cursor()
itemlist =[] #create an empty list that is able to be appended
for c in variables: #repeat the command for each item in list. Write them to the variable itemlist. they will be called later to get the labels for the plot
    itemlist.append(c) #append each item to the list
mylist = []#create an empty list that is able to be appended


for items in variables: #repeat the below commands in the loop for each list member
    cur.execute("select count(*) from kre_report.dph_prep_gte01_data where %s = 'Y' and hiv_risk_score >=%s" % (items, score_cutoff)) #query the database
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        #compute % to use as the value to display
        l[0]=(100 * int(l[0]) / num_scores)
        mylist.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
cur.close()


counts = mylist #the results of each sql query count

#heights of bars are the counts
height = list(chain(*counts))

#temp putting in %
#height = [34, 23, 66]

# x-coordinates
x = [1, 2, 3, 4]


tick_label = itemlist

bars = plt.bar(x, height, tick_label = tick_label,
        width = 0.4, color = ['#8fd2a4', '#4799b3', '#fdc676', '#dcf19f','#fdf5ad', '#5e4fa2', '#f47c4d','#cd364b','#4d66ac','#58afad','#a9dda2', '#ebf7a6', '#feeb9f'])


#assign the labels to to the top of the bars
for bar in bars:
    height = bar.get_height()
    plt.text(bar.get_x() + bar.get_width()/2., 0.99*height,
            '%d' % int(height) + "%", ha='center', va='bottom')

#plt.xticks(rotation='vertical')



# naming the x-axis
#plt.xlabel('x - axis')
# naming the y-axis
plt.ylabel('Percentage')


plt.title(site_name +' ESP Conditions - Risk Score >= ' + str(score_cutoff))
plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-conditions.png')


plt.clf()
plt.close()


# Basic Counts Chart


# Get count of all patients with a >= .01 score
cur = conn.cursor()
query = ( """
select count(*) from kre_report.dph_prep_gte01_data where hiv_risk_score >= .01
""")

# execute the query
cur.execute(query)
num_gte01_scores = cur.fetchone()

# close cursor and connection
cur.close()

num_gte01_scores = int(*num_gte01_scores)


# Get count of all patients with a >= .02 score
cur = conn.cursor()
query = ( """
select count(*) from kre_report.dph_prep_gte01_data where hiv_risk_score >= .02
""")

# execute the query
cur.execute(query)
num_gte02_scores = cur.fetchone()

# close cursor and connection
cur.close()

num_gte02_scores = int(*num_gte02_scores)

# Get count of all patients with a clinical encounter in 2019
cur = conn.cursor()
query = ( """
select count(distinct(patient_id)) from gen_pop_tools.clin_enc
where extract(year from date) = '2019'
""")

# execute the query
cur.execute(query)
num_pats_clin_enc = cur.fetchone()

# close cursor and connection
cur.close()

num_pats_clin_enc = int(*num_pats_clin_enc)

# Get count of patients that acquired HIV in 2019
cur = conn.cursor()
query = ( """
select count(distinct(patient_id)) from nodis_case
where extract(year from date) = '2019' and condition = 'hiv'
""")

# execute the query
cur.execute(query)
num_new_hiv = cur.fetchone()

# close cursor and connection
cur.close()

num_new_hiv = int(*num_new_hiv)




#fig = plt.figure(dpi=120)
#fig = plt.figure()
#ax = fig.add_subplot(1,1,1)

fig, ax = plt.subplots()
fig.set_size_inches(10, 5)
fig.set_dpi(150)


table_data=[
    ["Total # of Patients with a Risk Score", num_all_scores],
    ["Risk Score of .01 Represents a 1% Risk of HIV Acquisition Next Year",''],
    ["Total patients with a Risk Score >= 0.01", num_gte01_scores],
    ["Risk Score of .02 Represents a 2% Risk of HIV Acquisition Next Year",''],
    ["Total patients with a Risk Score >= 0.02", num_gte02_scores],
    ["General Population - Risk of Acquisition", '12/100000'],
    ["Site New Cases of HIV in 2019", str(num_new_hiv)+'/'+ str(num_pats_clin_enc)],
]


#table = plt.table(cellText=table_data, loc='best', colWidths=column_width)
table = plt.table(cellText=table_data, loc='best')
#table.auto_set_font_size(False)
#table.set_fontsize(9)
table.scale(1,2)
ax.axis('off')

plt.title(site_name +' Overall Summary')


plt.savefig('/tmp/' + site_name + '-overall-counts.png')



#COUNTS AND PREVALANCE

variables = ['truvada_ever', 'truvada_rx_2020', 'hiv_test_2yr', 'hiv_tst_ever', 'syph_diag_ever', 'bicillin_1yr', 'bicillin_2yr', 'pos_gon_2yr', 'chlam_tst_ever', 'suboxone_2yr', 'vd_cont_dx_ever', 'hiv_couns_2yr', 'english_lang']


cur = conn.cursor()
itemlist =[] #create an empty list that is able to be appended
for c in variables: #repeat the command for each item in list.
    itemlist.append(c) #append each item to the list
mylist = []#create an empty list that is able to be appended
high_mylist = []

all_name_and_count = []
high_name_and_count = []


for items in variables: #repeat the below commands in the loop for each list member
    cur.execute("select count(*) from kre_report.dph_prep_gte01_data where %s = 'Y' and hiv_risk_score <%s" % (items, score_cutoff)) #query the database
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        #compute % to use as the value to display
        #l[0]=(100 * int(l[0]) / num_all_scores)
        mylist.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
        all_name_and_count.append(ted)
    cur.execute("select count(*) from kre_report.dph_prep_gte01_data where %s = 'Y' and hiv_risk_score >=%s" % (items, score_cutoff)) #query the database
    result = cur.fetchall() #cursor.fetchall shows all results from the querys in the iterative loop.
    for row in result: #iterate through the result of each SQL query and perform the following commands in the iterative loop
        l= list(map(int,row)) #the count of the occurrence in the database will be a long. we need to turn it into an integer.
        #compute % to use as the value to display
        #l[0]=(100 * int(l[0]) / num_all_scores)
        high_mylist.append(l) #append all results of the SQL query to mylist
        ted= items,l[0] #define a variable that includes the item name, and then their number of occurrence
        high_name_and_count.append(ted)

cur.close()

all_counts = mylist
high_counts = high_mylist


table_data=[]


i=0
while i < len(variables):
    all_pct = '{percent:.2%}'.format(percent=float(*all_counts[i]) / float(num_all_scores))
    high_pct = '{percent:.2%}'.format(percent=float(*high_counts[i]) / float(num_scores))
    prev_ratio = "{0:.2f}".format( (float(*high_counts[i]) / float(num_scores)) / (float(*all_counts[i]) / float(num_all_scores)))

    table_data.append([variables[i], int(*all_counts[i]), int(*high_counts[i]), all_pct, high_pct, prev_ratio])
    i = i+1

#reset the view
fig, ax = plt.subplots()
fig.set_size_inches(10, 10)

column_labels = ['Characteristic', '# Below '+ str(score_cutoff), '# Above ' + str(score_cutoff), '% Below ' + str(score_cutoff), '% Above ' + str(score_cutoff),  'Prevalence Ratio']

table = plt.table(cellText=table_data, loc='best', colLabels = column_labels)

table.scale(1,2)
ax.axis('off')

plt.suptitle(site_name +' Characteristic Counts & Prevalence Ratios')
plt.title('Risk Score >= ' + str(score_cutoff))
plt.savefig('/tmp/' + site_name + '-hiv-gte' + str(score_cutoff) + '-counts-prev.png')



conn.close()














