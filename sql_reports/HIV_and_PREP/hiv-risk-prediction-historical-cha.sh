#!/bin/bash

## SET YOUR VARIABLES HERE ##

#DB_NAME=esp30
DB_NAME=esp

#SCRIPT_DIR=/esp/app/esp_tools/sql_reports/HIV_and_PREP
SCRIPT_DIR=/srv/esp/esp_tools/sql_reports/HIV_and_PREP
SCRIPT='hiv-risk-prediction-historical.sql'


## END VARIABLE SET ##
## DO NOT MODIFY BELOW THIS LINE ##

HIV_SCRIPT=$SCRIPT_DIR/$SCRIPT


if [ "$1" != "" ]; then
    end_calc_year=$1
else
    echo "Insert Last Day of Year to Be Calculated (i.e. 2016-12-31)"
    read end_calc_year
fi

echo "You entered: $end_calc_year"

hiv_risk_sql=$(psql $DB_NAME -f "$HIV_SCRIPT" -v end_calc_year="'$end_calc_year'" -t | tr -d ' \n')

echo "FINISHED HIV RISK SCORE CALCULATION FOR YEAR ENDING $end_calc_year"