-- PREP R21
-- Population-Level Effects of Increasing PrEP Uptake on Incidence of HIV and Bacterial STIs

-- Person-time data set with one record per individual per week of follow-up. 

-- The following inclusion criteria at some point during 2012-2019: 
-- 1) Male; 
-- 2) aged ≥15
-- 3) an HIV-negative test. 
-- The first week of follow-up (week=0) for an individual will be the first week when all of these inclusion criteria are met. 

CREATE SCHEMA IF NOT EXISTS hiv_rpt;


SET SEARCH_PATH TO hiv_rpt, public;

DROP TABLE IF EXISTS hiv_rpt.r21_hiv_neg_labs;
DROP TABLE IF EXISTS hiv_rpt.r21_potential_pats;
DROP TABLE IF EXISTS hiv_rpt.r21_potential_pats_details;
DROP TABLE IF EXISTS hiv_rpt.r21_index_pats;

DROP TABLE IF EXISTS hiv_rpt.r21_week_intervals;

DROP TABLE IF EXISTS hiv_rpt.r21_hef_w_lab_details;

DROP TABLE IF EXISTS hiv_rpt.r21_gon_chlam_events;
DROP TABLE IF EXISTS hiv_rpt.r21_gon_chlam_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_syph_events;
DROP TABLE IF EXISTS hiv_rpt.r21_syph_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_sti_tst_episodes;
DROP TABLE IF EXISTS hiv_rpt.r21_gon_chlam_rectal_tst_episodes;
DROP TABLE IF EXISTS hiv_rpt.r21_gon_chlam_throat_tst_episodes;

DROP TABLE IF EXISTS hiv_rpt.r21_esp_cases;
DROP TABLE IF EXISTS hiv_rpt.r21_case_counts;
DROP TABLE IF EXISTS hiv_rpt.sti_case_encs;

DROP TABLE IF EXISTS hiv_rpt.r21_encounters;

DROP TABLE IF EXISTS hiv_rpt.r21_hiv_events;
DROP TABLE IF EXISTS hiv_rpt.r21_hiv_events_dist_by_date;
DROP TABLE IF EXISTS hiv_rpt.r21_hiv_counts_nohef;
DROP TABLE IF EXISTS hiv_rpt.r21_hiv_counts_hef;
DROP TABLE IF EXISTS hiv_rpt.r21_hiv_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_hep_events;
DROP TABLE IF EXISTS hiv_rpt.r21_hep_events_dist_by_date;
DROP TABLE IF EXISTS hiv_rpt.r21_hep_counts_nohef;
DROP TABLE IF EXISTS hiv_rpt.r21_hep_counts_hef;
DROP TABLE IF EXISTS hiv_rpt.r21_hep_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_hiv_meds;
DROP TABLE IF EXISTS hiv_rpt.r21_non_hiv_rx_of_interest;
DROP TABLE IF EXISTS hiv_rpt.r21_all_rx_of_interest;
DROP TABLE IF EXISTS hiv_rpt.r21_rx_of_int_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_dx_codes;
DROP TABLE IF EXISTS hiv_rpt.r21_dx_counts;

DROP TABLE IF EXISTS hiv_rpt.r21_bmi_measurements;
DROP TABLE IF EXISTS hiv_rpt.r21_bmi_counts;


DROP TABLE IF EXISTS hiv_rpt.r21_baseline_table;
DROP TABLE IF EXISTS hiv_rpt.r21_output_table;

DROP SEQUENCE IF EXISTS hiv_rpt.r21_masked_id_seq;
DROP TABLE IF EXISTS hiv_rpt.r21_masked_patients;
DROP TABLE IF EXISTS hiv_rpt.r21_output_table_masked;


-------------------
------------------- IDENTIFY INDEX PATIENTS
-------------------

	
-- Generate Starting Potential Patients List Based on specific HIV Negative Tests
-- Exclude Patients Who Have Had a Positive HIV test Prior to Study Period
-- Exclude Patients Who Have An ESP Case Prior to Study Period
-- FOR NOW LEAVE OFF HIV CASE EXCLUSION
CREATE TABLE hiv_rpt.r21_hiv_neg_labs AS
SELECT T1.patient_id, T1.name, T1.date as hiv_hef_neg_date
FROM hef_event T1
WHERE T1.date >= '01-01-2012'
AND T1.date < '01-01-2020'
AND T1.name in ('lx:hiv_elisa:negative',
                 'lx:hiv_ag_ab:negative',
				 'lx:hiv_wb:negative',
				 'lx:hiv_geenius:negative',
				 'lx:hiv_multispot:negative')
AND patient_id NOT IN (SELECT T1.patient_id
    FROM hef_event T1
    WHERE T1.name ilike 'lx:hiv%:positive'
    AND T1.date <= '01-01-2012')
	;
--AND patient_id NOT IN (SELECT T1.patient_id
--    FROM nodis_case T1
--	WHERE T1.condition = 'hiv'
--	AND T1.date <= '01-01-2012');

-- All individuals that are male and at least 15 on 12/31/2019 
-- Include HIV lab date (when the patient was 15 on lab test date) 
CREATE TABLE hiv_rpt.r21_potential_pats AS
SELECT T1.id as master_patient_id,
date_of_birth::date,
min(hiv_hef_neg_date) abs_index_date,
(date_of_birth + INTERVAL '15 years')::date date_turned_15
FROM emr_patient T1,
hiv_rpt.r21_hiv_neg_labs T2
WHERE T1.id = T2.patient_id
-- only include males
AND gender in ('M', 'MALE')
-- exclude patients that are not 15 on last day of reporting year
AND date_part('year', age('2019-12-31', date_of_birth)) >= 15
-- patient must be 15 on the date of the neg_hiv_test_date
AND (date_of_birth + INTERVAL '15 years')::date <= hiv_hef_neg_date
GROUP by T1.id;

-- -- Index Week, Index Patients & Demographics
-- -- Exclude Patients with a Positive HIV test prior or on the same date as first negative test during the study period
-- -- FOR NOW LEAVE THIS OFF: Exclude Patients with HIV on the problem list prior to or on the same date as first negative test 
-- CREATE TABLE hiv_rpt.r21_potential_pats_details AS
-- SELECT master_patient_id,
-- -- first day of week representing week 0
-- (date_trunc('week', abs_index_date))::date week_0,
-- -- age on the last day of week 0
-- date_part('year', age((date_trunc('week', abs_index_date) + interval '1 week' - interval '1 day')::date, T2.date_of_birth)) age_week_0,
-- gender as sex,
-- race,
-- ethnicity,
-- home_language,
-- T3.female_partner_yn,
-- T3.male_partner_yn,
-- date_of_death
-- FROM hiv_rpt.r21_potential_pats T1
-- INNER JOIN emr_patient T2 on (T1.master_patient_id = T2.id)
-- --FOR ATRIUS ONLY
-- --LEFT JOIN kre_report.atrius_soc_history T3 ON (T3.pat_id = T2.natural_key)
-- LEFT JOIN hiv_rpt.static_soc_history T3 ON (T3.pat_id = T2.natural_key)
-- LEFT JOIN (SELECT patient_id, min(date) pos_test_date from hef_event where name ilike 'lx:hiv%positive' group by patient_id) T4 on (T4.patient_id = T1.master_patient_id) 
-- --LEFT JOIN (SELECT patient_id, min(date) hiv_prob_date from emr_problem where date <= '01-01-2012' and dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B97.35|icd10:Z21)' and status not in ('Deleted') group by patient_id) T5 on (T5.patient_id = T1.master_patient_id)
-- WHERE pos_test_date is null or abs_index_date < pos_test_date;
-- --AND hiv_prob_date is null or abs_index_date < hiv_prob_date;


CREATE TABLE hiv_rpt.r21_potential_pats_details AS
SELECT DISTINCT 

-- THIS IS FOR FENWAY ONLY -- COMMENT OUT AT OTHER SITES
-- FENWAY START
CASE WHEN upper(T2.religion) in ('SOMETHING ELSE') then 'SOMETHING ELSE'
	 WHEN upper(T2.religion) in ('BISEXUAL') then 'BISEXUAL'
	 WHEN upper(T2.religion) in ('LESBIAN, GAY, OR HOMOSEXUAL') then 'LESBIAN OR GAY'
	 WHEN upper(T2.religion) in ('DON''T KNOW') then 'DON’T KNOW'
	 WHEN upper(T2.religion) in ('NOT REPORTED') then 'CHOOSE NOT TO DISCLOSE'
	 WHEN upper(T2.religion) in ('STRAIGHT OR HETEROSEXUAL') then 'STRAIGHT (NOT LESBIAN OR GAY)'
	 ELSE 'UNKNOWN' 
	 END AS sexual_orientation,
-- FENWAY END	 
 
master_patient_id,
-- first day of week representing week 0
(date_trunc('week', abs_index_date))::date week_0,
-- age on the last day of week 0
date_part('year', age((date_trunc('week', abs_index_date) + interval '1 week' - interval '1 day')::date, T2.date_of_birth)) age_week_0,
gender as sex,
race,
ethnicity,
home_language,
case when sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' end female_partner_yn,
case when sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' end male_partner_yn,
date_of_death
FROM hiv_rpt.r21_potential_pats T1
INNER JOIN emr_patient T2 on (T1.master_patient_id = T2.id)
LEFT JOIN ((select max(date) sochist_date, patient_id
	from emr_socialhistory T1,
	hiv_rpt.r21_potential_pats T2
	where T1.patient_id = T2.master_patient_id
	and date <= '01-01-2021'
	and sex_partner_gender is not null
	-- sex_partner data can be from week 0 or before
	and date <= (date_trunc('week', abs_index_date))::date + INTERVAL '7 days'
	GROUP BY patient_id) ) T3 on (T3.patient_id = T1.master_patient_id)
LEFT JOIN (SELECT patient_id, min(date) pos_test_date from hef_event where name ilike 'lx:hiv%positive' group by patient_id) T4 on (T4.patient_id = T1.master_patient_id) 
--LEFT JOIN (SELECT patient_id, min(date) hiv_prob_date from emr_problem where date <= '01-01-2012' and dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B97.35|icd10:Z21)' and status not in ('Deleted') group by patient_id) T5 on (T5.patient_id = T1.master_patient_id)
-- If 2 socialhistory records on the same date just return one of them 
LEFT JOIN (select patient_id, max(sex_partner_gender) as sex_partner_gender, date from emr_socialhistory where sex_partner_gender is not null GROUP BY patient_id, date) T5 
               on (T3.patient_id = T5.patient_id and T1.master_patient_id = T5.patient_id and T3.sochist_date = T5.date)
WHERE (pos_test_date is null or abs_index_date < pos_test_date);
--AND hiv_prob_date is null or abs_index_date < hiv_prob_date;



-- Index Patient List with Details
-- Patient Death Ends Follow-Up
-- The week of first HIV case ends the follow_up
-- Need to eliminate patients that had a HIV case before week_0 (but after study period start)
-- Need to get end_of_followup_week for all other index patients
-- FOR NOW LEAVE OFF HIV CASE EXCLUSION -- MAINTAING AS A PLACEHOLDER FOR FOLLOW-UP END DATA IF WE HAVE IT
CREATE TABLE hiv_rpt.r21_index_pats AS
SELECT T1.*,
-- first day of week representing end of follow_up
case when date_of_death is not null then (date_trunc('week', date_of_death))::date else null END as end_of_followup_week
--null::date as end_of_followup_week
FROM hiv_rpt.r21_potential_pats_details T1
;
--LEFT JOIN (SELECT date, patient_id from nodis_case where condition = 'hiv') T2 on T1.master_patient_id = T2.patient_id
--WHERE date is null or (date_trunc('week', date))::date > T1.week_0;

-------------------
------------------- CREATE FOLLOW-UP TIME INTERVALS
-------------------
CREATE TABLE hiv_rpt.r21_week_intervals AS
SELECT T2 week_id, T1.master_patient_id,week_0, end_of_followup_week,
case when T2 = 0 then week_0::date
when T2 = 1 and ((week_0 + interval '1 week') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '1 week')::date
when T2 = 2 and ((week_0 + interval '2 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '2 weeks'):: date
when T2 = 3 and ((week_0 + interval '3 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '3 weeks'):: date
when T2 = 4 and ((week_0 + interval '4 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '4 weeks'):: date
when T2 = 5 and ((week_0 + interval '5 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '5 weeks'):: date
when T2 = 6 and ((week_0 + interval '6 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '6 weeks'):: date
when T2 = 7 and ((week_0 + interval '7 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '7 weeks'):: date
when T2 = 8 and ((week_0 + interval '8 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '8 weeks'):: date
when T2 = 9 and ((week_0 + interval '9 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '9 weeks'):: date
when T2 = 10 and ((week_0 + interval '10 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '10 weeks'):: date
when T2 = 11 and ((week_0 + interval '11 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '11 weeks'):: date
when T2 = 12 and ((week_0 + interval '12 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '12 weeks'):: date
when T2 = 13 and ((week_0 + interval '13 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '13 weeks'):: date
when T2 = 14 and ((week_0 + interval '14 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '14 weeks'):: date
when T2 = 15 and ((week_0 + interval '15 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '15 weeks'):: date
when T2 = 16 and ((week_0 + interval '16 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '16 weeks'):: date
when T2 = 17 and ((week_0 + interval '17 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '17 weeks'):: date
when T2 = 18 and ((week_0 + interval '18 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '18 weeks'):: date
when T2 = 19 and ((week_0 + interval '19 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '19 weeks'):: date
when T2 = 20 and ((week_0 + interval '20 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '20 weeks'):: date
when T2 = 21 and ((week_0 + interval '21 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '21 weeks'):: date
when T2 = 22 and ((week_0 + interval '22 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '22 weeks'):: date
when T2 = 23 and ((week_0 + interval '23 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '23 weeks'):: date
when T2 = 24 and ((week_0 + interval '24 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '24 weeks'):: date
when T2 = 25 and ((week_0 + interval '25 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '25 weeks'):: date
when T2 = 26 and ((week_0 + interval '26 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '26 weeks'):: date
when T2 = 27 and ((week_0 + interval '27 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '27 weeks'):: date
when T2 = 28 and ((week_0 + interval '28 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '28 weeks'):: date
when T2 = 29 and ((week_0 + interval '29 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '29 weeks'):: date
when T2 = 30 and ((week_0 + interval '30 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '30 weeks'):: date
when T2 = 31 and ((week_0 + interval '31 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '31 weeks'):: date
when T2 = 32 and ((week_0 + interval '32 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '32 weeks'):: date
when T2 = 33 and ((week_0 + interval '33 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '33 weeks'):: date
when T2 = 34 and ((week_0 + interval '34 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '34 weeks'):: date
when T2 = 35 and ((week_0 + interval '35 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '34 weeks'):: date
when T2 = 36 and ((week_0 + interval '36 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '36 weeks'):: date
when T2 = 37 and ((week_0 + interval '37 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '37 weeks'):: date
when T2 = 38 and ((week_0 + interval '38 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '38 weeks'):: date
when T2 = 39 and ((week_0 + interval '39 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '39 weeks'):: date
when T2 = 40 and ((week_0 + interval '40 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '40 weeks'):: date
when T2 = 41 and ((week_0 + interval '41 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '41 weeks'):: date
when T2 = 42 and ((week_0 + interval '42 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '42 weeks'):: date
when T2 = 43 and ((week_0 + interval '43 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '43 weeks'):: date
when T2 = 44 and ((week_0 + interval '44 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '44 weeks'):: date
when T2 = 45 and ((week_0 + interval '45 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '45 weeks'):: date
when T2 = 46 and ((week_0 + interval '46 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '46 weeks'):: date
when T2 = 47 and ((week_0 + interval '47 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '47 weeks'):: date
when T2 = 48 and ((week_0 + interval '48 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '48 weeks'):: date
when T2 = 49 and ((week_0 + interval '49 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '49 weeks'):: date
when T2 = 50 and ((week_0 + interval '50 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '50 weeks'):: date
when T2 = 51 and ((week_0 + interval '51 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '51 weeks'):: date
when T2 = 52 and ((week_0 + interval '52 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '52 weeks'):: date
when T2 = 53 and ((week_0 + interval '53 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '53 weeks'):: date
when T2 = 54 and ((week_0 + interval '54 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '54 weeks'):: date
when T2 = 55 and ((week_0 + interval '55 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '55 weeks'):: date
when T2 = 56 and ((week_0 + interval '56 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '56 weeks'):: date
when T2 = 57 and ((week_0 + interval '57 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '57 weeks'):: date
when T2 = 58 and ((week_0 + interval '58 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '58 weeks'):: date
when T2 = 59 and ((week_0 + interval '59 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '59 weeks'):: date
when T2 = 60 and ((week_0 + interval '60 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '60 weeks'):: date
when T2 = 61 and ((week_0 + interval '61 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '61 weeks'):: date
when T2 = 62 and ((week_0 + interval '62 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '62 weeks'):: date
when T2 = 63 and ((week_0 + interval '63 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '63 weeks'):: date
when T2 = 64 and ((week_0 + interval '64 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '64 weeks'):: date
when T2 = 65 and ((week_0 + interval '65 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '65 weeks'):: date
when T2 = 66 and ((week_0 + interval '66 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '66 weeks'):: date
when T2 = 67 and ((week_0 + interval '67 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '67 weeks'):: date
when T2 = 68 and ((week_0 + interval '68 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '68 weeks'):: date
when T2 = 69 and ((week_0 + interval '69 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '69 weeks'):: date
when T2 = 70 and ((week_0 + interval '70 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '70 weeks'):: date
when T2 = 71 and ((week_0 + interval '71 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '71 weeks'):: date
when T2 = 72 and ((week_0 + interval '72 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '72 weeks'):: date
when T2 = 73 and ((week_0 + interval '73 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '73 weeks'):: date
when T2 = 74 and ((week_0 + interval '74 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '74 weeks'):: date
when T2 = 75 and ((week_0 + interval '75 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '75 weeks'):: date
when T2 = 76 and ((week_0 + interval '76 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '76 weeks'):: date
when T2 = 77 and ((week_0 + interval '77 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '77 weeks'):: date
when T2 = 78 and ((week_0 + interval '78 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '78 weeks'):: date
when T2 = 79 and ((week_0 + interval '79 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '79 weeks'):: date
when T2 = 80 and ((week_0 + interval '80 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '80 weeks'):: date
when T2 = 81 and ((week_0 + interval '81 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '81 weeks'):: date
when T2 = 82 and ((week_0 + interval '82 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '82 weeks'):: date
when T2 = 83 and ((week_0 + interval '83 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '83 weeks'):: date
when T2 = 84 and ((week_0 + interval '84 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '84 weeks'):: date
when T2 = 85 and ((week_0 + interval '85 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '85 weeks'):: date
when T2 = 86 and ((week_0 + interval '86 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '86 weeks'):: date
when T2 = 87 and ((week_0 + interval '87 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '87 weeks'):: date
when T2 = 88 and ((week_0 + interval '88 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '88 weeks'):: date
when T2 = 89 and ((week_0 + interval '89 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '89 weeks'):: date
when T2 = 90 and ((week_0 + interval '90 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '90 weeks'):: date
when T2 = 91 and ((week_0 + interval '91 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '91 weeks'):: date
when T2 = 92 and ((week_0 + interval '92 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '92 weeks'):: date
when T2 = 93 and ((week_0 + interval '93 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '93 weeks'):: date
when T2 = 94 and ((week_0 + interval '94 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '94 weeks'):: date
when T2 = 95 and ((week_0 + interval '95 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '95 weeks'):: date
when T2 = 96 and ((week_0 + interval '96 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '96 weeks'):: date
when T2 = 97 and ((week_0 + interval '97 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '97 weeks'):: date
when T2 = 98 and ((week_0 + interval '98 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '98 weeks'):: date
when T2 = 99 and ((week_0 + interval '99 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '99 weeks'):: date
when T2 = 100 and ((week_0 + interval '100 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '100 weeks'):: date
when T2 = 101 and ((week_0 + interval '101 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '101 weeks'):: date
when T2 = 102 and ((week_0 + interval '102 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '102 weeks'):: date
when T2 = 103 and ((week_0 + interval '103 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '103 weeks'):: date
when T2 = 104 and ((week_0 + interval '104 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '104 weeks')::date
when T2 = 105 and ((week_0 + interval '105 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '105 weeks')::date
when T2 = 106 and ((week_0 + interval '106 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '106 weeks')::date
when T2 = 107 and ((week_0 + interval '107 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '107 weeks')::date
when T2 = 108 and ((week_0 + interval '108 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '108 weeks')::date
when T2 = 109 and ((week_0 + interval '109 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '109 weeks')::date
when T2 = 110 and ((week_0 + interval '110 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '110 weeks')::date
when T2 = 111 and ((week_0 + interval '111 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '111 weeks')::date
when T2 = 112 and ((week_0 + interval '112 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '112 weeks')::date
when T2 = 113 and ((week_0 + interval '113 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '113 weeks')::date
when T2 = 114 and ((week_0 + interval '114 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '114 weeks')::date
when T2 = 115 and ((week_0 + interval '115 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '115 weeks')::date
when T2 = 116 and ((week_0 + interval '116 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '116 weeks')::date
when T2 = 117 and ((week_0 + interval '117 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '117 weeks')::date
when T2 = 118 and ((week_0 + interval '118 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '118 weeks')::date
when T2 = 119 and ((week_0 + interval '119 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '119 weeks')::date
when T2 = 120 and ((week_0 + interval '120 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '120 weeks')::date
when T2 = 121 and ((week_0 + interval '121 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '121 weeks')::date
when T2 = 122 and ((week_0 + interval '122 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '122 weeks')::date
when T2 = 123 and ((week_0 + interval '123 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '123 weeks')::date
when T2 = 124 and ((week_0 + interval '124 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '124 weeks')::date
when T2 = 125 and ((week_0 + interval '125 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '125 weeks')::date
when T2 = 126 and ((week_0 + interval '126 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '126 weeks')::date
when T2 = 127 and ((week_0 + interval '127 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '127 weeks')::date
when T2 = 128 and ((week_0 + interval '128 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '128 weeks')::date
when T2 = 129 and ((week_0 + interval '129 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '129 weeks')::date
when T2 = 130 and ((week_0 + interval '130 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '130 weeks')::date
when T2 = 131 and ((week_0 + interval '131 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '131 weeks')::date
when T2 = 132 and ((week_0 + interval '132 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '132 weeks')::date
when T2 = 133 and ((week_0 + interval '133 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '133 weeks')::date
when T2 = 134 and ((week_0 + interval '134 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '134 weeks')::date
when T2 = 135 and ((week_0 + interval '135 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '135 weeks')::date
when T2 = 136 and ((week_0 + interval '136 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '136 weeks')::date
when T2 = 137 and ((week_0 + interval '137 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '137 weeks')::date
when T2 = 138 and ((week_0 + interval '138 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '138 weeks')::date
when T2 = 139 and ((week_0 + interval '139 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '139 weeks')::date
when T2 = 140 and ((week_0 + interval '140 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '140 weeks')::date
when T2 = 141 and ((week_0 + interval '141 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '141 weeks')::date
when T2 = 142 and ((week_0 + interval '142 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '142 weeks')::date
when T2 = 143 and ((week_0 + interval '143 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '143 weeks')::date
when T2 = 144 and ((week_0 + interval '144 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '144 weeks')::date
when T2 = 145 and ((week_0 + interval '145 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '145 weeks')::date
when T2 = 146 and ((week_0 + interval '146 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '146 weeks')::date
when T2 = 147 and ((week_0 + interval '147 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '147 weeks')::date
when T2 = 148 and ((week_0 + interval '148 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '148 weeks')::date
when T2 = 149 and ((week_0 + interval '149 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '149 weeks')::date
when T2 = 150 and ((week_0 + interval '150 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '150 weeks')::date
when T2 = 151 and ((week_0 + interval '151 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '151 weeks')::date
when T2 = 152 and ((week_0 + interval '152 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '152 weeks')::date
when T2 = 153 and ((week_0 + interval '153 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '153 weeks')::date
when T2 = 154 and ((week_0 + interval '154 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '154 weeks')::date
when T2 = 155 and ((week_0 + interval '155 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '155 weeks')::date
when T2 = 156 and ((week_0 + interval '156 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '156 weeks')::date
when T2 = 157 and ((week_0 + interval '157 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '157 weeks')::date
when T2 = 158 and ((week_0 + interval '158 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '158 weeks')::date
when T2 = 159 and ((week_0 + interval '159 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '159 weeks')::date
when T2 = 160 and ((week_0 + interval '160 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '160 weeks')::date
when T2 = 161 and ((week_0 + interval '161 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '161 weeks')::date
when T2 = 162 and ((week_0 + interval '162 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '162 weeks')::date
when T2 = 163 and ((week_0 + interval '163 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '163 weeks')::date
when T2 = 164 and ((week_0 + interval '164 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '164 weeks')::date
when T2 = 165 and ((week_0 + interval '165 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '165 weeks')::date
when T2 = 166 and ((week_0 + interval '166 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '166 weeks')::date
when T2 = 167 and ((week_0 + interval '167 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '167 weeks')::date
when T2 = 168 and ((week_0 + interval '168 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '168 weeks')::date
when T2 = 169 and ((week_0 + interval '169 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '169 weeks')::date
when T2 = 170 and ((week_0 + interval '170 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '170 weeks')::date
when T2 = 171 and ((week_0 + interval '171 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '171 weeks')::date
when T2 = 172 and ((week_0 + interval '172 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '172 weeks')::date
when T2 = 173 and ((week_0 + interval '173 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '173 weeks')::date
when T2 = 174 and ((week_0 + interval '174 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '174 weeks')::date
when T2 = 175 and ((week_0 + interval '175 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '175 weeks')::date
when T2 = 176 and ((week_0 + interval '176 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '176 weeks')::date
when T2 = 177 and ((week_0 + interval '177 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '177 weeks')::date
when T2 = 178 and ((week_0 + interval '178 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '178 weeks')::date
when T2 = 179 and ((week_0 + interval '179 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '179 weeks')::date
when T2 = 180 and ((week_0 + interval '180 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '180 weeks')::date
when T2 = 181 and ((week_0 + interval '181 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '181 weeks')::date
when T2 = 182 and ((week_0 + interval '182 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '182 weeks')::date
when T2 = 183 and ((week_0 + interval '183 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '183 weeks')::date
when T2 = 184 and ((week_0 + interval '184 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '184 weeks')::date
when T2 = 185 and ((week_0 + interval '185 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '185 weeks')::date
when T2 = 186 and ((week_0 + interval '186 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '186 weeks')::date
when T2 = 187 and ((week_0 + interval '187 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '187 weeks')::date
when T2 = 188 and ((week_0 + interval '188 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '188 weeks')::date
when T2 = 189 and ((week_0 + interval '189 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '189 weeks')::date
when T2 = 190 and ((week_0 + interval '190 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '190 weeks')::date
when T2 = 191 and ((week_0 + interval '191 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '191 weeks')::date
when T2 = 192 and ((week_0 + interval '192 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '192 weeks')::date
when T2 = 193 and ((week_0 + interval '193 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '193 weeks')::date
when T2 = 194 and ((week_0 + interval '194 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '194 weeks')::date
when T2 = 195 and ((week_0 + interval '195 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '195 weeks')::date
when T2 = 196 and ((week_0 + interval '196 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '196 weeks')::date
when T2 = 197 and ((week_0 + interval '197 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '197 weeks')::date
when T2 = 198 and ((week_0 + interval '198 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '198 weeks')::date
when T2 = 199 and ((week_0 + interval '199 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '199 weeks')::date
when T2 = 200 and ((week_0 + interval '200 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '200 weeks')::date
when T2 = 201 and ((week_0 + interval '201 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '201 weeks')::date
when T2 = 202 and ((week_0 + interval '202 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '202 weeks')::date
when T2 = 203 and ((week_0 + interval '203 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '203 weeks')::date
when T2 = 204 and ((week_0 + interval '204 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '204 weeks')::date
when T2 = 205 and ((week_0 + interval '205 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '205 weeks')::date
when T2 = 206 and ((week_0 + interval '206 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '206 weeks')::date
when T2 = 207 and ((week_0 + interval '207 weeks') <= end_of_followup_week or end_of_followup_week is null) then (week_0 + interval '207 weeks')::date
else null end week_start,
case when T2 = 0 then (week_0 + interval '1 week')::date
when T2 = 1 then (week_0 + interval '2 weeks')::date
when T2 = 2 then (week_0 + interval '3 weeks'):: date
when T2 = 3 then (week_0 + interval '4 weeks'):: date
when T2 = 4 then (week_0 + interval '5 weeks'):: date
when T2 = 5 then (week_0 + interval '6 weeks'):: date
when T2 = 6 then (week_0 + interval '7 weeks'):: date
when T2 = 7 then (week_0 + interval '8 weeks'):: date
when T2 = 8 then (week_0 + interval '9 weeks'):: date
when T2 = 9 then (week_0 + interval '10 weeks'):: date
when T2 = 10 then (week_0 + interval '11 weeks'):: date
when T2 = 11 then (week_0 + interval '12 weeks'):: date
when T2 = 12 then (week_0 + interval '13 weeks'):: date
when T2 = 13 then (week_0 + interval '14 weeks'):: date
when T2 = 14 then (week_0 + interval '15 weeks'):: date
when T2 = 15 then (week_0 + interval '16 weeks'):: date
when T2 = 16 then (week_0 + interval '17 weeks'):: date
when T2 = 17 then (week_0 + interval '18 weeks'):: date
when T2 = 18 then (week_0 + interval '19 weeks'):: date
when T2 = 19 then (week_0 + interval '20 weeks'):: date
when T2 = 20 then (week_0 + interval '21 weeks'):: date
when T2 = 21 then (week_0 + interval '22 weeks'):: date
when T2 = 22 then (week_0 + interval '23 weeks'):: date
when T2 = 23 then (week_0 + interval '24 weeks'):: date
when T2 = 24 then (week_0 + interval '25 weeks'):: date
when T2 = 25 then (week_0 + interval '26 weeks'):: date
when T2 = 26 then (week_0 + interval '27 weeks'):: date
when T2 = 27 then (week_0 + interval '28 weeks'):: date
when T2 = 28 then (week_0 + interval '29 weeks'):: date
when T2 = 29 then (week_0 + interval '30 weeks'):: date
when T2 = 30 then (week_0 + interval '31 weeks'):: date
when T2 = 31 then (week_0 + interval '32 weeks'):: date
when T2 = 32 then (week_0 + interval '33 weeks'):: date
when T2 = 33 then (week_0 + interval '34 weeks'):: date
when T2 = 34 then (week_0 + interval '35 weeks'):: date
when T2 = 35 then (week_0 + interval '36 weeks'):: date
when T2 = 36 then (week_0 + interval '37 weeks'):: date
when T2 = 37 then (week_0 + interval '38 weeks'):: date
when T2 = 38 then (week_0 + interval '39 weeks'):: date
when T2 = 39 then (week_0 + interval '40 weeks'):: date
when T2 = 40 then (week_0 + interval '41 weeks'):: date
when T2 = 41 then (week_0 + interval '42 weeks'):: date
when T2 = 42 then (week_0 + interval '43 weeks'):: date
when T2 = 43 then (week_0 + interval '44 weeks'):: date
when T2 = 44 then (week_0 + interval '45 weeks'):: date
when T2 = 45 then (week_0 + interval '46 weeks'):: date
when T2 = 46 then (week_0 + interval '47 weeks'):: date
when T2 = 47 then (week_0 + interval '48 weeks'):: date
when T2 = 48 then (week_0 + interval '49 weeks'):: date
when T2 = 49 then (week_0 + interval '50 weeks'):: date
when T2 = 50 then (week_0 + interval '51 weeks'):: date
when T2 = 51 then (week_0 + interval '52 weeks'):: date
when T2 = 52 then (week_0 + interval '53 weeks'):: date
when T2 = 53 then (week_0 + interval '54 weeks'):: date
when T2 = 54 then (week_0 + interval '55 weeks'):: date
when T2 = 55 then (week_0 + interval '56 weeks'):: date
when T2 = 56 then (week_0 + interval '57 weeks'):: date
when T2 = 57 then (week_0 + interval '58 weeks'):: date
when T2 = 58 then (week_0 + interval '59 weeks'):: date
when T2 = 59 then (week_0 + interval '60 weeks'):: date
when T2 = 60 then (week_0 + interval '61 weeks'):: date
when T2 = 61 then (week_0 + interval '62 weeks'):: date
when T2 = 62 then (week_0 + interval '63 weeks'):: date
when T2 = 63 then (week_0 + interval '64 weeks'):: date
when T2 = 64 then (week_0 + interval '65 weeks'):: date
when T2 = 65 then (week_0 + interval '66 weeks'):: date
when T2 = 66 then (week_0 + interval '67 weeks'):: date
when T2 = 67 then (week_0 + interval '68 weeks'):: date
when T2 = 68 then (week_0 + interval '69 weeks'):: date
when T2 = 69 then (week_0 + interval '70 weeks'):: date
when T2 = 70 then (week_0 + interval '71 weeks'):: date
when T2 = 71 then (week_0 + interval '72 weeks'):: date
when T2 = 72 then (week_0 + interval '73 weeks'):: date
when T2 = 73 then (week_0 + interval '74 weeks'):: date
when T2 = 74 then (week_0 + interval '75 weeks'):: date
when T2 = 75 then (week_0 + interval '76 weeks'):: date
when T2 = 76 then (week_0 + interval '77 weeks'):: date
when T2 = 77 then (week_0 + interval '78 weeks'):: date
when T2 = 78 then (week_0 + interval '79 weeks'):: date
when T2 = 79 then (week_0 + interval '80 weeks'):: date
when T2 = 80 then (week_0 + interval '81 weeks'):: date
when T2 = 81 then (week_0 + interval '82 weeks'):: date
when T2 = 82 then (week_0 + interval '83 weeks'):: date
when T2 = 83 then (week_0 + interval '84 weeks'):: date
when T2 = 84 then (week_0 + interval '85 weeks'):: date
when T2 = 85 then (week_0 + interval '86 weeks'):: date
when T2 = 86 then (week_0 + interval '87 weeks'):: date
when T2 = 87 then (week_0 + interval '88 weeks'):: date
when T2 = 88 then (week_0 + interval '89 weeks'):: date
when T2 = 89 then (week_0 + interval '90 weeks'):: date
when T2 = 90 then (week_0 + interval '91 weeks'):: date
when T2 = 91 then (week_0 + interval '92 weeks'):: date
when T2 = 92 then (week_0 + interval '93 weeks'):: date
when T2 = 93 then (week_0 + interval '94 weeks'):: date
when T2 = 94 then (week_0 + interval '95 weeks'):: date
when T2 = 95 then (week_0 + interval '96 weeks'):: date
when T2 = 96 then (week_0 + interval '97 weeks'):: date
when T2 = 97 then (week_0 + interval '98 weeks'):: date
when T2 = 98 then (week_0 + interval '99 weeks'):: date
when T2 = 99 then (week_0 + interval '100 weeks'):: date
when T2 = 100 then (week_0 + interval '101 weeks'):: date
when T2 = 101 then (week_0 + interval '102 weeks'):: date
when T2 = 102 then (week_0 + interval '103 weeks'):: date
when T2 = 103 then (week_0 + interval '104 weeks'):: date
when T2 = 104 then (week_0 + interval '105 weeks')::date
when T2 = 105 then (week_0 + interval '106 weeks')::date
when T2 = 106 then (week_0 + interval '107 weeks')::date
when T2 = 107 then (week_0 + interval '108 weeks')::date
when T2 = 108 then (week_0 + interval '109 weeks')::date
when T2 = 109 then (week_0 + interval '110 weeks')::date
when T2 = 110 then (week_0 + interval '111 weeks')::date
when T2 = 111 then (week_0 + interval '112 weeks')::date
when T2 = 112 then (week_0 + interval '113 weeks')::date
when T2 = 113 then (week_0 + interval '114 weeks')::date
when T2 = 114 then (week_0 + interval '115 weeks')::date
when T2 = 115 then (week_0 + interval '116 weeks')::date
when T2 = 116 then (week_0 + interval '117 weeks')::date
when T2 = 117 then (week_0 + interval '118 weeks')::date
when T2 = 118 then (week_0 + interval '119 weeks')::date
when T2 = 119 then (week_0 + interval '120 weeks')::date
when T2 = 120 then (week_0 + interval '121 weeks')::date
when T2 = 121 then (week_0 + interval '122 weeks')::date
when T2 = 122 then (week_0 + interval '123 weeks')::date
when T2 = 123 then (week_0 + interval '124 weeks')::date
when T2 = 124 then (week_0 + interval '125 weeks')::date
when T2 = 125 then (week_0 + interval '126 weeks')::date
when T2 = 126 then (week_0 + interval '127 weeks')::date
when T2 = 127 then (week_0 + interval '128 weeks')::date
when T2 = 128 then (week_0 + interval '129 weeks')::date
when T2 = 129 then (week_0 + interval '130 weeks')::date
when T2 = 130 then (week_0 + interval '131 weeks')::date
when T2 = 131 then (week_0 + interval '132 weeks')::date
when T2 = 132 then (week_0 + interval '133 weeks')::date
when T2 = 133 then (week_0 + interval '134 weeks')::date
when T2 = 134 then (week_0 + interval '135 weeks')::date
when T2 = 135 then (week_0 + interval '136 weeks')::date
when T2 = 136 then (week_0 + interval '137 weeks')::date
when T2 = 137 then (week_0 + interval '138 weeks')::date
when T2 = 138 then (week_0 + interval '139 weeks')::date
when T2 = 139 then (week_0 + interval '140 weeks')::date
when T2 = 140 then (week_0 + interval '141 weeks')::date
when T2 = 141 then (week_0 + interval '142 weeks')::date
when T2 = 142 then (week_0 + interval '143 weeks')::date
when T2 = 143 then (week_0 + interval '144 weeks')::date
when T2 = 144 then (week_0 + interval '145 weeks')::date
when T2 = 145 then (week_0 + interval '146 weeks')::date
when T2 = 146 then (week_0 + interval '147 weeks')::date
when T2 = 147 then (week_0 + interval '148 weeks')::date
when T2 = 148 then (week_0 + interval '149 weeks')::date
when T2 = 149 then (week_0 + interval '150 weeks')::date
when T2 = 150 then (week_0 + interval '151 weeks')::date
when T2 = 151 then (week_0 + interval '152 weeks')::date
when T2 = 152 then (week_0 + interval '153 weeks')::date
when T2 = 153 then (week_0 + interval '154 weeks')::date
when T2 = 154 then (week_0 + interval '155 weeks')::date
when T2 = 155 then (week_0 + interval '156 weeks')::date
when T2 = 156 then (week_0 + interval '157 weeks')::date
when T2 = 157 then (week_0 + interval '158 weeks')::date
when T2 = 158 then (week_0 + interval '159 weeks')::date
when T2 = 159 then (week_0 + interval '160 weeks')::date
when T2 = 160 then (week_0 + interval '161 weeks')::date
when T2 = 161 then (week_0 + interval '162 weeks')::date
when T2 = 162 then (week_0 + interval '163 weeks')::date
when T2 = 163 then (week_0 + interval '164 weeks')::date
when T2 = 164 then (week_0 + interval '165 weeks')::date
when T2 = 165 then (week_0 + interval '166 weeks')::date
when T2 = 166 then (week_0 + interval '167 weeks')::date
when T2 = 167 then (week_0 + interval '168 weeks')::date
when T2 = 168 then (week_0 + interval '169 weeks')::date
when T2 = 169 then (week_0 + interval '170 weeks')::date
when T2 = 170 then (week_0 + interval '171 weeks')::date
when T2 = 171 then (week_0 + interval '172 weeks')::date
when T2 = 172 then (week_0 + interval '173 weeks')::date
when T2 = 173 then (week_0 + interval '174 weeks')::date
when T2 = 174 then (week_0 + interval '175 weeks')::date
when T2 = 175 then (week_0 + interval '176 weeks')::date
when T2 = 176 then (week_0 + interval '177 weeks')::date
when T2 = 177 then (week_0 + interval '178 weeks')::date
when T2 = 178 then (week_0 + interval '179 weeks')::date
when T2 = 179 then (week_0 + interval '180 weeks')::date
when T2 = 180 then (week_0 + interval '181 weeks')::date
when T2 = 181 then (week_0 + interval '182 weeks')::date
when T2 = 182 then (week_0 + interval '183 weeks')::date
when T2 = 183 then (week_0 + interval '184 weeks')::date
when T2 = 184 then (week_0 + interval '185 weeks')::date
when T2 = 185 then (week_0 + interval '186 weeks')::date
when T2 = 186 then (week_0 + interval '187 weeks')::date
when T2 = 187 then (week_0 + interval '188 weeks')::date
when T2 = 188 then (week_0 + interval '189 weeks')::date
when T2 = 189 then (week_0 + interval '190 weeks')::date
when T2 = 190 then (week_0 + interval '191 weeks')::date
when T2 = 191 then (week_0 + interval '192 weeks')::date
when T2 = 192 then (week_0 + interval '193 weeks')::date
when T2 = 193 then (week_0 + interval '194 weeks')::date
when T2 = 194 then (week_0 + interval '195 weeks')::date
when T2 = 195 then (week_0 + interval '196 weeks')::date
when T2 = 196 then (week_0 + interval '197 weeks')::date
when T2 = 197 then (week_0 + interval '198 weeks')::date
when T2 = 198 then (week_0 + interval '199 weeks')::date
when T2 = 199 then (week_0 + interval '200 weeks')::date
when T2 = 200 then (week_0 + interval '201 weeks')::date
when T2 = 201 then (week_0 + interval '202 weeks')::date
when T2 = 202 then (week_0 + interval '203 weeks')::date
when T2 = 203 then (week_0 + interval '204 weeks')::date
when T2 = 204 then (week_0 + interval '205 weeks')::date
when T2 = 205 then (week_0 + interval '206 weeks')::date
when T2 = 206 then (week_0 + interval '207 weeks')::date
else null end week_end
FROM generate_series(0,206) T2,
hiv_rpt.r21_index_pats T1;

-- IF FOLLOW-UP LESS THAN ONE WEEK THEN REMOVE ROWS
DELETE FROM hiv_rpt.r21_week_intervals where week_start is null;

-- LIMIT THE TIME PERIOD TO END AT 2021-01-01
DELETE FROM hiv_rpt.r21_week_intervals WHERE week_start >= '2021-01-01' OR week_end >= '2021-01-01';



-------------------
-------------------  LAB & HEF DATA
-------------------


-- All chlam/gon lab hef events for index patients
-- Start is 2006 to capture all hef events [needed for 1 demog variable for chlam/gon testing]
CREATE TABLE hiv_rpt.r21_hef_w_lab_details AS 
SELECT T1.id,T2.name,T2.patient_id,T2.date,T1.specimen_source,T2.object_id,T1.native_code, T1.native_name, T1.procedure_name
FROM emr_labresult T1
INNER JOIN hiv_rpt.r21_index_pats T3 on (T1.patient_id = T3.master_patient_id)
INNER JOIN hef_event T2 on (T1.patient_id = T2.patient_id and T3.master_patient_id = T2.patient_id and T1.id = T2.object_id)
INNER JOIN conf_labtestmap T4 on (T1.native_code = T4.native_code)
WHERE T2.date >= '01-01-2006'
AND T2.date < '01-01-2021'
AND upper(result_string) not in ('CANCELLED', 'TNP')
AND T4.test_name in ('chlamydia', 'gonorrhea');


-------------------
------------------- GONORRHEA & CHLAMYDIA
-------------------

-- GONORRHEA - Gather up gonorrhea events and normalize specimen sources
-- CHLAMYDIA - Gather up chlamydia events and normalize specimen sources
CREATE TABLE hiv_rpt.r21_gon_chlam_events AS
SELECT name, date, patient_id, object_id,
CASE WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(PHARYNGEAL|THROAT)' or native_code ~* '(PHARYNGEAL|THROAT)' or procedure_name ~* '(PHARYNGEAL|THROAT)')then 'THROAT'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(RECTAL|ANAL)' OR native_code ~* '(RECTAL|ANAL)' OR procedure_name ~* '(RECTAL|ANAL)') THEN 'RECTAL'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(GENITAL|URINE|URN|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)' OR native_code  ~* '(GENITAL|URINE|URN|URI|UR|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)' OR procedure_name  ~* '(GENITAL|URINE|URN|URI|UR|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)') THEN 'UROG'
WHEN specimen_source ~* '^(RECTUM|RECT|ANAL)' then 'RECTAL'
WHEN specimen_source ~* '^(THROAT)' then 'THROAT'
WHEN specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG|UREATHRA|URETHRA)' then 'UROG'
ELSE specimen_source END specimen_source,
specimen_source specimen_source_orig,
native_code
FROM hiv_rpt.r21_hef_w_lab_details T1
WHERE (name like 'lx:gonorrhea%' OR name like 'lx:chlamydia%');


CREATE TABLE hiv_rpt.r21_gon_chlam_counts as
select T1.patient_id, week_id, week_start,
-- find any chlam/gon test for rectal or throat ever before or during week 0
case when week_id = 0 then max(case when (name ilike 'lx:gonorrhea:%' or name ilike 'lx:chlamydia') and specimen_source in ('RECTAL', 'THROAT') and date <= week_end then 1 else 0 end) else null end prior_gon_chlam_rectal_throat_ever,
case when week_id = 0 then count(case when name = 'lx:gonorrhea:positive' and specimen_source = 'RECTAL' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_pos_gon_rectal,
case when week_id = 0 then count(case when name = 'lx:gonorrhea:positive' and specimen_source = 'THROAT' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_pos_gon_oropharynx,
case when week_id = 0 then count(case when name = 'lx:gonorrhea:positive' and specimen_source = 'UROG' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_pos_gon_urog,
max(case when name ilike 'lx:gonorrhea:%' and specimen_source = 'RECTAL' and date >= week_start and date < week_end then 1 end) as any_gon_rectal,
max(case when name = 'lx:gonorrhea:positive' and specimen_source = 'RECTAL' and date >= week_start and date < week_end then 1 end) as pos_gon_rectal,
max(case when name ilike 'lx:gonorrhea:%' and specimen_source = 'THROAT' and date >= week_start and date < week_end then 1 end) as any_gon_throat,
max(case when name = 'lx:gonorrhea:positive' and specimen_source = 'THROAT'  and date >= week_start and date < week_end then 1 end) as pos_gon_throat,
max(case when name ilike 'lx:gonorrhea:%' and specimen_source = 'UROG' and date >= week_start and date >= week_start and date < week_end then 1 end) as any_gon_urog,
max(case when name = 'lx:gonorrhea:positive' and specimen_source = 'UROG' and date >= week_start and date < week_end then 1 end) as pos_gon_urog,
case when week_id = 0 then count(case when name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_pos_chlam_rectal,
case when week_id = 0 then count(case when name = 'lx:chlamydia:positive' and specimen_source = 'THROAT' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_pos_chlam_oropharynx,
case when week_id = 0 then count(case when name = 'lx:chlamydia:positive' and specimen_source = 'UROG' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_pos_chlam_urog,
max(case when name ilike 'lx:chlamydia:%' and specimen_source = 'RECTAL' and date >= week_start and date < week_end then 1 end) as any_chlam_rectal,
max(case when name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' and date >= week_start and date < week_end then 1 end) as pos_chlam_rectal,
max(case when name ilike 'lx:chlamydia:%' and specimen_source = 'THROAT' and date >= week_start and date < week_end then 1 end) as any_chlam_throat,
max(case when name = 'lx:chlamydia:positive' and specimen_source = 'THROAT'  and date >= week_start and date < week_end then 1 end) as pos_chlam_throat,
max(case when name ilike 'lx:chlamydia:%' and specimen_source = 'UROG' and date >= week_start and date >= week_start and date < week_end then 1 end) as any_chlam_urog,
max(case when name = 'lx:chlamydia:positive' and specimen_source = 'UROG' and date >= week_start and date < week_end then 1 end) as pos_chlam_urog
FROM hiv_rpt.r21_gon_chlam_events T1,
hiv_rpt.r21_week_intervals T2
WHERE T1.patient_id = T2.master_patient_id
--AND T1.patient_id in (1688763, 38662886)
GROUP BY T1.patient_id, week_id, week_start
ORDER BY patient_id,week_id;


-------------------
------------------- SYPHILIS
-------------------

-- Gather up all syphilis related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
CREATE TABLE hiv_rpt.r21_syph_events AS 
SELECT T2.patient_id, T2.id, T3.test_name, T4.name as hef_name, T2.date, result_string 
FROM hiv_rpt.r21_index_pats T1
INNER JOIN emr_labresult T2 ON (T1.master_patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.master_patient_id = T4.patient_id))
WHERE T2.date >= '12-01-2010'
AND T2.date < '01-01-2021'
AND T3.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm', 'tp-cia')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined', 'Not Done', 'Not performed',  'Not tested', 'Not Tested', 'test', 'CANCELLED. DUPLICATE REQUEST.', 'REPORT')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|QUANTITY NOT SUFFICIENT|NOT PROCESSED')
and result_string is not null;

-- Any test for syphilis (per the ESP syphilis spec, syphilis tests include RPR, VDRL, TPPA, FTA-ABS, TP-IGM, TP-CIA and TP-IGG)
CREATE TABLE hiv_rpt.r21_syph_counts as
SELECT T1.patient_id, week_id, week_start,
max(case when date >= week_start and date < week_end then 1 end) as any_syph
FROM hiv_rpt.r21_syph_events T1,
hiv_rpt.r21_week_intervals T2
WHERE T1.patient_id = T2.master_patient_id
--AND T1.patient_id in (1688763, 38662886)
GROUP BY T1.patient_id, week_id, week_start
ORDER BY patient_id,week_id;



-------------------
------------------- STI TESTING EPISODES
-------------------

-- Number of gonorrhea, chlamydia, or syphilis testing episodes (any anatomic site; multiple tests on same date count as 1 testing episode)
CREATE TABLE hiv_rpt.r21_sti_tst_episodes AS 
SELECT patient_id, count(*) gon_chlam_syph_tst_episodes 
FROM (
	SELECT T2.master_patient_id as patient_id, date as testing_date
	FROM hiv_rpt.r21_syph_events T1,
	hiv_rpt.r21_week_intervals T2
	WHERE T1.patient_id = T2.master_patient_id
	AND week_id = 0
	AND date < week_start and date >= week_start - interval '12 months'
	--AND T1.master_patient_id in (1688763, 38662886, 9614)
	GROUP BY T2.master_patient_id, date
	UNION
	SELECT patient_id, date as testing_date
	FROM hiv_rpt.r21_gon_chlam_events T1,
	hiv_rpt.r21_week_intervals T2
	WHERE T1.patient_id = T2.master_patient_id
	AND week_id = 0
	AND date < week_start and date >= week_start - interval '12 months'
	--AND patient_id in (1688763, 38662886, 9614)
	GROUP BY patient_id, date
	ORDER BY patient_id, testing_date) T1
GROUP BY patient_id;

CREATE TABLE hiv_rpt.r21_gon_chlam_rectal_tst_episodes AS 
SELECT patient_id, count(*) gon_chlam_rectal_tst_episodes 
FROM (
SELECT T1.patient_id as patient_id, date as testing_date
	FROM hiv_rpt.r21_gon_chlam_events T1,
	hiv_rpt.r21_week_intervals T2
	WHERE T1.patient_id = T2.master_patient_id
	AND week_id = 0
	AND date < week_start and date >= week_start - interval '12 months'
	AND specimen_source = 'RECTAL'
	--AND T1.patient_id in (1688763, 38662886, 9614)
	GROUP BY T1.patient_id, date) T1
GROUP BY patient_id;

CREATE TABLE hiv_rpt.r21_gon_chlam_throat_tst_episodes AS 
SELECT patient_id, count(*) gon_chlam_orpharynx_tst_episodes 
FROM (
SELECT T1.patient_id as patient_id, date as testing_date
	FROM hiv_rpt.r21_gon_chlam_events T1,
	hiv_rpt.r21_week_intervals T2
	WHERE T1.patient_id = T2.master_patient_id
	AND week_id = 0
	AND date < week_start and date >= week_start - interval '12 months'
	AND specimen_source = 'THROAT'
	--AND T1.patient_id in (1688763, 38662886, 9614)
	GROUP BY T1.patient_id, date) T1
GROUP BY patient_id;



-------------------
------------------- ESP CASES
-------------------

-- All gon, chlam, syph, and hep_b_acute cases for index patients
-- Add in HepC Acute Cases
CREATE TABLE hiv_rpt.r21_esp_cases AS 
SELECT T1.patient_id, T1.condition, T1.date
FROM nodis_case T1,
hiv_rpt.r21_index_pats T2
WHERE T1.patient_id = T2.master_patient_id
AND condition in ('gonorrhea', 'chlamydia', 'syphilis', 'hepatitis_b:acute')
AND T1.date >= '01-01-2010'
AND T1.date < '01-01-2021'

UNION

-- CAN ONLY GO FOR Undetermined to Acute (or Chronic) so we just need all Hep C Acute Cases
SELECT T1.patient_id, 'hepatitis_c:acute' as condition, T1.date
FROM nodis_case T1, 
nodis_caseactivehistory T2,
hiv_rpt.r21_index_pats T3
WHERE T1.condition = 'hepatitis_c'
AND T1.date >= '01-01-2010'
AND T1.date < '01-01-2021' 
and T2.status = 'HEP_C-A'
AND T1.id = T2.case_id 
AND T1. patient_id = T3.master_patient_id;


CREATE TABLE hiv_rpt.r21_case_counts as
SELECT T1.patient_id, week_id, week_start,
case when week_id = 0 then count(case when condition = 'gonorrhea' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_gon_cases,
case when week_id = 0 then count(case when condition = 'chlamydia' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_chlam_cases,
case when week_id = 0 then count(case when condition = 'syphilis' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_syph_cases,
case when week_id = 0 then count(case when condition = 'hepatitis_b:acute' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hep_b_acute_case,
case when week_id = 0 then count(case when condition = 'hepatitis_c:acute' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hep_c_acute_case,
max(case when condition = 'syphilis' and date >= week_start and date < week_end then 1 end) as esp_syph_case,
max(case when condition = 'hepatitis_b:acute' and date >= week_start and date < week_end then 1 end) as esp_hep_b_acute_case,
max(case when condition = 'hepatitis_c:acute' and date >= week_start and date < week_end then 1 end) as esp_hep_c_acute_case
FROM hiv_rpt.r21_esp_cases T1,
hiv_rpt.r21_week_intervals T2
WHERE T1.patient_id = T2.master_patient_id
--AND T1.patient_id in (1688763, 38662886)
GROUP BY T1.patient_id, week_id, week_start
ORDER BY patient_id,week_id;

-- Number of prior encounters that met ESP criteria for chlamydia, gonorrhea, syphilis (i.e., number of unique case dates for any of these 3 conditions; if patient met ESP criteria for multiple STIs on the same day then count as 1 STI encounter)
-- Restrict to specific conditions
CREATE TABLE hiv_rpt.sti_case_encs AS 
SELECT patient_id, count(*) sti_esp_case_encounters 
FROM (
SELECT T1.patient_id as patient_id, date as testing_date
	FROM hiv_rpt.r21_esp_cases T1,
	hiv_rpt.r21_week_intervals T2
	WHERE T1.patient_id = T2.master_patient_id
	AND week_id = 0
	AND date < week_start and date >= week_start - interval '12 months'
	AND condition in ('chlamydia', 'gonorrhea', 'syphilis')
	GROUP BY T1.patient_id, date) T1
GROUP BY patient_id;


-------------------
------------------- ENCOUNTERS
-------------------

CREATE TABLE hiv_rpt.r21_encounters as
SELECT T1.patient_id, week_id, week_start,
case when week_id = 0 then count(case when date < week_start and date >= week_start - interval '12 months' then 1 end) else null end number_of_encs,
max(case when date >= week_start and date < week_end then 1 end) as any_enc
FROM public.emr_encounter T1
JOIN hiv_rpt.r21_week_intervals T2 ON (T1.patient_id = T2.master_patient_id)
LEFT JOIN static_enc_type_lookup T3 on (T1.raw_encounter_type = T3.raw_encounter_type)
-- Treat null encounter type as ambulatory
WHERE (T1.raw_encounter_type is NULL or T3.ambulatory = 1)
AND T1.date < '01-01-2021'
GROUP BY T1.patient_id, week_id, week_start
ORDER BY patient_id,week_id;

-------------------
------------------- HIV LABS
-------------------


-- Gather up all hiv related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS
-- ONLY PRINT RESULT STRING FOR TESTING. GROUP SO LOG RESULTS DON'T GET COUNTED TWICE
CREATE TABLE hiv_rpt.r21_hiv_events AS 
SELECT T2.patient_id, T3.test_name, T4.name as hef_name, 
--result_string,
T2.date
FROM hiv_rpt.r21_index_pats T1
INNER JOIN emr_labresult T2 ON (T1.master_patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.master_patient_id = T4.patient_id))
WHERE T2.date >= '12-01-2010'
AND T2.date < '01-01-2021'
AND T3.test_name ilike '%hiv%'
AND T3.test_name not in ('hiv not a test')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined', 'Not Done', 'Not performed',  'Not tested', 'Not Tested', 'test', 'test not done')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED')
AND result_string is not null
GROUP BY T2.patient_id, T3.test_name, T4.name, T2.date;


-- Remove hef name so tests that don't have pos/neg don't get counted twice
CREATE TABLE hiv_rpt.r21_hiv_events_dist_by_date AS 
SELECT patient_id, test_name, date
FROM hiv_rpt.r21_hiv_events
GROUP BY patient_id, test_name, date;


CREATE TABLE hiv_rpt.r21_hiv_counts_nohef as
SELECT T1.patient_id, week_id, week_start,
case when week_id = 0 then count(case when test_name = 'hiv_elisa' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hiv_elisa,
case when week_id = 0 then count(case when test_name = 'hiv_wb' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hiv_wb,
case when week_id = 0 then count(case when test_name = 'hiv_rna_viral' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hiv_rna_viral,
case when week_id = 0 then count(case when test_name = 'hiv_pcr' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hiv_pcr,
case when week_id = 0 then count(case when test_name = 'hiv_ag_ab' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hiv_ag_ab,
max(case when test_name = 'hiv_elisa' and date >= week_start and date < week_end then 1 end) as any_hiv_elisa,
max(case when test_name = 'hiv_wb' and date >= week_start and date < week_end then 1 end) as any_hiv_wb,
max(case when test_name = 'hiv_rna_viral' and date >= week_start and date < week_end then 1 end) as any_hiv_rna_viral,
max(case when test_name = 'hiv_ag_ab' and date >= week_start and date < week_end then 1 end) as any_hiv_ag_ab,
max(case when test_name = 'hiv_pcr' and date >= week_start and date < week_end then 1 end) as any_hiv_pcr
from 
hiv_rpt.r21_hiv_events_dist_by_date T1,
hiv_rpt.r21_week_intervals T2
WHERE T1.patient_id = T2.master_patient_id
GROUP BY patient_id, week_id, week_start
ORDER BY patient_id,week_id;


CREATE TABLE hiv_rpt.r21_hiv_counts_hef as
SELECT T1.patient_id, week_id, week_start,
max(case when test_name = 'hiv_elisa' and hef_name ilike '%pos%' and date >= week_start and date < week_end then 1 end) as pos_hiv_elisa,
max(case when test_name = 'hiv_wb' and hef_name ilike '%pos%' and date >= week_start and date < week_end then 1 end) as pos_hiv_wb,
max(case when test_name = 'hiv_rna_viral' and (hef_name ='lx:hiv_rna_viral:threshold:gt:200' or hef_name = 'lx:hiv_rna_viral:positive')  and date >= week_start and date < week_end then 1 end) as pos_hiv_rna_viral,
max(case when test_name = 'hiv_ag_ab' and hef_name ilike '%pos%' and date >= week_start and date < week_end then 1 end) as pos_hiv_ag_ab,
max(case when test_name = 'hiv_pcr' and hef_name ilike '%pos%' and date >= week_start and date < week_end then 1 end) as pos_hiv_pcr
FROM hiv_rpt.r21_hiv_events T1,
hiv_rpt.r21_week_intervals T2
WHERE T1.patient_id = T2.master_patient_id
GROUP BY T1.patient_id, week_id, week_start
ORDER BY patient_id,week_id;

CREATE TABLE hiv_rpt.r21_hiv_counts as
SELECT T1.patient_id, T1.week_id, T1.week_start,
prior_hiv_elisa,
prior_hiv_wb,
prior_hiv_rna_viral,
prior_hiv_pcr,
prior_hiv_ag_ab,
any_hiv_elisa,
pos_hiv_elisa,
any_hiv_wb,
pos_hiv_wb,
any_hiv_rna_viral,
pos_hiv_rna_viral,
any_hiv_ag_ab,
pos_hiv_ag_ab,
any_hiv_pcr,
pos_hiv_pcr
FROM 
hiv_rpt.r21_hiv_counts_nohef T1,
hiv_rpt.r21_hiv_counts_hef T2
WHERE T1.patient_id = T2.patient_id
AND T1.week_id = T2.week_id
AND T1.week_start = T2.week_start;



-------------------
------------------- HEP LABS
-------------------


-- Gather up all hep related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS
-- ONLY PRINT RESULT STRING FOR TESTING. GROUP SO LOG RESULTS DON'T GET COUNTED TWICE
CREATE TABLE hiv_rpt.r21_hep_events AS 
SELECT T2.patient_id, T3.test_name, T4.name as hef_name, --result_string,
T2.date
FROM hiv_rpt.r21_index_pats T1
INNER JOIN emr_labresult T2 ON (T1.master_patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.master_patient_id = T4.patient_id))
WHERE T2.date >= '12-01-2010'
AND T2.date < '01-01-2021'
AND T3.test_name in ('hepatitis_c_elisa', 'hepatitis_c_rna', 'hepatitis_b_surface_antigen', 'hepatitis_b_viral_dna', 'hepatitis_b_core_antigen_igm_antibody')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined', 'Not Done', 'Not performed',  'Not tested', 'Not Tested', 'test')
AND upper(result_string) !~ ('CANCELLED|CREDITED|TEST NOT PERFORMED|INCORRECT|TNP')
AND result_string is not null
GROUP BY T2.patient_id, T3.test_name, T4.name, T2.date;


-- Remove hef name so tests that don't have pos/neg don't get counted twice
CREATE TABLE hiv_rpt.r21_hep_events_dist_by_date AS 
SELECT patient_id, test_name, date
FROM hiv_rpt.r21_hep_events
GROUP BY patient_id, test_name, date;


CREATE TABLE hiv_rpt.r21_hep_counts_nohef as
SELECT T1.patient_id, week_id, week_start,
case when week_id = 0 then count(case when test_name = 'hepatitis_c_elisa' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hepc_elisa,
case when week_id = 0 then count(case when test_name = 'hepatitis_c_rna' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hepc_rna,
case when week_id = 0 then count(case when test_name = 'hepatitis_b_surface_antigen' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hepb_surf_ant,
case when week_id = 0 then count(case when test_name = 'hepatitis_b_viral_dna' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hepb_viral_dna,
case when week_id = 0 then count(case when test_name = 'hepatitis_b_core_antigen_igm_antibody' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_hepb_core_ag_igm_ab,
max(case when test_name = 'hepatitis_c_elisa' and date >= week_start and date < week_end then 1 end) as any_hepc_elisa,
max(case when test_name = 'hepatitis_c_rna' and date >= week_start and date < week_end then 1 end) as any_hepc_rna,
max(case when test_name = 'hepatitis_b_surface_antigen' and date >= week_start and date < week_end then 1 end) as any_hepb_surf_ant,
max(case when test_name = 'hepatitis_b_viral_dna' and date >= week_start and date < week_end then 1 end) as any_hepb_viral_dna,
max(case when test_name = 'hepatitis_b_core_antigen_igm_antibody' and date >= week_start and date < week_end then 1 end) as any_hepb_core_ag_igm_ab
from 
hiv_rpt.r21_hep_events_dist_by_date T1,
hiv_rpt.r21_week_intervals T2
WHERE T1.patient_id = T2.master_patient_id
GROUP BY patient_id, week_id, week_start
ORDER BY patient_id,week_id;


CREATE TABLE hiv_rpt.r21_hep_counts_hef as
SELECT T1.patient_id, week_id, week_start,
case when week_id = 0 then count(case when test_name = 'hepatitis_c_elisa' and hef_name ilike '%pos%' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_pos_hepc_elisa,
case when week_id = 0 then count(case when test_name = 'hepatitis_c_rna' and hef_name ilike '%pos%' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_pos_hepc_rna,
case when week_id = 0 then count(case when test_name = 'hepatitis_b_core_antigen_igm_antibody' and hef_name ilike '%pos%' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_pos_hepb_core_ag_igm_ab,
case when week_id = 0 then max(case when (test_name = 'hepatitis_b_surface_antigen' or test_name = 'hepatitis_b_viral_dna') and hef_name ilike '%pos%' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_pos_hepb,
max(case when test_name = 'hepatitis_c_elisa' and hef_name ilike '%pos%' and date >= week_start and date < week_end then 1 end) as pos_hepc_elisa,
max(case when test_name = 'hepatitis_c_rna' and hef_name ilike '%pos%' and date >= week_start and date < week_end then 1 end) as pos_hepc_rna,
max(case when test_name = 'hepatitis_b_core_antigen_igm_antibody' and hef_name ilike '%pos%' and date >= week_start and date < week_end then 1 end) as pos_hepb_core_ag_igm_ab,
max(case when (test_name = 'hepatitis_b_surface_antigen' or test_name = 'hepatitis_b_viral_dna') and hef_name ilike '%pos%' and date >= week_start and date < week_end then 1 end) as pos_hepb
FROM hiv_rpt.r21_hep_events T1,
hiv_rpt.r21_week_intervals T2
WHERE T1.patient_id = T2.master_patient_id
--AND T1.patient_id in (1688763, 38662886)
GROUP BY T1.patient_id, week_id, week_start
ORDER BY patient_id,week_id;

CREATE TABLE hiv_rpt.r21_hep_counts as
SELECT T1.patient_id, T1.week_id, T1.week_start,
prior_hepc_elisa,
prior_hepc_rna,
prior_pos_hepc_elisa,
prior_pos_hepc_rna,
prior_hepb_surf_ant,
prior_hepb_viral_dna,
prior_hepb_core_ag_igm_ab,
prior_pos_hepb_core_ag_igm_ab,
prior_pos_hepb,
any_hepc_elisa,
any_hepc_rna,
pos_hepc_elisa,
pos_hepc_rna,
any_hepb_surf_ant,
any_hepb_viral_dna,
any_hepb_core_ag_igm_ab,
pos_hepb_core_ag_igm_ab,
pos_hepb
FROM 
hiv_rpt.r21_hep_counts_nohef T1,
hiv_rpt.r21_hep_counts_hef T2
WHERE T1.patient_id = T2.patient_id
AND T1.week_id = T2.week_id
AND T1.week_start = T2.week_start;


-------------------
------------------- PRESCRIPTIONS
-------------------

-- HIV MEDS

CREATE TABLE hiv_rpt.r21_hiv_meds as
SELECT T1.patient_id, T1.name, T3.name as hef_name, T1.date, T1.quantity, T1.quantity_float, T1.refills, T1.start_date, T1.end_date, 
CASE WHEN refills~E'^\\d+$' THEN ((refills::real +1) * quantity_float) 
     WHEN refills is null then quantity_float end as total_quantity_per_rx,
case when T3.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end rx_truvada,
case when T3.name not in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end other_hiv_rx_non_truvada
FROM emr_prescription T1
JOIN hiv_rpt.r21_index_pats T2 on (T1.patient_id = T2.master_patient_id)
JOIN hef_event T3 on (T1.patient_id = T3.patient_id and T1.id = T3.object_id)
WHERE T3.name ilike 'rx:hiv%'
AND T1.date >= '01-01-2010'
AND T1.date < '01-01-2021';



CREATE TABLE hiv_rpt.r21_non_hiv_rx_of_interest AS 
SELECT T1.patient_id,T1.name, T1.date, T1.quantity, T1.quantity_float, T1.refills, T1.start_date, T1.end_date, T1.dose,
CASE WHEN refills~E'^\\d+$' THEN ((refills::real +1) * quantity_float) 
     WHEN refills is null then quantity_float end as total_quantity_per_rx,
CASE WHEN T1.name in (
-- ATRIUS
'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)',
'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE',
'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
--CHA 
'BICILLIN L-A 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE & PROC 1200000 UNIT/2ML IM SUSP',
'PENICILLIN G IVPB IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G IVPB (MINIBAG-PLUS) 5 MILLION UNITS',
'PENICILLIN G IVPB MINIBAG PLUS 5 MILLION UNITS',
'PENICILLIN G POTASSIUM 20000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM 5000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM IN D5W 40000 UNIT/ML IV SOLN',
'PENICILLIN G POTASSIUM IN D5W 60000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 20000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 40000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 60000 UNIT/ML IV SOLN',
'PENICILLIN G PROCAINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 300000-900000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G SODIUM 5000000 IU IJ SOLR',
--BMC
'BICILLIN L-A 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
'PENICILLIN G IVPB  IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G POTASSIUM 20 MILLION UNIT SOLUTION FOR INJECTION',
'PENICILLIN G POTASSIUM 5 MILLION UNIT SOLUTION FOR INJECTION',
'PENICILLIN G POTASSIUM IV 5 MILLION UNITS MBP',
'PENICILLIN G SODIUM 5 MILLION UNIT SOLUTION FOR INJECTION',
--FENWAY
'BICILLIN LA 100,000 UNITS',
'BICILLIN LA 100M000 UNITS',
'BICILLIN L-A 2400000 U/4ML SUSPN',
'BICILLIN L-A 2400000 UNIT/4ML INTRAMUSCULAR SUSPENSION',
'BICILLIN L-A 2400000 UNIT/4ML SUSP',
'BICILLIN L-A 2400000 UNIT/4ML  SUSP',
'Med: Bicillin LA 2.4 million units IM, THREE doses over THREE weeks',
'Med:  Bicillin LA 2.4 million units IM, TWO doses 1 week apart',
'PENICILLIN G LA 2.4 MILLION UNITS'
)
-- ATRIUS 
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE', 'BICILLIN L-A DISP SYRIN 600000 U IM (PENICILLIN G BENZATHINE)', 'BICILLIN L-A DISP SYRIN 600MU/1ML IM (PENICILLIN G BENZATHINE)') and quantity_float >= 4)
-- CHA
OR (name in ('BICILLIN L-A 1200000 UNIT/2ML IM SUSP', 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600000 UNIT/ML IM SUSP', 'PENICILLIN G BENZATHINE 600000 UNIT/ML IM SUSP') and quantity_float >= 4)
-- BMC
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML INTRAMUSCULAR SYRINGE') and (dose = '2.4 Million Units' or dose = '2400000 Units'))
-- FENWAY
OR (name in ('BICILLIN L-A 1200000 U/2ML SUSPN', 'BICILLIN L-A 1200000 UNIT/2ML IM SUSP', 'BICILLIN L-A 1200000 UNIT/2ML INTRAMUSCULAR SUSPENSION', 'BICILLIN L-A 1200000 UNIT/2ML SUSP', 'Med:  Bicillin 1.2 mil/u  x 1', 'Med: Bicillin LA 1.2 million units IM') and quantity_float >= 2)
OR (name in ('Bicillin injection, route of administration') and 
	(quantity ilike '%1.2%' 
	or quantity ilike '%2.4%' 
	or quantity ilike '%2ml%' 
	or quantity ilike '%2.5%' 
	or quantity ilike '%2x2.0%' 
	or quantity ilike '%2.0 ml%'
	or quantity ilike '%240million%'))
then 1 end rx_bicillin,	 
CASE WHEN T1.name in (
    -- ATRIUS
	'AZITHROMYCIN 1 GRAM ORAL PACKET', 
	'AZITHROMYCIN CHLAMYDIA RX (1 G POWDER PACKET)', 
	'ZITHROMAX 1 GRAM ORAL PACKET (AZITHROMYCIN)', 
	-- CHA
	'AZITHROMYCIN 1 G PO PACK',
	'ZITHROMAX 1 G PO PACK',
	'AZITHROMYCIN 2 G PO SUSR',
	-- BMC
	'AZITHROMYCIN 1 GRAM ORAL PACKET',
	-- FENWAY
	'AZITHROMYCIN 1G',
	'AZITHROMYCIN 1 GM ORAL PACK',
	'AZITHROMYCIN 1 GM ORAL PACKET',
	'AZITHROMYCIN 1 GM PACK',
	'AZITHROMYCIN 1 GRAM',
	'AZITHROMYCIN  Up to 1 GRAM',
	'Med:  Zithromax 1gr slurry PO',
	'ZITHROMAX 1 GM ORAL PACKET',
	'ZITHROMAX 1 GM PACK',
	'ZITHROMAX POW 1GM PAK'	,
	'ZITHROMAX UP TO 1GM'
	) 
	OR (name in ('Zithromax Dose') and 
	(quantity ilike '%1 g%' 
	or quantity ilike '%1g%' 
	or quantity ilike '%1000 mg%' 
	or quantity ilike '%2%500mg%'))
	then 1 end rx_azithromycin,
CASE WHEN T1.name in ( 
     -- ATRIUS
	'CEFTRIAXONE IM INJECTION <=250 MG', 
	'CEFTRIAXONE-LIDOCAINE IM INJECTION <=250 MG',
	'ROCEPHIN 250 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)', 
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION', 
	-- CHA
	'CEFTRIAXONE SODIUM 250 MG IJ SOLR',
	'ROCEPHIN 250 MG IJ SOLR',
	-- BMC
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION',
	'CEFTRIAXONE IM INJECTION <=250 MG',
	-- FENWAY
	'CEFTRIAXONE 250MG',
	'CEFTRIAXONE EA 250MG',
	'CEFTRIAXONE SODIUM 250 MG/1ML INJ SOLR (CEFTRIAXONE SODIUM)',
	'CEFTRIAXONE SODIUM 250 MG INJECTION SOLUTION RECONSTITUTED',
	'CEFTRIAXONE SODIUM 250 MG INJ SOLR',
	'CEFTRIAXONE SODIUM 250 MG SOLR',
	'Med:  Rocephin  250mg IM',
	'Rocephin 250 mg',
	'ROCEPHIN 250 MG',
	'ROCEPHIN 250 MG INJ RECON SOL',
	'ROCEPHIN 250 MG INJ SOLR',
	'ROCEPHIN INJ 250MG'
	) 
	OR (name in ('CEFTRIAXONE SODIUM POWD', 'CEFTRIAXONE SODIUM POWDER', 'CEFTRIAXONE SODIUM PWDR', 'Rocephin (Ceftriaxone) Injection') and 
	(quantity ilike '%250%'
	or quantity ilike '%125%'))
	then 1 end rx_ceftriaxone,
CASE WHEN 
	T1.name ilike '%methadone%' 
	or T1.name ilike '%dolophine%' 
	or T1.name ilike '%methadose%' then 1 end rx_methadone,
CASE WHEN 
	T1.name ilike '%suboxone%' 
	or T1.name ilike 'buprenorphine%naloxone%' 
	or T1.name ilike '%bunavail%'
	or T1.name ilike '%cassipa%'
	or T1.name ilike '%zubsolv%' then 1 end rx_suboxone,
CASE WHEN 
	T1.name ilike '%tadalafil%' 
	or T1.name ilike '%cilais%' 
	or T1.name ilike '%vardenafil%' 
	or T1.name ilike '%staxyn%'
	or T1.name ilike '%levitra%' 
	or T1.name ilike '%sildenafil%' 
	or T1.name ilike '%viagra%' then 1 end rx_viagara_cilais_or_levitra 
FROM emr_prescription T1
JOIN hiv_rpt.r21_index_pats T2 on (T1.patient_id = T2.master_patient_id)
WHERE T1.date >= '01-01-2010'
AND T1.date < '01-01-2021'
AND ( name in (
     --ATRIUS
	'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)',
	'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)',
	'BICILLIN L-A DISP SYRIN 600000 U IM (PENICILLIN G BENZATHINE)',
	'BICILLIN L-A DISP SYRIN 600MU/1ML IM (PENICILLIN G BENZATHINE)',
	'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE',
	'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
	'BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 
	'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE'
	'BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 
	'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE'
	'AZITHROMYCIN 1 GRAM ORAL PACKET', 
	'AZITHROMYCIN CHLAMYDIA RX (1 G POWDER PACKET)', 
	'ZITHROMAX 1 GRAM ORAL PACKET (AZITHROMYCIN)', 
	'CEFTRIAXONE IM INJECTION <=250 MG', 
	'CEFTRIAXONE-LIDOCAINE IM INJECTION <=250 MG',
	'ROCEPHIN 250 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)', 
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION', 
	--CHA
	'BICILLIN L-A 1200000 UNIT/2ML IM SUSP',
	'BICILLIN L-A 2400000 UNIT/4ML IM SUSP',
	'BICILLIN L-A 600000 UNIT/ML IM SUSP',
	'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP',
	'PENICILLIN G BENZATHINE 2400000 UNIT/4ML IM SUSP',
	'PENICILLIN G BENZATHINE 600000 UNIT/ML IM SUSP',
	'PENICILLIN G BENZATHINE & PROC 1200000 UNIT/2ML IM SUSP',
	'PENICILLIN G IVPB IN 100 ML',
	'PENICILLIN G IVPB IN 50 ML',
	'PENICILLIN G IVPB (MINIBAG-PLUS) 5 MILLION UNITS',
	'PENICILLIN G IVPB MINIBAG PLUS 5 MILLION UNITS',
	'PENICILLIN G POTASSIUM 20000000 UNITS IJ SOLR',
	'PENICILLIN G POTASSIUM 5000000 UNITS IJ SOLR',
	'PENICILLIN G POTASSIUM IN D5W 40000 UNIT/ML IV SOLN',
	'PENICILLIN G POTASSIUM IN D5W 60000 UNIT/ML IV SOLN',
	'PENICILLIN G POT IN DEXTROSE 20000 UNIT/ML IV SOLN',
	'PENICILLIN G POT IN DEXTROSE 40000 UNIT/ML IV SOLN',
	'PENICILLIN G POT IN DEXTROSE 60000 UNIT/ML IV SOLN',
	'PENICILLIN G PROCAINE 600000 UNIT/ML IM SUSP',
	'PENICILLIN G PROC & BENZATHINE 300000-900000 UNIT/ML IM SUSP',
	'PENICILLIN G PROC & BENZATHINE 600000 UNIT/ML IM SUSP',
	'PENICILLIN G SODIUM 5000000 IU IJ SOLR',
	'AZITHROMYCIN 1 G PO PACK',
	'ZITHROMAX 1 G PO PACK',
	'AZITHROMYCIN 2 G PO SUSR',
	'CEFTRIAXONE SODIUM 250 MG IJ SOLR',
	'ROCEPHIN 250 MG IJ SOLR', 
	--BMC 
	'BICILLIN L-A 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
    'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
    'PENICILLIN G IVPB  IN 100 ML',
    'PENICILLIN G IVPB IN 50 ML',
    'PENICILLIN G POTASSIUM 20 MILLION UNIT SOLUTION FOR INJECTION',
    'PENICILLIN G POTASSIUM 5 MILLION UNIT SOLUTION FOR INJECTION',
    'PENICILLIN G POTASSIUM IV 5 MILLION UNITS MBP',
    'PENICILLIN G SODIUM 5 MILLION UNIT SOLUTION FOR INJECTION',
	'BICILLIN L-A 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE',
	'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE',
	'PENICILLIN G BENZATHINE 600,000 UNIT/ML INTRAMUSCULAR SYRINGE',
	'AZITHROMYCIN 1 GRAM ORAL PACKET',
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION',
	'CEFTRIAXONE IM INJECTION <=250 MG',
	--FENWAY
	'BICILLIN LA 100,000 UNITS',
	'BICILLIN LA 100M000 UNITS',
	'BICILLIN L-A 2400000 U/4ML SUSPN',
	'BICILLIN L-A 2400000 UNIT/4ML INTRAMUSCULAR SUSPENSION',
	'BICILLIN L-A 2400000 UNIT/4ML SUSP',
	'BICILLIN L-A 2400000 UNIT/4ML  SUSP',
	'Med: Bicillin LA 2.4 million units IM, THREE doses over THREE weeks',
	'Med:  Bicillin LA 2.4 million units IM, TWO doses 1 week apart',
	'PENICILLIN G LA 2.4 MILLION UNITS',
	'BICILLIN L-A 1200000 U/2ML SUSPN',
	'BICILLIN L-A 1200000 UNIT/2ML IM SUSP',
	'BICILLIN L-A 1200000 UNIT/2ML INTRAMUSCULAR SUSPENSION',
	'BICILLIN L-A 1200000 UNIT/2ML SUSP',
	'Med:  Bicillin 1.2 mil/u  x 1',
	'Med: Bicillin LA 1.2 million units IM',
	'Bicillin injection, route of administration',
	'AZITHROMYCIN 1G',
	'AZITHROMYCIN 1 GM ORAL PACK',
	'AZITHROMYCIN 1 GM ORAL PACKET',
	'AZITHROMYCIN 1 GM PACK',
	'AZITHROMYCIN 1 GRAM',
	'AZITHROMYCIN  Up to 1 GRAM',
	'Med:  Zithromax 1gr slurry PO',
	'ZITHROMAX 1 GM ORAL PACKET',
	'ZITHROMAX 1 GM PACK',
	'ZITHROMAX POW 1GM PAK',
	'Zithromax Dose',
	'ZITHROMAX UP TO 1GM',
	'CEFTRIAXONE 250MG',
	'CEFTRIAXONE EA 250MG',
	'CEFTRIAXONE SODIUM 250 MG/1ML INJ SOLR (CEFTRIAXONE SODIUM)',
	'CEFTRIAXONE SODIUM 250 MG INJECTION SOLUTION RECONSTITUTED',
	'CEFTRIAXONE SODIUM 250 MG INJ SOLR',
	'CEFTRIAXONE SODIUM 250 MG SOLR',
	'Med:  Rocephin  250mg IM',
	'Rocephin 250 mg',
	'ROCEPHIN 250 MG',
	'ROCEPHIN 250 MG INJ RECON SOL',
	'ROCEPHIN 250 MG INJ SOLR',
	'ROCEPHIN INJ 250MG',
	'CEFTRIAXONE SODIUM POWD', 
	'CEFTRIAXONE SODIUM POWDER', 
	'CEFTRIAXONE SODIUM PWDR',
	'Rocephin (Ceftriaxone) Injection'
	)
OR (name ilike '%methadone%' 
	or name ilike '%dolophine%' 
	or name ilike '%methadose%' 
	or name ilike '%suboxone%'  
	or name ilike 'buprenorphine%naloxone%'
	or name ilike '%zubsolv%' 
	or name ilike '%bunavail%' 
	or name ilike '%cassipa%'  
	or name ilike '%tadalafil%'  
	or name ilike '%cilais%'  
	or name ilike '%vardenafil%' 
	or name ilike '%staxyn%' 
	or name ilike '%levitra%' 
	or name ilike '%sildenafil%'  
	or name ilike '%viagra%') 
AND name not ilike '%revatio%'
AND name not ilike '%ANTIHYPERTENSIVE%'
AND name not ilike '%HYPERTENSION%'
AND name not ilike '%adcirca%'
AND name not ilike '%alyq%');
	

CREATE TABLE hiv_rpt.r21_all_rx_of_interest AS 
SELECT patient_id, date,  total_quantity_per_rx, rx_truvada, other_hiv_rx_non_truvada, null as rx_bicillin,	null as rx_azithromycin, null as rx_ceftriaxone, null as rx_methadone, null as rx_suboxone, null as rx_viagara_cilais_or_levitra 
FROM hiv_rpt.r21_hiv_meds
UNION
SELECT patient_id, date,  total_quantity_per_rx, null as rx_truvada, null as other_hiv_rx_non_truvada, rx_bicillin,	rx_azithromycin, rx_ceftriaxone, rx_methadone, rx_suboxone, rx_viagara_cilais_or_levitra
FROM hiv_rpt.r21_non_hiv_rx_of_interest
;

CREATE TABLE hiv_rpt.r21_rx_of_int_counts as
SELECT T1.master_patient_id as patient_id, week_id, week_start,
case when week_id = 0 then count(case when rx_truvada = 1 and T2.date < week_start and T2.date >= week_start - interval '3 months' then 1 end) else null end prior_rx_truvada_3m,
case when week_id = 0 then sum(case when rx_truvada = 1 and T2.date < week_start and T2.date >= week_start - interval '3 months' then T2.total_quantity_per_rx end) else null end prior_rx_truvada_3m_total_quantity,
case when week_id = 0 then count(case when rx_truvada = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then 1 end) else null end prior_rx_truvada_12m,
case when week_id = 0 then sum(case when rx_truvada = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_truvada_12m_total_quantity,
case when week_id = 0 then count(case when other_hiv_rx_non_truvada = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then 1 end) else null end prior_rx_hiv_other,
case when week_id = 0 then sum(case when other_hiv_rx_non_truvada = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_hiv_other_total_quantity,
case when week_id = 0 then count(case when rx_bicillin = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then 1 end) else null end prior_rx_bicillin,	
case when week_id = 0 then sum(case when rx_bicillin = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_bicillin_total_quantity,	
case when week_id = 0 then count(case when rx_azithromycin = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then 1 end) else null end prior_rx_azithromycin,	
case when week_id = 0 then sum(case when rx_azithromycin = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_azithromycin_total_quantity,	
case when week_id = 0 then count(case when rx_ceftriaxone = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then 1 end) else null end prior_rx_ceftriaxone,
case when week_id = 0 then sum(case when rx_ceftriaxone = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_ceftriaxone_total_quantity,		
case when week_id = 0 then count(case when rx_methadone = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then 1 end) else null end prior_rx_methadone,
case when week_id = 0 then sum(case when rx_methadone = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_methadone_total_quantity,	
case when week_id = 0 then count(case when rx_suboxone = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then 1 end) else null end prior_rx_suboxone,
case when week_id = 0 then sum(case when rx_suboxone = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_suboxone_total_quantity,
case when week_id = 0 then count(case when rx_viagara_cilais_or_levitra = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then 1 end) else null end prior_rx_viagara_cilais_or_levitra,
case when week_id = 0 then sum(case when rx_viagara_cilais_or_levitra = 1 and T2.date < week_start and T2.date >= week_start - interval '12 months' then T2.total_quantity_per_rx end) else null end prior_rx_viagara_cilais_or_levitra_total_quantity,
max(case when rx_truvada = 1 and T2.date >= week_start and T2.date < week_end then 1 end) as rx_truvada,
sum(case when rx_truvada = 1 and T2.date >= week_start and T2.date < week_end then T2.total_quantity_per_rx end) as rx_truvada_total_quantity,
max(case when other_hiv_rx_non_truvada = 1 and T2.date >= week_start and T2.date < week_end then 1 end) as rx_hiv_other,
sum(case when other_hiv_rx_non_truvada  = 1 and T2.date >= week_start and T2.date < week_end then T2.total_quantity_per_rx end) as rx_hiv_other_total_quantity,
max(case when rx_bicillin = 1 and T2.date >= week_start and T2.date < week_end then 1 end) as rx_bicillin,
sum(case when rx_bicillin = 1 and T2.date >= week_start and T2.date < week_end then T2.total_quantity_per_rx end) as rx_bicillin_total_quantity,
max(case when rx_azithromycin = 1 and T2.date >= week_start and T2.date < week_end then 1 end) as rx_azithromycin,
sum(case when rx_azithromycin = 1 and T2.date >= week_start and T2.date < week_end then T2.total_quantity_per_rx end) as rx_azithromycin_total_quantity,
max(case when rx_ceftriaxone = 1 and T2.date >= week_start and T2.date < week_end then 1 end) as rx_ceftriaxone,
sum(case when rx_ceftriaxone = 1 and T2.date >= week_start and T2.date < week_end then T2.total_quantity_per_rx end) as rx_ceftriaxone_total_quantity,
max(case when rx_methadone = 1 and T2.date >= week_start and T2.date < week_end then 1 end) as rx_methadone,
sum(case when rx_methadone = 1 and T2.date >= week_start and T2.date < week_end then T2.total_quantity_per_rx end) as rx_methadone_total_quantity,
max(case when rx_suboxone = 1 and T2.date >= week_start and T2.date < week_end then 1 end) as rx_suboxone,
sum(case when rx_suboxone = 1 and T2.date >= week_start and T2.date < week_end then T2.total_quantity_per_rx end) as rx_suboxone_total_quantity,
max(case when rx_viagara_cilais_or_levitra = 1 and T2.date >= week_start and T2.date < week_end then 1 end) as rx_viagara_cilais_or_levitra,
sum(case when rx_viagara_cilais_or_levitra = 1 and T2.date >= week_start and T2.date < week_end then T2.total_quantity_per_rx end) as rx_viagara_cilais_or_levitra_total_quantity
FROM hiv_rpt.r21_week_intervals T1
JOIN hiv_rpt.r21_all_rx_of_interest T2 on (T2.patient_id = T1.master_patient_id)
--AND T1.patient_id in (1688763, 38662886)
GROUP BY T1.master_patient_id, week_id, week_start
ORDER BY T1.master_patient_id,week_id;



-------------------
------------------- DIAGNOSIS CODES
-------------------

CREATE TABLE hiv_rpt.r21_dx_codes AS 
SELECT T3.*, T2.patient_id, T2.date
FROM 
hiv_rpt.r21_index_pats T1
JOIN emr_encounter T2 on (T1.master_patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 on (T3.encounter_id = T2.id)
WHERE T2.date >= '01-01-2010'
AND T2.date < '01-01-2021'
AND (dx_code_id ~
'^(icd10:B20|icd10:Z21|icd9:796.7|icd10:R85.61|icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.|icd9:099.4|icd9:054.1|icd10:A60.0|icd10:A63.|icd10:Z72.5|icd9:296.2|icd9:296.3|icd10:F32.|icd10:F33.|icd9:314.|icd10:F90.|icd9:649.0|icd10:F17.|icd9:305.0|icd9:304.0|icd9:305.5|icd10:F11.|icd9:304.1|icd9:305.4|icd10:F13.|icd9:304.2|icd9:305.6|icd10:F14.|icd9:304.4|icd9:305.7|icd10:F15.|icd9:304.9|icd9:292.8|icd10:F19.|icd10:F10.1|icd10:T18.5|icd10:F50.0|icd10:F50.8)'
OR dx_code_id in (
    'icd9:042',
    'icd9:V08',
	'icd9:079.53',
	'icd10:B97.35',
	'icd9:569.44',
	'icd10:K62.82',
	'icd9:230.5',
	'icd9:230.6',
	'icd10:D01.3',	
	'icd9:097.9',
	'icd9:097.1',
	'icd10:A53.0',
	'icd10:A53.9',
	'icd9:091.1',
	'icd10:A51.1',
	'icd9:098.7',
	'icd10:A54.6',
	'icd9:098.6',
	'icd10:A54.5', 
	'icd9:099.52', 
	'icd10:A56.3', 
	'icd9:099.51', 
	'icd10:A56.4', 
	'icd9:099.1', 
	'icd10:A55', 
	'icd9:099.0', 
	'icd10:A57', 
	'icd9:099.2', 
	'icd10:A58', 
	'icd10:N34.1', 
	'icd9:054.8', 
	'icd9:054.9', 
	'icd9:054.79', 
	'icd10:A60.1', 
	'icd10:A60.9', 
	'icd10:B00.9',
	'icd9:078.11', 
	'icd10:A63.0', 
	'icd9:569.41', 
	'icd10:K62.6'
	'icd9:099.8',
	'icd9:099.9',
	'icd10:A64', 
	'icd9:V01.6',
	'icd10:Z20.2', 
	'icd10:Z20.6', 
	'icd9:V69.2', 
	'icd9:V65.44', 
	'icd10:Z71.7', 
	'icd9:307.1', 
	'icd9:307.51', 
	'icd10:F50.2', 
	'icd9:307.50', 
	'icd10:F50.9', 
	'icd9:296.82',
	'icd9:300.4',
	'icd9:301.12',
	'icd9:308',
	'icd9:309',
	'icd9:309.1',
	'icd9:309.28',
	'icd9:311',
	'icd10:F34.1',
	'icd10:F43.21',
	'icd10:F43.23',
	'icd10:F06.31',
	'icd10:F06.32',
	'icd9:302.0',
	'icd9:302.6',
	'icd9:302.85',
	'icd10:F64.2',
	'icd10:F64.8',
	'icd10:F64.9',
	'icd10:F66.0',
	'icd9:302.5', 
	'icd9:302.50', 
	'icd9:302.51', 
	'icd9:302.52', 
	'icd9:302.53', 
	'icd10:F64.0',
	'icd10:F64.1',
	'icd10:F65.1',
	'icd9:V61.21',
	'icd10:Z61.4', 
	'icd10:Z61.5',
	'icd10:Z69.010',
	'icd9:937', 
	'icd9:305.1',
	'icd9:V15.82',
	'icd9:303.90', 
	'icd9:303.91', 
	'icd9:303.92', 
	'icd10:F10.2'
));



CREATE TABLE hiv_rpt.r21_dx_counts AS 
SELECT patient_id, week_id, week_start,
-- icd10:O98.7 removed as this is for males only
case when week_id = 0 then count(case when dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B97.35|icd10:Z21)' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_hiv,
case when week_id = 0 then count(case when dx_code_id ~ '^(icd9:796.7|icd10:R85.61)' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_abn_anal_cyt,
case when week_id = 0 then count(case when dx_code_id in ('icd9:569.44', 'icd10:K62.82') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_anal_dyspl,
case when week_id = 0 then count(case when dx_code_id in ('icd9:230.5', 'icd9:230.6', 'icd10:D01.3') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_anal_carc,
case when week_id = 0 then count(case when (dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1')) and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_syph,
case when week_id = 0 then count(case when dx_code_id in ('icd9:091.1', 'icd10:A51.1') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_anal_syph,
case when week_id = 0 then count(case when dx_code_id in ('icd9:098.7','icd10:A54.6') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_gonoc_inf_rectum,
case when week_id = 0 then count(case when dx_code_id in ('icd9:098.6','icd10:A54.5') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_gonoc_pharyn,
case when week_id = 0 then count(case when dx_code_id in ('icd9:099.52','icd10:A56.3') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_chlam_inf_rectum,
case when week_id = 0 then count(case when dx_code_id in ('icd9:099.51','icd10:A56.4') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_chlam_inf_pharyn,
case when week_id = 0 then count(case when dx_code_id in ('icd9:099.1','icd10:A55') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_lymphgranuloma_venereum,
case when week_id = 0 then count(case when dx_code_id in ('icd9:099.0','icd10:A57') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_chancroid,
case when week_id = 0 then count(case when dx_code_id in ('icd9:099.2', 'icd10:A58') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_granuloma_inguinale,
case when week_id = 0 then count(case when (dx_code_id like 'icd9:099.4%' or dx_code_id in ('icd10:N34.1')) and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_nongonococcal_urethritis,
case when week_id = 0 then count(case when dx_code_id in ('icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9', 'icd10:B00.9') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_herpes_simplex_w_complications,
case when week_id = 0 then count(case when dx_code_id ~ '^(icd9:054.1|icd10:A60.0)' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_genital_herpes,
case when week_id = 0 then count(case when dx_code_id in ('icd9:078.11', 'icd10:A63.0') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_anogenital_warts,
case when week_id = 0 then count(case when dx_code_id in ('icd9:569.41', 'icd10:K62.6') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_anorectal_ulcer,
case when week_id = 0 then count(case when (dx_code_id like 'icd10:A63.%' or dx_code_id in ('icd10:A64', 'icd9:099.8', 'icd9:099.9')) and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_unspecified_std,
case when week_id = 0 then count(case when dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_contact_with_or_exposure_to_venereal_disease,
case when week_id = 0 then count(case when (dx_code_id ~ '^(icd10:Z72.5)' or dx_code_id in ('icd9:V69.2')) and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_high_risk_sexual_behavior,
case when week_id = 0 then count(case when dx_code_id in ('icd9:V65.44','icd10:Z71.7') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_hiv_counseling,
case when week_id = 0 then count(case when (dx_code_id ~ '^(icd10:F50.0)' or dx_code_id in ('icd9:307.1')) and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_anorexia_nervosa,
case when week_id = 0 then count(case when dx_code_id in ('icd9:307.51','icd10:F50.2') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_bulimia_nervosa,
case when week_id = 0 then count(case when (dx_code_id ~ '^(icd10:F50.8)' or dx_code_id in ('icd9:307.50','icd10:F50.9')) and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_eating_disorder_nos,
case when week_id = 0 then count(case when (dx_code_id ~ '^(icd9:296.2|icd9:296.3|icd10:F32.|icd10:F33.)' or dx_code_id in ('icd9:296.82', 'icd9:300.4', 'icd9:301.12', 'icd9:308', 'icd9:309', 'icd9:309.1', 'icd9:309.28', 'icd9:311', 'icd10:F34.1', 'icd10:F43.21', 'icd10:F43.23', 'icd10:F06.31', 'icd10:F06.32')) and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_depression,
case when week_id = 0 then count(case when dx_code_id ~ '^(icd9:314.|icd10:F90.)' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_attn_def_disorder,
case when week_id = 0 then count(case when dx_code_id in ('icd9:302.0', 'icd9:302.6', 'icd9:302.85', 'icd10:F64.2', 'icd10:F64.8', 'icd10:F64.9', 'icd10:F66.0', 'icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F64.0','icd10:F64.1', 'icd10:F65.1', 'icd10:Z87.890') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_gend_iden_trans_sex_reassign,
case when week_id = 0 then count(case when dx_code_id in ('icd9:V61.21', 'icd10:Z61.4', 'icd10:Z61.5', 'icd10:Z69.010') and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_counseling_for_child_sexual_abuse,
case when week_id = 0 then count(case when (dx_code_id ~ '^(icd10:T18.5)' or dx_code_id in ('icd9:937')) and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_foreign_body_in_anus,
case when week_id = 0 then count(case when (dx_code_id ~ '^(icd9:649.0|icd10:F17.)' or dx_code_id in ('icd9:305.1', 'icd9:V15.82')) and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_smoking_tobacco_use,
case when week_id = 0 then count(case when (dx_code_id ~ '^(icd9:305.0|icd10:F10.1)' or dx_code_id in ('icd9:303.90', 'icd9:303.91', 'icd9:303.92', 'icd10:F10.2')) and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_alcohol_dependence_abuse,
case when week_id = 0 then count(case when dx_code_id ~ '^(icd9:304.0|icd10:F11.|icd9:305.5)' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_opioid_dependence_abuse,
case when week_id = 0 then count(case when dx_code_id ~ '^(icd9:304.1|icd9:305.4|icd10:F13.)' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_sed_hypn_anxio_depend_abuse,
case when week_id = 0 then count(case when dx_code_id ~ '^(icd9:304.2|icd9:305.6|icd10:F14.)' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_cocaine_dependence_abuse,
case when week_id = 0 then count(case when dx_code_id ~ '^(icd9:304.4|icd9:305.7|icd10:F15.)' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_amphet_stim_dependence_abuse,
case when week_id = 0 then count(case when dx_code_id ~ '^(icd9:304.9|icd9:292.8|icd10:F19)' and date < week_start and date >= week_start - interval '12 months' then 1 end) else null end prior_dx_oth_psycho_or_unspec_subs_depend_abuse,
max(case when dx_code_id ~ '^(icd9:796.7|icd10:R85.61)' and date >= week_start and date < week_end then 1 end) as dx_abn_anal_cyt,
max(case when dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B97.35|icd10:Z21)' and date >= week_start and date < week_end then 1 end) as dx_hiv,
max(case when dx_code_id in ('icd9:569.44', 'icd10:K62.82') and date >= week_start and date < week_end then 1 end) as dx_anal_dyspl,
max(case when dx_code_id in ('icd9:230.5', 'icd9:230.6', 'icd10:D01.3') and date >= week_start and date < week_end then 1 end) as dx_anal_carc,
max(case when (dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1')) and date >= week_start and date < week_end then 1 end) as dx_syph,
max(case when dx_code_id in ('icd9:091.1', 'icd10:A51.1') and date >= week_start and date < week_end then 1 end) as dx_anal_syph,
max(case when dx_code_id in ('icd9:098.7','icd10:A54.6') and date >= week_start and date < week_end then 1 end) as dx_gonoc_inf_rectum,
max(case when dx_code_id in ('icd9:098.6','icd10:A54.5') and date >= week_start and date < week_end then 1 end) as dx_gonoc_pharyn,
max(case when dx_code_id in ('icd9:099.52','icd10:A56.3') and date >= week_start and date < week_end then 1 end) as dx_chlam_inf_rectum,
max(case when dx_code_id in ('icd9:099.51','icd10:A56.4') and date >= week_start and date < week_end then 1 end) as dx_chlam_inf_pharyn,
max(case when dx_code_id in ('icd9:099.1','icd10:A55') and date >= week_start and date < week_end then 1 end) as dx_lymphgranuloma_venereum,
max(case when dx_code_id in ('icd9:099.0','icd10:A57') and date >= week_start and date < week_end then 1 end) as dx_chancroid,
max(case when dx_code_id in ('icd9:099.2', 'icd10:A58') and date >= week_start and date < week_end then 1 end) as dx_granuloma_inguinale,
max(case when (dx_code_id like 'icd9:099.4%' or dx_code_id in ('icd10:N34.1')) and date >= week_start and date < week_end then 1 end) as dx_nongonococcal_urethritis,
max(case when dx_code_id in ('icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9', 'icd10:B00.9') and date >= week_start and date < week_end then 1 end) as dx_herpes_simplex_w_complications,
max(case when dx_code_id ~ '^(icd9:054.1|icd10:A60.0)' and date >= week_start and date < week_end then 1 end) as dx_genital_herpes,
max(case when dx_code_id in ('icd9:078.11', 'icd10:A63.0') and date >= week_start and date < week_end then 1 end) as dx_anogenital_warts,
max(case when dx_code_id in ('icd9:569.41', 'icd10:K62.6') and date >= week_start and date < week_end then 1 end) as dx_anorectal_ulcer,
max(case when (dx_code_id like 'icd10:A63.%' or dx_code_id in ('icd10:A64', 'icd9:099.8', 'icd9:099.9')) and date >= week_start and date < week_end then 1 end) as dx_unspecified_std,
max(case when dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') and date >= week_start and date < week_end then 1 end) as dx_contact_with_or_exposure_to_venereal_disease,
max(case when (dx_code_id ~ '^(icd10:Z72.5)' or dx_code_id in ('icd9:V69.2')) and date >= week_start and date < week_end then 1 end) as dx_high_risk_sexual_behavior,
max(case when dx_code_id in ('icd9:V65.44','icd10:Z71.7') and date >= week_start and date < week_end then 1 end) as dx_hiv_counseling,
max(case when (dx_code_id ~ '^(icd10:F50.0)' or dx_code_id in ('icd9:307.1')) and date >= week_start and date < week_end then 1 end) as dx_anorexia_nervosa,
max(case when dx_code_id in ('icd9:307.51','icd10:F50.2') and date >= week_start and date < week_end then 1 end) as dx_bulimia_nervosa,
max(case when (dx_code_id ~ '^(icd10:F50.8)' or dx_code_id in ('icd9:307.50','icd10:F50.9')) and date >= week_start and date < week_end then 1 end) as dx_eating_disorder_nos,
max(case when (dx_code_id ~ '^(icd9:296.2|icd9:296.3|icd10:F32.|icd10:F33.)' or dx_code_id in ('icd9:296.82', 'icd9:300.4', 'icd9:301.12', 'icd9:308', 'icd9:309', 'icd9:309.1', 'icd9:309.28', 'icd9:311', 'icd10:F34.1', 'icd10:F43.21', 'icd10:F43.23', 'icd10:F06.31', 'icd10:F06.32')) and date >= week_start and date < week_end then 1 end) as dx_depression,
max(case when dx_code_id ~ '^(icd9:314.|icd10:F90.)' and date >= week_start and date < week_end then 1 end) as dx_attn_def_disorder,
max(case when dx_code_id in ('icd9:302.0', 'icd9:302.6', 'icd9:302.85', 'icd10:F64.2', 'icd10:F64.8', 'icd10:F64.9', 'icd10:F66.0', 'icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F64.0','icd10:F64.1', 'icd10:F65.1', 'icd10:Z87.890') and date >= week_start and date < week_end then 1 end) as dx_gend_iden_trans_sex_reassign,
max(case when dx_code_id in ('icd9:V61.21', 'icd10:Z61.4', 'icd10:Z61.5', 'icd10:Z69.010') and date >= week_start and date < week_end then 1 end) as dx_counseling_for_child_sexual_abuse,
max(case when (dx_code_id ~ '^(icd10:T18.5)' or dx_code_id in ('icd9:937')) and date >= week_start and date < week_end then 1 end) as dx_foreign_body_in_anus,
max(case when (dx_code_id ~ '^(icd9:649.0|icd10:F17.)' or dx_code_id in ('icd9:305.1', 'icd9:V15.82')) and date >= week_start and date < week_end then 1 end) as dx_smoking_tobacco_use,
max(case when (dx_code_id ~ '^(icd9:305.0|icd10:F10.1)' or dx_code_id in ('icd9:303.90', 'icd9:303.91', 'icd9:303.92', 'icd10:F10.2')) and date >= week_start and date < week_end then 1 end) as dx_alcohol_dependence_abuse,
max(case when dx_code_id ~ '^(icd9:304.0|icd10:F11.|icd9:305.5)' and date >= week_start and date < week_end then 1 end) as dx_opioid_dependence_abuse,
max(case when dx_code_id ~ '^(icd9:304.1|icd9:305.4|icd10:F13.)' and date >= week_start and date < week_end then 1 end) as dx_sed_hypn_anxio_depend_abuse,
max(case when dx_code_id ~ '^(icd9:304.2|icd9:305.6|icd10:F14.)' and date >= week_start and date < week_end then 1 end) as dx_cocaine_dependence_abuse,
max(case when dx_code_id ~ '^(icd9:304.4|icd9:305.7|icd10:F15.)' and date >= week_start and date < week_end then 1 end) as dx_amphet_stim_dependence_abuse,
max(case when dx_code_id ~ '^(icd9:304.9|icd9:292.8|icd10:F19)' and date >= week_start and date < week_end then 1 end) as dx_oth_psycho_or_unspec_subs_depend_abuse
FROM
hiv_rpt.r21_dx_codes T1,
hiv_rpt.r21_week_intervals T2
WHERE T1.patient_id = T2.master_patient_id
GROUP BY patient_id, week_id, week_start
ORDER BY patient_id,week_id;

-------------------
------------------- BMI
-------------------

CREATE TABLE hiv_rpt.r21_bmi_measurements as
SELECT T2.patient_id, T2.date, T2.bmi 
FROM hiv_rpt.r21_index_pats T1
INNER JOIN emr_encounter T2 ON (T1.master_patient_id = T2.patient_id)
WHERE T2.date >= '12-01-2010'
AND T2.date < '01-01-2021'
AND bmi is not null;


CREATE TABLE hiv_rpt.r21_bmi_counts as 
SELECT T1.master_patient_id as patient_id, T1.week_id, T1.week_start,
case when T1.week_id = 0 then max(case when date = max_prior_bmi_date then bmi end)else null end prior_bmi,
max(case when date = max_in_week_bmi_date then bmi end) as bmi
--max(case when T1.week_id = T4.week_id and T2.date = max_in_week_bmi_date then bmi else null end) bmi4
FROM hiv_rpt.r21_week_intervals T1
JOIN hiv_rpt.r21_bmi_measurements T2 ON (T1.master_patient_id = T2.patient_id)
LEFT JOIN (
	select patient_id, week_id, week_start,
	max(case when date < week_start and date >= week_start - interval '12 months' then date end) max_prior_bmi_date
	FROM hiv_rpt.r21_week_intervals T1
	JOIN hiv_rpt.r21_bmi_measurements T2 ON (T1.master_patient_id = T2.patient_id)
	WHERE week_id = 0
	--AND patient_id = 32
	GROUP BY patient_id, week_id, week_start) T3 on (T2.date = T3.max_prior_bmi_date and T1.master_patient_id = T3.patient_id)
LEFT JOIN (
        select patient_id, week_id, week_start,
	max(case when date >= week_start and date < week_end then date end) max_in_week_bmi_date
	FROM hiv_rpt.r21_week_intervals T1
	JOIN hiv_rpt.r21_bmi_measurements T2 ON (T1.master_patient_id = T2.patient_id)
	--AND patient_id = 32
        GROUP BY patient_id, week_id, week_start) T4 on (T2.date = T4.max_in_week_bmi_date and T1.master_patient_id = T4.patient_id and T1.week_id = T4.week_id)
--WHERE T1.master_patient_id = 32
GROUP BY T1.master_patient_id, T1.week_id, T1.week_start
order by week_id;


-------------------
------------------- HIV RISK SCORE
-------------------


-- HIV RISK SCORE FOR WEEK 0 IS COMPUTED VIA FUNCTION THAT IS CALLED FROM SHELL SCRIPT
-- THE FINAL OUTPUT TABLE IS THEN UPDATED WITH THE VALUE OF THE RISK SCORE

-------------------
------------------- BASELINE TABLE
-------------------

CREATE TABLE hiv_rpt.r21_baseline_table as
SELECT
T1.sexual_orientation,
T1.master_patient_id,
T1.week_0,
CASE WHEN T1.age_week_0 > 89 then '90+' else T1.age_week_0::varchar END age_week_0,
T1.sex,
T1.race,
T1.ethnicity,
T1.home_language,
T1.female_partner_yn,
T1.male_partner_yn,
coalesce(T3.prior_gon_chlam_rectal_throat_ever, 0) as prior_gon_chlam_rectal_throat_ever,
null::numeric as risk_score_week_0,
coalesce(T7.prior_gon_cases, 0) as prior_gon_cases,
coalesce(T3.prior_pos_gon_rectal, 0) prior_pos_gon_rectal,
coalesce(T3.prior_pos_gon_oropharynx,0) prior_pos_gon_oropharynx,
coalesce(T3.prior_pos_gon_urog, 0) prior_pos_gon_urog,
coalesce(T7.prior_chlam_cases, 0) as prior_chlam_cases,
coalesce(T3.prior_pos_chlam_rectal, 0) prior_pos_chlam_rectal,
coalesce(T3.prior_pos_chlam_oropharynx, 0) prior_pos_chlam_oropharynx,
coalesce(T3.prior_pos_chlam_urog, 0) prior_pos_chlam_urog,
coalesce(T7.prior_syph_cases, 0) as prior_syph_cases,
coalesce(T8.sti_esp_case_encounters, 0) as sti_esp_case_encounters,
coalesce(T9.number_of_encs, 0) as number_of_encs,
coalesce(T10.prior_hiv_elisa, 0) prior_hiv_elisa,
coalesce(T10.prior_hiv_wb, 0) prior_hiv_wb,
coalesce(T10.prior_hiv_rna_viral, 0) prior_hiv_rna_viral,
coalesce(T10.prior_hiv_pcr, 0) prior_hiv_pcr,
coalesce(T10.prior_hiv_ag_ab, 0) prior_hiv_ag_ab,
coalesce(T4.gon_chlam_syph_tst_episodes, 0) as gon_chlam_syph_tst_episodes,
coalesce(T5.gon_chlam_rectal_tst_episodes, 0) as gon_chlam_rectal_tst_episodes,
coalesce(T6.gon_chlam_orpharynx_tst_episodes , 0) as gon_chlam_orpharynx_tst_episodes,
coalesce(T11.prior_hepc_elisa, 0) as prior_hepc_elisa,
coalesce(T11.prior_hepc_rna, 0) as prior_hepc_rna,
coalesce(T11.prior_pos_hepc_elisa, 0) as prior_pos_hepc_elisa,
coalesce(T11.prior_pos_hepc_rna, 0) as prior_pos_hepc_rna,
coalesce(T11.prior_hepb_surf_ant, 0) as prior_hepb_surf_ant,
coalesce(T11.prior_hepb_viral_dna, 0) as prior_hepb_viral_dna,
coalesce(T11.prior_hepb_core_ag_igm_ab, 0) as prior_hepb_core_ag_igm_ab,
coalesce(T11.prior_pos_hepb_core_ag_igm_ab, 0) as prior_pos_hepb_core_ag_igm_ab,
coalesce(T11.prior_pos_hepb, 0) as prior_pos_hepb,
coalesce(T12.prior_rx_truvada_3m, 0) as prior_rx_truvada_3m,
coalesce(T12.prior_rx_truvada_3m_total_quantity, 0) as prior_rx_truvada_3m_total_quantity,
coalesce(T12.prior_rx_truvada_12m, 0) as prior_rx_truvada_12m,
coalesce(T12.prior_rx_truvada_12m_total_quantity, 0) as prior_rx_truvada_12m_total_quantity,
coalesce(T12.prior_rx_hiv_other, 0) as prior_rx_hiv_other,
coalesce(T12.prior_rx_hiv_other_total_quantity, 0) as prior_rx_hiv_other_total_quantity,
coalesce(T12.prior_rx_bicillin, 0) as prior_rx_bicillin,
coalesce(T12.prior_rx_bicillin_total_quantity, 0) as prior_rx_bicillin_total_quantity,
coalesce(T12.prior_rx_azithromycin, 0) as prior_rx_azithromycin, 
coalesce(T12.prior_rx_azithromycin_total_quantity, 0) as prior_rx_azithromycin_total_quantity,
coalesce(T12.prior_rx_ceftriaxone, 0 ) as prior_rx_ceftriaxone, 
coalesce(T12.prior_rx_ceftriaxone_total_quantity, 0) as prior_rx_ceftriaxone_total_quantity,
coalesce(T12.prior_rx_methadone, 0) as prior_rx_methadone, 
coalesce(T12.prior_rx_methadone_total_quantity, 0) as prior_rx_methadone_total_quantity,
coalesce(T12.prior_rx_suboxone, 0 ) as prior_rx_suboxone, 
coalesce(T12.prior_rx_suboxone_total_quantity, 0) as prior_rx_suboxone_total_quantity,
coalesce(T12.prior_rx_viagara_cilais_or_levitra, 0) as prior_rx_viagara_cilais_or_levitra, 
coalesce(T12.prior_rx_viagara_cilais_or_levitra_total_quantity, 0) as prior_rx_viagara_cilais_or_levitra_total_quantity,
coalesce(T13.prior_dx_hiv, 0) as prior_dx_hiv,
coalesce(T13.prior_dx_abn_anal_cyt, 0) as prior_dx_abn_anal_cyt,
coalesce(T7.prior_hep_b_acute_case, 0) as prior_hep_b_acute_case,
coalesce(T7.prior_hep_c_acute_case, 0) as prior_hep_c_acute_case,
coalesce(T13.prior_dx_anal_dyspl, 0) as prior_dx_anal_dyspl,
coalesce(T13.prior_dx_anal_carc, 0) as prior_dx_anal_carc,
coalesce(T13.prior_dx_syph, 0) as prior_dx_syph,
coalesce(T13.prior_dx_anal_syph, 0) as prior_dx_anal_syph,
coalesce(T13.prior_dx_gonoc_inf_rectum, 0) as prior_dx_gonoc_inf_rectum,
coalesce(T13.prior_dx_gonoc_pharyn,0) as prior_dx_gonoc_pharyn,
coalesce(T13.prior_dx_chlam_inf_rectum, 0) as prior_dx_chlam_inf_rectum,
coalesce(T13.prior_dx_chlam_inf_pharyn, 0) as prior_dx_chlam_inf_pharyn,
coalesce(T13.prior_dx_lymphgranuloma_venereum, 0) as prior_dx_lymphgranuloma_venereum,
coalesce(T13.prior_dx_chancroid, 0) as prior_dx_chancroid,
coalesce(T13.prior_dx_granuloma_inguinale, 0) as prior_dx_granuloma_inguinale,
coalesce(T13.prior_dx_nongonococcal_urethritis, 0) as prior_dx_nongonococcal_urethritis,
coalesce(T13.prior_dx_herpes_simplex_w_complications, 0) as prior_dx_herpes_simplex_w_complications,
coalesce(T13.prior_dx_genital_herpes, 0) as prior_dx_genital_herpes,
coalesce(T13.prior_dx_anogenital_warts, 0) as prior_dx_anogenital_warts,
coalesce(T13.prior_dx_anorectal_ulcer, 0) as prior_dx_anorectal_ulcer,
coalesce(T13.prior_dx_unspecified_std, 0) as prior_dx_unspecified_std,
coalesce(T13.prior_dx_contact_with_or_exposure_to_venereal_disease, 0) as prior_dx_contact_with_or_exposure_to_venereal_disease,
coalesce(T13.prior_dx_high_risk_sexual_behavior, 0) as prior_dx_high_risk_sexual_behavior,
coalesce(T13.prior_dx_hiv_counseling, 0) as prior_dx_hiv_counseling,
coalesce(T13.prior_dx_anorexia_nervosa, 0) as prior_dx_anorexia_nervosa,
coalesce(T13.prior_dx_bulimia_nervosa, 0) as prior_dx_bulimia_nervosa,
coalesce(T13.prior_dx_eating_disorder_nos, 0) as prior_dx_eating_disorder_nos,
coalesce(T13.prior_dx_depression, 0) as prior_dx_depression,
coalesce(T13.prior_dx_attn_def_disorder, 0) as prior_dx_attn_def_disorder,
coalesce(T13.prior_dx_gend_iden_trans_sex_reassign, 0) as prior_dx_gend_iden_trans_sex_reassign,
--coalesce(T13.prior_dx_transsexualism, 0) as prior_dx_transsexualism,
coalesce(T13.prior_dx_counseling_for_child_sexual_abuse, 0) as prior_dx_counseling_for_child_sexual_abuse,
coalesce(T13.prior_dx_foreign_body_in_anus, 0) as prior_dx_foreign_body_in_anus,
coalesce(T13.prior_dx_smoking_tobacco_use, 0) as prior_dx_smoking_tobacco_use,
coalesce(T13.prior_dx_alcohol_dependence_abuse, 0) as prior_dx_alcohol_dependence_abuse,
coalesce(T13.prior_dx_opioid_dependence_abuse, 0) as prior_dx_opioid_dependence_abuse,
coalesce(T13.prior_dx_sed_hypn_anxio_depend_abuse, 0) as prior_dx_sed_hypn_anxio_depend_abuse,
coalesce(T13.prior_dx_cocaine_dependence_abuse, 0) as prior_dx_cocaine_dependence_abuse,
coalesce(T13.prior_dx_amphet_stim_dependence_abuse, 0) as prior_dx_amphet_stim_dependence_abuse,
coalesce(T13.prior_dx_oth_psycho_or_unspec_subs_depend_abuse, 0) as prior_dx_oth_psycho_or_unspec_subs_depend_abuse,
coalesce(T14.prior_bmi, 0) as prior_bmi
FROM hiv_rpt.r21_index_pats T1
JOIN hiv_rpt.r21_week_intervals T2 on (T1.master_patient_id = T2.master_patient_id and T2.week_id = 0)
LEFT JOIN hiv_rpt.r21_gon_chlam_counts T3 on (T1.master_patient_id = T3.patient_id and T3.week_id = T2.week_id)
LEFT JOIN hiv_rpt.r21_sti_tst_episodes T4 on (T1.master_patient_id = T4.patient_id)
LEFT JOIN hiv_rpt.r21_gon_chlam_rectal_tst_episodes T5 on (T1.master_patient_id = T5.patient_id)
LEFT JOIN hiv_rpt.r21_gon_chlam_throat_tst_episodes T6 on (T1.master_patient_id = T6.patient_id)
LEFT JOIN hiv_rpt.r21_case_counts T7 on (T1.master_patient_id = T7.patient_id and T7.week_id = T2.week_id)
LEFT JOIN hiv_rpt.sti_case_encs T8 on (T1.master_patient_id = T8.patient_id)
LEFT JOIN hiv_rpt.r21_encounters T9 on (T1.master_patient_id = T9.patient_id and T9.week_id = T2.week_id)
LEFT JOIN hiv_rpt.r21_hiv_counts T10 on (T1.master_patient_id = T10.patient_id and T10.week_id = T2.week_id)
LEFT JOIN hiv_rpt.r21_hep_counts T11 on (T1.master_patient_id = T11.patient_id and T11.week_id = T2.week_id)
LEFT JOIN hiv_rpt.r21_rx_of_int_counts T12 on (T1.master_patient_id = T12.patient_id and T12.week_id = T2.week_id)
LEFT JOIN hiv_rpt.r21_dx_counts T13 on (T1.master_patient_id = T13.patient_id and T13.week_id = T2.week_id)
LEFT JOIN hiv_rpt.r21_bmi_counts T14 on (T1.master_patient_id = T14.patient_id and T14.week_id = T2.week_id)
--WHERE T1.master_patient_id in (1688763, 38662886, 9614, 7028, 145366)
ORDER BY T1.master_patient_id, T2.week_id;


-------------------
------------------- OUTPUT TABLE
-------------------

CREATE TABLE hiv_rpt.r21_output_table as
SELECT T2.*,
case when T1.week_start = end_of_followup_week then 1 else null end outcome_and_censoring,
T1.week_id,
T1.week_start,
coalesce(T6.any_enc, 0) any_enc,
coalesce(T7.any_hiv_elisa, 0) any_hiv_elisa,
coalesce(T7.pos_hiv_elisa, 0) pos_hiv_elisa,
coalesce(T7.any_hiv_wb, 0) any_hiv_wb,
coalesce(T7.pos_hiv_wb, 0) pos_hiv_wb,
coalesce(T7.any_hiv_rna_viral, 0) any_hiv_rna_viral,
coalesce(T7.pos_hiv_rna_viral, 0) pos_hiv_rna_viral,
coalesce(T7.any_hiv_ag_ab, 0) any_hiv_ag_ab,
coalesce(T7.pos_hiv_ag_ab, 0) pos_hiv_ag_ab,
coalesce(T7.any_hiv_pcr, 0) any_hiv_pcr,
coalesce(T7.pos_hiv_pcr, 0) pos_hiv_pcr,
coalesce(T3.any_gon_rectal, 0) any_gon_rectal,
coalesce(T3.pos_gon_rectal, 0) pos_gon_rectal,
coalesce(T3.any_gon_throat, 0) any_gon_oropharynx,
coalesce(T3.pos_gon_throat, 0) pos_gon_oropharynx,
coalesce(T3.any_gon_urog, 0) any_gon_urog,
coalesce(T3.pos_gon_urog, 0) pos_gon_urog,
coalesce(T3.any_chlam_rectal, 0) any_chlam_rectal,
coalesce(T3.pos_chlam_rectal, 0) pos_chlam_rectal, 
coalesce(T3.any_chlam_throat, 0) any_chlam_oropharynx,
coalesce(T3.pos_chlam_throat, 0) pos_chlam_oropharynx, 
coalesce(T3.any_chlam_urog, 0) any_chlam_urog,
coalesce(T3.pos_chlam_urog, 0) pos_chlam_urog,
coalesce(T4.any_syph, 0) any_syph,
coalesce(T5.esp_syph_case, 0) esp_syph_case,
coalesce(T8.any_hepc_elisa, 0) any_hepc_elisa,
coalesce(T8.any_hepc_rna, 0) any_hepc_rna,
coalesce(T8.pos_hepc_elisa, 0) pos_hepc_elisa,
coalesce(T8.pos_hepc_rna, 0) pos_hepc_rna,
coalesce(T8.any_hepb_surf_ant, 0) any_hepb_surf_ant,
coalesce(T8.any_hepb_viral_dna, 0) any_hepb_viral_dna,
coalesce(T8.any_hepb_core_ag_igm_ab, 0) any_hepb_core_ag_igm_ab,
coalesce(T8.pos_hepb_core_ag_igm_ab, 0) pos_hepb_core_ag_igm_ab,
coalesce(T8.pos_hepb, 0) pos_hepb,
coalesce(T9.rx_truvada, 0) rx_truvada,
coalesce(T9.rx_truvada_total_quantity, 0) rx_truvada_total_quantity,
coalesce(T9.rx_hiv_other, 0) rx_hiv_other,
coalesce(T9.rx_hiv_other_total_quantity, 0) rx_hiv_other_total_quantity,
coalesce(T9.rx_bicillin, 0) as rx_bicillin, 
coalesce(T9.rx_bicillin_total_quantity, 0) as rx_bicillin_total_quantity,
coalesce(T9.rx_azithromycin, 0) as rx_azithromycin, 
coalesce(T9.rx_azithromycin_total_quantity, 0) as rx_azithromycin_total_quantity,
coalesce(T9.rx_ceftriaxone, 0 ) as rx_ceftriaxone, 
coalesce(T9.rx_ceftriaxone_total_quantity, 0) as rx_ceftriaxone_total_quantity,
coalesce(T9.rx_methadone, 0) as rx_methadone, 
coalesce(T9.rx_methadone_total_quantity, 0) as rx_methadone_total_quantity,
coalesce(T9.rx_suboxone, 0 ) as rx_suboxone, 
coalesce(T9.rx_suboxone_total_quantity, 0) as rx_suboxone_total_quantity,
coalesce(T9.rx_viagara_cilais_or_levitra, 0) as rx_viagara_cilais_or_levitra, 
coalesce(T9.rx_viagara_cilais_or_levitra_total_quantity, 0) as rx_viagara_cilais_or_levitra_total_quantity,
coalesce(T10.dx_hiv, 0) as dx_hiv,
coalesce(T10.dx_abn_anal_cyt, 0) as dx_abn_anal_cyt,
coalesce(T5.esp_hep_b_acute_case, 0) as esp_hep_b_acute_case,
coalesce(T5.esp_hep_c_acute_case, 0) as esp_hep_c_acute_case,
coalesce(T10.dx_anal_dyspl, 0) as dx_anal_dyspl,
coalesce(T10.dx_anal_carc, 0) as dx_anal_carc,
coalesce(T10.dx_syph, 0) as dx_syph,
coalesce(T10.dx_anal_syph, 0) as dx_anal_syph,
coalesce(T10.dx_gonoc_inf_rectum, 0) as dx_gonoc_inf_rectum,
coalesce(T10.dx_gonoc_pharyn,0) as dx_gonoc_pharyn,
coalesce(T10.dx_chlam_inf_rectum, 0) as dx_chlam_inf_rectum,
coalesce(T10.dx_chlam_inf_pharyn, 0) as dx_chlam_inf_pharyn,
coalesce(T10.dx_lymphgranuloma_venereum, 0) as dx_lymphgranuloma_venereum,
coalesce(T10.dx_chancroid, 0) as dx_chancroid,
coalesce(T10.dx_granuloma_inguinale, 0) as dx_granuloma_inguinale,
coalesce(T10.dx_nongonococcal_urethritis, 0) as dx_nongonococcal_urethritis,
coalesce(T10.dx_herpes_simplex_w_complications, 0) as dx_herpes_simplex_w_complications,
coalesce(T10.dx_genital_herpes, 0) as dx_genital_herpes,
coalesce(T10.dx_anogenital_warts, 0) as dx_anogenital_warts,
coalesce(T10.dx_anorectal_ulcer, 0) as dx_anorectal_ulcer,
coalesce(T10.dx_unspecified_std, 0) as dx_unspecified_std,
coalesce(T10.dx_contact_with_or_exposure_to_venereal_disease, 0) as dx_contact_with_or_exposure_to_venereal_disease,
coalesce(T10.dx_high_risk_sexual_behavior, 0) as dx_high_risk_sexual_behavior,
coalesce(T10.dx_hiv_counseling, 0) as dx_hiv_counseling,
coalesce(T10.dx_anorexia_nervosa, 0) as dx_anorexia_nervosa,
coalesce(T10.dx_bulimia_nervosa, 0) as dx_bulimia_nervosa,
coalesce(T10.dx_eating_disorder_nos, 0) as dx_eating_disorder_nos,
coalesce(T10.dx_depression, 0) as dx_depression,
coalesce(T10.dx_attn_def_disorder, 0) as dx_attn_def_disorder,
coalesce(T10.dx_gend_iden_trans_sex_reassign, 0) as dx_gend_iden_trans_sex_reassign,
--coalesce(T10.dx_transsexualism, 0) as dx_transsexualism,
coalesce(T10.dx_counseling_for_child_sexual_abuse, 0) as dx_counseling_for_child_sexual_abuse,
coalesce(T10.dx_foreign_body_in_anus, 0) as dx_foreign_body_in_anus,
coalesce(T10.dx_smoking_tobacco_use, 0) as dx_smoking_tobacco_use,
coalesce(T10.dx_alcohol_dependence_abuse, 0) as dx_alcohol_dependence_abuse,
coalesce(T10.dx_opioid_dependence_abuse, 0) as dx_opioid_dependence_abuse,
coalesce(T10.dx_sed_hypn_anxio_depend_abuse, 0) as dx_sed_hypn_anxio_depend_abuse,
coalesce(T10.dx_cocaine_dependence_abuse, 0) as dx_cocaine_dependence_abuse,
coalesce(T10.dx_amphet_stim_dependence_abuse, 0) as dx_amphet_stim_dependence_abuse,
coalesce(T10.dx_oth_psycho_or_unspec_subs_depend_abuse, 0) as dx_oth_psycho_or_unspec_subs_depend_abuse,
coalesce(T11.bmi, 0) as bmi
FROM
hiv_rpt.r21_week_intervals T1
JOIN hiv_rpt.r21_baseline_table T2 ON (T1.master_patient_id = T2.master_patient_id)
LEFT JOIN hiv_rpt.r21_gon_chlam_counts T3 ON (T1.master_patient_id = T3.patient_id and T3.week_id = T1.week_id)
LEFT JOIN hiv_rpt.r21_syph_counts T4 ON (T1.master_patient_id = T4.patient_id and T4.week_id = T1.week_id)
LEFT JOIN hiv_rpt.r21_case_counts T5 ON (T1.master_patient_id = T5.patient_id and T5.week_id = T1.week_id)
LEFT JOIN hiv_rpt.r21_encounters T6 on (T1.master_patient_id = T6.patient_id and T6.week_id = T1.week_id)
LEFT JOIN hiv_rpt.r21_hiv_counts T7 on (T1.master_patient_id = T7.patient_id and T7.week_id = T1.week_id)
LEFT JOIN hiv_rpt.r21_hep_counts T8 on (T1.master_patient_id = T8.patient_id and T8.week_id = T1.week_id)
LEFT JOIN hiv_rpt.r21_rx_of_int_counts T9 on (T1.master_patient_id = T9.patient_id and T9.week_id = T1.week_id)
LEFT JOIN hiv_rpt.r21_dx_counts T10 on (T1.master_patient_id = T10.patient_id and T10.week_id = T1.week_id)
LEFT JOIN hiv_rpt.r21_bmi_counts T11 on (T1.master_patient_id = T11.patient_id and T11.week_id = T1.week_id)
;

-- CREATE INDEX
DROP INDEX IF EXISTS hiv_rpt.r21_output_table_master_patient_id_week_0_idx;
CREATE INDEX r21_output_table_master_patient_id_week_0_idx
  ON hiv_rpt.r21_output_table
  USING btree
  (master_patient_id, week_0);

-- CREATE A SEQUENCE FOR MASKING PATIENT ID's
CREATE SEQUENCE hiv_rpt.r21_masked_id_seq;

-- GENERATE A MASKED ID FOR ALL OUTPUT PATIENTS
CREATE TABLE hiv_rpt.r21_masked_patients as
SELECT master_patient_id,
nextval(('hiv_rpt.r21_masked_id_seq'::text)::regclass) masked_patient_id
from hiv_rpt.r21_output_table
group by master_patient_id;

-- CREATE INDEX
DROP INDEX IF EXISTS hiv_rpt.r21_masked_patients_master_patient_id_masked_patient_id_idx;

CREATE INDEX r21_masked_patients_master_patient_id_masked_patient_id_idx
  ON hiv_rpt.r21_masked_patients
  USING btree
  (master_patient_id, masked_patient_id);



-- CREATE THE OUTPUT TABLE WITH MASKED ID VALUES
CREATE TABLE hiv_rpt.r21_output_table_masked as
SELECT
sexual_orientation,
masked_patient_id,
--just print month and year
(date_trunc('month', week_0))::date week_0,
age_week_0,
sex,
race,
ethnicity,
home_language,
female_partner_yn,
male_partner_yn,
prior_gon_chlam_rectal_throat_ever,
risk_score_week_0,
prior_gon_cases,
prior_pos_gon_rectal,
prior_pos_gon_oropharynx,
prior_pos_gon_urog,
prior_chlam_cases,
prior_pos_chlam_rectal,
prior_pos_chlam_oropharynx,
prior_pos_chlam_urog,
prior_syph_cases,
sti_esp_case_encounters,
number_of_encs,
prior_hiv_elisa,
prior_hiv_wb,
prior_hiv_rna_viral,
prior_hiv_pcr,
prior_hiv_ag_ab,
gon_chlam_syph_tst_episodes,
gon_chlam_rectal_tst_episodes,
gon_chlam_orpharynx_tst_episodes,
prior_hepc_elisa,
prior_hepc_rna,
prior_pos_hepc_elisa,
prior_pos_hepc_rna,
prior_hepb_surf_ant,
prior_hepb_viral_dna,
prior_hepb_core_ag_igm_ab,
prior_pos_hepb_core_ag_igm_ab,
prior_pos_hepb,
prior_rx_truvada_3m,
prior_rx_truvada_3m_total_quantity,
prior_rx_truvada_12m,
prior_rx_truvada_12m_total_quantity,
prior_rx_hiv_other,
prior_rx_hiv_other_total_quantity,
prior_rx_bicillin,
prior_rx_bicillin_total_quantity,
prior_rx_azithromycin,
prior_rx_azithromycin_total_quantity,
prior_rx_ceftriaxone,
prior_rx_ceftriaxone_total_quantity,
prior_rx_methadone,
prior_rx_methadone_total_quantity,
prior_rx_suboxone,
prior_rx_suboxone_total_quantity,
prior_rx_viagara_cilais_or_levitra,
prior_rx_viagara_cilais_or_levitra_total_quantity,
prior_dx_hiv,
prior_dx_abn_anal_cyt,
prior_hep_b_acute_case,
prior_hep_c_acute_case,
prior_dx_anal_dyspl,
prior_dx_anal_carc,
prior_dx_syph,
prior_dx_anal_syph,
prior_dx_gonoc_inf_rectum,
prior_dx_gonoc_pharyn,
prior_dx_chlam_inf_rectum,
prior_dx_chlam_inf_pharyn,
prior_dx_lymphgranuloma_venereum,
prior_dx_chancroid,
prior_dx_granuloma_inguinale,
prior_dx_nongonococcal_urethritis,
prior_dx_herpes_simplex_w_complications,
prior_dx_genital_herpes,
prior_dx_anogenital_warts,
prior_dx_anorectal_ulcer,
prior_dx_unspecified_std,
prior_dx_contact_with_or_exposure_to_venereal_disease,
prior_dx_high_risk_sexual_behavior,
prior_dx_hiv_counseling,
prior_dx_anorexia_nervosa,
prior_dx_bulimia_nervosa,
prior_dx_eating_disorder_nos,
prior_dx_depression,
prior_dx_attn_def_disorder,
prior_dx_gend_iden_trans_sex_reassign,
prior_dx_counseling_for_child_sexual_abuse,
prior_dx_foreign_body_in_anus,
prior_dx_smoking_tobacco_use,
prior_dx_alcohol_dependence_abuse,
prior_dx_opioid_dependence_abuse,
prior_dx_sed_hypn_anxio_depend_abuse,
prior_dx_cocaine_dependence_abuse,
prior_dx_amphet_stim_dependence_abuse,
prior_dx_oth_psycho_or_unspec_subs_depend_abuse,
prior_bmi,
outcome_and_censoring,
week_id,
-- just print month and year 
(date_trunc('month', week_start))::date week_start,
any_enc,
any_hiv_elisa,
pos_hiv_elisa,
any_hiv_wb,
pos_hiv_wb,
any_hiv_rna_viral,
pos_hiv_rna_viral,
any_hiv_ag_ab,
pos_hiv_ag_ab,
any_hiv_pcr,
pos_hiv_pcr,
any_gon_rectal,
pos_gon_rectal,
any_gon_oropharynx,
pos_gon_oropharynx,
any_gon_urog,
pos_gon_urog,
any_chlam_rectal,
pos_chlam_rectal,
any_chlam_oropharynx,
pos_chlam_oropharynx,
any_chlam_urog,
pos_chlam_urog,
any_syph,
esp_syph_case,
any_hepc_elisa,
any_hepc_rna,
pos_hepc_elisa,
pos_hepc_rna,
any_hepb_surf_ant,
any_hepb_viral_dna,
any_hepb_core_ag_igm_ab,
pos_hepb_core_ag_igm_ab,
pos_hepb,
rx_truvada,
rx_truvada_total_quantity,
rx_hiv_other,
rx_hiv_other_total_quantity,
rx_bicillin,
rx_bicillin_total_quantity,
rx_azithromycin,
rx_azithromycin_total_quantity,
rx_ceftriaxone,
rx_ceftriaxone_total_quantity,
rx_methadone,
rx_methadone_total_quantity,
rx_suboxone,
rx_suboxone_total_quantity,
rx_viagara_cilais_or_levitra,
rx_viagara_cilais_or_levitra_total_quantity,
dx_hiv,
dx_abn_anal_cyt,
esp_hep_b_acute_case,
esp_hep_c_acute_case,
dx_anal_dyspl,
dx_anal_carc,
dx_syph,
dx_anal_syph,
dx_gonoc_inf_rectum,
dx_gonoc_pharyn,
dx_chlam_inf_rectum,
dx_chlam_inf_pharyn,
dx_lymphgranuloma_venereum,
dx_chancroid,
dx_granuloma_inguinale,
dx_nongonococcal_urethritis,
dx_herpes_simplex_w_complications,
dx_genital_herpes,
dx_anogenital_warts,
dx_anorectal_ulcer,
dx_unspecified_std,
dx_contact_with_or_exposure_to_venereal_disease,
dx_high_risk_sexual_behavior,
dx_hiv_counseling,
dx_anorexia_nervosa,
dx_bulimia_nervosa,
dx_eating_disorder_nos,
dx_depression,
dx_attn_def_disorder,
dx_gend_iden_trans_sex_reassign,
dx_counseling_for_child_sexual_abuse,
dx_foreign_body_in_anus,
dx_smoking_tobacco_use,
dx_alcohol_dependence_abuse,
dx_opioid_dependence_abuse,
dx_sed_hypn_anxio_depend_abuse,
dx_cocaine_dependence_abuse,
dx_amphet_stim_dependence_abuse,
dx_oth_psycho_or_unspec_subs_depend_abuse,
bmi
FROM hiv_rpt.r21_output_table T1,
hiv_rpt.r21_masked_patients T2
WHERE T1.master_patient_id = T2.master_patient_id;

--CREATE INDEX
DROP INDEX IF EXISTS hiv_rpt.r21_output_table_masked_masked_patient_id_week_0_idx;
CREATE INDEX r21_output_table_masked_masked_patient_id_week_0_idx
  ON hiv_rpt.r21_output_table_masked
  USING btree
  (masked_patient_id, week_0);



--\COPY (select * from hiv_rpt.r21_output_table_masked order by masked_patient_id, week_id ) TO '/tmp/2020-03-02-cha-r21-output.csv' WITH CSV HEADER;
--\COPY (select * from hiv_rpt.r21_output_table_masked order by masked_patient_id, week_id ) TO '/tmp/2021-04-12-atrius-r21-output.csv' WITH CSV HEADER;















