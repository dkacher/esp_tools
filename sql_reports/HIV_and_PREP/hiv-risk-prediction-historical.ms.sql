--set client_min_messages to error;

IF object_id('dbo.hrp_hist_coefficients', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_coefficients;
IF object_id('dbo.hrp_hist_index_patients', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_index_patients;
IF object_id('dbo.hrp_hist_pat_encs', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_pat_encs;
IF object_id('dbo.hrp_hist_hiv_labs', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_hiv_labs;
IF object_id('dbo.hrp_hist_hiv_lab_values', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_hiv_lab_values;
IF object_id('dbo.hrp_hist_gon_chlam_events', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_gon_chlam_events;
IF object_id('dbo.hrp_hist_gon_chlam_values', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_gon_chlam_values;
IF object_id('dbo.hrp_hist_diags', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_diags;
IF object_id('dbo.hrp_hist_diags_values', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_diags_values;
IF object_id('dbo.hrp_hist_bicillin', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_bicillin;
IF object_id('dbo.hrp_hist_bicillin_subset', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_bicillin_subset;
IF object_id('dbo.hrp_hist_bicillin_values', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_bicillin_values;
IF object_id('dbo.hrp_hist_suboxone', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_suboxone;
IF object_id('dbo.hrp_hist_suboxone_values', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_suboxone_values;
IF object_id('dbo.hrp_hist_truvada_values', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_truvada_values;
IF object_id('dbo.hrp_hist_truvada', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_truvada;
IF object_id('dbo.hrp_hist_syph_events', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_syph_events;
IF object_id('dbo.hrp_hist_syph_max_dates', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_syph_max_dates;
IF object_id('dbo.hrp_hist_pat_sum_x_value', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_pat_sum_x_value;
IF object_id('dbo.hrp_hist_index_data', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_index_data;
IF object_id('dbo.hrp_hist_risk_scores', 'U') IS NOT NULL DROP TABLE dbo.hrp_hist_risk_scores;


CREATE TABLE hrp_hist_coefficients 
  (sex real, 
   race_black real,
   race_caucasian real,
   english_lang real,
   has_language_data real,
   years_of_data real,
   has_1yr_data real,
   has_2yr_data real,
   t_hiv_rna_1_yr real,
   t_hiv_test_2yr real,
   t_hiv_test_e real,
   t_hiv_elisa_e real,
   acute_hiv_test_e real,
   acute_hiv_test_2yr real,
   t_pos_gon_test_2yr real,
   t_chla_test_t_e real,
   rx_bicillin_1_yr real,
   rx_bicillin_2yr real,
   rx_bicillin_e real,
   rx_suboxone_2yr real,
   syphilis_any_site_state_x_late_e real,
   contact_w_or_expo_venereal_dis_e real,
   hiv_counseling_2yr real);
INSERT INTO hrp_hist_coefficients 
   (sex, race_black, race_caucasian, english_lang, has_language_data, years_of_data, has_1yr_data, has_2yr_data, t_hiv_rna_1_yr,
   t_hiv_test_2yr, t_hiv_test_e, t_hiv_elisa_e, acute_hiv_test_e, acute_hiv_test_2yr, t_pos_gon_test_2yr, t_chla_test_t_e,
   rx_bicillin_1_yr, rx_bicillin_2yr, rx_bicillin_e, rx_suboxone_2yr, syphilis_any_site_state_x_late_e, contact_w_or_expo_venereal_dis_e,
   hiv_counseling_2yr) 
   VALUES 
   (1.00000000, 1.06186066, -0.6609999, -0.4213635, -0.0784053, -0.0660395, -0.6311702, -0.4035768, 0.14676104, 0.23349732, 0.12024552,
    0.16006917, 1.82082955, 0.15925514, 3.07004984, -0.154315, 1.35516827, 0.20795003, 1.79771045, 0.19721102, 0.9997348, 0.28886449,
    1.09544347);


-- DELETE ALL EXISTING ENTRIES FOR YEAR BEING COMPUTED
DELETE FROM gen_pop_tools.cc_hiv_risk_score where rpt_year = substr($(end_calc_year), 1, 4);


-- ONLY COMPUTING FOR PATIENTS THAT HAD Clinical ENCOUNTER IN THE 2 YEARS PRIOR TO RUN YEAR
SELECT T1.id as patient_id,
substr($(end_calc_year), 1, 4) rpt_year,
CASE WHEN upper(gender) in ('F', 'FEMALE') then -9.374485
     WHEN upper(gender) in ('M', 'MALE') then -7.508866
     END sex,
CASE WHEN upper(race) = 'BLACK' then 1 ELSE 0 END race_black,
CASE WHEN upper(race) in ('WHITE', 'CAUCASIAN') then 1 ELSE 0 END race_caucasian,
CASE WHEN upper(home_language) = 'ENGLISH' then 1 ELSE 0 END english_lang,
CASE WHEN upper(home_language) not in ('DECLINED', 'UNKNOWN', 'UNABLE TO BE DETERMINED', 'NONE', '') 
          and upper(home_language) is not null then 1 else 0 END has_language_data
INTO hrp_hist_index_patients
FROM emr_patient T1
JOIN emr_encounter T2 on (T1.id = T2.patient_id) 
LEFT JOIN gen_pop_tools.clin_enc T3 on (T2.patient_id = T3.patient_id and t2.date=t3.date)
WHERE upper(gender) in ('M', 'MALE', 'FEMALE', 'F')
-- must have a Clinical encounter in the past 2 years
AND T2.date >= dateadd(year, -2, convert(DATETIME, $(end_calc_year), 23))
-- must be at least age 15
AND datediff(year, date_of_birth, convert(DATETIME, $(end_calc_year), 23)) >= 15
-- don't compute for known hiv patients
AND T1.id NOT IN (select patient_id from nodis_case where condition = 'hiv' and date <= convert(DATETIME, $(end_calc_year), 23))
-- filter out test patients
AND T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%'
AND T1.last_name not ilike 'XYZ%'
AND T1.last_name not ilike 'ZZ%'
AND T1.last_name not ilike 'YY%'
AND T1.last_name not ilike 'test'
AND T1.last_name not ilike 'esp'
AND T1.first_name NOT ILIKE 'test'
AND T1.first_name not ilike 'yytest%'
AND T1.first_name not ILIKE 'zztest%'
AND T2.date <= convert(DATETIME, $(end_calc_year), 23)
GROUP BY T1.id, rpt_year, gender, race, home_language;


-- years_of_data 
-- length of available EHR history in years (based on encounter of specific type)
-- # has_1yr_data	1 if EHR history exists for previous year, 0 otherwise	-0.6311702
-- # has_2yr_data	1 if EHR history exists for previous 2 years, 0 otherwise	-0.4035768
SELECT patient_id, (max(abs(datediff(year, convert(DATETIME, $(end_calc_year), 23), getdate() )))  + 1) years_of_data,
max(CASE WHEN abs(datediff(year, convert(DATETIME, $(end_calc_year), 23), getdate() )) = 0 then 1 END) has_1yr_data,
max(CASE WHEN abs(datediff(year, convert(DATETIME, $(end_calc_year), 23), getdate() )) = 1 then 1 END) has_2yr_data
into hrp_hist_pat_encs
FROM emr_encounter T1,
JOIN gen_pop_tools.clin_enc T2 on t1.patient_id=t2.patient_id and t1.date=t2.date
WHERE T1.DATE >= convert(DATETIME,'2006-01-01', 23) 
AND T1.DATE <= $(end_calc_year)
GROUP BY T1.patient_id;

-- Gather up all of the HIV lab tests
-- (ELISA or Ab/Ag or RNA) 
-- NEED TO LIMIT TO THESE TEST TYPES TO ELISA, HIV_RNA_VIRAL, HIV_AG_AB
-- NEED TO GRAB ALL TESTS NOT JUST THOSE WITH A HEF EVENT OR ELSE YOU WILL MISS VIRAL LOAD TESTS
SELECT T1.patient_id, T2.test_name, YEAR(T1.date) rpt_year, 
 abs(datediff(year, T1.date, convert(DATETIME, $(end_calc_year), 23))) yrs_since_test, 
 T1.date, result_string, name as hef_name, concat(test_name, ':' , result_string) as result_data
into hrp_hist_hiv_labs
FROM emr_labresult T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
LEFT JOIN hef_event T3 ON (T1.id = T3.object_id and T1.patient_id = T3.patient_id)
WHERE test_name in ('hiv_elisa', 'hiv_rna_viral', 'hiv_ag_ab')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined', 'Not Done', 'Not performed',  'Not tested', 'Not Tested', 'test')
AND result_string not ilike '%cancelled%'
AND result_string is not null
AND T1.date >= convert(DATETIME,'2006-01-01', 23) 
AND T1.date <= convert(DATETIME, $(end_calc_year), 23);

-- Compute HIV test values
-- number of HIV RNA tests in the previous year
-- total number of HIV tests in past 2 years (regardless of test result)
-- total number of HIV tests ever (ELISA or Ab/Ag or RNA)
-- total number of HIV ELISA or Ab/Ag tests ever
-- 1 if had an HIV RNA test in past 2 years, 0 otherwise
-- 1 if had an HIV RNA test ever, 0 otherwise
SELECT
patient_id,
count(CASE WHEN yrs_since_test = 0 AND test_name = 'hiv_rna_viral' THEN 1 END) t_hiv_rna_1_yr,
count(CASE WHEN yrs_since_test <= 1 THEN 1 END) t_hiv_test_2yr,
count(*) t_hiv_test_e,
count(CASE WHEN test_name in ('hiv_elisa', 'hiv_ag_ab') THEN 1 END) t_hiv_elisa_e,
max(CASE WHEN yrs_since_test <= 1 AND test_name = 'hiv_rna_viral' THEN 1 END) acute_hiv_test_2yr,
max(CASE WHEN test_name = 'hiv_rna_viral' THEN 1 END) acute_hiv_test_e,
-- ADDED FOR COC
max(CASE WHEN rpt_year = YEAR(convert(DATETIME, $(end_calc_year), 23)) then 1 END) hiv_test_this_yr
into hrp_hist_hiv_lab_values
FROM hrp_hist_hiv_labs
GROUP BY patient_id;

-- Gather up labs hef events for chlamydia and gonorrhea 
SELECT T1.id,T2.test_name as condition, T3.name as hef_name,T1.patient_id,T1.date, 
    abs(datediff(year, convert(DATETIME, $(end_calc_year), 23), T1.date)) yrs_since_test, result_string
into hrp_hist_gon_chlam_events 
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
INNER JOIN hef_event T3 ON ((T1.id = T3.object_id) AND (T1.patient_id = T3.patient_id))
WHERE T1.date >= convert(DATETIME,'2006-01-01', 23)
AND T1.date <= convert(DATETIME, $(end_calc_year), 23)
AND T2.test_name in ('chlamydia', 'gonorrhea');


-- Compute Chlamydia and Gonorrhea values
-- # t_pos_gon_test_2yr	total number of positive gonorrhea tests in past 2 years
-- # t_chla_test_t_e	total number of chlamydia tests ever
SELECT 
patient_id,
count(CASE WHEN hef_name = 'lx:gonorrhea:positive' AND yrs_since_test <= 1 THEN 1 END) t_pos_gon_test_2yr,
count(CASE WHEN condition = 'chlamydia' THEN 1 END) t_chla_test_t_e
into hrp_hist_gon_chlam_values
FROM hrp_hist_gon_chlam_events
GROUP BY patient_id;

-- Gather up diagnosis codes of interest
-- # syphilis_any_site_state_x_late_e	1 if syphilis ever diagnosed, 0 otherwise (icd9:091.*, icd9:092.*, icd9:093.*, icd9:094.*, icd9:095.*, icd10:A51.*, icd9:097.9, icd10:A52.0, icd10:A52.7) 
-- # contact_w_or_expo_venereal_dis_e	1 if ever had contact or exposure to venereal disease, 0 otherwise (ICD9=V01.6 or ICD10=Z20.2 or Z20.6 ever)	
-- # hiv_counseling_2yr	1 if counseled for HIV in past 2 years, 0 otherwise (ICD9=V65.44 or ICD10=Z71.7)
SELECT T2.patient_id, T1.id, T1.encounter_id, T1.dx_code_id, abs(datediff(year, convert(DATETIME, $(end_calc_year), 23), T2.date)) yrs_since_diag
into hrp_hist_diags
FROM emr_encounter_dx_codes T1, 
emr_encounter T2
WHERE 
T1.encounter_id = T2.id
AND (dx_code_id like 'icd9:091.%' or dx_code_id like 'icd9:092.%' or dx_code_id like 'icd9:093.%' or dx_code_id like 'icd9:094.%' 
     or dx_code_id like 'icd9:095.%' or dx_code_id like 'icd10:A51.%' or dx_code_id like 'icd10:A52.%' 
     or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1', 'icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6', 'icd9:V65.44','icd10:Z71.7'))
AND T2.date <= convert(DATETIME, $(end_calc_year), 23);

SELECT T1.patient_id,
MAX(CASE WHEN dx_code_id like 'icd9:091.%' or dx_code_id like 'icd9:092.%' or dx_code_id like 'icd9:093.%' or dx_code_id like 'icd9:094.%' 
  or dx_code_id like 'icd9:095.%' or dx_code_id like 'icd10:A51.%' or dx_code_id like 'icd10:A52.%'  
  or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1') then 1 end) syphilis_any_site_state_x_late_e,
MAX(CASE WHEN dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') then  1 end) contact_w_or_expo_venereal_dis_e,
MAX(CASE WHEN dx_code_id in ('icd9:V65.44','icd10:Z71.7') and yrs_since_diag <=1  then 1 end) hiv_counseling_2yr
into hrp_hist_diags_values
FROM hrp_hist_diags T1
GROUP BY patient_id;


-- # rx_bicillin_1_year	number of prescriptions for bicillin in previous year
-- # rx_bicillin_2yr	total number of prescriptions for bicillin in past 2 years
-- # rx_bicillin_e	total number of prescriptions for bicillin ever	1.79771045
-- Only count 1 rx per date/ per name

SELECT patient_id, name, date, abs(datediff(year, convert(DATETIME, $(end_calc_year), 23), date)) yrs_since_rx
INTO hrp_hist_bicillin 
FROM emr_prescription 
WHERE ( name in (
-- ATRIUS
'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)',
'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE',
'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
--CHA 
'BICILLIN L-A 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE & PROC 1200000 UNIT/2ML IM SUSP',
'PENICILLIN G IVPB IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G IVPB (MINIBAG-PLUS) 5 MILLION UNITS',
'PENICILLIN G IVPB MINIBAG PLUS 5 MILLION UNITS',
'PENICILLIN G POTASSIUM 20000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM 5000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM IN D5W 40000 UNIT/ML IV SOLN',
'PENICILLIN G POTASSIUM IN D5W 60000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 20000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 40000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 60000 UNIT/ML IV SOLN',
'PENICILLIN G PROCAINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 300000-900000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G SODIUM 5000000 IU IJ SOLR',
--BMC
'BICILLIN L-A 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
'PENICILLIN G IVPB  IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G POTASSIUM 20 MILLION UNIT SOLUTION FOR INJECTION',
'PENICILLIN G POTASSIUM 5 MILLION UNIT SOLUTION FOR INJECTION',
'PENICILLIN G POTASSIUM IV 5 MILLION UNITS MBP',
'PENICILLIN G SODIUM 5 MILLION UNIT SOLUTION FOR INJECTION')
-- ATRIUS 
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE', 'BICILLIN L-A DISP SYRIN 600000 U IM (PENICILLIN G BENZATHINE)', 'BICILLIN L-A DISP SYRIN 600MU/1ML IM (PENICILLIN G BENZATHINE)') and quantity_float >= 4)
-- CHA
OR (name in ('BICILLIN L-A 1200000 UNIT/2ML IM SUSP', 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600000 UNIT/ML IM SUSP', 'PENICILLIN G BENZATHINE 600000 UNIT/ML IM SUSP') and quantity_float >= 4)
-- BMC
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML INTRAMUSCULAR SYRINGE') and (dose = '2.4 Million Units' or dose = '2400000 Units')) )
AND date <= convert(DATETIME, $(end_calc_year), 23)
GROUP BY patient_id, name, date;


-- Exclude bicillin prescriptions that match specific dx codes 
-- occurring on the same date
SELECT *
INTO hrp_hist_bicillin_subset
FROM
(SELECT * FROM  hrp_hist_bicillin
EXCEPT
SELECT T1.*
FROM hrp_hist_bicillin T1, 
emr_encounter T2,
emr_encounter_dx_codes T3
where T1.patient_id = T2.patient_id
AND T2.id = T3.encounter_id
AND T3.dx_code_id like  'icd10:A69.2%' or dx_code_id like 'icd10:B95%' or dx_code_id like 'icd10:I00%' or dx_code_id like 'icd10:I01%' 
  or dx_code_id like 'icd10:I02%' or dx_code_id like 'icd10:I05%' or dx_code_id like 'icd10:I06%' or dx_code_id like 'icd10:I07%' 
  or dx_code_id like 'icd10:I08%' or dx_code_id like 'icd10:I09%' or dx_code_id like 'icd10:I89.0%' or dx_code_id like 'icd10:I97.2%' 
  or dx_code_id like 'icd10:I97.89%' or dx_code_id like 'icd10:J02%' or dx_code_id like 'icd10:J03%' or dx_code_id like 'icd10:J36%' 
  or dx_code_id like 'icd10:L03%' or dx_code_id like 'icd10:Q82.0%' or dx_code_id like 'icd10:Z86.7%' or dx_code_id like 'icd9:034.0%' 
  or dx_code_id like 'icd9:041.0%' or dx_code_id like 'icd9:088.81%' or dx_code_id like 'icd9:390%' or dx_code_id like 'icd9:391%' 
  or dx_code_id like 'icd9:392%' or dx_code_id like 'icd9:393%' or dx_code_id like 'icd9:394%' or dx_code_id like 'icd9:395%' 
  or dx_code_id like 'icd9:396%' or dx_code_id like 'icd9:397%' or dx_code_id like 'icd9:398%' or dx_code_id like 'icd9:457%' 
  or dx_code_id like 'icd9:462%' or dx_code_id like 'icd9:463%' or dx_code_id like 'icd9:475%' or dx_code_id like 'icd9:682%' 
  or dx_code_id like 'icd9:757.0%' or dx_code_id like 'icd9:V12.5%'
and T1.date = T2.date
order by patient_id) t;


SELECT T1.patient_id,
COUNT(CASE WHEN yrs_since_rx = 0 THEN 1 END) rx_bicillin_1_yr,
COUNT(CASE WHEN yrs_since_rx <= 1 THEN 1 END )rx_bicillin_2yr,
count(*) rx_bicillin_e
into hrp_hist_bicillin_values
FROM hrp_hist_bicillin_subset T1
GROUP BY patient_id;

-- # rx_suboxone_2yr	total number of prescriptions for suboxone in past 2 years
SELECT patient_id, name, date, abs(datediff(year, convert(DATETIME, $(end_calc_year), 23), date)) yrs_since_rx
into hrp_hist_suboxone
FROM emr_prescription T1
WHERE (T1.name ilike '%suboxone%' 
or T1.name ilike 'buprenorphine%naloxone%' 
or T1.name ilike '%zubsolv%'
or T1.name ilike '%bunavail%'
or T1.name ilike '%cassipa%')
AND date <= convert(DATETIME, $(end_calc_year), 23)
GROUP BY patient_id, name, date;

SELECT T1.patient_id,
COUNT(CASE WHEN yrs_since_rx <= 1 THEN 1 END )rx_suboxone_2yr
into hrp_hist_suboxone_values
FROM hrp_hist_suboxone T1
GROUP BY patient_id;

-- # For COC, has patient had a prescription for Truvada in this year
SELECT patient_id, 1::integer  truvada_rx_this_yr
into hrp_hist_truvada_values
FROM hef_event T1
WHERE T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine')
AND year(T1.date) = year(convert(DATETIME, $(end_calc_year), 23))
AND date <= convert(DATETIME, $(end_calc_year), 23)
GROUP BY patient_id;

-- Get most recent Truvada rx date
SELECT patient_id, max(date) most_recent_truvada_rx
into hrp_hist_truvada
FROM hef_event T1
WHERE T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine')
AND date <= convert(DATETIME, $(end_calc_year), 23)
GROUP BY patient_id;


-- Gather up all syphilis related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
SELECT T1.id,T2.test_name as condition, T3.name as hef_name,T1.patient_id,T1.date, 
  abs(datediff(year, convert(DATETIME, $(end_calc_year), 23), date)) yrs_since_test, 
  result_string, concat(test_name, ':' , result_string) as result_data
into hrp_hist_syph_events
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
LEFT JOIN hef_event T3 ON ((T1.id = T3.object_id) AND (T1.patient_id = T3.patient_id))
WHERE T1.date >= convert(DATETIME, '2006-01-01', 23)
AND T1.date <= convert(DATETIME, $(end_calc_year), 23)
AND T2.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm', 'tp-cia')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined', 'Not Done', 'Not performed',  'Not tested', 'Not Tested', 'test', 'CANCELLED. DUPLICATE REQUEST.')
AND result_string !~ ('CANCELLED')
and result_string is not null;

-- Get date of most recent syph event for each patient
SELECT T1.patient_id, max(date) as syph_recent_date
into hrp_hist_syph_max_dates
FROM hrp_hist_syph_events T1
GROUP BY T1.patient_id;


-- Bring together all of the fields and values
SELECT T1.patient_id,
T1.rpt_year,
sex,
race_black,
race_caucasian,
english_lang,
has_language_data,
coalesce(years_of_data, 0) years_of_data,
coalesce(has_1yr_data, 0) has_1yr_data,
coalesce(has_2yr_data, 0) has_2yr_data,
coalesce(t_hiv_rna_1_yr, 0) t_hiv_rna_1_yr,
coalesce(t_hiv_test_2yr, 0) t_hiv_test_2yr,
coalesce(t_hiv_test_e, 0) t_hiv_test_e,
coalesce(t_hiv_elisa_e, 0) t_hiv_elisa_e,
coalesce(hiv_test_this_yr, 0) hiv_test_this_yr,
coalesce(truvada_rx_this_yr, 0) truvada_rx_this_yr,
coalesce(acute_hiv_test_2yr, 0) acute_hiv_test_2yr,
coalesce(acute_hiv_test_e, 0) acute_hiv_test_e,
coalesce(rx_bicillin_1_yr, 0) rx_bicillin_1_yr,
coalesce(rx_bicillin_2yr, 0) rx_bicillin_2yr,
coalesce(rx_bicillin_e, 0) rx_bicillin_e,
coalesce(rx_suboxone_2yr, 0) rx_suboxone_2yr,
coalesce(t_pos_gon_test_2yr, 0) t_pos_gon_test_2yr,
coalesce(t_chla_test_t_e, 0) t_chla_test_t_e,
coalesce(syphilis_any_site_state_x_late_e, 0) syphilis_any_site_state_x_late_e,
coalesce(contact_w_or_expo_venereal_dis_e, 0) contact_w_or_expo_venereal_dis_e,
coalesce(hiv_counseling_2yr, 0) hiv_counseling_2yr,
coalesce(syph_recent_date, null) syph_last_date,
coalesce(most_recent_truvada_rx, null) most_recent_truvada_rx
into hrp_hist_index_data
FROM hrp_hist_index_patients T1
LEFT JOIN hrp_hist_pat_encs T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN hrp_hist_hiv_lab_values T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN hrp_hist_bicillin_values T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN hrp_hist_suboxone_values T5 ON (T1.patient_id = T5.patient_id)
LEFT JOIN hrp_hist_gon_chlam_values T6 ON (T1.patient_id = T6.patient_id)
LEFT JOIN hrp_hist_diags_values T7 ON (T1.patient_id = T7.patient_id)
LEFT JOIN hrp_hist_truvada_values T8 ON (T1.patient_id = T8.patient_id)
LEFT JOIN hrp_hist_syph_max_dates T9 ON (T1.patient_id = T9.patient_id)
LEFT JOIN hrp_hist_truvada T10 ON (T1.patient_id = T10.patient_id)
;

-- OVERWRITE BICILLIN VALUES FOR PATIENTS WITHOUT OTHER INDICATORS
UPDATE hrp_hist_index_data
SET rx_bicillin_1_yr = 0,
rx_bicillin_2yr = 0,
rx_bicillin_e = 0
WHERE patient_id IN (
	SELECT patient_id
	FROM hrp_hist_index_data
	WHERE
	t_hiv_rna_1_yr = 0 and
	t_hiv_test_2yr = 0 and
	t_hiv_test_e = 0 and
	t_hiv_elisa_e = 0 and
	acute_hiv_test_2yr = 0 and
	acute_hiv_test_e = 0 and
	rx_suboxone_2yr = 0 and
	t_pos_gon_test_2yr = 0 and
	t_chla_test_t_e = 0 and
	syphilis_any_site_state_x_late_e = 0 and
	contact_w_or_expo_venereal_dis_e = 0 and
	hiv_counseling_2yr = 0 and
	syph_last_date is null and
	most_recent_truvada_rx is null and 
	rx_bicillin_e > 0)
;

-- Compute the sum to be used in the risk score calculation
SELECT patient_id, 
(
(i.sex * c.sex) +
(i.race_black * c.race_black) +
(i.race_caucasian * c.race_caucasian) +
(i.english_lang * c.english_lang) +
(i.has_language_data * c.has_language_data) +
(i.years_of_data * c.years_of_data) +
(i.has_1yr_data * c.has_1yr_data) +
(i.has_2yr_data * c.has_2yr_data) +
(i.t_hiv_rna_1_yr * c.t_hiv_rna_1_yr) + 
(i.t_hiv_test_2yr * c.t_hiv_test_2yr) +
(i.t_hiv_test_e * c.t_hiv_test_e) +
(i.t_hiv_elisa_e * c.t_hiv_elisa_e) +
(i.acute_hiv_test_e * c.acute_hiv_test_e) +
(i.acute_hiv_test_2yr * c.acute_hiv_test_2yr) +
(i.t_pos_gon_test_2yr * c.t_pos_gon_test_2yr) +
(i.t_chla_test_t_e * c.t_chla_test_t_e) +
(i.rx_bicillin_1_yr * c.rx_bicillin_1_yr) +
(i.rx_bicillin_2yr * c.rx_bicillin_2yr) +
(i.rx_bicillin_e * c.rx_bicillin_e) +
(i.rx_suboxone_2yr * c.rx_suboxone_2yr) +
(i.syphilis_any_site_state_x_late_e * c.syphilis_any_site_state_x_late_e) +
(i.contact_w_or_expo_venereal_dis_e * c.contact_w_or_expo_venereal_dis_e) +
(i.hiv_counseling_2yr * c.hiv_counseling_2yr)
) as pat_sum_x_value
into hrp_hist_index_data
FROM hrp_hist_index_data i,
hrp_hist_coefficients c;


SELECT patient_id, (1 / (1 + exp(-(pat_sum_x_value)))) hiv_risk_score
into hrp_hist_risk_scores
FROM hrp_hist_pat_sum_x_value;

-- POPULATE THE TABLE FOR COC REPORTING
INSERT INTO gen_pop_tools.cc_hiv_risk_score
	(SELECT T1.patient_id, T1.rpt_year, round(hiv_risk_score::numeric, 6) hiv_risk_score, hiv_test_this_yr as hiv_test_yn, truvada_rx_this_yr truvada_rx_yn
	FROM hrp_hist_index_data T1,
	hrp_hist_risk_scores T2
	WHERE T1.patient_id = T2.patient_id );


