CREATE SCHEMA gen_pop_tools;

CREATE TABLE gen_pop_tools.cc_hiv_risk_score
(
    patient_id integer NOT NULL,
    rpt_year character varying(4) COLLATE pg_catalog."default" NOT NULL,
    hiv_risk_score numeric,
    hiv_test_yn integer,
    truvada_rx_yn integer,
    CONSTRAINT cc_hiv_risk_score_pkey PRIMARY KEY (patient_id, rpt_year)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE gen_pop_tools.cc_hiv_risk_score
    OWNER to esp;