﻿DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_union_full CASCADE;

DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_union_full_w_enc CASCADE;


DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_012017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_042017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_depression_active CASCADE; 
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_mbp_with_depression CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_no_depression CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_mbp_without_depression CASCADE;

DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_diab_events CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diabetes_active CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_mbp_with_diabetes CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_no_diabetes CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_mbp_without_diabetes CASCADE;

DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_all_entries CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_dateofint CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_mbp_bmi_gte30 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_mbp_bmi_lt30 CASCADE;

DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_dateofint CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_current CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_mbp_smoker CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_non_smoker CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_mbp_non_smoker CASCADE;

DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_all_events CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_dateofint CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042017 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_ctrld_hyper_and_depr CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_ctrld_hyper_no_depr CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_ctrld_hyper_and_diab CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_ctrld_hyper_no_diab CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_ctrld_hyper_and_bmi_gte30 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_ctrld_hyper_and_bmi_lt30 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_ctrld_hyper_and_smoker CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_ctrld_hyper_non_smoker CASCADE;

DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_and_depr CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_no_depr CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_and_diab_t2 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_no_diab_t2 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_bmi_gte30 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_bmi_lt30 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_and_smoker CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_non_smoker CASCADE;

--
DROP TABLE IF EXISTS esp_mdphnet.ptwf_comm_conditions_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_patient CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_patient_full_state CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_patient_non_intervention CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_patient_intervention CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_patient_comparison_comm CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_all_denom_and_num CASCADE;



DROP TABLE IF EXISTS esp_mdphnet.pwtf_hist_comorbid_output_full_strat;


-- Encounters in the 2 years preceding 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_enc_012012  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_12'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '8', '9', '11', '15', '19', '20', '29')   
AND raw_encounter_type not in ('HISTORY')
AND date > ('01-01-2012'::date - interval  '2 years') and date < '01-01-2012')  AS DUMMYALIAS221 ;

-- Encounters in the 2 years preceding 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_enc_042012   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_12'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '9', '15', '19', '20', '29')  
AND raw_encounter_type not in ('HISTORY')        
AND date > ('04-01-2012'::date - interval  '2 years') and date < '04-01-2012')  AS DUMMYALIAS222 ;


-- Encounters in the 2 years preceding 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_enc_072012   AS
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_12'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '9', '15', '19', '20', '29')     
AND raw_encounter_type not in ('HISTORY')    
AND date > ('07-01-2012'::date - interval  '2 years') and date < '07-01-2012')  AS DUMMYALIAS223 ;

-- Encounters in the 2 years preceding 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_enc_102012   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_12'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '15', '19', '20', '29')  
AND raw_encounter_type not in ('HISTORY')       
AND date > ('10-01-2012'::date - interval  '2 years') and date < '10-01-2012')  AS DUMMYALIAS224 ;

-- Encounters in the 2 years preceding 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_enc_012013   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_13'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '15', '19', '20', '29')      
AND raw_encounter_type not in ('HISTORY') 
AND date > ('01-01-2013'::date - interval  '2 years') and date < '01-01-2013')  AS DUMMYALIAS225 ;

-- Encounters in the 2 years preceding 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_enc_042013   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_13'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '15', '19', '20', '29')    
AND raw_encounter_type not in ('HISTORY')     
AND date > ('04-01-2013'::date - interval  '2 years') and date < '04-01-2013')  AS DUMMYALIAS226 ;


-- Encounters in the 2 years preceding 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_enc_072013   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_13'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '15', '19', '20', '29')  
AND raw_encounter_type not in ('HISTORY')   
AND date > ('07-01-2013'::date - interval  '2 years') and date < '07-01-2013')  AS DUMMYALIAS227 ;

-- Encounters in the 2 years preceding 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_enc_102013   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_13'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '15', '19', '20', '29')  
AND raw_encounter_type not in ('HISTORY')    
AND date > ('10-01-2013'::date - interval  '2 years') and date < '10-01-2013')  AS DUMMYALIAS228 ;

-- Encounters in the 2 years preceding 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_enc_012014   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_14'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '15', '19', '20')     
AND raw_encounter_type not in ('HISTORY')
AND date > ('01-01-2014'::date - interval  '2 years') and date < '01-01-2014')  AS DUMMYALIAS229 ;

-- Encounters in the 2 years preceding 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_enc_042014   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_14'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '15', '19', '20')    
AND raw_encounter_type not in ('HISTORY')  
AND date > ('04-01-2014'::date - interval  '2 years') and date < '04-01-2014')  AS DUMMYALIAS230 ;

-- Encounters in the 2 years preceding 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_enc_072014   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_14'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '20')   
AND raw_encounter_type not in ('HISTORY')
AND date > '07-01-2014'::date - interval '2 years' and date < '07-01-2014')  AS DUMMYALIAS231 ;

-- Encounters in the 2 years preceding 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_enc_102014   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_14'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1')   
AND raw_encounter_type not in ('HISTORY')
AND date > '10-01-2014'::date - interval '2 years' and date < '10-01-2014')  AS DUMMYALIAS232 ;

-- Encounters in the 2 years preceding 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_enc_012015    AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_15'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '7')
AND raw_encounter_type not in ('HISTORY')
AND date > '01-01-2015'::date - interval '2 years' and date < '01-01-2015')  AS DUMMYALIAS233 ;

-- Encounters in the 2 years preceding 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_enc_042015    AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_15'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '7')  
AND raw_encounter_type not in ('HISTORY')
AND date > '04-01-2015'::date - interval '2 years' and date < '04-01-2015')  AS DUMMYALIAS234 ;

-- Encounters in the 2 years preceding 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_enc_072015   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_15'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '7')  
AND raw_encounter_type not in ('HISTORY')
AND date > '07-01-2015'::date - interval '2 years' and date < '07-01-2015')  AS DUMMYALIAS235 ;


-- Encounters in the 2 years preceding 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_enc_102015    AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_15'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1','7')   
AND raw_encounter_type not in ('HISTORY')
AND date > '10-01-2015'::date - interval '2 years' and date < '10-01-2015')  AS DUMMYALIAS236 ;


-- Encounters in the 2 years preceding 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_enc_012016    AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_16'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '6', '7')
AND raw_encounter_type not in ('HISTORY')
AND date > '01-01-2016'::date - interval '2 years' and date < '01-01-2016')  AS DUMMYALIAS237 ;

-- Encounters in the 2 years preceding 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_enc_042016   AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_16'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '6', '7') 
AND raw_encounter_type not in ('HISTORY')
AND date > '04-01-2016'::date - interval '2 years' and date < '04-01-2016')  AS DUMMYALIAS238 ;

-- Encounters in the 2 years preceding 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_enc_072016    AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_16'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '6', '7')
AND raw_encounter_type not in ('HISTORY')
AND date > '07-01-2016'::date - interval '2 years' and date < '07-01-2016')  AS DUMMYALIASt238 ;


-- Encounters in the 2 years preceding 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_enc_102016    AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_16'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '6', '7', '29')
AND raw_encounter_type not in ('HISTORY')
AND date > '10-01-2016'::date - interval '2 years' and date < '10-01-2016')  AS DUMMYALIASt238 ;


-- Encounters in the 2 years preceding 01-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_enc_012017    AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_17'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '6', '7', '29')
AND raw_encounter_type not in ('HISTORY')
AND date > '01-01-2017'::date - interval '2 years' and date < '01-01-2017')  AS DUMMYALIASt238 ;

-- Encounters in the 2 years preceding 04-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_enc_042017    AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_17'::text quarter_of_interest, 'encounter'::text condition 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '6', '7', '29')
AND raw_encounter_type not in ('HISTORY')
AND date > '04-01-2017'::date - interval '2 years' and date < '04-01-2017')  AS DUMMYALIASt238 ;


-- ENCOUNTERS -- Union together all of the quarters
CREATE  TABLE esp_mdphnet.smk_diab_denom_union_full  AS 
(SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012012) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042012) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072012) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102012) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012013) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042013) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072013) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102013)
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012014) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042014) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072014) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102014) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012015) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042015) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072015) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102015) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012016) 
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042016)
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072016)
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102016)
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012017)
UNION (SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042017);



-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 01-01-2012
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_012012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jan_12'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('01-01-2012'::date - interval  '1 years') and date < '01-01-2012'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 04-01-2012
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_042012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'apr_12'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('04-01-2012'::date - interval  '1 years') and date < '04-01-2012'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 07-01-2012
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_072012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jul_12'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('07-01-2012'::date - interval  '1 years') and date < '07-01-2012'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 10-01-2012
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_102012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'oct_12'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('10-01-2012'::date - interval  '1 years') and date < '10-01-2012'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 01-01-2013
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_012013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jan_13'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('01-01-2013'::date - interval  '1 years') and date < '01-01-2013'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 04-01-2013
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_042013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'apr_13'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('04-01-2013'::date - interval  '1 years') and date < '04-01-2013'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 07-01-2013
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_072013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jul_13'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('07-01-2013'::date - interval  '1 years') and date < '07-01-2013'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 10-01-2013
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_102013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'oct_13'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('10-01-2013'::date - interval  '1 years') and date < '10-01-2013'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 01-01-2014
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_012014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jan_14'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('01-01-2014'::date - interval  '1 years') and date < '01-01-2014'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 04-01-2014
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_042014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'apr_14'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('04-01-2014'::date - interval  '1 years') and date < '04-01-2014'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 07-01-2014
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_072014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jul_14'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('07-01-2014'::date - interval  '1 years') and date < '07-01-2014'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 10-01-2014
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_102014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'oct_14'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('10-01-2014'::date - interval  '1 years') and date < '10-01-2014'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 01-01-2015
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_012015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jan_15'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('01-01-2015'::date - interval  '1 years') and date < '01-01-2015'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 04-01-2015
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_042015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'apr_15'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('04-01-2015'::date - interval  '1 years') and date < '04-01-2015'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 07-01-2015
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_072015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jul_15'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('07-01-2015'::date - interval  '1 years') and date < '07-01-2015'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 10-01-2015
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_102015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'oct_15'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('10-01-2015'::date - interval  '1 years') and date < '10-01-2015'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 01-01-2016
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_012016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jan_16'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('01-01-2016'::date - interval  '1 years') and date < '01-01-2016'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 04-01-2016
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_042016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'apr_16'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('04-01-2016'::date - interval  '1 years') and date < '04-01-2016'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 07-01-2016
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_072016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jul_16'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('07-01-2016'::date - interval  '1 years') and date < '07-01-2016'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 10-01-2016
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_102016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'oct_16'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('10-01-2016'::date - interval  '1 years') and date < '10-01-2016'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 01-01-2017
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_012017 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jan_17'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('01-01-2017'::date - interval  '1 years') and date < '01-01-2017'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 04-01-2017
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_042017 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'apr_17'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('04-01-2017'::date - interval  '1 years') and date < '04-01-2017'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Union together all the patients and dates 
CREATE  TABLE esp_mdphnet.smk_diab_bp_measured_union_full AS 
(SELECT * FROM esp_mdphnet.smk_diab_bp_measured_012012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_042012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_072012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_102012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_012013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_042013) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_072013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_102013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_012014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_042014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_072014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_102014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_012015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_042015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_072015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_102015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_012016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_042016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_072016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_102016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_012017)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_042017);

-- MEASURED BP - JOIN WITH ENCOUNTERS TO APPLY CENTER & ENCOUNTER TYPE FILTERING
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_union_full_w_enc AS
SELECT T1.*, 'measured_bp'::text condition
FROM esp_mdphnet.smk_diab_bp_measured_union_full T1,
esp_mdphnet.smk_diab_denom_union_full T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = T2.quarter_of_interest;

-- DEPRESSION case and events prior to index date
CREATE  TABLE esp_mdphnet.smk_diab_depression_all AS 
select T1.patient_id, T1.id, T1.condition, T1.date as case_date, T2.date as history_date, T2.status, T1.isactive
from nodis_case T1, nodis_caseactivehistory T2
WHERE T1.date < '04-01-2017' 
AND T2.date < '04-01-2017'
and condition = 'depression'
ANd T1.id = T2.case_id
order by case_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_depression_012012 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_012012 T2   
WHERE  history_date < '01-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_depression_042012 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_042012 T2   
WHERE  history_date < '04-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_depression_072012 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_072012 T2   
WHERE  history_date < '07-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_depression_102012 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_102012 T2   
WHERE  history_date < '10-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_depression_012013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_012013 T2   
WHERE  history_date < '01-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_depression_042013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_042013 T2   
WHERE  history_date < '04-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_depression_072013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_072013 T2   
WHERE  history_date < '07-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_depression_102013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_102013 T2   
WHERE  history_date < '10-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_depression_012014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_012014 T2   
WHERE  history_date < '01-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_depression_042014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_042014 T2   
WHERE  history_date < '04-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_depression_072014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_072014 T2   
WHERE  history_date < '07-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_depression_102014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_102014 T2   
WHERE  history_date < '10-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_depression_012015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_012015 T2   
WHERE  history_date < '01-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_depression_042015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_042015 T2   
WHERE  history_date < '04-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_depression_072015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_072015 T2   
WHERE  history_date < '07-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_depression_102015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_102015 T2   
WHERE  history_date < '10-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_depression_012016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_012016 T2   
WHERE  history_date < '01-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_depression_042016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_042016 T2   
WHERE  history_date < '04-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_depression_072016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_072016 T2   
WHERE  history_date < '07-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_depression_102016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_102016 T2   
WHERE  history_date < '10-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 01-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_depression_012017 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_17'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_012017 T2   
WHERE  history_date < '01-01-2017'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- DEPRESSION Most recent depression date in the 2 years prior to the index date - 04-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_depression_042017 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_17'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_depression_all T1,
esp_mdphnet.smk_diab_enc_042017 T2   
WHERE  history_date < '04-01-2017'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;


-- DEPRESSION - Union together all of the data 
CREATE  TABLE esp_mdphnet.smk_diab_depression_union_full AS 
      SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_012012
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_042012
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_072012
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_102012
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_012013
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_042013
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_072013
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_102013
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_012014
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_042014
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_072014
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_102014
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_012015
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_042015
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_072015
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_102015
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_012016
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_042016
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_072016
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_102016
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_012017
UNION SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_depression_042017;

-- DEPRESSION - Get details for active depression
CREATE  TABLE esp_mdphnet.smk_diab_depression_active AS 
SELECT T1.patient_id, T2.id as case_id, T2.status,T1.quarter_of_interest, 'depression_act'::text as condition, T2.history_date as diab_active_date
FROM esp_mdphnet.smk_diab_depression_union_full T1 
INNER JOIN esp_mdphnet.smk_diab_depression_all T2 
ON ((T1.patient_id = T2.patient_id) 
AND (T1.max_case_event_date = T2.history_date))
AND status in ('I', 'R');

-- MBP WITH DEPRESSION
CREATE TABLE esp_mdphnet.smk_diab_mbp_with_depression AS
SELECT T1.patient_id, 'mbp_and_depression'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_depression_active T1
INNER JOIN esp_mdphnet.smk_diab_bp_measured_union_full_w_enc T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- PATIENTS WITHOUT ACTIVE DEPRESSION (Patients with Encounters in 2 yrs preceding the index date)
CREATE TABLE esp_mdphnet.smk_diab_no_depression AS
SELECT patient_id, quarter_of_interest, 'no_depression'::text condition
FROM esp_mdphnet.smk_diab_denom_union_full
EXCEPT
SELECT patient_id, quarter_of_interest, 'no_depression'::text condition
FROM esp_mdphnet.smk_diab_depression_active;

-- PATIENTS WITH BP MEASURED IN THE 1 YR PRECEDING THE INDEX DATE WITHOUT ACTIVE DEPRESSION
CREATE TABLE esp_mdphnet.smk_diab_mbp_without_depression AS
SELECT patient_id, quarter_of_interest, 'mbp_no_depression'::TEXT condition
FROM esp_mdphnet.smk_diab_bp_measured_union_full_w_enc
EXCEPT
SELECT patient_id, quarter_of_interest, 'mbp_no_depression'::TEXT condition
FROM esp_mdphnet.smk_diab_depression_active;

-- ACTIVE DIABETES All diabetes type2 cases and events
CREATE  TABLE esp_mdphnet.smk_diab_frank_diab_events AS 
select T1.patient_id, T1.id, T1.condition, T1.date as case_date, T2.date as history_date, T2.status, T1.isactive
from nodis_case T1, nodis_caseactivehistory T2
WHERE T1.date < '04-01-2017' 
AND T2.date < '04-01-2017'
and condition = 'diabetes:type-2'
ANd T1.id = T2.case_id
order by case_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_012012 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_012012 T2   
WHERE  history_date < '01-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_042012 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_042012 T2   
WHERE  history_date < '04-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_072012 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_072012 T2   
WHERE  history_date < '07-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_102012 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_102012 T2   
WHERE  history_date < '10-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_012013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_012013 T2   
WHERE  history_date < '01-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_042013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_042013 T2   
WHERE  history_date < '04-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_072013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_072013 T2   
WHERE  history_date < '07-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_102013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_102013 T2   
WHERE  history_date < '10-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_012014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_012014 T2   
WHERE  history_date < '01-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_042014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_042014 T2   
WHERE  history_date < '04-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_072014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_072014 T2   
WHERE  history_date < '07-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_102014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_102014 T2   
WHERE  history_date < '10-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_012015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_012015 T2   
WHERE  history_date < '01-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_042015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_042015 T2   
WHERE  history_date < '04-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_072015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_072015 T2   
WHERE  history_date < '07-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_102015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_102015 T2   
WHERE  history_date < '10-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_012016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_012016 T2   
WHERE  history_date < '01-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_042016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_042016 T2   
WHERE  history_date < '04-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_072016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_072016 T2   
WHERE  history_date < '07-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_102016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_102016 T2   
WHERE  history_date < '10-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 01-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_012017 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_17'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_012017 T2   
WHERE  history_date < '01-01-2017'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- ACTIVE DIABETES Most recent diabetes date in the 2 years prior to the index date - 04-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_042017 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_17'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1,
esp_mdphnet.smk_diab_enc_042017 T2   
WHERE  history_date < '04-01-2017'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

--  ACTIVE DIABETES - Bring Together All Active Diabetes Data 
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_union_full AS 
(SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_012012) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_042012) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_072012) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_102012) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_012013) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_042013) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_072013) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_102013)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_012014) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_042014) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_072014) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_102014) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_012015) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_042015) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_072015) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_102015) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_012016) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_042016)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_072016)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_102016)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_012017)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_act_diab_042017);

-- ACTIVE DIABETES - Get details for active diabetes
CREATE  TABLE esp_mdphnet.smk_diab_diabetes_active   AS 
SELECT T1.patient_id, T2.id as case_id, T2.status,T1.quarter_of_interest, 'diabetes_t2_act'::text as condition, T2.history_date as diab_active_date
FROM esp_mdphnet.smk_diab_act_diab_union_full T1 
INNER JOIN esp_mdphnet.smk_diab_frank_diab_events T2 
ON ((T1.patient_id = T2.patient_id) 
AND (T1.max_case_event_date = T2.history_date))
AND status in ('I', 'R');

-- MBP WITH DIABETES
CREATE TABLE esp_mdphnet.smk_diab_mbp_with_diabetes AS
SELECT T1.patient_id, 'mbp_and_diabetes_t2'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_diabetes_active T1
INNER JOIN esp_mdphnet.smk_diab_bp_measured_union_full_w_enc T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- PATIENTS WITHOUT ACTIVE DIABETES (Patients with Encounters in 2 yrs preceding the index date)
CREATE TABLE esp_mdphnet.smk_diab_no_diabetes AS
SELECT patient_id, quarter_of_interest, 'no_diabetes'::text condition
FROM esp_mdphnet.smk_diab_denom_union_full
EXCEPT
SELECT patient_id, quarter_of_interest, 'no_diabetes'::text condition
FROM esp_mdphnet.smk_diab_diabetes_active;

-- PATIENTS WITH BP MEASURED IN THE 1 YR PRECEDING THE INDEX DATE WITHOUT ACTIVE DIABETES
CREATE TABLE esp_mdphnet.smk_diab_mbp_without_diabetes AS
SELECT patient_id, quarter_of_interest, 'mbp_no_diabetes'::TEXT condition
FROM esp_mdphnet.smk_diab_bp_measured_union_full_w_enc
EXCEPT
SELECT patient_id, quarter_of_interest, 'mbp_no_diabetes'::TEXT condition
FROM esp_mdphnet.smk_diab_diabetes_active;

-- BMI Conditions - All condition entries during the time span of the report
CREATE  TABLE esp_mdphnet.smk_diab_bmi_all_entries   AS 
SELECT T2.id patient_id,T1.condition,(T1.date + '1960-01-01'::date) date 
FROM public.esp_condition T1 
INNER JOIN public.emr_patient T2 ON ((T1.patid = T2.natural_key))  
WHERE (date + '1960-01-01'::date) < '04-01-2017' 
and condition in ( 'BMI <25',  'BMI >= 30',  'BMI >=25 and <30', 'No Measured BMI');

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2012
CREATE TABLE esp_mdphnet.smk_diab_bmi_012012   AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_012012 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2012'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2012
CREATE TABLE esp_mdphnet.smk_diab_bmi_042012   AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_042012 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2012'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_bmi_072012 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_072012 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2012'
GROUP BY T1.patient_id;


-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_bmi_102012 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_102012 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2012'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_bmi_012013 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_012013 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2013'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_bmi_042013 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_042013 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2013'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_bmi_072013 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_072013 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2013'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_bmi_102013 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_102013 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2013'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_bmi_012014 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_012014 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2014'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_bmi_042014 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_042014 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2014'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_bmi_072014 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_072014 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2014'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_bmi_102014 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_102014 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2014'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_bmi_012015 AS
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_012015 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2015'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_bmi_042015 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_042015 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2015'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_bmi_072015  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_072015 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2015'
GROUP BY T1.patient_id;


-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_bmi_102015 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_102015 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2015'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_bmi_012016 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_16'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_012016 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2016'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_bmi_042016 AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_16'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_042016 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2016'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_bmi_072016  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_16'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_072016 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2016'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_bmi_102016  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_16'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_102016 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2016'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_bmi_012017  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_17'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_012017 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2017'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_bmi_042017  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_17'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_042017 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2017'
GROUP BY T1.patient_id;

-- BMI - Union together all the patients and dates
CREATE  TABLE esp_mdphnet.smk_diab_bmi_union_full AS 
(SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_012012) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_042012) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_072012) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_102012) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_012013) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_042013) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_072013) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_102013)
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_012014) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_042014) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_072014) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_102014) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_012015) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_042015) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_072015) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_102015) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_012016) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_042016)
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_072016)
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_102016)
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_012017)
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_042017);

-- BMI - Get the condition for the date of interest.
CREATE  TABLE esp_mdphnet.smk_diab_bmi_dateofint   AS 
SELECT distinct on (T1.patient_id, quarter_of_interest) T1.patient_id,T2.condition,T1.quarter_of_interest
FROM esp_mdphnet.smk_diab_bmi_union_full T1 
INNER JOIN esp_mdphnet.smk_diab_bmi_all_entries T2 
ON ((T1.patient_id = T2.patient_id) AND (T1.max_bmi_date = T2.date)) ;

-- MBP WITH BMI >= 30
CREATE TABLE esp_mdphnet.smk_diab_mbp_bmi_gte30 AS
SELECT T1.patient_id, 'mbp_and_bmi_gte30'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_dateofint T1
INNER JOIN esp_mdphnet.smk_diab_bp_measured_union_full_w_enc T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE T1.condition = 'BMI >= 30';

-- PATIENTS WITH BP MEASURED IN THE 1 YR PRECEDING THE INDEX DATE AND BMI < 30
CREATE TABLE esp_mdphnet.smk_diab_mbp_bmi_lt30 AS
SELECT patient_id, quarter_of_interest, 'mbp_and_bmi_lt30'::TEXT condition
FROM esp_mdphnet.smk_diab_bp_measured_union_full_w_enc
EXCEPT
SELECT patient_id, quarter_of_interest, 'mbp_and_bmi_lt30'::TEXT condition
FROM esp_mdphnet.smk_diab_bmi_dateofint
WHERE condition = 'BMI >= 30';

-- SMOKING - Get the most recent smoking date prior to the index date. 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_smk_012012 AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jan_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_enc_012012 T1 
INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id)) 
WHERE  T2.date < '01-01-2012' AND T2.tobacco_use is not null and T2.tobacco_use != '' 
GROUP BY T1.patient_id) AS DUMMYALIAS239 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_smk_042012 AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'apr_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_enc_042012 T1 
INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  
WHERE  T2.date < '04-01-2012' AND T2.tobacco_use is not null and T2.tobacco_use != '' 
GROUP BY T1.patient_id)  AS DUMMYALIAS240 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_smk_072012 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jul_12'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072012 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '07-01-2012' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS241 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_smk_102012 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'oct_12'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102012 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '10-01-2012' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS242 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_smk_012013  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jan_13'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012013 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '01-01-2013' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS243 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_smk_042013 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'apr_13'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042013 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '04-01-2013' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS244 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_smk_072013 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jul_13'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072013 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '07-01-2013' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS245 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_smk_102013 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'oct_13'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102013 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '10-01-2013' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS246 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_smk_012014 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jan_14'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012014 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '01-01-2014' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS247 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_smk_042014 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(t2.date) max_smoking_date,'apr_14'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042014 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '04-01-2014' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS248 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_smk_072014 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jul_14'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072014 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '07-01-2014' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS249 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_smk_102014 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(t2.date) max_smoking_date,'oct_14'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102014 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '10-01-2014' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS250 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_012015 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jan_15'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012015 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '01-01-2015' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS251 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_042015 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'apr_15'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042015 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '04-01-2015' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS252 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_072015 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jul_15'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072015 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '07-01-2015' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS253 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_102015 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'oct_15'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102015 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '10-01-2015' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS254 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_012016 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jan_16'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012016 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '01-01-2016' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS255 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_042016 AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'apr_16'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042016 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '04-01-2016' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS256 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_072016  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jul_16'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072016 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '07-01-2016' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIASt256 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_102016  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'oct_16'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102016 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '10-01-2016' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIASt256 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 01-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_smk_012017  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jan_17'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012017 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '01-01-2017' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIASt256 ;

-- SMOKING - Get the most recent smoking date prior to the index date. 04-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_smk_042017  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'apr_17'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042017 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '04-01-2017' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIASt256 ;

--  All of the smoking dates and patients 
CREATE  TABLE esp_mdphnet.smk_diab_smk_union_full AS 
(SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_012012) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_042012) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_072012) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_102012) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_012013) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_042013)
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_072013) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_102013)
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_012014) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_042014) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_072014) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_102014) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_012015) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_042015) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_072015) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_102015) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_012016) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_042016) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_072016)
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_102016)
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_012017)
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_042017);

-- SMOKING - Get tobacco_use for the date of interest
CREATE  TABLE esp_mdphnet.smk_diab_smk_dateofint AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.tobacco_use,T1.quarter_of_interest,
case when tobacco_use ILIKE ANY(ARRAY['%adults%', '%aunt%', '%both%', '%brother%', '%cousin%', '%dad%', '%everyone%', '%exposure%','%family smokes%', '%father%', '%fob%', '%foc%', '%gm%', '%gf%',  '%grandaughter%','%grandfather%', '%grandma%', '%grandmom%', '%grandmother%', '%grandparent%', '%husband%', '%many smokers%', '%members%', '%mgf%', '% mo %', '%mob%', '%moc%',  '%mgm%','%mggm%', '%mom%', '%mothr%', '%mothe%', '%parent%', '%paretns%', '%passive%', '%sister%', '% uncle %', '%uncle.%'])  
	AND tobacco_use NOT ILIKE ALL(ARRAY['have cut back%']) 
then 'Passive'      
when (
	tobacco_use in ('N','Never','no','Denies.','Nonsmoker.','never','No. .','none','0','negative','Nonsmoker','None.','Denies','Non-smoker','None','Non smoker','Never smoked.','non smoker','Non-smoker','Non-smoker.',         'Non smoker.','nonsmoker')
	or tobacco_use ILIKE ANY(ARRAY['%abstain%', 'No%', '%doesn%smoke%', 'negative%', '%never%'])
	)
	AND tobacco_use NOT ILIKE ALL(ARRAY['%quit%', '%plans%', '%smoke outside%', '%not clear%', '%daily%', '%not%', 'yes%', '%cut back%', 'smoked%', '%day%'])
then 'Never'  
when (
	tobacco_use in ('R','Prev','Previous','quit','Quit smoking','Former smoker.','Former smoker', 'Former','prev.','Quit.','quit smoking', 'X', '0 currently', 'Complete cessation', 'No. not at new apartment. .')
	or tobacco_use ~ '^[0123456789]{1,2}/[[0123456789]{1,2}/[0123456789]{2,4}'           
	or tobacco_use ILIKE ANY(ARRAY['%quit%', '%previous%', '%used to%', '%former%', 'jan%', 'june%', '%long time ago', '%not%smoking%', '%patch%', '%smoked%', '%pt quit%', '%quit%chantix%'])
	)           
	AND tobacco_use NOT ILIKE ALL(ARRAY['%less%', '%trying%', '%plans%', '%advised%', '%complications%', 'current%', '%quitting%', '%cig%patch%',  '%needs to%', '%not in house%', '%on/off%',            '%would like to%', '%quit by%', '%pick a date%', '%to quit%', '%to have more money%', 'up to%'])           
then 'Former'  
when (
	tobacco_use in ('Y','Current','X','Yes. .','precontemplative','counseled','one', 'Very Ready','Post Partum','ready','contemplative', 'Yes. outside. .','relapse','rare', 'rarely')
	or tobacco_use ~ '^[0123456789]'        
	or tobacco_use ~ '^.[0123456789]'        
	or tobacco_use ILIKE ANY(ARRAY['%<%','%>%','%~%','about%','%addicted%','%advised%', '%a lot%', '%almost%', '%approx%', '%apprx%','%as much%', '%avoid%', '%breath%', '%cancer%', '%casual%', '%cessation%', '%chew%', '%children%',         '%cig%', '%complica%', '%contemplat%','%continue%','%continues%', '%couple%','%current%', '%cut back%','%cut down%', '%daily%', '%day%', '%depends%', '%drinking%', '%enough%', '%expensive%', '%feel%', '%few%', '%half%', '%handout%', '%hardly%', '%harm%', '%health%','%infreq%','%improve%','%interm%', '%less%', '%live%', '%more than%', '%needs to%', '%occ%','%off and on%', '%on weekends%', '%one%', '%only%', '%pack%', '%cig%patch%', '%pipe%', '%pkg%','%positive%', '%quit%', '%rare%', '%ready%', '%recreation%', '%roll%', '%since%', '%smoke outside%',                       '%seldom%', '%smoke%', '%smoking%', '%social%', '%some%', '%start%', '%still%','%stress%', '%to be%','%tobacco%','%trying%', '%up to%', '%under%', '%var%', '%wants to%', '%while%', '%yes%']) 
	)
	AND tobacco_use not ILIKE ALL(ARRAY['~', '%refus%','%unknown%', '%not cigarettes%', '%pt is no%', '%w/o%'])
then 'Current'                      
else 'Not available' end  smoking 
FROM esp_mdphnet.smk_diab_smk_union_full T1 
INNER JOIN public.emr_socialhistory T2 
ON ((T1.patient_id = T2.patient_id) 
AND (T1.max_smoking_date = T2.date))
WHERE T2.tobacco_use is not null and T2.tobacco_use != '' )  AS DUMMYALIAS257 ;

-- SMOKING - Only want CURRENT (yes) smokers.
CREATE  TABLE esp_mdphnet.smk_diab_smk_current AS 
SELECT DISTINCT *, 'current_smoker'::text AS condition FROM (SELECT T1.patient_id,T1.tobacco_use,T1.quarter_of_interest 
FROM  esp_mdphnet.smk_diab_smk_dateofint T1   
WHERE  smoking =  'Current' )  AS DUMMYALIAS258 ;

-- MBP AND SMOKER
CREATE TABLE esp_mdphnet.smk_diab_mbp_smoker AS
SELECT T1.patient_id, 'mbp_and_smoker'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_smk_current  T1
INNER JOIN esp_mdphnet.smk_diab_bp_measured_union_full_w_enc T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- PATIENTS WHO ARE NOT CURRENT SMOKERS (Patients with Encounters in 2 yrs preceding the index date)
CREATE TABLE esp_mdphnet.smk_diab_non_smoker AS
SELECT patient_id, quarter_of_interest, 'non_smoker'::text condition
FROM esp_mdphnet.smk_diab_denom_union_full
EXCEPT
SELECT patient_id, quarter_of_interest, 'non_smoker'::text condition
FROM esp_mdphnet.smk_diab_smk_current;

-- PATIENTS WITH BP MEASURED IN THE 1 YR PRECEDING THE INDEX DATE WHO ARE NOT CURRENT SMOKERS
CREATE TABLE esp_mdphnet.smk_diab_mbp_non_smoker AS
SELECT patient_id, quarter_of_interest, 'mbp_and_non_smoker'::TEXT condition
FROM esp_mdphnet.smk_diab_bp_measured_union_full_w_enc
EXCEPT
SELECT patient_id, quarter_of_interest, 'mbp_and_non_smoker'::TEXT condition
FROM esp_mdphnet.smk_diab_mbp_smoker;

-- HYPERTENSION All hypertension cases and events
CREATE  TABLE esp_mdphnet.smk_diab_hyper_all_events   AS 
select T1.patient_id, T1.id, T1.condition, T1.date as case_date, T2.date as history_date, T2.status, T1.isactive
from nodis_case T1, nodis_caseactivehistory T2
WHERE T1.date < '04-01-2017' 
AND T2.date < '04-01-2017'
and condition = 'hypertension'
ANd T1.id = T2.case_id
order by case_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_012012   AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_012012 T2   
WHERE  history_date < '01-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_042012   AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_042012 T2   
WHERE  history_date < '04-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_072012 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_072012 T2   
WHERE  history_date < '07-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_102012 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_102012 T2   
WHERE  history_date < '10-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_012013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_012013 T2   
WHERE  history_date < '01-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_042013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_042013 T2   
WHERE  history_date < '04-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_072013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_072013 T2   
WHERE  history_date < '07-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_102013 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_102013 T2   
WHERE  history_date < '10-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_012014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_012014 T2   
WHERE  history_date < '01-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_042014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_042014 T2   
WHERE  history_date < '04-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_072014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_072014 T2   
WHERE  history_date < '07-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_102014 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_102014 T2   
WHERE  history_date < '10-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_012015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_012015 T2   
WHERE  history_date < '01-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_042015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_042015 T2   
WHERE  history_date < '04-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_072015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_072015 T2   
WHERE  history_date < '07-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_102015 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_102015 T2   
WHERE  history_date < '10-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_012016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_012016 T2   
WHERE  history_date < '01-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_042016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_042016 T2   
WHERE  history_date < '04-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_072016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_072016 T2   
WHERE  history_date < '07-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_102016 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_102016 T2   
WHERE  history_date < '10-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 01-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_hyper_012017 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_17'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_012017 T2   
WHERE  history_date < '01-01-2017'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 04-01-2017
CREATE  TABLE esp_mdphnet.smk_diab_hyper_042017 AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_17'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_042017 T2   
WHERE  history_date < '04-01-2017'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;


-- HYPERTENSION - Union together all the patients and dates 
CREATE  TABLE esp_mdphnet.smk_diab_hyper_union_full AS 
(SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_012012) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_042012)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_072012) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_102012)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_012013)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_042013) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_072013)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_102013)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_012014)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_042014)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_072014)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_102014)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_012015)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_042015)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_072015)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_102015)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_012016)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_042016)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_072016)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_102016)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_012017)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_042017);

-- HYPERTENSION- Get details for active hypertension
CREATE  TABLE esp_mdphnet.smk_diab_hyper_dateofint   AS 
SELECT T1.patient_id, T2.id as case_id, T2.status,T1.quarter_of_interest, 'hypertension'::text as condition, T2.history_date as hyper_active_date
FROM esp_mdphnet.smk_diab_hyper_union_full T1 
INNER JOIN esp_mdphnet.smk_diab_hyper_all_events T2 
ON ((T1.patient_id = T2.patient_id) 
AND (T1.max_case_event_date = T2.history_date))
AND status in ('I', 'R');

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 01-01-2012
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_012012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jan_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '01-01-2012'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '01-01-2012'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'jan_12';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 04-01-2012
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_042012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'apr_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '04-01-2012'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '04-01-2012'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'apr_12';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 07-01-2012
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_072012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jul_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '07-01-2012'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '07-01-2012'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'jul_12';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 10-01-2012
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_102012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'oct_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '10-01-2012'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '10-01-2012'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'oct_12';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 01-01-2013
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_012013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jan_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '01-01-2013'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '01-01-2013'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'jan_13';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 04-01-2013
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_042013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'apr_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '04-01-2013'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '04-01-2013'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'apr_13';



-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 07-01-2013
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_072013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jul_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '07-01-2013'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '07-01-2013'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'jul_13';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 10-01-2013
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_102013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'oct_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '10-01-2013'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '10-01-2013'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'oct_13';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 01-01-2014
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_012014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jan_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '01-01-2014'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '01-01-2014'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'jan_14';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 04-01-2014
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_042014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'apr_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '04-01-2014'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '04-01-2014'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'apr_14';



-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 07-01-2014
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_072014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jul_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '07-01-2014'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '07-01-2014'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'jul_14';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 10-01-2014
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_102014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'oct_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '10-01-2014'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '10-01-2014'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'oct_14';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 01-01-2015
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_012015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jan_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '01-01-2015'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '01-01-2015'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'jan_15';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 04-01-2015
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_042015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'apr_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '04-01-2015'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '04-01-2015'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'apr_15';



-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 07-01-2015
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_072015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jul_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '07-01-2015'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '07-01-2015'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'jul_15';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 10-01-2015
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_102015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'oct_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '10-01-2015'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '10-01-2015'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'oct_15';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 01-01-2016
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_012016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jan_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '01-01-2016'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '01-01-2016'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'jan_16';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 04-01-2016
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_042016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'apr_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '04-01-2016'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '04-01-2016'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'apr_16';



-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 07-01-2016
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_072016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jul_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '07-01-2016'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '07-01-2016'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'jul_16';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 10-01-2016
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_102016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'oct_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '10-01-2016'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '10-01-2016'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'oct_16';


-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 01-01-2017
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_012017 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jan_17'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '01-01-2017'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '01-01-2017'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'jan_17';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 04-01-2017
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_042017 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'apr_17'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1, esp_mdphnet.smk_diab_hyper_dateofint TT2
	WHERE date < '04-01-2017'
	AND TT1.patient_id = TT2.patient_id
	AND TT1.date > TT2.hyper_active_date
	AND TT1.bp_systolic is not null and TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2,
	(SELECT TT2.patient_id, TT2.bp_systolic, TT2.bp_diastolic, TT2.date 
	FROM emr_encounter TT2, esp_mdphnet.smk_diab_hyper_dateofint TT3
	WHERE DATE < '04-01-2017'
	AND TT2.bp_systolic < 140 and TT2.bp_diastolic < 90
	AND TT2.patient_id = TT3.patient_id
	AND TT2.date > TT3.hyper_active_date
	group by TT2.patient_id, TT2.bp_systolic , TT2.bp_diastolic, TT2.date) T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.max_cbp_date = T3.date
AND T1.quarter_of_interest = 'apr_17';

-- CONTROLLED HYPERTENSION - Union together all the patients and dates 
CREATE  TABLE esp_mdphnet.smk_diab_controlled_bp_union_full  AS 
(SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_012012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_042012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_072012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_102012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_012013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_042013) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_072013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_102013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_012014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_042014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_072014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_102014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_012015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_042015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_072015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_102015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_012016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_042016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_072016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_102016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_012017)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_042017);

-- CONTROLLED HYPERTENSION - JOIN WITH ENCOUNTERS TO APPLY CENTER & ENCOUNTER TYPE FILTERING
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc AS
SELECT T1.*, 'controlled_hyper'::text condition
FROM esp_mdphnet.smk_diab_controlled_bp_union_full T1,
esp_mdphnet.smk_diab_denom_union_full T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = T2.quarter_of_interest;

-- CONTROLLED HYPERTENSION WITH ACTIVE DEPRESSION
CREATE TABLE esp_mdphnet.smk_diab_ctrld_hyper_and_depr AS
SELECT T1.patient_id, 'ctrld_hyper_and_depression'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc  T1
INNER JOIN esp_mdphnet.smk_diab_depression_active T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- CONTROLLED HYPERTENSION WITHOUT ACTIVE DEPRESSION
CREATE TABLE esp_mdphnet.smk_diab_ctrld_hyper_no_depr AS
SELECT T1.patient_id, 'ctrld_hyper_no_depression'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc  T1
INNER JOIN esp_mdphnet.smk_diab_no_depression T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- CONTROLLED HYPERTENSION WITH ACTIVE DIABETES
CREATE TABLE esp_mdphnet.smk_diab_ctrld_hyper_and_diab AS
SELECT T1.patient_id, 'ctrld_hyper_and_diab'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc  T1
INNER JOIN esp_mdphnet.smk_diab_diabetes_active T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- CONTROLLED HYPERTENSION WITHOUT ACTIVE DIABETES
CREATE TABLE esp_mdphnet.smk_diab_ctrld_hyper_no_diab AS
SELECT T1.patient_id, 'ctrld_hyper_no_diab'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc  T1
INNER JOIN esp_mdphnet.smk_diab_no_diabetes T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- CONTROLLED HYPERTENSION WITH BMI >= 30
CREATE TABLE esp_mdphnet.smk_diab_ctrld_hyper_and_bmi_gte30 AS
SELECT T1.patient_id, 'ctrld_hyper_and_bmi_gte30'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc  T1
INNER JOIN esp_mdphnet.smk_diab_bmi_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE T2.condition = 'BMI >= 30';

-- CONTROLLED HYPERTENSION WITH BMI < 30
CREATE TABLE esp_mdphnet.smk_diab_ctrld_hyper_and_bmi_lt30 AS
SELECT patient_id, quarter_of_interest, 'ctrld_hyper_and_bmi_lt30'::TEXT condition
FROM esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc
EXCEPT
SELECT patient_id, quarter_of_interest, 'ctrld_hyper_and_bmi_lt30'::TEXT condition
FROM esp_mdphnet.smk_diab_bmi_dateofint
WHERE condition = 'BMI >= 30';

-- CONTROLLED HYPERTENSION WITH SMOKING
CREATE TABLE esp_mdphnet.smk_diab_ctrld_hyper_and_smoker AS
SELECT T1.patient_id, 'ctrld_hyper_and_smoker'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc  T1
INNER JOIN esp_mdphnet.smk_diab_smk_current T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- CONTROLLED HYPERTENSION WITHOUT SMOKING
CREATE TABLE esp_mdphnet.smk_diab_ctrld_hyper_non_smoker AS
SELECT patient_id, quarter_of_interest, 'ctrld_hyper_non_smoker'::text condition
FROM esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc
EXCEPT
SELECT patient_id, quarter_of_interest, 'ctrld_hyper_non_smoker'::text condition
FROM esp_mdphnet.smk_diab_smk_current;


-- ACTIVE HYPERTENSION AND ACTIVE DEPRESSION
CREATE TABLE esp_mdphnet.smk_diab_hyper_and_depr AS
SELECT T1.patient_id, 'hyper_and_depr'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_hyper_dateofint  T1
INNER JOIN esp_mdphnet.smk_diab_depression_active T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- ACTIVE HYPERTENSION WITHOUT DEPRESSION
CREATE TABLE esp_mdphnet.smk_diab_hyper_no_depr AS
SELECT T1.patient_id, 'hyper_no_depr'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_hyper_dateofint  T1
INNER JOIN esp_mdphnet.smk_diab_no_depression T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- ACTIVE HYPERTENSION AND T2 DIABETES
CREATE TABLE esp_mdphnet.smk_diab_hyper_and_diab_t2 AS
SELECT T1.patient_id, 'hyper_and_diab_t2'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_hyper_dateofint  T1
INNER JOIN esp_mdphnet.smk_diab_diabetes_active T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- ACTIVE HYPERTENSION WITHOUT T2 DIABETES
CREATE TABLE esp_mdphnet.smk_diab_hyper_no_diab_t2 AS
SELECT T1.patient_id, 'hyper_no_diab_t2'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_hyper_dateofint  T1
INNER JOIN esp_mdphnet.smk_diab_no_diabetes T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- ACTIVE HYPERTENSION AND BMI >= 30
CREATE TABLE esp_mdphnet.smk_diab_hyper_bmi_gte30 AS
SELECT T1.patient_id, 'hyper_and_bmi_gte30'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_hyper_dateofint  T1
INNER JOIN esp_mdphnet.smk_diab_bmi_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE T2.condition = 'BMI >= 30';

-- ACTIVE HYPETENSION AND BMI < 30 
CREATE TABLE esp_mdphnet.smk_diab_hyper_bmi_lt30 AS
SELECT patient_id, quarter_of_interest, 'hyper_and_bmi_lt30'::TEXT condition
FROM esp_mdphnet.smk_diab_hyper_dateofint
EXCEPT
SELECT patient_id, quarter_of_interest, 'hyper_and_bmi_lt30'::TEXT condition
FROM esp_mdphnet.smk_diab_bmi_dateofint
WHERE condition = 'BMI >= 30';

-- ACTIVE HYPERTENSION AND CURRENT SMOKER
CREATE TABLE esp_mdphnet.smk_diab_hyper_and_smoker AS
SELECT T1.patient_id, 'hyper_and_smoker'::text as condition, T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_hyper_dateofint  T1
INNER JOIN esp_mdphnet.smk_diab_smk_current T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest);

-- ACTIVE HYPERTENSION WITHOUT SMOKING
CREATE TABLE esp_mdphnet.smk_diab_hyper_non_smoker AS
SELECT patient_id, quarter_of_interest, 'hyper_non_smoker'::text condition
FROM esp_mdphnet.smk_diab_hyper_dateofint
EXCEPT
SELECT patient_id, quarter_of_interest, 'hyper_non_smoker'::text condition
FROM esp_mdphnet.smk_diab_smk_current;


-- UNION together all of the patients and conditions
CREATE TABLE esp_mdphnet.ptwf_comm_conditions_all AS
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_denom_union_full
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_bp_measured_union_full_w_enc
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_depression_active
UNION
SELECT patient_id, condition, quarter_of_interest FROM  esp_mdphnet.smk_diab_mbp_with_depression
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_no_depression
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_mbp_without_depression
UNION 
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_diabetes_active
UNION 
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_mbp_with_diabetes
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_no_diabetes
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_mbp_without_diabetes
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_dateofint
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_mbp_bmi_gte30
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_mbp_bmi_lt30
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_smk_current
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_mbp_smoker
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_non_smoker 
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_mbp_non_smoker
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_dateofint
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_ctrld_hyper_and_depr
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_ctrld_hyper_no_depr
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_ctrld_hyper_and_diab
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_ctrld_hyper_no_diab
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_ctrld_hyper_and_bmi_gte30
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_ctrld_hyper_and_bmi_lt30
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_ctrld_hyper_and_smoker
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_ctrld_hyper_non_smoker
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_and_depr
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_no_depr
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_and_diab_t2
UNION 
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_no_diab_t2
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_bmi_gte30
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_bmi_lt30
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_and_smoker
UNION
SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_non_smoker
;

-- -- Add in Denominator Encounter Data
--CREATE TABLE esp_mdphnet.pwtf_comm_conditions_denom AS 
--SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.ptwf_comm_conditions_all
--UNION
--SELECT patient_id, condition, quarter_of_interest FROM esp_mdphnet.smk_diab_denom_union_full;


-- PATIENT -- Determine age on index date and gather patient details from patient table and esp_demogrpahic table
-- Limit to patients with a zip in a PWTF community
CREATE  TABLE esp_mdphnet.pwtf_comm_conditions_patient AS 
SELECT T1.patient_id,T1.condition,
quarter_of_interest,
CASE   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' 
end age_group_10_yr, 
T2.natural_key,
T2.zip5,
T3.sex,
CASE when T3.race_ethnicity = 6 then 'hispanic'  
when T3.race_ethnicity = 5 then 'white' 
when T3.race_ethnicity = 3 then 'black' 
when T3.race_ethnicity = 2 then 'asian' 
when T3.race_ethnicity = 1 then 'native_american' 
when T3.race_ethnicity = 0 then 'unknown' 
end race,
coalesce(T4.city, 'NOT MAPPED'::text) town,
COALESCE(T4.region, 'NOT MAPPED'::TEXT) region
FROM esp_mdphnet.ptwf_comm_conditions_all T1
INNER JOIN public.emr_patient T2 ON (T1.patient_id = T2.id )
INNER JOIN esp_mdphnet.esp_demographic T3 on (T2.natural_key = T3.patid)
FULL OUTER JOIN static_eohhs_region_mappings T4 ON (T2.zip5 = T4.zip5);

-- Create rows for the full state
CREATE TABLE esp_mdphnet.pwtf_comm_conditions_patient_full_state AS
select distinct(patient_id), condition, quarter_of_interest, age_group_10_yr, natural_key, zip5, sex, race, 'FULL STATE' as town, 'FULL STATE' as region from esp_mdphnet.pwtf_comm_conditions_patient;

-- Create rows for the non-intervention communities
CREATE TABLE esp_mdphnet.pwtf_comm_conditions_patient_non_intervention AS
SELECT distinct(patient_id), condition, quarter_of_interest, age_group_10_yr, natural_key, zip5, sex, race, 'NON-INTERVENTION' as town, 'NON-INTERVENTION' as region 
FROM esp_mdphnet.pwtf_comm_conditions_patient 
WHERE zip5 not in (select zip5 from static_eohhs_region_mappings WHERE city ilike 'PWTF%');

-- Create rows for the intervention communities
CREATE TABLE esp_mdphnet.pwtf_comm_conditions_patient_intervention AS
SELECT distinct(patient_id), condition, quarter_of_interest, age_group_10_yr, natural_key, zip5, sex, race, 'INTERVENTION' as town, 'INTERVENTION' as region 
FROM esp_mdphnet.pwtf_comm_conditions_patient 
WHERE zip5 in (select zip5 from static_eohhs_region_mappings WHERE city ilike 'PWTF%');


-- Create rows for the comparison communities
CREATE TABLE esp_mdphnet.pwtf_comm_conditions_patient_comparison_comm AS
SELECT distinct(patient_id), condition, quarter_of_interest, age_group_10_yr, natural_key, zip5, sex, race, 'COMPARISION' as town, 'COMPARISON' as region 
FROM esp_mdphnet.pwtf_comm_conditions_patient 
WHERE town in ('Burlington', 'Fitchburg', 'Yarmouth', 'Hull', 'Leominster', 'Brockton', 'Salem', 'Norwood');


-- Union together the data for counts
CREATE TABLE esp_mdphnet.pwtf_comm_conditions_all_denom_and_num AS
SELECT * from esp_mdphnet.pwtf_comm_conditions_patient 
UNION 
SELECT * FROM esp_mdphnet.pwtf_comm_conditions_patient_full_state
UNION
SELECT * FROM esp_mdphnet.pwtf_comm_conditions_patient_non_intervention
UNION
SELECT * FROM esp_mdphnet.pwtf_comm_conditions_patient_intervention
UNION
SELECT * FROM esp_mdphnet.pwtf_comm_conditions_patient_comparison_comm;


-- Full Stratified output
CREATE TABLE esp_mdphnet.pwtf_hist_comorbid_output_full_strat AS 
select
age_group_10_yr, race, sex, town,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'jan_12' THEN 1 END) enc_jan_12,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'apr_12' THEN 1 END) enc_apr_12,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'jul_12' THEN 1 END) enc_jul_12,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'oct_12' THEN 1 END) enc_oct_12,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'jan_13' THEN 1 END) enc_jan_13,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'apr_13' THEN 1 END) enc_apr_13,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'jul_13' THEN 1 END) enc_jul_13,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'oct_13' THEN 1 END) enc_oct_13,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'jan_14' THEN 1 END) enc_jan_14,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'apr_14' THEN 1 END) enc_apr_14,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'jul_14' THEN 1 END) enc_jul_14,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'oct_14' THEN 1 END) enc_oct_14,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'jan_15' THEN 1 END) enc_jan_15,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'apr_15' THEN 1 END) enc_apr_15,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'jul_15' THEN 1 END) enc_jul_15,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'oct_15' THEN 1 END) enc_oct_15,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'jan_16' THEN 1 END) enc_jan_16,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'apr_16' THEN 1 END) enc_apr_16,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'jul_16' THEN 1 END) enc_jul_16,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'oct_16' THEN 1 END) enc_oct_16,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'jan_17' THEN 1 END) enc_jan_17,
count(CASE WHEN condition = 'encounter' and quarter_of_interest = 'apr_17' THEN 1 END) enc_apr_17,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'jan_12' then 1 END) measured_bp_jan_12,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'apr_12' then 1 END) measured_bp_apr_12,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'jul_12' then 1 END) measured_bp_jul_12,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'oct_12' then 1 END) measured_bp_oct_12,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'jan_13' then 1 END) measured_bp_jan_13,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'apr_13' then 1 END) measured_bp_apr_13,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'jul_13' then 1 END) measured_bp_jul_13,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'oct_13' then 1 END) measured_bp_oct_13,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'jan_14' then 1 END) measured_bp_jan_14,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'apr_14' then 1 END) measured_bp_apr_14,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'jul_14' then 1 END) measured_bp_jul_14,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'oct_14' then 1 END) measured_bp_oct_14,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'jan_15' then 1 END) measured_bp_jan_15,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'apr_15' then 1 END) measured_bp_apr_15,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'jul_15' then 1 END) measured_bp_jul_15,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'oct_15' then 1 END) measured_bp_oct_15,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'jan_16' then 1 END) measured_bp_jan_16,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'apr_16' then 1 END) measured_bp_apr_16,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'jul_16' then 1 END) measured_bp_jul_16,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'oct_16' then 1 END) measured_bp_oct_16,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'jan_17' then 1 END) measured_bp_jan_17,
count(CASE WHEN condition = 'measured_bp' and quarter_of_interest = 'apr_17' then 1 END) measured_bp_apr_17,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'jan_12' then 1 END) depr_jan_12,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'apr_12' then 1 END) depr_apr_12,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'jul_12' then 1 END) depr_jul_12,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'oct_12' then 1 END) depr_oct_12,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'jan_13' then 1 END) depr_jan_13,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'apr_13' then 1 END) depr_apr_13,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'jul_13' then 1 END) depr_jul_13,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'oct_13' then 1 END) depr_oct_13,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'jan_14' then 1 END) depr_jan_14,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'apr_14' then 1 END) depr_apr_14,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'jul_14' then 1 END) depr_jul_14,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'oct_14' then 1 END) depr_oct_14,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'jan_15' then 1 END) depr_jan_15,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'apr_15' then 1 END) depr_apr_15,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'jul_15' then 1 END) depr_jul_15,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'oct_15' then 1 END) depr_oct_15,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'jan_16' then 1 END) depr_jan_16,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'apr_16' then 1 END) depr_apr_16,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'jul_16' then 1 END) depr_jul_16,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'oct_16' then 1 END) depr_oct_16,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'jan_17' then 1 END) depr_jan_17,
count(CASE WHEN condition = 'depression_act' and quarter_of_interest = 'apr_17' then 1 END) depr_apr_17,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'jan_12' then 1 END) mbp_and_depr_jan_12,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'apr_12' then 1 END) mbp_and_depr_apr_12,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'jul_12' then 1 END) mbp_and_depr_jul_12,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'oct_12' then 1 END) mbp_and_depr_oct_12,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'jan_13' then 1 END) mbp_and_depr_jan_13,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'apr_13' then 1 END) mbp_and_depr_apr_13,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'jul_13' then 1 END) mbp_and_depr_jul_13,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'oct_13' then 1 END) mbp_and_depr_oct_13,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'jan_14' then 1 END) mbp_and_depr_jan_14,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'apr_14' then 1 END) mbp_and_depr_apr_14,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'jul_14' then 1 END) mbp_and_depr_jul_14,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'oct_14' then 1 END) mbp_and_depr_oct_14,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'jan_15' then 1 END) mbp_and_depr_jan_15,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'apr_15' then 1 END) mbp_and_depr_apr_15,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'jul_15' then 1 END) mbp_and_depr_jul_15,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'oct_15' then 1 END) mbp_and_depr_oct_15,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'jan_16' then 1 END) mbp_and_depr_jan_16,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'apr_16' then 1 END) mbp_and_depr_apr_16,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'jul_16' then 1 END) mbp_and_depr_jul_16,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'oct_16' then 1 END) mbp_and_depr_oct_16,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'jan_17' then 1 END) mbp_and_depr_jan_17,
count(CASE WHEN condition = 'mbp_and_depression' and quarter_of_interest = 'apr_17' then 1 END) mbp_and_depr_apr_17,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'jan_12' then 1 END) no_depr_jan_12,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'apr_12' then 1 END) no_depr_apr_12,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'jul_12' then 1 END) no_depr_jul_12,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'oct_12' then 1 END) no_depr_oct_12,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'jan_13' then 1 END) no_depr_jan_13,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'apr_13' then 1 END) no_depr_apr_13,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'jul_13' then 1 END) no_depr_jul_13,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'oct_13' then 1 END) no_depr_oct_13,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'jan_14' then 1 END) no_depr_jan_14,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'apr_14' then 1 END) no_depr_apr_14,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'jul_14' then 1 END) no_depr_jul_14,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'oct_14' then 1 END) no_depr_oct_14,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'jan_15' then 1 END) no_depr_jan_15,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'apr_15' then 1 END) no_depr_apr_15,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'jul_15' then 1 END) no_depr_jul_15,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'oct_15' then 1 END) no_depr_oct_15,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'jan_16' then 1 END) no_depr_jan_16,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'apr_16' then 1 END) no_depr_apr_16,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'jul_16' then 1 END) no_depr_jul_16,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'oct_16' then 1 END) no_depr_oct_16,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'jan_17' then 1 END) no_depr_jan_17,
count(CASE WHEN condition = 'no_depression' and quarter_of_interest = 'apr_17' then 1 END) no_depr_apr_17,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'jan_12' then 1 END) mbp_no_depr_jan_12,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'apr_12' then 1 END) mbp_no_depr_apr_12,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'jul_12' then 1 END) mbp_no_depr_jul_12,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'oct_12' then 1 END) mbp_no_depr_oct_12,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'jan_13' then 1 END) mbp_no_depr_jan_13,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'apr_13' then 1 END) mbp_no_depr_apr_13,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'jul_13' then 1 END) mbp_no_depr_jul_13,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'oct_13' then 1 END) mbp_no_depr_oct_13,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'jan_14' then 1 END) mbp_no_depr_jan_14,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'apr_14' then 1 END) mbp_no_depr_apr_14,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'jul_14' then 1 END) mbp_no_depr_jul_14,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'oct_14' then 1 END) mbp_no_depr_oct_14,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'jan_15' then 1 END) mbp_no_depr_jan_15,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'apr_15' then 1 END) mbp_no_depr_apr_15,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'jul_15' then 1 END) mbp_no_depr_jul_15,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'oct_15' then 1 END) mbp_no_depr_oct_15,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'jan_16' then 1 END) mbp_no_depr_jan_16,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'apr_16' then 1 END) mbp_no_depr_apr_16,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'jul_16' then 1 END) mbp_no_depr_jul_16,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'oct_16' then 1 END) mbp_no_depr_oct_16,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'jan_17' then 1 END) mbp_no_depr_jan_17,
count(CASE WHEN condition = 'mbp_no_depression' and quarter_of_interest = 'apr_17' then 1 END) mbp_no_depr_apr_17,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'jan_12' then 1 END) diabetes_t2_jan_12,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'apr_12' then 1 END) diabetes_t2_apr_12,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'jul_12' then 1 END) diabetes_t2_jul_12,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'oct_12' then 1 END) diabetes_t2_oct_12,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'jan_13' then 1 END) diabetes_t2_jan_13,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'apr_13' then 1 END) diabetes_t2_apr_13,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'jul_13' then 1 END) diabetes_t2_jul_13,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'oct_13' then 1 END) diabetes_t2_oct_13,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'jan_14' then 1 END) diabetes_t2_jan_14,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'apr_14' then 1 END) diabetes_t2_apr_14,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'jul_14' then 1 END) diabetes_t2_jul_14,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'oct_14' then 1 END) diabetes_t2_oct_14,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'jan_15' then 1 END) diabetes_t2_jan_15,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'apr_15' then 1 END) diabetes_t2_apr_15,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'jul_15' then 1 END) diabetes_t2_jul_15,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'oct_15' then 1 END) diabetes_t2_oct_15,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'jan_16' then 1 END) diabetes_t2_jan_16,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'apr_16' then 1 END) diabetes_t2_apr_16,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'jul_16' then 1 END) diabetes_t2_jul_16,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'oct_16' then 1 END) diabetes_t2_oct_16,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'jan_17' then 1 END) diabetes_t2_jan_17,
count(CASE WHEN condition = 'diabetes_t2_act' and quarter_of_interest = 'apr_17' then 1 END) diabetes_t2_apr_17,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'jan_12' then 1 END) mbp_and_diab_t2_jan_12,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'apr_12' then 1 END) mbp_and_diab_t2_apr_12,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'jul_12' then 1 END) mbp_and_diab_t2_jul_12,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'oct_12' then 1 END) mbp_and_diab_t2_oct_12,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'jan_13' then 1 END) mbp_and_diab_t2_jan_13,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'apr_13' then 1 END) mbp_and_diab_t2_apr_13,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'jul_13' then 1 END) mbp_and_diab_t2_jul_13,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'oct_13' then 1 END) mbp_and_diab_t2_oct_13,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'jan_14' then 1 END) mbp_and_diab_t2_jan_14,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'apr_14' then 1 END) mbp_and_diab_t2_apr_14,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'jul_14' then 1 END) mbp_and_diab_t2_jul_14,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'oct_14' then 1 END) mbp_and_diab_t2_oct_14,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'jan_15' then 1 END) mbp_and_diab_t2_jan_15,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'apr_15' then 1 END) mbp_and_diab_t2_apr_15,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'jul_15' then 1 END) mbp_and_diab_t2_jul_15,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'oct_15' then 1 END) mbp_and_diab_t2_oct_15,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'jan_16' then 1 END) mbp_and_diab_t2_jan_16,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'apr_16' then 1 END) mbp_and_diab_t2_apr_16,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'jul_16' then 1 END) mbp_and_diab_t2_jul_16,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'oct_16' then 1 END) mbp_and_diab_t2_oct_16,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'jan_17' then 1 END) mbp_and_diab_t2_jan_17,
count(CASE WHEN condition = 'mbp_and_diabetes_t2' and quarter_of_interest = 'apr_17' then 1 END) mbp_and_diab_t2_apr_17,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'jan_12' then 1 END) no_diab_t2_jan_12,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'apr_12' then 1 END) no_diab_t2_apr_12,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'jul_12' then 1 END) no_diab_t2_jul_12,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'oct_12' then 1 END) no_diab_t2_oct_12,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'jan_13' then 1 END) no_diab_t2_jan_13,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'apr_13' then 1 END) no_diab_t2_apr_13,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'jul_13' then 1 END) no_diab_t2_jul_13,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'oct_13' then 1 END) no_diab_t2_oct_13,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'jan_14' then 1 END) no_diab_t2_jan_14,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'apr_14' then 1 END) no_diab_t2_apr_14,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'jul_14' then 1 END) no_diab_t2_jul_14,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'oct_14' then 1 END) no_diab_t2_oct_14,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'jan_15' then 1 END) no_diab_t2_jan_15,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'apr_15' then 1 END) no_diab_t2_apr_15,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'jul_15' then 1 END) no_diab_t2_jul_15,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'oct_15' then 1 END) no_diab_t2_oct_15,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'jan_16' then 1 END) no_diab_t2_jan_16,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'apr_16' then 1 END) no_diab_t2_apr_16,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'jul_16' then 1 END) no_diab_t2_jul_16,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'oct_16' then 1 END) no_diab_t2_oct_16,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'jan_17' then 1 END) no_diab_t2_jan_17,
count(CASE WHEN condition = 'no_diabetes' and quarter_of_interest = 'apr_17' then 1 END) no_diab_t2_apr_17,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'jan_12' then 1 END) mbp_no_diab_t2_jan_12,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'apr_12' then 1 END) mbp_no_diab_t2_apr_12,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'jul_12' then 1 END) mbp_no_diab_t2_jul_12,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'oct_12' then 1 END) mbp_no_diab_t2_oct_12,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'jan_13' then 1 END) mbp_no_diab_t2_jan_13,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'apr_13' then 1 END) mbp_no_diab_t2_apr_13,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'jul_13' then 1 END) mbp_no_diab_t2_jul_13,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'oct_13' then 1 END) mbp_no_diab_t2_oct_13,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'jan_14' then 1 END) mbp_no_diab_t2_jan_14,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'apr_14' then 1 END) mbp_no_diab_t2_apr_14,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'jul_14' then 1 END) mbp_no_diab_t2_jul_14,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'oct_14' then 1 END) mbp_no_diab_t2_oct_14,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'jan_15' then 1 END) mbp_no_diab_t2_jan_15,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'apr_15' then 1 END) mbp_no_diab_t2_apr_15,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'jul_15' then 1 END) mbp_no_diab_t2_jul_15,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'oct_15' then 1 END) mbp_no_diab_t2_oct_15,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'jan_16' then 1 END) mbp_no_diab_t2_jan_16,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'apr_16' then 1 END) mbp_no_diab_t2_apr_16,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'jul_16' then 1 END) mbp_no_diab_t2_jul_16,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'oct_16' then 1 END) mbp_no_diab_t2_oct_16,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'jan_17' then 1 END) mbp_no_diab_t2_jan_17,
count(CASE WHEN condition = 'mbp_no_diabetes' and quarter_of_interest = 'apr_17' then 1 END) mbp_no_diab_t2_apr_17,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_12' then 1 END) bmi_gte30_jan_12,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_12' then 1 END) bmi_gte30_apr_12,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_12' then 1 END) bmi_gte30_jul_12,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_12' then 1 END) bmi_gte30_oct_12,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_13' then 1 END) bmi_gte30_jan_13,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_13' then 1 END) bmi_gte30_apr_13,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_13' then 1 END) bmi_gte30_jul_13,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_13' then 1 END) bmi_gte30_oct_13,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_14' then 1 END) bmi_gte30_jan_14,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_14' then 1 END) bmi_gte30_apr_14,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_14' then 1 END) bmi_gte30_jul_14,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_14' then 1 END) bmi_gte30_oct_14,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_15' then 1 END) bmi_gte30_jan_15,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_15' then 1 END) bmi_gte30_apr_15,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_15' then 1 END) bmi_gte30_jul_15,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_15' then 1 END) bmi_gte30_oct_15,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_16' then 1 END) bmi_gte30_jan_16,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_16' then 1 END) bmi_gte30_apr_16,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_16' then 1 END) bmi_gte30_jul_16,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_16' then 1 END) bmi_gte30_oct_16,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_17' then 1 END) bmi_gte30_jan_17,
count(CASE WHEN condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_17' then 1 END) bmi_gte30_apr_17,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_12' then 1 END) mbp_and_bmi_gte30_jan_12,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_12' then 1 END) mbp_and_bmi_gte30_apr_12,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_12' then 1 END) mbp_and_bmi_gte30_jul_12,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_12' then 1 END) mbp_and_bmi_gte30_oct_12,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_13' then 1 END) mbp_and_bmi_gte30_jan_13,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_13' then 1 END) mbp_and_bmi_gte30_apr_13,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_13' then 1 END) mbp_and_bmi_gte30_jul_13,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_13' then 1 END) mbp_and_bmi_gte30_oct_13,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_14' then 1 END) mbp_and_bmi_gte30_jan_14,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_14' then 1 END) mbp_and_bmi_gte30_apr_14,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_14' then 1 END) mbp_and_bmi_gte30_jul_14,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_14' then 1 END) mbp_and_bmi_gte30_oct_14,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_15' then 1 END) mbp_and_bmi_gte30_jan_15,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_15' then 1 END) mbp_and_bmi_gte30_apr_15,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_15' then 1 END) mbp_and_bmi_gte30_jul_15,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_15' then 1 END) mbp_and_bmi_gte30_oct_15,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_16' then 1 END) mbp_and_bmi_gte30_jan_16,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_16' then 1 END) mbp_and_bmi_gte30_apr_16,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_16' then 1 END) mbp_and_bmi_gte30_jul_16,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_16' then 1 END) mbp_and_bmi_gte30_oct_16,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_17' then 1 END) mbp_and_bmi_gte30_jan_17,
count(CASE WHEN condition = 'mbp_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_17' then 1 END) mbp_and_bmi_gte30_apr_17,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_12' then 1 END) bmi_lt30_jan_12,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_12' then 1 END) bmi_lt30_apr_12,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_12' then 1 END) bmi_lt30_jul_12,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_12' then 1 END) bmi_lt30_oct_12,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_13' then 1 END) bmi_lt30_jan_13,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_13' then 1 END) bmi_lt30_apr_13,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_13' then 1 END) bmi_lt30_jul_13,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_13' then 1 END) bmi_lt30_oct_13,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_14' then 1 END) bmi_lt30_jan_14,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_14' then 1 END) bmi_lt30_apr_14,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_14' then 1 END) bmi_lt30_jul_14,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_14' then 1 END) bmi_lt30_oct_14,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_15' then 1 END) bmi_lt30_jan_15,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_15' then 1 END) bmi_lt30_apr_15,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_15' then 1 END) bmi_lt30_jul_15,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_15' then 1 END) bmi_lt30_oct_15,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_16' then 1 END) bmi_lt30_jan_16,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_16' then 1 END) bmi_lt30_apr_16,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_16' then 1 END) bmi_lt30_jul_16,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_16' then 1 END) bmi_lt30_oct_16,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_17' then 1 END) bmi_lt30_jan_17,
count(CASE WHEN condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_17' then 1 END) bmi_lt30_apr_17,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_12' then 1 END) mbp_and_bmi_lt30_jan_12,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_12' then 1 END) mbp_and_bmi_lt30_apr_12,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_12' then 1 END) mbp_and_bmi_lt30_jul_12,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_12' then 1 END) mbp_and_bmi_lt30_oct_12,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_13' then 1 END) mbp_and_bmi_lt30_jan_13,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_13' then 1 END) mbp_and_bmi_lt30_apr_13,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_13' then 1 END) mbp_and_bmi_lt30_jul_13,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_13' then 1 END) mbp_and_bmi_lt30_oct_13,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_14' then 1 END) mbp_and_bmi_lt30_jan_14,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_14' then 1 END) mbp_and_bmi_lt30_apr_14,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_14' then 1 END) mbp_and_bmi_lt30_jul_14,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_14' then 1 END) mbp_and_bmi_lt30_oct_14,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_15' then 1 END) mbp_and_bmi_lt30_jan_15,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_15' then 1 END) mbp_and_bmi_lt30_apr_15,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_15' then 1 END) mbp_and_bmi_lt30_jul_15,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_15' then 1 END) mbp_and_bmi_lt30_oct_15,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_16' then 1 END) mbp_and_bmi_lt30_jan_16,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_16' then 1 END) mbp_and_bmi_lt30_apr_16,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_16' then 1 END) mbp_and_bmi_lt30_jul_16,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_16' then 1 END) mbp_and_bmi_lt30_oct_16,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_17' then 1 END) mbp_and_bmi_lt30_jan_17,
count(CASE WHEN condition = 'mbp_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_17' then 1 END) mbp_and_bmi_lt30_apr_17,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'jan_12' then 1 END) smoker_jan_12,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'apr_12' then 1 END) smoker_apr_12,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'jul_12' then 1 END) smoker_jul_12,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'oct_12' then 1 END) smoker_oct_12,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'jan_13' then 1 END) smoker_jan_13,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'apr_13' then 1 END) smoker_apr_13,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'jul_13' then 1 END) smoker_jul_13,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'oct_13' then 1 END) smoker_oct_13,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'jan_14' then 1 END) smoker_jan_14,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'apr_14' then 1 END) smoker_apr_14,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'jul_14' then 1 END) smoker_jul_14,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'oct_14' then 1 END) smoker_oct_14,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'jan_15' then 1 END) smoker_jan_15,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'apr_15' then 1 END) smoker_apr_15,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'jul_15' then 1 END) smoker_jul_15,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'oct_15' then 1 END) smoker_oct_15,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'jan_16' then 1 END) smoker_jan_16,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'apr_16' then 1 END) smoker_apr_16,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'jul_16' then 1 END) smoker_jul_16,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'oct_16' then 1 END) smoker_oct_16,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'jan_17' then 1 END) smoker_jan_17,
count(CASE WHEN condition = 'current_smoker' and quarter_of_interest = 'apr_17' then 1 END) smoker_apr_17,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'jan_12' then 1 END) mbp_and_smoker_jan_12,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'apr_12' then 1 END) mbp_and_smoker_apr_12,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'jul_12' then 1 END) mbp_and_smoker_jul_12,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'oct_12' then 1 END) mbp_and_smoker_oct_12,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'jan_13' then 1 END) mbp_and_smoker_jan_13,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'apr_13' then 1 END) mbp_and_smoker_apr_13,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'jul_13' then 1 END) mbp_and_smoker_jul_13,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'oct_13' then 1 END) mbp_and_smoker_oct_13,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'jan_14' then 1 END) mbp_and_smoker_jan_14,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'apr_14' then 1 END) mbp_and_smoker_apr_14,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'jul_14' then 1 END) mbp_and_smoker_jul_14,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'oct_14' then 1 END) mbp_and_smoker_oct_14,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'jan_15' then 1 END) mbp_and_smoker_jan_15,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'apr_15' then 1 END) mbp_and_smoker_apr_15,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'jul_15' then 1 END) mbp_and_smoker_jul_15,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'oct_15' then 1 END) mbp_and_smoker_oct_15,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'jan_16' then 1 END) mbp_and_smoker_jan_16,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'apr_16' then 1 END) mbp_and_smoker_apr_16,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'jul_16' then 1 END) mbp_and_smoker_jul_16,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'oct_16' then 1 END) mbp_and_smoker_oct_16,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'jan_17' then 1 END) mbp_and_smoker_jan_17,
count(CASE WHEN condition = 'mbp_and_smoker' and quarter_of_interest = 'apr_17' then 1 END) mbp_and_smoker_apr_17,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'jan_12' then 1 END) non_smoker_jan_12,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'apr_12' then 1 END) non_smoker_apr_12,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'jul_12' then 1 END) non_smoker_jul_12,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'oct_12' then 1 END) non_smoker_oct_12,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'jan_13' then 1 END) non_smoker_jan_13,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'apr_13' then 1 END) non_smoker_apr_13,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'jul_13' then 1 END) non_smoker_jul_13,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'oct_13' then 1 END) non_smoker_oct_13,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'jan_14' then 1 END) non_smoker_jan_14,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'apr_14' then 1 END) non_smoker_apr_14,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'jul_14' then 1 END) non_smoker_jul_14,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'oct_14' then 1 END) non_smoker_oct_14,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'jan_15' then 1 END) non_smoker_jan_15,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'apr_15' then 1 END) non_smoker_apr_15,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'jul_15' then 1 END) non_smoker_jul_15,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'oct_15' then 1 END) non_smoker_oct_15,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'jan_16' then 1 END) non_smoker_jan_16,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'apr_16' then 1 END) non_smoker_apr_16,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'jul_16' then 1 END) non_smoker_jul_16,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'oct_16' then 1 END) non_smoker_oct_16,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'jan_17' then 1 END) non_smoker_jan_17,
count(CASE WHEN condition = 'non_smoker' and quarter_of_interest = 'apr_17' then 1 END) non_smoker_apr_17,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'jan_12' then 1 END) mbp_and_non_smoker_jan_12,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'apr_12' then 1 END) mbp_and_non_smoker_apr_12,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'jul_12' then 1 END) mbp_and_non_smoker_jul_12,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'oct_12' then 1 END) mbp_and_non_smoker_oct_12,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'jan_13' then 1 END) mbp_and_non_smoker_jan_13,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'apr_13' then 1 END) mbp_and_non_smoker_apr_13,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'jul_13' then 1 END) mbp_and_non_smoker_jul_13,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'oct_13' then 1 END) mbp_and_non_smoker_oct_13,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'jan_14' then 1 END) mbp_and_non_smoker_jan_14,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'apr_14' then 1 END) mbp_and_non_smoker_apr_14,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'jul_14' then 1 END) mbp_and_non_smoker_jul_14,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'oct_14' then 1 END) mbp_and_non_smoker_oct_14,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'jan_15' then 1 END) mbp_and_non_smoker_jan_15,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'apr_15' then 1 END) mbp_and_non_smoker_apr_15,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'jul_15' then 1 END) mbp_and_non_smoker_jul_15,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'oct_15' then 1 END) mbp_and_non_smoker_oct_15,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'jan_16' then 1 END) mbp_and_non_smoker_jan_16,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'apr_16' then 1 END) mbp_and_non_smoker_apr_16,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'jul_16' then 1 END) mbp_and_non_smoker_jul_16,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'oct_16' then 1 END) mbp_and_non_smoker_oct_16,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'jan_17' then 1 END) mbp_and_non_smoker_jan_17,
count(CASE WHEN condition = 'mbp_and_non_smoker' and quarter_of_interest = 'apr_17' then 1 END) mbp_and_non_smoker_apr_17,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'jan_12' then 1 END) hypertension_jan_12,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'apr_12' then 1 END) hypertension_apr_12,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'jul_12' then 1 END) hypertension_jul_12,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'oct_12' then 1 END) hypertension_oct_12,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'jan_13' then 1 END) hypertension_jan_13,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'apr_13' then 1 END) hypertension_apr_13,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'jul_13' then 1 END) hypertension_jul_13,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'oct_13' then 1 END) hypertension_oct_13,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'jan_14' then 1 END) hypertension_jan_14,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'apr_14' then 1 END) hypertension_apr_14,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'jul_14' then 1 END) hypertension_jul_14,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'oct_14' then 1 END) hypertension_oct_14,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'jan_15' then 1 END) hypertension_jan_15,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'apr_15' then 1 END) hypertension_apr_15,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'jul_15' then 1 END) hypertension_jul_15,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'oct_15' then 1 END) hypertension_oct_15,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'jan_16' then 1 END) hypertension_jan_16,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'apr_16' then 1 END) hypertension_apr_16,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'jul_16' then 1 END) hypertension_jul_16,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'oct_16' then 1 END) hypertension_oct_16,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'jan_17' then 1 END) hypertension_jan_17,
count(CASE WHEN condition = 'hypertension' and quarter_of_interest = 'apr_17' then 1 END) hypertension_apr_17,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'jan_12' then 1 END) ctrld_hyper_jan_12,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'apr_12' then 1 END) ctrld_hyper_apr_12,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'jul_12' then 1 END) ctrld_hyper_jul_12,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'oct_12' then 1 END) ctrld_hyper_oct_12,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'jan_13' then 1 END) ctrld_hyper_jan_13,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'apr_13' then 1 END) ctrld_hyper_apr_13,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'jul_13' then 1 END) ctrld_hyper_jul_13,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'oct_13' then 1 END) ctrld_hyper_oct_13,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'jan_14' then 1 END) ctrld_hyper_jan_14,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'apr_14' then 1 END) ctrld_hyper_apr_14,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'jul_14' then 1 END) ctrld_hyper_jul_14,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'oct_14' then 1 END) ctrld_hyper_oct_14,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'jan_15' then 1 END) ctrld_hyper_jan_15,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'apr_15' then 1 END) ctrld_hyper_apr_15,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'jul_15' then 1 END) ctrld_hyper_jul_15,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'oct_15' then 1 END) ctrld_hyper_oct_15,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'jan_16' then 1 END) ctrld_hyper_jan_16,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'apr_16' then 1 END) ctrld_hyper_apr_16,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'jul_16' then 1 END) ctrld_hyper_jul_16,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'oct_16' then 1 END) ctrld_hyper_oct_16,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'jan_17' then 1 END) ctrld_hyper_jan_17,
count(CASE WHEN condition = 'controlled_hyper' and quarter_of_interest = 'apr_17' then 1 END) ctrld_hyper_apr_17,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'jan_12' then 1 END) ctrld_hyper_and_depr_jan_12,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'apr_12' then 1 END) ctrld_hyper_and_depr_apr_12,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'jul_12' then 1 END) ctrld_hyper_and_depr_jul_12,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'oct_12' then 1 END) ctrld_hyper_and_depr_oct_12,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'jan_13' then 1 END) ctrld_hyper_and_depr_jan_13,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'apr_13' then 1 END) ctrld_hyper_and_depr_apr_13,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'jul_13' then 1 END) ctrld_hyper_and_depr_jul_13,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'oct_13' then 1 END) ctrld_hyper_and_depr_oct_13,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'jan_14' then 1 END) ctrld_hyper_and_depr_jan_14,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'apr_14' then 1 END) ctrld_hyper_and_depr_apr_14,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'jul_14' then 1 END) ctrld_hyper_and_depr_jul_14,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'oct_14' then 1 END) ctrld_hyper_and_depr_oct_14,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'jan_15' then 1 END) ctrld_hyper_and_depr_jan_15,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'apr_15' then 1 END) ctrld_hyper_and_depr_apr_15,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'jul_15' then 1 END) ctrld_hyper_and_depr_jul_15,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'oct_15' then 1 END) ctrld_hyper_and_depr_oct_15,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'jan_16' then 1 END) ctrld_hyper_and_depr_jan_16,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'apr_16' then 1 END) ctrld_hyper_and_depr_apr_16,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'jul_16' then 1 END) ctrld_hyper_and_depr_jul_16,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'oct_16' then 1 END) ctrld_hyper_and_depr_oct_16,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'jan_17' then 1 END) ctrld_hyper_and_depr_jan_17,
count(CASE WHEN condition = 'ctrld_hyper_and_depression' and quarter_of_interest = 'apr_17' then 1 END) ctrld_hyper_and_depr_apr_17,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'jan_12' then 1 END) ctrld_hyper_no_depr_jan_12,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'apr_12' then 1 END) ctrld_hyper_no_depr_apr_12,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'jul_12' then 1 END) ctrld_hyper_no_depr_jul_12,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'oct_12' then 1 END) ctrld_hyper_no_depr_oct_12,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'jan_13' then 1 END) ctrld_hyper_no_depr_jan_13,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'apr_13' then 1 END) ctrld_hyper_no_depr_apr_13,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'jul_13' then 1 END) ctrld_hyper_no_depr_jul_13,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'oct_13' then 1 END) ctrld_hyper_no_depr_oct_13,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'jan_14' then 1 END) ctrld_hyper_no_depr_jan_14,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'apr_14' then 1 END) ctrld_hyper_no_depr_apr_14,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'jul_14' then 1 END) ctrld_hyper_no_depr_jul_14,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'oct_14' then 1 END) ctrld_hyper_no_depr_oct_14,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'jan_15' then 1 END) ctrld_hyper_no_depr_jan_15,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'apr_15' then 1 END) ctrld_hyper_no_depr_apr_15,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'jul_15' then 1 END) ctrld_hyper_no_depr_jul_15,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'oct_15' then 1 END) ctrld_hyper_no_depr_oct_15,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'jan_16' then 1 END) ctrld_hyper_no_depr_jan_16,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'apr_16' then 1 END) ctrld_hyper_no_depr_apr_16,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'jul_16' then 1 END) ctrld_hyper_no_depr_jul_16,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'oct_16' then 1 END) ctrld_hyper_no_depr_oct_16,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'jan_17' then 1 END) ctrld_hyper_no_depr_jan_17,
count(CASE WHEN condition = 'ctrld_hyper_no_depression' and quarter_of_interest = 'apr_17' then 1 END) ctrld_hyper_no_depr_apr_17,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'jan_12' then 1 END) ctrld_hyper_and_diab_jan_12,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'apr_12' then 1 END) ctrld_hyper_and_diab_apr_12,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'jul_12' then 1 END) ctrld_hyper_and_diab_jul_12,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'oct_12' then 1 END) ctrld_hyper_and_diab_oct_12,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'jan_13' then 1 END) ctrld_hyper_and_diab_jan_13,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'apr_13' then 1 END) ctrld_hyper_and_diab_apr_13,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'jul_13' then 1 END) ctrld_hyper_and_diab_jul_13,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'oct_13' then 1 END) ctrld_hyper_and_diab_oct_13,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'jan_14' then 1 END) ctrld_hyper_and_diab_jan_14,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'apr_14' then 1 END) ctrld_hyper_and_diab_apr_14,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'jul_14' then 1 END) ctrld_hyper_and_diab_jul_14,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'oct_14' then 1 END) ctrld_hyper_and_diab_oct_14,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'jan_15' then 1 END) ctrld_hyper_and_diab_jan_15,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'apr_15' then 1 END) ctrld_hyper_and_diab_apr_15,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'jul_15' then 1 END) ctrld_hyper_and_diab_jul_15,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'oct_15' then 1 END) ctrld_hyper_and_diab_oct_15,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'jan_16' then 1 END) ctrld_hyper_and_diab_jan_16,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'apr_16' then 1 END) ctrld_hyper_and_diab_apr_16,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'jul_16' then 1 END) ctrld_hyper_and_diab_jul_16,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'oct_16' then 1 END) ctrld_hyper_and_diab_oct_16,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'jan_17' then 1 END) ctrld_hyper_and_diab_jan_17,
count(CASE WHEN condition = 'ctrld_hyper_and_diab' and quarter_of_interest = 'apr_17' then 1 END) ctrld_hyper_and_diab_apr_17,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'jan_12' then 1 END) ctrld_hyper_no_diab_jan_12,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'apr_12' then 1 END) ctrld_hyper_no_diab_apr_12,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'jul_12' then 1 END) ctrld_hyper_no_diab_jul_12,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'oct_12' then 1 END) ctrld_hyper_no_diab_oct_12,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'jan_13' then 1 END) ctrld_hyper_no_diab_jan_13,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'apr_13' then 1 END) ctrld_hyper_no_diab_apr_13,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'jul_13' then 1 END) ctrld_hyper_no_diab_jul_13,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'oct_13' then 1 END) ctrld_hyper_no_diab_oct_13,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'jan_14' then 1 END) ctrld_hyper_no_diab_jan_14,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'apr_14' then 1 END) ctrld_hyper_no_diab_apr_14,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'jul_14' then 1 END) ctrld_hyper_no_diab_jul_14,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'oct_14' then 1 END) ctrld_hyper_no_diab_oct_14,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'jan_15' then 1 END) ctrld_hyper_no_diab_jan_15,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'apr_15' then 1 END) ctrld_hyper_no_diab_apr_15,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'jul_15' then 1 END) ctrld_hyper_no_diab_jul_15,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'oct_15' then 1 END) ctrld_hyper_no_diab_oct_15,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'jan_16' then 1 END) ctrld_hyper_no_diab_jan_16,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'apr_16' then 1 END) ctrld_hyper_no_diab_apr_16,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'jul_16' then 1 END) ctrld_hyper_no_diab_jul_16,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'oct_16' then 1 END) ctrld_hyper_no_diab_oct_16,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'jan_17' then 1 END) ctrld_hyper_no_diab_jan_17,
count(CASE WHEN condition = 'ctrld_hyper_no_diab' and quarter_of_interest = 'apr_17' then 1 END) ctrld_hyper_no_diab_apr_17,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'jan_12' then 1 END) ctrld_hyper_w_bmi_gte30_jan_12,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'apr_12' then 1 END) ctrld_hyper_w_bmi_gte30_apr_12,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'jul_12' then 1 END) ctrld_hyper_w_bmi_gte30_jul_12,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'oct_12' then 1 END) ctrld_hyper_w_bmi_gte30_oct_12,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'jan_13' then 1 END) ctrld_hyper_w_bmi_gte30_jan_13,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'apr_13' then 1 END) ctrld_hyper_w_bmi_gte30_apr_13,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'jul_13' then 1 END) ctrld_hyper_w_bmi_gte30_jul_13,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'oct_13' then 1 END) ctrld_hyper_w_bmi_gte30_oct_13,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'jan_14' then 1 END) ctrld_hyper_w_bmi_gte30_jan_14,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'apr_14' then 1 END) ctrld_hyper_w_bmi_gte30_apr_14,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'jul_14' then 1 END) ctrld_hyper_w_bmi_gte30_jul_14,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'oct_14' then 1 END) ctrld_hyper_w_bmi_gte30_oct_14,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'jan_15' then 1 END) ctrld_hyper_w_bmi_gte30_jan_15,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'apr_15' then 1 END) ctrld_hyper_w_bmi_gte30_apr_15,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'jul_15' then 1 END) ctrld_hyper_w_bmi_gte30_jul_15,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'oct_15' then 1 END) ctrld_hyper_w_bmi_gte30_oct_15,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'jan_16' then 1 END) ctrld_hyper_w_bmi_gte30_jan_16,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'apr_16' then 1 END) ctrld_hyper_w_bmi_gte30_apr_16,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'jul_16' then 1 END) ctrld_hyper_w_bmi_gte30_jul_16,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'oct_16' then 1 END) ctrld_hyper_w_bmi_gte30_oct_16,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'jan_17' then 1 END) ctrld_hyper_w_bmi_gte30_jan_17,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_gte30' and quarter_of_interest = 'apr_17' then 1 END) ctrld_hyper_w_bmi_gte30_apr_17,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'jan_12' then 1 END) ctrld_hyper_w_bmi_lt30_jan_12,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'apr_12' then 1 END) ctrld_hyper_w_bmi_lt30_apr_12,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'jul_12' then 1 END) ctrld_hyper_w_bmi_lt30_jul_12,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'oct_12' then 1 END) ctrld_hyper_w_bmi_lt30_oct_12,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'jan_13' then 1 END) ctrld_hyper_w_bmi_lt30_jan_13,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'apr_13' then 1 END) ctrld_hyper_w_bmi_lt30_apr_13,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'jul_13' then 1 END) ctrld_hyper_w_bmi_lt30_jul_13,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'oct_13' then 1 END) ctrld_hyper_w_bmi_lt30_oct_13,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'jan_14' then 1 END) ctrld_hyper_w_bmi_lt30_jan_14,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'apr_14' then 1 END) ctrld_hyper_w_bmi_lt30_apr_14,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'jul_14' then 1 END) ctrld_hyper_w_bmi_lt30_jul_14,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'oct_14' then 1 END) ctrld_hyper_w_bmi_lt30_oct_14,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'jan_15' then 1 END) ctrld_hyper_w_bmi_lt30_jan_15,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'apr_15' then 1 END) ctrld_hyper_w_bmi_lt30_apr_15,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'jul_15' then 1 END) ctrld_hyper_w_bmi_lt30_jul_15,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'oct_15' then 1 END) ctrld_hyper_w_bmi_lt30_oct_15,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'jan_16' then 1 END) ctrld_hyper_w_bmi_lt30_jan_16,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'apr_16' then 1 END) ctrld_hyper_w_bmi_lt30_apr_16,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'jul_16' then 1 END) ctrld_hyper_w_bmi_lt30_jul_16,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'oct_16' then 1 END) ctrld_hyper_w_bmi_lt30_oct_16,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'jan_17' then 1 END) ctrld_hyper_w_bmi_lt30_jan_17,
count(CASE WHEN condition = 'ctrld_hyper_and_bmi_lt30' and quarter_of_interest = 'apr_17' then 1 END) ctrld_hyper_w_bmi_lt30_apr_17,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'jan_12' then 1 END) ctrld_hyper_and_smk_jan_12,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'apr_12' then 1 END) ctrld_hyper_and_smk_apr_12,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'jul_12' then 1 END) ctrld_hyper_and_smk_jul_12,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'oct_12' then 1 END) ctrld_hyper_and_smk_oct_12,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'jan_13' then 1 END) ctrld_hyper_and_smk_jan_13,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'apr_13' then 1 END) ctrld_hyper_and_smk_apr_13,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'jul_13' then 1 END) ctrld_hyper_and_smk_jul_13,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'oct_13' then 1 END) ctrld_hyper_and_smk_oct_13,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'jan_14' then 1 END) ctrld_hyper_and_smk_jan_14,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'apr_14' then 1 END) ctrld_hyper_and_smk_apr_14,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'jul_14' then 1 END) ctrld_hyper_and_smk_jul_14,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'oct_14' then 1 END) ctrld_hyper_and_smk_oct_14,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'jan_15' then 1 END) ctrld_hyper_and_smk_jan_15,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'apr_15' then 1 END) ctrld_hyper_and_smk_apr_15,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'jul_15' then 1 END) ctrld_hyper_and_smk_jul_15,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'oct_15' then 1 END) ctrld_hyper_and_smk_oct_15,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'jan_16' then 1 END) ctrld_hyper_and_smk_jan_16,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'apr_16' then 1 END) ctrld_hyper_and_smk_apr_16,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'jul_16' then 1 END) ctrld_hyper_and_smk_jul_16,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'oct_16' then 1 END) ctrld_hyper_and_smk_oct_16,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'jan_17' then 1 END) ctrld_hyper_and_smk_jan_17,
count(CASE WHEN condition = 'ctrld_hyper_and_smoker' and quarter_of_interest = 'apr_17' then 1 END) ctrld_hyper_and_smk_apr_17,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'jan_12' then 1 END) ctrld_hyper_non_smk_jan_12,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'apr_12' then 1 END) ctrld_hyper_non_smk_apr_12,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'jul_12' then 1 END) ctrld_hyper_non_smk_jul_12,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'oct_12' then 1 END) ctrld_hyper_non_smk_oct_12,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'jan_13' then 1 END) ctrld_hyper_non_smk_jan_13,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'apr_13' then 1 END) ctrld_hyper_non_smk_apr_13,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'jul_13' then 1 END) ctrld_hyper_non_smk_jul_13,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'oct_13' then 1 END) ctrld_hyper_non_smk_oct_13,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'jan_14' then 1 END) ctrld_hyper_non_smk_jan_14,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'apr_14' then 1 END) ctrld_hyper_non_smk_apr_14,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'jul_14' then 1 END) ctrld_hyper_non_smk_jul_14,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'oct_14' then 1 END) ctrld_hyper_non_smk_oct_14,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'jan_15' then 1 END) ctrld_hyper_non_smk_jan_15,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'apr_15' then 1 END) ctrld_hyper_non_smk_apr_15,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'jul_15' then 1 END) ctrld_hyper_non_smk_jul_15,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'oct_15' then 1 END) ctrld_hyper_non_smk_oct_15,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'jan_16' then 1 END) ctrld_hyper_non_smk_jan_16,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'apr_16' then 1 END) ctrld_hyper_non_smk_apr_16,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'jul_16' then 1 END) ctrld_hyper_non_smk_jul_16,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'oct_16' then 1 END) ctrld_hyper_non_smk_oct_16,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'jan_17' then 1 END) ctrld_hyper_non_smk_jan_17,
count(CASE WHEN condition = 'ctrld_hyper_non_smoker' and quarter_of_interest = 'apr_17' then 1 END) ctrld_hyper_non_smk_apr_17,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'jan_12' then 1 END) hyper_and_depr_jan_12,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'apr_12' then 1 END) hyper_and_depr_apr_12,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'jul_12' then 1 END) hyper_and_depr_jul_12,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'oct_12' then 1 END) hyper_and_depr_oct_12,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'jan_13' then 1 END) hyper_and_depr_jan_13,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'apr_13' then 1 END) hyper_and_depr_apr_13,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'jul_13' then 1 END) hyper_and_depr_jul_13,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'oct_13' then 1 END) hyper_and_depr_oct_13,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'jan_14' then 1 END) hyper_and_depr_jan_14,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'apr_14' then 1 END) hyper_and_depr_apr_14,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'jul_14' then 1 END) hyper_and_depr_jul_14,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'oct_14' then 1 END) hyper_and_depr_oct_14,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'jan_15' then 1 END) hyper_and_depr_jan_15,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'apr_15' then 1 END) hyper_and_depr_apr_15,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'jul_15' then 1 END) hyper_and_depr_jul_15,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'oct_15' then 1 END) hyper_and_depr_oct_15,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'jan_16' then 1 END) hyper_and_depr_jan_16,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'apr_16' then 1 END) hyper_and_depr_apr_16,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'jul_16' then 1 END) hyper_and_depr_jul_16,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'oct_16' then 1 END) hyper_and_depr_oct_16,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'jan_17' then 1 END) hyper_and_depr_jan_17,
count(CASE WHEN condition = 'hyper_and_depr' and quarter_of_interest = 'apr_17' then 1 END) hyper_and_depr_apr_17,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'jan_12' then 1 END) hyper_no_depr_jan_12,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'apr_12' then 1 END) hyper_no_depr_apr_12,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'jul_12' then 1 END) hyper_no_depr_jul_12,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'oct_12' then 1 END) hyper_no_depr_oct_12,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'jan_13' then 1 END) hyper_no_depr_jan_13,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'apr_13' then 1 END) hyper_no_depr_apr_13,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'jul_13' then 1 END) hyper_no_depr_jul_13,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'oct_13' then 1 END) hyper_no_depr_oct_13,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'jan_14' then 1 END) hyper_no_depr_jan_14,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'apr_14' then 1 END) hyper_no_depr_apr_14,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'jul_14' then 1 END) hyper_no_depr_jul_14,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'oct_14' then 1 END) hyper_no_depr_oct_14,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'jan_15' then 1 END) hyper_no_depr_jan_15,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'apr_15' then 1 END) hyper_no_depr_apr_15,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'jul_15' then 1 END) hyper_no_depr_jul_15,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'oct_15' then 1 END) hyper_no_depr_oct_15,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'jan_16' then 1 END) hyper_no_depr_jan_16,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'apr_16' then 1 END) hyper_no_depr_apr_16,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'jul_16' then 1 END) hyper_no_depr_jul_16,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'oct_16' then 1 END) hyper_no_depr_oct_16,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'jan_17' then 1 END) hyper_no_depr_jan_17,
count(CASE WHEN condition = 'hyper_no_depr' and quarter_of_interest = 'apr_17' then 1 END) hyper_no_depr_apr_17,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'jan_12' then 1 END) hyper_and_diab_t2_jan_12,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'apr_12' then 1 END) hyper_and_diab_t2_apr_12,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'jul_12' then 1 END) hyper_and_diab_t2_jul_12,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'oct_12' then 1 END) hyper_and_diab_t2_oct_12,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'jan_13' then 1 END) hyper_and_diab_t2_jan_13,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'apr_13' then 1 END) hyper_and_diab_t2_apr_13,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'jul_13' then 1 END) hyper_and_diab_t2_jul_13,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'oct_13' then 1 END) hyper_and_diab_t2_oct_13,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'jan_14' then 1 END) hyper_and_diab_t2_jan_14,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'apr_14' then 1 END) hyper_and_diab_t2_apr_14,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'jul_14' then 1 END) hyper_and_diab_t2_jul_14,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'oct_14' then 1 END) hyper_and_diab_t2_oct_14,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'jan_15' then 1 END) hyper_and_diab_t2_jan_15,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'apr_15' then 1 END) hyper_and_diab_t2_apr_15,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'jul_15' then 1 END) hyper_and_diab_t2_jul_15,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'oct_15' then 1 END) hyper_and_diab_t2_oct_15,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'jan_16' then 1 END) hyper_and_diab_t2_jan_16,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'apr_16' then 1 END) hyper_and_diab_t2_apr_16,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'jul_16' then 1 END) hyper_and_diab_t2_jul_16,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'oct_16' then 1 END) hyper_and_diab_t2_oct_16,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'jan_17' then 1 END) hyper_and_diab_t2_jan_17,
count(CASE WHEN condition = 'hyper_and_diab_t2' and quarter_of_interest = 'apr_17' then 1 END) hyper_and_diab_t2_apr_17,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'jan_12' then 1 END) hyper_no_diab_t2_jan_12,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'apr_12' then 1 END) hyper_no_diab_t2_apr_12,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'jul_12' then 1 END) hyper_no_diab_t2_jul_12,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'oct_12' then 1 END) hyper_no_diab_t2_oct_12,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'jan_13' then 1 END) hyper_no_diab_t2_jan_13,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'apr_13' then 1 END) hyper_no_diab_t2_apr_13,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'jul_13' then 1 END) hyper_no_diab_t2_jul_13,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'oct_13' then 1 END) hyper_no_diab_t2_oct_13,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'jan_14' then 1 END) hyper_no_diab_t2_jan_14,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'apr_14' then 1 END) hyper_no_diab_t2_apr_14,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'jul_14' then 1 END) hyper_no_diab_t2_jul_14,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'oct_14' then 1 END) hyper_no_diab_t2_oct_14,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'jan_15' then 1 END) hyper_no_diab_t2_jan_15,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'apr_15' then 1 END) hyper_no_diab_t2_apr_15,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'jul_15' then 1 END) hyper_no_diab_t2_jul_15,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'oct_15' then 1 END) hyper_no_diab_t2_oct_15,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'jan_16' then 1 END) hyper_no_diab_t2_jan_16,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'apr_16' then 1 END) hyper_no_diab_t2_apr_16,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'jul_16' then 1 END) hyper_no_diab_t2_jul_16,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'oct_16' then 1 END) hyper_no_diab_t2_oct_16,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'jan_17' then 1 END) hyper_no_diab_t2_jan_17,
count(CASE WHEN condition = 'hyper_no_diab_t2' and quarter_of_interest = 'apr_17' then 1 END) hyper_no_diab_t2_apr_17,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_12' then 1 END) hyper_and_bmi_gte30_jan_12,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_12' then 1 END) hyper_and_bmi_gte30_apr_12,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_12' then 1 END) hyper_and_bmi_gte30_jul_12,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_12' then 1 END) hyper_and_bmi_gte30_oct_12,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_13' then 1 END) hyper_and_bmi_gte30_jan_13,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_13' then 1 END) hyper_and_bmi_gte30_apr_13,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_13' then 1 END) hyper_and_bmi_gte30_jul_13,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_13' then 1 END) hyper_and_bmi_gte30_oct_13,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_14' then 1 END) hyper_and_bmi_gte30_jan_14,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_14' then 1 END) hyper_and_bmi_gte30_apr_14,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_14' then 1 END) hyper_and_bmi_gte30_jul_14,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_14' then 1 END) hyper_and_bmi_gte30_oct_14,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_15' then 1 END) hyper_and_bmi_gte30_jan_15,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_15' then 1 END) hyper_and_bmi_gte30_apr_15,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_15' then 1 END) hyper_and_bmi_gte30_jul_15,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_15' then 1 END) hyper_and_bmi_gte30_oct_15,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_16' then 1 END) hyper_and_bmi_gte30_jan_16,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_16' then 1 END) hyper_and_bmi_gte30_apr_16,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_16' then 1 END) hyper_and_bmi_gte30_jul_16,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_16' then 1 END) hyper_and_bmi_gte30_oct_16,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_17' then 1 END) hyper_and_bmi_gte30_jan_17,
count(CASE WHEN condition = 'hyper_and_bmi_gte30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_17' then 1 END) hyper_and_bmi_gte30_apr_17,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_12' then 1 END) hyper_and_bmi_lt30_jan_12,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_12' then 1 END) hyper_and_bmi_lt30_apr_12,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_12' then 1 END) hyper_and_bmi_lt30_jul_12,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_12' then 1 END) hyper_and_bmi_lt30_oct_12,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_13' then 1 END) hyper_and_bmi_lt30_jan_13,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_13' then 1 END) hyper_and_bmi_lt30_apr_13,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_13' then 1 END) hyper_and_bmi_lt30_jul_13,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_13' then 1 END) hyper_and_bmi_lt30_oct_13,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_14' then 1 END) hyper_and_bmi_lt30_jan_14,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_14' then 1 END) hyper_and_bmi_lt30_apr_14,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_14' then 1 END) hyper_and_bmi_lt30_jul_14,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_14' then 1 END) hyper_and_bmi_lt30_oct_14,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_15' then 1 END) hyper_and_bmi_lt30_jan_15,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_15' then 1 END) hyper_and_bmi_lt30_apr_15,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_15' then 1 END) hyper_and_bmi_lt30_jul_15,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_15' then 1 END) hyper_and_bmi_lt30_oct_15,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_16' then 1 END) hyper_and_bmi_lt30_jan_16,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_16' then 1 END) hyper_and_bmi_lt30_apr_16,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jul_16' then 1 END) hyper_and_bmi_lt30_jul_16,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'oct_16' then 1 END) hyper_and_bmi_lt30_oct_16,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'jan_17' then 1 END) hyper_and_bmi_lt30_jan_17,
count(CASE WHEN condition = 'hyper_and_bmi_lt30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_17' then 1 END) hyper_and_bmi_lt30_apr_17,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'jan_12' then 1 END) hyper_and_smoker_jan_12,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'apr_12' then 1 END) hyper_and_smoker_apr_12,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'jul_12' then 1 END) hyper_and_smoker_jul_12,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'oct_12' then 1 END) hyper_and_smoker_oct_12,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'jan_13' then 1 END) hyper_and_smoker_jan_13,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'apr_13' then 1 END) hyper_and_smoker_apr_13,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'jul_13' then 1 END) hyper_and_smoker_jul_13,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'oct_13' then 1 END) hyper_and_smoker_oct_13,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'jan_14' then 1 END) hyper_and_smoker_jan_14,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'apr_14' then 1 END) hyper_and_smoker_apr_14,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'jul_14' then 1 END) hyper_and_smoker_jul_14,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'oct_14' then 1 END) hyper_and_smoker_oct_14,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'jan_15' then 1 END) hyper_and_smoker_jan_15,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'apr_15' then 1 END) hyper_and_smoker_apr_15,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'jul_15' then 1 END) hyper_and_smoker_jul_15,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'oct_15' then 1 END) hyper_and_smoker_oct_15,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'jan_16' then 1 END) hyper_and_smoker_jan_16,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'apr_16' then 1 END) hyper_and_smoker_apr_16,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'jul_16' then 1 END) hyper_and_smoker_jul_16,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'oct_16' then 1 END) hyper_and_smoker_oct_16,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'jan_17' then 1 END) hyper_and_smoker_jan_17,
count(CASE WHEN condition = 'hyper_and_smoker' and quarter_of_interest = 'apr_17' then 1 END) hyper_and_smoker_apr_17,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'jan_12' then 1 END) hyper_non_smoker_jan_12,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'apr_12' then 1 END) hyper_non_smoker_apr_12,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'jul_12' then 1 END) hyper_non_smoker_jul_12,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'oct_12' then 1 END) hyper_non_smoker_oct_12,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'jan_13' then 1 END) hyper_non_smoker_jan_13,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'apr_13' then 1 END) hyper_non_smoker_apr_13,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'jul_13' then 1 END) hyper_non_smoker_jul_13,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'oct_13' then 1 END) hyper_non_smoker_oct_13,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'jan_14' then 1 END) hyper_non_smoker_jan_14,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'apr_14' then 1 END) hyper_non_smoker_apr_14,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'jul_14' then 1 END) hyper_non_smoker_jul_14,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'oct_14' then 1 END) hyper_non_smoker_oct_14,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'jan_15' then 1 END) hyper_non_smoker_jan_15,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'apr_15' then 1 END) hyper_non_smoker_apr_15,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'jul_15' then 1 END) hyper_non_smoker_jul_15,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'oct_15' then 1 END) hyper_non_smoker_oct_15,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'jan_16' then 1 END) hyper_non_smoker_jan_16,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'apr_16' then 1 END) hyper_non_smoker_apr_16,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'jul_16' then 1 END) hyper_non_smoker_jul_16,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'oct_16' then 1 END) hyper_non_smoker_oct_16,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'jan_17' then 1 END) hyper_non_smoker_jan_17,
count(CASE WHEN condition = 'hyper_non_smoker' and quarter_of_interest = 'apr_17' then 1 END) hyper_non_smoker_apr_17
FROM 
esp_mdphnet.pwtf_comm_conditions_all_denom_and_num
GROUP BY age_group_10_yr,race,sex, town
order by town, sex, race, age_group_10_yr;



-- T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'

--select * from esp_mdphnet.pwtf_hist_comorbid_output_full_strat

-- select * from esp_mdphnet.pwtf_comm_conditions_all_denom_and_num order by patient_id, quarter_of_interest limit 2000




