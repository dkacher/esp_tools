DROP TABLE IF EXISTS esp_mdphnet.diop_denom_1;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_2;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_3;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_4;
DROP TABLE IF EXISTS esp_mdphnet.diop_amb_lifetime;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_5;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_6;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_7;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_8;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_9;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_10;
DROP TABLE IF EXISTS esp_mdphnet.diop_bmi_all_entries;
DROP TABLE IF EXISTS esp_mdphnet.diop_max_bmi_date;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_all;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_all_events;
DROP TABLE IF EXISTS esp_mdphnet.diop_max_hyper_date;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_all;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_all_events;
DROP TABLE IF EXISTS esp_mdphnet.diop_max_asthma_date;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_all;
DROP TABLE IF EXISTS esp_mdphnet.diop_max_smoking_date;
DROP TABLE IF EXISTS esp_mdphnet.diop_smoking_dateofint;
DROP TABLE IF EXISTS esp_mdphnet.diop_smoking_current;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d1;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d2;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d3;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d4;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d5;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d6;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d7;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d8;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d9;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d10;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d1;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d2;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d3;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d4;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d5;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d6;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d7;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d8;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d9;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d10;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d1;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d2;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d3;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d4;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d5;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d6;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d7;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d8;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d9;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d10;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d1;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d2;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d3;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d4;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d5;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d6;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d7;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d8;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d9;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d10;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d1;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d2;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d3;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d4;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d5;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d6;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d7;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d8;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d9;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d10;
DROP TABLE IF EXISTS esp_mdphnet.diop_all_conditions;
DROP TABLE IF EXISTS esp_mdphnet.diop_all_conditions_patient;

DROP TABLE IF EXISTS esp_mdphnet.diop_output_full_strat;


-- DENOM_1
-- RS/MDPHnet Encounters in the 1 years preceding 07-01-2016
-- with ≥1 encounter in the 1 year preceding the index date
CREATE  TABLE esp_mdphnet.diop_denom_1  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id, 'denom_encounter_1'::text condition 
FROM  gen_pop_tools.clin_enc T1, emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '6', '7', '34', '43')
AND date > '07-01-2016'::date - interval '1 years' and date < '07-01-2016')  AS DUMMYALIASt238 ;


-- DENOM_2
-- RS/MDPHnet Encounters in the 2 years preceding 07-01-2016
-- with ≥1 encounters in the 2 years preceding the index date
CREATE  TABLE esp_mdphnet.diop_denom_2  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id, 'denom_encounter_2'::text condition 
FROM  gen_pop_tools.clin_enc T1, emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '6', '7', '34', '43')
AND date > '07-01-2016'::date - interval '2 years' and date < '07-01-2016')  AS DUMMYALIASt238 ;

-- DENOM_3
-- RS/MDPHnet Encounters in the 1 years preceding 07-01-2016
-- with ≥2 encounters in the 1 year preceding the index date
CREATE  TABLE esp_mdphnet.diop_denom_3  AS 
SELECT count(*), patient_id, 'denom_encounter_3'::text condition 
FROM  gen_pop_tools.clin_enc T1, emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '6', '7', '34', '43')
AND date > '07-01-2016'::date - interval '1 years' and date < '07-01-2016'
GROUP BY patient_id
HAVING count(*) >=2;

-- DENOM_4
-- RS/MDPHnet Encounters in the 2 years preceding 07-01-2016
-- with ≥2 encounters in the 2 years preceding the index date
CREATE  TABLE esp_mdphnet.diop_denom_4  AS 
SELECT count(*), patient_id, 'denom_encounter_4'::text condition 
FROM  gen_pop_tools.clin_enc T1, emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '6', '7', '34', '43')
AND date > '07-01-2016'::date - interval '2 years' and date < '07-01-2016'
GROUP BY patient_id
HAVING count(*) >=2;

-- LIFETIME ENCOUNTERS 
CREATE  TABLE esp_mdphnet.diop_amb_lifetime  AS 
SELECT count(*), patient_id, 'amb_liftime_encs'::text condition 
FROM  gen_pop_tools.clin_enc T1, emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '6', '7', '34', '43')
and date < '07-01-2016'
GROUP BY patient_id;

-- DENOM5
-- with ≥1 encounter (with a dx code or vital sign) in the 1 year preceding the index date AND ≥2 lifetime encounters (with a dx code or vital sign) 
CREATE  TABLE esp_mdphnet.diop_denom_5  AS
SELECT T1.patient_id, 'denom_encounter_5'::text condition 
FROM esp_mdphnet.diop_amb_lifetime T1, esp_mdphnet.diop_denom_1 T2
WHERE T1.patient_id = T2.patient_id
AND T1.count >= 2;

-- DENOM6
-- with ≥1 encounter (with a dx code or vital sign) in the 1 year preceding the index date AND ≥5 lifetime encounters (with a dx code or vital sign)
CREATE  TABLE esp_mdphnet.diop_denom_6  AS
SELECT T1.patient_id, 'denom_encounter_6'::text condition 
FROM esp_mdphnet.diop_amb_lifetime T1, esp_mdphnet.diop_denom_1 T2
WHERE T1.patient_id = T2.patient_id
AND T1.count >= 5;

-- DENOM7
-- with ≥1 encounter (with a dx code or vital sign) in the 1 year preceding the index date AND ≥10 lifetime encounters (with a dx code or vital sign)
CREATE  TABLE esp_mdphnet.diop_denom_7  AS
SELECT T1.patient_id, 'denom_encounter_7'::text condition 
FROM esp_mdphnet.diop_amb_lifetime T1, esp_mdphnet.diop_denom_1 T2
WHERE T1.patient_id = T2.patient_id
AND T1.count >= 10;

-- DENOM8
-- with ≥1 encounter (with a dx code or vital sign) in the 2 years preceding the index date AND ≥2 lifetime encounters (with a dx code or vital sign)
CREATE  TABLE esp_mdphnet.diop_denom_8  AS
SELECT T1.patient_id, 'denom_encounter_8'::text condition 
FROM esp_mdphnet.diop_amb_lifetime T1, esp_mdphnet.diop_denom_2 T2
WHERE T1.patient_id = T2.patient_id
AND T1.count >= 2;

-- DENOM9
-- with ≥1 encounter (with a dx code or vital sign) in the 2 years preceding the index date AND ≥5 lifetime encounters (with a dx code or vital sign)
CREATE  TABLE esp_mdphnet.diop_denom_9  AS
SELECT T1.patient_id, 'denom_encounter_9'::text condition 
FROM esp_mdphnet.diop_amb_lifetime T1, esp_mdphnet.diop_denom_2 T2
WHERE T1.patient_id = T2.patient_id
AND T1.count >= 5;

-- DENOM10: 
-- with ≥1 encounter (with a dx code or vital sign) in the 2 years preceding the index date AND ≥10 lifetime encounters (with a dx code or vital sign)
CREATE  TABLE esp_mdphnet.diop_denom_10  AS
SELECT T1.patient_id, 'denom_encounter_10'::text condition 
FROM esp_mdphnet.diop_amb_lifetime T1, esp_mdphnet.diop_denom_2 T2
WHERE T1.patient_id = T2.patient_id
AND T1.count >= 10;

-- BMI Conditions - All condition entries during the time span of the report
CREATE  TABLE esp_mdphnet.diop_bmi_all_entries   AS 
SELECT T2.id patient_id,T1.condition,(T1.date + '1960-01-01'::date) date 
FROM public.esp_condition T1 
INNER JOIN public.emr_patient T2 ON ((T1.patid = T2.natural_key))  
WHERE (date + '1960-01-01'::date) < '07-01-2016' 
and condition in ( 'BMI <25',  'BMI >= 30',  'BMI >=25 and <30', 'No Measured BMI');

-- BMI - Most recent BMI condition date 
CREATE  TABLE esp_mdphnet.diop_max_bmi_date  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date
FROM esp_mdphnet.diop_bmi_all_entries T1
GROUP BY T1.patient_id;

-- BMI - Get the patients who are classified as obese as of the index date
CREATE  TABLE esp_mdphnet.diop_obese_all   AS 
SELECT distinct on (T1.patient_id) T1.patient_id, T2.condition
FROM esp_mdphnet.diop_max_bmi_date T1 
INNER JOIN esp_mdphnet.diop_bmi_all_entries T2 
ON ((T1.patient_id = T2.patient_id) AND (T1.max_bmi_date = T2.date)) 
WHERE T2.condition = 'BMI >= 30';

-- HYPERTENSION All hypertension cases and events
CREATE  TABLE esp_mdphnet.diop_hyper_all_events   AS 
select T1.patient_id, T1.id, T1.condition, T1.date as case_date, T2.date as history_date, T2.status, T1.isactive
from nodis_case T1, nodis_caseactivehistory T2
WHERE T1.date < '07-01-2016' 
AND T2.date < '07-01-2016'
and condition = 'hypertension'
ANd T1.id = T2.case_id
order by case_id;

-- HYPERTENSION Most recent hypertension date prior to the index date - 07-01-2016
CREATE  TABLE esp_mdphnet.diop_max_hyper_date AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date 
FROM  esp_mdphnet.diop_hyper_all_events  T1  
WHERE  history_date < '07-01-2016'
GROUP BY T1.patient_id;

-- HYPERTENSION- Get details for active hypertension
CREATE  TABLE esp_mdphnet.diop_hyper_all   AS 
SELECT T1.patient_id, T2.id as case_id, T2.status, T2.condition, T2.history_date as hyper_active_date
FROM esp_mdphnet.diop_max_hyper_date T1 
INNER JOIN esp_mdphnet.diop_hyper_all_events  T2 
ON ((T1.patient_id = T2.patient_id) 
AND (T1.max_case_event_date = T2.history_date))
AND status in ('I', 'R');

-- ACTIVE ASTHMA case and events prior to index date
CREATE  TABLE esp_mdphnet.diop_asthma_all_events AS 
select T1.patient_id, T1.id, T1.condition, T1.date as case_date, T2.date as history_date, T2.status, T1.isactive
from nodis_case T1, nodis_caseactivehistory T2
WHERE T1.date < '07-01-2016' 
AND T2.date < '07-01-2016'
and condition = 'asthma'
ANd T1.id = T2.case_id
order by case_id;

-- ACTIVE ASTHMA Most recent asthma date prior to the index date - 07-01-2016
CREATE  TABLE esp_mdphnet.diop_max_asthma_date AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date 
FROM  esp_mdphnet.diop_asthma_all_events T1
WHERE  history_date < '07-01-2016'
GROUP BY T1.patient_id;

-- ACTIVE ASTHMA - Get details for active asthma
CREATE  TABLE esp_mdphnet.diop_asthma_all AS 
SELECT T1.patient_id, T2.id as case_id, T2.status, T2.condition , T2.history_date as asthma_active_date
FROM esp_mdphnet.diop_max_asthma_date T1 
INNER JOIN esp_mdphnet.diop_asthma_all_events T2 
ON ((T1.patient_id = T2.patient_id) 
AND (T1.max_case_event_date = T2.history_date))
AND status in ('I', 'R');

-- SMOKING - Get the most recent smoking date prior to the index date. 07-01-2016
-- USE THIS FOR COUNT OF MEASURED SMOKING STATUS
CREATE  TABLE esp_mdphnet.diop_max_smoking_date AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T1.date) max_smoking_date 
FROM public.emr_socialhistory T1 
WHERE  T1.date < '07-01-2016' 
AND T1.tobacco_use is not null and T1.tobacco_use != '' and T1.tobacco_use not ilike 'NOT ASKED'
GROUP BY T1.patient_id) AS DUMMYALIAS239 ;

-- SMOKING - Get tobacco_use for the date of interest
CREATE  TABLE esp_mdphnet.diop_smoking_dateofint AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.tobacco_use,
case when tobacco_use ILIKE ANY(ARRAY['%adults%', '%aunt%', '%both%', '%brother%', '%cousin%', '%dad%', '%everyone%', '%exposure%','%family smokes%', '%father%', '%fob%', '%foc%', '%gm%', '%gf%',  '%grandaughter%','%grandfather%', '%grandma%', '%grandmom%', '%grandmother%', '%grandparent%', '%husband%', '%many smokers%', '%members%', '%mgf%', '% mo %', '%mob%', '%moc%',  '%mgm%','%mggm%', '%mom%', '%mothr%', '%mothe%', '%parent%', '%paretns%', '%passive%', '%sister%', '% uncle %', '%uncle.%'])  
	AND tobacco_use NOT ILIKE ALL(ARRAY['have cut back%']) 
then 'Passive'      
when (
	tobacco_use in ('N','Never','no','Denies.','Nonsmoker.','never','No. .','none','0','negative','Nonsmoker','None.','Denies','Non-smoker','None','Non smoker','Never smoked.','non smoker','Non-smoker','Non-smoker.',         'Non smoker.','nonsmoker')
	or tobacco_use ILIKE ANY(ARRAY['%abstain%', 'No%', '%doesn%smoke%', 'negative%', '%never%'])
	)
	AND tobacco_use NOT ILIKE ALL(ARRAY['%quit%', '%plans%', '%smoke outside%', '%not clear%', '%daily%', '%not%', 'yes%', '%cut back%', 'smoked%', '%day%'])
then 'Never'  
when (
	tobacco_use in ('R','Prev','Previous','quit','Quit smoking','Former smoker.','Former smoker', 'Former','prev.','Quit.','quit smoking', 'X', '0 currently', 'Complete cessation', 'No. not at new apartment. .')
	or tobacco_use ~ '^[0123456789]{1,2}/[[0123456789]{1,2}/[0123456789]{2,4}'           
	or tobacco_use ILIKE ANY(ARRAY['%quit%', '%previous%', '%used to%', '%former%', 'jan%', 'june%', '%long time ago', '%not%smoking%', '%patch%', '%smoked%', '%pt quit%', '%quit%chantix%'])
	)           
	AND tobacco_use NOT ILIKE ALL(ARRAY['%less%', '%trying%', '%plans%', '%advised%', '%complications%', 'current%', '%quitting%', '%cig%patch%',  '%needs to%', '%not in house%', '%on/off%',            '%would like to%', '%quit by%', '%pick a date%', '%to quit%', '%to have more money%', 'up to%'])           
then 'Former'  
when (
	tobacco_use in ('Y','Current','X','Yes. .','precontemplative','counseled','one', 'Very Ready','Post Partum','ready','contemplative', 'Yes. outside. .','relapse','rare', 'rarely')
	or tobacco_use ~ '^[0123456789]'        
	or tobacco_use ~ '^.[0123456789]'        
	or tobacco_use ILIKE ANY(ARRAY['%<%','%>%','%~%','about%','%addicted%','%advised%', '%a lot%', '%almost%', '%approx%', '%apprx%','%as much%', '%avoid%', '%breath%', '%cancer%', '%casual%', '%cessation%', '%chew%', '%children%',         '%cig%', '%complica%', '%contemplat%','%continue%','%continues%', '%couple%','%current%', '%cut back%','%cut down%', '%daily%', '%day%', '%depends%', '%drinking%', '%enough%', '%expensive%', '%feel%', '%few%', '%half%', '%handout%', '%hardly%', '%harm%', '%health%','%infreq%','%improve%','%interm%', '%less%', '%live%', '%more than%', '%needs to%', '%occ%','%off and on%', '%on weekends%', '%one%', '%only%', '%pack%', '%cig%patch%', '%pipe%', '%pkg%','%positive%', '%quit%', '%rare%', '%ready%', '%recreation%', '%roll%', '%since%', '%smoke outside%',                       '%seldom%', '%smoke%', '%smoking%', '%social%', '%some%', '%start%', '%still%','%stress%', '%to be%','%tobacco%','%trying%', '%up to%', '%under%', '%var%', '%wants to%', '%while%', '%yes%']) 
	)
	AND tobacco_use not ILIKE ALL(ARRAY['~', '%refus%','%unknown%', '%not cigarettes%', '%pt is no%', '%w/o%'])
then 'Current'                      
else 'Not available' end  smoking 
FROM esp_mdphnet.diop_max_smoking_date T1 
INNER JOIN public.emr_socialhistory T2 
ON ((T1.patient_id = T2.patient_id) 
AND (T1.max_smoking_date = T2.date))
WHERE T2.tobacco_use is not null and T2.tobacco_use != '' and T2.tobacco_use not ilike 'NOT ASKED' )  AS DUMMYALIAS257 ;

-- SMOKING - CURRENT (yes) smokers.
CREATE  TABLE esp_mdphnet.diop_smoking_current AS 
SELECT DISTINCT *, 'current_smoker'::text AS condition FROM (SELECT T1.patient_id,T1.tobacco_use 
FROM  esp_mdphnet.diop_smoking_dateofint T1   
WHERE  smoking =  'Current' )  AS DUMMYALIAS258 ;

-- OBESE DENOM1
CREATE TABLE esp_mdphnet.diop_obese_d1 AS
SELECT T1.patient_id, 'obese_d1'::text as condition 
FROM esp_mdphnet.diop_obese_all T1, 
esp_mdphnet.diop_denom_1 T2
WHERE T1.patient_id = T2.patient_id;

-- OBESE DENOM2
CREATE TABLE esp_mdphnet.diop_obese_d2 AS
SELECT T1.patient_id, 'obese_d2'::text as condition 
FROM esp_mdphnet.diop_obese_all T1, 
esp_mdphnet.diop_denom_2 T2
WHERE T1.patient_id = T2.patient_id;

-- OBESE DENOM3
CREATE TABLE esp_mdphnet.diop_obese_d3 AS
SELECT T1.patient_id, 'obese_d3'::text as condition 
FROM esp_mdphnet.diop_obese_all T1, 
esp_mdphnet.diop_denom_3 T2
WHERE T1.patient_id = T2.patient_id;

-- OBESE DENOM4
CREATE TABLE esp_mdphnet.diop_obese_d4 AS
SELECT T1.patient_id, 'obese_d4'::text as condition 
FROM esp_mdphnet.diop_obese_all T1, 
esp_mdphnet.diop_denom_4 T2
WHERE T1.patient_id = T2.patient_id;

-- OBESE DENOM5
CREATE TABLE esp_mdphnet.diop_obese_d5 AS
SELECT T1.patient_id, 'obese_d5'::text as condition 
FROM esp_mdphnet.diop_obese_all T1, 
esp_mdphnet.diop_denom_5 T2
WHERE T1.patient_id = T2.patient_id;

-- OBESE DENOM6
CREATE TABLE esp_mdphnet.diop_obese_d6 AS
SELECT T1.patient_id, 'obese_d6'::text as condition 
FROM esp_mdphnet.diop_obese_all T1, 
esp_mdphnet.diop_denom_6 T2
WHERE T1.patient_id = T2.patient_id;

-- OBESE DENOM7
CREATE TABLE esp_mdphnet.diop_obese_d7 AS
SELECT T1.patient_id, 'obese_d7'::text as condition 
FROM esp_mdphnet.diop_obese_all T1, 
esp_mdphnet.diop_denom_7 T2
WHERE T1.patient_id = T2.patient_id;

-- OBESE DENOM8
CREATE TABLE esp_mdphnet.diop_obese_d8 AS
SELECT T1.patient_id, 'obese_d8'::text as condition 
FROM esp_mdphnet.diop_obese_all T1, 
esp_mdphnet.diop_denom_8 T2
WHERE T1.patient_id = T2.patient_id;

-- OBESE DENOM9
CREATE TABLE esp_mdphnet.diop_obese_d9 AS
SELECT T1.patient_id, 'obese_d9'::text as condition 
FROM esp_mdphnet.diop_obese_all T1, 
esp_mdphnet.diop_denom_9 T2
WHERE T1.patient_id = T2.patient_id;

-- OBESE DENOM10
CREATE TABLE esp_mdphnet.diop_obese_d10 AS
SELECT T1.patient_id, 'obese_d10'::text as condition 
FROM esp_mdphnet.diop_obese_all T1, 
esp_mdphnet.diop_denom_10 T2
WHERE T1.patient_id = T2.patient_id;

-- HYPERTENSION DENOM1
CREATE TABLE esp_mdphnet.diop_hyper_d1 AS
SELECT T1.patient_id, 'hyper_d1'::text as condition 
FROM esp_mdphnet.diop_hyper_all T1, 
esp_mdphnet.diop_denom_1 T2
WHERE T1.patient_id = T2.patient_id;

-- HYPERTENSION DENOM2
CREATE TABLE esp_mdphnet.diop_hyper_d2 AS
SELECT T1.patient_id, 'hyper_d2'::text as condition 
FROM esp_mdphnet.diop_hyper_all T1, 
esp_mdphnet.diop_denom_2 T2
WHERE T1.patient_id = T2.patient_id;

-- HYPERTENSION DENOM3
CREATE TABLE esp_mdphnet.diop_hyper_d3 AS
SELECT T1.patient_id, 'hyper_d3'::text as condition 
FROM esp_mdphnet.diop_hyper_all T1, 
esp_mdphnet.diop_denom_3 T2
WHERE T1.patient_id = T2.patient_id;

-- HYPERTENSION DENOM4
CREATE TABLE esp_mdphnet.diop_hyper_d4 AS
SELECT T1.patient_id, 'hyper_d4'::text as condition 
FROM esp_mdphnet.diop_hyper_all T1, 
esp_mdphnet.diop_denom_4 T2
WHERE T1.patient_id = T2.patient_id;

-- HYPERTENSION DENOM5
CREATE TABLE esp_mdphnet.diop_hyper_d5 AS
SELECT T1.patient_id, 'hyper_d5'::text as condition 
FROM esp_mdphnet.diop_hyper_all T1, 
esp_mdphnet.diop_denom_5 T2
WHERE T1.patient_id = T2.patient_id;

-- HYPERTENSION DENOM6
CREATE TABLE esp_mdphnet.diop_hyper_d6 AS
SELECT T1.patient_id, 'hyper_d6'::text as condition 
FROM esp_mdphnet.diop_hyper_all T1, 
esp_mdphnet.diop_denom_6 T2
WHERE T1.patient_id = T2.patient_id;

-- HYPERTENSION DENOM7
CREATE TABLE esp_mdphnet.diop_hyper_d7 AS
SELECT T1.patient_id, 'hyper_d7'::text as condition 
FROM esp_mdphnet.diop_hyper_all T1, 
esp_mdphnet.diop_denom_7 T2
WHERE T1.patient_id = T2.patient_id;

-- HYPERTENSION DENOM8
CREATE TABLE esp_mdphnet.diop_hyper_d8 AS
SELECT T1.patient_id, 'hyper_d8'::text as condition 
FROM esp_mdphnet.diop_hyper_all T1, 
esp_mdphnet.diop_denom_8 T2
WHERE T1.patient_id = T2.patient_id;

-- HYPERTENSION DENOM9
CREATE TABLE esp_mdphnet.diop_hyper_d9 AS
SELECT T1.patient_id, 'hyper_d9'::text as condition 
FROM esp_mdphnet.diop_hyper_all T1, 
esp_mdphnet.diop_denom_9 T2
WHERE T1.patient_id = T2.patient_id;

-- HYPERTENSION DENOM10
CREATE TABLE esp_mdphnet.diop_hyper_d10 AS
SELECT T1.patient_id, 'hyper_d10'::text as condition 
FROM esp_mdphnet.diop_hyper_all T1, 
esp_mdphnet.diop_denom_10 T2
WHERE T1.patient_id = T2.patient_id;


-- ASTHMA DENOM1
CREATE TABLE esp_mdphnet.diop_asthma_d1 AS
SELECT T1.patient_id, 'asthma_d1'::text as condition 
FROM esp_mdphnet.diop_asthma_all T1, 
esp_mdphnet.diop_denom_1 T2
WHERE T1.patient_id = T2.patient_id;

-- ASTHMA DENOM2
CREATE TABLE esp_mdphnet.diop_asthma_d2 AS
SELECT T1.patient_id, 'asthma_d2'::text as condition 
FROM esp_mdphnet.diop_asthma_all T1, 
esp_mdphnet.diop_denom_2 T2
WHERE T1.patient_id = T2.patient_id;

-- ASTHMA DENOM3
CREATE TABLE esp_mdphnet.diop_asthma_d3 AS
SELECT T1.patient_id, 'asthma_d3'::text as condition 
FROM esp_mdphnet.diop_asthma_all T1, 
esp_mdphnet.diop_denom_3 T2
WHERE T1.patient_id = T2.patient_id;

-- ASTHMA DENOM4
CREATE TABLE esp_mdphnet.diop_asthma_d4 AS
SELECT T1.patient_id, 'asthma_d4'::text as condition 
FROM esp_mdphnet.diop_asthma_all T1, 
esp_mdphnet.diop_denom_4 T2
WHERE T1.patient_id = T2.patient_id;

-- ASTHMA DENOM5
CREATE TABLE esp_mdphnet.diop_asthma_d5 AS
SELECT T1.patient_id, 'asthma_d5'::text as condition 
FROM esp_mdphnet.diop_asthma_all T1, 
esp_mdphnet.diop_denom_5 T2
WHERE T1.patient_id = T2.patient_id;

-- ASTHMA DENOM6
CREATE TABLE esp_mdphnet.diop_asthma_d6 AS
SELECT T1.patient_id, 'asthma_d6'::text as condition 
FROM esp_mdphnet.diop_asthma_all T1, 
esp_mdphnet.diop_denom_6 T2
WHERE T1.patient_id = T2.patient_id;

-- ASTHMA DENOM7
CREATE TABLE esp_mdphnet.diop_asthma_d7 AS
SELECT T1.patient_id, 'asthma_d7'::text as condition 
FROM esp_mdphnet.diop_asthma_all T1, 
esp_mdphnet.diop_denom_7 T2
WHERE T1.patient_id = T2.patient_id;

-- ASTHMA DENOM8
CREATE TABLE esp_mdphnet.diop_asthma_d8 AS
SELECT T1.patient_id, 'asthma_d8'::text as condition 
FROM esp_mdphnet.diop_asthma_all T1, 
esp_mdphnet.diop_denom_8 T2
WHERE T1.patient_id = T2.patient_id;

-- ASTHMA DENOM9
CREATE TABLE esp_mdphnet.diop_asthma_d9 AS
SELECT T1.patient_id, 'asthma_d9'::text as condition 
FROM esp_mdphnet.diop_asthma_all T1, 
esp_mdphnet.diop_denom_9 T2
WHERE T1.patient_id = T2.patient_id;

-- ASTHMA DENOM10
CREATE TABLE esp_mdphnet.diop_asthma_d10 AS
SELECT T1.patient_id, 'asthma_d10'::text as condition 
FROM esp_mdphnet.diop_asthma_all T1, 
esp_mdphnet.diop_denom_10 T2
WHERE T1.patient_id = T2.patient_id;

-- CURRENT SMOKER DENOM1
CREATE TABLE esp_mdphnet.diop_cur_smoker_d1 AS
SELECT T1.patient_id, 'cur_smoker_d1'::text as condition 
FROM esp_mdphnet.diop_smoking_current T1, 
esp_mdphnet.diop_denom_1 T2
WHERE T1.patient_id = T2.patient_id;

-- CURRENT SMOKER DENOM2
CREATE TABLE esp_mdphnet.diop_cur_smoker_d2 AS
SELECT T1.patient_id, 'cur_smoker_d2'::text as condition 
FROM esp_mdphnet.diop_smoking_current T1, 
esp_mdphnet.diop_denom_2 T2
WHERE T1.patient_id = T2.patient_id;

-- CURRENT SMOKER DENOM3
CREATE TABLE esp_mdphnet.diop_cur_smoker_d3 AS
SELECT T1.patient_id, 'cur_smoker_d3'::text as condition 
FROM esp_mdphnet.diop_smoking_current T1, 
esp_mdphnet.diop_denom_3 T2
WHERE T1.patient_id = T2.patient_id;

-- CURRENT SMOKER DENOM4
CREATE TABLE esp_mdphnet.diop_cur_smoker_d4 AS
SELECT T1.patient_id, 'cur_smoker_d4'::text as condition 
FROM esp_mdphnet.diop_smoking_current T1, 
esp_mdphnet.diop_denom_4 T2
WHERE T1.patient_id = T2.patient_id;

-- CURRENT SMOKER DENOM5
CREATE TABLE esp_mdphnet.diop_cur_smoker_d5 AS
SELECT T1.patient_id, 'cur_smoker_d5'::text as condition 
FROM esp_mdphnet.diop_smoking_current T1, 
esp_mdphnet.diop_denom_5 T2
WHERE T1.patient_id = T2.patient_id;

-- CURRENT SMOKER DENOM6
CREATE TABLE esp_mdphnet.diop_cur_smoker_d6 AS
SELECT T1.patient_id, 'cur_smoker_d6'::text as condition 
FROM esp_mdphnet.diop_smoking_current T1, 
esp_mdphnet.diop_denom_6 T2
WHERE T1.patient_id = T2.patient_id;

-- CURRENT SMOKER DENOM7
CREATE TABLE esp_mdphnet.diop_cur_smoker_d7 AS
SELECT T1.patient_id, 'cur_smoker_d7'::text as condition 
FROM esp_mdphnet.diop_smoking_current T1, 
esp_mdphnet.diop_denom_7 T2
WHERE T1.patient_id = T2.patient_id;

-- CURRENT SMOKER DENOM8
CREATE TABLE esp_mdphnet.diop_cur_smoker_d8 AS
SELECT T1.patient_id, 'cur_smoker_d8'::text as condition 
FROM esp_mdphnet.diop_smoking_current T1, 
esp_mdphnet.diop_denom_8 T2
WHERE T1.patient_id = T2.patient_id;

-- CURRENT SMOKER DENOM9
CREATE TABLE esp_mdphnet.diop_cur_smoker_d9 AS
SELECT T1.patient_id, 'cur_smoker_d9'::text as condition 
FROM esp_mdphnet.diop_smoking_current T1, 
esp_mdphnet.diop_denom_9 T2
WHERE T1.patient_id = T2.patient_id;

-- CURRENT SMOKER DENOM10
CREATE TABLE esp_mdphnet.diop_cur_smoker_d10 AS
SELECT T1.patient_id, 'cur_smoker_d10'::text as condition 
FROM esp_mdphnet.diop_smoking_current T1, 
esp_mdphnet.diop_denom_10 T2
WHERE T1.patient_id = T2.patient_id;

-- MEASURED SMOKING STATUS DENOM1
CREATE TABLE esp_mdphnet.diop_meas_smoking_d1 AS
SELECT T1.patient_id, 'meas_smoking_d1'::text as condition 
FROM esp_mdphnet.diop_max_smoking_date T1, 
esp_mdphnet.diop_denom_1 T2
WHERE T1.patient_id = T2.patient_id;

-- MEASURED SMOKING STATUS DENOM2
CREATE TABLE esp_mdphnet.diop_meas_smoking_d2 AS
SELECT T1.patient_id, 'meas_smoking_d2'::text as condition 
FROM esp_mdphnet.diop_max_smoking_date T1, 
esp_mdphnet.diop_denom_2 T2
WHERE T1.patient_id = T2.patient_id;

-- MEASURED SMOKING STATUS DENOM3
CREATE TABLE esp_mdphnet.diop_meas_smoking_d3 AS
SELECT T1.patient_id, 'meas_smoking_d3'::text as condition 
FROM esp_mdphnet.diop_max_smoking_date T1, 
esp_mdphnet.diop_denom_3 T2
WHERE T1.patient_id = T2.patient_id;

-- MEASURED SMOKING STATUS DENOM4
CREATE TABLE esp_mdphnet.diop_meas_smoking_d4 AS
SELECT T1.patient_id, 'meas_smoking_d4'::text as condition 
FROM esp_mdphnet.diop_max_smoking_date T1, 
esp_mdphnet.diop_denom_4 T2
WHERE T1.patient_id = T2.patient_id;

-- MEASURED SMOKING STATUS DENOM5
CREATE TABLE esp_mdphnet.diop_meas_smoking_d5 AS
SELECT T1.patient_id, 'meas_smoking_d5'::text as condition 
FROM esp_mdphnet.diop_max_smoking_date T1, 
esp_mdphnet.diop_denom_5 T2
WHERE T1.patient_id = T2.patient_id;

-- MEASURED SMOKING STATUS DENOM6
CREATE TABLE esp_mdphnet.diop_meas_smoking_d6 AS
SELECT T1.patient_id, 'meas_smoking_d6'::text as condition 
FROM esp_mdphnet.diop_max_smoking_date T1, 
esp_mdphnet.diop_denom_6 T2
WHERE T1.patient_id = T2.patient_id;

-- MEASURED SMOKING STATUS DENOM7
CREATE TABLE esp_mdphnet.diop_meas_smoking_d7 AS
SELECT T1.patient_id, 'meas_smoking_d7'::text as condition 
FROM esp_mdphnet.diop_max_smoking_date T1, 
esp_mdphnet.diop_denom_7 T2
WHERE T1.patient_id = T2.patient_id;

-- MEASURED SMOKING STATUS DENOM8
CREATE TABLE esp_mdphnet.diop_meas_smoking_d8 AS
SELECT T1.patient_id, 'meas_smoking_d8'::text as condition 
FROM esp_mdphnet.diop_max_smoking_date T1, 
esp_mdphnet.diop_denom_8 T2
WHERE T1.patient_id = T2.patient_id;

-- MEASURED SMOKING STATUS DENOM9
CREATE TABLE esp_mdphnet.diop_meas_smoking_d9 AS
SELECT T1.patient_id, 'meas_smoking_d9'::text as condition 
FROM esp_mdphnet.diop_max_smoking_date T1, 
esp_mdphnet.diop_denom_9 T2
WHERE T1.patient_id = T2.patient_id;

-- MEASURED SMOKING STATUS DENOM10
CREATE TABLE esp_mdphnet.diop_meas_smoking_d10 AS
SELECT T1.patient_id, 'meas_smoking_d10'::text as condition 
FROM esp_mdphnet.diop_max_smoking_date T1, 
esp_mdphnet.diop_denom_10 T2
WHERE T1.patient_id = T2.patient_id;

-- UNION together all of the patients and conditions
CREATE TABLE esp_mdphnet.diop_all_conditions AS
SELECT patient_id, condition FROM esp_mdphnet.diop_denom_1
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_denom_2
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_denom_3
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_denom_4
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_denom_5
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_denom_6
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_denom_7
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_denom_8
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_denom_9
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_denom_10
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_obese_d1
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_obese_d2
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_obese_d3
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_obese_d4
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_obese_d5
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_obese_d6
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_obese_d7
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_obese_d8
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_obese_d9
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_obese_d10
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_hyper_d1
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_hyper_d2
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_hyper_d3
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_hyper_d4
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_hyper_d5
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_hyper_d6
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_hyper_d7
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_hyper_d8
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_hyper_d9
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_hyper_d10
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_asthma_d1
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_asthma_d2
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_asthma_d3
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_asthma_d4
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_asthma_d5
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_asthma_d6
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_asthma_d7
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_asthma_d8
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_asthma_d9
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_asthma_d10
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_cur_smoker_d1
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_cur_smoker_d2
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_cur_smoker_d3
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_cur_smoker_d4
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_cur_smoker_d5
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_cur_smoker_d6
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_cur_smoker_d7
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_cur_smoker_d8
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_cur_smoker_d9
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_cur_smoker_d10
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_meas_smoking_d1
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_meas_smoking_d2
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_meas_smoking_d3
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_meas_smoking_d4
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_meas_smoking_d5
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_meas_smoking_d6
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_meas_smoking_d7
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_meas_smoking_d8
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_meas_smoking_d9
UNION
SELECT patient_id, condition FROM esp_mdphnet.diop_meas_smoking_d10;

-- GET PATIENT DETAILS
CREATE  TABLE esp_mdphnet.diop_all_conditions_patient AS 
SELECT T1.patient_id, T1.condition,
CASE   
when date_part('year', age('2016-07-01', date_of_birth)) <= 9 then '0-9'    
when date_part('year', age('2016-07-01', date_of_birth)) <= 19 then '10-19'  
when date_part('year', age('2016-07-01', date_of_birth)) <= 39 then '20-39' 
when date_part('year', age('2016-07-01', date_of_birth)) <= 59 then '40-59' 
when date_part('year', age('2016-07-01', date_of_birth)) <= 79 then '60-79'   
when date_of_birth is null then 'UNKNOWN AGE'
else '80+' 
end age_group_10_yr, 
T2.natural_key,
T3.sex,
CASE when T3.race_ethnicity = 6 then 'hispanic'  
when T3.race_ethnicity = 5 then 'white' 
when T3.race_ethnicity = 3 then 'black' 
when T3.race_ethnicity = 2 then 'asian' 
when T3.race_ethnicity = 1 then 'native_american' 
when T3.race_ethnicity = 0 then 'unknown' 
end race
FROM esp_mdphnet.diop_all_conditions T1
INNER JOIN public.emr_patient T2 ON (T1.patient_id = T2.id )
INNER JOIN esp_mdphnet.esp_demographic T3 on (T2.natural_key = T3.patid);

-- Full Stratified output
-- INCLUDE UNKNOWN AGE IN COUNTS?!?!?!
CREATE TABLE esp_mdphnet.diop_output_full_strat AS 
SELECT
age_group_10_yr, race, sex,
count(CASE WHEN condition = 'denom_encounter_1' THEN 1 END) denom_encounter_1,
count(CASE WHEN condition = 'hyper_d1' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) hyper_gte20_d1,
count(CASE WHEN condition = 'obese_d1' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) obese_gte20_d1,
count(CASE WHEN condition = 'asthma_d1' AND age_group_10_yr = '0-9' THEN 1 END) asthma_lte9_d1,
count(CASE WHEN condition = 'asthma_d1' AND age_group_10_yr = '10-19' THEN 1 END) asthma_gte10_lte19_d1,
count(CASE WHEN condition = 'asthma_d1' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) asthma_gte20_d1,
count(CASE WHEN condition = 'cur_smoker_d1' AND age_group_10_yr = '10-19' THEN 1 END) cur_smoker_gte10_lte19_d1,
count(CASE WHEN condition = 'cur_smoker_d1' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) cur_smoker_gte20_d1,
count(CASE WHEN condition = 'meas_smoking_d1' AND age_group_10_yr = '10-19' THEN 1 END) meas_smoking_gte10_lte19_d1,
count(CASE WHEN condition = 'meas_smoking_d1' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) meas_smoking_gte20_d1,
count(CASE WHEN condition = 'denom_encounter_2' THEN 1 END) denom_encounter_2,
count(CASE WHEN condition = 'hyper_d2' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) hyper_gte20_d2,
count(CASE WHEN condition = 'obese_d2' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) obese_gte20_d2,
count(CASE WHEN condition = 'asthma_d2' AND age_group_10_yr = '0-9' THEN 1 END) asthma_lte9_d2,
count(CASE WHEN condition = 'asthma_d2' AND age_group_10_yr = '10-19' THEN 1 END) asthma_gte10_lte19_d2,
count(CASE WHEN condition = 'asthma_d2' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) asthma_gte20_d2,
count(CASE WHEN condition = 'cur_smoker_d2' AND age_group_10_yr = '10-19' THEN 1 END) cur_smoker_gte10_lte19_d2,
count(CASE WHEN condition = 'cur_smoker_d2' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) cur_smoker_gte20_d2,
count(CASE WHEN condition = 'meas_smoking_d2' AND age_group_10_yr = '10-19' THEN 1 END) meas_smoking_gte10_lte19_d2,
count(CASE WHEN condition = 'meas_smoking_d2' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) meas_smoking_gte20_d2,
count(CASE WHEN condition = 'denom_encounter_3' THEN 1 END) denom_encounter_3,
count(CASE WHEN condition = 'hyper_d3' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) hyper_gte20_d3,
count(CASE WHEN condition = 'obese_d3' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) obese_gte20_d3,
count(CASE WHEN condition = 'asthma_d3' AND age_group_10_yr = '0-9' THEN 1 END) asthma_lte9_d3,
count(CASE WHEN condition = 'asthma_d3' AND age_group_10_yr = '10-19' THEN 1 END) asthma_gte10_lte19_d3,
count(CASE WHEN condition = 'asthma_d3' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) asthma_gte20_d3,
count(CASE WHEN condition = 'cur_smoker_d3' AND age_group_10_yr = '10-19' THEN 1 END) cur_smoker_gte10_lte19_d3,
count(CASE WHEN condition = 'cur_smoker_d3' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) cur_smoker_gte20_d3,
count(CASE WHEN condition = 'meas_smoking_d3' AND age_group_10_yr = '10-19' THEN 1 END) meas_smoking_gte10_lte19_d3,
count(CASE WHEN condition = 'meas_smoking_d3' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) meas_smoking_gte20_d3,
count(CASE WHEN condition = 'denom_encounter_4' THEN 1 END) denom_encounter_4,
count(CASE WHEN condition = 'hyper_d4' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) hyper_gte20_d4,
count(CASE WHEN condition = 'obese_d4' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) obese_gte20_d4,
count(CASE WHEN condition = 'asthma_d4' AND age_group_10_yr = '0-9' THEN 1 END) asthma_lte9_d4,
count(CASE WHEN condition = 'asthma_d4' AND age_group_10_yr = '10-19' THEN 1 END) asthma_gte10_lte19_d4,
count(CASE WHEN condition = 'asthma_d4' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) asthma_gte20_d4,
count(CASE WHEN condition = 'cur_smoker_d4' AND age_group_10_yr = '10-19' THEN 1 END) cur_smoker_gte10_lte19_d4,
count(CASE WHEN condition = 'cur_smoker_d4' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) cur_smoker_gte20_d4,
count(CASE WHEN condition = 'meas_smoking_d4' AND age_group_10_yr = '10-19' THEN 1 END) meas_smoking_gte10_lte19_d4,
count(CASE WHEN condition = 'meas_smoking_d4' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) meas_smoking_gte20_d4,
count(CASE WHEN condition = 'denom_encounter_5' THEN 1 END) denom_encounter_5,
count(CASE WHEN condition = 'hyper_d5' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) hyper_gte20_d5,
count(CASE WHEN condition = 'obese_d5' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) obese_gte20_d5,
count(CASE WHEN condition = 'asthma_d5' AND age_group_10_yr = '0-9' THEN 1 END) asthma_lte9_d5,
count(CASE WHEN condition = 'asthma_d5' AND age_group_10_yr = '10-19' THEN 1 END) asthma_gte10_lte19_d5,
count(CASE WHEN condition = 'asthma_d5' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) asthma_gte20_d5,
count(CASE WHEN condition = 'cur_smoker_d5' AND age_group_10_yr = '10-19' THEN 1 END) cur_smoker_gte10_lte19_d5,
count(CASE WHEN condition = 'cur_smoker_d5' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) cur_smoker_gte20_d5,
count(CASE WHEN condition = 'meas_smoking_d5' AND age_group_10_yr = '10-19' THEN 1 END) meas_smoking_gte10_lte19_d5,
count(CASE WHEN condition = 'meas_smoking_d5' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) meas_smoking_gte20_d5,
count(CASE WHEN condition = 'denom_encounter_6' THEN 1 END) denom_encounter_6,
count(CASE WHEN condition = 'hyper_d6' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) hyper_gte20_d6,
count(CASE WHEN condition = 'obese_d6' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) obese_gte20_d6,
count(CASE WHEN condition = 'asthma_d6' AND age_group_10_yr = '0-9' THEN 1 END) asthma_lte9_d6,
count(CASE WHEN condition = 'asthma_d6' AND age_group_10_yr = '10-19' THEN 1 END) asthma_gte10_lte19_d6,
count(CASE WHEN condition = 'asthma_d6' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) asthma_gte20_d6,
count(CASE WHEN condition = 'cur_smoker_d6' AND age_group_10_yr = '10-19' THEN 1 END) cur_smoker_gte10_lte19_d6,
count(CASE WHEN condition = 'cur_smoker_d6' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) cur_smoker_gte20_d6,
count(CASE WHEN condition = 'meas_smoking_d6' AND age_group_10_yr = '10-19' THEN 1 END) meas_smoking_gte10_lte19_d6,
count(CASE WHEN condition = 'meas_smoking_d6' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) meas_smoking_gte20_d6,
count(CASE WHEN condition = 'denom_encounter_7' THEN 1 END) denom_encounter_7,
count(CASE WHEN condition = 'hyper_d7' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) hyper_gte20_d7,
count(CASE WHEN condition = 'obese_d7' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) obese_gte20_d7,
count(CASE WHEN condition = 'asthma_d7' AND age_group_10_yr = '0-9' THEN 1 END) asthma_lte9_d7,
count(CASE WHEN condition = 'asthma_d7' AND age_group_10_yr = '10-19' THEN 1 END) asthma_gte10_lte19_d7,
count(CASE WHEN condition = 'asthma_d7' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) asthma_gte20_d7,
count(CASE WHEN condition = 'cur_smoker_d7' AND age_group_10_yr = '10-19' THEN 1 END) cur_smoker_gte10_lte19_d7,
count(CASE WHEN condition = 'cur_smoker_d7' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) cur_smoker_gte20_d7,
count(CASE WHEN condition = 'meas_smoking_d7' AND age_group_10_yr = '10-19' THEN 1 END) meas_smoking_gte10_lte19_d7,
count(CASE WHEN condition = 'meas_smoking_d7' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) meas_smoking_gte20_d7,
count(CASE WHEN condition = 'denom_encounter_8' THEN 1 END) denom_encounter_8,
count(CASE WHEN condition = 'hyper_d8' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) hyper_gte20_d8,
count(CASE WHEN condition = 'obese_d8' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) obese_gte20_d8,
count(CASE WHEN condition = 'asthma_d8' AND age_group_10_yr = '0-9' THEN 1 END) asthma_lte9_d8,
count(CASE WHEN condition = 'asthma_d8' AND age_group_10_yr = '10-19' THEN 1 END) asthma_gte10_lte19_d8,
count(CASE WHEN condition = 'asthma_d8' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) asthma_gte20_d8,
count(CASE WHEN condition = 'cur_smoker_d8' AND age_group_10_yr = '10-19' THEN 1 END) cur_smoker_gte10_lte19_d8,
count(CASE WHEN condition = 'cur_smoker_d8' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) cur_smoker_gte20_d8,
count(CASE WHEN condition = 'meas_smoking_d8' AND age_group_10_yr = '10-19' THEN 1 END) meas_smoking_gte10_lte19_d8,
count(CASE WHEN condition = 'meas_smoking_d8' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) meas_smoking_gte20_d8,
count(CASE WHEN condition = 'denom_encounter_9' THEN 1 END) denom_encounter_9,
count(CASE WHEN condition = 'hyper_d9' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) hyper_gte20_d9,
count(CASE WHEN condition = 'obese_d9' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) obese_gte20_d9,
count(CASE WHEN condition = 'asthma_d9' AND age_group_10_yr = '0-9' THEN 1 END) asthma_lte9_d9,
count(CASE WHEN condition = 'asthma_d9' AND age_group_10_yr = '10-19' THEN 1 END) asthma_gte10_lte19_d9,
count(CASE WHEN condition = 'asthma_d9' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) asthma_gte20_d9,
count(CASE WHEN condition = 'cur_smoker_d9' AND age_group_10_yr = '10-19' THEN 1 END) cur_smoker_gte10_lte19_d9,
count(CASE WHEN condition = 'cur_smoker_d9' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) cur_smoker_gte20_d9,
count(CASE WHEN condition = 'meas_smoking_d9' AND age_group_10_yr = '10-19' THEN 1 END) meas_smoking_gte10_lte19_d9,
count(CASE WHEN condition = 'meas_smoking_d9' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) meas_smoking_gte20_d9,
count(CASE WHEN condition = 'denom_encounter_10' THEN 1 END) denom_encounter_10,
count(CASE WHEN condition = 'hyper_d10' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) hyper_gte20_d10,
count(CASE WHEN condition = 'obese_d10' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) obese_gte20_d10,
count(CASE WHEN condition = 'asthma_d10' AND age_group_10_yr = '0-9' THEN 1 END) asthma_lte9_d10,
count(CASE WHEN condition = 'asthma_d10' AND age_group_10_yr = '10-19' THEN 1 END) asthma_gte10_lte19_d10,
count(CASE WHEN condition = 'asthma_d10' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) asthma_gte20_d10,
count(CASE WHEN condition = 'cur_smoker_d10' AND age_group_10_yr = '10-19' THEN 1 END) cur_smoker_gte10_lte19_d10,
count(CASE WHEN condition = 'cur_smoker_d10' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) cur_smoker_gte20_d10,
count(CASE WHEN condition = 'meas_smoking_d10' AND age_group_10_yr = '10-19' THEN 1 END) meas_smoking_gte10_lte19_d10,
count(CASE WHEN condition = 'meas_smoking_d10' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' THEN 1 END) meas_smoking_gte20_d10
FROM 
esp_mdphnet.diop_all_conditions_patient
GROUP BY age_group_10_yr,race,sex
order by sex, race, age_group_10_yr;

DROP TABLE IF EXISTS esp_mdphnet.diop_denom_1;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_2;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_3;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_4;
DROP TABLE IF EXISTS esp_mdphnet.diop_amb_lifetime;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_5;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_6;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_7;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_8;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_9;
DROP TABLE IF EXISTS esp_mdphnet.diop_denom_10;
DROP TABLE IF EXISTS esp_mdphnet.diop_bmi_all_entries;
DROP TABLE IF EXISTS esp_mdphnet.diop_max_bmi_date;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_all;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_all_events;
DROP TABLE IF EXISTS esp_mdphnet.diop_max_hyper_date;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_all;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_all_events;
DROP TABLE IF EXISTS esp_mdphnet.diop_max_asthma_date;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_all;
DROP TABLE IF EXISTS esp_mdphnet.diop_max_smoking_date;
DROP TABLE IF EXISTS esp_mdphnet.diop_smoking_dateofint;
DROP TABLE IF EXISTS esp_mdphnet.diop_smoking_current;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d1;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d2;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d3;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d4;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d5;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d6;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d7;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d8;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d9;
DROP TABLE IF EXISTS esp_mdphnet.diop_obese_d10;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d1;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d2;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d3;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d4;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d5;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d6;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d7;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d8;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d9;
DROP TABLE IF EXISTS esp_mdphnet.diop_hyper_d10;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d1;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d2;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d3;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d4;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d5;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d6;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d7;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d8;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d9;
DROP TABLE IF EXISTS esp_mdphnet.diop_asthma_d10;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d1;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d2;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d3;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d4;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d5;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d6;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d7;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d8;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d9;
DROP TABLE IF EXISTS esp_mdphnet.diop_cur_smoker_d10;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d1;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d2;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d3;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d4;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d5;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d6;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d7;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d8;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d9;
DROP TABLE IF EXISTS esp_mdphnet.diop_meas_smoking_d10;
DROP TABLE IF EXISTS esp_mdphnet.diop_all_conditions;
DROP TABLE IF EXISTS esp_mdphnet.diop_all_conditions_patient;












