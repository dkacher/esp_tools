﻿--
-- Script body 
--

-- Step 1: Access - esp_mdphnet.esp_demographic
CREATE VIEW esp_mdphnet.smk_diab_a_100036_s_100969 AS SELECT * FROM esp_mdphnet.esp_demographic;

-- Step 2: Access - public.emr_encounter
CREATE VIEW esp_mdphnet.smk_diab_a_100036_s_100970 AS SELECT * FROM public.emr_encounter;

-- Step 3: Access - public.emr_patient
CREATE VIEW esp_mdphnet.smk_diab_a_100036_s_100971 AS SELECT * FROM public.emr_patient;

-- Step 4: Access - public.emr_socialhistory
CREATE VIEW esp_mdphnet.smk_diab_a_100036_s_100972 AS SELECT * FROM public.emr_socialhistory;

-- Step 5: Access - public.nodis_case
CREATE VIEW esp_mdphnet.smk_diab_a_100036_s_100973 AS SELECT * FROM public.nodis_case;

-- Step 6: Access - public.hef_event
CREATE VIEW esp_mdphnet.smk_diab_a_100036_s_100974 AS SELECT * FROM public.hef_event;

-- Step 7: Access - public.esp_condition
CREATE VIEW esp_mdphnet.smk_diab_a_100036_s_101120 AS SELECT * FROM public.esp_condition;

-- Step 8: Query - Encounters in the 2 years preceding 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100975  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_12'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > ('01-01-2012'::date - interval  '2 years') and date < '01-01-2012')  AS DUMMYALIAS221 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100975_1 ON esp_mdphnet.smk_diab_a_100036_s_100975 (patient_id);

-- Step 9: Query - Encounters in the 2 years preceding 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100976  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_12'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > ('04-01-2012'::date - interval  '2 years') and date < '04-01-2012')  AS DUMMYALIAS222 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100976_1 ON esp_mdphnet.smk_diab_a_100036_s_100976 (patient_id);

-- Step 10: Query - Encounters in the 2 years preceding 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100977  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_12'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > ('07-01-2012'::date - interval  '2 years') and date < '07-01-2012')  AS DUMMYALIAS223 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100977_1 ON esp_mdphnet.smk_diab_a_100036_s_100977 (patient_id);

-- Step 11: Query - Encounters in the 2 years preceding 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100978  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_12'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > ('10-01-2012'::date - interval  '2 years') and date < '10-01-2012')  AS DUMMYALIAS224 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100978_1 ON esp_mdphnet.smk_diab_a_100036_s_100978 (patient_id);

-- Step 12: Query - Encounters in the 2 years preceding 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100979  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_13'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > ('01-01-2013'::date - interval  '2 years') and date < '01-01-2013')  AS DUMMYALIAS225 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100979_1 ON esp_mdphnet.smk_diab_a_100036_s_100979 (patient_id);

-- Step 13: Query - Encounters in the 2 years preceding 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100980  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_13'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > ('04-01-2013'::date - interval  '2 years') and date < '04-01-2013')  AS DUMMYALIAS226 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100980_1 ON esp_mdphnet.smk_diab_a_100036_s_100980 (patient_id);

-- Step 14: Query - Encounters in the 2 years preceding 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100981  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_13'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > ('07-01-2013'::date - interval  '2 years') and date < '07-01-2013')  AS DUMMYALIAS227 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100981_1 ON esp_mdphnet.smk_diab_a_100036_s_100981 (patient_id);

-- Step 15: Query - Encounters in the 2 years preceding 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100982  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_13'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > ('10-01-2013'::date - interval  '2 years') and date < '10-01-2013')  AS DUMMYALIAS228 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100982_1 ON esp_mdphnet.smk_diab_a_100036_s_100982 (patient_id);

-- Step 16: Query - Encounters in the 2 years preceding 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100983  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_14'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > ('01-01-2014'::date - interval  '2 years') and date < '01-01-2014')  AS DUMMYALIAS229 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100983_1 ON esp_mdphnet.smk_diab_a_100036_s_100983 (patient_id);

-- Step 17: Query - Encounters in the 2 years preceding 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100984  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_14'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > ('04-01-2014'::date - interval  '2 years') and date < '04-01-2014')  AS DUMMYALIAS230 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100984_1 ON esp_mdphnet.smk_diab_a_100036_s_100984 (patient_id);

-- Step 18: Query - Encounters in the 2 years preceding 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100985  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_14'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > '07-01-2014'::date - interval '2 years' and date < '07-01-2014')  AS DUMMYALIAS231 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100985_1 ON esp_mdphnet.smk_diab_a_100036_s_100985 (patient_id);

-- Step 19: Query - Encounters in the 2 years preceding 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100986  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_14'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > '10-01-2014'::date - interval '2 years' and date < '10-01-2014')  AS DUMMYALIAS232 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100986_1 ON esp_mdphnet.smk_diab_a_100036_s_100986 (patient_id);

-- Step 20: Query - Encounters in the 2 years preceding 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100987  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_15'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > '01-01-2015'::date - interval '2 years' and date < '01-01-2015')  AS DUMMYALIAS233 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100987_1 ON esp_mdphnet.smk_diab_a_100036_s_100987 (patient_id);

-- Step 21: Query - Encounters in the 2 years preceding 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100988  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_15'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > '04-01-2015'::date - interval '2 years' and date < '04-01-2015')  AS DUMMYALIAS234 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100988_1 ON esp_mdphnet.smk_diab_a_100036_s_100988 (patient_id);

-- Step 22: Query - Encounters in the 2 years preceding 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100989  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_15'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > '07-01-2015'::date - interval '2 years' and date < '07-01-2015')  AS DUMMYALIAS235 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100989_1 ON esp_mdphnet.smk_diab_a_100036_s_100989 (patient_id);

-- Step 23: Query - Encounters in the 2 years preceding 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100990  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_15'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > '10-01-2015'::date - interval '2 years' and date < '10-01-2015')  AS DUMMYALIAS236 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100990_1 ON esp_mdphnet.smk_diab_a_100036_s_100990 (patient_id);

-- Step 24: Query - Encounters in the 2 years preceding 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100991  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_16'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > '01-01-2016'::date - interval '2 years' and date < '01-01-2016')  AS DUMMYALIAS237 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100991_1 ON esp_mdphnet.smk_diab_a_100036_s_100991 (patient_id);

-- Step 25: Query - Encounters in the 2 years preceding 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_100992  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_16'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > '04-01-2016'::date - interval '2 years' and date < '04-01-2016')  AS DUMMYALIAS238 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100992_1 ON esp_mdphnet.smk_diab_a_100036_s_100992 (patient_id);

-- Query - Encounters in the 2 years preceding 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_enc_072016  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_16'::text quarter_of_interest FROM  esp_mdphnet.smk_diab_a_100036_s_100970 T1   WHERE date > '07-01-2016'::date - interval '2 years' and date < '07-01-2016')  AS DUMMYALIASt238 ;
-- Indexes
CREATE INDEX smk_diab_enc_072016_1 ON esp_mdphnet.smk_diab_enc_072016 (patient_id);



-- Step 145: Query - BMI Conditions - All condition entries during the time span of the report
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101121  WITHOUT OIDS  AS 
SELECT T2.id patient_id,T1.condition,(T1.date + '1960-01-01'::date) date 
FROM esp_mdphnet.smk_diab_a_100036_s_101120 T1 
INNER JOIN esp_mdphnet.smk_diab_a_100036_s_100971 T2 ON ((T1.patid = T2.natural_key))  
WHERE (date + '1960-01-01'::date) < '07-01-2016' 
and condition in ( 'BMI <25',  'BMI >= 30',  'BMI >=25 and <30', 'No Measured BMI');

-- Step 147: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101132  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100975 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2012'
GROUP BY T1.patient_id;

-- Step 148: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101133  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100976 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2012'
GROUP BY T1.patient_id;

-- Step 149: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101131  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100977 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2012'
GROUP BY T1.patient_id;


-- Step 150: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101130  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100978 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2012'
GROUP BY T1.patient_id;

-- Step 151: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101129  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100979 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2013'
GROUP BY T1.patient_id;

-- Step 152: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101136  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100980 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2013'
GROUP BY T1.patient_id;

-- Step 153: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101135  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100981 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2013'
GROUP BY T1.patient_id;

-- Step 154: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101134  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100982 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2013'
GROUP BY T1.patient_id;

-- Step 155: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101140  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100983 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2014'
GROUP BY T1.patient_id;

-- Step 156: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101139  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100984 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2014'
GROUP BY T1.patient_id;

-- Step 157: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101138  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100985 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2014'
GROUP BY T1.patient_id;

-- Step 158: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101137  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100986 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2014'
GROUP BY T1.patient_id;

-- Step 159: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101144  WITHOUT OIDS  AS
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100987 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2015'
GROUP BY T1.patient_id;

-- Step 160: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101143  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100988 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2015'
GROUP BY T1.patient_id;

-- Step 161: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101142  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100989 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2015'
GROUP BY T1.patient_id;


-- Step 162: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101141  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100990 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2015'
GROUP BY T1.patient_id;

-- Step 163: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101145  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_16'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100991 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2016'
GROUP BY T1.patient_id;


-- Step 164: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101126  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_16'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_a_100036_s_100992 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2016'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_bmi_072016  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_16'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_a_100036_s_101121 T1, 
esp_mdphnet.smk_diab_enc_072016 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2016'
GROUP BY T1.patient_id;


-- Step 165: Union - BMI - Union together all the patients and dates (2012-2013)
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101146  WITHOUT OIDS  AS 
(SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101132) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101133) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101131) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101130) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101129) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101136) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101135) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101134);

-- Step 166: Union - BMI - Union together all of the patients and dates (2014-2016)
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101147  WITHOUT OIDS  AS 
(SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101140) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101139) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101138) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101137) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101144) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101143) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101142) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101141) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101145) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101126)
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_072016);

-- Step 167: Union - BMI - Union together all the data (all years)
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101149  WITHOUT OIDS  AS 
(SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101146) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101147);

select count(distinct(patient_id)) from esp_mdphnet.smk_diab_enc_072016
--213442

select count(distinct(patient_id)) from  esp_mdphnet.smk_diab_a_100036_s_101149 where quarter_of_interest = 'jul_16';
--171538
--171655

drop table kre_temp_bmi
create table kre_temp_bmi as select * from esp_mdphnet.smk_diab_a_100036_s_101149 where quarter_of_interest = 'jul_16';


select * from kre_temp_bmi limit 10