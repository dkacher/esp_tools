﻿-- Step 177: Union - DENOMINATOR - Bring together all encounters (2012-2013)
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101112  WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100975) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100976) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100977) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100978) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100979) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100980) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100981) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100982);

-- Step 178: Union - DENOMINATOR - Bring together all encounters (2014-2016)
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101113  WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100983) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100984) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100985) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100986) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100987) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100988) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100989) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100990) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100991) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_100992)
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072016);

-- Step 179: Union - DENOMINATOR - Bring Together All Data (all years)
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101114  WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101112) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_a_100036_s_101113);

-- Step 180: Query - DENOMINATOR - Determine 10 year age group & zip
CREATE  TABLE esp_mdphnet.smk_diab_a_100036_s_101115  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T1.quarter_of_interest,T2.natural_key,
CASE   when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
else '100+' 
end age_group_10_yr,
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_a_100036_s_101114 T1 
INNER JOIN esp_mdphnet.smk_diab_a_100036_s_100971 T2 
ON ((T1.patient_id = T2.id)) )  AS DUMMYALIAS299 ;



select count(distinct(patient_id)) from esp_mdphnet.smk_diab_a_100036_s_101115 where quarter_of_interest = 'jul_16'
and age_group_10_yr != '0-9' and age_group_10_yr != '10-19'

drop table kre_temp_bmi_enc
create table kre_temp_bmi_enc as select * from esp_mdphnet.smk_diab_a_100036_s_101115 where quarter_of_interest = 'jul_16'
and age_group_10_yr != '0-9' and age_group_10_yr != '10-19'

select patient_id from kre_temp_bmi_enc 
except select patient_id from kre_temp_bmi

select * from kre_temp_bmi where patient_id = 93858
select * from kre_temp_bmi_enc where patient_id = 93858

select natural_key, date_of_birth from emr_patient where id = 5289147

select patid, date, date + '1960-01-01'::date, condition from esp_condition where patid = 'Z1285234'

select date, bmi from  emr_encounter where patient_id = 1710710
order by date desc

select * from esp_mdphnet.smk_diab_a_100036_s_101115 where patient_id = 1536486



