--PWTF HISTORICAL REPORT

set client_min_messages to error;
--
-- Script setup section 
--

DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_union1 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_union2 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_dateofint CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_current CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_cases_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_union1 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_union2 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_prev_diabetes_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_union1 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_union2 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_med_dx_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_union1 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_union2 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_all_entries CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101159 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_union1 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_union2 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_dateofint CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101156 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101157 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101158 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_denom_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101154 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_union1 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_union2 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_all_events CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_dateofint CASCADE; 
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_prev_diabetes CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_act_diabetes CASCADE; 
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_denom CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_asthma CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_bmi CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_bmi_denom CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_hyper CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_diab_events CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_unk_bmi_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_status_rec_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_status_rec_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_recorded_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking_status_rec CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.current_smokers_on_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_still_seen_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking_still_seen CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_cessation_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking_cessation CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_union_full_w_enc CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_dateofint CASCADE; 
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_systolic CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_union_full_w_enc CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_dateofint CASCADE; 
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_diastolic CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_dateofint CASCADE; 
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_controlled_bp CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_union_full_w_enc CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_dateofint CASCADE; 
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_measured_bp CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_rx_events CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102012 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102013 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102014 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102015 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102016 CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_union_full CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_union_full_w_enc CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_dateofint CASCADE; 
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_agezip CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_pat_details CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_hyper_treated CASCADE;


DROP TABLE IF EXISTS esp_mdphnet.smk_diab_all_full_strat_output CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_comm_output CASCADE;


--
-- Script body 
--

-- Step 8: Query - Encounters in the 2 years preceding 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_enc_012012 WITHOUT OIDS AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_12'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '8', '9', '11', '15', '19', '20', '29')   
AND date > ('01-01-2012'::date - interval  '2 years') and date < '01-01-2012')  AS DUMMYALIAS221 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100975_1 ON esp_mdphnet.smk_diab_enc_012012 (patient_id);

-- Step 9: Query - Encounters in the 2 years preceding 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_enc_042012 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_12'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '9', '15', '19', '20', '29')          
AND date > ('04-01-2012'::date - interval  '2 years') and date < '04-01-2012')  AS DUMMYALIAS222 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100976_1 ON esp_mdphnet.smk_diab_enc_042012 (patient_id);

-- Step 10: Query - Encounters in the 2 years preceding 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_enc_072012 WITHOUT OIDS  AS
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_12'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '9', '15', '19', '20', '29')         
AND date > ('07-01-2012'::date - interval  '2 years') and date < '07-01-2012')  AS DUMMYALIAS223 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100977_1 ON esp_mdphnet.smk_diab_enc_072012 (patient_id);

-- Step 11: Query - Encounters in the 2 years preceding 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_enc_102012 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_12'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '15', '19', '20', '29')         
AND date > ('10-01-2012'::date - interval  '2 years') and date < '10-01-2012')  AS DUMMYALIAS224 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100978_1 ON esp_mdphnet.smk_diab_enc_102012 (patient_id);

-- Step 12: Query - Encounters in the 2 years preceding 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_enc_012013 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_13'::text quarter_of_interest 
FROM  public.emr_encounter T1,
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '15', '19', '20', '29')       
AND date > ('01-01-2013'::date - interval  '2 years') and date < '01-01-2013')  AS DUMMYALIAS225 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100979_1 ON esp_mdphnet.smk_diab_enc_012013 (patient_id);

-- Step 13: Query - Encounters in the 2 years preceding 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_enc_042013 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_13'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '15', '19', '20', '29')         
AND date > ('04-01-2013'::date - interval  '2 years') and date < '04-01-2013')  AS DUMMYALIAS226 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100980_1 ON esp_mdphnet.smk_diab_enc_042013 (patient_id);

-- Step 14: Query - Encounters in the 2 years preceding 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_enc_072013 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_13'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '15', '19', '20', '29')     
AND date > ('07-01-2013'::date - interval  '2 years') and date < '07-01-2013')  AS DUMMYALIAS227 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100981_1 ON esp_mdphnet.smk_diab_enc_072013 (patient_id);

-- Step 15: Query - Encounters in the 2 years preceding 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_enc_102013 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_13'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '15', '19', '20', '29')      
AND date > ('10-01-2013'::date - interval  '2 years') and date < '10-01-2013')  AS DUMMYALIAS228 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100982_1 ON esp_mdphnet.smk_diab_enc_102013 (patient_id);

-- Step 16: Query - Encounters in the 2 years preceding 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_enc_012014 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_14'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '15', '19', '20')     
AND date > ('01-01-2014'::date - interval  '2 years') and date < '01-01-2014')  AS DUMMYALIAS229 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100983_1 ON esp_mdphnet.smk_diab_enc_012014 (patient_id);

-- Step 17: Query - Encounters in the 2 years preceding 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_enc_042014 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_14'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '15', '19', '20')      
AND date > ('04-01-2014'::date - interval  '2 years') and date < '04-01-2014')  AS DUMMYALIAS230 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100984_1 ON esp_mdphnet.smk_diab_enc_042014 (patient_id);

-- Step 18: Query - Encounters in the 2 years preceding 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_enc_072014 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_14'::text quarter_of_interest 
FROM  public.emr_encounter T1,
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '20')   
AND date > '07-01-2014'::date - interval '2 years' and date < '07-01-2014')  AS DUMMYALIAS231 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100985_1 ON esp_mdphnet.smk_diab_enc_072014 (patient_id);

-- Step 19: Query - Encounters in the 2 years preceding 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_enc_102014 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_14'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1')   
AND date > '10-01-2014'::date - interval '2 years' and date < '10-01-2014')  AS DUMMYALIAS232 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100986_1 ON esp_mdphnet.smk_diab_enc_102014 (patient_id);

-- Step 20: Query - Encounters in the 2 years preceding 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_enc_012015  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_15'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '7')
AND date > '01-01-2015'::date - interval '2 years' and date < '01-01-2015')  AS DUMMYALIAS233 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100987_1 ON esp_mdphnet.smk_diab_enc_012015 (patient_id);

-- Step 21: Query - Encounters in the 2 years preceding 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_enc_042015  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_15'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '7')  
AND date > '04-01-2015'::date - interval '2 years' and date < '04-01-2015')  AS DUMMYALIAS234 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100988_1 ON esp_mdphnet.smk_diab_enc_042015 (patient_id);

-- Step 22: Query - Encounters in the 2 years preceding 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_enc_072015 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_15'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '7')  
AND date > '07-01-2015'::date - interval '2 years' and date < '07-01-2015')  AS DUMMYALIAS235 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100989_1 ON esp_mdphnet.smk_diab_enc_072015 (patient_id);

-- Step 23: Query - Encounters in the 2 years preceding 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_enc_102015  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_15'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1','7')   
AND date > '10-01-2015'::date - interval '2 years' and date < '10-01-2015')  AS DUMMYALIAS236 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100990_1 ON esp_mdphnet.smk_diab_enc_102015 (patient_id);

-- Step 24: Query - Encounters in the 2 years preceding 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_enc_012016  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,'jan_16'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '6', '7')
AND date > '01-01-2016'::date - interval '2 years' and date < '01-01-2016')  AS DUMMYALIAS237 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100991_1 ON esp_mdphnet.smk_diab_enc_012016 (patient_id);

-- Step 25: Query - Encounters in the 2 years preceding 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_enc_042016 WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'apr_16'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '6', '7') 
AND date > '04-01-2016'::date - interval '2 years' and date < '04-01-2016')  AS DUMMYALIAS238 ;
-- Indexes
CREATE INDEX smk_diab_a_100036_s_100992_1 ON esp_mdphnet.smk_diab_enc_042016 (patient_id);

-- Query - Encounters in the 2 years preceding 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_enc_072016  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'jul_16'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '6', '7')
AND date > '07-01-2016'::date - interval '2 years' and date < '07-01-2016')  AS DUMMYALIASt238 ;
-- Indexes
CREATE INDEX smk_diab_enc_072016_1 ON esp_mdphnet.smk_diab_enc_072016 (patient_id);

-- Query - Encounters in the 2 years preceding 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_enc_102016  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,'oct_16'::text quarter_of_interest 
FROM  public.emr_encounter T1,   
emr_patient T2
WHERE T1.patient_id = T2.id 
AND center_id not in ('1', '6', '7')
AND date > '10-01-2016'::date - interval '2 years' and date < '10-01-2016')  AS DUMMYALIASt238 ;
-- Indexes
CREATE INDEX smk_diab_enc_102016_1 ON esp_mdphnet.smk_diab_enc_102016 (patient_id);



-- Step 26: Query - SMOKING - Get the most recent smoking date prior to the index date. 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_smk_012012 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jan_12'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012012 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '01-01-2012' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS239 ;

-- Step 27: Query - SMOKING - Get the most recent smoking date prior to the index date. 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_smk_042012 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'apr_12'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042012 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '04-01-2012' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS240 ;

-- Step 28: Query - SMOKING - Get the most recent smoking date prior to the index date. 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_smk_072012 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jul_12'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072012 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '07-01-2012' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS241 ;

-- Step 29: Query - SMOKING - Get the most recent smoking date prior to the index date. 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_smk_102012 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'oct_12'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102012 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '10-01-2012' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS242 ;

-- Step 30: Query - SMOKING - Get the most recent smoking date prior to the index date. 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_smk_012013  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jan_13'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012013 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '01-01-2013' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS243 ;

-- Step 31: Query - SMOKING - Get the most recent smoking date prior to the index date. 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_smk_042013 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'apr_13'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042013 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '04-01-2013' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS244 ;

-- Step 32: Query - SMOKING - Get the most recent smoking date prior to the index date. 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_smk_072013 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jul_13'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072013 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '07-01-2013' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS245 ;

-- Step 33: Query - SMOKING - Get the most recent smoking date prior to the index date. 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_smk_102013 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'oct_13'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102013 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '10-01-2013' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS246 ;

-- Step 34: Query - SMOKING - Get the most recent smoking date prior to the index date. 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_smk_012014 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jan_14'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012014 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '01-01-2014' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS247 ;

-- Step 35: Query - SMOKING - Get the most recent smoking date prior to the index date. 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_smk_042014 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(t2.date) max_smoking_date,'apr_14'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042014 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '04-01-2014' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS248 ;

-- Step 36: Query - SMOKING - Get the most recent smoking date prior to the index date. 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_smk_072014 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jul_14'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072014 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '07-01-2014' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS249 ;

-- Step 37: Query - SMOKING - Get the most recent smoking date prior to the index date. 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_smk_102014 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(t2.date) max_smoking_date,'oct_14'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102014 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '10-01-2014' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS250 ;

-- Step 38: Query - SMOKING - Get the most recent smoking date prior to the index date. 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_012015 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jan_15'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012015 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '01-01-2015' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS251 ;

-- Step 39: Query - SMOKING - Get the most recent smoking date prior to the index date. 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_042015 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'apr_15'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042015 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '04-01-2015' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS252 ;

-- Step 40: Query - SMOKING - Get the most recent smoking date prior to the index date. 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_072015 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jul_15'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072015 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '07-01-2015' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS253 ;

-- Step 41: Query - SMOKING - Get the most recent smoking date prior to the index date. 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_102015 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'oct_15'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102015 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '10-01-2015' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS254 ;

-- Step 42: Query - SMOKING - Get the most recent smoking date prior to the index date. 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_012016 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jan_16'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012016 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '01-01-2016' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS255 ;

-- Step 43: Query - SMOKING - Get the most recent smoking date prior to the index date. 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_042016 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'apr_16'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042016 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '04-01-2016' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIAS256 ;

-- Query - SMOKING - Get the most recent smoking date prior to the index date. 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_072016  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'jul_16'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072016 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '07-01-2016' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIASt256 ;

-- Query - SMOKING - Get the most recent smoking date prior to the index date. 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_102016  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,max(T2.date) max_smoking_date,'oct_16'::text quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102016 T1 INNER JOIN public.emr_socialhistory T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '10-01-2016' AND T2.tobacco_use is not null and T2.tobacco_use != '' GROUP BY T1.patient_id)  AS DUMMYALIASt256 ;

-- Step 44: Union - SMOKING - All of the smoking dates and patients (2012-2013)
CREATE  TABLE esp_mdphnet.smk_diab_smk_union1 WITHOUT OIDS  AS 
(SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_012012) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_042012) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_072012) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_102012) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_012013) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_042013)
 UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_072013) 
 UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_102013);

-- Step 45: Union - SMOKING - All of the smoking dates and patients (2014-2016)
CREATE  TABLE esp_mdphnet.smk_diab_smk_union2 WITHOUT OIDS  AS 
(SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_012014) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_042014) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_072014) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_102014) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_012015) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_042015) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_072015) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_102015) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_012016) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_042016) 
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_072016)
UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_102016);

-- Step 46: Union - Union All Smoking Data (all years) 
CREATE  TABLE esp_mdphnet.smk_diab_smk_union_full WITHOUT OIDS  AS (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_union1) UNION (SELECT patient_id,max_smoking_date,quarter_of_interest FROM esp_mdphnet.smk_diab_smk_union2);

-- Step 47: Query - SMOKING - Get tobacco_use for the date of interest
CREATE  TABLE esp_mdphnet.smk_diab_smk_dateofint WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.tobacco_use,T1.quarter_of_interest,
case when tobacco_use ILIKE ANY(ARRAY['%adults%', '%aunt%', '%both%', '%brother%', '%cousin%', '%dad%', '%everyone%', '%exposure%','%family smokes%', '%father%', '%fob%', '%foc%', '%gm%', '%gf%',  '%grandaughter%','%grandfather%', '%grandma%', '%grandmom%', '%grandmother%', '%grandparent%', '%husband%', '%many smokers%', '%members%', '%mgf%', '% mo %', '%mob%', '%moc%',  '%mgm%','%mggm%', '%mom%', '%mothr%', '%mothe%', '%parent%', '%paretns%', '%passive%', '%sister%', '% uncle %', '%uncle.%'])  
	AND tobacco_use NOT ILIKE ALL(ARRAY['have cut back%']) 
then 'Passive'      
when (
	tobacco_use in ('N','Never','no','Denies.','Nonsmoker.','never','No. .','none','0','negative','Nonsmoker','None.','Denies','Non-smoker','None','Non smoker','Never smoked.','non smoker','Non-smoker','Non-smoker.',         'Non smoker.','nonsmoker')
	or tobacco_use ILIKE ANY(ARRAY['%abstain%', 'No%', '%doesn%smoke%', 'negative%', '%never%'])
	)
	AND tobacco_use NOT ILIKE ALL(ARRAY['%quit%', '%plans%', '%smoke outside%', '%not clear%', '%daily%', '%not%', 'yes%', '%cut back%', 'smoked%', '%day%'])
then 'Never'  
when (
	tobacco_use in ('R','Prev','Previous','quit','Quit smoking','Former smoker.','Former smoker', 'Former','prev.','Quit.','quit smoking', 'X', '0 currently', 'Complete cessation', 'No. not at new apartment. .')
	or tobacco_use ~ '^[0123456789]{1,2}/[[0123456789]{1,2}/[0123456789]{2,4}'           
	or tobacco_use ILIKE ANY(ARRAY['%quit%', '%previous%', '%used to%', '%former%', 'jan%', 'june%', '%long time ago', '%not%smoking%', '%patch%', '%smoked%', '%pt quit%', '%quit%chantix%'])
	)           
	AND tobacco_use NOT ILIKE ALL(ARRAY['%less%', '%trying%', '%plans%', '%advised%', '%complications%', 'current%', '%quitting%', '%cig%patch%',  '%needs to%', '%not in house%', '%on/off%',            '%would like to%', '%quit by%', '%pick a date%', '%to quit%', '%to have more money%', 'up to%'])           
then 'Former'  
when (
	tobacco_use in ('Y','Current','X','Yes. .','precontemplative','counseled','one', 'Very Ready','Post Partum','ready','contemplative', 'Yes. outside. .','relapse','rare', 'rarely')
	or tobacco_use ~ '^[0123456789]'        
	or tobacco_use ~ '^.[0123456789]'        
	or tobacco_use ILIKE ANY(ARRAY['%<%','%>%','%~%','about%','%addicted%','%advised%', '%a lot%', '%almost%', '%approx%', '%apprx%','%as much%', '%avoid%', '%breath%', '%cancer%', '%casual%', '%cessation%', '%chew%', '%children%',         '%cig%', '%complica%', '%contemplat%','%continue%','%continues%', '%couple%','%current%', '%cut back%','%cut down%', '%daily%', '%day%', '%depends%', '%drinking%', '%enough%', '%expensive%', '%feel%', '%few%', '%half%', '%handout%', '%hardly%', '%harm%', '%health%','%infreq%','%improve%','%interm%', '%less%', '%live%', '%more than%', '%needs to%', '%occ%','%off and on%', '%on weekends%', '%one%', '%only%', '%pack%', '%cig%patch%', '%pipe%', '%pkg%','%positive%', '%quit%', '%rare%', '%ready%', '%recreation%', '%roll%', '%since%', '%smoke outside%',                       '%seldom%', '%smoke%', '%smoking%', '%social%', '%some%', '%start%', '%still%','%stress%', '%to be%','%tobacco%','%trying%', '%up to%', '%under%', '%var%', '%wants to%', '%while%', '%yes%']) 
	)
	AND tobacco_use not ILIKE ALL(ARRAY['~', '%refus%','%unknown%', '%not cigarettes%', '%pt is no%', '%w/o%'])
then 'Current'                      
else 'Not available' end  smoking 
FROM esp_mdphnet.smk_diab_smk_union_full T1 
INNER JOIN public.emr_socialhistory T2 
ON ((T1.patient_id = T2.patient_id) 
AND (T1.max_smoking_date = T2.date))
WHERE T2.tobacco_use is not null and T2.tobacco_use != '' )  AS DUMMYALIAS257 ;

-- Step 48: Query - SMOKING - Only want CURRENT (yes) smokers.
CREATE  TABLE esp_mdphnet.smk_diab_smk_current WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T1.tobacco_use,T1.quarter_of_interest FROM  esp_mdphnet.smk_diab_smk_dateofint T1   WHERE  smoking =  'Current' )  AS DUMMYALIAS258 ;

-- Step 49: Query - SMOKING - Determine 10 yr age group and zip
CREATE  TABLE esp_mdphnet.smk_diab_smk_agezip WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.tobacco_use,T1.quarter_of_interest,
CASE   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' end age_group_10_yr,
T2.natural_key,
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_smk_current T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) ;

-- Step 50: Query - SMOKING - Get sex and race from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_smk_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.tobacco_use,T1.quarter_of_interest,T1.age_group_10_yr, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity,
T2.sex 
FROM esp_mdphnet.smk_diab_smk_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 ON ((T1.natural_key = T2.patid)) ;

-- Step 51: Query - SMOKING - Smoking Counts By Quarter Stratified By Age Group, Race, and Sex
CREATE  TABLE esp_mdphnet.smk_diab_smoking_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  curr_smoke_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  curr_smoke_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  curr_smoke_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  curr_smoke_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  curr_smoke_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  curr_smoke_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  curr_smoke_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  curr_smoke_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  curr_smoke_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  curr_smoke_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  curr_smoke_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  curr_smoke_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  curr_smoke_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  curr_smoke_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  curr_smoke_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  curr_smoke_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  curr_smoke_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  curr_smoke_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  curr_smoke_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  curr_smoke_oct_16 
FROM  esp_mdphnet.smk_diab_smk_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex;

-- SMOKING - Smoking Counts By Quarter Stratified By Age Group, Race, and Sex & Zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_smoking  WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,T1.pwtf_comm,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  curr_smoke_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  curr_smoke_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  curr_smoke_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  curr_smoke_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  curr_smoke_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  curr_smoke_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  curr_smoke_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  curr_smoke_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  curr_smoke_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  curr_smoke_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  curr_smoke_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  curr_smoke_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  curr_smoke_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  curr_smoke_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  curr_smoke_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  curr_smoke_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  curr_smoke_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  curr_smoke_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  curr_smoke_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  curr_smoke_oct_16    
FROM  esp_mdphnet.smk_diab_smk_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm;

--SMOKING STATUS RECORDED - Determine 10 yr age group and zip
CREATE  TABLE esp_mdphnet.smk_diab_smk_status_rec_agezip WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,
CASE   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' end age_group_10_yr,
T2.natural_key,
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_smk_union_full T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) ;

--SMOKING STATUS RECORDED - Get sex and race from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_smk_status_rec_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,T1.age_group_10_yr, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity,
T2.sex 
FROM esp_mdphnet.smk_diab_smk_status_rec_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 ON ((T1.natural_key = T2.patid)) ;

--SMOKING STATUS RECORDED - Smoking Counts (any status) By Quarter Stratified By Age Group, Race, and Sex
CREATE  TABLE esp_mdphnet.smk_diab_smoking_recorded_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  smoke_status_rec_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  smoke_status_rec_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  smoke_status_rec_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  smoke_status_rec_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  smoke_status_rec_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  smoke_status_rec_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  smoke_status_rec_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  smoke_status_rec_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  smoke_status_rec_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  smoke_status_rec_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  smoke_status_rec_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  smoke_status_rec_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  smoke_status_rec_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  smoke_status_rec_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  smoke_status_rec_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  smoke_status_rec_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  smoke_status_rec_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  smoke_status_rec_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  smoke_status_rec_jul_16, 
count(case when  quarter_of_interest = 'oct_16' then 1 end)  smoke_status_rec_oct_16 
FROM  esp_mdphnet.smk_diab_smk_status_rec_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex;

-- SMOKING STATUS RECORDED - Smoking Counts (any status) By Quarter Stratified By Age Group, Race, and Sex & Zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_smoking_status_rec  WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,T1.pwtf_comm,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  smoke_status_rec_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  smoke_status_rec_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  smoke_status_rec_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  smoke_status_rec_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  smoke_status_rec_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  smoke_status_rec_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  smoke_status_rec_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  smoke_status_rec_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  smoke_status_rec_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  smoke_status_rec_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  smoke_status_rec_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  smoke_status_rec_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  smoke_status_rec_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  smoke_status_rec_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  smoke_status_rec_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  smoke_status_rec_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  smoke_status_rec_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  smoke_status_rec_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  smoke_status_rec_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  smoke_status_rec_oct_16  
FROM  esp_mdphnet.smk_diab_smk_status_rec_pat_details T1  
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm;


-- CURRENT SMOKE STILL SEEN Base denom (should be same count as curr_smoke_jan_14)
CREATE TABLE esp_mdphnet.current_smokers_on_012014 AS
select T1.patient_id, T1.quarter_of_interest 
from esp_mdphnet.smk_diab_smk_current T1
where quarter_of_interest = 'jan_14';


-- CURRENT SMOKER STILL SEEN - curr_smoke_jan_14 patients still seen 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_042014 WITHOUT OIDS  AS 
SELECT T1.patient_id, 'apr_14'::text quarter_of_interest 
FROM esp_mdphnet.current_smokers_on_012014 T1 INNER JOIN esp_mdphnet.smk_diab_enc_042014 T2 ON ((T1.patient_id = T2.patient_id));  

-- CURRENT SMOKER STILL SEEN - curr_smoke_jan_14 patients still seen 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_072014 WITHOUT OIDS  AS 
SELECT T1.patient_id, 'jul_14'::text quarter_of_interest 
FROM esp_mdphnet.current_smokers_on_012014 T1 INNER JOIN esp_mdphnet.smk_diab_enc_072014 T2 ON ((T1.patient_id = T2.patient_id)); 

-- CURRENT SMOKER STILL SEEN - curr_smoke_jan_14 patients still seen 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_102014 WITHOUT OIDS  AS 
SELECT T1.patient_id, 'oct_14'::text quarter_of_interest 
FROM esp_mdphnet.current_smokers_on_012014 T1 INNER JOIN esp_mdphnet.smk_diab_enc_102014 T2 ON ((T1.patient_id = T2.patient_id));  

-- CURRENT SMOKER STILL SEEN - curr_smoke_jan_14 patients still seen 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_012015 WITHOUT OIDS  AS 
SELECT T1.patient_id, 'jan_15'::text quarter_of_interest 
FROM esp_mdphnet.current_smokers_on_012014 T1 INNER JOIN esp_mdphnet.smk_diab_enc_012015 T2 ON ((T1.patient_id = T2.patient_id));  

-- CURRENT SMOKER STILL SEEN - curr_smoke_jan_14 patients still seen 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_042015 WITHOUT OIDS  AS 
SELECT T1.patient_id, 'apr_15'::text quarter_of_interest 
FROM esp_mdphnet.current_smokers_on_012014 T1 INNER JOIN esp_mdphnet.smk_diab_enc_042015 T2 ON ((T1.patient_id = T2.patient_id));  

-- CURRENT SMOKER STILL SEEN - curr_smoke_jan_14 patients still seen 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_072015 WITHOUT OIDS  AS 
SELECT T1.patient_id, 'jul_15'::text quarter_of_interest 
FROM esp_mdphnet.current_smokers_on_012014 T1 INNER JOIN esp_mdphnet.smk_diab_enc_072015 T2 ON ((T1.patient_id = T2.patient_id)); 

-- CURRENT SMOKER STILL SEEN - curr_smoke_jan_14 patients still seen 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_102015 WITHOUT OIDS  AS 
SELECT T1.patient_id, 'oct_15'::text quarter_of_interest 
FROM esp_mdphnet.current_smokers_on_012014 T1 INNER JOIN esp_mdphnet.smk_diab_enc_102015 T2 ON ((T1.patient_id = T2.patient_id));  

-- CURRENT SMOKER STILL SEEN - curr_smoke_jan_14 patients still seen 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_012016 WITHOUT OIDS  AS 
SELECT T1.patient_id, 'jan_16'::text quarter_of_interest 
FROM esp_mdphnet.current_smokers_on_012014 T1 INNER JOIN esp_mdphnet.smk_diab_enc_012016 T2 ON ((T1.patient_id = T2.patient_id));  

-- CURRENT SMOKER STILL SEEN - curr_smoke_jan_14 patients still seen 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_042016 WITHOUT OIDS  AS 
SELECT T1.patient_id, 'apr_16'::text quarter_of_interest 
FROM esp_mdphnet.current_smokers_on_012014 T1 INNER JOIN esp_mdphnet.smk_diab_enc_042016 T2 ON ((T1.patient_id = T2.patient_id));  

-- CURRENT SMOKER STILL SEEN - curr_smoke_jan_14 patients still seen 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_072016 WITHOUT OIDS  AS 
SELECT T1.patient_id, 'jul_16'::text quarter_of_interest 
FROM esp_mdphnet.current_smokers_on_012014 T1 INNER JOIN esp_mdphnet.smk_diab_enc_072016 T2 ON ((T1.patient_id = T2.patient_id)); 

-- CURRENT SMOKER STILL SEEN - curr_smoke_jan_14 patients still seen 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_102016 WITHOUT OIDS  AS 
SELECT T1.patient_id, 'oct_16'::text quarter_of_interest 
FROM esp_mdphnet.current_smokers_on_012014 T1 INNER JOIN esp_mdphnet.smk_diab_enc_102016 T2 ON ((T1.patient_id = T2.patient_id));

-- CURRENT SMOKER STILL SEEN - Union together all the patients and dates (2012-2016)
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_union_full WITHOUT OIDS  AS 
(SELECT * FROM esp_mdphnet.current_smokers_on_012014) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_still_seen_042014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_still_seen_072014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_still_seen_102014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_still_seen_012015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_still_seen_042015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_still_seen_072015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_still_seen_102015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_still_seen_012016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_still_seen_042016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_still_seen_072016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_still_seen_102016);

-- CURRENT SMOKER STILL SEEN - Determine 10 yr age group and zip
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_agezip WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,
CASE   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' end age_group_10_yr,
T2.natural_key,
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_smk_still_seen_union_full T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) ;

--CURRENT SMOKER STILL SEEN - Get sex and race from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_smk_still_seen_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,T1.age_group_10_yr, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity,
T2.sex 
FROM esp_mdphnet.smk_diab_smk_still_seen_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 ON ((T1.natural_key = T2.patid)) ;

--CURRENT SMOKER STILL SEEN - Counts By Quarter Stratified By Age Group, Race, and Sex
CREATE  TABLE esp_mdphnet.smk_diab_smoking_still_seen_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  smoke_still_seen_base_denom_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  smoke_still_seen_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  smoke_still_seen_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  smoke_still_seen_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  smoke_still_seen_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  smoke_still_seen_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  smoke_still_seen_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  smoke_still_seen_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  smoke_still_seen_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  smoke_still_seen_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  smoke_still_seen_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  smoke_still_seen_oct_16 
FROM  esp_mdphnet.smk_diab_smk_still_seen_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex;

-- CURRENT SMOKER STILL SEEN - Counts By Quarter Stratified By Age Group, Race, and Sex & Zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_smoking_still_seen  WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,T1.pwtf_comm,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  smoke_still_seen_base_denom_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  smoke_still_seen_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  smoke_still_seen_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  smoke_still_seen_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  smoke_still_seen_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  smoke_still_seen_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  smoke_still_seen_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  smoke_still_seen_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  smoke_still_seen_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  smoke_still_seen_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  smoke_still_seen_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  smoke_still_seen_oct_16  
FROM  esp_mdphnet.smk_diab_smk_still_seen_pat_details T1  
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm;

-- CURRENT SMOKER CESSATION - curr_smoke_jan_14 patients still seen and now former or never 04-01-2014
CREATE TABLE esp_mdphnet.smk_diab_smk_cessation_042014 AS
SELECT T1.patient_id, T2.tobacco_use, T1.quarter_of_interest, T2.smoking
FROM esp_mdphnet.smk_diab_smk_still_seen_042014 T1
INNER JOIN esp_mdphnet.smk_diab_smk_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE smoking in ('Former', 'Never')
order by patient_id;

-- CURRENT SMOKER CESSATION - curr_smoke_jan_14 patients still seen and now former or never 07-01-2014
CREATE TABLE esp_mdphnet.smk_diab_smk_cessation_072014 AS
SELECT T1.patient_id, T2.tobacco_use, T1.quarter_of_interest, T2.smoking
FROM esp_mdphnet.smk_diab_smk_still_seen_072014 T1
INNER JOIN esp_mdphnet.smk_diab_smk_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE smoking in ('Former', 'Never')
order by patient_id;

-- CURRENT SMOKER CESSATION - curr_smoke_jan_14 patients still seen and now former or never 10-01-2014
CREATE TABLE esp_mdphnet.smk_diab_smk_cessation_102014 AS
SELECT T1.patient_id, T2.tobacco_use, T1.quarter_of_interest, T2.smoking
FROM esp_mdphnet.smk_diab_smk_still_seen_102014 T1
INNER JOIN esp_mdphnet.smk_diab_smk_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE smoking in ('Former', 'Never')
order by patient_id;

-- CURRENT SMOKER CESSATION - curr_smoke_jan_14 patients still seen and now former or never 01-01-2015
CREATE TABLE esp_mdphnet.smk_diab_smk_cessation_012015 AS
SELECT T1.patient_id, T2.tobacco_use, T1.quarter_of_interest, T2.smoking
FROM esp_mdphnet.smk_diab_smk_still_seen_012015 T1
INNER JOIN esp_mdphnet.smk_diab_smk_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE smoking in ('Former', 'Never')
order by patient_id;

-- CURRENT SMOKER CESSATION - curr_smoke_jan_14 patients still seen and now former or never 04-01-2015
CREATE TABLE esp_mdphnet.smk_diab_smk_cessation_042015 AS
SELECT T1.patient_id, T2.tobacco_use, T1.quarter_of_interest, T2.smoking
FROM esp_mdphnet.smk_diab_smk_still_seen_042015 T1
INNER JOIN esp_mdphnet.smk_diab_smk_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE smoking in ('Former', 'Never')
order by patient_id;

-- CURRENT SMOKER CESSATION - curr_smoke_jan_14 patients still seen and now former or never 07-01-2015
CREATE TABLE esp_mdphnet.smk_diab_smk_cessation_072015 AS
SELECT T1.patient_id, T2.tobacco_use, T1.quarter_of_interest, T2.smoking
FROM esp_mdphnet.smk_diab_smk_still_seen_072015 T1
INNER JOIN esp_mdphnet.smk_diab_smk_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE smoking in ('Former', 'Never')
order by patient_id;

-- CURRENT SMOKER CESSATION - curr_smoke_jan_14 patients still seen and now former or never 10-01-2015
CREATE TABLE esp_mdphnet.smk_diab_smk_cessation_102015 AS
SELECT T1.patient_id, T2.tobacco_use, T1.quarter_of_interest, T2.smoking
FROM esp_mdphnet.smk_diab_smk_still_seen_102015 T1
INNER JOIN esp_mdphnet.smk_diab_smk_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE smoking in ('Former', 'Never')
order by patient_id;

-- CURRENT SMOKER CESSATION - curr_smoke_jan_14 patients still seen and now former or never 01-01-2016
CREATE TABLE esp_mdphnet.smk_diab_smk_cessation_012016 AS
SELECT T1.patient_id, T2.tobacco_use, T1.quarter_of_interest, T2.smoking
FROM esp_mdphnet.smk_diab_smk_still_seen_012016 T1
INNER JOIN esp_mdphnet.smk_diab_smk_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE smoking in ('Former', 'Never')
order by patient_id;

-- CURRENT SMOKER CESSATION - curr_smoke_jan_14 patients still seen and now former or never 04-01-2016
CREATE TABLE esp_mdphnet.smk_diab_smk_cessation_042016 AS
SELECT T1.patient_id, T2.tobacco_use, T1.quarter_of_interest, T2.smoking
FROM esp_mdphnet.smk_diab_smk_still_seen_042016 T1
INNER JOIN esp_mdphnet.smk_diab_smk_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE smoking in ('Former', 'Never')
order by patient_id;

-- CURRENT SMOKER CESSATION - curr_smoke_jan_14 patients still seen and now former or never 07-01-2016
CREATE TABLE esp_mdphnet.smk_diab_smk_cessation_072016 AS
SELECT T1.patient_id, T2.tobacco_use, T1.quarter_of_interest, T2.smoking
FROM esp_mdphnet.smk_diab_smk_still_seen_072016 T1
INNER JOIN esp_mdphnet.smk_diab_smk_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE smoking in ('Former', 'Never')
order by patient_id;

-- CURRENT SMOKER CESSATION - curr_smoke_jan_14 patients still seen and now former or never 10-01-2016
CREATE TABLE esp_mdphnet.smk_diab_smk_cessation_102016 AS
SELECT T1.patient_id, T2.tobacco_use, T1.quarter_of_interest, T2.smoking
FROM esp_mdphnet.smk_diab_smk_still_seen_102016 T1
INNER JOIN esp_mdphnet.smk_diab_smk_dateofint T2 ON (T1.patient_id = T2.patient_id AND T1.quarter_of_interest = T2.quarter_of_interest)
WHERE smoking in ('Former', 'Never')
order by patient_id;

-- CURRENT SMOKER CESSATION - Union together all the patients and dates (2012-2016)
CREATE  TABLE esp_mdphnet.smk_diab_smk_cessation_union_full WITHOUT OIDS  AS 
(SELECT * FROM esp_mdphnet.smk_diab_smk_cessation_042014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_cessation_072014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_cessation_102014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_cessation_012015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_cessation_042015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_cessation_072015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_cessation_102015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_cessation_012016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_cessation_042016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_cessation_072016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_smk_cessation_102016);

-- CURRENT SMOKER CESSATION - Determine 10 yr age group and zip
CREATE  TABLE esp_mdphnet.smk_diab_smk_cessation_agezip WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,
CASE   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' end age_group_10_yr,
T2.natural_key,
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_smk_cessation_union_full T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) ;

--CURRENT SMOKER CESSATION - Get sex and race from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_smk_cessation_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,T1.age_group_10_yr, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity,
T2.sex 
FROM esp_mdphnet.smk_diab_smk_cessation_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 ON ((T1.natural_key = T2.patid)) ;

--CURRENT SMOKER CESSATION - Counts By Quarter Stratified By Age Group, Race, and Sex
CREATE  TABLE esp_mdphnet.smk_diab_smoking_cessation_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  smoke_cessation_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  smoke_cessation_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  smoke_cessation_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  smoke_cessation_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  smoke_cessation_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  smoke_cessation_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  smoke_cessation_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  smoke_cessation_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  smoke_cessation_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  smoke_cessation_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  smoke_cessation_oct_16 
FROM  esp_mdphnet.smk_diab_smk_cessation_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex;

-- CURRENT SMOKER CESSATION - Counts By Quarter Stratified By Age Group, Race, and Sex & Zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_smoking_cessation  WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,T1.pwtf_comm,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  smoke_cessation_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  smoke_cessation_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  smoke_cessation_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  smoke_cessation_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  smoke_cessation_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  smoke_cessation_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  smoke_cessation_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  smoke_cessation_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  smoke_cessation_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  smoke_cessation_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  smoke_cessation_oct_16  
FROM  esp_mdphnet.smk_diab_smk_cessation_pat_details T1  
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm;


-- Step 52: Query - DIABETES - All Type 1 and Type 2 Diabetes Cases 
CREATE  TABLE esp_mdphnet.smk_diab_diab_cases_all WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.condition FROM  public.nodis_case T1   WHERE  condition in ( 'diabetes:type-1',  'diabetes:type-2');

-- Step 53: Query - DIABETES - Diabetes & Encounter for index date 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_diab_012012 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'jan_12'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_012012 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '01-01-2012')  AS DUMMYALIAS259 ;

-- Step 54: Query - DIABETES - Diabetes & Encounter for index date 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_diab_042012 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'apr_12'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_042012 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '04-01-2012')  AS DUMMYALIAS260 ;

-- Step 55: Query - DIABETES - Diabetes & Encounter for index date 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_diab_072012 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'jul_12'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_072012 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '07-01-2012')  AS DUMMYALIAS261 ;

-- Step 56: Query - DIABETES - Diabetes & Encounter for index date 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_diab_102012 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'oct_12'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_102012 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '10-01-2012')  AS DUMMYALIAS262 ;

-- Step 57: Query - DIABETES - Diabetes & Encounter for index date 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_diab_012013  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'jan_13'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_012013 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '01-01-2013')  AS DUMMYALIAS263 ;

-- Step 58: Query - DIABETES - Diabetes & Encounter for index date 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_diab_042013 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'apr_13'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_042013 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '04-01-2013')  AS DUMMYALIAS264 ;

-- Step 59: Query - DIABETES - Diabetes & Encounter for index date 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_diab_072013 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'jul_13'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_072013 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '07-01-2013')  AS DUMMYALIAS265 ;

-- Step 60: Query - DIABETES - Diabetes & Encounter for index date 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_diab_102013 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'oct_13'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_102013 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '10-01-2013')  AS DUMMYALIAS266 ;

-- Step 61: Query - DIABETES - Diabetes & Encounter for index date 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_diab_012014 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'jan_14'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_012014 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '01-01-2014')  AS DUMMYALIAS267 ;

-- Step 62: Query - DIABETES - Diabetes & Encounter for index date 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_diab_042014 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'apr_14'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_042014 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE  T2.date < '04-01-2014')  AS DUMMYALIAS268 ;

-- Step 63: Query - DIABETES - Diabetes & Encounter for index date 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_diab_072014 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'jul_14'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_072014 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '07-01-2014')  AS DUMMYALIAS269 ;

-- Step 64: Query - DIABETES - Diabetes & Encounter for index date 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_diab_102014 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'oct_14'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_102014 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '10-01-2014')  AS DUMMYALIAS270 ;

-- Step 65: Query - DIABETES - Diabetes & Encounter for index date 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_diab_012015 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'jan_15'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_012015 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '01-01-2015')  AS DUMMYALIAS271 ;

-- Step 66: Query - DIABETES - Diabetes & Encounter for index date 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_diab_042015 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'apr_15'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_042015 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '04-01-2015')  AS DUMMYALIAS272 ;

-- Step 67: Query - DIABETES - Diabetes & Encounter for index date 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_diab_072015 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'jul_15'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_072015 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '07-01-2015')  AS DUMMYALIAS273 ;

-- Step 68: Query - DIABETES - Diabetes & Encounter for index date 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_diab_102015 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'oct_15'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_102015 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '10-01-2015')  AS DUMMYALIAS274 ;

-- Step 69: Query - DIABETES - Diabetes & Encounter for index date 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_diab_012016 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'jan_16'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_012016 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '01-01-2016')  AS DUMMYALIAS275 ;

-- Step 70: Query - DIABETES - Diabetes & Encounter for index date 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_diab_042016 WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'apr_16'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_042016 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '04-01-2016')  AS DUMMYALIAS276 ;

-- Query - DIABETES - Diabetes & Encounter for index date 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_diab_072016  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'jul_16'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_072016 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id))  WHERE T2.date < '07-01-2016')  AS DUMMYALIASt276 ;

-- Query - DIABETES - Diabetes & Encounter for index date 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_diab_102016  WITHOUT OIDS  AS SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.condition,'oct_16'::text quarter_of_interest, T2.date as case_date FROM esp_mdphnet.smk_diab_enc_102016 T1 INNER JOIN esp_mdphnet.smk_diab_diab_cases_all T2 ON ((T1.patient_id = T2.patient_id)) WHERE T2.date < '10-01-2016')  AS DUMMYALIASt276 ;



-- Step 71: Union - DIABETES - All Diabetes Cases & Quarter Infro (2012-2013)
CREATE  TABLE esp_mdphnet.smk_diab_diab_union1 WITHOUT OIDS  AS 
(SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_012012) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_042012) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_072012) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_102012) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_012013) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_042013) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_072013) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_102013);

-- Step 72: Union - DIABETES - All Diabetes Cases & Quarter Infro (2014-2016)
CREATE  TABLE esp_mdphnet.smk_diab_diab_union2 WITHOUT OIDS  AS 
(SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_012014) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_042014) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_072014) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_102014) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_012015) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_042015) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_072015) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_102015) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_012016) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_042016)
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_072016)
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_102016);;

-- Step 73: Union - DIABETES - Union together all data (all years)
CREATE  TABLE esp_mdphnet.smk_diab_diab_union_full WITHOUT OIDS  AS 
(SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_union1) 
UNION (SELECT patient_id,condition,quarter_of_interest FROM esp_mdphnet.smk_diab_diab_union2);

-- Step 74: Query - DIABETES - Determine 10 Year Age Group & Zip
CREATE  TABLE esp_mdphnet.smk_diab_diab_agezip WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.condition,T1.quarter_of_interest,T2.natural_key,
CASE   when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' 
end age_group_10_yr,
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_diab_union_full T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) ;

-- Step 75: Query - DIABETES - Get Race & Sex From mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_diab_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.condition,T1.quarter_of_interest,T1.age_group_10_yr, T2.sex, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity 
FROM esp_mdphnet.smk_diab_diab_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 
ON ((T1.natural_key = T2.patid)) ;

-- Step 76: Query - PREV DIABETES - Counts for T1, T2, & T1 or T2 by Quarter - Stratified by Age Group, Sex, and Race
CREATE  TABLE esp_mdphnet.smk_diab_prev_diabetes_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,
count (case when  quarter_of_interest =  'jan_12' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jan_12,
count (case when  quarter_of_interest =  'apr_12' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_apr_12,
count (case when  quarter_of_interest =  'jul_12' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jul_12,
count (case when  quarter_of_interest =  'oct_12' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_oct_12,
count (case when  quarter_of_interest =  'jan_13' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jan_13,
count (case when  quarter_of_interest =  'apr_13' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_apr_13,
count (case when  quarter_of_interest =  'jul_13' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jul_13,
count (case when  quarter_of_interest =  'oct_13' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_oct_13,
count (case when  quarter_of_interest =  'jan_14' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jan_14,
count (case when  quarter_of_interest =  'apr_14' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_apr_14,
count (case when  quarter_of_interest =  'jul_14' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jul_14,
count (case when  quarter_of_interest =  'oct_14' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_oct_14,
count (case when  quarter_of_interest =  'jan_15' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jan_15,
count (case when  quarter_of_interest =  'apr_15' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_apr_15,
count (case when  quarter_of_interest =  'jul_15' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jul_15,
count (case when  quarter_of_interest =  'oct_15' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_oct_15,
count (case when  quarter_of_interest =  'jan_16' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jan_16,
count (case when  quarter_of_interest =  'apr_16' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_apr_16,
count (case when  quarter_of_interest =  'jul_16' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jul_16,
count (case when  quarter_of_interest =  'oct_16' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_oct_16,
count (case when  quarter_of_interest =  'jan_12' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jan_12,
count (case when  quarter_of_interest =  'apr_12' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_apr_12,
count (case when  quarter_of_interest =  'jul_12' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jul_12,
count (case when  quarter_of_interest =  'oct_12' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_oct_12,
count (case when  quarter_of_interest =  'jan_13' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jan_13,
count (case when  quarter_of_interest =  'apr_13' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_apr_13,
count (case when  quarter_of_interest =  'jul_13' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jul_13,
count (case when  quarter_of_interest =  'oct_13' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_oct_13,
count (case when  quarter_of_interest =  'jan_14' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jan_14,
count (case when  quarter_of_interest =  'apr_14' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_apr_14,
count (case when  quarter_of_interest =  'jul_14' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jul_14,
count (case when  quarter_of_interest =  'oct_14' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_oct_14,
count (case when  quarter_of_interest =  'jan_15' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jan_15,
count (case when  quarter_of_interest =  'apr_15' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_apr_15,
count (case when  quarter_of_interest =  'jul_15' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jul_15,
count (case when  quarter_of_interest =  'oct_15' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_oct_15,
count (case when  quarter_of_interest =  'jan_16' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jan_16,
count (case when  quarter_of_interest =  'apr_16' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_apr_16,
count (case when  quarter_of_interest =  'jul_16' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jul_16,
count (case when  quarter_of_interest =  'oct_16' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_oct_16,
count (case when  quarter_of_interest =  'jan_12'  then 1 end) t1_or_t2_diab_prev_jan_12,
count (case when  quarter_of_interest =  'apr_12'  then 1 end) t1_or_t2_diab_prev_apr_12,
count (case when  quarter_of_interest =  'jul_12'  then 1 end) t1_or_t2_diab_prev_jul_12,
count (case when  quarter_of_interest =  'oct_12'  then 1 end) t1_or_t2_diab_prev_oct_12,
count (case when  quarter_of_interest =  'jan_13'  then 1 end) t1_or_t2_diab_prev_jan_13,
count (case when  quarter_of_interest =  'apr_13'  then 1 end) t1_or_t2_diab_prev_apr_13,
count (case when  quarter_of_interest =  'jul_13'  then 1 end) t1_or_t2_diab_prev_jul_13,
count (case when  quarter_of_interest =  'oct_13'  then 1 end) t1_or_t2_diab_prev_oct_13,
count (case when  quarter_of_interest =  'jan_14'  then 1 end) t1_or_t2_diab_prev_jan_14,
count (case when  quarter_of_interest =  'apr_14'  then 1 end) t1_or_t2_diab_prev_apr_14,
count (case when  quarter_of_interest =  'jul_14'  then 1 end) t1_or_t2_diab_prev_jul_14,
count (case when  quarter_of_interest =  'oct_14'  then 1 end) t1_or_t2_diab_prev_oct_14,
count (case when  quarter_of_interest =  'jan_15'  then 1 end) t1_or_t2_diab_prev_jan_15,
count (case when  quarter_of_interest =  'apr_15'  then 1 end) t1_or_t2_diab_prev_apr_15,
count (case when  quarter_of_interest =  'jul_15'  then 1 end) t1_or_t2_diab_prev_jul_15,
count (case when  quarter_of_interest =  'oct_15'  then 1 end) t1_or_t2_diab_prev_oct_15,
count (case when  quarter_of_interest =  'jan_16'  then 1 end) t1_or_t2_diab_prev_jan_16,
count (case when  quarter_of_interest =  'apr_16'  then 1 end) t1_or_t2_diab_prev_apr_16,
count (case when  quarter_of_interest =  'jul_16'  then 1 end) t1_or_t2_diab_prev_jul_16,
count (case when  quarter_of_interest =  'oct_16'  then 1 end) t1_or_t2_diab_prev_oct_16  
FROM  esp_mdphnet.smk_diab_diab_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity;

-- PREV DIABETES - Counts for T1, T2, & T1 or T2 by Quarter - Stratified by Age Group, Sex, and Race & Zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_prev_diabetes  WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,T1.pwtf_comm,
count (case when  quarter_of_interest =  'jan_12' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jan_12,
count (case when  quarter_of_interest =  'apr_12' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_apr_12,
count (case when  quarter_of_interest =  'jul_12' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jul_12,
count (case when  quarter_of_interest =  'oct_12' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_oct_12,
count (case when  quarter_of_interest =  'jan_13' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jan_13,
count (case when  quarter_of_interest =  'apr_13' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_apr_13,
count (case when  quarter_of_interest =  'jul_13' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jul_13,
count (case when  quarter_of_interest =  'oct_13' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_oct_13,
count (case when  quarter_of_interest =  'jan_14' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jan_14,
count (case when  quarter_of_interest =  'apr_14' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_apr_14,
count (case when  quarter_of_interest =  'jul_14' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jul_14,
count (case when  quarter_of_interest =  'oct_14' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_oct_14,
count (case when  quarter_of_interest =  'jan_15' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jan_15,
count (case when  quarter_of_interest =  'apr_15' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_apr_15,
count (case when  quarter_of_interest =  'jul_15' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jul_15,
count (case when  quarter_of_interest =  'oct_15' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_oct_15,
count (case when  quarter_of_interest =  'jan_16' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jan_16,
count (case when  quarter_of_interest =  'apr_16' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_apr_16,
count (case when  quarter_of_interest =  'jul_16' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_jul_16,
count (case when  quarter_of_interest =  'oct_16' and  condition =  'diabetes:type-1'  then 1 end) t1_diab_prev_oct_16,
count (case when  quarter_of_interest =  'jan_12' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jan_12,
count (case when  quarter_of_interest =  'apr_12' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_apr_12,
count (case when  quarter_of_interest =  'jul_12' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jul_12,
count (case when  quarter_of_interest =  'oct_12' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_oct_12,
count (case when  quarter_of_interest =  'jan_13' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jan_13,
count (case when  quarter_of_interest =  'apr_13' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_apr_13,
count (case when  quarter_of_interest =  'jul_13' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jul_13,
count (case when  quarter_of_interest =  'oct_13' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_oct_13,
count (case when  quarter_of_interest =  'jan_14' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jan_14,
count (case when  quarter_of_interest =  'apr_14' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_apr_14,
count (case when  quarter_of_interest =  'jul_14' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jul_14,
count (case when  quarter_of_interest =  'oct_14' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_oct_14,
count (case when  quarter_of_interest =  'jan_15' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jan_15,
count (case when  quarter_of_interest =  'apr_15' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_apr_15,
count (case when  quarter_of_interest =  'jul_15' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jul_15,
count (case when  quarter_of_interest =  'oct_15' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_oct_15,
count (case when  quarter_of_interest =  'jan_16' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jan_16,
count (case when  quarter_of_interest =  'apr_16' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_apr_16,
count (case when  quarter_of_interest =  'jul_16' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_jul_16,
count (case when  quarter_of_interest =  'oct_16' and  condition =  'diabetes:type-2'  then 1 end) t2_diab_prev_oct_16,
count (case when  quarter_of_interest =  'jan_12'  then 1 end) t1_or_t2_diab_prev_jan_12,
count (case when  quarter_of_interest =  'apr_12'  then 1 end) t1_or_t2_diab_prev_apr_12,
count (case when  quarter_of_interest =  'jul_12'  then 1 end) t1_or_t2_diab_prev_jul_12,
count (case when  quarter_of_interest =  'oct_12'  then 1 end) t1_or_t2_diab_prev_oct_12,
count (case when  quarter_of_interest =  'jan_13'  then 1 end) t1_or_t2_diab_prev_jan_13,
count (case when  quarter_of_interest =  'apr_13'  then 1 end) t1_or_t2_diab_prev_apr_13,
count (case when  quarter_of_interest =  'jul_13'  then 1 end) t1_or_t2_diab_prev_jul_13,
count (case when  quarter_of_interest =  'oct_13'  then 1 end) t1_or_t2_diab_prev_oct_13,
count (case when  quarter_of_interest =  'jan_14'  then 1 end) t1_or_t2_diab_prev_jan_14,
count (case when  quarter_of_interest =  'apr_14'  then 1 end) t1_or_t2_diab_prev_apr_14,
count (case when  quarter_of_interest =  'jul_14'  then 1 end) t1_or_t2_diab_prev_jul_14,
count (case when  quarter_of_interest =  'oct_14'  then 1 end) t1_or_t2_diab_prev_oct_14,
count (case when  quarter_of_interest =  'jan_15'  then 1 end) t1_or_t2_diab_prev_jan_15,
count (case when  quarter_of_interest =  'apr_15'  then 1 end) t1_or_t2_diab_prev_apr_15,
count (case when  quarter_of_interest =  'jul_15'  then 1 end) t1_or_t2_diab_prev_jul_15,
count (case when  quarter_of_interest =  'oct_15'  then 1 end) t1_or_t2_diab_prev_oct_15,
count (case when  quarter_of_interest =  'jan_16'  then 1 end) t1_or_t2_diab_prev_jan_16,
count (case when  quarter_of_interest =  'apr_16'  then 1 end) t1_or_t2_diab_prev_apr_16,
count (case when  quarter_of_interest =  'jul_16'  then 1 end) t1_or_t2_diab_prev_jul_16,
count (case when  quarter_of_interest =  'oct_16'  then 1 end) t1_or_t2_diab_prev_oct_16  
FROM  esp_mdphnet.smk_diab_diab_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity, T1.pwtf_comm;


-- SQL - ACTIVE DIABETES - Frank Diabetes Events & metformin)
CREATE  TABLE esp_mdphnet.smk_diab_frank_diab_events  WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.date,T1.name FROM  public.hef_event T1   WHERE date > ('01-01-2012'::date - interval '2 years') and date < '10-01-2016' and 
name in 
('lx:a1c:threshold:gte:6.5',
'lx:glucose-fasting:threshold:gte:126',
'lx:ogtt50-fasting:threshold:gte:126',
'lx:ogtt75-fasting:threshold:gte:126',
'lx:ogtt100-fasting:threshold:gte:126',
'lx:glucose-random:threshold:gte:200',  
'rx:insulin',
'dx:diabetes:all-types',
'rx:glyburide',
'rx:gliclazide',
'rx:glipizide',
'rx:glimepiride',
'rx:pioglitazone',
'rx:rosiglitizone',
'rx:repaglinide',
'rx:nateglinide',
'rx:meglitinide',
'rx:sitagliptin',
'rx:exenatide',
'rx:pramlintide',
'rx:metformin');


/* -- Step 77: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_frank_012012  WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   
WHERE (date > ('01-01-2012'::date - interval  '2 years') and date < '01-01-2012') 
OR (name = 'lx:a1c:threshold:gte:6.5' and date < '01-01-2012');

-- Step 78: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_frank_042012 WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('04-01-2012'::date - interval  '2 years') and date < '04-01-2012';

-- Step 79: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_frank_072012 WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('07-01-2012'::date - interval  '2 years') and date < '07-01-2012';

-- Step 80: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_frank_102012 WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('10-01-2012'::date - interval  '2 years') and date < '10-01-2012';

-- Step 81: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_frank_012013 WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('01-01-2013'::date - interval  '2 years') and date < '01-01-2013';

-- Step 82: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_frank_042013 WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('04-01-2013'::date - interval  '2 years') and date < '04-01-2013';

-- Step 83: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_frank_072013 WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('07-01-2013'::date - interval  '2 years') and date < '07-01-2013';

-- Step 84: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_frank_102013 WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('10-01-2013'::date - interval  '2 years') and date < '10-01-2013';

-- Step 85: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_frank_012014 WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('01-01-2014'::date - interval  '2 years') and date < '01-01-2014';

-- Step 86: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_frank_042014 WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('04-01-2014'::date - interval  '2 years') and date < '04-01-2014';

-- Step 87: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_frank_072014 WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('07-01-2014'::date - interval  '2 years') and date < '07-01-2014';


-- Step 88: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_frank_102014 WITHOUT OIDS  AS SELECT T1.patient_id,T1.name,T1.date FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('10-01-2014'::date - interval  '2 years') and date < '10-01-2014';

-- Step 89: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_frank_012015 WITHOUT OIDS  AS SELECT T1.patient_id,T1.name,T1.date FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('01-01-2015'::date - interval  '2 years') and date < '01-01-2015';

-- Step 90: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_frank_042015 WITHOUT OIDS  AS SELECT T1.patient_id,T1.name,T1.date FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('04-01-2015'::date - interval  '2 years') and date < '04-01-2015';

-- Step 91: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_frank_072015 WITHOUT OIDS  AS SELECT T1.patient_id,T1.name,T1.date FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('07-01-2015'::date - interval  '2 years') and date < '07-01-2015';

-- Step 92: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_frank_102015 WITHOUT OIDS  AS SELECT T1.patient_id,T1.date,T1.name FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('10-01-2015'::date - interval  '2 years') and date < '10-01-2015';

-- Step 93: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_frank_012016 WITHOUT OIDS  AS SELECT T1.patient_id,T1.name,T1.date FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('01-01-2016'::date - interval  '2 years') and date < '01-01-2016';

-- Step 94: Query - ACTIVE DIABETES - Frank Diabetes Events for index date 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_frank_042016 WITHOUT OIDS  AS SELECT T1.patient_id,T1.name,T1.date FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('04-01-2016'::date - interval  '2 years') and date < '04-01-2016';

-- Query - ACTIVE DIABETES - Frank Diabetes Events for index date 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_frank_072016  WITHOUT OIDS  AS SELECT T1.patient_id,T1.name,T1.date FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('07-01-2016'::date - interval  '2 years') and date < '07-01-2016';

-- Query - ACTIVE DIABETES - Frank Diabetes Events for index date 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_frank_102016  WITHOUT OIDS  AS SELECT T1.patient_id,T1.name,T1.date FROM  esp_mdphnet.smk_diab_frank_diab_events T1   WHERE date > ('10-01-2016'::date - interval  '2 years') and date < '10-01-2016'; */

-- ACTIVE DIABETES - Case & Frank Event 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_012012 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_012012 T2 
WHERE ((T1.date > ('01-01-2012'::date - interval  '2 years') and T1.date < '01-01-2012') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '01-01-2012' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_042012 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_042012 T2 
WHERE ((T1.date > ('04-01-2012'::date - interval  '2 years') and T1.date < '04-01-2012') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '04-01-2012' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_072012 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_072012 T2 
WHERE ((T1.date > ('07-01-2012'::date - interval  '2 years') and T1.date < '07-01-2012') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '07-01-2012' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_102012 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_102012 T2 
WHERE ((T1.date > ('10-01-2012'::date - interval  '2 years') and T1.date < '10-01-2012') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '10-01-2012' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_012013 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_012013 T2 
WHERE ((T1.date > ('01-01-2013'::date - interval  '2 years') and T1.date < '01-01-2013') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '01-01-2013' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_042013 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_042013 T2 
WHERE ((T1.date > ('04-01-2013'::date - interval  '2 years') and T1.date < '04-01-2013') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '04-01-2013' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_072013 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_072013 T2 
WHERE ((T1.date > ('07-01-2013'::date - interval  '2 years') and T1.date < '07-01-2013') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '07-01-2013' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_102013 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_102013 T2 
WHERE ((T1.date > ('10-01-2013'::date - interval  '2 years') and T1.date < '10-01-2013') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '10-01-2013' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_012014 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_012014 T2 
WHERE ((T1.date > ('01-01-2014'::date - interval  '2 years') and T1.date < '01-01-2014') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '01-01-2014' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_042014 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_042014 T2 
WHERE ((T1.date > ('04-01-2014'::date - interval  '2 years') and T1.date < '04-01-2014') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '04-01-2014' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_072014 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_072014 T2 
WHERE ((T1.date > ('07-01-2014'::date - interval  '2 years') and T1.date < '07-01-2014') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '07-01-2014' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_102014 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_102014 T2 
WHERE ((T1.date > ('10-01-2014'::date - interval  '2 years') and T1.date < '10-01-2014') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '10-01-2014' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_012015 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_012015 T2 
WHERE ((T1.date > ('01-01-2015'::date - interval  '2 years') and T1.date < '01-01-2015') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '01-01-2015' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_042015 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_042015 T2 
WHERE ((T1.date > ('04-01-2015'::date - interval  '2 years') and T1.date < '04-01-2015') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '04-01-2015' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_072015 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_072015 T2 
WHERE ((T1.date > ('07-01-2015'::date - interval  '2 years') and T1.date < '07-01-2015') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '07-01-2015' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_102015 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_102015 T2 
WHERE ((T1.date > ('10-01-2015'::date - interval  '2 years') and T1.date < '10-01-2015') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '10-01-2015' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_012016 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_012016 T2 
WHERE ((T1.date > ('01-01-2016'::date - interval  '2 years') and T1.date < '01-01-2016') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '01-01-2016' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_042016 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_042016 T2 
WHERE ((T1.date > ('04-01-2016'::date - interval  '2 years') and T1.date < '04-01-2016') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '04-01-2016' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_072016 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_072016 T2 
WHERE ((T1.date > ('07-01-2016'::date - interval  '2 years') and T1.date < '07-01-2016') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '07-01-2016' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;

-- ACTIVE DIABETES - Case & Frank Event 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_102016 AS 
SELECT DISTINCT * FROM (
SELECT T1.patient_id, T2.quarter_of_interest, T2.condition 
FROM  esp_mdphnet.smk_diab_frank_diab_events T1, esp_mdphnet.smk_diab_diab_102016 T2 
WHERE ((T1.date > ('10-01-2016'::date - interval  '2 years') and T1.date < '10-01-2016') 
OR (T1.name = 'lx:a1c:threshold:gte:6.5' and T1.date < '10-01-2016' AND T1.date > T2.case_date))
AND T1.patient_id = T2.patient_id ) AS DUMMYALIAS277 ;


-- Step 113: Union - ACTIVE DIABETES - Bring Together All Active Diabetes Data (2012-2013)
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_union1 WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_012012) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_042012) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_072012) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_102012) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_012013) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_042013) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_072013) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_102013);

-- Step 114: Union - ACTIVE DIABETES - Bring Together All Active Diabetes Data (2014-2016)
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_union2 WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_012014) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_042014) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_072014) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_102014) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_012015) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_042015) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_072015) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_102015) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_012016) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_042016)
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_072016)
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_102016);

-- Step 115: Union - ACTIVE DIABETES - Bring together all of the data (all years)
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_union_full WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_union1) 
UNION (SELECT patient_id,quarter_of_interest,condition FROM esp_mdphnet.smk_diab_act_diab_union2);

-- Step 116: Query - ACTIVE DIABETES - Determine 10 year age group & zip
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_agezip WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T1.condition,T1.quarter_of_interest,T2.natural_key,
CASE   when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' 
end age_group_10_yr,
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
 FROM esp_mdphnet.smk_diab_act_diab_union_full T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) )  AS DUMMYALIAS295 ;

-- Step 117: Query - ACTIVE DIABETES - Get race and sex from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_pat_details WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T1.condition,T1.quarter_of_interest,T1.age_group_10_yr, T2.sex, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity 
FROM esp_mdphnet.smk_diab_act_diab_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 
ON ((T1.natural_key = T2.patid)) )  AS DUMMYALIAS296 ;

-- Step 118: Query - ACTIVE DIABETES - Counts for T1, T2, and T1 or T2 stratified by age goup, sex, and race
CREATE  TABLE esp_mdphnet.smk_diab_act_diab_full_strat WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,
count(case when  quarter_of_interest =  'jan_12' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jan_12,
count(case when  quarter_of_interest =  'apr_12' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_apr_12,
count(case when  quarter_of_interest =  'jul_12' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jul_12,
count(case when  quarter_of_interest =  'oct_12' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_oct_12,
count(case when  quarter_of_interest =  'jan_13' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jan_13,
count(case when  quarter_of_interest =  'apr_13' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_apr_13,
count(case when  quarter_of_interest =  'jul_13' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jul_13,
count(case when  quarter_of_interest =  'oct_13' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_oct_13,
count(case when  quarter_of_interest =  'jan_14' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jan_14,
count(case when  quarter_of_interest =  'apr_14' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_apr_14,
count(case when  quarter_of_interest =  'jul_14' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jul_14,
count(case when  quarter_of_interest =  'oct_14' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_oct_14,
count(case when  quarter_of_interest =  'jan_15' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jan_15,
count(case when  quarter_of_interest =  'apr_15' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_apr_15,
count(case when  quarter_of_interest =  'jul_15' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jul_15,
count(case when  quarter_of_interest =  'oct_15' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_oct_15,
count(case when  quarter_of_interest =  'jan_16' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jan_16,
count(case when  quarter_of_interest =  'apr_16' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_apr_16,
count(case when  quarter_of_interest =  'jul_16' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jul_16,
count(case when  quarter_of_interest =  'oct_16' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_oct_16,
count(case when  quarter_of_interest =  'jan_12' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jan_12,
count(case when  quarter_of_interest =  'apr_12' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_apr_12,
count(case when  quarter_of_interest =  'jul_12' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jul_12,
count(case when  quarter_of_interest =  'oct_12' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_oct_12,
count(case when  quarter_of_interest =  'jan_13' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jan_13,
count(case when  quarter_of_interest =  'apr_13' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_apr_13,
count(case when  quarter_of_interest =  'jul_13' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jul_13,
count(case when  quarter_of_interest =  'oct_13' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_oct_13,
count(case when  quarter_of_interest =  'jan_14' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jan_14,
count(case when  quarter_of_interest =  'apr_14' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_apr_14,
count(case when  quarter_of_interest =  'jul_14' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jul_14,
count(case when  quarter_of_interest =  'oct_14' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_oct_14,
count(case when  quarter_of_interest =  'jan_15' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jan_15,
count(case when  quarter_of_interest =  'apr_15' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_apr_15,
count(case when  quarter_of_interest =  'jul_15' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jul_15,
count(case when  quarter_of_interest =  'oct_15' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_oct_15,
count(case when  quarter_of_interest =  'jan_16' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jan_16,
count(case when  quarter_of_interest =  'apr_16' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_apr_16,
count(case when  quarter_of_interest =  'jul_16' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jul_16,
count(case when  quarter_of_interest =  'oct_16' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_oct_16,
count (case when  quarter_of_interest =  'jan_12'  then 1 end) act_t1_or_t2_diab_jan_12,
count (case when  quarter_of_interest =  'apr_12'  then 1 end) act_t1_or_t2_diab_apr_12,
count (case when  quarter_of_interest =  'jul_12'  then 1 end) act_t1_or_t2_diab_jul_12,
count (case when  quarter_of_interest =  'oct_12'  then 1 end) act_t1_or_t2_diab_oct_12,
count (case when  quarter_of_interest =  'jan_13'  then 1 end) act_t1_or_t2_diab_jan_13,
count (case when  quarter_of_interest =  'apr_13'  then 1 end) act_t1_or_t2_diab_apr_13,
count (case when  quarter_of_interest =  'jul_13'  then 1 end) act_t1_or_t2_diab_jul_13,
count (case when  quarter_of_interest =  'oct_13'  then 1 end) act_t1_or_t2_diab_oct_13,
count (case when  quarter_of_interest =  'jan_14'  then 1 end) act_t1_or_t2_diab_jan_14,
count (case when  quarter_of_interest =  'apr_14'  then 1 end) act_t1_or_t2_diab_apr_14,
count (case when  quarter_of_interest =  'jul_14'  then 1 end) act_t1_or_t2_diab_jul_14,
count (case when  quarter_of_interest =  'oct_14'  then 1 end) act_t1_or_t2_diab_oct_14,
count (case when  quarter_of_interest =  'jan_15'  then 1 end) act_t1_or_t2_diab_jan_15,
count (case when  quarter_of_interest =  'apr_15'  then 1 end) act_t1_or_t2_diab_apr_15,
count (case when  quarter_of_interest =  'jul_15'  then 1 end) act_t1_or_t2_diab_jul_15,
count (case when  quarter_of_interest =  'oct_15'  then 1 end) act_t1_or_t2_diab_oct_15,
count (case when  quarter_of_interest =  'jan_16'  then 1 end) act_t1_or_t2_diab_jan_16,
count (case when  quarter_of_interest =  'apr_16'  then 1 end) act_t1_or_t2_diab_apr_16,
count (case when  quarter_of_interest =  'jul_16'  then 1 end) act_t1_or_t2_diab_jul_16,
count (case when  quarter_of_interest =  'oct_16'  then 1 end) act_t1_or_t2_diab_oct_16   
FROM  esp_mdphnet.smk_diab_act_diab_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity)  AS DUMMYALIAS297 ;

-- ACTIVE DIABETES - Counts for T1, T2, and T1 or T2 stratified by age goup, sex, and race & zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_act_diabetes  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,T1.pwtf_comm,
count(case when  quarter_of_interest =  'jan_12' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jan_12,
count(case when  quarter_of_interest =  'apr_12' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_apr_12,
count(case when  quarter_of_interest =  'jul_12' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jul_12,
count(case when  quarter_of_interest =  'oct_12' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_oct_12,
count(case when  quarter_of_interest =  'jan_13' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jan_13,
count(case when  quarter_of_interest =  'apr_13' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_apr_13,
count(case when  quarter_of_interest =  'jul_13' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jul_13,
count(case when  quarter_of_interest =  'oct_13' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_oct_13,
count(case when  quarter_of_interest =  'jan_14' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jan_14,
count(case when  quarter_of_interest =  'apr_14' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_apr_14,
count(case when  quarter_of_interest =  'jul_14' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jul_14,
count(case when  quarter_of_interest =  'oct_14' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_oct_14,
count(case when  quarter_of_interest =  'jan_15' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jan_15,
count(case when  quarter_of_interest =  'apr_15' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_apr_15,
count(case when  quarter_of_interest =  'jul_15' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jul_15,
count(case when  quarter_of_interest =  'oct_15' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_oct_15,
count(case when  quarter_of_interest =  'jan_16' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jan_16,
count(case when  quarter_of_interest =  'apr_16' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_apr_16,
count(case when  quarter_of_interest =  'jul_16' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_jul_16,
count(case when  quarter_of_interest =  'oct_16' and  condition =  'diabetes:type-1' then 1 end) act_t1_diab_oct_16,
count(case when  quarter_of_interest =  'jan_12' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jan_12,
count(case when  quarter_of_interest =  'apr_12' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_apr_12,
count(case when  quarter_of_interest =  'jul_12' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jul_12,
count(case when  quarter_of_interest =  'oct_12' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_oct_12,
count(case when  quarter_of_interest =  'jan_13' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jan_13,
count(case when  quarter_of_interest =  'apr_13' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_apr_13,
count(case when  quarter_of_interest =  'jul_13' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jul_13,
count(case when  quarter_of_interest =  'oct_13' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_oct_13,
count(case when  quarter_of_interest =  'jan_14' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jan_14,
count(case when  quarter_of_interest =  'apr_14' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_apr_14,
count(case when  quarter_of_interest =  'jul_14' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jul_14,
count(case when  quarter_of_interest =  'oct_14' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_oct_14,
count(case when  quarter_of_interest =  'jan_15' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jan_15,
count(case when  quarter_of_interest =  'apr_15' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_apr_15,
count(case when  quarter_of_interest =  'jul_15' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jul_15,
count(case when  quarter_of_interest =  'oct_15' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_oct_15,
count(case when  quarter_of_interest =  'jan_16' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jan_16,
count(case when  quarter_of_interest =  'apr_16' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_apr_16,
count(case when  quarter_of_interest =  'jul_16' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_jul_16,
count(case when  quarter_of_interest =  'oct_16' and  condition =  'diabetes:type-2' then 1 end) act_t2_diab_oct_16,
count (case when  quarter_of_interest =  'jan_12'  then 1 end) act_t1_or_t2_diab_jan_12,
count (case when  quarter_of_interest =  'apr_12'  then 1 end) act_t1_or_t2_diab_apr_12,
count (case when  quarter_of_interest =  'jul_12'  then 1 end) act_t1_or_t2_diab_jul_12,
count (case when  quarter_of_interest =  'oct_12'  then 1 end) act_t1_or_t2_diab_oct_12,
count (case when  quarter_of_interest =  'jan_13'  then 1 end) act_t1_or_t2_diab_jan_13,
count (case when  quarter_of_interest =  'apr_13'  then 1 end) act_t1_or_t2_diab_apr_13,
count (case when  quarter_of_interest =  'jul_13'  then 1 end) act_t1_or_t2_diab_jul_13,
count (case when  quarter_of_interest =  'oct_13'  then 1 end) act_t1_or_t2_diab_oct_13,
count (case when  quarter_of_interest =  'jan_14'  then 1 end) act_t1_or_t2_diab_jan_14,
count (case when  quarter_of_interest =  'apr_14'  then 1 end) act_t1_or_t2_diab_apr_14,
count (case when  quarter_of_interest =  'jul_14'  then 1 end) act_t1_or_t2_diab_jul_14,
count (case when  quarter_of_interest =  'oct_14'  then 1 end) act_t1_or_t2_diab_oct_14,
count (case when  quarter_of_interest =  'jan_15'  then 1 end) act_t1_or_t2_diab_jan_15,
count (case when  quarter_of_interest =  'apr_15'  then 1 end) act_t1_or_t2_diab_apr_15,
count (case when  quarter_of_interest =  'jul_15'  then 1 end) act_t1_or_t2_diab_jul_15,
count (case when  quarter_of_interest =  'oct_15'  then 1 end) act_t1_or_t2_diab_oct_15,
count (case when  quarter_of_interest =  'jan_16'  then 1 end) act_t1_or_t2_diab_jan_16,
count (case when  quarter_of_interest =  'apr_16'  then 1 end) act_t1_or_t2_diab_apr_16,
count (case when  quarter_of_interest =  'jul_16'  then 1 end) act_t1_or_t2_diab_jul_16,
count (case when  quarter_of_interest =  'oct_16'  then 1 end) act_t1_or_t2_diab_oct_16   
FROM  esp_mdphnet.smk_diab_act_diab_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity, T1.pwtf_comm)  AS DUMMYALIAS297 ;


-- Step 119: SQL - Asthma Med or Dx (Dx must be on diff days. Rx can be same day but must be diff rx)
CREATE  TABLE esp_mdphnet.smk_diab_asthma_med_dx_all WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.date, 
case when T1.name ilike 'dx%' then 'diagnosis' else name end med_or_dx 
FROM public.hef_event T1 
WHERE date > ('01-01-2012'::date - interval '2 years') and date < '10-01-2016' 
and name in (
'dx:asthma', 
'rx:aerobid', 
'rx:albuterol', 
'rx:albuterol-ipratropium:generic', 
'rx:albuterol-ipratropium:trade', 
'rx:alvesco', 
'rx:arformoterol', 
'rx:asmanex', 
'rx:beclomethasone', 
'rx:budesonide-formeterol:generic', 
'rx:budesonide-formeterol:trade', 
'rx:budesonide-inh', 
'rx:ciclesonide-inh', 
'rx:cromolyn-inh', 
'rx:flovent', 
'rx:flunisolide-inh', 
'rx:fluticasone-inh', 
'rx:fluticasone-salmeterol:generic', 
'rx:fluticasone-salmeterol:trade', 
'rx:formeterol', 
'rx:indacaterol', 
'rx:intal', 
'rx:ipratropium', 
'rx:levalbuterol', 
'rx:mometasone-formeterol:generic', 
'rx:mometasone-formeterol:trade', 
'rx:mometasone-inh', 
'rx:montelukast', 
'rx:omalizumab', 
'rx:pirbuterol', 
'rx:pulmicort', 
'rx:salmeterol', 
'rx:tiotropium', 
'rx:zafirlukast', 
'rx:zileuton')
GROUP BY T1.patient_id,T1.date, med_or_dx;

-- Step 120: Query - ASTHMA - Meds & DX for index date 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_asthma_012012 WITHOUT OIDS  AS SELECT T1.patient_id,'jan_12'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_012012  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('01-01-2012'::date - interval  '2 years') and date < '01-01-2012'  
GROUP BY T1.patient_id;

-- Step 121: Query - ASTHMA - Meds & DX for index date 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_asthma_042012 WITHOUT OIDS  AS SELECT T1.patient_id,'apr_12'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_042012  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('04-01-2012'::date - interval  '2 years') and date < '04-01-2012'  
GROUP BY T1.patient_id;

-- Step 122: Query - ASTHMA - Meds & DX for index date 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_asthma_072012 WITHOUT OIDS  AS SELECT T1.patient_id,'jul_12'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_072012  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('07-01-2012'::date - interval  '2 years') and date < '07-01-2012'  
GROUP BY T1.patient_id;

-- Step 123: Query - ASTHMA - Meds & DX for index date 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_asthma_102012 WITHOUT OIDS  AS SELECT T1.patient_id,'oct_12'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_102012 T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('10-01-2012'::date - interval  '2 years') and date < '10-01-2012'  
GROUP BY T1.patient_id;

-- Step 124: Query - ASTHMA - Meds & DX for index date 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_asthma_012013 WITHOUT OIDS  AS SELECT T1.patient_id,'jan_13'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_012013 T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('01-01-2013'::date - interval  '2 years') and date < '01-01-2013'  
GROUP BY T1.patient_id;

-- Step 125: Query - ASTHMA - Meds & DX for index date 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_asthma_042013 WITHOUT OIDS  AS SELECT T1.patient_id,'apr_13'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_042013 T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('04-01-2013'::date - interval  '2 years') and date < '04-01-2013'  
GROUP BY T1.patient_id;

-- Step 126: Query - ASTHMA - Meds & DX for index date 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_asthma_072013 WITHOUT OIDS  AS SELECT T1.patient_id,'jul_13'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_072013  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('07-01-2013'::date - interval  '2 years') and date < '07-01-2013'  
GROUP BY T1.patient_id;

-- Step 127: Query - ASTHMA - Meds & DX for index date 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_asthma_102013 WITHOUT OIDS  AS SELECT T1.patient_id,'oct_13'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_102013  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('10-01-2013'::date - interval  '2 years') and date < '10-01-2013'  
GROUP BY T1.patient_id;

-- Step 128: Query - ASTHMA - Meds & DX for index date 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_asthma_012014 WITHOUT OIDS  AS SELECT T1.patient_id,'jan_14'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_012014  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('01-01-2014'::date - interval  '2 years') and date < '01-01-2014'  
GROUP BY T1.patient_id;

-- Step 129: Query - ASTHMA - Meds & DX for index date 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_asthma_042014 WITHOUT OIDS  AS SELECT T1.patient_id,'apr_14'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_042014  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('04-01-2014'::date - interval  '2 years') and date < '04-01-2014'  
GROUP BY T1.patient_id;

-- Step 130: Query - ASTHMA - Meds & DX for index date 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_asthma_072014 WITHOUT OIDS  AS SELECT T1.patient_id,'jul_14'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_072014  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('07-01-2014'::date - interval  '2 years') and date < '07-01-2014'  
GROUP BY T1.patient_id;

-- Step 131: Query - ASTHMA - Meds & DX for index date 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_asthma_102014 WITHOUT OIDS  AS SELECT T1.patient_id,'oct_14'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_102014  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('10-01-2014'::date - interval  '2 years') and date < '10-01-2014'  
GROUP BY T1.patient_id;

-- Step 132: Query - ASTHMA - Meds & DX for index date 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_asthma_012015 WITHOUT OIDS  AS SELECT T1.patient_id,'jan_15'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_012015  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('01-01-2015'::date - interval  '2 years') and date < '01-01-2015'  
GROUP BY T1.patient_id;

-- Step 133: Query - ASTHMA - Meds & DX for index date 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_asthma_042015 WITHOUT OIDS  AS SELECT T1.patient_id,'apr_15'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_042015 T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('04-01-2015'::date - interval  '2 years') and date < '04-01-2015'  
GROUP BY T1.patient_id;

-- Step 134: Query - ASTHMA - Meds & DX for index date 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_asthma_072015 WITHOUT OIDS  AS SELECT T1.patient_id,'jul_15'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_072015 T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('07-01-2015'::date - interval  '2 years') and date < '07-01-2015'  
GROUP BY T1.patient_id;

-- Step 135: Query - ASTHMA - Meds & DX for index date 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_asthma_102015 WITHOUT OIDS  AS SELECT T1.patient_id,'oct_15'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_102015  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('10-01-2015'::date - interval  '2 years') and date < '10-01-2015'  
GROUP BY T1.patient_id;

-- Step 136: Query - ASTHMA - Meds & DX for index date 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_asthma_012016 WITHOUT OIDS  AS SELECT T1.patient_id,'jan_16'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_012016  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('01-01-2016'::date - interval  '2 years') and date < '01-01-2016'  
GROUP BY T1.patient_id;

-- Step 137: Query - ASTHMA - Meds & DX for index date 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_asthma_042016 WITHOUT OIDS  AS SELECT T1.patient_id,'apr_16'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,  
esp_mdphnet.smk_diab_enc_042016  T2
WHERE T1.patient_id = T2.patient_id 
AND date > ('04-01-2016'::date - interval  '2 years') 
and date < '04-01-2016'  
GROUP BY T1.patient_id;

-- ASTHMA - Meds & DX for index date 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_asthma_072016  WITHOUT OIDS  AS 
SELECT T1.patient_id,'jul_16'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count 
FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_072016  T2
WHERE T1.patient_id = T2.patient_id
AND date > ('07-01-2016'::date - interval  '2 years') and date < '07-01-2016' 
GROUP BY T1.patient_id;

-- ASTHMA - Meds & DX for index date 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_asthma_102016  WITHOUT OIDS  AS 
SELECT T1.patient_id,'oct_16'::text quarter_of_interest,count(case when med_or_dx = 'diagnosis' then 1 end) dx_asthma_count,count(case when med_or_dx ilike 'rx%' then 1 end) med_asthma_count 
FROM  esp_mdphnet.smk_diab_asthma_med_dx_all T1,   
esp_mdphnet.smk_diab_enc_102016  T2
WHERE T1.patient_id = T2.patient_id
AND date > ('10-01-2016'::date - interval  '2 years') and date < '10-01-2016' 
GROUP BY T1.patient_id;


-- Step 138: Union - ASTHMA - Union together all of the data (2012-2013)
CREATE  TABLE esp_mdphnet.smk_diab_asthma_union1 WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_012012) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_042012) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_072012) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_102012) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_012013) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_042013) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_072013) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_102013);

-- Step 139: Union - ASTHMA - Union together all of the data (2013-2016)
CREATE  TABLE esp_mdphnet.smk_diab_asthma_union2 WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_012014) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_042014) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_072014) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_102014) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_012015) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_042015) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_072015) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_102015) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_012016) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_042016)
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_072016)
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_102016);

-- Step 140: Union - ASTHMA - Union together all of the data (all years)
CREATE  TABLE esp_mdphnet.smk_diab_asthma_union_full WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_union1) 
UNION (SELECT patient_id,quarter_of_interest,dx_asthma_count,med_asthma_count FROM esp_mdphnet.smk_diab_asthma_union2);

-- Step 141: Query - ASTHMA - Include patients with ≥2 ICD-9s or ≥2 prescriptions for asthma medications within 2 yrs preceding the index data
CREATE  TABLE esp_mdphnet.smk_diab_asthma_active WITHOUT OIDS  AS SELECT T1.patient_id,T1.quarter_of_interest,T1.dx_asthma_count,T1.med_asthma_count FROM  esp_mdphnet.smk_diab_asthma_union_full T1   WHERE  dx_asthma_count >= 2 or  med_asthma_count >= 2;

-- Step 142: Query - ASTHMA - Determine 10 year age group & zip
CREATE  TABLE esp_mdphnet.smk_diab_asthma_active_agezip WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.natural_key,T1.quarter_of_interest,
CASE   when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' 
end age_group_10_yr, 
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_asthma_active T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) )  AS DUMMYALIAS298 ;

-- Step 143: Query - ASTHMA - Get race and sex from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_asthma_active_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,T1.age_group_10_yr, T2.sex, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity 
FROM esp_mdphnet.smk_diab_asthma_active_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 
ON ((T1.natural_key = T2.patid)) ;

-- Step 144: Query - ASTHMA - Counts stratified by age goup, sex, and race
CREATE  TABLE esp_mdphnet.smk_diab_asthma_active_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  asthma_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  asthma_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  asthma_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  asthma_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  asthma_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  asthma_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  asthma_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  asthma_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  asthma_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  asthma_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  asthma_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  asthma_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  asthma_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  asthma_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  asthma_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  asthma_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  asthma_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  asthma_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  asthma_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  asthma_oct_16    
FROM  esp_mdphnet.smk_diab_asthma_active_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity;

--  ASTHMA - Counts stratified by age goup, sex, and race & zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_asthma  WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,T1.pwtf_comm,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  asthma_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  asthma_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  asthma_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  asthma_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  asthma_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  asthma_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  asthma_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  asthma_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  asthma_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  asthma_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  asthma_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  asthma_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  asthma_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  asthma_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  asthma_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  asthma_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  asthma_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  asthma_apr_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  asthma_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  asthma_oct_16    
FROM  esp_mdphnet.smk_diab_asthma_active_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity,T1.pwtf_comm;


-- Step 145: Query - BMI Conditions - All condition entries during the time span of the report
CREATE  TABLE esp_mdphnet.smk_diab_bmi_all_entries WITHOUT OIDS  AS 
SELECT T2.id patient_id,T1.condition,(T1.date + '1960-01-01'::date) date 
FROM public.esp_condition T1 
INNER JOIN public.emr_patient T2 ON ((T1.patid = T2.natural_key))  
WHERE (date + '1960-01-01'::date) < '10-01-2016' 
and condition in ( 'BMI <25',  'BMI >= 30',  'BMI >=25 and <30', 'No Measured BMI');


-- Step 147: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_bmi_012012 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_012012 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2012'
GROUP BY T1.patient_id;

-- Step 148: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_bmi_042012 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_042012 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2012'
GROUP BY T1.patient_id;

-- Step 149: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_bmi_072012 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_072012 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2012'
GROUP BY T1.patient_id;


-- Step 150: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_bmi_102012 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_12'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_102012 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2012'
GROUP BY T1.patient_id;

-- Step 151: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_bmi_012013 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_012013 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2013'
GROUP BY T1.patient_id;

-- Step 152: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_bmi_042013 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_042013 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2013'
GROUP BY T1.patient_id;

-- Step 153: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_bmi_072013 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_072013 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2013'
GROUP BY T1.patient_id;

-- Step 154: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_bmi_102013 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_13'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_102013 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2013'
GROUP BY T1.patient_id;

-- Step 155: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_bmi_012014 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_012014 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2014'
GROUP BY T1.patient_id;

-- Step 156: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_bmi_042014 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_042014 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2014'
GROUP BY T1.patient_id;

-- Step 157: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_bmi_072014 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_072014 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2014'
GROUP BY T1.patient_id;

-- Step 158: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_bmi_102014 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_14'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_102014 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2014'
GROUP BY T1.patient_id;

-- Step 159: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_bmi_012015 WITHOUT OIDS  AS
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_012015 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2015'
GROUP BY T1.patient_id;

-- Step 160: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_bmi_042015 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_042015 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2015'
GROUP BY T1.patient_id;

-- Step 161: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_bmi_072015  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_072015 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2015'
GROUP BY T1.patient_id;


-- Step 162: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_bmi_102015 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_15'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_102015 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2015'
GROUP BY T1.patient_id;

-- Step 163: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_bmi_012016 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jan_16'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_012016 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '01-01-2016'
GROUP BY T1.patient_id;


-- Step 164: Query - BMI - Most recent BMI condition date in the 2 years prior to the index date - 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_bmi_042016 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'apr_16'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_042016 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '04-01-2016'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_bmi_072016  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'jul_16'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_072016 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '07-01-2016'
GROUP BY T1.patient_id;

-- BMI - Most recent BMI condition date in the 2 years prior to the index date - 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_bmi_102016  WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.date) max_bmi_date,'oct_16'::text quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_all_entries T1, 
esp_mdphnet.smk_diab_enc_102016 T2
WHERE T1.patient_id = T2.patient_id
AND T1.date < '10-01-2016'
GROUP BY T1.patient_id;


-- Step 165: Union - BMI - Union together all the patients and dates (2012-2013)
CREATE  TABLE esp_mdphnet.smk_diab_bmi_union1 WITHOUT OIDS  AS 
(SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_012012) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_042012) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_072012) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_102012) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_012013) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_042013) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_072013) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_102013);

-- Step 166: Union - BMI - Union together all of the patients and dates (2014-2016)
CREATE  TABLE esp_mdphnet.smk_diab_bmi_union2  WITHOUT OIDS  AS 
(SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_012014) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_042014) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_072014) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_102014) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_012015) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_042015) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_072015) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_102015) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_012016) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_042016)
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_072016)
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_102016);

-- Step 167: Union - BMI - Union together all the data (all years)
CREATE  TABLE esp_mdphnet.smk_diab_bmi_union_full  WITHOUT OIDS  AS 
(SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_union1) 
UNION (SELECT patient_id,max_bmi_date,quarter_of_interest FROM esp_mdphnet.smk_diab_bmi_union2);

-- Step 168: Query - BMI - Get the condition for the date of interest.
CREATE  TABLE esp_mdphnet.smk_diab_bmi_dateofint WITHOUT OIDS  AS 
SELECT distinct on (T1.patient_id, quarter_of_interest) T1.patient_id,T2.condition,T1.quarter_of_interest 
FROM esp_mdphnet.smk_diab_bmi_union_full T1 
INNER JOIN esp_mdphnet.smk_diab_bmi_all_entries T2 
ON ((T1.patient_id = T2.patient_id) AND (T1.max_bmi_date = T2.date)) ;


-- Step 169: Query - BMI - Determine 10 year age group & zip
CREATE  TABLE esp_mdphnet.smk_diab_bmi_agezip WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.condition,T1.quarter_of_interest,T2.natural_key,
CASE   when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' 
end age_group_10_yr,
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_bmi_dateofint T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) ;

-- Step 170: Query - BMI - Get race and sex from mdphnet demographic (filter out 0-9 and 10-19 age group)
CREATE  TABLE esp_mdphnet.smk_diab_bmi_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.condition,T1.quarter_of_interest,T1.age_group_10_yr,T2.sex, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity 
FROM esp_mdphnet.smk_diab_bmi_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 ON ((T1.natural_key = T2.patid))  
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19';

-- Step 171: SQL - BMI Produce counts stratified by age, sex, and race
CREATE  TABLE esp_mdphnet.smk_diab_bmi_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,
count(case when  quarter_of_interest = 'jan_12' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jan_12,
count(case when  quarter_of_interest = 'apr_12' and condition = 'BMI >=25 and <30' then 1 end)  overweight_apr_12,
count(case when  quarter_of_interest = 'jul_12' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jul_12,
count(case when  quarter_of_interest = 'oct_12' and condition = 'BMI >=25 and <30' then 1 end)  overweight_oct_12,
count(case when  quarter_of_interest = 'jan_13' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jan_13,
count(case when  quarter_of_interest = 'apr_13' and condition = 'BMI >=25 and <30' then 1 end)  overweight_apr_13,
count(case when  quarter_of_interest = 'jul_13' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jul_13,
count(case when  quarter_of_interest = 'oct_13' and condition = 'BMI >=25 and <30' then 1 end)  overweight_oct_13,
count(case when  quarter_of_interest = 'jan_14' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jan_14,
count(case when  quarter_of_interest = 'apr_14' and condition = 'BMI >=25 and <30' then 1 end)  overweight_apr_14,
count(case when  quarter_of_interest = 'jul_14' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jul_14,
count(case when  quarter_of_interest = 'oct_14' and condition = 'BMI >=25 and <30' then 1 end)  overweight_oct_14,
count(case when  quarter_of_interest = 'jan_15' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jan_15,
count(case when  quarter_of_interest = 'apr_15' and condition = 'BMI >=25 and <30' then 1 end)  overweight_apr_15,
count(case when  quarter_of_interest = 'jul_15' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jul_15,
count(case when  quarter_of_interest = 'oct_15' and condition = 'BMI >=25 and <30' then 1 end)  overweight_oct_15,
count(case when  quarter_of_interest = 'jan_16' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jan_16,
count(case when  quarter_of_interest = 'apr_16' and condition = 'BMI >=25 and <30' then 1 end)  overweight_apr_16,
count(case when  quarter_of_interest = 'jul_16' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jul_16,
count(case when  quarter_of_interest = 'oct_16' and condition = 'BMI >=25 and <30' then 1 end)  overweight_oct_16,
count(case when  quarter_of_interest = 'jan_12' and condition = 'BMI >= 30' then 1 end)  obese_jan_12,
count(case when  quarter_of_interest = 'apr_12' and condition = 'BMI >= 30' then 1 end)  obese_apr_12,
count(case when  quarter_of_interest = 'jul_12' and condition = 'BMI >= 30' then 1 end)  obese_jul_12,
count(case when  quarter_of_interest = 'oct_12' and condition = 'BMI >= 30' then 1 end)  obese_oct_12,
count(case when  quarter_of_interest = 'jan_13' and condition = 'BMI >= 30' then 1 end)  obese_jan_13,
count(case when  quarter_of_interest = 'apr_13' and condition = 'BMI >= 30' then 1 end)  obese_apr_13,
count(case when  quarter_of_interest = 'jul_13' and condition = 'BMI >= 30' then 1 end)  obese_jul_13,
count(case when  quarter_of_interest = 'oct_13' and condition = 'BMI >= 30' then 1 end)  obese_oct_13,
count(case when  quarter_of_interest = 'jan_14' and condition = 'BMI >= 30' then 1 end)  obese_jan_14,
count(case when  quarter_of_interest = 'apr_14' and condition = 'BMI >= 30' then 1 end)  obese_apr_14,
count(case when  quarter_of_interest = 'jul_14' and condition = 'BMI >= 30' then 1 end)  obese_jul_14,
count(case when  quarter_of_interest = 'oct_14' and condition = 'BMI >= 30' then 1 end)  obese_oct_14,
count(case when  quarter_of_interest = 'jan_15' and condition = 'BMI >= 30' then 1 end)  obese_jan_15,
count(case when  quarter_of_interest = 'apr_15' and condition = 'BMI >= 30' then 1 end)  obese_apr_15,
count(case when  quarter_of_interest = 'jul_15' and condition = 'BMI >= 30' then 1 end)  obese_jul_15,
count(case when  quarter_of_interest = 'oct_15' and condition = 'BMI >= 30' then 1 end)  obese_oct_15,
count(case when  quarter_of_interest = 'jan_16' and condition = 'BMI >= 30' then 1 end)  obese_jan_16,
count(case when  quarter_of_interest = 'apr_16' and condition = 'BMI >= 30' then 1 end)  obese_apr_16,
count(case when  quarter_of_interest = 'jul_16' and condition = 'BMI >= 30' then 1 end)  obese_jul_16,
count(case when  quarter_of_interest = 'oct_16' and condition = 'BMI >= 30' then 1 end)  obese_oct_16,
count(case when  quarter_of_interest = 'jan_12' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jan_12,
count(case when  quarter_of_interest = 'apr_12' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_apr_12,
count(case when  quarter_of_interest = 'jul_12' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jul_12,
count(case when  quarter_of_interest = 'oct_12' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_oct_12,
count(case when  quarter_of_interest = 'jan_13' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jan_13,
count(case when  quarter_of_interest = 'apr_13' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_apr_13,
count(case when  quarter_of_interest = 'jul_13' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jul_13,
count(case when  quarter_of_interest = 'oct_13' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_oct_13,
count(case when  quarter_of_interest = 'jan_14' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jan_14,
count(case when  quarter_of_interest = 'apr_14' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_apr_14,
count(case when  quarter_of_interest = 'jul_14' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jul_14,
count(case when  quarter_of_interest = 'oct_14' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_oct_14,
count(case when  quarter_of_interest = 'jan_15' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jan_15,
count(case when  quarter_of_interest = 'apr_15' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_apr_15,
count(case when  quarter_of_interest = 'jul_15' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jul_15,
count(case when  quarter_of_interest = 'oct_15' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_oct_15,
count(case when  quarter_of_interest = 'jan_16' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jan_16,
count(case when  quarter_of_interest = 'apr_16' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_apr_16,
count(case when  quarter_of_interest = 'jul_16' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jul_16,
count(case when  quarter_of_interest = 'oct_16' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_oct_16,
count(case when  quarter_of_interest = 'jan_12' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jan_12,
count(case when  quarter_of_interest = 'apr_12' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_apr_12,
count(case when  quarter_of_interest = 'jul_12' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jul_12,
count(case when  quarter_of_interest = 'oct_12' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_oct_12,
count(case when  quarter_of_interest = 'jan_13' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jan_13,
count(case when  quarter_of_interest = 'apr_13' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_apr_13,
count(case when  quarter_of_interest = 'jul_13' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jul_13,
count(case when  quarter_of_interest = 'oct_13' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_oct_13,
count(case when  quarter_of_interest = 'jan_14' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jan_14,
count(case when  quarter_of_interest = 'apr_14' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_apr_14,
count(case when  quarter_of_interest = 'jul_14' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jul_14,
count(case when  quarter_of_interest = 'oct_14' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_oct_14,
count(case when  quarter_of_interest = 'jan_15' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jan_15,
count(case when  quarter_of_interest = 'apr_15' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_apr_15,
count(case when  quarter_of_interest = 'jul_15' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jul_15,
count(case when  quarter_of_interest = 'oct_15' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_oct_15,
count(case when  quarter_of_interest = 'jan_16' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jan_16,
count(case when  quarter_of_interest = 'apr_16' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_apr_16,
count(case when  quarter_of_interest = 'jul_16' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jul_16,
count(case when  quarter_of_interest = 'oct_16' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_oct_16
FROM  esp_mdphnet.smk_diab_bmi_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex
order by age_group_10_yr;

-- BMI Produce counts stratified by age, sex, and race & zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_bmi  WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm,
count(case when  quarter_of_interest = 'jan_12' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jan_12,
count(case when  quarter_of_interest = 'apr_12' and condition = 'BMI >=25 and <30' then 1 end)  overweight_apr_12,
count(case when  quarter_of_interest = 'jul_12' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jul_12,
count(case when  quarter_of_interest = 'oct_12' and condition = 'BMI >=25 and <30' then 1 end)  overweight_oct_12,
count(case when  quarter_of_interest = 'jan_13' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jan_13,
count(case when  quarter_of_interest = 'apr_13' and condition = 'BMI >=25 and <30' then 1 end)  overweight_apr_13,
count(case when  quarter_of_interest = 'jul_13' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jul_13,
count(case when  quarter_of_interest = 'oct_13' and condition = 'BMI >=25 and <30' then 1 end)  overweight_oct_13,
count(case when  quarter_of_interest = 'jan_14' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jan_14,
count(case when  quarter_of_interest = 'apr_14' and condition = 'BMI >=25 and <30' then 1 end)  overweight_apr_14,
count(case when  quarter_of_interest = 'jul_14' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jul_14,
count(case when  quarter_of_interest = 'oct_14' and condition = 'BMI >=25 and <30' then 1 end)  overweight_oct_14,
count(case when  quarter_of_interest = 'jan_15' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jan_15,
count(case when  quarter_of_interest = 'apr_15' and condition = 'BMI >=25 and <30' then 1 end)  overweight_apr_15,
count(case when  quarter_of_interest = 'jul_15' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jul_15,
count(case when  quarter_of_interest = 'oct_15' and condition = 'BMI >=25 and <30' then 1 end)  overweight_oct_15,
count(case when  quarter_of_interest = 'jan_16' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jan_16,
count(case when  quarter_of_interest = 'apr_16' and condition = 'BMI >=25 and <30' then 1 end)  overweight_apr_16,
count(case when  quarter_of_interest = 'jul_16' and condition = 'BMI >=25 and <30' then 1 end)  overweight_jul_16,
count(case when  quarter_of_interest = 'oct_16' and condition = 'BMI >=25 and <30' then 1 end)  overweight_oct_16,
count(case when  quarter_of_interest = 'jan_12' and condition = 'BMI >= 30' then 1 end)  obese_jan_12,
count(case when  quarter_of_interest = 'apr_12' and condition = 'BMI >= 30' then 1 end)  obese_apr_12,
count(case when  quarter_of_interest = 'jul_12' and condition = 'BMI >= 30' then 1 end)  obese_jul_12,
count(case when  quarter_of_interest = 'oct_12' and condition = 'BMI >= 30' then 1 end)  obese_oct_12,
count(case when  quarter_of_interest = 'jan_13' and condition = 'BMI >= 30' then 1 end)  obese_jan_13,
count(case when  quarter_of_interest = 'apr_13' and condition = 'BMI >= 30' then 1 end)  obese_apr_13,
count(case when  quarter_of_interest = 'jul_13' and condition = 'BMI >= 30' then 1 end)  obese_jul_13,
count(case when  quarter_of_interest = 'oct_13' and condition = 'BMI >= 30' then 1 end)  obese_oct_13,
count(case when  quarter_of_interest = 'jan_14' and condition = 'BMI >= 30' then 1 end)  obese_jan_14,
count(case when  quarter_of_interest = 'apr_14' and condition = 'BMI >= 30' then 1 end)  obese_apr_14,
count(case when  quarter_of_interest = 'jul_14' and condition = 'BMI >= 30' then 1 end)  obese_jul_14,
count(case when  quarter_of_interest = 'oct_14' and condition = 'BMI >= 30' then 1 end)  obese_oct_14,
count(case when  quarter_of_interest = 'jan_15' and condition = 'BMI >= 30' then 1 end)  obese_jan_15,
count(case when  quarter_of_interest = 'apr_15' and condition = 'BMI >= 30' then 1 end)  obese_apr_15,
count(case when  quarter_of_interest = 'jul_15' and condition = 'BMI >= 30' then 1 end)  obese_jul_15,
count(case when  quarter_of_interest = 'oct_15' and condition = 'BMI >= 30' then 1 end)  obese_oct_15,
count(case when  quarter_of_interest = 'jan_16' and condition = 'BMI >= 30' then 1 end)  obese_jan_16,
count(case when  quarter_of_interest = 'apr_16' and condition = 'BMI >= 30' then 1 end)  obese_apr_16,
count(case when  quarter_of_interest = 'jul_16' and condition = 'BMI >= 30' then 1 end)  obese_jul_16,
count(case when  quarter_of_interest = 'oct_16' and condition = 'BMI >= 30' then 1 end)  obese_oct_16,
count(case when  quarter_of_interest = 'jan_12' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jan_12,
count(case when  quarter_of_interest = 'apr_12' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_apr_12,
count(case when  quarter_of_interest = 'jul_12' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jul_12,
count(case when  quarter_of_interest = 'oct_12' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_oct_12,
count(case when  quarter_of_interest = 'jan_13' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jan_13,
count(case when  quarter_of_interest = 'apr_13' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_apr_13,
count(case when  quarter_of_interest = 'jul_13' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jul_13,
count(case when  quarter_of_interest = 'oct_13' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_oct_13,
count(case when  quarter_of_interest = 'jan_14' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jan_14,
count(case when  quarter_of_interest = 'apr_14' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_apr_14,
count(case when  quarter_of_interest = 'jul_14' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jul_14,
count(case when  quarter_of_interest = 'oct_14' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_oct_14,
count(case when  quarter_of_interest = 'jan_15' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jan_15,
count(case when  quarter_of_interest = 'apr_15' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_apr_15,
count(case when  quarter_of_interest = 'jul_15' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jul_15,
count(case when  quarter_of_interest = 'oct_15' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_oct_15,
count(case when  quarter_of_interest = 'jan_16' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jan_16,
count(case when  quarter_of_interest = 'apr_16' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_apr_16,
count(case when  quarter_of_interest = 'jul_16' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_jul_16,
count(case when  quarter_of_interest = 'oct_16' and (condition = 'BMI >=25 and <30' or condition = 'BMI >= 30') then 1 end)  overweight_or_obese_oct_16,
count(case when  quarter_of_interest = 'jan_12' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jan_12,
count(case when  quarter_of_interest = 'apr_12' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_apr_12,
count(case when  quarter_of_interest = 'jul_12' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jul_12,
count(case when  quarter_of_interest = 'oct_12' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_oct_12,
count(case when  quarter_of_interest = 'jan_13' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jan_13,
count(case when  quarter_of_interest = 'apr_13' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_apr_13,
count(case when  quarter_of_interest = 'jul_13' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jul_13,
count(case when  quarter_of_interest = 'oct_13' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_oct_13,
count(case when  quarter_of_interest = 'jan_14' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jan_14,
count(case when  quarter_of_interest = 'apr_14' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_apr_14,
count(case when  quarter_of_interest = 'jul_14' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jul_14,
count(case when  quarter_of_interest = 'oct_14' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_oct_14,
count(case when  quarter_of_interest = 'jan_15' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jan_15,
count(case when  quarter_of_interest = 'apr_15' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_apr_15,
count(case when  quarter_of_interest = 'jul_15' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jul_15,
count(case when  quarter_of_interest = 'oct_15' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_oct_15,
count(case when  quarter_of_interest = 'jan_16' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jan_16,
count(case when  quarter_of_interest = 'apr_16' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_apr_16,
count(case when  quarter_of_interest = 'jul_16' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_jul_16,
count(case when  quarter_of_interest = 'oct_16' and condition = 'No Measured BMI' then 1 end)  no_measured_bmi_oct_16
FROM  esp_mdphnet.smk_diab_bmi_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm
order by age_group_10_yr;


-- Step 177: Union - DENOMINATOR - Bring together all encounters (2012-2013)
CREATE  TABLE esp_mdphnet.smk_diab_denom_union1 WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012012) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042012) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072012) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102012) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012013) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042013) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072013) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102013);

-- Step 178: Union - DENOMINATOR - Bring together all encounters (2014-2016)
CREATE  TABLE esp_mdphnet.smk_diab_denom_union2 WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012014) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042014) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072014) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102014) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012015) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042015) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072015) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102015) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_012016) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_042016)
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_072016)
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_enc_102016);

-- Step 179: Union - DENOMINATOR - Bring Together All Data (all years)
CREATE  TABLE esp_mdphnet.smk_diab_denom_union_full WITHOUT OIDS  AS 
(SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_denom_union1) 
UNION (SELECT patient_id,quarter_of_interest FROM esp_mdphnet.smk_diab_denom_union2);

-- Step 180: Query - DENOMINATOR - Determine 10 year age group & zip
CREATE  TABLE esp_mdphnet.smk_diab_denom_agezip WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T1.quarter_of_interest,T2.natural_key,
CASE   when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' 
end age_group_10_yr,
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_denom_union_full T1 
INNER JOIN public.emr_patient T2 
ON ((T1.patient_id = T2.id)) )  AS DUMMYALIAS299 ;

-- Step 181: Query - DENOMINATOR - Get race and sex from mdphnet
CREATE  TABLE esp_mdphnet.smk_diab_denom_pat_details WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T1.quarter_of_interest,T1.age_group_10_yr, T2.sex, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity 
FROM esp_mdphnet.smk_diab_denom_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 
ON ((T1.natural_key = T2.patid)) )  AS DUMMYALIAS300 ;

-- Step 182: Query - DENOMINATOR - Counts per quarter stratified by age group, sex, and race
CREATE  TABLE esp_mdphnet.smk_diab_denom_full_strat WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,
count (case when  quarter_of_interest =  'jan_12'  then 1 end) denominator_jan_12,
count (case when  quarter_of_interest =  'apr_12'  then 1 end) denominator_apr_12,
count (case when  quarter_of_interest =  'jul_12'  then 1 end) denominator_jul_12,
count (case when  quarter_of_interest =  'oct_12'  then 1 end) denominator_oct_12,
count (case when  quarter_of_interest =  'jan_13'  then 1 end) denominator_jan_13,
count (case when  quarter_of_interest =  'apr_13'  then 1 end) denominator_apr_13,
count (case when  quarter_of_interest =  'jul_13'  then 1 end) denominator_jul_13,
count (case when  quarter_of_interest =  'oct_13'  then 1 end) denominator_oct_13,
count (case when  quarter_of_interest =  'jan_14'  then 1 end) denominator_jan_14,
count (case when  quarter_of_interest =  'apr_14'  then 1 end) denominator_apr_14,
count (case when  quarter_of_interest =  'jul_14'  then 1 end) denominator_jul_14,
count (case when  quarter_of_interest =  'oct_14'  then 1 end) denominator_oct_14,
count (case when  quarter_of_interest =  'jan_15'  then 1 end) denominator_jan_15,
count (case when  quarter_of_interest =  'apr_15'  then 1 end) denominator_apr_15,
count (case when  quarter_of_interest =  'jul_15'  then 1 end) denominator_jul_15,
count (case when  quarter_of_interest =  'oct_15'  then 1 end) denominator_oct_15,
count (case when  quarter_of_interest =  'jan_16'  then 1 end) denominator_jan_16,
count (case when  quarter_of_interest =  'apr_16'  then 1 end) denominator_apr_16,
count (case when  quarter_of_interest =  'jul_16'  then 1 end) denominator_jul_16,
count (case when  quarter_of_interest =  'oct_16'  then 1 end) denominator_oct_16  
FROM  esp_mdphnet.smk_diab_denom_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity)  AS DUMMYALIAS301 ;

-- DENOMINATOR - Counts per quarter stratified by age group, sex, and race & zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_denom  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity, T1.pwtf_comm,
count (case when  quarter_of_interest =  'jan_12'  then 1 end) denominator_jan_12,
count (case when  quarter_of_interest =  'apr_12'  then 1 end) denominator_apr_12,
count (case when  quarter_of_interest =  'jul_12'  then 1 end) denominator_jul_12,
count (case when  quarter_of_interest =  'oct_12'  then 1 end) denominator_oct_12,
count (case when  quarter_of_interest =  'jan_13'  then 1 end) denominator_jan_13,
count (case when  quarter_of_interest =  'apr_13'  then 1 end) denominator_apr_13,
count (case when  quarter_of_interest =  'jul_13'  then 1 end) denominator_jul_13,
count (case when  quarter_of_interest =  'oct_13'  then 1 end) denominator_oct_13,
count (case when  quarter_of_interest =  'jan_14'  then 1 end) denominator_jan_14,
count (case when  quarter_of_interest =  'apr_14'  then 1 end) denominator_apr_14,
count (case when  quarter_of_interest =  'jul_14'  then 1 end) denominator_jul_14,
count (case when  quarter_of_interest =  'oct_14'  then 1 end) denominator_oct_14,
count (case when  quarter_of_interest =  'jan_15'  then 1 end) denominator_jan_15,
count (case when  quarter_of_interest =  'apr_15'  then 1 end) denominator_apr_15,
count (case when  quarter_of_interest =  'jul_15'  then 1 end) denominator_jul_15,
count (case when  quarter_of_interest =  'oct_15'  then 1 end) denominator_oct_15,
count (case when  quarter_of_interest =  'jan_16'  then 1 end) denominator_jan_16,
count (case when  quarter_of_interest =  'apr_16'  then 1 end) denominator_apr_16,
count (case when  quarter_of_interest =  'jul_16'  then 1 end) denominator_jul_16,
count (case when  quarter_of_interest =  'oct_16'  then 1 end) denominator_oct_16    
FROM  esp_mdphnet.smk_diab_denom_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity, T1.pwtf_comm)  AS DUMMYALIAS301 ;

-- BMI Denominators Method Using Standard Denominator and Not Including Those Under 20 - stratified by age group, sex, and race
CREATE  TABLE esp_mdphnet.smk_diab_bmi_denom_full_strat WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,
count(case when  quarter_of_interest = 'jan_12'  then 1 end)  bmi_denom_jan_12,
count(case when  quarter_of_interest = 'apr_12'  then 1 end)  bmi_denom_apr_12,
count(case when  quarter_of_interest = 'jul_12'  then 1 end)  bmi_denom_jul_12,
count(case when  quarter_of_interest = 'oct_12'  then 1 end)  bmi_denom_oct_12,
count(case when  quarter_of_interest = 'jan_13'  then 1 end)  bmi_denom_jan_13,
count(case when  quarter_of_interest = 'apr_13'  then 1 end)  bmi_denom_apr_13,
count(case when  quarter_of_interest = 'jul_13'  then 1 end)  bmi_denom_jul_13,
count(case when  quarter_of_interest = 'oct_13'  then 1 end)  bmi_denom_oct_13,
count(case when  quarter_of_interest = 'jan_14'  then 1 end)  bmi_denom_jan_14,
count(case when  quarter_of_interest = 'apr_14'  then 1 end)  bmi_denom_apr_14,
count(case when  quarter_of_interest = 'jul_14'  then 1 end)  bmi_denom_jul_14,
count(case when  quarter_of_interest = 'oct_14'  then 1 end)  bmi_denom_oct_14,
count(case when  quarter_of_interest = 'jan_15'  then 1 end)  bmi_denom_jan_15,
count(case when  quarter_of_interest = 'apr_15'  then 1 end)  bmi_denom_apr_15,
count(case when  quarter_of_interest = 'jul_15'  then 1 end)  bmi_denom_jul_15,
count(case when  quarter_of_interest = 'oct_15'  then 1 end)  bmi_denom_oct_15,
count(case when  quarter_of_interest = 'jan_16'  then 1 end)  bmi_denom_jan_16,
count(case when  quarter_of_interest = 'apr_16'  then 1 end)  bmi_denom_apr_16,
count(case when  quarter_of_interest = 'jul_16'  then 1 end)  bmi_denom_jul_16,
count(case when  quarter_of_interest = 'oct_16'  then 1 end)  bmi_denom_oct_16
FROM  esp_mdphnet.smk_diab_denom_pat_details T1  
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity)  AS DUMMYALIAS301 ;

-- BMI Denominators Method Using Standard Denominator and Not Including Those Under 20 - stratified by age group, sex, and race & zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_bmi_denom  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity, T1.pwtf_comm,
count(case when  quarter_of_interest = 'jan_12'  then 1 end)  bmi_denom_jan_12,
count(case when  quarter_of_interest = 'apr_12'  then 1 end)  bmi_denom_apr_12,
count(case when  quarter_of_interest = 'jul_12'  then 1 end)  bmi_denom_jul_12,
count(case when  quarter_of_interest = 'oct_12'  then 1 end)  bmi_denom_oct_12,
count(case when  quarter_of_interest = 'jan_13'  then 1 end)  bmi_denom_jan_13,
count(case when  quarter_of_interest = 'apr_13'  then 1 end)  bmi_denom_apr_13,
count(case when  quarter_of_interest = 'jul_13'  then 1 end)  bmi_denom_jul_13,
count(case when  quarter_of_interest = 'oct_13'  then 1 end)  bmi_denom_oct_13,
count(case when  quarter_of_interest = 'jan_14'  then 1 end)  bmi_denom_jan_14,
count(case when  quarter_of_interest = 'apr_14'  then 1 end)  bmi_denom_apr_14,
count(case when  quarter_of_interest = 'jul_14'  then 1 end)  bmi_denom_jul_14,
count(case when  quarter_of_interest = 'oct_14'  then 1 end)  bmi_denom_oct_14,
count(case when  quarter_of_interest = 'jan_15'  then 1 end)  bmi_denom_jan_15,
count(case when  quarter_of_interest = 'apr_15'  then 1 end)  bmi_denom_apr_15,
count(case when  quarter_of_interest = 'jul_15'  then 1 end)  bmi_denom_jul_15,
count(case when  quarter_of_interest = 'oct_15'  then 1 end)  bmi_denom_oct_15,
count(case when  quarter_of_interest = 'jan_16'  then 1 end)  bmi_denom_jan_16,
count(case when  quarter_of_interest = 'apr_16'  then 1 end)  bmi_denom_apr_16,
count(case when  quarter_of_interest = 'jul_16'  then 1 end)  bmi_denom_jul_16,
count(case when  quarter_of_interest = 'oct_16'  then 1 end)  bmi_denom_oct_16
FROM  esp_mdphnet.smk_diab_denom_pat_details T1  
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity, T1.pwtf_comm)  AS DUMMYALIAS301 ;

-- HYPERTENSION All hypertension cases and events
CREATE  TABLE esp_mdphnet.smk_diab_hyper_all_events WITHOUT OIDS  AS 
select T1.patient_id, T1.id, T1.condition, T1.date as case_date, T2.date as history_date, T2.status, T1.isactive
from nodis_case T1, nodis_caseactivehistory T2
WHERE T1.date < '10-01-2016' 
and condition = 'hypertension'
ANd T1.id = T2.case_id
order by case_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_012012 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_012012 T2   
WHERE  history_date < '01-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_042012 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_042012 T2   
WHERE  history_date < '04-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_072012 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_072012 T2   
WHERE  history_date < '07-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_102012 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_12'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_102012 T2   
WHERE  history_date < '10-01-2012'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_012013 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_012013 T2   
WHERE  history_date < '01-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_042013 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_042013 T2   
WHERE  history_date < '04-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_072013 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_072013 T2   
WHERE  history_date < '07-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_102013 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_13'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_102013 T2   
WHERE  history_date < '10-01-2013'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_012014 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_012014 T2   
WHERE  history_date < '01-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_042014 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_042014 T2   
WHERE  history_date < '04-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_072014 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_072014 T2   
WHERE  history_date < '07-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_102014 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_14'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_102014 T2   
WHERE  history_date < '10-01-2014'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_012015 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_012015 T2   
WHERE  history_date < '01-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_042015 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_042015 T2   
WHERE  history_date < '04-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_072015 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_072015 T2   
WHERE  history_date < '07-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_102015 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_15'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_102015 T2   
WHERE  history_date < '10-01-2015'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_012016 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jan_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_012016 T2   
WHERE  history_date < '01-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_042016 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'apr_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_042016 T2   
WHERE  history_date < '04-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_072016 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'jul_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_072016 T2   
WHERE  history_date < '07-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;

-- HYPERTENSION Most recent hypertension date in the 2 years prior to the index date - 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_102016 WITHOUT OIDS  AS 
SELECT T1.patient_id,max(T1.history_date) max_case_event_date,'oct_16'::text quarter_of_interest 
FROM  esp_mdphnet.smk_diab_hyper_all_events T1,
esp_mdphnet.smk_diab_enc_102016 T2   
WHERE  history_date < '10-01-2016'
AND T1.patient_id = T2.patient_id  
GROUP BY T1.patient_id;


-- HYPERTENSION - Union together all the patients and dates (2012-2016)
CREATE  TABLE esp_mdphnet.smk_diab_hyper_union_full WITHOUT OIDS  AS 
(SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_012012) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_042012)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_072012) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_102012)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_012013)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_042013) 
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_072013)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_102013)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_012014)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_042014)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_072014)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_102014)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_012015)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_042015)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_072015)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_102015)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_012016)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_042016)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_072016)
UNION (SELECT patient_id,max_case_event_date,quarter_of_interest FROM esp_mdphnet.smk_diab_hyper_102016);

-- HYPERTENSION- Get details for active hypertension
CREATE  TABLE esp_mdphnet.smk_diab_hyper_dateofint WITHOUT OIDS  AS 
SELECT T1.patient_id, T2.id as case_id, T2.status,T1.quarter_of_interest, T2.history_date as hyper_active_date
FROM esp_mdphnet.smk_diab_hyper_union_full T1 
INNER JOIN esp_mdphnet.smk_diab_hyper_all_events T2 
ON ((T1.patient_id = T2.patient_id) 
AND (T1.max_case_event_date = T2.history_date))
AND status in ('I', 'R');

-- HYPERTENSION - Determine 10 yr age group & zip
CREATE  TABLE esp_mdphnet.smk_diab_hyper_agezip WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.status,T1.quarter_of_interest,
CASE   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' end age_group_10_yr,
T2.natural_key,
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_hyper_dateofint T1 
INNER JOIN public.emr_patient T2 
ON ((T1.patient_id = T2.id)) ;

-- HYPERTENSION - Get sex and race from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_hyper_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.status,T1.quarter_of_interest,T1.age_group_10_yr, T1.pwtf_comm,
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity,T2.sex 
FROM esp_mdphnet.smk_diab_hyper_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 
ON ((T1.natural_key = T2.patid)) ;

-- HYPERTENSION - Hypertension Counts By Quarter Stratified By Age Group, Race, and Sex
CREATE  TABLE esp_mdphnet.smk_diab_hyper_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  hypertension_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  hypertension_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  hypertension_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  hypertension_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  hypertension_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  hypertension_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  hypertension_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  hypertension_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  hypertension_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  hypertension_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  hypertension_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  hypertension_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  hypertension_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  hypertension_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  hypertension_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  hypertension_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  hypertension_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  hypertension_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  hypertension_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  hypertension_oct_16  
FROM  esp_mdphnet.smk_diab_hyper_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex;

-- HYPERTENSION - Hypertension Counts By Quarter Stratified By Age Group, Race, and Sex & Zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_hyper  WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  hypertension_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  hypertension_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  hypertension_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  hypertension_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  hypertension_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  hypertension_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  hypertension_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  hypertension_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  hypertension_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  hypertension_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  hypertension_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  hypertension_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  hypertension_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  hypertension_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  hypertension_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  hypertension_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  hypertension_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  hypertension_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  hypertension_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  hypertension_oct_16    
FROM  esp_mdphnet.smk_diab_hyper_pat_details T1   
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 01-01-2012
CREATE TABLE esp_mdphnet.smk_diab_systolic_012012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'jan_12'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('01-01-2012'::date - interval  '1 years') and date < '01-01-2012'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('01-01-2012'::date - interval  '1 years') and date < '01-01-2012'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 04-01-2012
CREATE TABLE esp_mdphnet.smk_diab_systolic_042012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'apr_12'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('04-01-2012'::date - interval  '1 years') and date < '04-01-2012'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('04-01-2012'::date - interval  '1 years') and date < '04-01-2012'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 07-01-2012
CREATE TABLE esp_mdphnet.smk_diab_systolic_072012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'jul_12'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('07-01-2012'::date - interval  '1 years') and date < '07-01-2012'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('07-01-2012'::date - interval  '1 years') and date < '07-01-2012'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 10-01-2012
CREATE TABLE esp_mdphnet.smk_diab_systolic_102012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'oct_12'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('10-01-2012'::date - interval  '1 years') and date < '10-01-2012'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('10-01-2012'::date - interval  '1 years') and date < '10-01-2012'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 01-01-2013
CREATE TABLE esp_mdphnet.smk_diab_systolic_012013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'jan_13'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('01-01-2013'::date - interval  '1 years') and date < '01-01-2013'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('01-01-2013'::date - interval  '1 years') and date < '01-01-2013'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 04-01-2013
CREATE TABLE esp_mdphnet.smk_diab_systolic_042013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'apr_13'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('04-01-2013'::date - interval  '1 years') and date < '04-01-2013'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('04-01-2013'::date - interval  '1 years') and date < '04-01-2013'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 07-01-2013
CREATE TABLE esp_mdphnet.smk_diab_systolic_072013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'jul_13'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('07-01-2013'::date - interval  '1 years') and date < '07-01-2013'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('07-01-2013'::date - interval  '1 years') and date < '07-01-2013'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 10-01-2013
CREATE TABLE esp_mdphnet.smk_diab_systolic_102013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'oct_13'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('10-01-2013'::date - interval  '1 years') and date < '10-01-2013'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('10-01-2013'::date - interval  '1 years') and date < '10-01-2013'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 01-01-2014
CREATE TABLE esp_mdphnet.smk_diab_systolic_012014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'jan_14'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('01-01-2014'::date - interval  '1 years') and date < '01-01-2014'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('01-01-2014'::date - interval  '1 years') and date < '01-01-2014'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 04-01-2014
CREATE TABLE esp_mdphnet.smk_diab_systolic_042014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'apr_14'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('04-01-2014'::date - interval  '1 years') and date < '04-01-2014'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('04-01-2014'::date - interval  '1 years') and date < '04-01-2014'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 07-01-2014
CREATE TABLE esp_mdphnet.smk_diab_systolic_072014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'jul_14'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('07-01-2014'::date - interval  '1 years') and date < '07-01-2014'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('07-01-2014'::date - interval  '1 years') and date < '07-01-2014'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 10-01-2014
CREATE TABLE esp_mdphnet.smk_diab_systolic_102014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'oct_14'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('10-01-2014'::date - interval  '1 years') and date < '10-01-2014'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('10-01-2014'::date - interval  '1 years') and date < '10-01-2014'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 01-01-2015
CREATE TABLE esp_mdphnet.smk_diab_systolic_012015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'jan_15'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('01-01-2015'::date - interval  '1 years') and date < '01-01-2015'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('01-01-2015'::date - interval  '1 years') and date < '01-01-2015'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 04-01-2015
CREATE TABLE esp_mdphnet.smk_diab_systolic_042015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'apr_15'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('04-01-2015'::date - interval  '1 years') and date < '04-01-2015'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('04-01-2015'::date - interval  '1 years') and date < '04-01-2015'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 07-01-2015
CREATE TABLE esp_mdphnet.smk_diab_systolic_072015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'jul_15'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('07-01-2015'::date - interval  '1 years') and date < '07-01-2015'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('07-01-2015'::date - interval  '1 years') and date < '07-01-2015'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 10-01-2015
CREATE TABLE esp_mdphnet.smk_diab_systolic_102015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'oct_15'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('10-01-2015'::date - interval  '1 years') and date < '10-01-2015'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('10-01-2015'::date - interval  '1 years') and date < '10-01-2015'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 01-01-2016
CREATE TABLE esp_mdphnet.smk_diab_systolic_012016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'jan_16'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('01-01-2016'::date - interval  '1 years') and date < '01-01-2016'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('01-01-2016'::date - interval  '1 years') and date < '01-01-2016'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 04-01-2016
CREATE TABLE esp_mdphnet.smk_diab_systolic_042016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'apr_16'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('04-01-2016'::date - interval  '1 years') and date < '04-01-2016'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('04-01-2016'::date - interval  '1 years') and date < '04-01-2016'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 07-01-2016
CREATE TABLE esp_mdphnet.smk_diab_systolic_072016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'jul_16'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('07-01-2016'::date - interval  '1 years') and date < '07-01-2016'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('07-01-2016'::date - interval  '1 years') and date < '07-01-2016'
ORDER BY T1.patient_id;

-- AVERAGE SBP Most recent SBP date and value in the 1 year prior to the index date - 10-01-2016
CREATE TABLE esp_mdphnet.smk_diab_systolic_102016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_sbp_date, T1.bp_systolic, 'oct_16'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_sbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('10-01-2016'::date - interval  '1 years') and date < '10-01-2016'
	AND TT1.bp_systolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_sbp_date
AND T1.bp_systolic is not null
AND date > ('10-01-2016'::date - interval  '1 years') and date < '10-01-2016'
ORDER BY T1.patient_id;

-- AVERAGE SBP - Union together all the patients and dates (2012-2016)
CREATE  TABLE esp_mdphnet.smk_diab_systolic_union_full WITHOUT OIDS  AS 
(SELECT * FROM esp_mdphnet.smk_diab_systolic_012012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_042012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_072012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_102012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_012013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_042013) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_072013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_102013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_012014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_042014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_072014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_102014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_012015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_042015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_072015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_102015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_012016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_042016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_072016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_systolic_102016);

-- AVERAGE SBP - JOIN WITH ENCOUNTERS TO APPLY CENTER FILTERING
CREATE TABLE esp_mdphnet.smk_diab_systolic_union_full_w_enc AS
SELECT T1.*
FROM esp_mdphnet.smk_diab_systolic_union_full T1,
esp_mdphnet.smk_diab_denom_union_full T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = T2.quarter_of_interest;

-- AVERAGE SBP - Determine 10 yr age group & zip
CREATE  TABLE esp_mdphnet.smk_diab_systolic_agezip WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.natural_key,T1.quarter_of_interest,
CASE   when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' 
end age_group_10_yr, 
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm,
T1.bp_systolic
FROM esp_mdphnet.smk_diab_systolic_union_full_w_enc T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) )  AS DUMMYALIAS298 ;

-- AVERAGE SBP - Get race and sex from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_systolic_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,T1.age_group_10_yr, T2.sex, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity,
T1.bp_systolic 
FROM esp_mdphnet.smk_diab_systolic_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 
ON ((T1.natural_key = T2.patid)) ;

-- AVERAGE SBP - Averages By Quarter Stratified By Age Group, Race, and Sex
CREATE  TABLE esp_mdphnet.smk_diab_systolic_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,
round(avg(CASE WHEN quarter_of_interest = 'jan_12' then bp_systolic end)) systolic_avg_jan_12,
round(avg(CASE WHEN quarter_of_interest = 'apr_12' then bp_systolic end)) systolic_avg_apr_12,
round(avg(CASE WHEN quarter_of_interest = 'jul_12' then bp_systolic end)) systolic_avg_jul_12,
round(avg(CASE WHEN quarter_of_interest = 'oct_12' then bp_systolic end)) systolic_avg_oct_12,
round(avg(CASE WHEN quarter_of_interest = 'jan_13' then bp_systolic end)) systolic_avg_jan_13,
round(avg(CASE WHEN quarter_of_interest = 'apr_13' then bp_systolic end)) systolic_avg_apr_13,
round(avg(CASE WHEN quarter_of_interest = 'jul_13' then bp_systolic end)) systolic_avg_jul_13,
round(avg(CASE WHEN quarter_of_interest = 'oct_13' then bp_systolic end)) systolic_avg_oct_13,
round(avg(CASE WHEN quarter_of_interest = 'jan_14' then bp_systolic end)) systolic_avg_jan_14,
round(avg(CASE WHEN quarter_of_interest = 'apr_14' then bp_systolic end)) systolic_avg_apr_14,
round(avg(CASE WHEN quarter_of_interest = 'jul_14' then bp_systolic end)) systolic_avg_jul_14,
round(avg(CASE WHEN quarter_of_interest = 'oct_14' then bp_systolic end)) systolic_avg_oct_14,
round(avg(CASE WHEN quarter_of_interest = 'jan_15' then bp_systolic end)) systolic_avg_jan_15,
round(avg(CASE WHEN quarter_of_interest = 'apr_15' then bp_systolic end)) systolic_avg_apr_15,
round(avg(CASE WHEN quarter_of_interest = 'jul_15' then bp_systolic end)) systolic_avg_jul_15,
round(avg(CASE WHEN quarter_of_interest = 'oct_15' then bp_systolic end)) systolic_avg_oct_15,
round(avg(CASE WHEN quarter_of_interest = 'jan_16' then bp_systolic end)) systolic_avg_jan_16,
round(avg(CASE WHEN quarter_of_interest = 'apr_16' then bp_systolic end)) systolic_avg_apr_16,
round(avg(CASE WHEN quarter_of_interest = 'jul_16' then bp_systolic end)) systolic_avg_jul_16,
round(avg(CASE WHEN quarter_of_interest = 'oct_16' then bp_systolic end)) systolic_avg_oct_16
FROM esp_mdphnet.smk_diab_systolic_pat_details T1
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity;

-- AVERAGE SBP - Averages By Quarter Stratified By Age Group, Race, and Sex & Zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_systolic WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,T1.pwtf_comm,
round(avg(CASE WHEN quarter_of_interest = 'jan_12' then bp_systolic end)) systolic_avg_jan_12,
round(avg(CASE WHEN quarter_of_interest = 'apr_12' then bp_systolic end)) systolic_avg_apr_12,
round(avg(CASE WHEN quarter_of_interest = 'jul_12' then bp_systolic end)) systolic_avg_jul_12,
round(avg(CASE WHEN quarter_of_interest = 'oct_12' then bp_systolic end)) systolic_avg_oct_12,
round(avg(CASE WHEN quarter_of_interest = 'jan_13' then bp_systolic end)) systolic_avg_jan_13,
round(avg(CASE WHEN quarter_of_interest = 'apr_13' then bp_systolic end)) systolic_avg_apr_13,
round(avg(CASE WHEN quarter_of_interest = 'jul_13' then bp_systolic end)) systolic_avg_jul_13,
round(avg(CASE WHEN quarter_of_interest = 'oct_13' then bp_systolic end)) systolic_avg_oct_13,
round(avg(CASE WHEN quarter_of_interest = 'jan_14' then bp_systolic end)) systolic_avg_jan_14,
round(avg(CASE WHEN quarter_of_interest = 'apr_14' then bp_systolic end)) systolic_avg_apr_14,
round(avg(CASE WHEN quarter_of_interest = 'jul_14' then bp_systolic end)) systolic_avg_jul_14,
round(avg(CASE WHEN quarter_of_interest = 'oct_14' then bp_systolic end)) systolic_avg_oct_14,
round(avg(CASE WHEN quarter_of_interest = 'jan_15' then bp_systolic end)) systolic_avg_jan_15,
round(avg(CASE WHEN quarter_of_interest = 'apr_15' then bp_systolic end)) systolic_avg_apr_15,
round(avg(CASE WHEN quarter_of_interest = 'jul_15' then bp_systolic end)) systolic_avg_jul_15,
round(avg(CASE WHEN quarter_of_interest = 'oct_15' then bp_systolic end)) systolic_avg_oct_15,
round(avg(CASE WHEN quarter_of_interest = 'jan_16' then bp_systolic end)) systolic_avg_jan_16,
round(avg(CASE WHEN quarter_of_interest = 'apr_16' then bp_systolic end)) systolic_avg_apr_16,
round(avg(CASE WHEN quarter_of_interest = 'jul_16' then bp_systolic end)) systolic_avg_jul_16,
round(avg(CASE WHEN quarter_of_interest = 'oct_16' then bp_systolic end)) systolic_avg_oct_16
FROM  esp_mdphnet.smk_diab_systolic_pat_details T1
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'   
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity,T1.pwtf_comm;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 01-01-2012
CREATE TABLE esp_mdphnet.smk_diab_diastolic_012012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'jan_12'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('01-01-2012'::date - interval  '1 years') and date < '01-01-2012'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('01-01-2012'::date - interval  '1 years') and date < '01-01-2012'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 04-01-2012
CREATE TABLE esp_mdphnet.smk_diab_diastolic_042012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'apr_12'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('04-01-2012'::date - interval  '1 years') and date < '04-01-2012'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('04-01-2012'::date - interval  '1 years') and date < '04-01-2012'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 07-01-2012
CREATE TABLE esp_mdphnet.smk_diab_diastolic_072012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'jul_12'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('07-01-2012'::date - interval  '1 years') and date < '07-01-2012'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('07-01-2012'::date - interval  '1 years') and date < '07-01-2012'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 10-01-2012
CREATE TABLE esp_mdphnet.smk_diab_diastolic_102012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'oct_12'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('10-01-2012'::date - interval  '1 years') and date < '10-01-2012'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('10-01-2012'::date - interval  '1 years') and date < '10-01-2012'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 01-01-2013
CREATE TABLE esp_mdphnet.smk_diab_diastolic_012013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'jan_13'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('01-01-2013'::date - interval  '1 years') and date < '01-01-2013'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('01-01-2013'::date - interval  '1 years') and date < '01-01-2013'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 04-01-2013
CREATE TABLE esp_mdphnet.smk_diab_diastolic_042013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'apr_13'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('04-01-2013'::date - interval  '1 years') and date < '04-01-2013'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('04-01-2013'::date - interval  '1 years') and date < '04-01-2013'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 07-01-2013
CREATE TABLE esp_mdphnet.smk_diab_diastolic_072013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'jul_13'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('07-01-2013'::date - interval  '1 years') and date < '07-01-2013'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('07-01-2013'::date - interval  '1 years') and date < '07-01-2013'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 10-01-2013
CREATE TABLE esp_mdphnet.smk_diab_diastolic_102013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'oct_13'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('10-01-2013'::date - interval  '1 years') and date < '10-01-2013'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('10-01-2013'::date - interval  '1 years') and date < '10-01-2013'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 01-01-2014
CREATE TABLE esp_mdphnet.smk_diab_diastolic_012014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'jan_14'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('01-01-2014'::date - interval  '1 years') and date < '01-01-2014'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('01-01-2014'::date - interval  '1 years') and date < '01-01-2014'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 04-01-2014
CREATE TABLE esp_mdphnet.smk_diab_diastolic_042014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'apr_14'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('04-01-2014'::date - interval  '1 years') and date < '04-01-2014'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('04-01-2014'::date - interval  '1 years') and date < '04-01-2014'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 07-01-2014
CREATE TABLE esp_mdphnet.smk_diab_diastolic_072014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'jul_14'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('07-01-2014'::date - interval  '1 years') and date < '07-01-2014'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('07-01-2014'::date - interval  '1 years') and date < '07-01-2014'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 10-01-2014
CREATE TABLE esp_mdphnet.smk_diab_diastolic_102014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'oct_14'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('10-01-2014'::date - interval  '1 years') and date < '10-01-2014'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('10-01-2014'::date - interval  '1 years') and date < '10-01-2014'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 01-01-2015
CREATE TABLE esp_mdphnet.smk_diab_diastolic_012015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'jan_15'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('01-01-2015'::date - interval  '1 years') and date < '01-01-2015'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('01-01-2015'::date - interval  '1 years') and date < '01-01-2015'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 04-01-2015
CREATE TABLE esp_mdphnet.smk_diab_diastolic_042015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'apr_15'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('04-01-2015'::date - interval  '1 years') and date < '04-01-2015'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('04-01-2015'::date - interval  '1 years') and date < '04-01-2015'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 07-01-2015
CREATE TABLE esp_mdphnet.smk_diab_diastolic_072015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'jul_15'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('07-01-2015'::date - interval  '1 years') and date < '07-01-2015'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('07-01-2015'::date - interval  '1 years') and date < '07-01-2015'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 10-01-2015
CREATE TABLE esp_mdphnet.smk_diab_diastolic_102015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'oct_15'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('10-01-2015'::date - interval  '1 years') and date < '10-01-2015'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('10-01-2015'::date - interval  '1 years') and date < '10-01-2015'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 01-01-2016
CREATE TABLE esp_mdphnet.smk_diab_diastolic_012016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'jan_16'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('01-01-2016'::date - interval  '1 years') and date < '01-01-2016'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('01-01-2016'::date - interval  '1 years') and date < '01-01-2016'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 04-01-2016
CREATE TABLE esp_mdphnet.smk_diab_diastolic_042016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'apr_16'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('04-01-2016'::date - interval  '1 years') and date < '04-01-2016'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('04-01-2016'::date - interval  '1 years') and date < '04-01-2016'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 07-01-2016
CREATE TABLE esp_mdphnet.smk_diab_diastolic_072016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'jul_16'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('07-01-2016'::date - interval  '1 years') and date < '07-01-2016'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('07-01-2016'::date - interval  '1 years') and date < '07-01-2016'
ORDER BY T1.patient_id;

-- AVERAGE DBP Most recent DBP date and value in the 1 year prior to the index date - 10-01-2016
CREATE TABLE esp_mdphnet.smk_diab_diastolic_102016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_dbp_date, T1.bp_diastolic, 'oct_16'::text quarter_of_interest
FROM emr_encounter T1,
	(SELECT max(TT1.date) max_dbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date > ('10-01-2016'::date - interval  '1 years') and date < '10-01-2016'
	AND TT1.bp_diastolic is not null		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.date = T2.max_dbp_date
AND T1.bp_diastolic is not null
AND date > ('10-01-2016'::date - interval  '1 years') and date < '10-01-2016'
ORDER BY T1.patient_id;

-- AVERAGE DBP - Union together all the patients and dates (2012-2016)
CREATE  TABLE esp_mdphnet.smk_diab_diastolic_union_full WITHOUT OIDS  AS 
(SELECT * FROM esp_mdphnet.smk_diab_diastolic_012012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_042012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_072012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_102012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_012013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_042013) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_072013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_102013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_012014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_042014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_072014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_102014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_012015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_042015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_072015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_102015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_012016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_042016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_072016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_diastolic_102016);

-- AVERAGE DBP - JOIN WITH ENCOUNTERS TO APPLY CENTER FILTERING
CREATE TABLE esp_mdphnet.smk_diab_diastolic_union_full_w_enc AS
SELECT T1.*
FROM esp_mdphnet.smk_diab_diastolic_union_full T1,
esp_mdphnet.smk_diab_denom_union_full T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = T2.quarter_of_interest;

-- AVERAGE DBP - Determine 10 yr age group & zip
CREATE  TABLE esp_mdphnet.smk_diab_diastolic_agezip WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.natural_key,T1.quarter_of_interest,
CASE   when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' 
end age_group_10_yr, 
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm,
T1.bp_diastolic
FROM esp_mdphnet.smk_diab_diastolic_union_full_w_enc T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) )  AS DUMMYALIAS298 ;

-- AVERAGE DBP - Get race and sex from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_diastolic_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,T1.age_group_10_yr, T2.sex, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity,
T1.bp_diastolic 
FROM esp_mdphnet.smk_diab_diastolic_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 
ON ((T1.natural_key = T2.patid)) ;

-- AVERAGE DBP - Averages By Quarter Stratified By Age Group, Race, and Sex
CREATE  TABLE esp_mdphnet.smk_diab_diastolic_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,
round(avg(CASE WHEN quarter_of_interest = 'jan_12' then bp_diastolic end)) diastolic_avg_jan_12,
round(avg(CASE WHEN quarter_of_interest = 'apr_12' then bp_diastolic end)) diastolic_avg_apr_12,
round(avg(CASE WHEN quarter_of_interest = 'jul_12' then bp_diastolic end)) diastolic_avg_jul_12,
round(avg(CASE WHEN quarter_of_interest = 'oct_12' then bp_diastolic end)) diastolic_avg_oct_12,
round(avg(CASE WHEN quarter_of_interest = 'jan_13' then bp_diastolic end)) diastolic_avg_jan_13,
round(avg(CASE WHEN quarter_of_interest = 'apr_13' then bp_diastolic end)) diastolic_avg_apr_13,
round(avg(CASE WHEN quarter_of_interest = 'jul_13' then bp_diastolic end)) diastolic_avg_jul_13,
round(avg(CASE WHEN quarter_of_interest = 'oct_13' then bp_diastolic end)) diastolic_avg_oct_13,
round(avg(CASE WHEN quarter_of_interest = 'jan_14' then bp_diastolic end)) diastolic_avg_jan_14,
round(avg(CASE WHEN quarter_of_interest = 'apr_14' then bp_diastolic end)) diastolic_avg_apr_14,
round(avg(CASE WHEN quarter_of_interest = 'jul_14' then bp_diastolic end)) diastolic_avg_jul_14,
round(avg(CASE WHEN quarter_of_interest = 'oct_14' then bp_diastolic end)) diastolic_avg_oct_14,
round(avg(CASE WHEN quarter_of_interest = 'jan_15' then bp_diastolic end)) diastolic_avg_jan_15,
round(avg(CASE WHEN quarter_of_interest = 'apr_15' then bp_diastolic end)) diastolic_avg_apr_15,
round(avg(CASE WHEN quarter_of_interest = 'jul_15' then bp_diastolic end)) diastolic_avg_jul_15,
round(avg(CASE WHEN quarter_of_interest = 'oct_15' then bp_diastolic end)) diastolic_avg_oct_15,
round(avg(CASE WHEN quarter_of_interest = 'jan_16' then bp_diastolic end)) diastolic_avg_jan_16,
round(avg(CASE WHEN quarter_of_interest = 'apr_16' then bp_diastolic end)) diastolic_avg_apr_16,
round(avg(CASE WHEN quarter_of_interest = 'jul_16' then bp_diastolic end)) diastolic_avg_jul_16,
round(avg(CASE WHEN quarter_of_interest = 'oct_16' then bp_diastolic end)) diastolic_avg_oct_16
FROM esp_mdphnet.smk_diab_diastolic_pat_details T1
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity;

-- AVERAGE DBP - Averages By Quarter Stratified By Age Group, Race, and Sex & Zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_diastolic WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.sex,T1.race_ethnicity,T1.pwtf_comm,
round(avg(CASE WHEN quarter_of_interest = 'jan_12' then bp_diastolic end)) diastolic_avg_jan_12,
round(avg(CASE WHEN quarter_of_interest = 'apr_12' then bp_diastolic end)) diastolic_avg_apr_12,
round(avg(CASE WHEN quarter_of_interest = 'jul_12' then bp_diastolic end)) diastolic_avg_jul_12,
round(avg(CASE WHEN quarter_of_interest = 'oct_12' then bp_diastolic end)) diastolic_avg_oct_12,
round(avg(CASE WHEN quarter_of_interest = 'jan_13' then bp_diastolic end)) diastolic_avg_jan_13,
round(avg(CASE WHEN quarter_of_interest = 'apr_13' then bp_diastolic end)) diastolic_avg_apr_13,
round(avg(CASE WHEN quarter_of_interest = 'jul_13' then bp_diastolic end)) diastolic_avg_jul_13,
round(avg(CASE WHEN quarter_of_interest = 'oct_13' then bp_diastolic end)) diastolic_avg_oct_13,
round(avg(CASE WHEN quarter_of_interest = 'jan_14' then bp_diastolic end)) diastolic_avg_jan_14,
round(avg(CASE WHEN quarter_of_interest = 'apr_14' then bp_diastolic end)) diastolic_avg_apr_14,
round(avg(CASE WHEN quarter_of_interest = 'jul_14' then bp_diastolic end)) diastolic_avg_jul_14,
round(avg(CASE WHEN quarter_of_interest = 'oct_14' then bp_diastolic end)) diastolic_avg_oct_14,
round(avg(CASE WHEN quarter_of_interest = 'jan_15' then bp_diastolic end)) diastolic_avg_jan_15,
round(avg(CASE WHEN quarter_of_interest = 'apr_15' then bp_diastolic end)) diastolic_avg_apr_15,
round(avg(CASE WHEN quarter_of_interest = 'jul_15' then bp_diastolic end)) diastolic_avg_jul_15,
round(avg(CASE WHEN quarter_of_interest = 'oct_15' then bp_diastolic end)) diastolic_avg_oct_15,
round(avg(CASE WHEN quarter_of_interest = 'jan_16' then bp_diastolic end)) diastolic_avg_jan_16,
round(avg(CASE WHEN quarter_of_interest = 'apr_16' then bp_diastolic end)) diastolic_avg_apr_16,
round(avg(CASE WHEN quarter_of_interest = 'jul_16' then bp_diastolic end)) diastolic_avg_jul_16,
round(avg(CASE WHEN quarter_of_interest = 'oct_16' then bp_diastolic end)) diastolic_avg_oct_16
FROM  esp_mdphnet.smk_diab_diastolic_pat_details T1
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'   
GROUP BY T1.age_group_10_yr,T1.sex,T1.race_ethnicity,T1.pwtf_comm;



-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 01-01-2012
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_012012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jan_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '01-01-2012'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'jan_12';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 04-01-2012
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_042012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'apr_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '04-01-2012'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'apr_12';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 07-01-2012
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_072012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jul_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '07-01-2012'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'jul_12';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 10-01-2012
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_102012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'oct_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '10-01-2012'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'oct_12';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 01-01-2013
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_012013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jan_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '01-01-2013'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'jan_13';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 04-01-2013
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_042013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'apr_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '04-01-2013'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'apr_13';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 07-01-2013
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_072013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jul_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '07-01-2013'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'jul_13';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 10-01-2013
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_102013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'oct_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '10-01-2013'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'oct_13';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 01-01-2014
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_012014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jan_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '01-01-2014'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'jan_14';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 04-01-2014
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_042014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'apr_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '04-01-2014'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'apr_14';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 07-01-2014
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_072014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jul_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '07-01-2014'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'jul_14';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 10-01-2014
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_102014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'oct_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '10-01-2014'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'oct_14';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 01-01-2015
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_012015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jan_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '01-01-2015'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'jan_15';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 04-01-2015
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_042015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'apr_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '04-01-2015'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'apr_15';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 07-01-2015
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_072015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jul_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '07-01-2015'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'jul_15';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 10-01-2015
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_102015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'oct_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '10-01-2015'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'oct_15';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 01-01-2016
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_012016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jan_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '01-01-2016'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'jan_16';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 04-01-2016
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_042016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'apr_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '04-01-2016'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'apr_16';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 07-01-2016
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_072016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'jul_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '07-01-2016'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'jul_16';

-- CONTROLLED HYPERTENSION For Active Hypertension Patients For The Quarter Find Those with a Most recent blood pressure
-- that is normal (but after hypertension case date) - 10-01-2016
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_102016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_cbp_date, T1.hyper_active_date, 'oct_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_cbp_date, TT1.patient_id
	FROM emr_encounter TT1
	WHERE date < '10-01-2016'
	AND TT1.bp_systolic < 140 and TT1.bp_diastolic < 90		
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T2.max_cbp_date > T1.hyper_active_date
AND T1.quarter_of_interest = 'oct_16';

-- CONTROLLED HYPERTENSION - Union together all the patients and dates (2012-2016)
CREATE  TABLE esp_mdphnet.smk_diab_controlled_bp_union_full WITHOUT OIDS  AS 
(SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_012012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_042012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_072012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_102012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_012013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_042013) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_072013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_102013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_012014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_042014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_072014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_102014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_012015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_042015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_072015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_102015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_012016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_042016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_072016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_controlled_bp_102016);

-- CONTROLLED HYPERTENSION - JOIN WITH ENCOUNTERS TO APPLY CENTER FILTERING
CREATE TABLE esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc AS
SELECT T1.*
FROM esp_mdphnet.smk_diab_controlled_bp_union_full T1,
esp_mdphnet.smk_diab_denom_union_full T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = T2.quarter_of_interest;

-- CONTROLLED HYPERTENSION - Determine 10 yr age group & zip
CREATE  TABLE esp_mdphnet.smk_diab_controlled_bp_agezip WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.natural_key,T1.quarter_of_interest,
CASE   when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' 
end age_group_10_yr, 
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) )  AS DUMMYALIAS298 ;

-- CONTROLLED HYPERTENSION - Get race and sex from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_controlled_bp_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,T1.age_group_10_yr, T2.sex, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity
FROM esp_mdphnet.smk_diab_controlled_bp_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 
ON ((T1.natural_key = T2.patid)) ;

-- CONTROLLED HYPERTENSION - Controlled Hypertension Counts By Quarter Stratified By Age Group, Race, and Sex
CREATE  TABLE esp_mdphnet.smk_diab_controlled_bp_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  controlled_hyper_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  controlled_hyper_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  controlled_hyper_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  controlled_hyper_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  controlled_hyper_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  controlled_hyper_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  controlled_hyper_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  controlled_hyper_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  controlled_hyper_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  controlled_hyper_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  controlled_hyper_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  controlled_hyper_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  controlled_hyper_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  controlled_hyper_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  controlled_hyper_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  controlled_hyper_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  controlled_hyper_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  controlled_hyper_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  controlled_hyper_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  controlled_hyper_oct_16    
FROM  esp_mdphnet.smk_diab_controlled_bp_pat_details T1
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'      
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex;

-- CONTROLLED HYPERTENSION - Controlled Hypertension Counts By Quarter Stratified By Age Group, Race, and Sex & Zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_controlled_bp  WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  controlled_hyper_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  controlled_hyper_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  controlled_hyper_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  controlled_hyper_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  controlled_hyper_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  controlled_hyper_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  controlled_hyper_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  controlled_hyper_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  controlled_hyper_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  controlled_hyper_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  controlled_hyper_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  controlled_hyper_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  controlled_hyper_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  controlled_hyper_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  controlled_hyper_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  controlled_hyper_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  controlled_hyper_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  controlled_hyper_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  controlled_hyper_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  controlled_hyper_oct_16    
FROM  esp_mdphnet.smk_diab_controlled_bp_pat_details T1 
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'     
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm;

-- HYPERTENSION ON TREATMENT - Hypertension RX Events
CREATE  TABLE esp_mdphnet.smk_diab_hyper_rx_events  WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.date,T1.name FROM  public.hef_event T1   WHERE date > ('01-01-2012'::date - interval '1 years') and date < '10-01-2016' and 
name in 
('rx:hydrochlorothiazide',
	'rx:chlorthalidone',
	'rx:indapamide',
	'rx:amlodipine',
	'rx:clevidipine',
	'rx:felodipine',
	'rx:isradipine',
	'rx:nicardipine',
	'rx:nifedipine',
	'rx:nisoldipine',
	'rx:diltiazem',
	'rx:verapamil',
	'rx:acebutolol',
	'rx:atenolol',
	'rx:betaxolol',
	'rx:bisoprolol',
	'rx:carvedilol',
	'rx:labetolol',
	'rx:metoprolol',
	'rx:nadolol',
	'rx:nebivolol',
	'rx:pindolol',
	'rx:propranolol',
	'rx:benazepril',
	'rx:catopril',
	'rx:enalapril',
	'rx:fosinopril',
	'rx:lisinopril',
	'rx:moexipril',
	'rx:perindopril',
	'rx:quinapril',
	'rx:ramipril',
	'rx:trandolapril',
	'rx:candesartan',
	'rx:eprosartan',
	'rx:irbesartan',
	'rx:losartan',
	'rx:olmesartan',
	'rx:telmisartan',
	'rx:valsartan',
	'rx:clonidine',
	'rx:doxazosin',
	'rx:guanfacine',
	'rx:methyldopa',
	'rx:prazosin',
	'rx:terazosin',
	'rx:eplerenone',
	'rx:sprinolactone',
	'rx:aliskiren',
	'rx:hydralazine');
	
-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 01-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_012012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'jan_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('01-01-2012'::date - interval  '1 years') and date < '01-01-2012'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'jan_12'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 04-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_042012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'apr_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('04-01-2012'::date - interval  '1 years') and date < '04-01-2012'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'apr_12'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 07-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_072012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'jul_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('07-01-2012'::date - interval  '1 years') and date < '07-01-2012'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'jul_12'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 10-01-2012
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_102012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'oct_12'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('10-01-2012'::date - interval  '1 years') and date < '10-01-2012'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'oct_12'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 01-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_012013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'jan_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('01-01-2013'::date - interval  '1 years') and date < '01-01-2013'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'jan_13'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 04-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_042013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'apr_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('04-01-2013'::date - interval  '1 years') and date < '04-01-2013'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'apr_13'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 07-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_072013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'jul_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('07-01-2013'::date - interval  '1 years') and date < '07-01-2013'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'jul_13'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 10-01-2013
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_102013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'oct_13'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('10-01-2013'::date - interval  '1 years') and date < '10-01-2013'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'oct_13'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 01-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_012014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'jan_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('01-01-2014'::date - interval  '1 years') and date < '01-01-2014'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'jan_14'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 04-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_042014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'apr_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('04-01-2014'::date - interval  '1 years') and date < '04-01-2014'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'apr_14'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 07-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_072014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'jul_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('07-01-2014'::date - interval  '1 years') and date < '07-01-2014'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'jul_14'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 10-01-2014
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_102014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'oct_14'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('10-01-2014'::date - interval  '1 years') and date < '10-01-2014'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'oct_14'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 01-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_012015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'jan_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('01-01-2015'::date - interval  '1 years') and date < '01-01-2015'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'jan_15'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 04-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_042015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'apr_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('04-01-2015'::date - interval  '1 years') and date < '04-01-2015'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'apr_15'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 07-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_072015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'jul_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('07-01-2015'::date - interval  '1 years') and date < '07-01-2015'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'jul_15'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 10-01-2015
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_102015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'oct_15'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('10-01-2015'::date - interval  '1 years') and date < '10-01-2015'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'oct_15'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 01-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_012016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'jan_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('01-01-2016'::date - interval  '1 years') and date < '01-01-2016'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'jan_16'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 04-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_042016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'apr_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('04-01-2016'::date - interval  '1 years') and date < '04-01-2016'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'apr_16'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 07-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_072016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'jul_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('07-01-2016'::date - interval  '1 years') and date < '07-01-2016'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'jul_16'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Active Hypertension & Rx Event 10-01-2016
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_102016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T2.max_treat_date, T1.hyper_active_date, 'oct_16'::text quarter_of_interest
FROM esp_mdphnet.smk_diab_hyper_dateofint T1,
	(SELECT max(TT1.date) max_treat_date, TT1.patient_id
	FROM esp_mdphnet.smk_diab_hyper_rx_events TT1
	WHERE date > ('10-01-2016'::date - interval  '1 years') and date < '10-01-2016'
	GROUP BY TT1.patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = 'oct_16'
AND T2.max_treat_date > T1.hyper_active_date;

-- HYPERTENSION ON TREATMENT - Union together all the patients and dates (2012-2016)
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_union_full WITHOUT OIDS  AS 
(SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_012012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_042012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_072012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_102012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_012013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_042013) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_072013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_102013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_012014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_042014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_072014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_102014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_012015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_042015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_072015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_102015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_012016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_042016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_072016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_hyper_treated_102016);


-- HYPERTENSION ON TREATMENT -  JOIN WITH ENCOUNTERS TO APPLY CENTER FILTERING
CREATE TABLE esp_mdphnet.smk_diab_hyper_treated_union_full_w_enc AS
SELECT T1.*
FROM esp_mdphnet.smk_diab_hyper_treated_union_full T1,
esp_mdphnet.smk_diab_denom_union_full T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = T2.quarter_of_interest;

-- HYPERTENSION ON TREATMENT - Determine 10 yr age group & zip
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_agezip WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.natural_key,T1.quarter_of_interest,
CASE   when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' 
end age_group_10_yr, 
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_hyper_treated_union_full_w_enc T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) )  AS DUMMYALIAS298 ;

-- HYPERTENSION ON TREATMENT - Get race and sex from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,T1.age_group_10_yr, T2.sex, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity
FROM esp_mdphnet.smk_diab_hyper_treated_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 
ON ((T1.natural_key = T2.patid)) ;

-- HYPERTENSION ON TREATMENT - Measured BP Counts By Quarter Stratified By Age Group, Race, and Sex
CREATE  TABLE esp_mdphnet.smk_diab_hyper_treated_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  hypertension_treated_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  hypertension_treated_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  hypertension_treated_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  hypertension_treated_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  hypertension_treated_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  hypertension_treated_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  hypertension_treated_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  hypertension_treated_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  hypertension_treated_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  hypertension_treated_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  hypertension_treated_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  hypertension_treated_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  hypertension_treated_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  hypertension_treated_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  hypertension_treated_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  hypertension_treated_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  hypertension_treated_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  hypertension_treated_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  hypertension_treated_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  hypertension_treated_oct_16    
FROM  esp_mdphnet.smk_diab_hyper_treated_pat_details T1
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'      
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex;

-- HYPERTENSION ON TREATMENT - Measured BP Counts By Quarter Stratified By Age Group, Race, and Sex & Zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_hyper_treated  WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  hypertension_treated_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  hypertension_treated_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  hypertension_treated_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  hypertension_treated_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  hypertension_treated_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  hypertension_treated_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  hypertension_treated_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  hypertension_treated_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  hypertension_treated_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  hypertension_treated_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  hypertension_treated_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  hypertension_treated_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  hypertension_treated_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  hypertension_treated_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  hypertension_treated_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  hypertension_treated_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  hypertension_treated_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  hypertension_treated_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  hypertension_treated_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  hypertension_treated_oct_16    
FROM  esp_mdphnet.smk_diab_hyper_treated_pat_details T1 
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'     
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm;




-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 01-01-2012
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_012012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jan_12'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('01-01-2012'::date - interval  '1 years') and date < '01-01-2012'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 04-01-2012
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_042012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'apr_12'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('04-01-2012'::date - interval  '1 years') and date < '04-01-2012'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 07-01-2012
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_072012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jul_12'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('07-01-2012'::date - interval  '1 years') and date < '07-01-2012'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 10-01-2012
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_102012 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'oct_12'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('10-01-2012'::date - interval  '1 years') and date < '10-01-2012'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 01-01-2013
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_012013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jan_13'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('01-01-2013'::date - interval  '1 years') and date < '01-01-2013'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 04-01-2013
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_042013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'apr_13'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('04-01-2013'::date - interval  '1 years') and date < '04-01-2013'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 07-01-2013
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_072013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jul_13'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('07-01-2013'::date - interval  '1 years') and date < '07-01-2013'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 10-01-2013
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_102013 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'oct_13'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('10-01-2013'::date - interval  '1 years') and date < '10-01-2013'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 01-01-2014
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_012014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jan_14'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('01-01-2014'::date - interval  '1 years') and date < '01-01-2014'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 04-01-2014
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_042014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'apr_14'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('04-01-2014'::date - interval  '1 years') and date < '04-01-2014'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 07-01-2014
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_072014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jul_14'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('07-01-2014'::date - interval  '1 years') and date < '07-01-2014'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 10-01-2014
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_102014 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'oct_14'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('10-01-2014'::date - interval  '1 years') and date < '10-01-2014'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 01-01-2015
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_012015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jan_15'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('01-01-2015'::date - interval  '1 years') and date < '01-01-2015'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 04-01-2015
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_042015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'apr_15'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('04-01-2015'::date - interval  '1 years') and date < '04-01-2015'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 07-01-2015
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_072015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jul_15'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('07-01-2015'::date - interval  '1 years') and date < '07-01-2015'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 10-01-2015
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_102015 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'oct_15'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('10-01-2015'::date - interval  '1 years') and date < '10-01-2015'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 01-01-2016
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_012016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jan_16'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('01-01-2016'::date - interval  '1 years') and date < '01-01-2016'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 04-01-2016
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_042016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'apr_16'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('04-01-2016'::date - interval  '1 years') and date < '04-01-2016'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 07-01-2016
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_072016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'jul_16'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('07-01-2016'::date - interval  '1 years') and date < '07-01-2016'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Encounter where there is a blood pressure value available (any value) 
-- within the year preceding the index date of each quarter 10-01-2016
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_102016 AS
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, T1.bp_systolic, T1.bp_diastolic, 'oct_16'::text quarter_of_interest
FROM emr_encounter T1
WHERE date > ('10-01-2016'::date - interval  '1 years') and date < '10-01-2016'
AND (T1.bp_systolic IS NOT NULL OR T1.bp_diastolic IS NOT NULL);

-- MEASURED BP - Union together all the patients and dates (2012-2016)
CREATE  TABLE esp_mdphnet.smk_diab_bp_measured_union_full WITHOUT OIDS  AS 
(SELECT * FROM esp_mdphnet.smk_diab_bp_measured_012012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_042012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_072012) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_102012)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_012013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_042013) 
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_072013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_102013)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_012014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_042014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_072014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_102014)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_012015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_042015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_072015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_102015)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_012016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_042016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_072016)
UNION (SELECT * FROM esp_mdphnet.smk_diab_bp_measured_102016);


-- MEASURED BP - JOIN WITH ENCOUNTERS TO APPLY CENTER FILTERING
CREATE TABLE esp_mdphnet.smk_diab_bp_measured_union_full_w_enc AS
SELECT T1.*
FROM esp_mdphnet.smk_diab_bp_measured_union_full T1,
esp_mdphnet.smk_diab_denom_union_full T2
WHERE T1.patient_id = T2.patient_id
AND T1.quarter_of_interest = T2.quarter_of_interest;

-- MEASURED BP - Determine 10 yr age group & zip
CREATE  TABLE esp_mdphnet.smk_diab_bp_measured_agezip WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,T2.natural_key,T1.quarter_of_interest,
CASE   when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 9 then '0-9'    
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 19 then '10-19'  
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 29 then '20-29' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 39 then '30-39' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 49 then '40-49'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 59 then '50-59' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 69 then '60-69' 
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth))<= 79 then '70-79'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 89 then '80-89'   
when date_part('year', age((split_part(quarter_of_interest, '_', 1) || '-01-' || split_part(quarter_of_interest, '_', 2))::date, date_of_birth)) <= 99 then '90-99' 
when date_of_birth is null then 'UNKNOWN AGE'
else '100+' 
end age_group_10_yr, 
CASE 
when T2.zip5 in ('02562', '02532', '02559', '02556', '02543', '02540', '02536', '02649', '02635', '02655', '02648', '02668', '02632', '02601', '02630') then 'barnstable'  
when T2.zip5 in ('01267', '01247', '01367', '01343', '01256', '01220', '01237', '01225', '01270', '01026', '01235', '01226', '01224', '01201', '01254', '01240', '01223', '01011', '01008', '01253', '01253', '01238', '01262', '01266', '01236', '01230', '01245', '01255', '01259', '01222', '01257', '01258') then 'berkshire_county'  
when T2.zip5 in ('02120', '02119', '02125', '02121', '02122') then 'boston_ndorchester_roxbury'  
when T2.zip5 in ('01040') then 'holyoke'  
when T2.zip5 in ('01532', '01752', '01749', '01701', '01702') then 'hudson'  
when T2.zip5 in ('01904', '01902', '01905') then 'lynn'  
when T2.zip5 in ('02171', '02169', '02188', '02189', '02190') then 'manet'  
when T2.zip5 in ('02745', '02746', '02740', '02744') then 'new_bedford'  
when T2.zip5 in ('01608', '01610', '01607', '01604', '01603') then 'worcester' 
when T2.zip5 in ('01803', '01805') then 'burlington'
when T2.zip5 in ('01420') then 'fitchburg'
when T2.zip5 in ('02664', '02673', '02675') then 'yarmouth'
when T2.zip5 in ('02045') then 'hull'
when T2.zip5 in ('01453') then 'leominster'
when T2.zip5 in ('02301', '02302', '02303', '02304', '02305') then 'brockton'
when T2.zip5 in ('01947', '01970', '01971') then 'salem'
when T2.zip5 in ('02062') then 'norwood'
else 'not_mapped' end pwtf_comm
FROM esp_mdphnet.smk_diab_bp_measured_union_full_w_enc T1 INNER JOIN public.emr_patient T2 ON ((T1.patient_id = T2.id)) )  AS DUMMYALIAS298 ;

-- MEASURED BP - Get race and sex from mdphnet demographic
CREATE  TABLE esp_mdphnet.smk_diab_bp_measured_pat_details WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.quarter_of_interest,T1.age_group_10_yr, T2.sex, T1.pwtf_comm, 
CASE when T2.race_ethnicity = 6 then 'hispanic'  
when T2.race_ethnicity = 5 then 'white' 
when T2.race_ethnicity = 3 then 'black' 
when T2.race_ethnicity = 2 then 'asian' 
when T2.race_ethnicity = 1 then 'native_american' 
when T2.race_ethnicity = 0 then 'unknown' 
end race_ethnicity
FROM esp_mdphnet.smk_diab_bp_measured_agezip T1 
INNER JOIN esp_mdphnet.esp_demographic T2 
ON ((T1.natural_key = T2.patid)) ;

-- MEASURED BP - Measured BP Counts By Quarter Stratified By Age Group, Race, and Sex
CREATE  TABLE esp_mdphnet.smk_diab_bp_measured_full_strat WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  measured_bp_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  measured_bp_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  measured_bp_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  measured_bp_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  measured_bp_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  measured_bp_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  measured_bp_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  measured_bp_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  measured_bp_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  measured_bp_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  measured_bp_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  measured_bp_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  measured_bp_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  measured_bp_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  measured_bp_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  measured_bp_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  measured_bp_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  measured_bp_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  measured_bp_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  measured_bp_oct_16    
FROM  esp_mdphnet.smk_diab_bp_measured_pat_details T1
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'      
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex;

-- MEASURED BP - Measured BP Counts By Quarter Stratified By Age Group, Race, and Sex & Zip
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_measured_bp  WITHOUT OIDS  AS 
SELECT T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm,
count(case when  quarter_of_interest = 'jan_12' then 1 end)  measured_bp_jan_12,
count(case when  quarter_of_interest = 'apr_12' then 1 end)  measured_bp_apr_12,
count(case when  quarter_of_interest = 'jul_12' then 1 end)  measured_bp_jul_12,
count(case when  quarter_of_interest = 'oct_12' then 1 end)  measured_bp_oct_12,
count(case when  quarter_of_interest = 'jan_13' then 1 end)  measured_bp_jan_13,
count(case when  quarter_of_interest = 'apr_13' then 1 end)  measured_bp_apr_13,
count(case when  quarter_of_interest = 'jul_13' then 1 end)  measured_bp_jul_13,
count(case when  quarter_of_interest = 'oct_13' then 1 end)  measured_bp_oct_13,
count(case when  quarter_of_interest = 'jan_14' then 1 end)  measured_bp_jan_14,
count(case when  quarter_of_interest = 'apr_14' then 1 end)  measured_bp_apr_14,
count(case when  quarter_of_interest = 'jul_14' then 1 end)  measured_bp_jul_14,
count(case when  quarter_of_interest = 'oct_14' then 1 end)  measured_bp_oct_14,
count(case when  quarter_of_interest = 'jan_15' then 1 end)  measured_bp_jan_15,
count(case when  quarter_of_interest = 'apr_15' then 1 end)  measured_bp_apr_15,
count(case when  quarter_of_interest = 'jul_15' then 1 end)  measured_bp_jul_15,
count(case when  quarter_of_interest = 'oct_15' then 1 end)  measured_bp_oct_15,
count(case when  quarter_of_interest = 'jan_16' then 1 end)  measured_bp_jan_16,
count(case when  quarter_of_interest = 'apr_16' then 1 end)  measured_bp_apr_16,
count(case when  quarter_of_interest = 'jul_16' then 1 end)  measured_bp_jul_16,
count(case when  quarter_of_interest = 'oct_16' then 1 end)  measured_bp_oct_16    
FROM  esp_mdphnet.smk_diab_bp_measured_pat_details T1 
WHERE  T1.age_group_10_yr != '0-9' and T1.age_group_10_yr != '10-19'     
GROUP BY T1.age_group_10_yr,T1.race_ethnicity,T1.sex, T1.pwtf_comm;


-- Step 183: SQL - FINAL - Bring together all of the values
CREATE  TABLE esp_mdphnet.smk_diab_all_full_strat_output WITHOUT OIDS  AS SELECT
coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr, b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, dia.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr, smkcess.age_group_10_yr, hyptreat.age_group_10_yr) as age_group_10_year, 
coalesce(s.sex, pd.sex, ad.sex, d.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex, smkcess.sex, hyptreat.sex) as sex,
coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity, smkcess.race_ethnicity, hyptreat.race_ethnicity) as race,
coalesce(smoke_status_rec_jan_12, 0) as smoke_status_rec_jan_12,
coalesce(smoke_status_rec_apr_12, 0) as smoke_status_rec_apr_12,
coalesce(smoke_status_rec_jul_12, 0) as smoke_status_rec_jul_12,
coalesce(smoke_status_rec_oct_12, 0) as smoke_status_rec_oct_12,
coalesce(smoke_status_rec_jan_13, 0) as smoke_status_rec_jan_13,
coalesce(smoke_status_rec_apr_13, 0) as smoke_status_rec_apr_13,
coalesce(smoke_status_rec_jul_13, 0) as smoke_status_rec_jul_13,
coalesce(smoke_status_rec_oct_13, 0) as smoke_status_rec_oct_13,
coalesce(smoke_status_rec_jan_14, 0) as smoke_status_rec_jan_14,
coalesce(smoke_status_rec_apr_14, 0) as smoke_status_rec_apr_14,
coalesce(smoke_status_rec_jul_14, 0) as smoke_status_rec_jul_14,
coalesce(smoke_status_rec_oct_14, 0) as smoke_status_rec_oct_14,
coalesce(smoke_status_rec_jan_15, 0) as smoke_status_rec_jan_15,
coalesce(smoke_status_rec_apr_15, 0) as smoke_status_rec_apr_15,
coalesce(smoke_status_rec_jul_15, 0) as smoke_status_rec_jul_15,
coalesce(smoke_status_rec_oct_15, 0) as smoke_status_rec_oct_15,
coalesce(smoke_status_rec_jan_16, 0) as smoke_status_rec_jan_16,
coalesce(smoke_status_rec_apr_16, 0) as smoke_status_rec_apr_16,
coalesce(smoke_status_rec_jul_16, 0) as smoke_status_rec_jul_16,
coalesce(smoke_status_rec_oct_16, 0) as smoke_status_rec_oct_16,
coalesce (curr_smoke_jan_12, 0 ) curr_smoke_jan_12,
coalesce (curr_smoke_apr_12, 0 ) curr_smoke_apr_12,
coalesce (curr_smoke_jul_12, 0 ) curr_smoke_jul_12,
coalesce (curr_smoke_oct_12, 0 ) curr_smoke_oct_12,
coalesce (curr_smoke_jan_13, 0 ) curr_smoke_jan_13,
coalesce (curr_smoke_apr_13, 0 ) curr_smoke_apr_13,
coalesce (curr_smoke_jul_13, 0 ) curr_smoke_jul_13,
coalesce (curr_smoke_oct_13, 0 ) curr_smoke_oct_13,
coalesce (curr_smoke_jan_14, 0 ) curr_smoke_jan_14,
coalesce (curr_smoke_apr_14, 0 ) curr_smoke_apr_14,
coalesce (curr_smoke_jul_14, 0 ) curr_smoke_jul_14,
coalesce (curr_smoke_oct_14, 0 ) curr_smoke_oct_14,
coalesce (curr_smoke_jan_15, 0 ) curr_smoke_jan_15,
coalesce (curr_smoke_apr_15, 0 ) curr_smoke_apr_15,
coalesce (curr_smoke_jul_15, 0 ) curr_smoke_jul_15,
coalesce (curr_smoke_oct_15, 0 ) curr_smoke_oct_15,
coalesce (curr_smoke_jan_16, 0 ) curr_smoke_jan_16,
coalesce (curr_smoke_apr_16, 0) curr_smoke_apr_16,
coalesce (curr_smoke_jul_16, 0) curr_smoke_jul_16,
coalesce (curr_smoke_oct_16, 0) curr_smoke_oct_16,
coalesce(smoke_still_seen_base_denom_jan_14, 0) as smoke_still_seen_base_denom_jan_14,
coalesce(smoke_still_seen_apr_14, 0) as smoke_still_seen_apr_14,
coalesce(smoke_still_seen_jul_14, 0) as smoke_still_seen_jul_14,
coalesce(smoke_still_seen_oct_14, 0) as smoke_still_seen_oct_14,
coalesce(smoke_still_seen_jan_15, 0) as smoke_still_seen_jan_15,
coalesce(smoke_still_seen_apr_15, 0) as smoke_still_seen_apr_15,
coalesce(smoke_still_seen_jul_15, 0) as smoke_still_seen_jul_15,
coalesce(smoke_still_seen_oct_15, 0) as smoke_still_seen_oct_15,
coalesce(smoke_still_seen_jan_16, 0) as smoke_still_seen_jan_16,
coalesce(smoke_still_seen_apr_16, 0) as smoke_still_seen_apr_16,
coalesce(smoke_still_seen_jul_16, 0) as smoke_still_seen_jul_16,
coalesce(smoke_still_seen_oct_16, 0) as smoke_still_seen_oct_16,
coalesce(smoke_cessation_apr_14, 0) as smoke_cessation_apr_14,
coalesce(smoke_cessation_jul_14, 0) as smoke_cessation_jul_14,
coalesce(smoke_cessation_oct_14, 0) as smoke_cessation_oct_14,
coalesce(smoke_cessation_jan_15, 0) as smoke_cessation_jan_15,
coalesce(smoke_cessation_apr_15, 0) as smoke_cessation_apr_15,
coalesce(smoke_cessation_jul_15, 0) as smoke_cessation_jul_15,
coalesce(smoke_cessation_oct_15, 0) as smoke_cessation_oct_15,
coalesce(smoke_cessation_jan_16, 0) as smoke_cessation_jan_16,
coalesce(smoke_cessation_apr_16, 0) as smoke_cessation_apr_16,
coalesce(smoke_cessation_jul_16, 0) as smoke_cessation_jul_16,
coalesce(smoke_cessation_oct_16, 0) as smoke_cessation_oct_16,
coalesce (t1_diab_prev_jan_12, 0 ) as t1_diab_prev_jan_12,
coalesce (t1_diab_prev_apr_12, 0 ) as t1_diab_prev_apr_12,
coalesce (t1_diab_prev_jul_12, 0 ) as t1_diab_prev_jul_12,
coalesce (t1_diab_prev_oct_12, 0 ) as t1_diab_prev_oct_12,
coalesce (t1_diab_prev_jan_13, 0 ) as t1_diab_prev_jan_13,
coalesce (t1_diab_prev_apr_13, 0 ) as t1_diab_prev_apr_13,
coalesce (t1_diab_prev_jul_13, 0 ) as t1_diab_prev_jul_13,
coalesce (t1_diab_prev_oct_13, 0 ) as t1_diab_prev_oct_13,
coalesce (t1_diab_prev_jan_14, 0 ) as t1_diab_prev_jan_14,
coalesce (t1_diab_prev_apr_14, 0 ) as t1_diab_prev_apr_14,
coalesce (t1_diab_prev_jul_14, 0 ) as t1_diab_prev_jul_14,
coalesce (t1_diab_prev_oct_14, 0 ) as t1_diab_prev_oct_14,
coalesce (t1_diab_prev_jan_15, 0 ) as t1_diab_prev_jan_15,
coalesce (t1_diab_prev_apr_15, 0 ) as t1_diab_prev_apr_15,
coalesce (t1_diab_prev_jul_15, 0 ) as t1_diab_prev_jul_15,
coalesce (t1_diab_prev_oct_15, 0 ) as t1_diab_prev_oct_15,
coalesce (t1_diab_prev_jan_16, 0 ) as t1_diab_prev_jan_16,
coalesce (t1_diab_prev_apr_16, 0 ) as t1_diab_prev_apr_16,
coalesce (t1_diab_prev_jul_16, 0 ) as t1_diab_prev_jul_16,
coalesce (t1_diab_prev_oct_16, 0 ) as t1_diab_prev_oct_16,
coalesce (act_t1_diab_jan_12, 0 ) as act_t1_diab_jan_12,
coalesce (act_t1_diab_apr_12, 0 ) as act_t1_diab_apr_12,
coalesce (act_t1_diab_jul_12, 0 ) as act_t1_diab_jul_12,
coalesce (act_t1_diab_oct_12, 0 ) as act_t1_diab_oct_12,
coalesce (act_t1_diab_jan_13, 0 ) as act_t1_diab_jan_13,
coalesce (act_t1_diab_apr_13, 0 ) as act_t1_diab_apr_13,
coalesce (act_t1_diab_jul_13, 0 ) as act_t1_diab_jul_13,
coalesce (act_t1_diab_oct_13, 0 ) as act_t1_diab_oct_13,
coalesce (act_t1_diab_jan_14, 0 ) as act_t1_diab_jan_14,
coalesce (act_t1_diab_apr_14, 0 ) as act_t1_diab_apr_14,
coalesce (act_t1_diab_jul_14, 0 ) as act_t1_diab_jul_14,
coalesce (act_t1_diab_oct_14, 0 ) as act_t1_diab_oct_14,
coalesce (act_t1_diab_jan_15, 0 ) as act_t1_diab_jan_15,
coalesce (act_t1_diab_apr_15, 0 ) as act_t1_diab_apr_15,
coalesce (act_t1_diab_jul_15, 0 ) as act_t1_diab_jul_15,
coalesce (act_t1_diab_oct_15, 0 ) as act_t1_diab_oct_15,
coalesce (act_t1_diab_jan_16, 0 ) as act_t1_diab_jan_16,
coalesce (act_t1_diab_apr_16, 0 ) as act_t1_diab_apr_16,
coalesce (act_t1_diab_jul_16, 0 ) as act_t1_diab_jul_16,
coalesce (act_t1_diab_oct_16, 0 ) as act_t1_diab_oct_16,
coalesce (t2_diab_prev_jan_12, 0 ) as t2_diab_prev_jan_12,
coalesce (t2_diab_prev_apr_12, 0 ) as t2_diab_prev_apr_12,
coalesce (t2_diab_prev_jul_12, 0 ) as t2_diab_prev_jul_12,
coalesce (t2_diab_prev_oct_12, 0 ) as t2_diab_prev_oct_12,
coalesce (t2_diab_prev_jan_13, 0 ) as t2_diab_prev_jan_13,
coalesce (t2_diab_prev_apr_13, 0 ) as t2_diab_prev_apr_13,
coalesce (t2_diab_prev_jul_13, 0 ) as t2_diab_prev_jul_13,
coalesce (t2_diab_prev_oct_13, 0 ) as t2_diab_prev_oct_13,
coalesce (t2_diab_prev_jan_14, 0 ) as t2_diab_prev_jan_14,
coalesce (t2_diab_prev_apr_14, 0 ) as t2_diab_prev_apr_14,
coalesce (t2_diab_prev_jul_14, 0 ) as t2_diab_prev_jul_14,
coalesce (t2_diab_prev_oct_14, 0 ) as t2_diab_prev_oct_14,
coalesce (t2_diab_prev_jan_15, 0 ) as t2_diab_prev_jan_15,
coalesce (t2_diab_prev_apr_15, 0 ) as t2_diab_prev_apr_15,
coalesce (t2_diab_prev_jul_15, 0 ) as t2_diab_prev_jul_15,
coalesce (t2_diab_prev_oct_15, 0 ) as t2_diab_prev_oct_15,
coalesce (t2_diab_prev_jan_16, 0 ) as t2_diab_prev_jan_16,
coalesce (t2_diab_prev_apr_16, 0 ) as t2_diab_prev_apr_16,
coalesce (t2_diab_prev_jul_16, 0 ) as t2_diab_prev_jul_16,
coalesce (t2_diab_prev_oct_16, 0 ) as t2_diab_prev_oct_16,
coalesce (act_t2_diab_jan_12, 0 ) as act_t2_diab_jan_12,
coalesce (act_t2_diab_apr_12, 0 ) as act_t2_diab_apr_12,
coalesce (act_t2_diab_jul_12, 0 ) as act_t2_diab_jul_12,
coalesce (act_t2_diab_oct_12, 0 ) as act_t2_diab_oct_12,
coalesce (act_t2_diab_jan_13, 0 ) as act_t2_diab_jan_13,
coalesce (act_t2_diab_apr_13, 0 ) as act_t2_diab_apr_13,
coalesce (act_t2_diab_jul_13, 0 ) as act_t2_diab_jul_13,
coalesce (act_t2_diab_oct_13, 0 ) as act_t2_diab_oct_13,
coalesce (act_t2_diab_jan_14, 0 ) as act_t2_diab_jan_14,
coalesce (act_t2_diab_apr_14, 0 ) as act_t2_diab_apr_14,
coalesce (act_t2_diab_jul_14, 0 ) as act_t2_diab_jul_14,
coalesce (act_t2_diab_oct_14, 0 ) as act_t2_diab_oct_14,
coalesce (act_t2_diab_jan_15, 0 ) as act_t2_diab_jan_15,
coalesce (act_t2_diab_apr_15, 0 ) as act_t2_diab_apr_15,
coalesce (act_t2_diab_jul_15, 0 ) as act_t2_diab_jul_15,
coalesce (act_t2_diab_oct_15, 0 ) as act_t2_diab_oct_15,
coalesce (act_t2_diab_jan_16, 0 ) as act_t2_diab_jan_16,
coalesce (act_t2_diab_apr_16, 0 ) as act_t2_diab_apr_16,
coalesce (act_t2_diab_jul_16, 0 ) as act_t2_diab_jul_16,
coalesce (act_t2_diab_oct_16, 0 ) as act_t2_diab_oct_16,
coalesce (t1_or_t2_diab_prev_jan_12, 0 ) as t1_or_t2_diab_prev_jan_12,
coalesce (t1_or_t2_diab_prev_apr_12, 0 ) as t1_or_t2_diab_prev_apr_12,
coalesce (t1_or_t2_diab_prev_jul_12, 0 ) as t1_or_t2_diab_prev_jul_12,
coalesce (t1_or_t2_diab_prev_oct_12, 0 ) as t1_or_t2_diab_prev_oct_12,
coalesce (t1_or_t2_diab_prev_jan_13, 0 ) as t1_or_t2_diab_prev_jan_13,
coalesce (t1_or_t2_diab_prev_apr_13, 0 ) as t1_or_t2_diab_prev_apr_13,
coalesce (t1_or_t2_diab_prev_jul_13, 0 ) as t1_or_t2_diab_prev_jul_13,
coalesce (t1_or_t2_diab_prev_oct_13, 0 ) as t1_or_t2_diab_prev_oct_13,
coalesce (t1_or_t2_diab_prev_jan_14, 0 ) as t1_or_t2_diab_prev_jan_14,
coalesce (t1_or_t2_diab_prev_apr_14, 0 ) as t1_or_t2_diab_prev_apr_14,
coalesce (t1_or_t2_diab_prev_jul_14, 0 ) as t1_or_t2_diab_prev_jul_14,
coalesce (t1_or_t2_diab_prev_oct_14, 0 ) as t1_or_t2_diab_prev_oct_14,
coalesce (t1_or_t2_diab_prev_jan_15, 0 ) as t1_or_t2_diab_prev_jan_15,
coalesce (t1_or_t2_diab_prev_apr_15, 0 ) as t1_or_t2_diab_prev_apr_15,
coalesce (t1_or_t2_diab_prev_jul_15, 0 ) as t1_or_t2_diab_prev_jul_15,
coalesce (t1_or_t2_diab_prev_oct_15, 0 ) as t1_or_t2_diab_prev_oct_15,
coalesce (t1_or_t2_diab_prev_jan_16, 0 ) as t1_or_t2_diab_prev_jan_16,
coalesce (t1_or_t2_diab_prev_apr_16, 0 ) as t1_or_t2_diab_prev_apr_16,
coalesce (t1_or_t2_diab_prev_jul_16, 0 ) as t1_or_t2_diab_prev_jul_16,
coalesce (t1_or_t2_diab_prev_oct_16, 0 ) as t1_or_t2_diab_prev_oct_16,
coalesce (act_t1_or_t2_diab_jan_12, 0 ) as act_t1_or_t2_diab_jan_12,
coalesce (act_t1_or_t2_diab_apr_12, 0 ) as act_t1_or_t2_diab_apr_12,
coalesce (act_t1_or_t2_diab_jul_12, 0 ) as act_t1_or_t2_diab_jul_12,
coalesce (act_t1_or_t2_diab_oct_12, 0 ) as act_t1_or_t2_diab_oct_12,
coalesce (act_t1_or_t2_diab_jan_13, 0 ) as act_t1_or_t2_diab_jan_13,
coalesce (act_t1_or_t2_diab_apr_13, 0 ) as act_t1_or_t2_diab_apr_13,
coalesce (act_t1_or_t2_diab_jul_13, 0 ) as act_t1_or_t2_diab_jul_13,
coalesce (act_t1_or_t2_diab_oct_13, 0 ) as act_t1_or_t2_diab_oct_13,
coalesce (act_t1_or_t2_diab_jan_14, 0 ) as act_t1_or_t2_diab_jan_14,
coalesce (act_t1_or_t2_diab_apr_14, 0 ) as act_t1_or_t2_diab_apr_14,
coalesce (act_t1_or_t2_diab_jul_14, 0 ) as act_t1_or_t2_diab_jul_14,
coalesce (act_t1_or_t2_diab_oct_14, 0 ) as act_t1_or_t2_diab_oct_14,
coalesce (act_t1_or_t2_diab_jan_15, 0 ) as act_t1_or_t2_diab_jan_15,
coalesce (act_t1_or_t2_diab_apr_15, 0 ) as act_t1_or_t2_diab_apr_15,
coalesce (act_t1_or_t2_diab_jul_15, 0 ) as act_t1_or_t2_diab_jul_15,
coalesce (act_t1_or_t2_diab_oct_15, 0 ) as act_t1_or_t2_diab_oct_15,
coalesce (act_t1_or_t2_diab_jan_16, 0 ) as act_t1_or_t2_diab_jan_16,
coalesce (act_t1_or_t2_diab_apr_16, 0 ) as act_t1_or_t2_diab_apr_16,
coalesce (act_t1_or_t2_diab_jul_16, 0 ) as act_t1_or_t2_diab_jul_16,
coalesce (act_t1_or_t2_diab_oct_16, 0 ) as act_t1_or_t2_diab_oct_16,
coalesce (asthma_jan_12, 0 ) as asthma_jan_12,
coalesce (asthma_apr_12, 0 ) as asthma_apr_12,
coalesce (asthma_jul_12, 0 ) as asthma_jul_12,
coalesce (asthma_oct_12, 0 ) as asthma_oct_12,
coalesce (asthma_jan_13, 0 ) as asthma_jan_13,
coalesce (asthma_apr_13, 0 ) as asthma_apr_13,
coalesce (asthma_jul_13, 0 ) as asthma_jul_13,
coalesce (asthma_oct_13, 0 ) as asthma_oct_13,
coalesce (asthma_jan_14, 0 ) as asthma_jan_14,
coalesce (asthma_apr_14, 0 ) as asthma_apr_14,
coalesce (asthma_jul_14, 0 ) as asthma_jul_14,
coalesce (asthma_oct_14, 0 ) as asthma_oct_14,
coalesce (asthma_jan_15, 0 ) as asthma_jan_15,
coalesce (asthma_apr_15, 0 ) as asthma_apr_15,
coalesce (asthma_jul_15, 0 ) as asthma_jul_15,
coalesce (asthma_oct_15, 0 ) as asthma_oct_15,
coalesce (asthma_jan_16, 0 ) as asthma_jan_16,
coalesce (asthma_apr_16, 0 ) as asthma_apr_16,
coalesce (asthma_jul_16, 0 ) as asthma_jul_16,
coalesce (asthma_oct_16, 0 ) as asthma_oct_16,
coalesce (denominator_jan_12, 0 ) as denominator_jan_12,
coalesce (denominator_apr_12, 0 ) as denominator_apr_12,
coalesce (denominator_jul_12, 0 ) as denominator_jul_12,
coalesce (denominator_oct_12, 0 ) as denominator_oct_12,
coalesce (denominator_jan_13, 0 ) as denominator_jan_13,
coalesce (denominator_apr_13, 0 ) as denominator_apr_13,
coalesce (denominator_jul_13, 0 ) as denominator_jul_13,
coalesce (denominator_oct_13, 0 ) as denominator_oct_13,
coalesce (denominator_jan_14, 0 ) as denominator_jan_14,
coalesce (denominator_apr_14, 0 ) as denominator_apr_14,
coalesce (denominator_jul_14, 0 ) as denominator_jul_14,
coalesce (denominator_oct_14, 0 ) as denominator_oct_14,
coalesce (denominator_jan_15, 0 ) as denominator_jan_15,
coalesce (denominator_apr_15, 0 ) as denominator_apr_15,
coalesce (denominator_jul_15, 0 ) as denominator_jul_15,
coalesce (denominator_oct_15, 0 ) as denominator_oct_15,
coalesce (denominator_jan_16, 0 ) as denominator_jan_16,
coalesce (denominator_apr_16, 0 ) as denominator_apr_16,
coalesce (denominator_jul_16, 0 ) as denominator_jul_16,
coalesce (denominator_oct_16, 0 ) as denominator_oct_16,
coalesce(overweight_jan_12, 0)  as overweight_jan_12,
coalesce(overweight_apr_12, 0)  as overweight_apr_12,
coalesce(overweight_jul_12, 0)  as overweight_jul_12,
coalesce(overweight_oct_12, 0)  as overweight_oct_12,
coalesce(overweight_jan_13, 0)  as overweight_jan_13,
coalesce(overweight_apr_13, 0)  as overweight_apr_13,
coalesce(overweight_jul_13, 0)  as overweight_jul_13,
coalesce(overweight_oct_13, 0)  as overweight_oct_13,
coalesce(overweight_jan_14, 0)  as overweight_jan_14,
coalesce(overweight_apr_14, 0)  as overweight_apr_14,
coalesce(overweight_jul_14, 0)  as overweight_jul_14,
coalesce(overweight_oct_14, 0)  as overweight_oct_14,
coalesce(overweight_jan_15, 0)  as overweight_jan_15,
coalesce(overweight_apr_15, 0)  as overweight_apr_15,
coalesce(overweight_jul_15, 0)  as overweight_jul_15,
coalesce(overweight_oct_15, 0)  as overweight_oct_15,
coalesce(overweight_jan_16, 0)  as overweight_jan_16,
coalesce(overweight_apr_16, 0)  as overweight_apr_16,
coalesce(overweight_jul_16, 0)  as overweight_jul_16,
coalesce(overweight_oct_16, 0)  as overweight_oct_16,
coalesce(obese_jan_12, 0)  as obese_jan_12,
coalesce(obese_apr_12, 0)  as obese_apr_12,
coalesce(obese_jul_12, 0)  as obese_jul_12,
coalesce(obese_oct_12, 0)  as obese_oct_12,
coalesce(obese_jan_13, 0)  as obese_jan_13,
coalesce(obese_apr_13, 0)  as obese_apr_13,
coalesce(obese_jul_13, 0)  as obese_jul_13,
coalesce(obese_oct_13, 0)  as obese_oct_13,
coalesce(obese_jan_14, 0)  as obese_jan_14,
coalesce(obese_apr_14, 0)  as obese_apr_14,
coalesce(obese_jul_14, 0)  as obese_jul_14,
coalesce(obese_oct_14, 0)  as obese_oct_14,
coalesce(obese_jan_15, 0)  as obese_jan_15,
coalesce(obese_apr_15, 0)  as obese_apr_15,
coalesce(obese_jul_15, 0)  as obese_jul_15,
coalesce(obese_oct_15, 0)  as obese_oct_15,
coalesce(obese_jan_16, 0)  as obese_jan_16,
coalesce(obese_apr_16, 0)  as obese_apr_16,
coalesce(obese_jul_16, 0)  as obese_jul_16,
coalesce(obese_oct_16, 0)  as obese_oct_16,
coalesce(overweight_or_obese_jan_12, 0)  as overweight_or_obese_jan_12,
coalesce(overweight_or_obese_apr_12, 0)  as overweight_or_obese_apr_12,
coalesce(overweight_or_obese_jul_12, 0)  as overweight_or_obese_jul_12,
coalesce(overweight_or_obese_oct_12, 0)  as overweight_or_obese_oct_12,
coalesce(overweight_or_obese_jan_13, 0)  as overweight_or_obese_jan_13,
coalesce(overweight_or_obese_apr_13, 0)  as overweight_or_obese_apr_13,
coalesce(overweight_or_obese_jul_13, 0)  as overweight_or_obese_jul_13,
coalesce(overweight_or_obese_oct_13, 0)  as overweight_or_obese_oct_13,
coalesce(overweight_or_obese_jan_14, 0)  as overweight_or_obese_jan_14,
coalesce(overweight_or_obese_apr_14, 0)  as overweight_or_obese_apr_14,
coalesce(overweight_or_obese_jul_14, 0)  as overweight_or_obese_jul_14,
coalesce(overweight_or_obese_oct_14, 0)  as overweight_or_obese_oct_14,
coalesce(overweight_or_obese_jan_15, 0)  as overweight_or_obese_jan_15,
coalesce(overweight_or_obese_apr_15, 0)  as overweight_or_obese_apr_15,
coalesce(overweight_or_obese_jul_15, 0)  as overweight_or_obese_jul_15,
coalesce(overweight_or_obese_oct_15, 0)  as overweight_or_obese_oct_15,
coalesce(overweight_or_obese_jan_16, 0)  as overweight_or_obese_jan_16,
coalesce(overweight_or_obese_apr_16, 0)  as overweight_or_obese_apr_16,
coalesce(overweight_or_obese_jul_16, 0)  as overweight_or_obese_jul_16,
coalesce(overweight_or_obese_oct_16, 0)  as overweight_or_obese_oct_16,
coalesce(no_measured_bmi_jan_12, 0 )  as   no_measured_bmi_jan_12,
coalesce(no_measured_bmi_apr_12, 0 )  as   no_measured_bmi_apr_12,
coalesce(no_measured_bmi_jul_12, 0 )  as   no_measured_bmi_jul_12,
coalesce(no_measured_bmi_oct_12, 0 )  as   no_measured_bmi_oct_12,
coalesce(no_measured_bmi_jan_13, 0 )  as   no_measured_bmi_jan_13,
coalesce(no_measured_bmi_apr_13, 0 )  as   no_measured_bmi_apr_13,
coalesce(no_measured_bmi_jul_13, 0 )  as   no_measured_bmi_jul_13,
coalesce(no_measured_bmi_oct_13, 0 )  as   no_measured_bmi_oct_13,
coalesce(no_measured_bmi_jan_14, 0 )  as   no_measured_bmi_jan_14,
coalesce(no_measured_bmi_apr_14, 0 )  as   no_measured_bmi_apr_14,
coalesce(no_measured_bmi_jul_14, 0 )  as   no_measured_bmi_jul_14,
coalesce(no_measured_bmi_oct_14, 0 )  as   no_measured_bmi_oct_14,
coalesce(no_measured_bmi_jan_15, 0 )  as   no_measured_bmi_jan_15,
coalesce(no_measured_bmi_apr_15, 0 )  as   no_measured_bmi_apr_15,
coalesce(no_measured_bmi_jul_15, 0 )  as   no_measured_bmi_jul_15,
coalesce(no_measured_bmi_oct_15, 0 )  as   no_measured_bmi_oct_15,
coalesce(no_measured_bmi_jan_16, 0 )  as   no_measured_bmi_jan_16,
coalesce(no_measured_bmi_apr_16, 0 )  as   no_measured_bmi_apr_16,
coalesce(no_measured_bmi_jul_16, 0 )  as   no_measured_bmi_jul_16,
coalesce(no_measured_bmi_oct_16, 0 )  as   no_measured_bmi_oct_16,
coalesce( denominator_jan_12, 0)  as  bmi_denom_jan_12,
coalesce( denominator_apr_12, 0)  as  bmi_denom_apr_12,
coalesce( denominator_jul_12, 0)  as  bmi_denom_jul_12,
coalesce( denominator_oct_12, 0)  as  bmi_denom_oct_12,
coalesce( denominator_jan_13, 0)  as  bmi_denom_jan_13,
coalesce( denominator_apr_13, 0)  as  bmi_denom_apr_13,
coalesce( denominator_jul_13, 0)  as  bmi_denom_jul_13,
coalesce( denominator_oct_13, 0)  as  bmi_denom_oct_13,
coalesce( denominator_jan_14, 0)  as  bmi_denom_jan_14,
coalesce( denominator_apr_14, 0)  as  bmi_denom_apr_14,
coalesce( denominator_jul_14, 0)  as  bmi_denom_jul_14,
coalesce( denominator_oct_14, 0)  as  bmi_denom_oct_14,
coalesce( denominator_jan_15, 0)  as  bmi_denom_jan_15,
coalesce( denominator_apr_15, 0)  as  bmi_denom_apr_15,
coalesce( denominator_jul_15, 0)  as  bmi_denom_jul_15,
coalesce( denominator_oct_15, 0)  as  bmi_denom_oct_15,
coalesce( denominator_jan_16, 0)  as  bmi_denom_jan_16,
coalesce( denominator_apr_16, 0)  as  bmi_denom_apr_16,
coalesce( denominator_jul_16, 0)  as  bmi_denom_jul_16,
coalesce( denominator_oct_16, 0)  as  bmi_denom_oct_16,
coalesce(hypertension_jan_12, 0 )  as   hypertension_jan_12,
coalesce(hypertension_apr_12, 0 )  as   hypertension_apr_12,
coalesce(hypertension_jul_12, 0 )  as   hypertension_jul_12,
coalesce(hypertension_oct_12, 0 )  as   hypertension_oct_12,
coalesce(hypertension_jan_13, 0 )  as   hypertension_jan_13,
coalesce(hypertension_apr_13, 0 )  as   hypertension_apr_13,
coalesce(hypertension_jul_13, 0 )  as   hypertension_jul_13,
coalesce(hypertension_oct_13, 0 )  as   hypertension_oct_13,
coalesce(hypertension_jan_14, 0 )  as   hypertension_jan_14,
coalesce(hypertension_apr_14, 0 )  as   hypertension_apr_14,
coalesce(hypertension_jul_14, 0 )  as   hypertension_jul_14,
coalesce(hypertension_oct_14, 0 )  as   hypertension_oct_14,
coalesce(hypertension_jan_15, 0 )  as   hypertension_jan_15,
coalesce(hypertension_apr_15, 0 )  as   hypertension_apr_15,
coalesce(hypertension_jul_15, 0 )  as   hypertension_jul_15,
coalesce(hypertension_oct_15, 0 )  as   hypertension_oct_15,
coalesce(hypertension_jan_16, 0 )  as   hypertension_jan_16,
coalesce(hypertension_apr_16, 0 )  as   hypertension_apr_16,
coalesce(hypertension_jul_16, 0 )  as   hypertension_jul_16,
coalesce(hypertension_oct_16, 0 )  as   hypertension_oct_16,
coalesce(systolic_avg_jan_12, 0) as systolic_avg_jan_12, 
coalesce(systolic_avg_apr_12, 0) as systolic_avg_apr_12, 
coalesce(systolic_avg_jul_12, 0) as systolic_avg_jul_12, 
coalesce(systolic_avg_oct_12, 0) as systolic_avg_oct_12,
coalesce(systolic_avg_jan_13, 0) as systolic_avg_jan_13, 
coalesce(systolic_avg_apr_13, 0) as systolic_avg_apr_13, 
coalesce(systolic_avg_jul_13, 0) as systolic_avg_jul_13, 
coalesce(systolic_avg_oct_13, 0) as systolic_avg_oct_13,
coalesce(systolic_avg_jan_14, 0) as systolic_avg_jan_14, 
coalesce(systolic_avg_apr_14, 0) as systolic_avg_apr_14, 
coalesce(systolic_avg_jul_14, 0) as systolic_avg_jul_14, 
coalesce(systolic_avg_oct_14, 0) as systolic_avg_oct_14,
coalesce(systolic_avg_jan_15, 0) as systolic_avg_jan_15, 
coalesce(systolic_avg_apr_15, 0) as systolic_avg_apr_15, 
coalesce(systolic_avg_jul_15, 0) as systolic_avg_jul_15, 
coalesce(systolic_avg_oct_15, 0) as systolic_avg_oct_15,
coalesce(systolic_avg_jan_16, 0) as systolic_avg_jan_16, 
coalesce(systolic_avg_apr_16, 0) as systolic_avg_apr_16, 
coalesce(systolic_avg_jul_16, 0) as systolic_avg_jul_16, 
coalesce(systolic_avg_oct_16, 0) as systolic_avg_oct_16,
coalesce(diastolic_avg_jan_12, 0) as diastolic_avg_jan_12, 
coalesce(diastolic_avg_apr_12, 0) as diastolic_avg_apr_12, 
coalesce(diastolic_avg_jul_12, 0) as diastolic_avg_jul_12, 
coalesce(diastolic_avg_oct_12, 0) as diastolic_avg_oct_12,
coalesce(diastolic_avg_jan_13, 0) as diastolic_avg_jan_13, 
coalesce(diastolic_avg_apr_13, 0) as diastolic_avg_apr_13, 
coalesce(diastolic_avg_jul_13, 0) as diastolic_avg_jul_13, 
coalesce(diastolic_avg_oct_13, 0) as diastolic_avg_oct_13,
coalesce(diastolic_avg_jan_14, 0) as diastolic_avg_jan_14, 
coalesce(diastolic_avg_apr_14, 0) as diastolic_avg_apr_14, 
coalesce(diastolic_avg_jul_14, 0) as diastolic_avg_jul_14, 
coalesce(diastolic_avg_oct_14, 0) as diastolic_avg_oct_14,
coalesce(diastolic_avg_jan_15, 0) as diastolic_avg_jan_15, 
coalesce(diastolic_avg_apr_15, 0) as diastolic_avg_apr_15, 
coalesce(diastolic_avg_jul_15, 0) as diastolic_avg_jul_15, 
coalesce(diastolic_avg_oct_15, 0) as diastolic_avg_oct_15,
coalesce(diastolic_avg_jan_16, 0) as diastolic_avg_jan_16, 
coalesce(diastolic_avg_apr_16, 0) as diastolic_avg_apr_16, 
coalesce(diastolic_avg_jul_16, 0) as diastolic_avg_jul_16, 
coalesce(diastolic_avg_oct_16, 0) as diastolic_avg_oct_16,
coalesce(controlled_hyper_jan_12, 0) as controlled_hyper_jan_12,
coalesce(controlled_hyper_apr_12, 0) as controlled_hyper_apr_12,
coalesce(controlled_hyper_jul_12, 0) as controlled_hyper_jul_12,
coalesce(controlled_hyper_oct_12, 0) as controlled_hyper_oct_12,
coalesce(controlled_hyper_jan_13, 0) as controlled_hyper_jan_13,
coalesce(controlled_hyper_apr_13, 0) as controlled_hyper_apr_13,
coalesce(controlled_hyper_jul_13, 0) as controlled_hyper_jul_13,
coalesce(controlled_hyper_oct_13, 0) as controlled_hyper_oct_13,
coalesce(controlled_hyper_jan_14, 0) as controlled_hyper_jan_14,
coalesce(controlled_hyper_apr_14, 0) as controlled_hyper_apr_14,
coalesce(controlled_hyper_jul_14, 0) as controlled_hyper_jul_14,
coalesce(controlled_hyper_oct_14, 0) as controlled_hyper_oct_14,
coalesce(controlled_hyper_jan_15, 0) as controlled_hyper_jan_15,
coalesce(controlled_hyper_apr_15, 0) as controlled_hyper_apr_15,
coalesce(controlled_hyper_jul_15, 0) as controlled_hyper_jul_15,
coalesce(controlled_hyper_oct_15, 0) as controlled_hyper_oct_15,
coalesce(controlled_hyper_jan_16, 0) as controlled_hyper_jan_16,
coalesce(controlled_hyper_apr_16, 0) as controlled_hyper_apr_16,
coalesce(controlled_hyper_jul_16, 0) as controlled_hyper_jul_16,
coalesce(controlled_hyper_oct_16, 0) as controlled_hyper_oct_16,
coalesce(hypertension_treated_jan_12, 0) as hypertension_treated_jan_12,
coalesce(hypertension_treated_apr_12, 0) as hypertension_treated_apr_12,
coalesce(hypertension_treated_jul_12, 0) as hypertension_treated_jul_12,
coalesce(hypertension_treated_oct_12, 0) as hypertension_treated_oct_12,
coalesce(hypertension_treated_jan_13, 0) as hypertension_treated_jan_13,
coalesce(hypertension_treated_apr_13, 0) as hypertension_treated_apr_13,
coalesce(hypertension_treated_jul_13, 0) as hypertension_treated_jul_13,
coalesce(hypertension_treated_oct_13, 0) as hypertension_treated_oct_13,
coalesce(hypertension_treated_jan_14, 0) as hypertension_treated_jan_14,
coalesce(hypertension_treated_apr_14, 0) as hypertension_treated_apr_14,
coalesce(hypertension_treated_jul_14, 0) as hypertension_treated_jul_14,
coalesce(hypertension_treated_oct_14, 0) as hypertension_treated_oct_14,
coalesce(hypertension_treated_jan_15, 0) as hypertension_treated_jan_15,
coalesce(hypertension_treated_apr_15, 0) as hypertension_treated_apr_15,
coalesce(hypertension_treated_jul_15, 0) as hypertension_treated_jul_15,
coalesce(hypertension_treated_oct_15, 0) as hypertension_treated_oct_15,
coalesce(hypertension_treated_jan_16, 0) as hypertension_treated_jan_16,
coalesce(hypertension_treated_apr_16, 0) as hypertension_treated_apr_16,
coalesce(hypertension_treated_jul_16, 0) as hypertension_treated_jul_16,
coalesce(hypertension_treated_oct_16, 0) as hypertension_treated_oct_16,
coalesce(measured_bp_jan_12, 0) as measured_bp_jan_12,
coalesce(measured_bp_apr_12, 0) as measured_bp_apr_12,
coalesce(measured_bp_jul_12, 0) as measured_bp_jul_12,
coalesce(measured_bp_oct_12, 0) as measured_bp_oct_12,
coalesce(measured_bp_jan_13, 0) as measured_bp_jan_13,
coalesce(measured_bp_apr_13, 0) as measured_bp_apr_13,
coalesce(measured_bp_jul_13, 0) as measured_bp_jul_13,
coalesce(measured_bp_oct_13, 0) as measured_bp_oct_13,
coalesce(measured_bp_jan_14, 0) as measured_bp_jan_14,
coalesce(measured_bp_apr_14, 0) as measured_bp_apr_14,
coalesce(measured_bp_jul_14, 0) as measured_bp_jul_14,
coalesce(measured_bp_oct_14, 0) as measured_bp_oct_14,
coalesce(measured_bp_jan_15, 0) as measured_bp_jan_15,
coalesce(measured_bp_apr_15, 0) as measured_bp_apr_15,
coalesce(measured_bp_jul_15, 0) as measured_bp_jul_15,
coalesce(measured_bp_oct_15, 0) as measured_bp_oct_15,
coalesce(measured_bp_jan_16, 0) as measured_bp_jan_16,
coalesce(measured_bp_apr_16, 0) as measured_bp_apr_16,
coalesce(measured_bp_jul_16, 0) as measured_bp_jul_16,
coalesce(measured_bp_oct_16, 0) as measured_bp_oct_16      
FROM
esp_mdphnet.smk_diab_smoking_full_strat s
FULL OUTER JOIN esp_mdphnet.smk_diab_prev_diabetes_full_strat pd
ON pd.age_group_10_yr = s.age_group_10_yr
AND pd.sex = s.sex
AND pd.race_ethnicity = s.race_ethnicity
FULL OUTER JOIN esp_mdphnet.smk_diab_act_diab_full_strat ad
ON ad.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr)
AND ad.sex = coalesce(s.sex, pd.sex)
AND ad.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_denom_full_strat d
ON d.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr)
AND d.sex = coalesce(s.sex, pd.sex, ad.sex)
AND d.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_asthma_active_full_strat a
ON a.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr)
AND a.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex)
AND a.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_bmi_full_strat b
ON b.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr)
AND b.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex)
AND b.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity)
-- FULL OUTER JOIN esp_mdphnet.smk_diab_bmi_denom_full_strat bd
-- ON bd.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr)
-- AND bd.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex)
-- AND bd.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_hyper_full_strat h
ON h.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr)
AND h.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex)
AND h.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_systolic_full_strat sys
ON sys.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr)
AND sys.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex)
AND sys.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_diastolic_full_strat dia
ON dia.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr)
AND dia.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex)
AND dia.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_controlled_bp_full_strat cbp
ON cbp.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr)
AND cbp.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex)
AND cbp.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_bp_measured_full_strat mbp
ON mbp.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr)
AND mbp.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex)
AND mbp.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_smoking_recorded_full_strat smkstat
ON smkstat.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr)
AND smkstat.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex)
AND smkstat.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_smoking_still_seen_full_strat smkseen
ON smkseen.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr)
AND smkseen.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex)
AND smkseen.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_smoking_cessation_full_strat smkcess
ON smkcess.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr)
AND smkcess.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex)
AND smkcess.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_hyper_treated_full_strat hyptreat
ON hyptreat.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr, smkcess.age_group_10_yr)
AND hyptreat.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex, smkcess.sex)
AND hyptreat.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity, smkcess.race_ethnicity)
;

-- MLCHC WORK AROUND
update esp_mdphnet.smk_diab_all_full_strat_output 
set 
bmi_denom_jan_12 = 0,
bmi_denom_apr_12 = 0,
bmi_denom_jul_12 = 0,
bmi_denom_oct_12 = 0,
bmi_denom_jan_13 = 0,
bmi_denom_apr_13 = 0,
bmi_denom_jul_13 = 0,
bmi_denom_oct_13 = 0,
bmi_denom_jan_14 = 0,
bmi_denom_apr_14 = 0,
bmi_denom_jul_14 = 0,
bmi_denom_oct_14 = 0,
bmi_denom_jan_15 = 0,
bmi_denom_apr_15 = 0,
bmi_denom_jul_15 = 0,
bmi_denom_oct_15 = 0,
bmi_denom_jan_16 = 0,
bmi_denom_apr_16 = 0,
bmi_denom_jul_16 = 0,
bmi_denom_oct_16 = 0
where age_group_10_year in ('0-9', '10-19');


-- Step 183: SQL - FINAL - Bring together all of the values for PWTF Community
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_comm_output  WITHOUT OIDS  AS SELECT
coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr, b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, dia.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr, smkcess.age_group_10_yr, hyptreat.age_group_10_yr) as age_group_10_year, 
coalesce(s.sex, pd.sex, ad.sex, d.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex, smkcess.sex, hyptreat.sex) as sex,
coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity, smkcess.race_ethnicity, hyptreat.race_ethnicity) as race,
coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm, mbp.pwtf_comm, smkstat.pwtf_comm, smkseen.pwtf_comm, smkcess.pwtf_comm) as pwtf_comm,
coalesce(smoke_status_rec_jan_12, 0) as smoke_status_rec_jan_12,
coalesce(smoke_status_rec_apr_12, 0) as smoke_status_rec_apr_12,
coalesce(smoke_status_rec_jul_12, 0) as smoke_status_rec_jul_12,
coalesce(smoke_status_rec_oct_12, 0) as smoke_status_rec_oct_12,
coalesce(smoke_status_rec_jan_13, 0) as smoke_status_rec_jan_13,
coalesce(smoke_status_rec_apr_13, 0) as smoke_status_rec_apr_13,
coalesce(smoke_status_rec_jul_13, 0) as smoke_status_rec_jul_13,
coalesce(smoke_status_rec_oct_13, 0) as smoke_status_rec_oct_13,
coalesce(smoke_status_rec_jan_14, 0) as smoke_status_rec_jan_14,
coalesce(smoke_status_rec_apr_14, 0) as smoke_status_rec_apr_14,
coalesce(smoke_status_rec_jul_14, 0) as smoke_status_rec_jul_14,
coalesce(smoke_status_rec_oct_14, 0) as smoke_status_rec_oct_14,
coalesce(smoke_status_rec_jan_15, 0) as smoke_status_rec_jan_15,
coalesce(smoke_status_rec_apr_15, 0) as smoke_status_rec_apr_15,
coalesce(smoke_status_rec_jul_15, 0) as smoke_status_rec_jul_15,
coalesce(smoke_status_rec_oct_15, 0) as smoke_status_rec_oct_15,
coalesce(smoke_status_rec_jan_16, 0) as smoke_status_rec_jan_16,
coalesce(smoke_status_rec_apr_16, 0) as smoke_status_rec_apr_16,
coalesce(smoke_status_rec_jul_16, 0) as smoke_status_rec_jul_16,
coalesce(smoke_status_rec_oct_16, 0) as smoke_status_rec_oct_16,
coalesce (curr_smoke_jan_12, 0 ) curr_smoke_jan_12,
coalesce (curr_smoke_apr_12, 0 ) curr_smoke_apr_12,
coalesce (curr_smoke_jul_12, 0 ) curr_smoke_jul_12,
coalesce (curr_smoke_oct_12, 0 ) curr_smoke_oct_12,
coalesce (curr_smoke_jan_13, 0 ) curr_smoke_jan_13,
coalesce (curr_smoke_apr_13, 0 ) curr_smoke_apr_13,
coalesce (curr_smoke_jul_13, 0 ) curr_smoke_jul_13,
coalesce (curr_smoke_oct_13, 0 ) curr_smoke_oct_13,
coalesce (curr_smoke_jan_14, 0 ) curr_smoke_jan_14,
coalesce (curr_smoke_apr_14, 0 ) curr_smoke_apr_14,
coalesce (curr_smoke_jul_14, 0 ) curr_smoke_jul_14,
coalesce (curr_smoke_oct_14, 0 ) curr_smoke_oct_14,
coalesce (curr_smoke_jan_15, 0 ) curr_smoke_jan_15,
coalesce (curr_smoke_apr_15, 0 ) curr_smoke_apr_15,
coalesce (curr_smoke_jul_15, 0 ) curr_smoke_jul_15,
coalesce (curr_smoke_oct_15, 0 ) curr_smoke_oct_15,
coalesce (curr_smoke_jan_16, 0 ) curr_smoke_jan_16,
coalesce (curr_smoke_apr_16, 0) curr_smoke_apr_16,
coalesce (curr_smoke_jul_16, 0) curr_smoke_jul_16,
coalesce (curr_smoke_oct_16, 0) curr_smoke_oct_16,
coalesce(smoke_still_seen_base_denom_jan_14, 0) as smoke_still_seen_base_denom_jan_14,
coalesce(smoke_still_seen_apr_14, 0) as smoke_still_seen_apr_14,
coalesce(smoke_still_seen_jul_14, 0) as smoke_still_seen_jul_14,
coalesce(smoke_still_seen_oct_14, 0) as smoke_still_seen_oct_14,
coalesce(smoke_still_seen_jan_15, 0) as smoke_still_seen_jan_15,
coalesce(smoke_still_seen_apr_15, 0) as smoke_still_seen_apr_15,
coalesce(smoke_still_seen_jul_15, 0) as smoke_still_seen_jul_15,
coalesce(smoke_still_seen_oct_15, 0) as smoke_still_seen_oct_15,
coalesce(smoke_still_seen_jan_16, 0) as smoke_still_seen_jan_16,
coalesce(smoke_still_seen_apr_16, 0) as smoke_still_seen_apr_16,
coalesce(smoke_still_seen_jul_16, 0) as smoke_still_seen_jul_16,
coalesce(smoke_still_seen_oct_16, 0) as smoke_still_seen_oct_16,
coalesce(smoke_cessation_apr_14, 0) as smoke_cessation_apr_14,
coalesce(smoke_cessation_jul_14, 0) as smoke_cessation_jul_14,
coalesce(smoke_cessation_oct_14, 0) as smoke_cessation_oct_14,
coalesce(smoke_cessation_jan_15, 0) as smoke_cessation_jan_15,
coalesce(smoke_cessation_apr_15, 0) as smoke_cessation_apr_15,
coalesce(smoke_cessation_jul_15, 0) as smoke_cessation_jul_15,
coalesce(smoke_cessation_oct_15, 0) as smoke_cessation_oct_15,
coalesce(smoke_cessation_jan_16, 0) as smoke_cessation_jan_16,
coalesce(smoke_cessation_apr_16, 0) as smoke_cessation_apr_16,
coalesce(smoke_cessation_jul_16, 0) as smoke_cessation_jul_16,
coalesce(smoke_cessation_oct_16, 0) as smoke_cessation_oct_16,
coalesce (t1_diab_prev_jan_12, 0 ) as t1_diab_prev_jan_12,
coalesce (t1_diab_prev_apr_12, 0 ) as t1_diab_prev_apr_12,
coalesce (t1_diab_prev_jul_12, 0 ) as t1_diab_prev_jul_12,
coalesce (t1_diab_prev_oct_12, 0 ) as t1_diab_prev_oct_12,
coalesce (t1_diab_prev_jan_13, 0 ) as t1_diab_prev_jan_13,
coalesce (t1_diab_prev_apr_13, 0 ) as t1_diab_prev_apr_13,
coalesce (t1_diab_prev_jul_13, 0 ) as t1_diab_prev_jul_13,
coalesce (t1_diab_prev_oct_13, 0 ) as t1_diab_prev_oct_13,
coalesce (t1_diab_prev_jan_14, 0 ) as t1_diab_prev_jan_14,
coalesce (t1_diab_prev_apr_14, 0 ) as t1_diab_prev_apr_14,
coalesce (t1_diab_prev_jul_14, 0 ) as t1_diab_prev_jul_14,
coalesce (t1_diab_prev_oct_14, 0 ) as t1_diab_prev_oct_14,
coalesce (t1_diab_prev_jan_15, 0 ) as t1_diab_prev_jan_15,
coalesce (t1_diab_prev_apr_15, 0 ) as t1_diab_prev_apr_15,
coalesce (t1_diab_prev_jul_15, 0 ) as t1_diab_prev_jul_15,
coalesce (t1_diab_prev_oct_15, 0 ) as t1_diab_prev_oct_15,
coalesce (t1_diab_prev_jan_16, 0 ) as t1_diab_prev_jan_16,
coalesce (t1_diab_prev_apr_16, 0 ) as t1_diab_prev_apr_16,
coalesce (t1_diab_prev_jul_16, 0 ) as t1_diab_prev_jul_16,
coalesce (t1_diab_prev_oct_16, 0 ) as t1_diab_prev_oct_16,
coalesce (act_t1_diab_jan_12, 0 ) as act_t1_diab_jan_12,
coalesce (act_t1_diab_apr_12, 0 ) as act_t1_diab_apr_12,
coalesce (act_t1_diab_jul_12, 0 ) as act_t1_diab_jul_12,
coalesce (act_t1_diab_oct_12, 0 ) as act_t1_diab_oct_12,
coalesce (act_t1_diab_jan_13, 0 ) as act_t1_diab_jan_13,
coalesce (act_t1_diab_apr_13, 0 ) as act_t1_diab_apr_13,
coalesce (act_t1_diab_jul_13, 0 ) as act_t1_diab_jul_13,
coalesce (act_t1_diab_oct_13, 0 ) as act_t1_diab_oct_13,
coalesce (act_t1_diab_jan_14, 0 ) as act_t1_diab_jan_14,
coalesce (act_t1_diab_apr_14, 0 ) as act_t1_diab_apr_14,
coalesce (act_t1_diab_jul_14, 0 ) as act_t1_diab_jul_14,
coalesce (act_t1_diab_oct_14, 0 ) as act_t1_diab_oct_14,
coalesce (act_t1_diab_jan_15, 0 ) as act_t1_diab_jan_15,
coalesce (act_t1_diab_apr_15, 0 ) as act_t1_diab_apr_15,
coalesce (act_t1_diab_jul_15, 0 ) as act_t1_diab_jul_15,
coalesce (act_t1_diab_oct_15, 0 ) as act_t1_diab_oct_15,
coalesce (act_t1_diab_jan_16, 0 ) as act_t1_diab_jan_16,
coalesce (act_t1_diab_apr_16, 0 ) as act_t1_diab_apr_16,
coalesce (act_t1_diab_jul_16, 0 ) as act_t1_diab_jul_16,
coalesce (act_t1_diab_oct_16, 0 ) as act_t1_diab_oct_16,
coalesce (t2_diab_prev_jan_12, 0 ) as t2_diab_prev_jan_12,
coalesce (t2_diab_prev_apr_12, 0 ) as t2_diab_prev_apr_12,
coalesce (t2_diab_prev_jul_12, 0 ) as t2_diab_prev_jul_12,
coalesce (t2_diab_prev_oct_12, 0 ) as t2_diab_prev_oct_12,
coalesce (t2_diab_prev_jan_13, 0 ) as t2_diab_prev_jan_13,
coalesce (t2_diab_prev_apr_13, 0 ) as t2_diab_prev_apr_13,
coalesce (t2_diab_prev_jul_13, 0 ) as t2_diab_prev_jul_13,
coalesce (t2_diab_prev_oct_13, 0 ) as t2_diab_prev_oct_13,
coalesce (t2_diab_prev_jan_14, 0 ) as t2_diab_prev_jan_14,
coalesce (t2_diab_prev_apr_14, 0 ) as t2_diab_prev_apr_14,
coalesce (t2_diab_prev_jul_14, 0 ) as t2_diab_prev_jul_14,
coalesce (t2_diab_prev_oct_14, 0 ) as t2_diab_prev_oct_14,
coalesce (t2_diab_prev_jan_15, 0 ) as t2_diab_prev_jan_15,
coalesce (t2_diab_prev_apr_15, 0 ) as t2_diab_prev_apr_15,
coalesce (t2_diab_prev_jul_15, 0 ) as t2_diab_prev_jul_15,
coalesce (t2_diab_prev_oct_15, 0 ) as t2_diab_prev_oct_15,
coalesce (t2_diab_prev_jan_16, 0 ) as t2_diab_prev_jan_16,
coalesce (t2_diab_prev_apr_16, 0 ) as t2_diab_prev_apr_16,
coalesce (t2_diab_prev_jul_16, 0 ) as t2_diab_prev_jul_16,
coalesce (t2_diab_prev_oct_16, 0 ) as t2_diab_prev_oct_16,
coalesce (act_t2_diab_jan_12, 0 ) as act_t2_diab_jan_12,
coalesce (act_t2_diab_apr_12, 0 ) as act_t2_diab_apr_12,
coalesce (act_t2_diab_jul_12, 0 ) as act_t2_diab_jul_12,
coalesce (act_t2_diab_oct_12, 0 ) as act_t2_diab_oct_12,
coalesce (act_t2_diab_jan_13, 0 ) as act_t2_diab_jan_13,
coalesce (act_t2_diab_apr_13, 0 ) as act_t2_diab_apr_13,
coalesce (act_t2_diab_jul_13, 0 ) as act_t2_diab_jul_13,
coalesce (act_t2_diab_oct_13, 0 ) as act_t2_diab_oct_13,
coalesce (act_t2_diab_jan_14, 0 ) as act_t2_diab_jan_14,
coalesce (act_t2_diab_apr_14, 0 ) as act_t2_diab_apr_14,
coalesce (act_t2_diab_jul_14, 0 ) as act_t2_diab_jul_14,
coalesce (act_t2_diab_oct_14, 0 ) as act_t2_diab_oct_14,
coalesce (act_t2_diab_jan_15, 0 ) as act_t2_diab_jan_15,
coalesce (act_t2_diab_apr_15, 0 ) as act_t2_diab_apr_15,
coalesce (act_t2_diab_jul_15, 0 ) as act_t2_diab_jul_15,
coalesce (act_t2_diab_oct_15, 0 ) as act_t2_diab_oct_15,
coalesce (act_t2_diab_jan_16, 0 ) as act_t2_diab_jan_16,
coalesce (act_t2_diab_apr_16, 0 ) as act_t2_diab_apr_16,
coalesce (act_t2_diab_jul_16, 0 ) as act_t2_diab_jul_16,
coalesce (act_t2_diab_oct_16, 0 ) as act_t2_diab_oct_16,
coalesce (t1_or_t2_diab_prev_jan_12, 0 ) as t1_or_t2_diab_prev_jan_12,
coalesce (t1_or_t2_diab_prev_apr_12, 0 ) as t1_or_t2_diab_prev_apr_12,
coalesce (t1_or_t2_diab_prev_jul_12, 0 ) as t1_or_t2_diab_prev_jul_12,
coalesce (t1_or_t2_diab_prev_oct_12, 0 ) as t1_or_t2_diab_prev_oct_12,
coalesce (t1_or_t2_diab_prev_jan_13, 0 ) as t1_or_t2_diab_prev_jan_13,
coalesce (t1_or_t2_diab_prev_apr_13, 0 ) as t1_or_t2_diab_prev_apr_13,
coalesce (t1_or_t2_diab_prev_jul_13, 0 ) as t1_or_t2_diab_prev_jul_13,
coalesce (t1_or_t2_diab_prev_oct_13, 0 ) as t1_or_t2_diab_prev_oct_13,
coalesce (t1_or_t2_diab_prev_jan_14, 0 ) as t1_or_t2_diab_prev_jan_14,
coalesce (t1_or_t2_diab_prev_apr_14, 0 ) as t1_or_t2_diab_prev_apr_14,
coalesce (t1_or_t2_diab_prev_jul_14, 0 ) as t1_or_t2_diab_prev_jul_14,
coalesce (t1_or_t2_diab_prev_oct_14, 0 ) as t1_or_t2_diab_prev_oct_14,
coalesce (t1_or_t2_diab_prev_jan_15, 0 ) as t1_or_t2_diab_prev_jan_15,
coalesce (t1_or_t2_diab_prev_apr_15, 0 ) as t1_or_t2_diab_prev_apr_15,
coalesce (t1_or_t2_diab_prev_jul_15, 0 ) as t1_or_t2_diab_prev_jul_15,
coalesce (t1_or_t2_diab_prev_oct_15, 0 ) as t1_or_t2_diab_prev_oct_15,
coalesce (t1_or_t2_diab_prev_jan_16, 0 ) as t1_or_t2_diab_prev_jan_16,
coalesce (t1_or_t2_diab_prev_apr_16, 0 ) as t1_or_t2_diab_prev_apr_16,
coalesce (t1_or_t2_diab_prev_jul_16, 0 ) as t1_or_t2_diab_prev_jul_16,
coalesce (t1_or_t2_diab_prev_oct_16, 0 ) as t1_or_t2_diab_prev_oct_16,
coalesce (act_t1_or_t2_diab_jan_12, 0 ) as act_t1_or_t2_diab_jan_12,
coalesce (act_t1_or_t2_diab_apr_12, 0 ) as act_t1_or_t2_diab_apr_12,
coalesce (act_t1_or_t2_diab_jul_12, 0 ) as act_t1_or_t2_diab_jul_12,
coalesce (act_t1_or_t2_diab_oct_12, 0 ) as act_t1_or_t2_diab_oct_12,
coalesce (act_t1_or_t2_diab_jan_13, 0 ) as act_t1_or_t2_diab_jan_13,
coalesce (act_t1_or_t2_diab_apr_13, 0 ) as act_t1_or_t2_diab_apr_13,
coalesce (act_t1_or_t2_diab_jul_13, 0 ) as act_t1_or_t2_diab_jul_13,
coalesce (act_t1_or_t2_diab_oct_13, 0 ) as act_t1_or_t2_diab_oct_13,
coalesce (act_t1_or_t2_diab_jan_14, 0 ) as act_t1_or_t2_diab_jan_14,
coalesce (act_t1_or_t2_diab_apr_14, 0 ) as act_t1_or_t2_diab_apr_14,
coalesce (act_t1_or_t2_diab_jul_14, 0 ) as act_t1_or_t2_diab_jul_14,
coalesce (act_t1_or_t2_diab_oct_14, 0 ) as act_t1_or_t2_diab_oct_14,
coalesce (act_t1_or_t2_diab_jan_15, 0 ) as act_t1_or_t2_diab_jan_15,
coalesce (act_t1_or_t2_diab_apr_15, 0 ) as act_t1_or_t2_diab_apr_15,
coalesce (act_t1_or_t2_diab_jul_15, 0 ) as act_t1_or_t2_diab_jul_15,
coalesce (act_t1_or_t2_diab_oct_15, 0 ) as act_t1_or_t2_diab_oct_15,
coalesce (act_t1_or_t2_diab_jan_16, 0 ) as act_t1_or_t2_diab_jan_16,
coalesce (act_t1_or_t2_diab_apr_16, 0 ) as act_t1_or_t2_diab_apr_16,
coalesce (act_t1_or_t2_diab_jul_16, 0 ) as act_t1_or_t2_diab_jul_16,
coalesce (act_t1_or_t2_diab_oct_16, 0 ) as act_t1_or_t2_diab_oct_16,
coalesce (asthma_jan_12, 0 ) as asthma_jan_12,
coalesce (asthma_apr_12, 0 ) as asthma_apr_12,
coalesce (asthma_jul_12, 0 ) as asthma_jul_12,
coalesce (asthma_oct_12, 0 ) as asthma_oct_12,
coalesce (asthma_jan_13, 0 ) as asthma_jan_13,
coalesce (asthma_apr_13, 0 ) as asthma_apr_13,
coalesce (asthma_jul_13, 0 ) as asthma_jul_13,
coalesce (asthma_oct_13, 0 ) as asthma_oct_13,
coalesce (asthma_jan_14, 0 ) as asthma_jan_14,
coalesce (asthma_apr_14, 0 ) as asthma_apr_14,
coalesce (asthma_jul_14, 0 ) as asthma_jul_14,
coalesce (asthma_oct_14, 0 ) as asthma_oct_14,
coalesce (asthma_jan_15, 0 ) as asthma_jan_15,
coalesce (asthma_apr_15, 0 ) as asthma_apr_15,
coalesce (asthma_jul_15, 0 ) as asthma_jul_15,
coalesce (asthma_oct_15, 0 ) as asthma_oct_15,
coalesce (asthma_jan_16, 0 ) as asthma_jan_16,
coalesce (asthma_apr_16, 0 ) as asthma_apr_16,
coalesce (asthma_jul_16, 0 ) as asthma_jul_16,
coalesce (asthma_oct_16, 0 ) as asthma_oct_16,
coalesce (denominator_jan_12, 0 ) as denominator_jan_12,
coalesce (denominator_apr_12, 0 ) as denominator_apr_12,
coalesce (denominator_jul_12, 0 ) as denominator_jul_12,
coalesce (denominator_oct_12, 0 ) as denominator_oct_12,
coalesce (denominator_jan_13, 0 ) as denominator_jan_13,
coalesce (denominator_apr_13, 0 ) as denominator_apr_13,
coalesce (denominator_jul_13, 0 ) as denominator_jul_13,
coalesce (denominator_oct_13, 0 ) as denominator_oct_13,
coalesce (denominator_jan_14, 0 ) as denominator_jan_14,
coalesce (denominator_apr_14, 0 ) as denominator_apr_14,
coalesce (denominator_jul_14, 0 ) as denominator_jul_14,
coalesce (denominator_oct_14, 0 ) as denominator_oct_14,
coalesce (denominator_jan_15, 0 ) as denominator_jan_15,
coalesce (denominator_apr_15, 0 ) as denominator_apr_15,
coalesce (denominator_jul_15, 0 ) as denominator_jul_15,
coalesce (denominator_oct_15, 0 ) as denominator_oct_15,
coalesce (denominator_jan_16, 0 ) as denominator_jan_16,
coalesce (denominator_apr_16, 0 ) as denominator_apr_16,
coalesce (denominator_jul_16, 0 ) as denominator_jul_16,
coalesce (denominator_oct_16, 0 ) as denominator_oct_16,
coalesce(overweight_jan_12, 0)  as overweight_jan_12,
coalesce(overweight_apr_12, 0)  as overweight_apr_12,
coalesce(overweight_jul_12, 0)  as overweight_jul_12,
coalesce(overweight_oct_12, 0)  as overweight_oct_12,
coalesce(overweight_jan_13, 0)  as overweight_jan_13,
coalesce(overweight_apr_13, 0)  as overweight_apr_13,
coalesce(overweight_jul_13, 0)  as overweight_jul_13,
coalesce(overweight_oct_13, 0)  as overweight_oct_13,
coalesce(overweight_jan_14, 0)  as overweight_jan_14,
coalesce(overweight_apr_14, 0)  as overweight_apr_14,
coalesce(overweight_jul_14, 0)  as overweight_jul_14,
coalesce(overweight_oct_14, 0)  as overweight_oct_14,
coalesce(overweight_jan_15, 0)  as overweight_jan_15,
coalesce(overweight_apr_15, 0)  as overweight_apr_15,
coalesce(overweight_jul_15, 0)  as overweight_jul_15,
coalesce(overweight_oct_15, 0)  as overweight_oct_15,
coalesce(overweight_jan_16, 0)  as overweight_jan_16,
coalesce(overweight_apr_16, 0)  as overweight_apr_16,
coalesce(overweight_jul_16, 0)  as overweight_jul_16,
coalesce(overweight_oct_16, 0)  as overweight_oct_16,
coalesce(obese_jan_12, 0)  as obese_jan_12,
coalesce(obese_apr_12, 0)  as obese_apr_12,
coalesce(obese_jul_12, 0)  as obese_jul_12,
coalesce(obese_oct_12, 0)  as obese_oct_12,
coalesce(obese_jan_13, 0)  as obese_jan_13,
coalesce(obese_apr_13, 0)  as obese_apr_13,
coalesce(obese_jul_13, 0)  as obese_jul_13,
coalesce(obese_oct_13, 0)  as obese_oct_13,
coalesce(obese_jan_14, 0)  as obese_jan_14,
coalesce(obese_apr_14, 0)  as obese_apr_14,
coalesce(obese_jul_14, 0)  as obese_jul_14,
coalesce(obese_oct_14, 0)  as obese_oct_14,
coalesce(obese_jan_15, 0)  as obese_jan_15,
coalesce(obese_apr_15, 0)  as obese_apr_15,
coalesce(obese_jul_15, 0)  as obese_jul_15,
coalesce(obese_oct_15, 0)  as obese_oct_15,
coalesce(obese_jan_16, 0)  as obese_jan_16,
coalesce(obese_apr_16, 0)  as obese_apr_16,
coalesce(obese_jul_16, 0)  as obese_jul_16,
coalesce(obese_oct_16, 0)  as obese_oct_16,
coalesce(overweight_or_obese_jan_12, 0)  as overweight_or_obese_jan_12,
coalesce(overweight_or_obese_apr_12, 0)  as overweight_or_obese_apr_12,
coalesce(overweight_or_obese_jul_12, 0)  as overweight_or_obese_jul_12,
coalesce(overweight_or_obese_oct_12, 0)  as overweight_or_obese_oct_12,
coalesce(overweight_or_obese_jan_13, 0)  as overweight_or_obese_jan_13,
coalesce(overweight_or_obese_apr_13, 0)  as overweight_or_obese_apr_13,
coalesce(overweight_or_obese_jul_13, 0)  as overweight_or_obese_jul_13,
coalesce(overweight_or_obese_oct_13, 0)  as overweight_or_obese_oct_13,
coalesce(overweight_or_obese_jan_14, 0)  as overweight_or_obese_jan_14,
coalesce(overweight_or_obese_apr_14, 0)  as overweight_or_obese_apr_14,
coalesce(overweight_or_obese_jul_14, 0)  as overweight_or_obese_jul_14,
coalesce(overweight_or_obese_oct_14, 0)  as overweight_or_obese_oct_14,
coalesce(overweight_or_obese_jan_15, 0)  as overweight_or_obese_jan_15,
coalesce(overweight_or_obese_apr_15, 0)  as overweight_or_obese_apr_15,
coalesce(overweight_or_obese_jul_15, 0)  as overweight_or_obese_jul_15,
coalesce(overweight_or_obese_oct_15, 0)  as overweight_or_obese_oct_15,
coalesce(overweight_or_obese_jan_16, 0)  as overweight_or_obese_jan_16,
coalesce(overweight_or_obese_apr_16, 0)  as overweight_or_obese_apr_16,
coalesce(overweight_or_obese_jul_16, 0)  as overweight_or_obese_jul_16,
coalesce(overweight_or_obese_oct_16, 0)  as overweight_or_obese_oct_16,
coalesce(no_measured_bmi_jan_12, 0 )  as   no_measured_bmi_jan_12,
coalesce(no_measured_bmi_apr_12, 0 )  as   no_measured_bmi_apr_12,
coalesce(no_measured_bmi_jul_12, 0 )  as   no_measured_bmi_jul_12,
coalesce(no_measured_bmi_oct_12, 0 )  as   no_measured_bmi_oct_12,
coalesce(no_measured_bmi_jan_13, 0 )  as   no_measured_bmi_jan_13,
coalesce(no_measured_bmi_apr_13, 0 )  as   no_measured_bmi_apr_13,
coalesce(no_measured_bmi_jul_13, 0 )  as   no_measured_bmi_jul_13,
coalesce(no_measured_bmi_oct_13, 0 )  as   no_measured_bmi_oct_13,
coalesce(no_measured_bmi_jan_14, 0 )  as   no_measured_bmi_jan_14,
coalesce(no_measured_bmi_apr_14, 0 )  as   no_measured_bmi_apr_14,
coalesce(no_measured_bmi_jul_14, 0 )  as   no_measured_bmi_jul_14,
coalesce(no_measured_bmi_oct_14, 0 )  as   no_measured_bmi_oct_14,
coalesce(no_measured_bmi_jan_15, 0 )  as   no_measured_bmi_jan_15,
coalesce(no_measured_bmi_apr_15, 0 )  as   no_measured_bmi_apr_15,
coalesce(no_measured_bmi_jul_15, 0 )  as   no_measured_bmi_jul_15,
coalesce(no_measured_bmi_oct_15, 0 )  as   no_measured_bmi_oct_15,
coalesce(no_measured_bmi_jan_16, 0 )  as   no_measured_bmi_jan_16,
coalesce(no_measured_bmi_apr_16, 0 )  as   no_measured_bmi_apr_16,
coalesce(no_measured_bmi_jul_16, 0 )  as   no_measured_bmi_jul_16,
coalesce(no_measured_bmi_oct_16, 0 )  as   no_measured_bmi_oct_16,
coalesce( denominator_jan_12, 0)  as  bmi_denom_jan_12,
coalesce( denominator_apr_12, 0)  as  bmi_denom_apr_12,
coalesce( denominator_jul_12, 0)  as  bmi_denom_jul_12,
coalesce( denominator_oct_12, 0)  as  bmi_denom_oct_12,
coalesce( denominator_jan_13, 0)  as  bmi_denom_jan_13,
coalesce( denominator_apr_13, 0)  as  bmi_denom_apr_13,
coalesce( denominator_jul_13, 0)  as  bmi_denom_jul_13,
coalesce( denominator_oct_13, 0)  as  bmi_denom_oct_13,
coalesce( denominator_jan_14, 0)  as  bmi_denom_jan_14,
coalesce( denominator_apr_14, 0)  as  bmi_denom_apr_14,
coalesce( denominator_jul_14, 0)  as  bmi_denom_jul_14,
coalesce( denominator_oct_14, 0)  as  bmi_denom_oct_14,
coalesce( denominator_jan_15, 0)  as  bmi_denom_jan_15,
coalesce( denominator_apr_15, 0)  as  bmi_denom_apr_15,
coalesce( denominator_jul_15, 0)  as  bmi_denom_jul_15,
coalesce( denominator_oct_15, 0)  as  bmi_denom_oct_15,
coalesce( denominator_jan_16, 0)  as  bmi_denom_jan_16,
coalesce( denominator_apr_16, 0)  as  bmi_denom_apr_16,
coalesce( denominator_jul_16, 0)  as  bmi_denom_jul_16,
coalesce( denominator_oct_16, 0)  as  bmi_denom_oct_16,
coalesce(hypertension_jan_12, 0 )  as   hypertension_jan_12,
coalesce(hypertension_apr_12, 0 )  as   hypertension_apr_12,
coalesce(hypertension_jul_12, 0 )  as   hypertension_jul_12,
coalesce(hypertension_oct_12, 0 )  as   hypertension_oct_12,
coalesce(hypertension_jan_13, 0 )  as   hypertension_jan_13,
coalesce(hypertension_apr_13, 0 )  as   hypertension_apr_13,
coalesce(hypertension_jul_13, 0 )  as   hypertension_jul_13,
coalesce(hypertension_oct_13, 0 )  as   hypertension_oct_13,
coalesce(hypertension_jan_14, 0 )  as   hypertension_jan_14,
coalesce(hypertension_apr_14, 0 )  as   hypertension_apr_14,
coalesce(hypertension_jul_14, 0 )  as   hypertension_jul_14,
coalesce(hypertension_oct_14, 0 )  as   hypertension_oct_14,
coalesce(hypertension_jan_15, 0 )  as   hypertension_jan_15,
coalesce(hypertension_apr_15, 0 )  as   hypertension_apr_15,
coalesce(hypertension_jul_15, 0 )  as   hypertension_jul_15,
coalesce(hypertension_oct_15, 0 )  as   hypertension_oct_15,
coalesce(hypertension_jan_16, 0 )  as   hypertension_jan_16,
coalesce(hypertension_apr_16, 0 )  as   hypertension_apr_16,
coalesce(hypertension_jul_16, 0 )  as   hypertension_jul_16,
coalesce(hypertension_oct_16, 0 )  as   hypertension_oct_16,
coalesce(systolic_avg_jan_12, 0) as systolic_avg_jan_12, 
coalesce(systolic_avg_apr_12, 0) as systolic_avg_apr_12, 
coalesce(systolic_avg_jul_12, 0) as systolic_avg_jul_12, 
coalesce(systolic_avg_oct_12, 0) as systolic_avg_oct_12,
coalesce(systolic_avg_jan_13, 0) as systolic_avg_jan_13, 
coalesce(systolic_avg_apr_13, 0) as systolic_avg_apr_13, 
coalesce(systolic_avg_jul_13, 0) as systolic_avg_jul_13, 
coalesce(systolic_avg_oct_13, 0) as systolic_avg_oct_13,
coalesce(systolic_avg_jan_14, 0) as systolic_avg_jan_14, 
coalesce(systolic_avg_apr_14, 0) as systolic_avg_apr_14, 
coalesce(systolic_avg_jul_14, 0) as systolic_avg_jul_14, 
coalesce(systolic_avg_oct_14, 0) as systolic_avg_oct_14,
coalesce(systolic_avg_jan_15, 0) as systolic_avg_jan_15, 
coalesce(systolic_avg_apr_15, 0) as systolic_avg_apr_15, 
coalesce(systolic_avg_jul_15, 0) as systolic_avg_jul_15, 
coalesce(systolic_avg_oct_15, 0) as systolic_avg_oct_15,
coalesce(systolic_avg_jan_16, 0) as systolic_avg_jan_16, 
coalesce(systolic_avg_apr_16, 0) as systolic_avg_apr_16, 
coalesce(systolic_avg_jul_16, 0) as systolic_avg_jul_16, 
coalesce(systolic_avg_oct_16, 0) as systolic_avg_oct_16,
coalesce(diastolic_avg_jan_12, 0) as diastolic_avg_jan_12, 
coalesce(diastolic_avg_apr_12, 0) as diastolic_avg_apr_12, 
coalesce(diastolic_avg_jul_12, 0) as diastolic_avg_jul_12, 
coalesce(diastolic_avg_oct_12, 0) as diastolic_avg_oct_12,
coalesce(diastolic_avg_jan_13, 0) as diastolic_avg_jan_13, 
coalesce(diastolic_avg_apr_13, 0) as diastolic_avg_apr_13, 
coalesce(diastolic_avg_jul_13, 0) as diastolic_avg_jul_13, 
coalesce(diastolic_avg_oct_13, 0) as diastolic_avg_oct_13,
coalesce(diastolic_avg_jan_14, 0) as diastolic_avg_jan_14, 
coalesce(diastolic_avg_apr_14, 0) as diastolic_avg_apr_14, 
coalesce(diastolic_avg_jul_14, 0) as diastolic_avg_jul_14, 
coalesce(diastolic_avg_oct_14, 0) as diastolic_avg_oct_14,
coalesce(diastolic_avg_jan_15, 0) as diastolic_avg_jan_15, 
coalesce(diastolic_avg_apr_15, 0) as diastolic_avg_apr_15, 
coalesce(diastolic_avg_jul_15, 0) as diastolic_avg_jul_15, 
coalesce(diastolic_avg_oct_15, 0) as diastolic_avg_oct_15,
coalesce(diastolic_avg_jan_16, 0) as diastolic_avg_jan_16, 
coalesce(diastolic_avg_apr_16, 0) as diastolic_avg_apr_16, 
coalesce(diastolic_avg_jul_16, 0) as diastolic_avg_jul_16, 
coalesce(diastolic_avg_oct_16, 0) as diastolic_avg_oct_16,
coalesce(controlled_hyper_jan_12, 0) as controlled_hyper_jan_12,
coalesce(controlled_hyper_apr_12, 0) as controlled_hyper_apr_12,
coalesce(controlled_hyper_jul_12, 0) as controlled_hyper_jul_12,
coalesce(controlled_hyper_oct_12, 0) as controlled_hyper_oct_12,
coalesce(controlled_hyper_jan_13, 0) as controlled_hyper_jan_13,
coalesce(controlled_hyper_apr_13, 0) as controlled_hyper_apr_13,
coalesce(controlled_hyper_jul_13, 0) as controlled_hyper_jul_13,
coalesce(controlled_hyper_oct_13, 0) as controlled_hyper_oct_13,
coalesce(controlled_hyper_jan_14, 0) as controlled_hyper_jan_14,
coalesce(controlled_hyper_apr_14, 0) as controlled_hyper_apr_14,
coalesce(controlled_hyper_jul_14, 0) as controlled_hyper_jul_14,
coalesce(controlled_hyper_oct_14, 0) as controlled_hyper_oct_14,
coalesce(controlled_hyper_jan_15, 0) as controlled_hyper_jan_15,
coalesce(controlled_hyper_apr_15, 0) as controlled_hyper_apr_15,
coalesce(controlled_hyper_jul_15, 0) as controlled_hyper_jul_15,
coalesce(controlled_hyper_oct_15, 0) as controlled_hyper_oct_15,
coalesce(controlled_hyper_jan_16, 0) as controlled_hyper_jan_16,
coalesce(controlled_hyper_apr_16, 0) as controlled_hyper_apr_16,
coalesce(controlled_hyper_jul_16, 0) as controlled_hyper_jul_16,
coalesce(controlled_hyper_oct_16, 0) as controlled_hyper_oct_16,
coalesce(hypertension_treated_jan_12, 0) as hypertension_treated_jan_12,
coalesce(hypertension_treated_apr_12, 0) as hypertension_treated_apr_12,
coalesce(hypertension_treated_jul_12, 0) as hypertension_treated_jul_12,
coalesce(hypertension_treated_oct_12, 0) as hypertension_treated_oct_12,
coalesce(hypertension_treated_jan_13, 0) as hypertension_treated_jan_13,
coalesce(hypertension_treated_apr_13, 0) as hypertension_treated_apr_13,
coalesce(hypertension_treated_jul_13, 0) as hypertension_treated_jul_13,
coalesce(hypertension_treated_oct_13, 0) as hypertension_treated_oct_13,
coalesce(hypertension_treated_jan_14, 0) as hypertension_treated_jan_14,
coalesce(hypertension_treated_apr_14, 0) as hypertension_treated_apr_14,
coalesce(hypertension_treated_jul_14, 0) as hypertension_treated_jul_14,
coalesce(hypertension_treated_oct_14, 0) as hypertension_treated_oct_14,
coalesce(hypertension_treated_jan_15, 0) as hypertension_treated_jan_15,
coalesce(hypertension_treated_apr_15, 0) as hypertension_treated_apr_15,
coalesce(hypertension_treated_jul_15, 0) as hypertension_treated_jul_15,
coalesce(hypertension_treated_oct_15, 0) as hypertension_treated_oct_15,
coalesce(hypertension_treated_jan_16, 0) as hypertension_treated_jan_16,
coalesce(hypertension_treated_apr_16, 0) as hypertension_treated_apr_16,
coalesce(hypertension_treated_jul_16, 0) as hypertension_treated_jul_16,
coalesce(hypertension_treated_oct_16, 0) as hypertension_treated_oct_16,
coalesce(measured_bp_jan_12, 0) as measured_bp_jan_12,
coalesce(measured_bp_apr_12, 0) as measured_bp_apr_12,
coalesce(measured_bp_jul_12, 0) as measured_bp_jul_12,
coalesce(measured_bp_oct_12, 0) as measured_bp_oct_12,
coalesce(measured_bp_jan_13, 0) as measured_bp_jan_13,
coalesce(measured_bp_apr_13, 0) as measured_bp_apr_13,
coalesce(measured_bp_jul_13, 0) as measured_bp_jul_13,
coalesce(measured_bp_oct_13, 0) as measured_bp_oct_13,
coalesce(measured_bp_jan_14, 0) as measured_bp_jan_14,
coalesce(measured_bp_apr_14, 0) as measured_bp_apr_14,
coalesce(measured_bp_jul_14, 0) as measured_bp_jul_14,
coalesce(measured_bp_oct_14, 0) as measured_bp_oct_14,
coalesce(measured_bp_jan_15, 0) as measured_bp_jan_15,
coalesce(measured_bp_apr_15, 0) as measured_bp_apr_15,
coalesce(measured_bp_jul_15, 0) as measured_bp_jul_15,
coalesce(measured_bp_oct_15, 0) as measured_bp_oct_15,
coalesce(measured_bp_jan_16, 0) as measured_bp_jan_16,
coalesce(measured_bp_apr_16, 0) as measured_bp_apr_16,
coalesce(measured_bp_jul_16, 0) as measured_bp_jul_16,
coalesce(measured_bp_oct_16, 0) as measured_bp_oct_16     
FROM
esp_mdphnet.smk_diab_a_pwtf_smoking s
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_prev_diabetes pd
ON pd.age_group_10_yr = s.age_group_10_yr
AND pd.sex = s.sex
AND pd.race_ethnicity = s.race_ethnicity
AND pd.pwtf_comm = s.pwtf_comm
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_act_diabetes ad
ON ad.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr)
AND ad.sex = coalesce(s.sex, pd.sex)
AND ad.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity)
AND ad.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_denom d
ON d.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr)
AND d.sex = coalesce(s.sex, pd.sex, ad.sex)
AND d.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity)
AND d.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_asthma a
ON a.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr)
AND a.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex)
AND a.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity)
AND a.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_bmi b
ON b.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr)
AND b.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex)
AND b.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity)
AND b.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm)
-- FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_bmi_denom bd
-- ON bd.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr)
-- AND bd.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex)
-- AND bd.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity)
-- AND bd.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_hyper h
ON h.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr)
AND h.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex)
AND h.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity)
AND h.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_systolic sys
ON sys.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr)
AND sys.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex)
AND sys.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity)
AND sys.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_diastolic dia
ON dia.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr)
AND dia.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex)
AND dia.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity)
AND dia.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_controlled_bp cbp
ON cbp.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr)
AND cbp.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex)
AND cbp.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity)
AND cbp.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_measured_bp mbp
ON mbp.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr)
AND mbp.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex)
AND mbp.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity)
AND mbp.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_smoking_status_rec smkstat
ON smkstat.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr)
AND smkstat.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex)
AND smkstat.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity)
AND smkstat.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm, mbp.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_smoking_still_seen smkseen
ON smkseen.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr)
AND smkseen.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex,h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex)
AND smkseen.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity)
AND smkseen.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm, mbp.pwtf_comm, smkstat.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_smoking_cessation smkcess
ON smkcess.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr)
AND smkcess.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex)
AND smkcess.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity)
AND smkcess.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm, mbp.pwtf_comm, smkstat.pwtf_comm, smkseen.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_hyper_treated hyptreat
ON hyptreat.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr, smkcess.age_group_10_yr)
AND hyptreat.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex, smkcess.sex)
AND hyptreat.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity, smkcess.race_ethnicity)
AND hyptreat.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm, mbp.pwtf_comm, smkstat.pwtf_comm, smkseen.pwtf_comm, smkcess.pwtf_comm)
;

-- MLCHC WORK AROUND
update esp_mdphnet.smk_diab_a_pwtf_comm_output
set 
bmi_denom_jan_12 = 0,
bmi_denom_apr_12 = 0,
bmi_denom_jul_12 = 0,
bmi_denom_oct_12 = 0,
bmi_denom_jan_13 = 0,
bmi_denom_apr_13 = 0,
bmi_denom_jul_13 = 0,
bmi_denom_oct_13 = 0,
bmi_denom_jan_14 = 0,
bmi_denom_apr_14 = 0,
bmi_denom_jul_14 = 0,
bmi_denom_oct_14 = 0,
bmi_denom_jan_15 = 0,
bmi_denom_apr_15 = 0,
bmi_denom_jul_15 = 0,
bmi_denom_oct_15 = 0,
bmi_denom_jan_16 = 0,
bmi_denom_apr_16 = 0,
bmi_denom_jul_16 = 0,
bmi_denom_oct_16 = 0
where age_group_10_year in ('0-9', '10-19');


--
-- Script shutdown section 
--

-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_dateofint CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_current CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_cases_all CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_prev_diabetes_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_med_dx_all CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_all_entries CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101159 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_dateofint CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101156 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101157 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101158 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_denom_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101154 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_all_events CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_prev_diabetes CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_act_diabetes CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_denom CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_asthma CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_bmi CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_bmi_denom CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_hyper CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_diab_events CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_unk_bmi_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_status_rec_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_status_rec_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_recorded_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking_status_rec CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.current_smokers_on_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_still_seen_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking_still_seen CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_cessation_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking_cessation CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_union_full_w_enc CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_systolic CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_union_full_w_enc CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_diastolic CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_controlled_bp CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_union_full_w_enc CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_measured_bp CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_rx_events CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_union_full_w_enc CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_hyper_treated CASCADE;





--Print output to screen
select * from esp_mdphnet.smk_diab_all_full_strat_output order by age_group_10_year, sex, race asc;

--Print output to screen
select * from esp_mdphnet.smk_diab_a_pwtf_comm_output order by pwtf_comm, age_group_10_year, sex, race asc;

set client_min_messages to notice;

