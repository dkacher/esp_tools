-- Step 183: SQL - FINAL - Bring together all of the values
CREATE  TABLE esp_mdphnet.smk_diab_all_full_strat_output WITHOUT OIDS  AS SELECT
coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr, b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, dia.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr, smkcess.age_group_10_yr, hyptreat.age_group_10_yr) as age_group_10_year, 
coalesce(s.sex, pd.sex, ad.sex, d.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex, smkcess.sex, hyptreat.sex) as sex,
coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity, smkcess.race_ethnicity, hyptreat.race_ethnicity) as race,
coalesce(smoke_status_rec_jan_12, 0) as smoke_status_rec_jan_12,
coalesce(smoke_status_rec_apr_12, 0) as smoke_status_rec_apr_12,
coalesce(smoke_status_rec_jul_12, 0) as smoke_status_rec_jul_12,
coalesce(smoke_status_rec_oct_12, 0) as smoke_status_rec_oct_12,
coalesce(smoke_status_rec_jan_13, 0) as smoke_status_rec_jan_13,
coalesce(smoke_status_rec_apr_13, 0) as smoke_status_rec_apr_13,
coalesce(smoke_status_rec_jul_13, 0) as smoke_status_rec_jul_13,
coalesce(smoke_status_rec_oct_13, 0) as smoke_status_rec_oct_13,
coalesce(smoke_status_rec_jan_14, 0) as smoke_status_rec_jan_14,
coalesce(smoke_status_rec_apr_14, 0) as smoke_status_rec_apr_14,
coalesce(smoke_status_rec_jul_14, 0) as smoke_status_rec_jul_14,
coalesce(smoke_status_rec_oct_14, 0) as smoke_status_rec_oct_14,
coalesce(smoke_status_rec_jan_15, 0) as smoke_status_rec_jan_15,
coalesce(smoke_status_rec_apr_15, 0) as smoke_status_rec_apr_15,
coalesce(smoke_status_rec_jul_15, 0) as smoke_status_rec_jul_15,
coalesce(smoke_status_rec_oct_15, 0) as smoke_status_rec_oct_15,
coalesce(smoke_status_rec_jan_16, 0) as smoke_status_rec_jan_16,
coalesce(smoke_status_rec_apr_16, 0) as smoke_status_rec_apr_16,
coalesce(smoke_status_rec_jul_16, 0) as smoke_status_rec_jul_16,
coalesce(smoke_status_rec_oct_16, 0) as smoke_status_rec_oct_16,
coalesce (curr_smoke_jan_12, 0 ) curr_smoke_jan_12,
coalesce (curr_smoke_apr_12, 0 ) curr_smoke_apr_12,
coalesce (curr_smoke_jul_12, 0 ) curr_smoke_jul_12,
coalesce (curr_smoke_oct_12, 0 ) curr_smoke_oct_12,
coalesce (curr_smoke_jan_13, 0 ) curr_smoke_jan_13,
coalesce (curr_smoke_apr_13, 0 ) curr_smoke_apr_13,
coalesce (curr_smoke_jul_13, 0 ) curr_smoke_jul_13,
coalesce (curr_smoke_oct_13, 0 ) curr_smoke_oct_13,
coalesce (curr_smoke_jan_14, 0 ) curr_smoke_jan_14,
coalesce (curr_smoke_apr_14, 0 ) curr_smoke_apr_14,
coalesce (curr_smoke_jul_14, 0 ) curr_smoke_jul_14,
coalesce (curr_smoke_oct_14, 0 ) curr_smoke_oct_14,
coalesce (curr_smoke_jan_15, 0 ) curr_smoke_jan_15,
coalesce (curr_smoke_apr_15, 0 ) curr_smoke_apr_15,
coalesce (curr_smoke_jul_15, 0 ) curr_smoke_jul_15,
coalesce (curr_smoke_oct_15, 0 ) curr_smoke_oct_15,
coalesce (curr_smoke_jan_16, 0 ) curr_smoke_jan_16,
coalesce (curr_smoke_apr_16, 0) curr_smoke_apr_16,
coalesce (curr_smoke_jul_16, 0) curr_smoke_jul_16,
coalesce (curr_smoke_oct_16, 0) curr_smoke_oct_16,
coalesce(smoke_still_seen_base_denom_jan_14, 0) as smoke_still_seen_base_denom_jan_14,
coalesce(smoke_still_seen_apr_14, 0) as smoke_still_seen_apr_14,
coalesce(smoke_still_seen_jul_14, 0) as smoke_still_seen_jul_14,
coalesce(smoke_still_seen_oct_14, 0) as smoke_still_seen_oct_14,
coalesce(smoke_still_seen_jan_15, 0) as smoke_still_seen_jan_15,
coalesce(smoke_still_seen_apr_15, 0) as smoke_still_seen_apr_15,
coalesce(smoke_still_seen_jul_15, 0) as smoke_still_seen_jul_15,
coalesce(smoke_still_seen_oct_15, 0) as smoke_still_seen_oct_15,
coalesce(smoke_still_seen_jan_16, 0) as smoke_still_seen_jan_16,
coalesce(smoke_still_seen_apr_16, 0) as smoke_still_seen_apr_16,
coalesce(smoke_still_seen_jul_16, 0) as smoke_still_seen_jul_16,
coalesce(smoke_still_seen_oct_16, 0) as smoke_still_seen_oct_16,
coalesce(smoke_cessation_apr_14, 0) as smoke_cessation_apr_14,
coalesce(smoke_cessation_jul_14, 0) as smoke_cessation_jul_14,
coalesce(smoke_cessation_oct_14, 0) as smoke_cessation_oct_14,
coalesce(smoke_cessation_jan_15, 0) as smoke_cessation_jan_15,
coalesce(smoke_cessation_apr_15, 0) as smoke_cessation_apr_15,
coalesce(smoke_cessation_jul_15, 0) as smoke_cessation_jul_15,
coalesce(smoke_cessation_oct_15, 0) as smoke_cessation_oct_15,
coalesce(smoke_cessation_jan_16, 0) as smoke_cessation_jan_16,
coalesce(smoke_cessation_apr_16, 0) as smoke_cessation_apr_16,
coalesce(smoke_cessation_jul_16, 0) as smoke_cessation_jul_16,
coalesce(smoke_cessation_oct_16, 0) as smoke_cessation_oct_16,
coalesce (t1_diab_prev_jan_12, 0 ) as t1_diab_prev_jan_12,
coalesce (t1_diab_prev_apr_12, 0 ) as t1_diab_prev_apr_12,
coalesce (t1_diab_prev_jul_12, 0 ) as t1_diab_prev_jul_12,
coalesce (t1_diab_prev_oct_12, 0 ) as t1_diab_prev_oct_12,
coalesce (t1_diab_prev_jan_13, 0 ) as t1_diab_prev_jan_13,
coalesce (t1_diab_prev_apr_13, 0 ) as t1_diab_prev_apr_13,
coalesce (t1_diab_prev_jul_13, 0 ) as t1_diab_prev_jul_13,
coalesce (t1_diab_prev_oct_13, 0 ) as t1_diab_prev_oct_13,
coalesce (t1_diab_prev_jan_14, 0 ) as t1_diab_prev_jan_14,
coalesce (t1_diab_prev_apr_14, 0 ) as t1_diab_prev_apr_14,
coalesce (t1_diab_prev_jul_14, 0 ) as t1_diab_prev_jul_14,
coalesce (t1_diab_prev_oct_14, 0 ) as t1_diab_prev_oct_14,
coalesce (t1_diab_prev_jan_15, 0 ) as t1_diab_prev_jan_15,
coalesce (t1_diab_prev_apr_15, 0 ) as t1_diab_prev_apr_15,
coalesce (t1_diab_prev_jul_15, 0 ) as t1_diab_prev_jul_15,
coalesce (t1_diab_prev_oct_15, 0 ) as t1_diab_prev_oct_15,
coalesce (t1_diab_prev_jan_16, 0 ) as t1_diab_prev_jan_16,
coalesce (t1_diab_prev_apr_16, 0 ) as t1_diab_prev_apr_16,
coalesce (t1_diab_prev_jul_16, 0 ) as t1_diab_prev_jul_16,
coalesce (t1_diab_prev_oct_16, 0 ) as t1_diab_prev_oct_16,
coalesce (act_t1_diab_jan_12, 0 ) as act_t1_diab_jan_12,
coalesce (act_t1_diab_apr_12, 0 ) as act_t1_diab_apr_12,
coalesce (act_t1_diab_jul_12, 0 ) as act_t1_diab_jul_12,
coalesce (act_t1_diab_oct_12, 0 ) as act_t1_diab_oct_12,
coalesce (act_t1_diab_jan_13, 0 ) as act_t1_diab_jan_13,
coalesce (act_t1_diab_apr_13, 0 ) as act_t1_diab_apr_13,
coalesce (act_t1_diab_jul_13, 0 ) as act_t1_diab_jul_13,
coalesce (act_t1_diab_oct_13, 0 ) as act_t1_diab_oct_13,
coalesce (act_t1_diab_jan_14, 0 ) as act_t1_diab_jan_14,
coalesce (act_t1_diab_apr_14, 0 ) as act_t1_diab_apr_14,
coalesce (act_t1_diab_jul_14, 0 ) as act_t1_diab_jul_14,
coalesce (act_t1_diab_oct_14, 0 ) as act_t1_diab_oct_14,
coalesce (act_t1_diab_jan_15, 0 ) as act_t1_diab_jan_15,
coalesce (act_t1_diab_apr_15, 0 ) as act_t1_diab_apr_15,
coalesce (act_t1_diab_jul_15, 0 ) as act_t1_diab_jul_15,
coalesce (act_t1_diab_oct_15, 0 ) as act_t1_diab_oct_15,
coalesce (act_t1_diab_jan_16, 0 ) as act_t1_diab_jan_16,
coalesce (act_t1_diab_apr_16, 0 ) as act_t1_diab_apr_16,
coalesce (act_t1_diab_jul_16, 0 ) as act_t1_diab_jul_16,
coalesce (act_t1_diab_oct_16, 0 ) as act_t1_diab_oct_16,
coalesce (t2_diab_prev_jan_12, 0 ) as t2_diab_prev_jan_12,
coalesce (t2_diab_prev_apr_12, 0 ) as t2_diab_prev_apr_12,
coalesce (t2_diab_prev_jul_12, 0 ) as t2_diab_prev_jul_12,
coalesce (t2_diab_prev_oct_12, 0 ) as t2_diab_prev_oct_12,
coalesce (t2_diab_prev_jan_13, 0 ) as t2_diab_prev_jan_13,
coalesce (t2_diab_prev_apr_13, 0 ) as t2_diab_prev_apr_13,
coalesce (t2_diab_prev_jul_13, 0 ) as t2_diab_prev_jul_13,
coalesce (t2_diab_prev_oct_13, 0 ) as t2_diab_prev_oct_13,
coalesce (t2_diab_prev_jan_14, 0 ) as t2_diab_prev_jan_14,
coalesce (t2_diab_prev_apr_14, 0 ) as t2_diab_prev_apr_14,
coalesce (t2_diab_prev_jul_14, 0 ) as t2_diab_prev_jul_14,
coalesce (t2_diab_prev_oct_14, 0 ) as t2_diab_prev_oct_14,
coalesce (t2_diab_prev_jan_15, 0 ) as t2_diab_prev_jan_15,
coalesce (t2_diab_prev_apr_15, 0 ) as t2_diab_prev_apr_15,
coalesce (t2_diab_prev_jul_15, 0 ) as t2_diab_prev_jul_15,
coalesce (t2_diab_prev_oct_15, 0 ) as t2_diab_prev_oct_15,
coalesce (t2_diab_prev_jan_16, 0 ) as t2_diab_prev_jan_16,
coalesce (t2_diab_prev_apr_16, 0 ) as t2_diab_prev_apr_16,
coalesce (t2_diab_prev_jul_16, 0 ) as t2_diab_prev_jul_16,
coalesce (t2_diab_prev_oct_16, 0 ) as t2_diab_prev_oct_16,
coalesce (act_t2_diab_jan_12, 0 ) as act_t2_diab_jan_12,
coalesce (act_t2_diab_apr_12, 0 ) as act_t2_diab_apr_12,
coalesce (act_t2_diab_jul_12, 0 ) as act_t2_diab_jul_12,
coalesce (act_t2_diab_oct_12, 0 ) as act_t2_diab_oct_12,
coalesce (act_t2_diab_jan_13, 0 ) as act_t2_diab_jan_13,
coalesce (act_t2_diab_apr_13, 0 ) as act_t2_diab_apr_13,
coalesce (act_t2_diab_jul_13, 0 ) as act_t2_diab_jul_13,
coalesce (act_t2_diab_oct_13, 0 ) as act_t2_diab_oct_13,
coalesce (act_t2_diab_jan_14, 0 ) as act_t2_diab_jan_14,
coalesce (act_t2_diab_apr_14, 0 ) as act_t2_diab_apr_14,
coalesce (act_t2_diab_jul_14, 0 ) as act_t2_diab_jul_14,
coalesce (act_t2_diab_oct_14, 0 ) as act_t2_diab_oct_14,
coalesce (act_t2_diab_jan_15, 0 ) as act_t2_diab_jan_15,
coalesce (act_t2_diab_apr_15, 0 ) as act_t2_diab_apr_15,
coalesce (act_t2_diab_jul_15, 0 ) as act_t2_diab_jul_15,
coalesce (act_t2_diab_oct_15, 0 ) as act_t2_diab_oct_15,
coalesce (act_t2_diab_jan_16, 0 ) as act_t2_diab_jan_16,
coalesce (act_t2_diab_apr_16, 0 ) as act_t2_diab_apr_16,
coalesce (act_t2_diab_jul_16, 0 ) as act_t2_diab_jul_16,
coalesce (act_t2_diab_oct_16, 0 ) as act_t2_diab_oct_16,
coalesce (t1_or_t2_diab_prev_jan_12, 0 ) as t1_or_t2_diab_prev_jan_12,
coalesce (t1_or_t2_diab_prev_apr_12, 0 ) as t1_or_t2_diab_prev_apr_12,
coalesce (t1_or_t2_diab_prev_jul_12, 0 ) as t1_or_t2_diab_prev_jul_12,
coalesce (t1_or_t2_diab_prev_oct_12, 0 ) as t1_or_t2_diab_prev_oct_12,
coalesce (t1_or_t2_diab_prev_jan_13, 0 ) as t1_or_t2_diab_prev_jan_13,
coalesce (t1_or_t2_diab_prev_apr_13, 0 ) as t1_or_t2_diab_prev_apr_13,
coalesce (t1_or_t2_diab_prev_jul_13, 0 ) as t1_or_t2_diab_prev_jul_13,
coalesce (t1_or_t2_diab_prev_oct_13, 0 ) as t1_or_t2_diab_prev_oct_13,
coalesce (t1_or_t2_diab_prev_jan_14, 0 ) as t1_or_t2_diab_prev_jan_14,
coalesce (t1_or_t2_diab_prev_apr_14, 0 ) as t1_or_t2_diab_prev_apr_14,
coalesce (t1_or_t2_diab_prev_jul_14, 0 ) as t1_or_t2_diab_prev_jul_14,
coalesce (t1_or_t2_diab_prev_oct_14, 0 ) as t1_or_t2_diab_prev_oct_14,
coalesce (t1_or_t2_diab_prev_jan_15, 0 ) as t1_or_t2_diab_prev_jan_15,
coalesce (t1_or_t2_diab_prev_apr_15, 0 ) as t1_or_t2_diab_prev_apr_15,
coalesce (t1_or_t2_diab_prev_jul_15, 0 ) as t1_or_t2_diab_prev_jul_15,
coalesce (t1_or_t2_diab_prev_oct_15, 0 ) as t1_or_t2_diab_prev_oct_15,
coalesce (t1_or_t2_diab_prev_jan_16, 0 ) as t1_or_t2_diab_prev_jan_16,
coalesce (t1_or_t2_diab_prev_apr_16, 0 ) as t1_or_t2_diab_prev_apr_16,
coalesce (t1_or_t2_diab_prev_jul_16, 0 ) as t1_or_t2_diab_prev_jul_16,
coalesce (t1_or_t2_diab_prev_oct_16, 0 ) as t1_or_t2_diab_prev_oct_16,
coalesce (act_t1_or_t2_diab_jan_12, 0 ) as act_t1_or_t2_diab_jan_12,
coalesce (act_t1_or_t2_diab_apr_12, 0 ) as act_t1_or_t2_diab_apr_12,
coalesce (act_t1_or_t2_diab_jul_12, 0 ) as act_t1_or_t2_diab_jul_12,
coalesce (act_t1_or_t2_diab_oct_12, 0 ) as act_t1_or_t2_diab_oct_12,
coalesce (act_t1_or_t2_diab_jan_13, 0 ) as act_t1_or_t2_diab_jan_13,
coalesce (act_t1_or_t2_diab_apr_13, 0 ) as act_t1_or_t2_diab_apr_13,
coalesce (act_t1_or_t2_diab_jul_13, 0 ) as act_t1_or_t2_diab_jul_13,
coalesce (act_t1_or_t2_diab_oct_13, 0 ) as act_t1_or_t2_diab_oct_13,
coalesce (act_t1_or_t2_diab_jan_14, 0 ) as act_t1_or_t2_diab_jan_14,
coalesce (act_t1_or_t2_diab_apr_14, 0 ) as act_t1_or_t2_diab_apr_14,
coalesce (act_t1_or_t2_diab_jul_14, 0 ) as act_t1_or_t2_diab_jul_14,
coalesce (act_t1_or_t2_diab_oct_14, 0 ) as act_t1_or_t2_diab_oct_14,
coalesce (act_t1_or_t2_diab_jan_15, 0 ) as act_t1_or_t2_diab_jan_15,
coalesce (act_t1_or_t2_diab_apr_15, 0 ) as act_t1_or_t2_diab_apr_15,
coalesce (act_t1_or_t2_diab_jul_15, 0 ) as act_t1_or_t2_diab_jul_15,
coalesce (act_t1_or_t2_diab_oct_15, 0 ) as act_t1_or_t2_diab_oct_15,
coalesce (act_t1_or_t2_diab_jan_16, 0 ) as act_t1_or_t2_diab_jan_16,
coalesce (act_t1_or_t2_diab_apr_16, 0 ) as act_t1_or_t2_diab_apr_16,
coalesce (act_t1_or_t2_diab_jul_16, 0 ) as act_t1_or_t2_diab_jul_16,
coalesce (act_t1_or_t2_diab_oct_16, 0 ) as act_t1_or_t2_diab_oct_16,
coalesce (asthma_jan_12, 0 ) as asthma_jan_12,
coalesce (asthma_apr_12, 0 ) as asthma_apr_12,
coalesce (asthma_jul_12, 0 ) as asthma_jul_12,
coalesce (asthma_oct_12, 0 ) as asthma_oct_12,
coalesce (asthma_jan_13, 0 ) as asthma_jan_13,
coalesce (asthma_apr_13, 0 ) as asthma_apr_13,
coalesce (asthma_jul_13, 0 ) as asthma_jul_13,
coalesce (asthma_oct_13, 0 ) as asthma_oct_13,
coalesce (asthma_jan_14, 0 ) as asthma_jan_14,
coalesce (asthma_apr_14, 0 ) as asthma_apr_14,
coalesce (asthma_jul_14, 0 ) as asthma_jul_14,
coalesce (asthma_oct_14, 0 ) as asthma_oct_14,
coalesce (asthma_jan_15, 0 ) as asthma_jan_15,
coalesce (asthma_apr_15, 0 ) as asthma_apr_15,
coalesce (asthma_jul_15, 0 ) as asthma_jul_15,
coalesce (asthma_oct_15, 0 ) as asthma_oct_15,
coalesce (asthma_jan_16, 0 ) as asthma_jan_16,
coalesce (asthma_apr_16, 0 ) as asthma_apr_16,
coalesce (asthma_jul_16, 0 ) as asthma_jul_16,
coalesce (asthma_oct_16, 0 ) as asthma_oct_16,
coalesce (denominator_jan_12, 0 ) as denominator_jan_12,
coalesce (denominator_apr_12, 0 ) as denominator_apr_12,
coalesce (denominator_jul_12, 0 ) as denominator_jul_12,
coalesce (denominator_oct_12, 0 ) as denominator_oct_12,
coalesce (denominator_jan_13, 0 ) as denominator_jan_13,
coalesce (denominator_apr_13, 0 ) as denominator_apr_13,
coalesce (denominator_jul_13, 0 ) as denominator_jul_13,
coalesce (denominator_oct_13, 0 ) as denominator_oct_13,
coalesce (denominator_jan_14, 0 ) as denominator_jan_14,
coalesce (denominator_apr_14, 0 ) as denominator_apr_14,
coalesce (denominator_jul_14, 0 ) as denominator_jul_14,
coalesce (denominator_oct_14, 0 ) as denominator_oct_14,
coalesce (denominator_jan_15, 0 ) as denominator_jan_15,
coalesce (denominator_apr_15, 0 ) as denominator_apr_15,
coalesce (denominator_jul_15, 0 ) as denominator_jul_15,
coalesce (denominator_oct_15, 0 ) as denominator_oct_15,
coalesce (denominator_jan_16, 0 ) as denominator_jan_16,
coalesce (denominator_apr_16, 0 ) as denominator_apr_16,
coalesce (denominator_jul_16, 0 ) as denominator_jul_16,
coalesce (denominator_oct_16, 0 ) as denominator_oct_16,
coalesce(overweight_jan_12, 0)  as overweight_jan_12,
coalesce(overweight_apr_12, 0)  as overweight_apr_12,
coalesce(overweight_jul_12, 0)  as overweight_jul_12,
coalesce(overweight_oct_12, 0)  as overweight_oct_12,
coalesce(overweight_jan_13, 0)  as overweight_jan_13,
coalesce(overweight_apr_13, 0)  as overweight_apr_13,
coalesce(overweight_jul_13, 0)  as overweight_jul_13,
coalesce(overweight_oct_13, 0)  as overweight_oct_13,
coalesce(overweight_jan_14, 0)  as overweight_jan_14,
coalesce(overweight_apr_14, 0)  as overweight_apr_14,
coalesce(overweight_jul_14, 0)  as overweight_jul_14,
coalesce(overweight_oct_14, 0)  as overweight_oct_14,
coalesce(overweight_jan_15, 0)  as overweight_jan_15,
coalesce(overweight_apr_15, 0)  as overweight_apr_15,
coalesce(overweight_jul_15, 0)  as overweight_jul_15,
coalesce(overweight_oct_15, 0)  as overweight_oct_15,
coalesce(overweight_jan_16, 0)  as overweight_jan_16,
coalesce(overweight_apr_16, 0)  as overweight_apr_16,
coalesce(overweight_jul_16, 0)  as overweight_jul_16,
coalesce(overweight_oct_16, 0)  as overweight_oct_16,
coalesce(obese_jan_12, 0)  as obese_jan_12,
coalesce(obese_apr_12, 0)  as obese_apr_12,
coalesce(obese_jul_12, 0)  as obese_jul_12,
coalesce(obese_oct_12, 0)  as obese_oct_12,
coalesce(obese_jan_13, 0)  as obese_jan_13,
coalesce(obese_apr_13, 0)  as obese_apr_13,
coalesce(obese_jul_13, 0)  as obese_jul_13,
coalesce(obese_oct_13, 0)  as obese_oct_13,
coalesce(obese_jan_14, 0)  as obese_jan_14,
coalesce(obese_apr_14, 0)  as obese_apr_14,
coalesce(obese_jul_14, 0)  as obese_jul_14,
coalesce(obese_oct_14, 0)  as obese_oct_14,
coalesce(obese_jan_15, 0)  as obese_jan_15,
coalesce(obese_apr_15, 0)  as obese_apr_15,
coalesce(obese_jul_15, 0)  as obese_jul_15,
coalesce(obese_oct_15, 0)  as obese_oct_15,
coalesce(obese_jan_16, 0)  as obese_jan_16,
coalesce(obese_apr_16, 0)  as obese_apr_16,
coalesce(obese_jul_16, 0)  as obese_jul_16,
coalesce(obese_oct_16, 0)  as obese_oct_16,
coalesce(overweight_or_obese_jan_12, 0)  as overweight_or_obese_jan_12,
coalesce(overweight_or_obese_apr_12, 0)  as overweight_or_obese_apr_12,
coalesce(overweight_or_obese_jul_12, 0)  as overweight_or_obese_jul_12,
coalesce(overweight_or_obese_oct_12, 0)  as overweight_or_obese_oct_12,
coalesce(overweight_or_obese_jan_13, 0)  as overweight_or_obese_jan_13,
coalesce(overweight_or_obese_apr_13, 0)  as overweight_or_obese_apr_13,
coalesce(overweight_or_obese_jul_13, 0)  as overweight_or_obese_jul_13,
coalesce(overweight_or_obese_oct_13, 0)  as overweight_or_obese_oct_13,
coalesce(overweight_or_obese_jan_14, 0)  as overweight_or_obese_jan_14,
coalesce(overweight_or_obese_apr_14, 0)  as overweight_or_obese_apr_14,
coalesce(overweight_or_obese_jul_14, 0)  as overweight_or_obese_jul_14,
coalesce(overweight_or_obese_oct_14, 0)  as overweight_or_obese_oct_14,
coalesce(overweight_or_obese_jan_15, 0)  as overweight_or_obese_jan_15,
coalesce(overweight_or_obese_apr_15, 0)  as overweight_or_obese_apr_15,
coalesce(overweight_or_obese_jul_15, 0)  as overweight_or_obese_jul_15,
coalesce(overweight_or_obese_oct_15, 0)  as overweight_or_obese_oct_15,
coalesce(overweight_or_obese_jan_16, 0)  as overweight_or_obese_jan_16,
coalesce(overweight_or_obese_apr_16, 0)  as overweight_or_obese_apr_16,
coalesce(overweight_or_obese_jul_16, 0)  as overweight_or_obese_jul_16,
coalesce(overweight_or_obese_oct_16, 0)  as overweight_or_obese_oct_16,
coalesce(no_measured_bmi_jan_12, 0 )  as   no_measured_bmi_jan_12,
coalesce(no_measured_bmi_apr_12, 0 )  as   no_measured_bmi_apr_12,
coalesce(no_measured_bmi_jul_12, 0 )  as   no_measured_bmi_jul_12,
coalesce(no_measured_bmi_oct_12, 0 )  as   no_measured_bmi_oct_12,
coalesce(no_measured_bmi_jan_13, 0 )  as   no_measured_bmi_jan_13,
coalesce(no_measured_bmi_apr_13, 0 )  as   no_measured_bmi_apr_13,
coalesce(no_measured_bmi_jul_13, 0 )  as   no_measured_bmi_jul_13,
coalesce(no_measured_bmi_oct_13, 0 )  as   no_measured_bmi_oct_13,
coalesce(no_measured_bmi_jan_14, 0 )  as   no_measured_bmi_jan_14,
coalesce(no_measured_bmi_apr_14, 0 )  as   no_measured_bmi_apr_14,
coalesce(no_measured_bmi_jul_14, 0 )  as   no_measured_bmi_jul_14,
coalesce(no_measured_bmi_oct_14, 0 )  as   no_measured_bmi_oct_14,
coalesce(no_measured_bmi_jan_15, 0 )  as   no_measured_bmi_jan_15,
coalesce(no_measured_bmi_apr_15, 0 )  as   no_measured_bmi_apr_15,
coalesce(no_measured_bmi_jul_15, 0 )  as   no_measured_bmi_jul_15,
coalesce(no_measured_bmi_oct_15, 0 )  as   no_measured_bmi_oct_15,
coalesce(no_measured_bmi_jan_16, 0 )  as   no_measured_bmi_jan_16,
coalesce(no_measured_bmi_apr_16, 0 )  as   no_measured_bmi_apr_16,
coalesce(no_measured_bmi_jul_16, 0 )  as   no_measured_bmi_jul_16,
coalesce(no_measured_bmi_oct_16, 0 )  as   no_measured_bmi_oct_16,
coalesce( denominator_jan_12, 0)  as  bmi_denom_jan_12,
coalesce( denominator_apr_12, 0)  as  bmi_denom_apr_12,
coalesce( denominator_jul_12, 0)  as  bmi_denom_jul_12,
coalesce( denominator_oct_12, 0)  as  bmi_denom_oct_12,
coalesce( denominator_jan_13, 0)  as  bmi_denom_jan_13,
coalesce( denominator_apr_13, 0)  as  bmi_denom_apr_13,
coalesce( denominator_jul_13, 0)  as  bmi_denom_jul_13,
coalesce( denominator_oct_13, 0)  as  bmi_denom_oct_13,
coalesce( denominator_jan_14, 0)  as  bmi_denom_jan_14,
coalesce( denominator_apr_14, 0)  as  bmi_denom_apr_14,
coalesce( denominator_jul_14, 0)  as  bmi_denom_jul_14,
coalesce( denominator_oct_14, 0)  as  bmi_denom_oct_14,
coalesce( denominator_jan_15, 0)  as  bmi_denom_jan_15,
coalesce( denominator_apr_15, 0)  as  bmi_denom_apr_15,
coalesce( denominator_jul_15, 0)  as  bmi_denom_jul_15,
coalesce( denominator_oct_15, 0)  as  bmi_denom_oct_15,
coalesce( denominator_jan_16, 0)  as  bmi_denom_jan_16,
coalesce( denominator_apr_16, 0)  as  bmi_denom_apr_16,
coalesce( denominator_jul_16, 0)  as  bmi_denom_jul_16,
coalesce( denominator_oct_16, 0)  as  bmi_denom_oct_16,
coalesce(hypertension_jan_12, 0 )  as   hypertension_jan_12,
coalesce(hypertension_apr_12, 0 )  as   hypertension_apr_12,
coalesce(hypertension_jul_12, 0 )  as   hypertension_jul_12,
coalesce(hypertension_oct_12, 0 )  as   hypertension_oct_12,
coalesce(hypertension_jan_13, 0 )  as   hypertension_jan_13,
coalesce(hypertension_apr_13, 0 )  as   hypertension_apr_13,
coalesce(hypertension_jul_13, 0 )  as   hypertension_jul_13,
coalesce(hypertension_oct_13, 0 )  as   hypertension_oct_13,
coalesce(hypertension_jan_14, 0 )  as   hypertension_jan_14,
coalesce(hypertension_apr_14, 0 )  as   hypertension_apr_14,
coalesce(hypertension_jul_14, 0 )  as   hypertension_jul_14,
coalesce(hypertension_oct_14, 0 )  as   hypertension_oct_14,
coalesce(hypertension_jan_15, 0 )  as   hypertension_jan_15,
coalesce(hypertension_apr_15, 0 )  as   hypertension_apr_15,
coalesce(hypertension_jul_15, 0 )  as   hypertension_jul_15,
coalesce(hypertension_oct_15, 0 )  as   hypertension_oct_15,
coalesce(hypertension_jan_16, 0 )  as   hypertension_jan_16,
coalesce(hypertension_apr_16, 0 )  as   hypertension_apr_16,
coalesce(hypertension_jul_16, 0 )  as   hypertension_jul_16,
coalesce(hypertension_oct_16, 0 )  as   hypertension_oct_16,
coalesce(systolic_avg_jan_12, 0) as systolic_avg_jan_12, 
coalesce(systolic_avg_apr_12, 0) as systolic_avg_apr_12, 
coalesce(systolic_avg_jul_12, 0) as systolic_avg_jul_12, 
coalesce(systolic_avg_oct_12, 0) as systolic_avg_oct_12,
coalesce(systolic_avg_jan_13, 0) as systolic_avg_jan_13, 
coalesce(systolic_avg_apr_13, 0) as systolic_avg_apr_13, 
coalesce(systolic_avg_jul_13, 0) as systolic_avg_jul_13, 
coalesce(systolic_avg_oct_13, 0) as systolic_avg_oct_13,
coalesce(systolic_avg_jan_14, 0) as systolic_avg_jan_14, 
coalesce(systolic_avg_apr_14, 0) as systolic_avg_apr_14, 
coalesce(systolic_avg_jul_14, 0) as systolic_avg_jul_14, 
coalesce(systolic_avg_oct_14, 0) as systolic_avg_oct_14,
coalesce(systolic_avg_jan_15, 0) as systolic_avg_jan_15, 
coalesce(systolic_avg_apr_15, 0) as systolic_avg_apr_15, 
coalesce(systolic_avg_jul_15, 0) as systolic_avg_jul_15, 
coalesce(systolic_avg_oct_15, 0) as systolic_avg_oct_15,
coalesce(systolic_avg_jan_16, 0) as systolic_avg_jan_16, 
coalesce(systolic_avg_apr_16, 0) as systolic_avg_apr_16, 
coalesce(systolic_avg_jul_16, 0) as systolic_avg_jul_16, 
coalesce(systolic_avg_oct_16, 0) as systolic_avg_oct_16,
coalesce(diastolic_avg_jan_12, 0) as diastolic_avg_jan_12, 
coalesce(diastolic_avg_apr_12, 0) as diastolic_avg_apr_12, 
coalesce(diastolic_avg_jul_12, 0) as diastolic_avg_jul_12, 
coalesce(diastolic_avg_oct_12, 0) as diastolic_avg_oct_12,
coalesce(diastolic_avg_jan_13, 0) as diastolic_avg_jan_13, 
coalesce(diastolic_avg_apr_13, 0) as diastolic_avg_apr_13, 
coalesce(diastolic_avg_jul_13, 0) as diastolic_avg_jul_13, 
coalesce(diastolic_avg_oct_13, 0) as diastolic_avg_oct_13,
coalesce(diastolic_avg_jan_14, 0) as diastolic_avg_jan_14, 
coalesce(diastolic_avg_apr_14, 0) as diastolic_avg_apr_14, 
coalesce(diastolic_avg_jul_14, 0) as diastolic_avg_jul_14, 
coalesce(diastolic_avg_oct_14, 0) as diastolic_avg_oct_14,
coalesce(diastolic_avg_jan_15, 0) as diastolic_avg_jan_15, 
coalesce(diastolic_avg_apr_15, 0) as diastolic_avg_apr_15, 
coalesce(diastolic_avg_jul_15, 0) as diastolic_avg_jul_15, 
coalesce(diastolic_avg_oct_15, 0) as diastolic_avg_oct_15,
coalesce(diastolic_avg_jan_16, 0) as diastolic_avg_jan_16, 
coalesce(diastolic_avg_apr_16, 0) as diastolic_avg_apr_16, 
coalesce(diastolic_avg_jul_16, 0) as diastolic_avg_jul_16, 
coalesce(diastolic_avg_oct_16, 0) as diastolic_avg_oct_16,
coalesce(controlled_hyper_jan_12, 0) as controlled_hyper_jan_12,
coalesce(controlled_hyper_apr_12, 0) as controlled_hyper_apr_12,
coalesce(controlled_hyper_jul_12, 0) as controlled_hyper_jul_12,
coalesce(controlled_hyper_oct_12, 0) as controlled_hyper_oct_12,
coalesce(controlled_hyper_jan_13, 0) as controlled_hyper_jan_13,
coalesce(controlled_hyper_apr_13, 0) as controlled_hyper_apr_13,
coalesce(controlled_hyper_jul_13, 0) as controlled_hyper_jul_13,
coalesce(controlled_hyper_oct_13, 0) as controlled_hyper_oct_13,
coalesce(controlled_hyper_jan_14, 0) as controlled_hyper_jan_14,
coalesce(controlled_hyper_apr_14, 0) as controlled_hyper_apr_14,
coalesce(controlled_hyper_jul_14, 0) as controlled_hyper_jul_14,
coalesce(controlled_hyper_oct_14, 0) as controlled_hyper_oct_14,
coalesce(controlled_hyper_jan_15, 0) as controlled_hyper_jan_15,
coalesce(controlled_hyper_apr_15, 0) as controlled_hyper_apr_15,
coalesce(controlled_hyper_jul_15, 0) as controlled_hyper_jul_15,
coalesce(controlled_hyper_oct_15, 0) as controlled_hyper_oct_15,
coalesce(controlled_hyper_jan_16, 0) as controlled_hyper_jan_16,
coalesce(controlled_hyper_apr_16, 0) as controlled_hyper_apr_16,
coalesce(controlled_hyper_jul_16, 0) as controlled_hyper_jul_16,
coalesce(controlled_hyper_oct_16, 0) as controlled_hyper_oct_16,
coalesce(hypertension_treated_jan_12, 0) as hypertension_treated_jan_12,
coalesce(hypertension_treated_apr_12, 0) as hypertension_treated_apr_12,
coalesce(hypertension_treated_jul_12, 0) as hypertension_treated_jul_12,
coalesce(hypertension_treated_oct_12, 0) as hypertension_treated_oct_12,
coalesce(hypertension_treated_jan_13, 0) as hypertension_treated_jan_13,
coalesce(hypertension_treated_apr_13, 0) as hypertension_treated_apr_13,
coalesce(hypertension_treated_jul_13, 0) as hypertension_treated_jul_13,
coalesce(hypertension_treated_oct_13, 0) as hypertension_treated_oct_13,
coalesce(hypertension_treated_jan_14, 0) as hypertension_treated_jan_14,
coalesce(hypertension_treated_apr_14, 0) as hypertension_treated_apr_14,
coalesce(hypertension_treated_jul_14, 0) as hypertension_treated_jul_14,
coalesce(hypertension_treated_oct_14, 0) as hypertension_treated_oct_14,
coalesce(hypertension_treated_jan_15, 0) as hypertension_treated_jan_15,
coalesce(hypertension_treated_apr_15, 0) as hypertension_treated_apr_15,
coalesce(hypertension_treated_jul_15, 0) as hypertension_treated_jul_15,
coalesce(hypertension_treated_oct_15, 0) as hypertension_treated_oct_15,
coalesce(hypertension_treated_jan_16, 0) as hypertension_treated_jan_16,
coalesce(hypertension_treated_apr_16, 0) as hypertension_treated_apr_16,
coalesce(hypertension_treated_jul_16, 0) as hypertension_treated_jul_16,
coalesce(hypertension_treated_oct_16, 0) as hypertension_treated_oct_16,
coalesce(measured_bp_jan_12, 0) as measured_bp_jan_12,
coalesce(measured_bp_apr_12, 0) as measured_bp_apr_12,
coalesce(measured_bp_jul_12, 0) as measured_bp_jul_12,
coalesce(measured_bp_oct_12, 0) as measured_bp_oct_12,
coalesce(measured_bp_jan_13, 0) as measured_bp_jan_13,
coalesce(measured_bp_apr_13, 0) as measured_bp_apr_13,
coalesce(measured_bp_jul_13, 0) as measured_bp_jul_13,
coalesce(measured_bp_oct_13, 0) as measured_bp_oct_13,
coalesce(measured_bp_jan_14, 0) as measured_bp_jan_14,
coalesce(measured_bp_apr_14, 0) as measured_bp_apr_14,
coalesce(measured_bp_jul_14, 0) as measured_bp_jul_14,
coalesce(measured_bp_oct_14, 0) as measured_bp_oct_14,
coalesce(measured_bp_jan_15, 0) as measured_bp_jan_15,
coalesce(measured_bp_apr_15, 0) as measured_bp_apr_15,
coalesce(measured_bp_jul_15, 0) as measured_bp_jul_15,
coalesce(measured_bp_oct_15, 0) as measured_bp_oct_15,
coalesce(measured_bp_jan_16, 0) as measured_bp_jan_16,
coalesce(measured_bp_apr_16, 0) as measured_bp_apr_16,
coalesce(measured_bp_jul_16, 0) as measured_bp_jul_16,
coalesce(measured_bp_oct_16, 0) as measured_bp_oct_16      
FROM
esp_mdphnet.smk_diab_smoking_full_strat s
FULL OUTER JOIN esp_mdphnet.smk_diab_prev_diabetes_full_strat pd
ON pd.age_group_10_yr = s.age_group_10_yr
AND pd.sex = s.sex
AND pd.race_ethnicity = s.race_ethnicity
FULL OUTER JOIN esp_mdphnet.smk_diab_act_diab_full_strat ad
ON ad.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr)
AND ad.sex = coalesce(s.sex, pd.sex)
AND ad.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_denom_full_strat d
ON d.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr)
AND d.sex = coalesce(s.sex, pd.sex, ad.sex)
AND d.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_asthma_active_full_strat a
ON a.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr)
AND a.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex)
AND a.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_bmi_full_strat b
ON b.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr)
AND b.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex)
AND b.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity)
-- FULL OUTER JOIN esp_mdphnet.smk_diab_bmi_denom_full_strat bd
-- ON bd.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr)
-- AND bd.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex)
-- AND bd.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_hyper_full_strat h
ON h.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr)
AND h.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex)
AND h.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_systolic_full_strat sys
ON sys.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr)
AND sys.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex)
AND sys.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_diastolic_full_strat dia
ON dia.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr)
AND dia.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex)
AND dia.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_controlled_bp_full_strat cbp
ON cbp.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr)
AND cbp.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex)
AND cbp.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_bp_measured_full_strat mbp
ON mbp.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr)
AND mbp.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex)
AND mbp.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_smoking_recorded_full_strat smkstat
ON smkstat.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr)
AND smkstat.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex)
AND smkstat.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_smoking_still_seen_full_strat smkseen
ON smkseen.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr)
AND smkseen.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex)
AND smkseen.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_smoking_cessation_full_strat smkcess
ON smkcess.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr)
AND smkcess.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex)
AND smkcess.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity)
FULL OUTER JOIN esp_mdphnet.smk_diab_hyper_treated_full_strat hyptreat
ON hyptreat.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr, smkcess.age_group_10_yr)
AND hyptreat.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex, smkcess.sex)
AND hyptreat.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity, smkcess.race_ethnicity)
;

-- MLCHC WORK AROUND
update esp_mdphnet.smk_diab_all_full_strat_output 
set 
bmi_denom_jan_12 = 0,
bmi_denom_apr_12 = 0,
bmi_denom_jul_12 = 0,
bmi_denom_oct_12 = 0,
bmi_denom_jan_13 = 0,
bmi_denom_apr_13 = 0,
bmi_denom_jul_13 = 0,
bmi_denom_oct_13 = 0,
bmi_denom_jan_14 = 0,
bmi_denom_apr_14 = 0,
bmi_denom_jul_14 = 0,
bmi_denom_oct_14 = 0,
bmi_denom_jan_15 = 0,
bmi_denom_apr_15 = 0,
bmi_denom_jul_15 = 0,
bmi_denom_oct_15 = 0,
bmi_denom_jan_16 = 0,
bmi_denom_apr_16 = 0,
bmi_denom_jul_16 = 0,
bmi_denom_oct_16 = 0
where age_group_10_year in ('0-9', '10-19');



-- Step 183: SQL - FINAL - Bring together all of the values for PWTF Community
CREATE  TABLE esp_mdphnet.smk_diab_a_pwtf_comm_output  WITHOUT OIDS  AS SELECT
coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr, b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, dia.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr, smkcess.age_group_10_yr, hyptreat.age_group_10_yr) as age_group_10_year, 
coalesce(s.sex, pd.sex, ad.sex, d.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex, smkcess.sex, hyptreat.sex) as sex,
coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity, smkcess.race_ethnicity, hyptreat.race_ethnicity) as race,
coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm, mbp.pwtf_comm, smkstat.pwtf_comm, smkseen.pwtf_comm, smkcess.pwtf_comm) as pwtf_comm,
coalesce(smoke_status_rec_jan_12, 0) as smoke_status_rec_jan_12,
coalesce(smoke_status_rec_apr_12, 0) as smoke_status_rec_apr_12,
coalesce(smoke_status_rec_jul_12, 0) as smoke_status_rec_jul_12,
coalesce(smoke_status_rec_oct_12, 0) as smoke_status_rec_oct_12,
coalesce(smoke_status_rec_jan_13, 0) as smoke_status_rec_jan_13,
coalesce(smoke_status_rec_apr_13, 0) as smoke_status_rec_apr_13,
coalesce(smoke_status_rec_jul_13, 0) as smoke_status_rec_jul_13,
coalesce(smoke_status_rec_oct_13, 0) as smoke_status_rec_oct_13,
coalesce(smoke_status_rec_jan_14, 0) as smoke_status_rec_jan_14,
coalesce(smoke_status_rec_apr_14, 0) as smoke_status_rec_apr_14,
coalesce(smoke_status_rec_jul_14, 0) as smoke_status_rec_jul_14,
coalesce(smoke_status_rec_oct_14, 0) as smoke_status_rec_oct_14,
coalesce(smoke_status_rec_jan_15, 0) as smoke_status_rec_jan_15,
coalesce(smoke_status_rec_apr_15, 0) as smoke_status_rec_apr_15,
coalesce(smoke_status_rec_jul_15, 0) as smoke_status_rec_jul_15,
coalesce(smoke_status_rec_oct_15, 0) as smoke_status_rec_oct_15,
coalesce(smoke_status_rec_jan_16, 0) as smoke_status_rec_jan_16,
coalesce(smoke_status_rec_apr_16, 0) as smoke_status_rec_apr_16,
coalesce(smoke_status_rec_jul_16, 0) as smoke_status_rec_jul_16,
coalesce(smoke_status_rec_oct_16, 0) as smoke_status_rec_oct_16,
coalesce (curr_smoke_jan_12, 0 ) curr_smoke_jan_12,
coalesce (curr_smoke_apr_12, 0 ) curr_smoke_apr_12,
coalesce (curr_smoke_jul_12, 0 ) curr_smoke_jul_12,
coalesce (curr_smoke_oct_12, 0 ) curr_smoke_oct_12,
coalesce (curr_smoke_jan_13, 0 ) curr_smoke_jan_13,
coalesce (curr_smoke_apr_13, 0 ) curr_smoke_apr_13,
coalesce (curr_smoke_jul_13, 0 ) curr_smoke_jul_13,
coalesce (curr_smoke_oct_13, 0 ) curr_smoke_oct_13,
coalesce (curr_smoke_jan_14, 0 ) curr_smoke_jan_14,
coalesce (curr_smoke_apr_14, 0 ) curr_smoke_apr_14,
coalesce (curr_smoke_jul_14, 0 ) curr_smoke_jul_14,
coalesce (curr_smoke_oct_14, 0 ) curr_smoke_oct_14,
coalesce (curr_smoke_jan_15, 0 ) curr_smoke_jan_15,
coalesce (curr_smoke_apr_15, 0 ) curr_smoke_apr_15,
coalesce (curr_smoke_jul_15, 0 ) curr_smoke_jul_15,
coalesce (curr_smoke_oct_15, 0 ) curr_smoke_oct_15,
coalesce (curr_smoke_jan_16, 0 ) curr_smoke_jan_16,
coalesce (curr_smoke_apr_16, 0) curr_smoke_apr_16,
coalesce (curr_smoke_jul_16, 0) curr_smoke_jul_16,
coalesce (curr_smoke_oct_16, 0) curr_smoke_oct_16,
coalesce(smoke_still_seen_base_denom_jan_14, 0) as smoke_still_seen_base_denom_jan_14,
coalesce(smoke_still_seen_apr_14, 0) as smoke_still_seen_apr_14,
coalesce(smoke_still_seen_jul_14, 0) as smoke_still_seen_jul_14,
coalesce(smoke_still_seen_oct_14, 0) as smoke_still_seen_oct_14,
coalesce(smoke_still_seen_jan_15, 0) as smoke_still_seen_jan_15,
coalesce(smoke_still_seen_apr_15, 0) as smoke_still_seen_apr_15,
coalesce(smoke_still_seen_jul_15, 0) as smoke_still_seen_jul_15,
coalesce(smoke_still_seen_oct_15, 0) as smoke_still_seen_oct_15,
coalesce(smoke_still_seen_jan_16, 0) as smoke_still_seen_jan_16,
coalesce(smoke_still_seen_apr_16, 0) as smoke_still_seen_apr_16,
coalesce(smoke_still_seen_jul_16, 0) as smoke_still_seen_jul_16,
coalesce(smoke_still_seen_oct_16, 0) as smoke_still_seen_oct_16,
coalesce(smoke_cessation_apr_14, 0) as smoke_cessation_apr_14,
coalesce(smoke_cessation_jul_14, 0) as smoke_cessation_jul_14,
coalesce(smoke_cessation_oct_14, 0) as smoke_cessation_oct_14,
coalesce(smoke_cessation_jan_15, 0) as smoke_cessation_jan_15,
coalesce(smoke_cessation_apr_15, 0) as smoke_cessation_apr_15,
coalesce(smoke_cessation_jul_15, 0) as smoke_cessation_jul_15,
coalesce(smoke_cessation_oct_15, 0) as smoke_cessation_oct_15,
coalesce(smoke_cessation_jan_16, 0) as smoke_cessation_jan_16,
coalesce(smoke_cessation_apr_16, 0) as smoke_cessation_apr_16,
coalesce(smoke_cessation_jul_16, 0) as smoke_cessation_jul_16,
coalesce(smoke_cessation_oct_16, 0) as smoke_cessation_oct_16,
coalesce (t1_diab_prev_jan_12, 0 ) as t1_diab_prev_jan_12,
coalesce (t1_diab_prev_apr_12, 0 ) as t1_diab_prev_apr_12,
coalesce (t1_diab_prev_jul_12, 0 ) as t1_diab_prev_jul_12,
coalesce (t1_diab_prev_oct_12, 0 ) as t1_diab_prev_oct_12,
coalesce (t1_diab_prev_jan_13, 0 ) as t1_diab_prev_jan_13,
coalesce (t1_diab_prev_apr_13, 0 ) as t1_diab_prev_apr_13,
coalesce (t1_diab_prev_jul_13, 0 ) as t1_diab_prev_jul_13,
coalesce (t1_diab_prev_oct_13, 0 ) as t1_diab_prev_oct_13,
coalesce (t1_diab_prev_jan_14, 0 ) as t1_diab_prev_jan_14,
coalesce (t1_diab_prev_apr_14, 0 ) as t1_diab_prev_apr_14,
coalesce (t1_diab_prev_jul_14, 0 ) as t1_diab_prev_jul_14,
coalesce (t1_diab_prev_oct_14, 0 ) as t1_diab_prev_oct_14,
coalesce (t1_diab_prev_jan_15, 0 ) as t1_diab_prev_jan_15,
coalesce (t1_diab_prev_apr_15, 0 ) as t1_diab_prev_apr_15,
coalesce (t1_diab_prev_jul_15, 0 ) as t1_diab_prev_jul_15,
coalesce (t1_diab_prev_oct_15, 0 ) as t1_diab_prev_oct_15,
coalesce (t1_diab_prev_jan_16, 0 ) as t1_diab_prev_jan_16,
coalesce (t1_diab_prev_apr_16, 0 ) as t1_diab_prev_apr_16,
coalesce (t1_diab_prev_jul_16, 0 ) as t1_diab_prev_jul_16,
coalesce (t1_diab_prev_oct_16, 0 ) as t1_diab_prev_oct_16,
coalesce (act_t1_diab_jan_12, 0 ) as act_t1_diab_jan_12,
coalesce (act_t1_diab_apr_12, 0 ) as act_t1_diab_apr_12,
coalesce (act_t1_diab_jul_12, 0 ) as act_t1_diab_jul_12,
coalesce (act_t1_diab_oct_12, 0 ) as act_t1_diab_oct_12,
coalesce (act_t1_diab_jan_13, 0 ) as act_t1_diab_jan_13,
coalesce (act_t1_diab_apr_13, 0 ) as act_t1_diab_apr_13,
coalesce (act_t1_diab_jul_13, 0 ) as act_t1_diab_jul_13,
coalesce (act_t1_diab_oct_13, 0 ) as act_t1_diab_oct_13,
coalesce (act_t1_diab_jan_14, 0 ) as act_t1_diab_jan_14,
coalesce (act_t1_diab_apr_14, 0 ) as act_t1_diab_apr_14,
coalesce (act_t1_diab_jul_14, 0 ) as act_t1_diab_jul_14,
coalesce (act_t1_diab_oct_14, 0 ) as act_t1_diab_oct_14,
coalesce (act_t1_diab_jan_15, 0 ) as act_t1_diab_jan_15,
coalesce (act_t1_diab_apr_15, 0 ) as act_t1_diab_apr_15,
coalesce (act_t1_diab_jul_15, 0 ) as act_t1_diab_jul_15,
coalesce (act_t1_diab_oct_15, 0 ) as act_t1_diab_oct_15,
coalesce (act_t1_diab_jan_16, 0 ) as act_t1_diab_jan_16,
coalesce (act_t1_diab_apr_16, 0 ) as act_t1_diab_apr_16,
coalesce (act_t1_diab_jul_16, 0 ) as act_t1_diab_jul_16,
coalesce (act_t1_diab_oct_16, 0 ) as act_t1_diab_oct_16,
coalesce (t2_diab_prev_jan_12, 0 ) as t2_diab_prev_jan_12,
coalesce (t2_diab_prev_apr_12, 0 ) as t2_diab_prev_apr_12,
coalesce (t2_diab_prev_jul_12, 0 ) as t2_diab_prev_jul_12,
coalesce (t2_diab_prev_oct_12, 0 ) as t2_diab_prev_oct_12,
coalesce (t2_diab_prev_jan_13, 0 ) as t2_diab_prev_jan_13,
coalesce (t2_diab_prev_apr_13, 0 ) as t2_diab_prev_apr_13,
coalesce (t2_diab_prev_jul_13, 0 ) as t2_diab_prev_jul_13,
coalesce (t2_diab_prev_oct_13, 0 ) as t2_diab_prev_oct_13,
coalesce (t2_diab_prev_jan_14, 0 ) as t2_diab_prev_jan_14,
coalesce (t2_diab_prev_apr_14, 0 ) as t2_diab_prev_apr_14,
coalesce (t2_diab_prev_jul_14, 0 ) as t2_diab_prev_jul_14,
coalesce (t2_diab_prev_oct_14, 0 ) as t2_diab_prev_oct_14,
coalesce (t2_diab_prev_jan_15, 0 ) as t2_diab_prev_jan_15,
coalesce (t2_diab_prev_apr_15, 0 ) as t2_diab_prev_apr_15,
coalesce (t2_diab_prev_jul_15, 0 ) as t2_diab_prev_jul_15,
coalesce (t2_diab_prev_oct_15, 0 ) as t2_diab_prev_oct_15,
coalesce (t2_diab_prev_jan_16, 0 ) as t2_diab_prev_jan_16,
coalesce (t2_diab_prev_apr_16, 0 ) as t2_diab_prev_apr_16,
coalesce (t2_diab_prev_jul_16, 0 ) as t2_diab_prev_jul_16,
coalesce (t2_diab_prev_oct_16, 0 ) as t2_diab_prev_oct_16,
coalesce (act_t2_diab_jan_12, 0 ) as act_t2_diab_jan_12,
coalesce (act_t2_diab_apr_12, 0 ) as act_t2_diab_apr_12,
coalesce (act_t2_diab_jul_12, 0 ) as act_t2_diab_jul_12,
coalesce (act_t2_diab_oct_12, 0 ) as act_t2_diab_oct_12,
coalesce (act_t2_diab_jan_13, 0 ) as act_t2_diab_jan_13,
coalesce (act_t2_diab_apr_13, 0 ) as act_t2_diab_apr_13,
coalesce (act_t2_diab_jul_13, 0 ) as act_t2_diab_jul_13,
coalesce (act_t2_diab_oct_13, 0 ) as act_t2_diab_oct_13,
coalesce (act_t2_diab_jan_14, 0 ) as act_t2_diab_jan_14,
coalesce (act_t2_diab_apr_14, 0 ) as act_t2_diab_apr_14,
coalesce (act_t2_diab_jul_14, 0 ) as act_t2_diab_jul_14,
coalesce (act_t2_diab_oct_14, 0 ) as act_t2_diab_oct_14,
coalesce (act_t2_diab_jan_15, 0 ) as act_t2_diab_jan_15,
coalesce (act_t2_diab_apr_15, 0 ) as act_t2_diab_apr_15,
coalesce (act_t2_diab_jul_15, 0 ) as act_t2_diab_jul_15,
coalesce (act_t2_diab_oct_15, 0 ) as act_t2_diab_oct_15,
coalesce (act_t2_diab_jan_16, 0 ) as act_t2_diab_jan_16,
coalesce (act_t2_diab_apr_16, 0 ) as act_t2_diab_apr_16,
coalesce (act_t2_diab_jul_16, 0 ) as act_t2_diab_jul_16,
coalesce (act_t2_diab_oct_16, 0 ) as act_t2_diab_oct_16,
coalesce (t1_or_t2_diab_prev_jan_12, 0 ) as t1_or_t2_diab_prev_jan_12,
coalesce (t1_or_t2_diab_prev_apr_12, 0 ) as t1_or_t2_diab_prev_apr_12,
coalesce (t1_or_t2_diab_prev_jul_12, 0 ) as t1_or_t2_diab_prev_jul_12,
coalesce (t1_or_t2_diab_prev_oct_12, 0 ) as t1_or_t2_diab_prev_oct_12,
coalesce (t1_or_t2_diab_prev_jan_13, 0 ) as t1_or_t2_diab_prev_jan_13,
coalesce (t1_or_t2_diab_prev_apr_13, 0 ) as t1_or_t2_diab_prev_apr_13,
coalesce (t1_or_t2_diab_prev_jul_13, 0 ) as t1_or_t2_diab_prev_jul_13,
coalesce (t1_or_t2_diab_prev_oct_13, 0 ) as t1_or_t2_diab_prev_oct_13,
coalesce (t1_or_t2_diab_prev_jan_14, 0 ) as t1_or_t2_diab_prev_jan_14,
coalesce (t1_or_t2_diab_prev_apr_14, 0 ) as t1_or_t2_diab_prev_apr_14,
coalesce (t1_or_t2_diab_prev_jul_14, 0 ) as t1_or_t2_diab_prev_jul_14,
coalesce (t1_or_t2_diab_prev_oct_14, 0 ) as t1_or_t2_diab_prev_oct_14,
coalesce (t1_or_t2_diab_prev_jan_15, 0 ) as t1_or_t2_diab_prev_jan_15,
coalesce (t1_or_t2_diab_prev_apr_15, 0 ) as t1_or_t2_diab_prev_apr_15,
coalesce (t1_or_t2_diab_prev_jul_15, 0 ) as t1_or_t2_diab_prev_jul_15,
coalesce (t1_or_t2_diab_prev_oct_15, 0 ) as t1_or_t2_diab_prev_oct_15,
coalesce (t1_or_t2_diab_prev_jan_16, 0 ) as t1_or_t2_diab_prev_jan_16,
coalesce (t1_or_t2_diab_prev_apr_16, 0 ) as t1_or_t2_diab_prev_apr_16,
coalesce (t1_or_t2_diab_prev_jul_16, 0 ) as t1_or_t2_diab_prev_jul_16,
coalesce (t1_or_t2_diab_prev_oct_16, 0 ) as t1_or_t2_diab_prev_oct_16,
coalesce (act_t1_or_t2_diab_jan_12, 0 ) as act_t1_or_t2_diab_jan_12,
coalesce (act_t1_or_t2_diab_apr_12, 0 ) as act_t1_or_t2_diab_apr_12,
coalesce (act_t1_or_t2_diab_jul_12, 0 ) as act_t1_or_t2_diab_jul_12,
coalesce (act_t1_or_t2_diab_oct_12, 0 ) as act_t1_or_t2_diab_oct_12,
coalesce (act_t1_or_t2_diab_jan_13, 0 ) as act_t1_or_t2_diab_jan_13,
coalesce (act_t1_or_t2_diab_apr_13, 0 ) as act_t1_or_t2_diab_apr_13,
coalesce (act_t1_or_t2_diab_jul_13, 0 ) as act_t1_or_t2_diab_jul_13,
coalesce (act_t1_or_t2_diab_oct_13, 0 ) as act_t1_or_t2_diab_oct_13,
coalesce (act_t1_or_t2_diab_jan_14, 0 ) as act_t1_or_t2_diab_jan_14,
coalesce (act_t1_or_t2_diab_apr_14, 0 ) as act_t1_or_t2_diab_apr_14,
coalesce (act_t1_or_t2_diab_jul_14, 0 ) as act_t1_or_t2_diab_jul_14,
coalesce (act_t1_or_t2_diab_oct_14, 0 ) as act_t1_or_t2_diab_oct_14,
coalesce (act_t1_or_t2_diab_jan_15, 0 ) as act_t1_or_t2_diab_jan_15,
coalesce (act_t1_or_t2_diab_apr_15, 0 ) as act_t1_or_t2_diab_apr_15,
coalesce (act_t1_or_t2_diab_jul_15, 0 ) as act_t1_or_t2_diab_jul_15,
coalesce (act_t1_or_t2_diab_oct_15, 0 ) as act_t1_or_t2_diab_oct_15,
coalesce (act_t1_or_t2_diab_jan_16, 0 ) as act_t1_or_t2_diab_jan_16,
coalesce (act_t1_or_t2_diab_apr_16, 0 ) as act_t1_or_t2_diab_apr_16,
coalesce (act_t1_or_t2_diab_jul_16, 0 ) as act_t1_or_t2_diab_jul_16,
coalesce (act_t1_or_t2_diab_oct_16, 0 ) as act_t1_or_t2_diab_oct_16,
coalesce (asthma_jan_12, 0 ) as asthma_jan_12,
coalesce (asthma_apr_12, 0 ) as asthma_apr_12,
coalesce (asthma_jul_12, 0 ) as asthma_jul_12,
coalesce (asthma_oct_12, 0 ) as asthma_oct_12,
coalesce (asthma_jan_13, 0 ) as asthma_jan_13,
coalesce (asthma_apr_13, 0 ) as asthma_apr_13,
coalesce (asthma_jul_13, 0 ) as asthma_jul_13,
coalesce (asthma_oct_13, 0 ) as asthma_oct_13,
coalesce (asthma_jan_14, 0 ) as asthma_jan_14,
coalesce (asthma_apr_14, 0 ) as asthma_apr_14,
coalesce (asthma_jul_14, 0 ) as asthma_jul_14,
coalesce (asthma_oct_14, 0 ) as asthma_oct_14,
coalesce (asthma_jan_15, 0 ) as asthma_jan_15,
coalesce (asthma_apr_15, 0 ) as asthma_apr_15,
coalesce (asthma_jul_15, 0 ) as asthma_jul_15,
coalesce (asthma_oct_15, 0 ) as asthma_oct_15,
coalesce (asthma_jan_16, 0 ) as asthma_jan_16,
coalesce (asthma_apr_16, 0 ) as asthma_apr_16,
coalesce (asthma_jul_16, 0 ) as asthma_jul_16,
coalesce (asthma_oct_16, 0 ) as asthma_oct_16,
coalesce (denominator_jan_12, 0 ) as denominator_jan_12,
coalesce (denominator_apr_12, 0 ) as denominator_apr_12,
coalesce (denominator_jul_12, 0 ) as denominator_jul_12,
coalesce (denominator_oct_12, 0 ) as denominator_oct_12,
coalesce (denominator_jan_13, 0 ) as denominator_jan_13,
coalesce (denominator_apr_13, 0 ) as denominator_apr_13,
coalesce (denominator_jul_13, 0 ) as denominator_jul_13,
coalesce (denominator_oct_13, 0 ) as denominator_oct_13,
coalesce (denominator_jan_14, 0 ) as denominator_jan_14,
coalesce (denominator_apr_14, 0 ) as denominator_apr_14,
coalesce (denominator_jul_14, 0 ) as denominator_jul_14,
coalesce (denominator_oct_14, 0 ) as denominator_oct_14,
coalesce (denominator_jan_15, 0 ) as denominator_jan_15,
coalesce (denominator_apr_15, 0 ) as denominator_apr_15,
coalesce (denominator_jul_15, 0 ) as denominator_jul_15,
coalesce (denominator_oct_15, 0 ) as denominator_oct_15,
coalesce (denominator_jan_16, 0 ) as denominator_jan_16,
coalesce (denominator_apr_16, 0 ) as denominator_apr_16,
coalesce (denominator_jul_16, 0 ) as denominator_jul_16,
coalesce (denominator_oct_16, 0 ) as denominator_oct_16,
coalesce(overweight_jan_12, 0)  as overweight_jan_12,
coalesce(overweight_apr_12, 0)  as overweight_apr_12,
coalesce(overweight_jul_12, 0)  as overweight_jul_12,
coalesce(overweight_oct_12, 0)  as overweight_oct_12,
coalesce(overweight_jan_13, 0)  as overweight_jan_13,
coalesce(overweight_apr_13, 0)  as overweight_apr_13,
coalesce(overweight_jul_13, 0)  as overweight_jul_13,
coalesce(overweight_oct_13, 0)  as overweight_oct_13,
coalesce(overweight_jan_14, 0)  as overweight_jan_14,
coalesce(overweight_apr_14, 0)  as overweight_apr_14,
coalesce(overweight_jul_14, 0)  as overweight_jul_14,
coalesce(overweight_oct_14, 0)  as overweight_oct_14,
coalesce(overweight_jan_15, 0)  as overweight_jan_15,
coalesce(overweight_apr_15, 0)  as overweight_apr_15,
coalesce(overweight_jul_15, 0)  as overweight_jul_15,
coalesce(overweight_oct_15, 0)  as overweight_oct_15,
coalesce(overweight_jan_16, 0)  as overweight_jan_16,
coalesce(overweight_apr_16, 0)  as overweight_apr_16,
coalesce(overweight_jul_16, 0)  as overweight_jul_16,
coalesce(overweight_oct_16, 0)  as overweight_oct_16,
coalesce(obese_jan_12, 0)  as obese_jan_12,
coalesce(obese_apr_12, 0)  as obese_apr_12,
coalesce(obese_jul_12, 0)  as obese_jul_12,
coalesce(obese_oct_12, 0)  as obese_oct_12,
coalesce(obese_jan_13, 0)  as obese_jan_13,
coalesce(obese_apr_13, 0)  as obese_apr_13,
coalesce(obese_jul_13, 0)  as obese_jul_13,
coalesce(obese_oct_13, 0)  as obese_oct_13,
coalesce(obese_jan_14, 0)  as obese_jan_14,
coalesce(obese_apr_14, 0)  as obese_apr_14,
coalesce(obese_jul_14, 0)  as obese_jul_14,
coalesce(obese_oct_14, 0)  as obese_oct_14,
coalesce(obese_jan_15, 0)  as obese_jan_15,
coalesce(obese_apr_15, 0)  as obese_apr_15,
coalesce(obese_jul_15, 0)  as obese_jul_15,
coalesce(obese_oct_15, 0)  as obese_oct_15,
coalesce(obese_jan_16, 0)  as obese_jan_16,
coalesce(obese_apr_16, 0)  as obese_apr_16,
coalesce(obese_jul_16, 0)  as obese_jul_16,
coalesce(obese_oct_16, 0)  as obese_oct_16,
coalesce(overweight_or_obese_jan_12, 0)  as overweight_or_obese_jan_12,
coalesce(overweight_or_obese_apr_12, 0)  as overweight_or_obese_apr_12,
coalesce(overweight_or_obese_jul_12, 0)  as overweight_or_obese_jul_12,
coalesce(overweight_or_obese_oct_12, 0)  as overweight_or_obese_oct_12,
coalesce(overweight_or_obese_jan_13, 0)  as overweight_or_obese_jan_13,
coalesce(overweight_or_obese_apr_13, 0)  as overweight_or_obese_apr_13,
coalesce(overweight_or_obese_jul_13, 0)  as overweight_or_obese_jul_13,
coalesce(overweight_or_obese_oct_13, 0)  as overweight_or_obese_oct_13,
coalesce(overweight_or_obese_jan_14, 0)  as overweight_or_obese_jan_14,
coalesce(overweight_or_obese_apr_14, 0)  as overweight_or_obese_apr_14,
coalesce(overweight_or_obese_jul_14, 0)  as overweight_or_obese_jul_14,
coalesce(overweight_or_obese_oct_14, 0)  as overweight_or_obese_oct_14,
coalesce(overweight_or_obese_jan_15, 0)  as overweight_or_obese_jan_15,
coalesce(overweight_or_obese_apr_15, 0)  as overweight_or_obese_apr_15,
coalesce(overweight_or_obese_jul_15, 0)  as overweight_or_obese_jul_15,
coalesce(overweight_or_obese_oct_15, 0)  as overweight_or_obese_oct_15,
coalesce(overweight_or_obese_jan_16, 0)  as overweight_or_obese_jan_16,
coalesce(overweight_or_obese_apr_16, 0)  as overweight_or_obese_apr_16,
coalesce(overweight_or_obese_jul_16, 0)  as overweight_or_obese_jul_16,
coalesce(overweight_or_obese_oct_16, 0)  as overweight_or_obese_oct_16,
coalesce(no_measured_bmi_jan_12, 0 )  as   no_measured_bmi_jan_12,
coalesce(no_measured_bmi_apr_12, 0 )  as   no_measured_bmi_apr_12,
coalesce(no_measured_bmi_jul_12, 0 )  as   no_measured_bmi_jul_12,
coalesce(no_measured_bmi_oct_12, 0 )  as   no_measured_bmi_oct_12,
coalesce(no_measured_bmi_jan_13, 0 )  as   no_measured_bmi_jan_13,
coalesce(no_measured_bmi_apr_13, 0 )  as   no_measured_bmi_apr_13,
coalesce(no_measured_bmi_jul_13, 0 )  as   no_measured_bmi_jul_13,
coalesce(no_measured_bmi_oct_13, 0 )  as   no_measured_bmi_oct_13,
coalesce(no_measured_bmi_jan_14, 0 )  as   no_measured_bmi_jan_14,
coalesce(no_measured_bmi_apr_14, 0 )  as   no_measured_bmi_apr_14,
coalesce(no_measured_bmi_jul_14, 0 )  as   no_measured_bmi_jul_14,
coalesce(no_measured_bmi_oct_14, 0 )  as   no_measured_bmi_oct_14,
coalesce(no_measured_bmi_jan_15, 0 )  as   no_measured_bmi_jan_15,
coalesce(no_measured_bmi_apr_15, 0 )  as   no_measured_bmi_apr_15,
coalesce(no_measured_bmi_jul_15, 0 )  as   no_measured_bmi_jul_15,
coalesce(no_measured_bmi_oct_15, 0 )  as   no_measured_bmi_oct_15,
coalesce(no_measured_bmi_jan_16, 0 )  as   no_measured_bmi_jan_16,
coalesce(no_measured_bmi_apr_16, 0 )  as   no_measured_bmi_apr_16,
coalesce(no_measured_bmi_jul_16, 0 )  as   no_measured_bmi_jul_16,
coalesce(no_measured_bmi_oct_16, 0 )  as   no_measured_bmi_oct_16,
coalesce( denominator_jan_12, 0)  as  bmi_denom_jan_12,
coalesce( denominator_apr_12, 0)  as  bmi_denom_apr_12,
coalesce( denominator_jul_12, 0)  as  bmi_denom_jul_12,
coalesce( denominator_oct_12, 0)  as  bmi_denom_oct_12,
coalesce( denominator_jan_13, 0)  as  bmi_denom_jan_13,
coalesce( denominator_apr_13, 0)  as  bmi_denom_apr_13,
coalesce( denominator_jul_13, 0)  as  bmi_denom_jul_13,
coalesce( denominator_oct_13, 0)  as  bmi_denom_oct_13,
coalesce( denominator_jan_14, 0)  as  bmi_denom_jan_14,
coalesce( denominator_apr_14, 0)  as  bmi_denom_apr_14,
coalesce( denominator_jul_14, 0)  as  bmi_denom_jul_14,
coalesce( denominator_oct_14, 0)  as  bmi_denom_oct_14,
coalesce( denominator_jan_15, 0)  as  bmi_denom_jan_15,
coalesce( denominator_apr_15, 0)  as  bmi_denom_apr_15,
coalesce( denominator_jul_15, 0)  as  bmi_denom_jul_15,
coalesce( denominator_oct_15, 0)  as  bmi_denom_oct_15,
coalesce( denominator_jan_16, 0)  as  bmi_denom_jan_16,
coalesce( denominator_apr_16, 0)  as  bmi_denom_apr_16,
coalesce( denominator_jul_16, 0)  as  bmi_denom_jul_16,
coalesce( denominator_oct_16, 0)  as  bmi_denom_oct_16,
coalesce(hypertension_jan_12, 0 )  as   hypertension_jan_12,
coalesce(hypertension_apr_12, 0 )  as   hypertension_apr_12,
coalesce(hypertension_jul_12, 0 )  as   hypertension_jul_12,
coalesce(hypertension_oct_12, 0 )  as   hypertension_oct_12,
coalesce(hypertension_jan_13, 0 )  as   hypertension_jan_13,
coalesce(hypertension_apr_13, 0 )  as   hypertension_apr_13,
coalesce(hypertension_jul_13, 0 )  as   hypertension_jul_13,
coalesce(hypertension_oct_13, 0 )  as   hypertension_oct_13,
coalesce(hypertension_jan_14, 0 )  as   hypertension_jan_14,
coalesce(hypertension_apr_14, 0 )  as   hypertension_apr_14,
coalesce(hypertension_jul_14, 0 )  as   hypertension_jul_14,
coalesce(hypertension_oct_14, 0 )  as   hypertension_oct_14,
coalesce(hypertension_jan_15, 0 )  as   hypertension_jan_15,
coalesce(hypertension_apr_15, 0 )  as   hypertension_apr_15,
coalesce(hypertension_jul_15, 0 )  as   hypertension_jul_15,
coalesce(hypertension_oct_15, 0 )  as   hypertension_oct_15,
coalesce(hypertension_jan_16, 0 )  as   hypertension_jan_16,
coalesce(hypertension_apr_16, 0 )  as   hypertension_apr_16,
coalesce(hypertension_jul_16, 0 )  as   hypertension_jul_16,
coalesce(hypertension_oct_16, 0 )  as   hypertension_oct_16,
coalesce(systolic_avg_jan_12, 0) as systolic_avg_jan_12, 
coalesce(systolic_avg_apr_12, 0) as systolic_avg_apr_12, 
coalesce(systolic_avg_jul_12, 0) as systolic_avg_jul_12, 
coalesce(systolic_avg_oct_12, 0) as systolic_avg_oct_12,
coalesce(systolic_avg_jan_13, 0) as systolic_avg_jan_13, 
coalesce(systolic_avg_apr_13, 0) as systolic_avg_apr_13, 
coalesce(systolic_avg_jul_13, 0) as systolic_avg_jul_13, 
coalesce(systolic_avg_oct_13, 0) as systolic_avg_oct_13,
coalesce(systolic_avg_jan_14, 0) as systolic_avg_jan_14, 
coalesce(systolic_avg_apr_14, 0) as systolic_avg_apr_14, 
coalesce(systolic_avg_jul_14, 0) as systolic_avg_jul_14, 
coalesce(systolic_avg_oct_14, 0) as systolic_avg_oct_14,
coalesce(systolic_avg_jan_15, 0) as systolic_avg_jan_15, 
coalesce(systolic_avg_apr_15, 0) as systolic_avg_apr_15, 
coalesce(systolic_avg_jul_15, 0) as systolic_avg_jul_15, 
coalesce(systolic_avg_oct_15, 0) as systolic_avg_oct_15,
coalesce(systolic_avg_jan_16, 0) as systolic_avg_jan_16, 
coalesce(systolic_avg_apr_16, 0) as systolic_avg_apr_16, 
coalesce(systolic_avg_jul_16, 0) as systolic_avg_jul_16, 
coalesce(systolic_avg_oct_16, 0) as systolic_avg_oct_16,
coalesce(diastolic_avg_jan_12, 0) as diastolic_avg_jan_12, 
coalesce(diastolic_avg_apr_12, 0) as diastolic_avg_apr_12, 
coalesce(diastolic_avg_jul_12, 0) as diastolic_avg_jul_12, 
coalesce(diastolic_avg_oct_12, 0) as diastolic_avg_oct_12,
coalesce(diastolic_avg_jan_13, 0) as diastolic_avg_jan_13, 
coalesce(diastolic_avg_apr_13, 0) as diastolic_avg_apr_13, 
coalesce(diastolic_avg_jul_13, 0) as diastolic_avg_jul_13, 
coalesce(diastolic_avg_oct_13, 0) as diastolic_avg_oct_13,
coalesce(diastolic_avg_jan_14, 0) as diastolic_avg_jan_14, 
coalesce(diastolic_avg_apr_14, 0) as diastolic_avg_apr_14, 
coalesce(diastolic_avg_jul_14, 0) as diastolic_avg_jul_14, 
coalesce(diastolic_avg_oct_14, 0) as diastolic_avg_oct_14,
coalesce(diastolic_avg_jan_15, 0) as diastolic_avg_jan_15, 
coalesce(diastolic_avg_apr_15, 0) as diastolic_avg_apr_15, 
coalesce(diastolic_avg_jul_15, 0) as diastolic_avg_jul_15, 
coalesce(diastolic_avg_oct_15, 0) as diastolic_avg_oct_15,
coalesce(diastolic_avg_jan_16, 0) as diastolic_avg_jan_16, 
coalesce(diastolic_avg_apr_16, 0) as diastolic_avg_apr_16, 
coalesce(diastolic_avg_jul_16, 0) as diastolic_avg_jul_16, 
coalesce(diastolic_avg_oct_16, 0) as diastolic_avg_oct_16,
coalesce(controlled_hyper_jan_12, 0) as controlled_hyper_jan_12,
coalesce(controlled_hyper_apr_12, 0) as controlled_hyper_apr_12,
coalesce(controlled_hyper_jul_12, 0) as controlled_hyper_jul_12,
coalesce(controlled_hyper_oct_12, 0) as controlled_hyper_oct_12,
coalesce(controlled_hyper_jan_13, 0) as controlled_hyper_jan_13,
coalesce(controlled_hyper_apr_13, 0) as controlled_hyper_apr_13,
coalesce(controlled_hyper_jul_13, 0) as controlled_hyper_jul_13,
coalesce(controlled_hyper_oct_13, 0) as controlled_hyper_oct_13,
coalesce(controlled_hyper_jan_14, 0) as controlled_hyper_jan_14,
coalesce(controlled_hyper_apr_14, 0) as controlled_hyper_apr_14,
coalesce(controlled_hyper_jul_14, 0) as controlled_hyper_jul_14,
coalesce(controlled_hyper_oct_14, 0) as controlled_hyper_oct_14,
coalesce(controlled_hyper_jan_15, 0) as controlled_hyper_jan_15,
coalesce(controlled_hyper_apr_15, 0) as controlled_hyper_apr_15,
coalesce(controlled_hyper_jul_15, 0) as controlled_hyper_jul_15,
coalesce(controlled_hyper_oct_15, 0) as controlled_hyper_oct_15,
coalesce(controlled_hyper_jan_16, 0) as controlled_hyper_jan_16,
coalesce(controlled_hyper_apr_16, 0) as controlled_hyper_apr_16,
coalesce(controlled_hyper_jul_16, 0) as controlled_hyper_jul_16,
coalesce(controlled_hyper_oct_16, 0) as controlled_hyper_oct_16,
coalesce(hypertension_treated_jan_12, 0) as hypertension_treated_jan_12,
coalesce(hypertension_treated_apr_12, 0) as hypertension_treated_apr_12,
coalesce(hypertension_treated_jul_12, 0) as hypertension_treated_jul_12,
coalesce(hypertension_treated_oct_12, 0) as hypertension_treated_oct_12,
coalesce(hypertension_treated_jan_13, 0) as hypertension_treated_jan_13,
coalesce(hypertension_treated_apr_13, 0) as hypertension_treated_apr_13,
coalesce(hypertension_treated_jul_13, 0) as hypertension_treated_jul_13,
coalesce(hypertension_treated_oct_13, 0) as hypertension_treated_oct_13,
coalesce(hypertension_treated_jan_14, 0) as hypertension_treated_jan_14,
coalesce(hypertension_treated_apr_14, 0) as hypertension_treated_apr_14,
coalesce(hypertension_treated_jul_14, 0) as hypertension_treated_jul_14,
coalesce(hypertension_treated_oct_14, 0) as hypertension_treated_oct_14,
coalesce(hypertension_treated_jan_15, 0) as hypertension_treated_jan_15,
coalesce(hypertension_treated_apr_15, 0) as hypertension_treated_apr_15,
coalesce(hypertension_treated_jul_15, 0) as hypertension_treated_jul_15,
coalesce(hypertension_treated_oct_15, 0) as hypertension_treated_oct_15,
coalesce(hypertension_treated_jan_16, 0) as hypertension_treated_jan_16,
coalesce(hypertension_treated_apr_16, 0) as hypertension_treated_apr_16,
coalesce(hypertension_treated_jul_16, 0) as hypertension_treated_jul_16,
coalesce(hypertension_treated_oct_16, 0) as hypertension_treated_oct_16,
coalesce(measured_bp_jan_12, 0) as measured_bp_jan_12,
coalesce(measured_bp_apr_12, 0) as measured_bp_apr_12,
coalesce(measured_bp_jul_12, 0) as measured_bp_jul_12,
coalesce(measured_bp_oct_12, 0) as measured_bp_oct_12,
coalesce(measured_bp_jan_13, 0) as measured_bp_jan_13,
coalesce(measured_bp_apr_13, 0) as measured_bp_apr_13,
coalesce(measured_bp_jul_13, 0) as measured_bp_jul_13,
coalesce(measured_bp_oct_13, 0) as measured_bp_oct_13,
coalesce(measured_bp_jan_14, 0) as measured_bp_jan_14,
coalesce(measured_bp_apr_14, 0) as measured_bp_apr_14,
coalesce(measured_bp_jul_14, 0) as measured_bp_jul_14,
coalesce(measured_bp_oct_14, 0) as measured_bp_oct_14,
coalesce(measured_bp_jan_15, 0) as measured_bp_jan_15,
coalesce(measured_bp_apr_15, 0) as measured_bp_apr_15,
coalesce(measured_bp_jul_15, 0) as measured_bp_jul_15,
coalesce(measured_bp_oct_15, 0) as measured_bp_oct_15,
coalesce(measured_bp_jan_16, 0) as measured_bp_jan_16,
coalesce(measured_bp_apr_16, 0) as measured_bp_apr_16,
coalesce(measured_bp_jul_16, 0) as measured_bp_jul_16,
coalesce(measured_bp_oct_16, 0) as measured_bp_oct_16     
FROM
esp_mdphnet.smk_diab_a_pwtf_smoking s
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_prev_diabetes pd
ON pd.age_group_10_yr = s.age_group_10_yr
AND pd.sex = s.sex
AND pd.race_ethnicity = s.race_ethnicity
AND pd.pwtf_comm = s.pwtf_comm
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_act_diabetes ad
ON ad.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr)
AND ad.sex = coalesce(s.sex, pd.sex)
AND ad.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity)
AND ad.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_denom d
ON d.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr)
AND d.sex = coalesce(s.sex, pd.sex, ad.sex)
AND d.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity)
AND d.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_asthma a
ON a.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr)
AND a.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex)
AND a.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity)
AND a.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_bmi b
ON b.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr)
AND b.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex)
AND b.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity)
AND b.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm)
-- FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_bmi_denom bd
-- ON bd.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr)
-- AND bd.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex)
-- AND bd.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity)
-- AND bd.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_hyper h
ON h.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr)
AND h.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex)
AND h.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity)
AND h.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_systolic sys
ON sys.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr)
AND sys.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex)
AND sys.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity)
AND sys.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_diastolic dia
ON dia.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr)
AND dia.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex)
AND dia.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity)
AND dia.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_controlled_bp cbp
ON cbp.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr)
AND cbp.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex)
AND cbp.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity)
AND cbp.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_measured_bp mbp
ON mbp.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr)
AND mbp.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex)
AND mbp.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity)
AND mbp.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_smoking_status_rec smkstat
ON smkstat.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr)
AND smkstat.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex)
AND smkstat.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity)
AND smkstat.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm, mbp.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_smoking_still_seen smkseen
ON smkseen.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr)
AND smkseen.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex,h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex)
AND smkseen.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity)
AND smkseen.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm, mbp.pwtf_comm, smkstat.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_smoking_cessation smkcess
ON smkcess.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr)
AND smkcess.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex)
AND smkcess.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity)
AND smkcess.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm, mbp.pwtf_comm, smkstat.pwtf_comm, smkseen.pwtf_comm)
FULL OUTER JOIN esp_mdphnet.smk_diab_a_pwtf_hyper_treated hyptreat
ON hyptreat.age_group_10_yr = coalesce(s.age_group_10_yr, pd.age_group_10_yr, ad.age_group_10_yr, d.age_group_10_yr, a.age_group_10_yr,b.age_group_10_yr, h.age_group_10_yr, sys.age_group_10_yr, sys.age_group_10_yr, cbp.age_group_10_yr, mbp.age_group_10_yr, smkstat.age_group_10_yr, smkseen.age_group_10_yr, smkcess.age_group_10_yr)
AND hyptreat.sex = coalesce(s.sex, pd.sex, ad.sex, d.sex, a.sex, b.sex, h.sex, sys.sex, dia.sex, cbp.sex, mbp.sex, smkstat.sex, smkseen.sex, smkcess.sex)
AND hyptreat.race_ethnicity = coalesce(s.race_ethnicity, pd.race_ethnicity, ad.race_ethnicity, d.race_ethnicity, a.race_ethnicity, b.race_ethnicity, h.race_ethnicity, sys.race_ethnicity, dia.race_ethnicity, cbp.race_ethnicity, mbp.race_ethnicity, smkstat.race_ethnicity, smkseen.race_ethnicity, smkcess.race_ethnicity)
AND hyptreat.pwtf_comm = coalesce(s.pwtf_comm, pd.pwtf_comm, ad.pwtf_comm, d.pwtf_comm, a.pwtf_comm, b.pwtf_comm, h.pwtf_comm, sys.pwtf_comm, dia.pwtf_comm, cbp.pwtf_comm, mbp.pwtf_comm, smkstat.pwtf_comm, smkseen.pwtf_comm, smkcess.pwtf_comm)
;

-- MLCHC WORK AROUND
update esp_mdphnet.smk_diab_a_pwtf_comm_output
set 
bmi_denom_jan_12 = 0,
bmi_denom_apr_12 = 0,
bmi_denom_jul_12 = 0,
bmi_denom_oct_12 = 0,
bmi_denom_jan_13 = 0,
bmi_denom_apr_13 = 0,
bmi_denom_jul_13 = 0,
bmi_denom_oct_13 = 0,
bmi_denom_jan_14 = 0,
bmi_denom_apr_14 = 0,
bmi_denom_jul_14 = 0,
bmi_denom_oct_14 = 0,
bmi_denom_jan_15 = 0,
bmi_denom_apr_15 = 0,
bmi_denom_jul_15 = 0,
bmi_denom_oct_15 = 0,
bmi_denom_jan_16 = 0,
bmi_denom_apr_16 = 0,
bmi_denom_jul_16 = 0,
bmi_denom_oct_16 = 0
where age_group_10_year in ('0-9', '10-19');


--
-- Script shutdown section 
--

-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_dateofint CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_current CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_cases_all CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_prev_diabetes_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_med_dx_all CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_active_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_all_entries CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101159 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_dateofint CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101156 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101157 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101158 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_denom_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_100036_s_101154 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_union1 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_union2 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_denom_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_all_events CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_prev_diabetes CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_act_diabetes CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_denom CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_asthma CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_bmi CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_bmi_denom CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_hyper CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_enc_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diab_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_diab_events CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_frank_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_act_diab_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_asthma_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bmi_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_unk_bmi_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_status_rec_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_status_rec_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_recorded_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking_status_rec CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.current_smokers_on_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_still_seen_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_still_seen_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking_still_seen CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smk_cessation_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_smoking_cessation_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_smoking_cessation CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_union_full_w_enc CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_systolic_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_systolic CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_union_full_w_enc CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_diastolic_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_diastolic CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_union_full_w_enc CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_controlled_bp_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_controlled_bp CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_union_full_w_enc CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_bp_measured_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_measured_bp CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_rx_events CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102012 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102013 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102014 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102015 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_012016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_042016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_072016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_102016 CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_union_full CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_union_full_w_enc CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_dateofint CASCADE; 
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_agezip CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_pat_details CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_hyper_treated_full_strat CASCADE;
-- DROP TABLE IF EXISTS esp_mdphnet.smk_diab_a_pwtf_hyper_treated CASCADE;





--Print output to screen
select * from esp_mdphnet.smk_diab_all_full_strat_output order by age_group_10_year, sex, race asc;

--Print output to screen
select * from esp_mdphnet.smk_diab_a_pwtf_comm_output order by pwtf_comm, age_group_10_year, sex, race asc;

set client_min_messages to notice;
