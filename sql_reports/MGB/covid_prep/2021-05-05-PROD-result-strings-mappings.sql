-- clean up ignored
delete from conf_ignoredcode where native_code in (select native_code from conf_labtestmap);


-- unmap and ignore
delete from conf_labtestmap where native_code = '84999--5290007942'; 
delete from conf_labtestmap where native_code = '84999--5290007497'; 
insert into conf_ignoredcode VALUES(default, '84999--5290007942' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '84999--5290007497' ) ON CONFLICT DO NOTHING;


-- add mappings for result strings
insert into conf_resultstring VALUES (default, 'NO ADENOVIRUS ISOLATED', 'neg', 'istartswith', 'false') ON CONFLICT DO NOTHING;
insert into conf_resultstring VALUES (default, 'Inconclusive', 'ind', 'istartswith', 'true') ON CONFLICT DO NOTHING;
insert into conf_resultstring VALUES (default, 'Undetec', 'neg', 'istartswith', 'true') ON CONFLICT DO NOTHING;
