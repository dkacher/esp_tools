drop table if exists temp_tb_ignored;

create table temp_tb_ignored as
select count(*), l.native_name, l.native_code, l.procedure_name
from emr_labresult l
where l.native_code in (select native_code from conf_ignoredcode)
and l.native_name ~* '(tuber|afb|cult|mycob|tb|igra|feron|qft|t-spot)'
group by l.native_name, l.native_code, procedure_name;


DROP TABLE IF EXISTS temp_tb_strings_ignored_with_result_strings;
CREATE TABLE temp_tb_strings_ignored_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_tb_ignored S2
   where S1.native_code = S2.native_code
   and S2.native_name ~* '(tuber|afb|cult|mycob|tb|igra|feron|qft|t-spot)'
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name
  from emr_labresult T1,
  temp_tb_ignored T2
  where T1.native_code = T2.native_code 
  and T2.native_name ~* '(tuber|afb|cult|mycob|tb|igra|feron|qft|t-spot)'
  group by T1.native_code, T1.native_name, T1.procedure_name) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;


DROP TABLE IF EXISTS temp_tb_strings_ignored_with_result_strings_final;
CREATE TABLE temp_tb_strings_ignored_with_result_strings_final AS
select T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, max(date) as most_recent_lab_date, T1.top_result_strings
from temp_tb_strings_ignored_with_result_strings T1,
emr_labresult T2
where T1.native_code = T2.native_code
group by T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, T1.top_result_strings
order by T1.native_code, T1.native_name, T1.procedure_name;

\COPY temp_tb_strings_ignored_with_result_strings_final TO '/tmp/all_ignored_tb_for_review.csv' DELIMITER ',' CSV HEADER;