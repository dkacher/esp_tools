﻿-- TODO Jay are all tables in esp_popmednet? or some in dbo?

-- we can't do DROP CASCADE in tsql so we drop dependent tables (in order) here
IF OBJECT_ID('esp_popmednet.UVT_SEX', 'U') IS NOT NULL DROP TABLE esp_popmednet.UVT_SEX;
IF OBJECT_ID('esp_popmednet.UVT_RACE', 'U') IS NOT NULL DROP TABLE esp_popmednet.UVT_RACE;
IF OBJECT_ID('esp_popmednet.UVT_PROVIDER', 'U') IS NOT NULL DROP TABLE esp_popmednet.UVT_PROVIDER;
IF OBJECT_ID('esp_popmednet.UVT_SITE', 'U') IS NOT NULL DROP TABLE esp_popmednet.UVT_SITE;
IF OBJECT_ID('esp_popmednet.UVT_CENTER', 'U') IS NOT NULL DROP TABLE esp_popmednet.UVT_CENTER;
IF OBJECT_ID('esp_popmednet.UVT_PERIOD', 'U') IS NOT NULL DROP TABLE esp_popmednet.UVT_PERIOD;
IF OBJECT_ID('esp_popmednet.UVT_ENCOUNTER', 'U') IS NOT NULL DROP TABLE esp_popmednet.UVT_ENCOUNTER;
IF OBJECT_ID('esp_popmednet.uvt_agegroup_5yr', 'U') IS NOT NULL DROP TABLE esp_popmednet.uvt_agegroup_5yr;
IF OBJECT_ID('esp_popmednet.uvt_agegroup_10yr', 'U') IS NOT NULL DROP TABLE esp_popmednet.uvt_agegroup_10yr;
IF OBJECT_ID('esp_popmednet.uvt_agegroup_ms', 'U') IS NOT NULL DROP TABLE esp_popmednet.uvt_agegroup_ms;
IF OBJECT_ID('esp_popmednet.uvt_dx', 'U') IS NOT NULL DROP TABLE esp_popmednet.uvt_dx;
IF OBJECT_ID('esp_popmednet.uvt_dx_3dig', 'U') IS NOT NULL DROP TABLE esp_popmednet.uvt_dx_3dig;
IF OBJECT_ID('esp_popmednet.uvt_dx_4dig', 'U') IS NOT NULL DROP TABLE esp_popmednet.uvt_dx_4dig;
IF OBJECT_ID('esp_popmednet.uvt_dx_5dig', 'U') IS NOT NULL DROP TABLE esp_popmednet.uvt_dx_5dig;
IF OBJECT_ID('esp_popmednet.UVT_DETECTED_CONDITION', 'U') IS NOT NULL DROP TABLE esp_popmednet.UVT_DETECTED_CONDITION;
IF OBJECT_ID('esp_popmednet.UVT_DETECTED_CRITERIA', 'U') IS NOT NULL DROP TABLE esp_popmednet.UVT_DETECTED_CRITERIA;
IF OBJECT_ID('esp_popmednet.UVT_DETECTED_STATUS', 'U') IS NOT NULL DROP TABLE esp_popmednet.UVT_DETECTED_STATUS;
IF OBJECT_ID('esp_popmednet.esp_diagnosis', 'U') IS NOT NULL DROP TABLE esp_popmednet.esp_diagnosis;
IF OBJECT_ID('esp_popmednet.esp_encounter', 'U') IS NOT NULL DROP TABLE esp_popmednet.esp_encounter;
IF OBJECT_ID('esp_popmednet.esp_demographics', 'U') IS NOT NULL DROP TABLE esp_popmednet.esp_demographics;


IF OBJECT_ID('esp_popmednet.esp_temp_smoking', 'U') IS NOT NULL DROP TABLE esp_popmednet.esp_temp_smoking;
CREATE TABLE esp_popmednet.esp_temp_smoking as
   select case when t1.latest='Yes' then 'Current'
               when t2.yesOrQuit='Quit' then 'Former'
               when t3.passive='Passive' then 'Passive'
               when t4.never='Never' then 'Never'
               else 'Not available' 
           end as smoking, 
           t0.natural_key as patid
   from
     emr_patient t0
   left outer join 
     (select t00.tobacco_use as latest, t00.patient_id 
      from emr_socialhistory t00
      inner join
      (select max(date) as maxdate, patient_id 
       from emr_socialhistory 
       where tobacco_use is not null and tobacco_use<>''
       group by patient_id) t01 on t00.patient_id=t01.patient_id and t00.date=t01.maxdate) t1 on t0.id=t1.patient_id
   left outer join
     (select max(val) as yesOrQuit, patient_id
      from (select CAST('Quit' AS text) as val, patient_id
            from emr_socialhistory where tobacco_use in ('Yes','Quit')) t00
            group by patient_id) t2 on t0.id=t2.patient_id
   left outer join
     (select max(val) as passive, patient_id
      from (select CAST('Passive' AS text) as val, patient_id
            from emr_socialhistory where tobacco_use ='Passive') t00
            group by patient_id) t3 on t0.id=t3.patient_id
   left outer join
     (select max(val) as never, patient_id
      from (select CAST('Never' AS text) as val, patient_id
            from emr_socialhistory where tobacco_use ='Never') t00
            group by patient_id) t4 on t0.id=t4.patient_id;
alter table esp_popmednet.esp_temp_smoking add primary key (patid);
IF OBJECT_ID('esp_popmednet.esp_demographic_v', 'V') IS NOT NULL DROP VIEW esp_popmednet.esp_demographic_v;
CREATE VIEW esp_popmednet.esp_demographic_v AS
SELECT CAST('1' AS character varying(1)) centerid,
       pat.natural_key patid,
       (DATEDIFF(day, CAST('1960-01-01', date), CAST(pat.date_of_birth, date)) birth_date,
       CASE
         WHEN UPPER(gender) = 'M' THEN CAST('M' AS char(1))
         WHEN UPPER(gender) = 'F' THEN CAST('F' AS char(1))
         WHEN UPPER(gender) = 'U' THEN CAST('U' AS char(1))
         ELSE CAST('U' AS char(1))
       END sex,
       CASE
         WHEN UPPER(race) = 'HISPANIC' THEN CAST('Y' AS char(1))
         ELSE CAST('U' AS char(1))
       END Hispanic,
       CASE
         WHEN UPPER(race) in ('NAT AMERICAN','ALASKAN') THEN 1
         WHEN UPPER(race) in ('ASIAN','INDIAN') THEN 2
         WHEN UPPER(race) = 'BLACK'  THEN 3
         WHEN UPPER(race) = 'NATIVE HAWAI' then 4
         WHEN UPPER(race) = 'CAUCASIAN' THEN 5
         ELSE 0
       END race,
       case 
         when upper(race)='HISPANIC' then 6 
         when UPPER(race) = 'CAUCASIAN' then 5
         when UPPER(race) in ('ASIAN','INDIAN','NATIVE HAWAI') then 2
         when UPPER(race) = 'BLACK'then 3
         when UPPER(race) in ('NAT AMERICAN','ALASKAN') then 1
         else 0
       end as race_ethnicity,
       pat.zip5,
       smk.smoking
  FROM dbo.emr_patient pat,
       dbo.emr_provenance prvn,
       esp_popmednet.esp_temp_smoking smk
  WHERE pat.provenance_id=prvn.provenance_id and prvn.source ilike 'epicmem%'
        and pat.natural_key=smk.patid 
        and exists (select null from emr_encounter t0 where t0.patient_id=pat.id); --patient must have at least one encounter record;

-- Instantiate table from previously created view
IF OBJECT_ID('esp_popmednet.esp_demographic', 'U') IS NOT NULL DROP TABLE esp_popmednet.esp_demographic;
CREATE TABLE esp_popmednet.esp_demographic as select * from esp_popmednet.esp_demographic_v;
create unique index esp_demographic_patid_unique_idx on esp_popmednet.esp_demographic (patid);
create index esp_demographic_centerid_idx on esp_popmednet.esp_demographic (centerid);
create index esp_demographic_birth_date_idx on esp_popmednet.esp_demographic (birth_date);
create index esp_demographic_sex_idx on esp_popmednet.esp_demographic (sex);
create index esp_demographic_hispanic_idx on esp_popmednet.esp_demographic (hispanic);
create index esp_demographic_race_idx on esp_popmednet.esp_demographic (race);
create index esp_demog_race_eth_idx on esp_popmednet.esp_demographic (race_ethnicity);
create index esp_demographic_zip5_idx on esp_popmednet.esp_demographic (zip5);
alter table esp_popmednet.esp_demographic add primary key (patid);

IF OBJECT_ID('esp_popmednet.esp_encounter_v', 'V') IS NOT NULL DROP VIEW esp_popmednet.esp_encounter_v;
CREATE VIEW esp_popmednet.esp_encounter_v AS
SELECT CAST('1' AS character varying(1)) centerid,
       pat.natural_key patid,
       enc.natural_key encounterid,
	   DATEDIFF(day, CAST('1960-01-01' AS date), enc.date) a_date,
	   DATEDIFF(day, CAST('1960-01-01' AS date), enc.date_closed) d_date,
       prov.natural_key provider,
       enc.site_name facility_location,
       CAST('AV' AS character varying(10)) as enc_type, --this is initial value for Mass League data
       enc.site_natural_key facility_code,
       datepart(year, enc.date) enc_year,
       age_at_year_start(enc.date, CAST(pat.date_of_birth AS date) age_at_enc_year,
       CAST(age_group_5yr(enc.date, CAST(pat.date_of_birth AS date) AS character varying(5)) age_group_5yr,
       CAST(age_group_10yr(enc.date, CAST(pat.date_of_birth AS date) AS character varying(5)) age_group_10yr,
       CAST(age_group_ms(enc.date, CAST(pat.date_of_birth AS date) AS character varying(5)) age_group_ms
  FROM dbo.emr_encounter enc
         INNER JOIN (select t0.* from dbo.emr_patient t0 join esp_popmednet.esp_demographic t1 on t1.patid=t0.natural_key) pat ON enc.patient_id = pat.id
         LEFT JOIN dbo.emr_provider prov ON enc.provider_id = prov.id;

IF OBJECT_ID('esp_popmednet.esp_diagnosis_v', 'V') IS NOT NULL DROP VIEW esp_popmednet.esp_diagnosis_v;
CREATE VIEW esp_popmednet.esp_diagnosis_v AS
SELECT CAST('1' AS character varying(1)) centerid,
       pat.natural_key patid,
       enc.natural_key encounterid,
       DATEDIFF(day, (CAST('1960-01-01' AS date), enc.date)) a_date,
       prov.natural_key provider,
       CAST('AV' AS character varying(10)) enc_type, --this is initial value for Mass League data
       substr(diag.dx_code_id,6) dx,
       CAST(icd9_prefix(diag.dx_code_id, 3) AS character varying(4)) dx_code_3dig,
       CAST(icd9_prefix(diag.dx_code_id, 4) AS character varying(5)) dx_code_4dig,
       case 
         when length(icd9_prefix(diag.dx_code_id, 4))=5
	   then substr(diag.dx_code_id,6,6)
         else substr(diag.dx_code_id,6,5)
       end as dx_code_4dig_with_dec,
       CAST(icd9_prefix(diag.dx_code_id, 5) AS character varying(6)) dx_code_5dig,
       case 
         when length(icd9_prefix(diag.dx_code_id, 4))=6
	   then substr(diag.dx_code_id,6,7)
         else substr(diag.dx_code_id,6,6)
       end as dx_code_5dig_with_dec,
       enc.site_name facility_location,
       enc.site_natural_key facility_code,
       DATEPART(year, enc.date) enc_year,
       age_at_year_start(enc.date, CAST(pat.date_of_birth AS date)) age_at_enc_year,
       CAST(age_group_5yr(enc.date, CAST(pat.date_of_birth AS date)) AS character varying(5)) age_group_5yr,
       CAST(age_group_10yr(enc.date, CAST(pat.date_of_birth AS date)) AS character varying(5)) age_group_10yr,
       CAST(age_group_ms(enc.date, CAST(pat.date_of_birth AS date)) AS character varying(5)) age_group_ms
  FROM dbo.emr_encounter enc
         INNER JOIN (select t0.* from dbo.emr_patient t0 join esp_popmednet.esp_demographic t1 on t1.patid=t0.natural_key) pat ON enc.patient_id = pat.id
         INNER JOIN (select * from dbo.emr_encounter_dx_codes 
                     where strpos(trim(dx_code_id),'.')<>3
                       and length(trim(dx_code_id))>=3 ) diag ON enc.id = diag.encounter_id
         LEFT JOIN dbo.emr_provider prov ON enc.provider_id = prov.id;

IF OBJECT_ID('esp_popmednet.esp_disease_v', 'V') IS NOT NULL DROP VIEW esp_popmednet.esp_disease_v;
CREATE VIEW esp_popmednet.esp_disease_v AS
SELECT CAST('1' AS character varying(1)) centerid,
       pat.natural_key patid,
       disease.condition,
       disease.date - CAST('1960-01-01' AS date) date,
       age_at_year_start(disease.date, CAST(pat.date_of_birth AS date)) age_at_detect_year,
       CAST(age_group_5yr(disease.date, CAST(pat.date_of_birth AS date)) AS character varying(5)) age_group_5yr,
       CAST(age_group_10yr(disease.date, CAST(pat.date_of_birth AS date)) AS character varying(5)) age_group_10yr,
       CAST(age_group_ms(disease.date, CAST(pat.date_of_birth AS date)) AS character varying(5)) age_group_ms,
       disease.criteria,
       disease.status,
       disease.notes
  FROM dbo.nodis_case disease
         INNER JOIN (select t0.* from dbo.emr_patient t0 join esp_popmednet.esp_demographic t1 on t1.patid=t0.natural_key) pat ON disease.patient_id = pat.id;

-- Instantiate table from previously created view
IF OBJECT_ID('esp_popmednet.esp_encounter', 'U') IS NOT NULL DROP TABLE esp_popmednet.esp_encounter;
CREATE TABLE esp_popmednet.esp_encounter as select * from esp_popmednet.esp_encounter_v;
create index esp_encounter_centerid_idx on esp_popmednet.esp_encounter (centerid);
create index esp_encounter_patid_idx on esp_popmednet.esp_encounter (patid);
create unique index esp_encounter_encounterid_idx on esp_popmednet.esp_encounter (encounterid);
create index esp_encounter_a_date_idx on esp_popmednet.esp_encounter (a_date);
create index esp_encounter_d_date_idx on esp_popmednet.esp_encounter (d_date);
create index esp_encounter_provider_idx on esp_popmednet.esp_encounter (provider);
create index esp_encounter_facility_location_idx on esp_popmednet.esp_encounter (facility_location);
create index esp_encounter_facility_code_idx on esp_popmednet.esp_encounter (facility_code);
create index esp_encounter_enc_year_idx on esp_popmednet.esp_encounter (enc_year);
create index esp_encounter_age_at_enc_year_idx on esp_popmednet.esp_encounter (age_at_enc_year);
create index esp_encounter_age_group_5yr_idx on esp_popmednet.esp_encounter (age_group_5yr);
create index esp_encounter_age_group_10yr_idx on esp_popmednet.esp_encounter (age_group_10yr);
create index esp_encounter_age_group_ms_idx on esp_popmednet.esp_encounter (age_group_ms);
alter table esp_popmednet.esp_encounter add primary key (encounterid);
alter table esp_popmednet.esp_encounter add foreign key (patid) references esp_popmednet.esp_demographic (patid);

IF OBJECT_ID('esp_popmednet.esp_diagnosis', 'U') IS NOT NULL DROP TABLE esp_popmednet.esp_diagnosis;
CREATE TABLE esp_popmednet.esp_diagnosis as select * from esp_popmednet.esp_diagnosis_v;
create index esp_diagnosis_dx_idx on esp_popmednet.esp_diagnosis (dx);
CREATE index esp_diagnosis_dx_like_idx ON esp_popmednet.esp_diagnosis USING btree (dx character varying_pattern_ops);
create index esp_diagnosis_centerid_idx on esp_popmednet.esp_diagnosis (centerid);
create index esp_diagnosis_patid_idx on esp_popmednet.esp_diagnosis (patid);
create index esp_diagnosis_encounterid_idx on esp_popmednet.esp_diagnosis (encounterid);
create index esp_diagnosis_provider_idx on esp_popmednet.esp_diagnosis (provider);
create index esp_diagnosis_enc_type_idx on esp_popmednet.esp_diagnosis (enc_type);
create index esp_diagnosis_dx_code_3dig_idx on esp_popmednet.esp_diagnosis (dx_code_3dig);
create index esp_diagnosis_dx_code_4dig_idx on esp_popmednet.esp_diagnosis (dx_code_4dig);
create index esp_diagnosis_dx_code_5dig_idx on esp_popmednet.esp_diagnosis (dx_code_5dig);
create index esp_diagnosis_dx_code_4dig_with_dec_idx on esp_popmednet.esp_diagnosis (dx_code_4dig_with_dec);
create index esp_diagnosis_dx_code_5dig_with_dec_idx on esp_popmednet.esp_diagnosis (dx_code_5dig_with_dec);
create index esp_diagnosis_dx_code_5dig_like_idx on esp_popmednet.esp_diagnosis using btree (dx_code_5dig character varying_pattern_ops);
create index esp_diagnosis_facility_loc_idx on esp_popmednet.esp_diagnosis (facility_location);
create index esp_diagnosis_facility_code_idx on esp_popmednet.esp_diagnosis (facility_code);
create index esp_diagnosis_enc_year_idx on esp_popmednet.esp_diagnosis (enc_year);
create index esp_diagnosis_age_at_enc_year_idx on esp_popmednet.esp_diagnosis (age_at_enc_year);
create index esp_diagnosis_age_group_5yr_idx on esp_popmednet.esp_diagnosis (age_group_5yr);
create index esp_diagnosis_age_group_10yr_idx on esp_popmednet.esp_diagnosis (age_group_10yr);
create index esp_diagnosis_age_group_ms_idx on esp_popmednet.esp_diagnosis (age_group_ms);
alter table esp_popmednet.esp_diagnosis add primary key (patid, encounterid, dx);
alter table esp_popmednet.esp_diagnosis add foreign key (patid) references esp_popmednet.esp_demographic (patid);
alter table esp_popmednet.esp_diagnosis add foreign key (encounterid) references esp_popmednet.esp_encounter (encounterid);

IF OBJECT_ID('esp_popmednet.esp_disease', 'U') IS NOT NULL DROP TABLE esp_popmednet.esp_disease;
CREATE TABLE esp_popmednet.esp_disease as select * from esp_popmednet.esp_disease_v;
create index esp_disease_age_group_10yr_idx on esp_popmednet.esp_disease (age_group_10yr);
create index esp_disease_age_group_5yr_idx on esp_popmednet.esp_disease (age_group_5yr);
create index esp_disease_age_group_ms_idx on esp_popmednet.esp_disease (age_group_ms);
create index esp_disease_centerid_idx on esp_popmednet.esp_disease (centerid);
create index esp_disease_patid_idx on esp_popmednet.esp_disease (patid);
create index esp_disease_condition_idx on esp_popmednet.esp_disease (condition);
create index esp_disease_date_idx on esp_popmednet.esp_disease (date);
create index esp_disease_age_at_detect_year_idx on esp_popmednet.esp_disease (age_at_detect_year);
create index esp_disease_criteria_idx on esp_popmednet.esp_disease (criteria);
create index esp_disease_status_idx on esp_popmednet.esp_disease (status);
alter table esp_popmednet.esp_disease add primary key (patid, condition, date);
alter table esp_popmednet.esp_disease add foreign key (patid) references esp_popmednet.esp_demographic (patid);

IF OBJECT_ID('esp_popmednet.esp_disease_v', 'V') IS NOT NULL DROP VIEW esp_popmednet.esp_disease_v;
IF OBJECT_ID('esp_popmednet.esp_diagnosis_v', 'V') IS NOT NULL DROP VIEW esp_popmednet.esp_diagnosis_v;
IF OBJECT_ID('esp_popmednet.esp_encounter_v', 'V') IS NOT NULL DROP VIEW esp_popmednet.esp_encounter_v;
IF OBJECT_ID('esp_popmednet.esp_demographic_v', 'V') IS NOT NULL DROP VIEW esp_popmednet.esp_demographic_v;

--now vacuum analyze each table
vacuum analyze esp_popmednet.esp_demographic;
vacuum analyze esp_popmednet.esp_encounter;
vacuum analyse esp_popmednet.esp_diagnosis;
vacuum analyse esp_popmednet.esp_disease;

/*
-- Create the summary table for 3-digit ICD9 codes and populate it with information for the 5 year age groups
DROP TABLE esp_diagnosis_icd9_3dig cascade;
CREATE TABLE esp_diagnosis_icd9_3dig AS
SELECT t1.*, icd9.name dx_name
  FROM (SELECT diag.centerid, 'Age Group 5yr' AS character varying(15) age_group_type, diag.age_group_5yr age_group,
               pat.sex, diag.enc_year period, diag.dx_code_3dig code_, diag.enc_type setting,
               count(distinct diag.patid) members,
               count(distinct diag.encounterid) events
          FROM esp_diagnosis diag
                 INNER JOIN esp_demographic pat ON diag.patid = pat.patid
        GROUP BY diag.centerid, diag.age_group_5yr, pat.sex, diag.enc_year, diag.dx_code_3dig,
                 diag.enc_type) t1
         LEFT JOIN (select * from dbo.static_dx_code
                    where   strpos(trim(code),'.')<>3
                       and length(trim(code))>=3 ) icd9 
                 ON t1.code_ = replace(icd9.code, '.', '')
UNION
-- Add the summary information for the 10 year age groups
-- INSERT INTO esp_diagnosis_icd9_3dig
--        (centerid, age_group_type, age_group, sex, period, code_, setting, members, events, dx_name)
SELECT t1.*, icd9.name dx_name
  FROM (SELECT diag.centerid, 'Age Group 10yr' AS character varying(15) age_group_type, diag.age_group_10yr age_group,
               pat.sex, diag.enc_year period, diag.dx_code_3dig code_, diag.enc_type setting,
               count(distinct diag.patid) members,
               count(distinct diag.encounterid) events
          FROM esp_diagnosis diag
                 INNER JOIN esp_demographic pat ON diag.patid = pat.patid
        GROUP BY diag.centerid, diag.age_group_10yr, pat.sex, diag.enc_year, diag.dx_code_3dig,
                 diag.enc_type) t1
         LEFT JOIN (select * from dbo.static_dx_code
                    where   strpos(trim(code),'.')<>3
                       and length(trim(code))>=3 ) icd9
                 ON t1.code_ = replace(icd9.code, '.', '')
UNION
-- Add the summary information for the mini-Sentinel age groups
-- INSERT INTO esp_diagnosis_icd9_3dig
--        (centerid, age_group_type, age_group, sex, period, code_, setting, members, events, dx_name)
SELECT t1.*, icd9.name dx_name
  FROM (SELECT diag.centerid, 'Age Group MS' AS character varying(15) age_group_type, diag.age_group_ms age_group,
               pat.sex, diag.enc_year period, diag.dx_code_3dig code_, diag.enc_type setting,
               count(distinct diag.patid) members,
               count(distinct diag.encounterid) events
          FROM esp_diagnosis diag
                 INNER JOIN esp_demographic pat ON diag.patid = pat.patid
        GROUP BY diag.centerid, diag.age_group_ms, pat.sex, diag.enc_year, diag.dx_code_3dig,
                 diag.enc_type) t1
         LEFT JOIN (select * from dbo.static_dx_code
                    where   strpos(trim(code),'.')<>3
                       and length(trim(code))>=3) icd9
                 ON t1.code_ = replace(icd9.code, '.', '');
create index diag3dig_centerid_idx on esp_diagnosis_icd9_3dig (centerid);
create index diag3dig_age_group_type_idx on esp_diagnosis_icd9_3dig (age_group_type);
create index diag3dig_age_group_idx on esp_diagnosis_icd9_3dig (age_group);
create index diag3dig_sex_idx on esp_diagnosis_icd9_3dig (sex);
create index diag3dig_period_idx on esp_diagnosis_icd9_3dig (period);
create index diag3dig_code_idx on esp_diagnosis_icd9_3dig (code_);
create index diag3dig_setting_idx on esp_diagnosis_icd9_3dig (setting);


-- Create the summary table for 4-digit ICD9 codes and populate it with information for the 5 year age groups
DROP TABLE esp_diagnosis_icd9_4dig cascade;
CREATE TABLE esp_diagnosis_icd9_4dig AS
SELECT t1.*, icd9.name dx_name
  FROM (SELECT diag.centerid, 'Age Group 5yr' AS character varying(15) age_group_type, diag.age_group_5yr age_group,
               pat.sex, diag.enc_year period, diag.dx_code_4dig code_, diag.enc_type setting,
               count(distinct diag.patid) members,
               count(distinct diag.encounterid) events
          FROM esp_diagnosis diag
                 INNER JOIN esp_demographic pat ON diag.patid = pat.patid
        GROUP BY diag.centerid, diag.age_group_5yr, pat.sex, diag.enc_year, diag.dx_code_4dig,
                 diag.enc_type) t1
         LEFT JOIN (select * from dbo.static_dx_code
                    where   strpos(trim(code),'.')<>3
                       and length(trim(code))>=3 ) icd9
                 ON t1.code_ = replace(icd9.code, '.', '')
UNION
-- Add the summary information for the 10 year age groups
-- INSERT INTO esp_diagnosis_icd9_4dig
--        (centerid, age_group_type, age_group, sex, period, code_, setting, members, events, dx_name)
SELECT t1.*, icd9.name dx_name
  FROM (SELECT diag.centerid, 'Age Group 10yr' AS character varying(15) age_group_type, diag.age_group_10yr age_group,
               pat.sex, diag.enc_year period, diag.dx_code_4dig code_, diag.enc_type setting,
               count(distinct diag.patid) members,
               count(distinct diag.encounterid) events
          FROM esp_diagnosis diag
                 INNER JOIN esp_demographic pat ON diag.patid = pat.patid
        GROUP BY diag.centerid, diag.age_group_10yr, pat.sex, diag.enc_year, diag.dx_code_4dig,
                 diag.enc_type) t1
         LEFT JOIN (select * from dbo.static_dx_code
                    where   strpos(trim(code),'.')<>3
                       and length(trim(code))>=3 ) icd9
                 ON t1.code_ = replace(icd9.code, '.', '')
UNION
-- Add the summary information for the mini-Sentinel age groups
-- INSERT INTO esp_diagnosis_icd9_4dig
--        (centerid, age_group_type, age_group, sex, period, code_, setting, members, events, dx_name)
SELECT t1.*, icd9.name dx_name
  FROM (SELECT diag.centerid, 'Age Group MS' AS character varying(15) age_group_type, diag.age_group_ms age_group,
               pat.sex, diag.enc_year period, diag.dx_code_4dig code_, diag.enc_type setting,
               count(distinct diag.patid) members,
               count(distinct diag.encounterid) events
          FROM esp_diagnosis diag
                 INNER JOIN esp_demographic pat ON diag.patid = pat.patid
        GROUP BY diag.centerid, diag.age_group_ms, pat.sex, diag.enc_year, diag.dx_code_4dig,
                 diag.enc_type) t1
         LEFT JOIN (select * from dbo.static_dx_code
                    where   strpos(trim(code),'.')<>3
                       and length(trim(code))>=3) icd9
                 ON t1.code_ = replace(icd9.code, '.', '');
create index diag4dig_centerid_idx on esp_diagnosis_icd9_4dig (centerid);
create index diag4dig_age_group_type_idx on esp_diagnosis_icd9_4dig (age_group_type);
create index diag4dig_age_group_idx on esp_diagnosis_icd9_4dig (age_group);
create index diag4dig_sex_idx on esp_diagnosis_icd9_4dig (sex);
create index diag4dig_period_idx on esp_diagnosis_icd9_4dig (period);
create index diag4dig_code_idx on esp_diagnosis_icd9_4dig (code_);
create index diag4dig_setting_idx on esp_diagnosis_icd9_4dig (setting);


-- Create the summary table for 5-digit ICD9 codes and populate it with information for the 5 year age groups
DROP TABLE esp_diagnosis_icd9_5dig cascade;
CREATE TABLE esp_diagnosis_icd9_5dig AS
SELECT t1.*, icd9.name dx_name
  FROM (SELECT diag.centerid, 'Age Group 5yr' AS character varying(15) age_group_type, diag.age_group_5yr age_group,
               pat.sex, diag.enc_year period, diag.dx_code_5dig code_, diag.enc_type setting,
               count(distinct diag.patid) members,
               count(distinct diag.encounterid) events
          FROM esp_diagnosis diag
                 INNER JOIN esp_demographic pat ON diag.patid = pat.patid
        GROUP BY diag.centerid, diag.age_group_5yr, pat.sex, diag.enc_year, diag.dx_code_5dig,
                 diag.enc_type) t1
         LEFT JOIN (select * from dbo.static_dx_code
                    where   strpos(trim(code),'.')<>3
                       and length(trim(code))>=3 ) icd9
                 ON t1.code_ = replace(icd9.code, '.', '')
UNION
-- Add the summary information for the 10 year age groups
-- INSERT INTO esp_diagnosis_icd9_5dig
--        (centerid, age_group_type, age_group, sex, period, code_, setting, members, events, dx_name)
SELECT t1.*, icd9.name dx_name
  FROM (SELECT diag.centerid, 'Age Group 10yr' AS character varying(15) age_group_type, diag.age_group_10yr age_group,
               pat.sex, diag.enc_year period, diag.dx_code_5dig code_, diag.enc_type setting,
               count(distinct diag.patid) members,
               count(distinct diag.encounterid) events
          FROM esp_diagnosis diag
                 INNER JOIN esp_demographic pat ON diag.patid = pat.patid
        GROUP BY diag.centerid, diag.age_group_10yr, pat.sex, diag.enc_year, diag.dx_code_5dig,
                 diag.enc_type) t1
         LEFT JOIN (select * from dbo.static_dx_code
                    where   strpos(trim(code),'.')<>3
                       and length(trim(code))>=3 ) icd9
                 ON t1.code_ = replace(icd9.code, '.', '')
UNION
-- Add the summary information for the mini-Sentinel age groups
-- INSERT INTO esp_diagnosis_icd9_5dig
--        (centerid, age_group_type, age_group, sex, period, code_, setting, members, events, dx_name)
SELECT t1.*, icd9.name dx_name
  FROM (SELECT diag.centerid, 'Age Group MS' AS character varying(15) age_group_type, diag.age_group_ms age_group,
               pat.sex, diag.enc_year period, diag.dx_code_5dig code_, diag.enc_type setting,
               count(distinct diag.patid) members,
               count(distinct diag.encounterid) events
          FROM esp_diagnosis diag
                 INNER JOIN esp_demographic pat ON diag.patid = pat.patid
        GROUP BY diag.centerid, diag.age_group_ms, pat.sex, diag.enc_year, diag.dx_code_5dig,
                 diag.enc_type) t1
         LEFT JOIN (select * from dbo.static_dx_code
                    where   strpos(trim(code),'.')<>3
                       and length(trim(code))>=3 ) icd9
                 ON t1.code_ = replace(icd9.code, '.', '');
create index diag5dig_centerid_idx on esp_diagnosis_icd9_5dig (centerid);
create index diag5dig_age_group_type_idx on esp_diagnosis_icd9_5dig (age_group_type);
create index diag5dig_age_group_idx on esp_diagnosis_icd9_5dig (age_group);
create index diag5dig_sex_idx on esp_diagnosis_icd9_5dig (sex);
create index diag5dig_period_idx on esp_diagnosis_icd9_5dig (period);
create index diag5dig_code_idx on esp_diagnosis_icd9_5dig (code_);
create index diag5dig_setting_idx on esp_diagnosis_icd9_5dig (setting);
*/

-- UVT_TABLES
--    UVT_SEX
      DROP TABLE esp_popmednet.UVT_SEX;
      CREATE TABLE esp_popmednet.UVT_SEX AS
      SELECT DISTINCT
             pat.sex item_code,
             CASE
               WHEN pat.sex = 'M' THEN CAST('Male' AS character varying(10))
               WHEN pat.sex = 'F' THEN CAST('Female' AS character varying(10))
               WHEN pat.sex = 'U' THEN CAST('Unknown' AS character varying(10))
               ELSE CAST('Not Mapped' AS character varying(10))
             END item_text
        FROM esp_popmednet.esp_demographic pat;
        ALTER TABLE esp_popmednet.UVT_SEX ADD PRIMARY KEY (item_code);

--    UVT_RACE
      DROP TABLE esp_popmednet.UVT_RACE;
      CREATE TABLE esp_popmednet.UVT_RACE AS
      SELECT DISTINCT
             pat.race item_code,
             CASE
               WHEN pat.race = 0 THEN CAST('Unknown' AS character varying(50))
               WHEN pat.race = 1 THEN CAST('American Indian or Alaska Native' AS character varying(50))
               WHEN pat.race = 2 THEN CAST('Asian' AS character varying(50))
               WHEN pat.race = 3 THEN CAST('Black or African American' AS character varying(50))
               WHEN pat.race = 4 THEN CAST('Native Hawaiian or Other Pacific Islander' AS character varying(50))
               WHEN pat.race = 5 THEN CAST('White' AS character varying(50))
               ELSE CAST('Not Mapped' AS character varying(50))
             END item_text
        FROM esp_popmednet.esp_demographic pat;
        ALTER TABLE esp_popmednet.UVT_RACE ADD PRIMARY KEY (item_code);

--    UVT_RACE_ETHNICITY
      DROP TABLE esp_popmednet.UVT_RACE_ETHNICITY;
      CREATE TABLE esp_popmednet.UVT_RACE_ETHNICITY as
      select distinct 
             pat.race_ethnicity item_code,
             case
               when pat.race_ethnicity=5 then CAST('White' AS character varying(50))
               when pat.race_ethnicity=3 then CAST('Black' AS character varying(50))
               when pat.race_ethnicity=2 then CAST('Asian' AS character varying(50))
               when pat.race_ethnicity=6 then CAST('Hispanic' AS character varying(50))
               when pat.race_ethnicity=1 then CAST('Native American' AS character varying(50))
               when pat.race_ethnicity=0 then CAST('Unknown' AS character varying(50))
             end item_text
     from esp_popmednet.esp_demographic pat;
     alter table esp_popmednet.UVT_RACE_ETHNICITY add primary key (item_code);
        
--    UVT_ZIP5
      DROP TABLE esp_popmednet.UVT_ZIP5;
      CREATE TABLE esp_popmednet.UVT_ZIP5 as 
      select distinct 
             zip5 as item_code, 
	         CAST(null AS character varying(10)) item_text
      from esp_popmednet.esp_demographic where zip5 is not null;
      alter table esp_popmednet.UVT_ZIP5 add primary key (item_code);

--    UVT_SMOKING
      DROP TABLE esp_popmednet.UVT_SMOKING;
      CREATE TABLE esp_popmednet.UVT_SMOKING as 
      select distinct 
             smoking as item_code, 
	         CAST(null AS character varying(10)) item_text
      from esp_popmednet.esp_demographic where smoking is not null;
      alter table esp_popmednet.UVT_SMOKING add primary key (item_code);

--    UVT_PROVIDER
      DROP TABLE esp_popmednet.UVT_PROVIDER;
      CREATE TABLE esp_popmednet.UVT_PROVIDER AS
      SELECT DISTINCT
             enc.provider item_code,
             CAST('' AS character varying(10)) item_text
        FROM esp_popmednet.esp_encounter enc;
        ALTER TABLE esp_popmednet.UVT_PROVIDER ADD PRIMARY KEY (item_code);

--    UVT_SITE
      DROP TABLE esp_popmednet.UVT_SITE;
      CREATE TABLE esp_popmednet.UVT_SITE AS
      SELECT DISTINCT
             enc.facility_code item_code,
             enc.facility_location item_text
        FROM esp_popmednet.esp_encounter enc 
      WHERE enc.facility_code is not null;
        ALTER TABLE esp_popmednet.UVT_SITE ADD PRIMARY KEY (item_code);

--    UVT_CENTER
      DROP TABLE esp_popmednet.UVT_CENTER;
      CREATE TABLE esp_popmednet.UVT_CENTER AS
      SELECT DISTINCT
             pat.centerid item_code,
             pat.centerid item_text
        FROM esp_popmednet.esp_demographic pat;
        ALTER TABLE esp_popmednet.UVT_CENTER ADD PRIMARY KEY (item_code);

--    UVT_PERIOD
      DROP TABLE esp_popmednet.UVT_PERIOD;
      CREATE TABLE esp_popmednet.UVT_PERIOD AS
      SELECT DISTINCT
             enc.enc_year item_code,
             CAST(enc.enc_year AS character varying(4)) item_text
        FROM esp_popmednet.esp_encounter enc;
        ALTER TABLE esp_popmednet.UVT_PERIOD ADD PRIMARY KEY (item_code);

--    UVT_ENCOUNTER
      DROP TABLE esp_popmednet.UVT_ENCOUNTER;
      CREATE TABLE esp_popmednet.UVT_ENCOUNTER AS
      SELECT DISTINCT
             enc.enc_type item_code,
             CASE
               WHEN enc.enc_type = 'IP' THEN 'Inpatient Hospital Stay'
               WHEN enc.enc_type = 'IS' THEN 'Non-Acute Institutional Stay'
               WHEN enc.enc_type = 'ED' THEN 'Emergency Department'
               WHEN enc.enc_type = 'AV' THEN 'Ambulatory Visit'
               WHEN enc.enc_type = 'OA' THEN 'Other Ambulatory Visit'
               ELSE 'Not Mapped'
             END item_text
        FROM esp_popmednet.esp_encounter enc;
        ALTER TABLE esp_popmednet.UVT_ENCOUNTER ADD PRIMARY KEY (item_code);

--    UVT_AGEGROUP_5YR
      DROP TABLE esp_popmednet.UVT_AGEGROUP_5YR;
      CREATE TABLE esp_popmednet.UVT_AGEGROUP_5YR AS
      SELECT DISTINCT
             enc.age_group_5yr item_code,
             CAST(enc.age_group_5yr AS character varying(5)) item_text
        FROM esp_popmednet.esp_encounter enc
      WHERE enc.age_group_5yr is not null;
        ALTER TABLE esp_popmednet.UVT_AGEGROUP_5YR ADD PRIMARY KEY (item_code);

--    UVT_AGEGROUP_10YR
      DROP TABLE esp_popmednet.UVT_AGEGROUP_10YR;
      CREATE TABLE esp_popmednet.UVT_AGEGROUP_10YR AS
      SELECT DISTINCT
             enc.age_group_10yr item_code,
             CAST(enc.age_group_10yr AS character varying(5)) item_text
        FROM esp_popmednet.esp_encounter enc
      WHERE enc.age_group_10yr is not null;
        ALTER TABLE esp_popmednet.UVT_AGEGROUP_10YR ADD PRIMARY KEY (item_code);

--    UVT_AGEGROUP_MS
      DROP TABLE esp_popmednet.UVT_AGEGROUP_MS;
      CREATE TABLE esp_popmednet.UVT_AGEGROUP_MS AS
      SELECT DISTINCT
             enc.age_group_ms item_code,
             CAST(enc.age_group_ms AS character varying(5)) item_text
        FROM esp_popmednet.esp_encounter enc
      WHERE enc.age_group_ms is not null;
        ALTER TABLE esp_popmednet.UVT_AGEGROUP_MS ADD PRIMARY KEY (item_code);

--    UVT_DX
      DROP TABLE esp_popmednet.UVT_DX;
      CREATE TABLE esp_popmednet.UVT_DX AS
      SELECT DISTINCT
             diag.dx item_code,
             icd9.name item_text
        FROM esp_popmednet.esp_diagnosis diag
               INNER JOIN dbo.static_dx_code icd9 ON diag.dx = icd9.code
        where icd9.type='icd9';
        ALTER TABLE esp_popmednet.UVT_DX ADD PRIMARY KEY (item_code);

--    UVT_DX_3DIG
      DROP TABLE esp_popmednet.UVT_DX_3DIG;
      CREATE TABLE esp_popmednet.UVT_DX_3DIG AS
      SELECT DISTINCT
             diag.dx_code_3dig item_code,
             icd9.name item_text
        FROM esp_popmednet.esp_diagnosis diag
               LEFT OUTER JOIN  (select * from dbo.static_dx_code
                    where   strpos(trim(code),'.')<>3
                       and length(trim(code))>=3 
                       and type='icd9') icd9 
               ON diag.dx_code_3dig = REPLACE(icd9.code, '.', '')
        WHERE diag.dx_code_3dig is not null;
        ALTER TABLE esp_popmednet.UVT_DX_3DIG ADD PRIMARY KEY (item_code);

--    UVT_DX_4DIG
      DROP TABLE esp_popmednet.UVT_DX_4DIG;
      CREATE TABLE esp_popmednet.UVT_DX_4DIG AS
      SELECT DISTINCT
             diag.dx_code_4dig item_code,
             diag.dx_code_4dig_with_dec item_code_with_dec,
             icd9.name item_text
        FROM esp_popmednet.esp_diagnosis diag
               LEFT OUTER JOIN  dbo.static_dx_code icd9
               ON diag.dx_code_4dig_with_dec = icd9.code
        WHERE diag.dx_code_4dig is not null and icd9.type='icd9';
        ALTER TABLE esp_popmednet.UVT_DX_4DIG ADD PRIMARY KEY (item_code_with_dec);
        create index uvt_dx_code_4dig_idx on esp_popmednet.UVT_DX_4DIG (item_code);

--    UVT_DX_5DIG
      DROP TABLE esp_popmednet.UVT_DX_5DIG;
      CREATE TABLE esp_popmednet.UVT_DX_5DIG AS
      SELECT DISTINCT
             diag.dx_code_5dig item_code,
             diag.dx_code_5dig_with_dec item_code_with_dec,
             icd9.name item_text
        FROM esp_popmednet.esp_diagnosis diag
               LEFT OUTER JOIN  dbo.static_dx_code icd9
               ON diag.dx_code_5dig_with_dec = icd9.code
        WHERE diag.dx_code_5dig is not null and icd9.type='icd9';
        ALTER TABLE esp_popmednet.UVT_DX_5DIG ADD PRIMARY KEY (item_code_with_dec);
        create index uvt_dx_code_5dig_idx on esp_popmednet.UVT_DX_5DIG (item_code);

--    UVT_DETECTED_CONDITION
      DROP TABLE esp_popmednet.UVT_DETECTED_CONDITION;
      CREATE TABLE esp_popmednet.UVT_DETECTED_CONDITION AS
      SELECT DISTINCT
             disease.condition item_code,
             disease.condition item_text
        FROM esp_popmednet.esp_disease disease;
        ALTER TABLE esp_popmednet.UVT_DETECTED_CONDITION ADD PRIMARY KEY (item_code);

--    UVT_DETECTED_CRITERIA
      DROP TABLE esp_popmednet.UVT_DETECTED_CRITERIA;
      CREATE TABLE esp_popmednet.UVT_DETECTED_CRITERIA AS
      SELECT DISTINCT
             disease.criteria item_code,
             disease.criteria item_text
        FROM esp_popmednet.esp_disease disease
      WHERE criteria is not null;
        ALTER TABLE esp_popmednet.UVT_DETECTED_CRITERIA ADD PRIMARY KEY (item_code);
      
--    UVT_DETECTED_STATUS
      DROP TABLE esp_popmednet.UVT_DETECTED_STATUS;
      CREATE TABLE esp_popmednet.UVT_DETECTED_STATUS AS
      SELECT DISTINCT
             disease.status item_code,
             CASE
               WHEN disease.status = 'AR' THEN CAST('Awaiting Review' AS character varying(80))
               WHEN disease.status = 'UR' THEN CAST('Under Review' AS character varying(80))
               WHEN disease.status = 'RM' THEN CAST('Review By MD' AS character varying(80))
               WHEN disease.status = 'FP' THEN CAST('False Positive - Do Not Process' AS character varying(80))
               WHEN disease.status = 'Q'  THEN CAST('Confirmed Case, Transmit to Health Department' AS character varying(80))
               WHEN disease.status = 'S'  THEN CAST('Transmitted to Health Department' AS character varying(80))
               ELSE CAST('Not Mapped' AS character varying(80))
             END item_text
        FROM esp_popmednet.esp_disease disease;
        ALTER TABLE esp_popmednet.UVT_DETECTED_STATUS ADD PRIMARY KEY (item_code);

--    REMOTE KEYS USING UVTs
      ALTER TABLE esp_popmednet.esp_demographic ADD FOREIGN KEY (sex) REFERENCES esp_popmednet.UVT_SEX (item_code);
      ALTER TABLE esp_popmednet.esp_demographic ADD FOREIGN KEY (race) REFERENCES esp_popmednet.UVT_RACE (item_code);
      ALTER TABLE esp_popmednet.esp_encounter ADD FOREIGN KEY (provider) REFERENCES esp_popmednet.UVT_PROVIDER (item_code);
      ALTER TABLE esp_popmednet.esp_encounter ADD FOREIGN KEY (facility_code) REFERENCES esp_popmednet.UVT_SITE (item_code);
      ALTER TABLE esp_popmednet.esp_demographic ADD FOREIGN KEY (centerid) REFERENCES esp_popmednet.UVT_CENTER (item_code);
      ALTER TABLE esp_popmednet.esp_encounter ADD FOREIGN KEY (enc_year) REFERENCES esp_popmednet.UVT_PERIOD (item_code);
      ALTER TABLE esp_popmednet.esp_encounter ADD FOREIGN KEY (enc_type) REFERENCES esp_popmednet.UVT_ENCOUNTER (item_code);
      ALTER TABLE esp_popmednet.esp_encounter ADD FOREIGN KEY (age_group_5yr) 
                  REFERENCES esp_popmednet.UVT_AGEGROUP_5YR (item_code);
      ALTER TABLE esp_popmednet.esp_encounter ADD FOREIGN KEY (age_group_10yr) 
                  REFERENCES esp_popmednet.UVT_AGEGROUP_10YR (item_code);
      ALTER TABLE esp_popmednet.esp_encounter ADD FOREIGN KEY (age_group_ms) 
                  REFERENCES esp_popmednet.UVT_AGEGROUP_MS (item_code);
      ALTER TABLE esp_popmednet.esp_diagnosis ADD FOREIGN KEY (dx) REFERENCES esp_popmednet.UVT_DX (item_code);

      ALTER TABLE esp_popmednet.esp_diagnosis ADD FOREIGN KEY (dx_code_3dig)
                  REFERENCES esp_popmednet.UVT_DX_3DIG (item_code);
      ALTER TABLE esp_popmednet.esp_diagnosis ADD FOREIGN KEY (dx_code_4dig_with_dec)
                  REFERENCES esp_popmednet.UVT_DX_4DIG (item_code_with_dec);
      ALTER TABLE esp_popmednet.esp_diagnosis ADD FOREIGN KEY (dx_code_5dig_with_dec)
                  REFERENCES esp_popmednet.UVT_DX_5DIG (item_code_with_dec);

      ALTER TABLE esp_popmednet.esp_disease ADD FOREIGN KEY (condition) 
                  REFERENCES esp_popmednet.UVT_DETECTED_CONDITION (item_code);
      ALTER TABLE esp_popmednet.esp_disease ADD FOREIGN KEY (criteria) 
                  REFERENCES esp_popmednet.UVT_DETECTED_CRITERIA (item_code);
      ALTER TABLE esp_popmednet.esp_disease ADD FOREIGN KEY (status) 
                  REFERENCES esp_popmednet.UVT_DETECTED_STATUS (item_code);
