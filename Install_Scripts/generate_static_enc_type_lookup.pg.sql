set search_path to public;
CREATE TABLE static_enc_type_lookup
(
  id serial NOT NULL,
  raw_encounter_type character varying(100) NOT NULL,
  rs_mdphnet integer,
  ambulatory integer,
  other_ambulatory integer,
  ed integer,
  inpat_hosp integer,
  prep integer,
  CONSTRAINT static_enc_type_lookup_pkey PRIMARY KEY (raw_encounter_type)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE static_enc_type_lookup
  OWNER TO esp;
GRANT ALL ON TABLE static_enc_type_lookup TO esp;
GRANT SELECT ON TABLE static_enc_type_lookup TO esp_popmednet;
insert into static_enc_type_lookup (raw_encounter_type,rs_mdphnet, ambulatory, other_ambulatory, ed, inpat_hosp, prep) values
('OFFICE VISIT',1,1,0,0,0,0);
select distinct(raw_encounter_type) from emr_encounter where raw_encounter_type not in (select raw_encounter_type from static_enc_type_lookup)
