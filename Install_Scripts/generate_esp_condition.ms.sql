if object_id('dbo.esp_condition','U') IS NOT NULL drop table dbo.esp_condition;
GO
CREATE TABLE esp_condition
( centerid varchar(1),
  patid varchar(128) NOT NULL,
  condition varchar(100) NOT NULL,
  date integer NOT NULL,
  age_at_detect_year integer,
  age_group_5yr varchar(5),
  age_group_10yr varchar(5),
  age_group_ms varchar(5),
  criteria varchar(2000),
  status varchar(32),
  notes text,
  cond_start_date date,
  cond_expire_date date,
  CONSTRAINT esp_condition_r_pkey1 PRIMARY KEY (patid, condition, date)
);
GO
