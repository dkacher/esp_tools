IF object_id('dbo.static_enc_type_lookup', 'U') IS NOT NULL DROP TABLE dbo.static_enc_type_lookup;
GO
CREATE TABLE dbo.static_enc_type_lookup
(
  id int IDENTITY(1,1),
  raw_encounter_type character varying(100) NOT NULL,
  rs_mdphnet integer,
  ambulatory integer,
  other_ambulatory integer,
  ed integer,
  inpat_hosp integer,
  prep integer,
  CONSTRAINT static_enc_type_lookup_pkey PRIMARY KEY (raw_encounter_type)
);
GO
INSERT INTO dbo.static_enc_type_lookup 
SELECT DISTINCT raw_encounter_type, 1 as rs_mdphnet, 1 as ambulatory, 0 as other_ambulatory, 
    0 as ed, 0 asinpat_hosp, 0 as prep
FROM dbo.emr_encounter where raw_encounter_type IS NOT NULL;
GO
