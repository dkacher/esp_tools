﻿/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                         General Population Report
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2013 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some PostgreSQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/
---
-- First build the patient_encounter list
---
select 'Starting to build gen_pop_tools.gpr_enc_pat at: ',getdate();
IF object_id('gen_pop_tools.gpr_enc_pat', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_enc_pat;
GO
select patient_id, max(date) as date
INTO gen_pop_tools.gpr_enc_pat 
from dbo.emr_encounter
group by patient_id;
GO
alter table gen_pop_tools.gpr_enc_pat alter column patient_id int not null;
GO
alter table gen_pop_tools.gpr_enc_pat add primary key (patient_id);
UPDATE STATISTICS gen_pop_tools.gpr_enc_pat;
GO

--
-- now build patient table
--
select 'Starting to build gen_pop_tools.gpr_pat at: ',getdate();
IF object_id('gen_pop_tools.gpr_pat', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_pat;
GO
SELECT 
  pat.id AS patient_id, pat.mrn, DATEDIFF(day, CAST(pat.date_of_birth AS date), getdate())/365.25 as age, pat.gender, pat.race, pat.date_of_death,
  case
    when substring(pat.zip,6,1)='-' then substring(pat.zip,1,5)
    else pat.zip
  end as zip, lastenc.date as last_enc_date
INTO gen_pop_tools.gpr_pat 
FROM dbo.emr_patient pat
join gen_pop_tools.gpr_enc_pat lastenc on lastenc.patient_id=pat.id;
GO
alter table gen_pop_tools.gpr_pat alter column patient_id int not null;
GO
alter table gen_pop_tools.gpr_pat add primary key (patient_id);
UPDATE STATISTICS gen_pop_tools.gpr_pat;
GO
--
-- Max blood pressure between two and three years
--  using most recent max mean aterial pressure for the period
--
select 'Starting to build gen_pop_tools.gpr_enc_bp at: ',getdate();
IF object_id('gen_pop_tools.gpr_enc_bp', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_enc_bp;
GO
select patient_id, date, bp_systolic, bp_diastolic, (2*bp_diastolic + bp_systolic)/3 as mean_arterial, height
INTO gen_pop_tools.gpr_enc_bp 
from dbo.emr_encounter
where bp_systolic is not null and bp_diastolic is not null;
GO
create index gpr_enc_bp_idx
on gen_pop_tools.gpr_enc_bp (patient_id, date);
UPDATE STATISTICS gen_pop_tools.gpr_enc_bp;
GO

select 'Starting to build gen_pop_tools.gpr_bp3 at: ',getdate();
IF object_id('gen_pop_tools.gpr_bp3', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_bp3;
GO
SELECT *
INTO gen_pop_tools.gpr_bp3 
from (
	SELECT 
	  t0.patient_id
	, t0.bp_systolic AS max_bp_systolic
	, t0.bp_diastolic AS max_bp_diastolic
        , t1.max_mean_arterial as map
        , gen_pop_tools.NHBP(DATEDIFF(day, CAST(pat.date_of_birth AS date), t0.date)/365.25, 
                     pat.gender, 
                     gen_pop_tools.CDC_HGT(CAST(DATEDIFF(month, CAST(pat.date_of_birth AS date), t0.date) AS numeric),
						 CAST((case when pat.gender='M' then '1' when pat.gender='F' then 2 else null end) AS varchar(max)), 
						 CAST(t0.height AS numeric), 
						 CAST('HTZ' AS varchar(max)) ), 
					 CAST(t0.bp_systolic AS numeric), 
					 CAST('SYS' AS varchar(max)), 
					 CAST('BPPCT' AS varchar(max))) as syspct
        , gen_pop_tools.NHBP(DATEDIFF(day, CAST(pat.date_of_birth AS date), t0.date)/365.25, 
                     pat.gender, 
                     gen_pop_tools.CDC_HGT(CAST(DATEDIFF(month, CAST(pat.date_of_birth AS date), t0.date) AS numeric),
						 CAST((case when pat.gender='M' then '1' when pat.gender='F' then 2 else null end) AS varchar(max)), 
						 CAST(t0.height AS numeric), 
						 CAST('HTZ' AS varchar(max)) ), 
					 CAST(t0.bp_systolic AS numeric), 
					 CAST('DIA' AS varchar(max)), 
					 CAST('BPPCT' AS varchar(max))) as diapct
        , row_number() over (partition by t0.patient_id order by t0.date desc) as rownum
	FROM gen_pop_tools.gpr_enc_bp t0,
             (select patient_id, max(mean_arterial) as max_mean_arterial
              from gen_pop_tools.gpr_enc_bp
              WHERE date between DATEADD(year, -3, getdate()) and DATEADD(year, -2, getdate())
              group by patient_id) as t1,
             dbo.emr_patient as pat
	WHERE t0.date between DATEADD(year, -3, getdate()) and DATEADD(year, -2, getdate())
	and t0.mean_arterial = t1.max_mean_arterial and t0.patient_id=t1.patient_id
        and t0.patient_id=pat.id) as t
        where rownum=1;
GO
--
-- Max blood pressure between one and two years
--  using most recent max mean aterial pressure for the period
--
select 'Starting to build gen_pop_tools.gpr_bp2 at: ',getdate();
IF object_id('gen_pop_tools.gpr_bp2', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_bp2;
GO
SELECT * 
INTO gen_pop_tools.gpr_bp2  
from (
	SELECT 
	  t0.patient_id
	, t0.bp_systolic AS max_bp_systolic
	, t0.bp_diastolic AS max_bp_diastolic
        , t1.max_mean_arterial as map
        , gen_pop_tools.NHBP(DATEDIFF(day, CAST(pat.date_of_birth AS date), t0.date)/365.25, 
                     pat.gender, 
                     gen_pop_tools.CDC_HGT(CAST(DATEDIFF(month, CAST(pat.date_of_birth AS date), t0.date) AS numeric),
						 CAST((case when pat.gender='M' then '1' when pat.gender='F' then 2 else null end) AS varchar(max)), 
						 CAST(t0.height AS numeric), 
						 CAST('HTZ' AS varchar(max)) ), 
					 CAST(t0.bp_systolic AS numeric), 
					 CAST('SYS' AS varchar(max)), 
					 CAST('BPPCT' AS varchar(max))) as syspct
        , gen_pop_tools.NHBP(DATEDIFF(day, CAST(pat.date_of_birth AS date), t0.date)/365.25, 
                     pat.gender, 
                     gen_pop_tools.CDC_HGT(CAST(DATEDIFF(month, CAST(pat.date_of_birth AS date), t0.date) AS numeric),
						 CAST((case when pat.gender='M' then '1' when pat.gender='F' then 2 else null end) AS varchar(max)), 
						 CAST(t0.height AS numeric), 
						 CAST('HTZ' AS varchar(max)) ), 
					 CAST(t0.bp_systolic AS numeric), 
					 CAST('DIA' AS varchar(max)), 
					 CAST('BPPCT' AS varchar(max))) as diapct
        , row_number() over (partition by t0.patient_id order by t0.date desc) as rownum
	FROM gen_pop_tools.gpr_enc_bp t0,
             (select patient_id, max(mean_arterial) as max_mean_arterial
              from gen_pop_tools.gpr_enc_bp
              WHERE date between ( DATEADD( year, -3, getdate()) ) and ( DATEADD( year, -2, getdate()) )
              group by patient_id) as t1,
             dbo.emr_patient as pat
	 WHERE t0.date between ( DATEADD( year, -2, getdate()) ) and ( DATEADD( year, -1, getdate()) )
	and t0.mean_arterial = t1.max_mean_arterial and t0.patient_id=t1.patient_id
        and t0.patient_id=pat.id) as t
        where rownum=1;
GO
--
-- Max blood pressure between now and one years
--  using most recent max mean aterial pressure for the period
--
select 'Starting to build gen_pop_tools.gpr_bp1 at: ',getdate();
IF object_id('gen_pop_tools.gpr_bp1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_bp1;
GO
SELECT *
INTO gen_pop_tools.gpr_bp1
from (
	SELECT 
	  t0.patient_id
	, t0.bp_systolic AS max_bp_systolic
	, t0.bp_diastolic AS max_bp_diastolic
        , t1.max_mean_arterial as map
        , gen_pop_tools.NHBP(DATEDIFF(day, CAST(pat.date_of_birth AS date), t0.date)/365.25, 
                     pat.gender, 
                     gen_pop_tools.CDC_HGT(CAST(DATEDIFF(month, CAST(pat.date_of_birth AS date), t0.date) AS numeric),
						 CAST((case when pat.gender='M' then '1' when pat.gender='F' then 2 else null end) AS varchar(max)), 
						 CAST(t0.height AS numeric), 
						 CAST('HTZ' AS varchar(max)) ), 
					 CAST(t0.bp_systolic AS numeric), 
					 CAST('SYS' AS varchar(max)), 
					 CAST('BPPCT' AS varchar(max))) as syspct
        , gen_pop_tools.NHBP(DATEDIFF(day, CAST(pat.date_of_birth AS date), t0.date)/365.25, 
                     pat.gender, 
                     gen_pop_tools.CDC_HGT(CAST(DATEDIFF(month, CAST(pat.date_of_birth AS date), t0.date) AS numeric),
						 CAST((case when pat.gender='M' then '1' when pat.gender='F' then 2 else null end) AS varchar(max)), 
						 CAST(t0.height AS numeric), 
						 CAST('HTZ' AS varchar(max)) ), 
					 CAST(t0.bp_systolic AS numeric), 
					 CAST('DIA' AS varchar(max)), 
					 CAST('BPPCT' AS varchar(max))) as diapct
        , row_number() over (partition by t0.patient_id order by t0.date desc) as rownum
	FROM gen_pop_tools.gpr_enc_bp t0,
             (select patient_id, max(mean_arterial) as max_mean_arterial
              from gen_pop_tools.gpr_enc_bp
              WHERE date between ( DATEADD( year, -1, getdate()) ) and getdate()
              group by patient_id) as t1,
             dbo.emr_patient as pat
	WHERE t0.date between ( DATEADD( year, -1, getdate()) ) and getdate() 
	and t0.mean_arterial = t1.max_mean_arterial and t0.patient_id=t1.patient_id
        and t0.patient_id=pat.id) as t
        where rownum=1;
GO
--
-- most recent blood pressure 
--
select 'Starting to build gen_pop_tools.gpr_recbp at: ',getdate();
IF object_id('gen_pop_tools.gpr_recbp', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_recbp;
GO
SELECT 
  t0.patient_id
, max(t0.bp_systolic) as bp_systolic
, max(t0.bp_diastolic) as bp_diastolic
    , max(gen_pop_tools.NHBP(DATEDIFF(day, CAST(pat.date_of_birth AS date), t0.date)/365.25,
                 pat.gender, 
                 gen_pop_tools.CDC_HGT(CAST(DATEDIFF(month, CAST(pat.date_of_birth AS date), t0.date) AS numeric),
                     CAST((case when pat.gender='M' then '1' when pat.gender='F' then 2 else null end) AS varchar(max)), 
                     CAST(t0.height AS numeric), 
                     CAST('HTZ' AS varchar(max)) ), 
                 CAST(t0.bp_systolic AS numeric), 
                 CAST('SYS' AS varchar(max)), 
                 CAST('BPPCT' AS varchar(max)))) as syspct
    , max(gen_pop_tools.NHBP(DATEDIFF(day, CAST(pat.date_of_birth AS date), t0.date)/365.25, 
                 pat.gender, 
                 gen_pop_tools.CDC_HGT(CAST(DATEDIFF(month, CAST(pat.date_of_birth AS date), t0.date) AS numeric),
                     CAST((case when pat.gender='M' then '1' when pat.gender='F' then 2 else null end) AS varchar(max)), 
                     CAST(t0.height AS numeric), 
                     CAST('HTZ' AS varchar(max)) ), 
                 CAST(t0.bp_systolic AS numeric), 
                 CAST('DIA' AS varchar(max)), 
                 CAST('BPPCT' AS varchar(max)))) as diapct
, t0.date
    , max(t0.mean_arterial) as map
INTO gen_pop_tools.gpr_recbp
	FROM gen_pop_tools.gpr_enc_bp as t0,
	     (select patient_id, max(date) as date from gen_pop_tools.gpr_enc_bp 
	           group by patient_id) as t1,
             dbo.emr_patient as pat
	WHERE   t0.patient_id=t1.patient_id and
	   t0.date = t1.date and t0.date >= DATEADD( year, -2, getdate())
           and t0.patient_id=pat.id
	GROUP BY t0.patient_id, t0.date;
GO
--
-- most recent BMI 
--
select 'Starting to build gen_pop_tools.gpr_bmi at: ',getdate();
IF object_id('gen_pop_tools.gpr_bmi', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_bmi;
GO
select t0.*
    , gen_pop_tools.CDC_BMI(DATEDIFF(month, CAST(pat.date_of_birth AS date), t0.date),
    CAST((case when pat.gender='M' then '1' when pat.gender='F' then '2' else null end) AS varchar(max)), 
    CAST(null AS numeric), 
    CAST(null AS numeric), 
    CAST(t0.bmi AS numeric), 
    CAST('BMIPCT' AS varchar(max))) as bmipct
INTO gen_pop_tools.gpr_bmi
from (
	SELECT 
	  t0.patient_id,
	  t0.date
	, MAX( t0.weight / POWER((t0.height/100),2) ) AS bmi
	FROM dbo.emr_encounter t0,
	     (select patient_id, max(date) as date
	      from dbo.emr_encounter 
	      where weight is not null and height is not null
	      group by patient_id) t1
	WHERE t0.date >= ( DATEADD(year, -2, getdate()) )
	  and t0.weight is not null and t0.height is not null
	  and t0.date=t1.date
	  and t0.patient_id=t1.patient_id
	GROUP BY t0.patient_id, t0.date
) t0,
  dbo.emr_patient as pat
  where t0.patient_id=pat.id;
GO
--
-- lab subset
--
select 'Starting to build gen_pop_tools.gpr_sublab at: ',getdate();
IF object_id('gen_pop_tools.gpr_sublab', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_sublab;
GO
select patient_id, date, result_float, t1.test_name
INTO gen_pop_tools.gpr_sublab
from dbo.emr_labresult t0
  inner join conf_labtestmap t1
    on t1.native_code=t0.native_code 
where t1.test_name in ('a1c','cholesterol-ldl','triglycerides','gonorrhea','chlamydia') 
  and (t0.result_float is not null or t1.test_name in ('gonorrhea','chlamydia'));
GO
create index gpr_sublab_patid_idx on gen_pop_tools.gpr_sublab (patient_id);
create index gpr_sublab_testname_idx on gen_pop_tools.gpr_sublab (test_name);
create index gpr_sublab_date_idx on gen_pop_tools.gpr_sublab (date);
UPDATE STATISTICS gen_pop_tools.gpr_sublab;
GO
--
-- Recent A1C lab result
--
select 'Starting to build gen_pop_tools.gpr_a1c at: ',getdate();
IF object_id('gen_pop_tools.gpr_a1c', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_a1c;
GO
SELECT 
	  l0.patient_id
	, l0.date  
	, MAX(l0.result_float) AS recent_a1c
INTO gen_pop_tools.gpr_a1c
	FROM gen_pop_tools.gpr_sublab l0,
	(
		SELECT 
		  l1.patient_id
		, l1.test_name
		, MAX(l1.date) AS date
		FROM gen_pop_tools.gpr_sublab l1
		where l1.test_name = 'a1c'
		GROUP BY 
		  l1.patient_id
		, l1.test_name
	) u0
	WHERE l0.test_name = 'a1c'
	  and u0.patient_id = l0.patient_id
		AND u0.test_name = l0.test_name
		AND u0.date=l0.date
	  AND l0.date >= ( DATEADD(year, -2, getdate()) )
	GROUP BY 
	  l0.patient_id, l0.date;
GO
--
-- Max A1C lab result last year
--
select 'Starting to build gen_pop_tools.gpr_a1c1 at: ',getdate();
IF object_id('gen_pop_tools.gpr_a1c1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_a1c1;
GO
SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_a1c1
INTO gen_pop_tools.gpr_a1c1
	FROM gen_pop_tools.gpr_sublab l0
	WHERE l0.test_name = 'a1c'
	  AND l0.date between ( DATEADD(year, -1, getdate()) ) and getdate()
	GROUP BY 
	  l0.patient_id;
GO
--
-- Max A1C lab result between 1 and 2 years ago
--
select 'Starting to build gen_pop_tools.gpr_a1c2 at: ',getdate();
IF object_id('gen_pop_tools.gpr_a1c2', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_a1c2;
GO
SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_a1c2
INTO gen_pop_tools.gpr_a1c2
	FROM gen_pop_tools.gpr_sublab l0
	WHERE l0.test_name = 'a1c'
	  AND l0.date between ( DATEADD(year, -2, getdate()) ) and ( DATEADD(year, -1, getdate()) )
	GROUP BY 
	  l0.patient_id;
GO
--
-- Max A1C lab result between 2 and 3 years ago
--
select 'Starting to build gen_pop_tools.gpr_a1c3 at: ',getdate();
IF object_id('gen_pop_tools.gpr_a1c3', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_a1c3;
GO
SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_a1c3
INTO gen_pop_tools.gpr_a1c3
	FROM gen_pop_tools.gpr_sublab l0
	WHERE l0.test_name = 'a1c'
	  AND l0.date between ( DATEADD(year, -3, getdate()) ) and ( DATEADD(year, -2, getdate()) )
	GROUP BY 
	  l0.patient_id;
GO
--
-- Recent cholesterol LDL lab result
--
select 'Starting to build gen_pop_tools.gpr_ldl at: ',getdate();
IF object_id('gen_pop_tools.gpr_ldl', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_ldl;
GO
SELECT 
	  l0.patient_id
	, l0.date
	, MAX(l0.result_float) AS recent_ldl
INTO gen_pop_tools.gpr_ldl
	FROM gen_pop_tools.gpr_sublab l0
	INNER JOIN (
		SELECT 
		  l1.patient_id
		, l1.test_name
		, MAX(l1.date) AS date
		FROM gen_pop_tools.gpr_sublab l1
        where l1.test_name = 'cholesterol-ldl'
		GROUP BY 
		  l1.patient_id
		, l1.test_name
	) u0
		ON u0.patient_id = l0.patient_id
		AND u0.date = l0.date
	WHERE l0.test_name = 'cholesterol-ldl'
	AND l0.date >= ( DATEADD(year, -2, getdate()) )
	GROUP BY 
	  l0.patient_id, l0.date;
GO
--
-- Max ldl lab result last year
--
select 'Starting to build gen_pop_tools.gpr_ldl1 at: ',getdate();
IF object_id('gen_pop_tools.gpr_ldl1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_ldl1;
GO
SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_ldl1
INTO gen_pop_tools.gpr_ldl1
	FROM gen_pop_tools.gpr_sublab l0
	WHERE l0.test_name = 'cholesterol-ldl'
	  AND l0.date between ( DATEADD(year, -1, getdate()) ) and getdate()
	GROUP BY 
	  l0.patient_id;
GO
--
-- Max ldl lab result between 1 and 2 years ago
--
select 'Starting to build gen_pop_tools.gpr_ldl2 at: ',getdate();
IF object_id('gen_pop_tools.gpr_ldl2', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_ldl2;
GO
SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_ldl2
INTO gen_pop_tools.gpr_ldl2
	FROM gen_pop_tools.gpr_sublab l0
	WHERE l0.test_name = 'cholesterol-ldl'
	  AND l0.date between ( DATEADD(year, -2, getdate()) ) and ( DATEADD(year, -1, getdate()) )
	GROUP BY 
	  l0.patient_id;
GO
--
-- Max ldl lab result between 2 and 3 years ago
--
select 'Starting to build gen_pop_tools.gpr_ldl3 at: ',getdate();
IF object_id('gen_pop_tools.gpr_ldl3', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_ldl3;
GO
SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_ldl3
INTO gen_pop_tools.gpr_ldl3
	FROM gen_pop_tools.gpr_sublab l0
	WHERE l0.test_name = 'cholesterol-ldl'
	  AND l0.date between ( DATEADD(year, -3, getdate()) ) and ( DATEADD(year, -2, getdate()) )
	GROUP BY 
	  l0.patient_id;
GO
--
-- Recent Triglycerides lab result
--
select 'Starting to build gen_pop_tools.gpr_trig at: ',getdate();
IF object_id('gen_pop_tools.gpr_trig', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_trig;
GO
SELECT 
	  l0.patient_id
	, l0.date
	, MAX(l0.result_float) AS recent_tgl 
INTO gen_pop_tools.gpr_trig
	FROM gen_pop_tools.gpr_sublab l0
	INNER JOIN (
		SELECT 
		  l1.patient_id
		, l1.test_name
		, MAX(l1.date) AS date
		FROM gen_pop_tools.gpr_sublab l1
		where l1.test_name = 'triglycerides'
		GROUP BY 
		  l1.patient_id
		, l1.test_name
	) u0
		ON u0.patient_id = l0.patient_id
		AND u0.test_name = l0.test_name
		AND u0.date = l0.date
	WHERE l0.test_name = 'triglycerides'
	AND l0.date >= ( DATEADD(year, -2, getdate()) )
	GROUP BY 
	  l0.patient_id, l0.date;
GO
--
-- Max trig lab result last year
--
select 'Starting to build gen_pop_tools.gpr_trig1 at: ',getdate();
IF object_id('gen_pop_tools.gpr_trig1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_trig1;
GO
SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_trig1
INTO gen_pop_tools.gpr_trig1
	FROM gen_pop_tools.gpr_sublab l0
	WHERE l0.test_name = 'triglycerides'
	  AND l0.date between ( DATEADD(year, -1, getdate()) ) and getdate()
	GROUP BY 
	  l0.patient_id;
GO
--
-- Max trig lab result between 1 and 2 years ago
--
select 'Starting to build gen_pop_tools.gpr_trig2 at: ',getdate();
IF object_id('gen_pop_tools.gpr_trig2', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_trig2;
GO
	SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_trig2
INTO gen_pop_tools.gpr_trig2 
	FROM gen_pop_tools.gpr_sublab l0
	WHERE l0.test_name = 'triglycerides'
	  AND l0.date between ( DATEADD(year, -2, getdate()) ) and ( DATEADD(year, -1, getdate()) )
	GROUP BY 
	  l0.patient_id;
GO
--
-- Max trig lab result between 2 and 3 years ago
--
select 'Starting to build gen_pop_tools.gpr_trig3 at: ',getdate();
IF object_id('gen_pop_tools.gpr_trig3', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_trig3;
GO
SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_trig3
INTO gen_pop_tools.gpr_trig3
	FROM gen_pop_tools.gpr_sublab l0
	WHERE l0.test_name = 'triglycerides'
	  AND l0.date between ( DATEADD(year, -3, getdate()) ) and ( DATEADD(year, -2, getdate()) )
	GROUP BY 
	  l0.patient_id;
GO
--
-- Prediabetes
--
select 'Starting to build gen_pop_tools.gpr_predm at: ',getdate();
IF object_id('gen_pop_tools.gpr_predm', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_predm;
GO
SELECT 
	  DISTINCT c0.patient_id
	, 1 AS prediabetes
INTO gen_pop_tools.gpr_predm
	FROM nodis_case c0
	INNER JOIN nodis_case c1
		ON c1.patient_id = c0.patient_id
		AND c1.condition NOT LIKE 'diabetes:type-%'
	WHERE c0.condition = 'diabetes:prediabetes'
	     and not exists (select null from nodis_case c2 
	                     where c2.date <= '2008-01-01'
	                         and c2.patient_id=c0.patient_id
	                         and c2.condition = 'diabetes:prediabetes')
	     and not exists (select null from nodis_case c3
	                     where c3.patient_id=c0.patient_id
	                         and c3.condition in ('diabetes:type-1','diabetes:type-2'));
GO
--
-- Type 1 Diabetes
--
select 'Starting to build gen_pop_tools.gpr_type1 at: ',getdate();
IF object_id('gen_pop_tools.gpr_type1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_type1;
GO
SELECT 
	DISTINCT patient_id
	, 1 AS type_1_diabetes
INTO gen_pop_tools.gpr_type1
	FROM nodis_case
	WHERE condition = 'diabetes:type-1';
GO
--
-- Type 2 Diabetes
--
select 'Starting to build gen_pop_tools.gpr_type2 at: ',getdate();
IF object_id('gen_pop_tools.gpr_type2', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_type2;
GO
SELECT 
	DISTINCT patient_id
	, 1 AS type_2_diabetes
INTO gen_pop_tools.gpr_type2
	FROM nodis_case
	WHERE condition = 'diabetes:type-2';
GO
--
-- Current Gestational diabetes
--
select 'Starting to build gen_pop_tools.gpr_gdm at: ',getdate();
IF object_id('gen_pop_tools.gpr_gdm', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_gdm;
GO
SELECT distinct
	c0.patient_id
	, 1 AS current_gdm
INTO gen_pop_tools.gpr_gdm
	FROM nodis_case AS c0
        INNER JOIN nodis_case_timespans AS nct0
            ON nct0.case_id = c0.id
        INNER JOIN hef_timespan AS ts0
            ON nct0.timespan_id = ts0.id
	WHERE c0.condition = 'diabetes:gestational'
            AND ts0.start_date <= getdate()
            AND ( ts0.end_date >= getdate() OR ts0.end_date IS NULL);
GO
--
-- Recent Gestational diabetes 1 year
--
select 'Starting to build gen_pop_tools.gpr_recgdm1 at: ',getdate();
IF object_id('gen_pop_tools.gpr_recgdm1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_recgdm1;
GO
SELECT distinct
	c0.patient_id
	, 1 AS recent_gdm
INTO gen_pop_tools.gpr_recgdm1
	FROM nodis_case AS c0
        INNER JOIN nodis_case_timespans AS nct0
            ON nct0.case_id = c0.id
        INNER JOIN hef_timespan AS ts0
            ON nct0.timespan_id = ts0.id
	WHERE c0.condition = 'diabetes:gestational' and
	             ((start_date between (DATEADD(year, -1, getdate()))  and getdate() and (end_date >= start_date or end_date is null))
	           or end_date between (DATEADD(year, -1, getdate())) and getdate());
GO
--
-- Recent Gestational diabetes 2 year
--
select 'Starting to build gen_pop_tools.gpr_recgdm2 at: ',getdate();
IF object_id('gen_pop_tools.gpr_recgdm2', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_recgdm2;
GO
SELECT distinct
	c0.patient_id
	, 1 AS recent_gdm
INTO gen_pop_tools.gpr_recgdm2
	FROM nodis_case AS c0
        INNER JOIN nodis_case_timespans AS nct0
            ON nct0.case_id = c0.id
        INNER JOIN hef_timespan AS ts0
            ON nct0.timespan_id = ts0.id
	WHERE c0.condition = 'diabetes:gestational' and
	             ((start_date between (DATEADD(year, -2, getdate())) and getdate() and (end_date >= start_date or end_date is null))
	           or end_date between (DATEADD(year, -2, getdate())) and getdate());
GO
--
-- Recent pregnancy 2 year
--
select 'Starting to build gen_pop_tools.gpr_recpreg2 at: ',getdate();
IF object_id('gen_pop_tools.gpr_recpreg2', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_recpreg2;
GO
SELECT
	  patient_id
	, 1 AS recent_pregnancy -- if there is more than one pregnancy in the period, take the last one
INTO gen_pop_tools.gpr_recpreg2
FROM hef_timespan
	WHERE name = 'pregnancy' and 
	             ((start_date between (DATEADD(year, -2, getdate())) and getdate() and (end_date >= start_date or end_date is null))
	           or end_date between (DATEADD(year, -2, getdate())) and getdate())
	GROUP BY patient_id;
GO
--
-- Recent pregnancy 1 year
--
select 'Starting to build gen_pop_tools.gpr_recpreg1 at: ',getdate();
IF object_id('gen_pop_tools.gpr_recpreg1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_recpreg1;
GO
SELECT
	  patient_id
	, 1 AS recent_pregnancy
INTO gen_pop_tools.gpr_recpreg1
	FROM hef_timespan
	WHERE name = 'pregnancy' and 
	             ((start_date between (DATEADD(year, -1, getdate())) and getdate() and (end_date >= start_date or end_date is null))
	           or end_date between (DATEADD(year, -1, getdate())) and getdate())
	GROUP BY patient_id;
GO
--
-- Current pregnancy
--
select 'Starting to build gen_pop_tools.gpr_curpreg at: ',getdate();
IF object_id('gen_pop_tools.gpr_curpreg', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_curpreg;
GO
SELECT
	  DISTINCT ts.patient_id
	, 1 AS currently_pregnant
INTO gen_pop_tools.gpr_curpreg
	FROM hef_timespan ts
	WHERE name = 'pregnancy'
            AND start_date between (DATEADD(month, -9, getdate())) and getdate() 
            AND ( end_date >= getdate() OR end_date IS NULL);
GO
--
-- Insulin
--    Prescription for insulin within the previous year
--
select 'Starting to build gen_pop_tools.gpr_insulin at: ',getdate();
IF object_id('gen_pop_tools.gpr_insulin', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_insulin;
GO
SELECT 
	  DISTINCT patient_id
	, 1 AS insulin
INTO gen_pop_tools.gpr_insulin
	FROM dbo.emr_prescription
	WHERE lower(name) LIKE '%insulin%'
            AND date >= ( DATEADD(year, -1, getdate()) );
GO
--
-- Metformin
--     Prescription for metformin within the previous year
--
select 'Starting to build gen_pop_tools.gpr_metformin at: ',getdate();
IF object_id('gen_pop_tools.gpr_metformin', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_metformin;
GO
SELECT 
	  DISTINCT patient_id
	, 1 AS metformin
INTO gen_pop_tools.gpr_metformin  
	FROM dbo.emr_prescription
	WHERE lower(name) LIKE '%metformin%'
            AND date >= ( DATEADD(year, -1, getdate()) );
GO
--
-- Influenza vaccine
--     Prescription for influenza vaccine current flu season
--
select 'Starting to build gen_pop_tools.gpr_flu_cur at: ',getdate();
IF object_id('gen_pop_tools.gpr_flu_cur', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_flu_cur;
GO
SELECT 
	  DISTINCT patient_id
	, 1 AS influenza_vaccine
INTO gen_pop_tools.gpr_flu_cur
	FROM dbo.emr_immunization
	WHERE lower(name) LIKE '%influenza%'
	AND case
		  when DATEPART(quarter, getdate())=4 then DATEPART(year, getdate())+1
		  else DATEPART(year, getdate())
	    end =
	    case
		  when DATEPART(quarter, date)=4 then DATEPART(year, date)+1
		  else DATEPART(year, date)
	    end;
GO
--
-- Influenza vaccine
--     Prescription for influenza vaccine previous flu season
--
select 'Starting to build gen_pop_tools.gpr_flu_prev at: ',getdate();
IF object_id('gen_pop_tools.gpr_flu_prev', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_flu_prev;
GO
SELECT 
	  DISTINCT patient_id
	, 1 AS influenza_vaccine
INTO gen_pop_tools.gpr_flu_prev
	FROM dbo.emr_immunization
	WHERE lower(name) LIKE '%influenza%'
	AND case
		  when DATEPART(quarter, getdate())=4 then DATEPART(year, getdate())
		  else DATEPART(year, getdate())-1
	    end =
	    case
		  when DATEPART(quarter, date)=4 then DATEPART(year, date)
		  else DATEPART(year, date)-1
	    end;
GO
--
-- most recent chlamydia lab result
--
select 'Starting to build gen_pop_tools.gpr_chlamydia at: ',getdate();
IF object_id('gen_pop_tools.gpr_chlamydia', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_chlamydia;
GO
SELECT 
	  l0.patient_id
	, case 
            when l0.date >= ( DATEADD(year, -1, getdate()) ) then '1'
            when l0.date >= ( DATEADD(year, -2, getdate()) ) then '2'
            when l0.date < (DATEADD(year, -2, getdate())) then '3'
            else '4'     
          end  AS recent_chlamydia 
INTO gen_pop_tools.gpr_chlamydia
	FROM gen_pop_tools.gpr_sublab l0
	INNER JOIN (
		SELECT 
		  l1.patient_id
		, MAX(l1.date) AS date
		FROM gen_pop_tools.gpr_sublab l1
		where l1.test_name = 'chlamydia'
		GROUP BY 
		  l1.patient_id
		, l1.test_name
	) u0
		ON u0.patient_id = l0.patient_id
		AND u0.date = l0.date
	WHERE l0.test_name = 'chlamydia'
	GROUP BY 
	  l0.patient_id, l0.date;
GO
--
-- most recent gonorrhea lab result
--
select 'Starting to build gen_pop_tools.gpr_gonorrhea at: ',getdate();
IF object_id('gen_pop_tools.gpr_gonorrhea', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_gonorrhea;
GO
SELECT 
	  l0.patient_id
	, case 
            when l0.date >= ( DATEADD(year, -1, getdate()) ) then '1'
            when l0.date >= ( DATEADD(year, -2, getdate()) ) then '2'
            when l0.date < (DATEADD(year, -2, getdate())) then '3'
            else '4'     
          end  AS recent_gonorrhea 
INTO gen_pop_tools.gpr_gonorrhea
	FROM gen_pop_tools.gpr_sublab l0
	INNER JOIN (
		SELECT 
		  l1.patient_id
		, MAX(l1.date) AS date
		FROM gen_pop_tools.gpr_sublab l1
		where l1.test_name = 'gonorrhea'
		GROUP BY 
		  l1.patient_id
		, l1.test_name
	) u0
		ON u0.patient_id = l0.patient_id
		AND u0.date = l0.date
	WHERE l0.test_name = 'gonorrhea'
	GROUP BY 
	  l0.patient_id, l0.date;
GO
--
-- Smoking
--
select 'Starting to build gen_pop_tools.gpr_smoking at: ',getdate();
IF object_id('gen_pop_tools.gpr_smoking', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_smoking;
GO
select case when t1.latest='Yes' then '1'
               when t2.yesOrQuit='Quit' then '2'
               when t3.passive='Passive' then '4'
               when t4.never='Never' then '3'
               else '5' 
           end as smoking,
           t1.patient_id
INTO gen_pop_tools.gpr_smoking
from
     (select t00.tobacco_use as latest, t00.patient_id 
      from dbo.emr_socialhistory t00
      inner join
      (select max(date) as maxdate, patient_id 
       from dbo.emr_socialhistory 
       where tobacco_use is not null and tobacco_use<>''
       group by patient_id) t01 on t00.patient_id=t01.patient_id and t00.date=t01.maxdate) t1
   left outer join
     (select max(val) as yesOrQuit, patient_id
      from (select CAST('Quit' AS varchar(max)) as val, patient_id
            from dbo.emr_socialhistory where tobacco_use in ('Yes','Quit')) t00
            group by patient_id) t2 on t1.patient_id=t2.patient_id
   left outer join
     (select max(val) as passive, patient_id
      from (select CAST('Passive' AS varchar(max)) as val, patient_id
            from dbo.emr_socialhistory where tobacco_use ='Passive') t00
            group by patient_id) t3 on t1.patient_id=t3.patient_id
   left outer join
     (select max(val) as never, patient_id
      from (select CAST('Never' AS varchar(max)) as val, patient_id
            from dbo.emr_socialhistory where tobacco_use ='Never') t00
            group by patient_id) t4 on t1.patient_id=t4.patient_id;
GO
--
-- Asthma
--
select 'Starting to build gen_pop_tools.gpr_asthma at: ',getdate();
IF object_id('gen_pop_tools.gpr_asthma', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_asthma;
GO
select case when count(*) > 0 then 1 else 2 end as asthma,
  patient_id
INTO gen_pop_tools.gpr_asthma
from nodis_case
where condition='asthma'
group by patient_id;
GO
--
-- Number of encounters last year
--
select 'Starting to build gen_pop_tools.gpr_enc at: ',getdate();
IF object_id('gen_pop_tools.gpr_enc', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_enc;
GO
select case when count(*) >= 2 then 2
              else count(*) end as nvis,
  patient_id
INTO gen_pop_tools.gpr_enc
  from dbo.emr_encounter
  where date>=DATEADD(year, -1, GETDATE())
  group by patient_id;        
GO
--
-- Depression
--
select 'Starting to build gen_pop_tools.gpr_depression at: ',getdate();
IF object_id('gen_pop_tools.gpr_depression', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_depression;
GO
SELECT
      hef.patient_id
    , case
        when hef.date >= ( DATEADD(year, -1, getdate()) ) then '1'
        when hef.date >= ( DATEADD(year, -2, getdate()) ) then '2'
        else '3'
      end  AS depression
INTO gen_pop_tools.gpr_depression
from nodis_case nodis
  join nodis_case_events nce on nce.case_id=nodis.id
  join hef_event hef on hef.id=nce.event_id
  JOIN (
        SELECT
          hf.patient_id
         , MAX(hf.date) AS date
        FROM nodis_case nds
          join nodis_case_events nce on nce.case_id=nds.id
          join hef_event hf on hf.id=nce.event_id
                where nds.condition = 'depression'
                GROUP BY
                  hf.patient_id
        ) u0
                ON u0.patient_id = hef.patient_id
                AND u0.date = hef.date
where condition='depression'
        GROUP BY
          hef.patient_id, hef.date;
GO
--
-- Opioid
--
select 'Starting to build gen_pop_tools.gpr_opi_in_progress at: ',getdate();
IF object_id('gen_pop_tools.gpr_opi_in_progress', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_opi_in_progress;
GO
SELECT T2.id as patient_id,
    case
        when DATEADD(day, T1.date, '1960-01-01') >= DATEADD(day, -1, DATEADD(year, -1, getdate())) and condition = 'opioidrx' then '1'
        when DATEADD(day, T1.date, '1960-01-01') >= DATEADD(day, -1, DATEADD(year, -2, getdate()))and condition = 'opioidrx' then '2'
        when DATEADD(day, T1.date, '1960-01-01') < DATEADD(day, -1, DATEADD(year, -2, getdate())) and condition = 'opioidrx' then '3'
        else '4'
        end any_opi,
    case
        when DATEADD(day, T1.date, '1960-01-01') >= DATEADD(day, -1, DATEADD(year, -1, getdate())) and condition = 'benzodiarx' then '1'
        when DATEADD(day, T1.date, '1960-01-01') >= DATEADD(day, -1, DATEADD(year, -2, getdate())) and condition = 'benzodiarx' then '2'
        when DATEADD(day, T1.date, '1960-01-01') < DATEADD(day, -1, DATEADD(year, -2, getdate())) and condition = 'benzodiarx' then '3'
        else '4'
        end any_benzo,
    case
        when DATEADD(day, T1.date, '1960-01-01') >= DATEADD(day, -1, DATEADD(year, -1, getdate()))and condition = 'benzopiconcurrent' then '1'
        when DATEADD(day, T1.date, '1960-01-01') >= DATEADD(day, -1, DATEADD(year, -2, getdate())) and condition = 'benzopiconcurrent' then '2'
        when DATEADD(day, T1.date, '1960-01-01') < DATEADD(day, -1, DATEADD(year, -2, getdate())) and condition = 'benzopiconcurrent' then '3'
        else '4'
        end  concur_opi_benzo,
    case
        when DATEADD(day, T1.date, '1960-01-01') >= DATEADD(day, -1, DATEADD(year, -1, getdate())) and condition = 'highopioiduse' then '1'
        when DATEADD(day, T1.date, '1960-01-01') >= DATEADD(day, -1, DATEADD(year, -2, getdate())) and condition = 'highopioiduse' then '2'
        when DATEADD(day, T1.date, '1960-01-01') < DATEADD(day, -1, DATEADD(year, -2, getdate())) and condition = 'highopioiduse'  then '3'
        else '4'
        end   high_dose_opi
INTO gen_pop_tools.gpr_opi_in_progress
FROM dbo.esp_condition T1
    INNER JOIN dbo.emr_patient T2 ON ((T1.patid = T2.natural_key))
    GROUP BY T1.condition, T2.id, T1.date;
GO

select 'Starting to build gen_pop_tools.gpr_opi at: ',getdate();
IF object_id('gen_pop_tools.gpr_opi', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_opi;
GO
SELECT T1.patient_id,
           min(T1.any_opi) any_opi,
           min(T1.any_benzo) any_benzo,
           min(T1.concur_opi_benzo) concur_opi_benzo,
           min(T1.high_dose_opi) high_dose_opi
INTO gen_pop_tools.gpr_opi
FROM  gen_pop_tools.gpr_opi_in_progress T1
    GROUP BY T1.patient_id;
GO

DROP TABLE gen_pop_tools.gpr_opi_in_progress;
--
-- Hypertension
--
select 'Starting to build gen_pop_tools.gpr_hypertension at: ',getdate();
IF object_id('gen_pop_tools.gpr_hypertension', 'U') IS NOT NULL DROP TABLE gen_pop_tools.gpr_hypertension;
GO
SELECT C1.patient_id,
--    case 
--       when C1.isactive or C1.inactive_date>= (DATEADD(year, -1, getdate())) then '1'
--       when C1.inactive_date between (DATEADD(year, -1, getdate())) and (DATEADD(year, -2, getdate())) then '2'
--       when C1.inactive_date < (DATEADD(year, -2, getdate())) then '3'
--       else '4'
--    end as hypertension
    CAST('4' AS varchar(max)) as hypertension
INTO gen_pop_tools.gpr_hypertension
FROM nodis_case C1
WHERE C1.condition='hypertension' and 
   C1.date=(select max(C2.date) 
            from nodis_case C2
            where C2.patient_id=C1.patient_id and C2.condition='hypertension');
GO

--appended original functions.pg.sql 

IF object_id('gen_pop_tools.cc_hiv_risk_score', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_hiv_risk_score;
GO
CREATE TABLE gen_pop_tools.cc_hiv_risk_score
(
  patient_id integer NOT NULL,
  rpt_year varchar(4) NOT NULL,
  hiv_risk_score numeric,
  hiv_test_yn integer,
  truvada_rx_yn integer,
  CONSTRAINT cc_hiv_risk_score_pkey PRIMARY KEY (patient_id, rpt_year)
);
GO

