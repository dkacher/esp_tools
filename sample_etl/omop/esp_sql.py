#!/usr/bin/python
"""
Name: esp_sql.py

Purpose:  Container for SQL commands.

Modifications:
    17-Apr-2020 Created.  MC
	29-May-2020 Update queries. MC
"""

import esp_gd

SELECT_ESP_Patient_TABLE="""
SELECT	p.person_id AS natural_key,
		p.person_source_value AS mrn,
		NULL AS last_name,
		NULL AS first_name,
		NULL AS middle_name,
		NULL AS address1,
		NULL AS address2,
		loc.city AS city,
		loc.state AS state,
		loc.zip AS zip,
		NULL AS country,
		NULL AS areacode,
		NULL AS tel,
		NULL AS tel_ext,
        case
            when p.year_of_birth is null then null
            when p.month_of_birth is null then trim(to_char(p.year_of_birth,'9999'))
            when p.day_of_birth is null then trim(to_char(p.year_of_birth,'9999')) || trim(to_char(p.month_of_birth,'99'))
            else trim(to_char(p.year_of_birth,'9999')) || trim(to_char(p.month_of_birth,'99')) || trim(to_char(p.day_of_birth,'99'))
        end date_of_birth,
		gcon.concept_name gender,
		rcon.concept_name race,
		NULL AS home_language,
		NULL AS ssn,
		NULL AS pcp_id,
		NULL AS marital_stat,
		NULL AS religion,
		NULL AS aliases,
		NULL AS mother_mrn,
		to_char(d.death_date,'yyyymmdd') AS date_of_death,
		NULL AS center_id,
		econ.concept_name ethnicity
FROM person p
left join concept gcon on gcon.concept_id=p.gender_concept_id
left join location loc on loc.location_id=p.location_id
left join concept rcon on rcon.concept_id=p.race_concept_id
left join concept econ on econ.concept_id=p.ethnicity_concept_id
left join death d on p.person_id=d.person_id
WHERE (exists (SELECT null FROM visit_occurrence v
	WHERE visit_start_date < %s AND visit_start_date >= %s
      and p.person_id=v.person_id)
    or exists (SELECT null from drug_exposure d
	WHERE drug_exposure_start_date < %s AND drug_exposure_start_date >= %s
      and p.person_id=d.person_id)
    or exists (SELECT null from measurement m
	WHERE measurement_date < %s AND measurement_date >= %s
      and p.person_id=m.person_id)
    or exists (SELECT null from observation o
	WHERE observation_date < %s AND observation_date >= %s
      and p.person_id=o.person_id))
""" % (esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday)

ESP_Patient_DICT= {'SELECT':SELECT_ESP_Patient_TABLE}

SELECT_ESP_Visit_TABLE="""
SELECT	v.person_id patient_id,
		p.person_source_value AS mrn,
		v.visit_occurrence_id AS natural_key,
		to_char(v.visit_start_date, 'yyyyMMdd') AS date,
		NULL AS is_closed,
		NULL AS date_closed,
		NULL AS provider_id,
		NULL AS site_natural_key,
		NULL AS site_name,
		vcon.concept_name AS raw_encounter_type,
		NULL AS edd, --concept table has "Estimated date of delivery" as concept, but this data is not found in any of obs, meas, cond.
		m1.value_as_number AS raw_temperature,
		NULL AS cpt,
		m2.value_as_number AS raw_weight,
		m3.value_as_number AS raw_height,
		Vital.SYSTOLIC AS raw_bp_systolic,
		Vital.DIASTOLIC AS raw_bp_diastolic,
		NULL AS raw_o2_stat,
		NULL AS raw_peak_flow,
		(SELECT
				CASE
					WHEN dx.DX_TYPE = '10' THEN 'icd10:' + dx.DX
					WHEN dx.DX_TYPE = '09' THEN 'icd9:' + dx.DX
					ELSE NULL
				END + ';'
			FROM REACHNET.DIAGNOSIS dx
			WHERE dx.ENCOUNTERID = Diagnosis.ENCOUNTERID
			FOR XML PATH('')) AS dx_code_id,
		m6.value_as_number AS raw_bmi,
		NULL AS hosp_admit_dt,
		to_char(v.visit_end_date,'yyyymmdd') AS hosp_dschrg_dt,
		Encounter.RAW_PAYER_NAME_PRIMARY AS primary_payer
FROM visit_occurrence v
join person p on p.person_id=v.persion_id
left join concept vcon on vcon.concept_id=v.visit_type_concept_id
left join (select m.visit_occurrence_id, value_as_number 
           from measurement m join concept c on c.concept_id=m.measurement_concept_id
           where c.concept_name ilike '%temperature%') m1 on v.visit_occurrence_id=m1.visit_occurrence_id -- and v.person_id=m1.person_id (can we assume a visit id goes with a specific patient?) 
left join (select m.visit_occurrence_id, value_as_number 
           from measurement m join concept c on c.concept_id=m.measurement_concept_id
           where c.concept_name = 'Body weight') m2 on v.visit_occurrence_id=m1.visit_occurrence_id -- and v.person_id=m1.person_id (can we assume a visit id goes with a specific patient?) 
left join (select m.visit_occurrence_id, value_as_number 
           from measurement m join concept c on c.concept_id=m.measurement_concept_id
           where c.concept_name = 'Height') m3 on v.visit_occurrence_id=m1.visit_occurrence_id -- and v.person_id=m1.person_id (can we assume a visit id goes with a specific patient?) 
left join (select m.visit_occurrence_id, value_as_number 
           from measurement m join concept c on c.concept_id=m.measurement_concept_id
           where c.concept_name like 'Systolic blood pressure%') m4 on v.visit_occurrence_id=m1.visit_occurrence_id -- and v.person_id=m1.person_id (can we assume a visit id goes with a specific patient?) 
left join (select m.visit_occurrence_id, value_as_number 
           from measurement m join concept c on c.concept_id=m.measurement_concept_id
           where c.concept_name like 'Diastolic blood pressure%') m5 on v.visit_occurrence_id=m1.visit_occurrence_id -- and v.person_id=m1.person_id (can we assume a visit id goes with a specific patient?) 
left join (select m.visit_occurrence_id, value_as_number 
           from measurement m join concept c on c.concept_id=m.measurement_concept_id
           where c.concept_name like 'Body mass index (BMI)%') m6 on v.visit_occurrence_id=m1.visit_occurrence_id -- and v.person_id=m1.person_id (can we assume a visit id goes with a specific patient?) 
WHERE v.visit_start_date < %s AND v.visit_start_date >= %S
"""  % (esp_gd.today, esp_gd.yesterday)

ESP_Visit_DICT= {'SELECT':SELECT_ESP_Visit_TABLE}


# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Immune_TABLE="""
select d.person_id, target.concept_code, target.concept_name, to_char(d.drug_exposure_start_date, 'yyyyMMdd'),
	null as Dose, 
	null as MFG_C, 
	null as MFG_Name, 
	null as Lot, 
	d.drug_exposure_id
from drug_exposure d 
join concept source on d.drug_concept_id=source.concept_id
join concept_ancestor anc on anc.descendant_concept_id = source.concept_id 
join concept target on target.concept_id=anc.ancestor_concept_id
where target.vocabulary_id='CVX'
and d.drug_exposure_start_date < %s AND d.drug_exposure_start_date >= %s
"""  % (esp_gd.today, esp_gd.yesterday)

ESP_Immune_DICT= {'SELECT':SELECT_ESP_Immune_TABLE}


SELECT_ESP_Social_Hx_TABLE="""
select o.person_id, null as MRN, c.concept_name, null as alcohol_use, 
 to_char(o.observation_date, 'yyyyMMdd') AS date_noted, o.observation_id 
from observation o join concept c on c.concept_id=o.observation_concept_id
where (c.concept_name ilike '%smoke%' or c.concept_name ilike '%tobacco%' or c.concept_name ilike '%cigarette%') 
and c.concept_id not in (4218741, 40756893, 43054909)
and o.observation_date < %s AND o.observation_date >= %s
""" % (esp_gd.today, esp_gd.yesterday)

ESP_Social_Hx_DICT= {'SELECT':SELECT_ESP_Social_Hx_TABLE}


# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Test_Results_TABLE="""
select m.person_id, null as mrn, null as order_natural_key, 
  to_char(m.measurement_date, 'yyyyMMdd') as ldate, 
  null as result_date,
  null as provider_id, null as order_type, null as cpt_native_code,
  c.concept_code, c.concept_name,
  case when qual.concept_name is null or qual.concept_name='No matching concept' then rtrim(to_char(m.value_as_number,'FM9999999999.9999'),'.')
       else qual.concept_name end value_string, null as abnormal_flag,
  m.range_low, m.range_high, unit.concept_name
from measurement m 
join concept c on c.concept_id=m.measurement_concept_id
left join concept qual on qual.concept_id=m.value_as_concept_id
left join concept unit on unit.concept_id=m.unit_concept_id
where c.vocabulary_id='LOINC'
and m.measurement_date < %s AND m.measurement_date >= %s
""" % (esp_gd.today, esp_gd.yesterday)

ESP_Test_Results_DICT= {'SELECT':SELECT_ESP_Test_Results_TABLE}


# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Meds_TABLE="""
select d.person_id, null as mrn, d.drug_exposure_id, null as provider_id,
  to_char(d.drug_exposure_start_date, 'yyyyMMdd') as ddate,
  null as status, d.sig, source.concept_code,
  source.concept_name, d.quantity, d.refills, 
  to_char(d.drug_exposure_start_date, 'yyyyMMdd') AS start_date, to_char(d.drug_exposure_end_date, 'yyyyMMdd') AS end_date,
  route.concept_name
from drug_exposure d 
join concept source on d.drug_concept_id=source.concept_id and source.vocabulary_id='RxNorm'
left join concept route on d.route_concept_id = route.concept_id
WHERE d.drug_exposure_start_date < %s AND d.drug_exposure_start_date >= %s
""" % (esp_gd.today, esp_gd.yesterday)

ESP_Meds_DICT= {'SELECT':SELECT_ESP_Meds_TABLE}


if __name__ == '__main__':
    pass
