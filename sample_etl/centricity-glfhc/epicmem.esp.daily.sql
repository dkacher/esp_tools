
SET NOCOUNT ON

DECLARE @StartDt DATE
DECLARE @EndDt DATE

SET @StartDt = getdate() - 5
SET @EndDt = getdate()


SELECT DISTINCT pp.PId 'Natural Key'
	  ,pp.PatientId 'MRN'
	  ,ISNULL(pp.Last,'') 'PatLastNm'
	  ,ISNULL(pp.First,'') 'PatFirstNm'
	  ,ISNULL(pp.Middle,'') 'PatMidNm'
	  ,ISNULL(pp.Address1,'') 'AddrLine1'
	  ,ISNULL (pp.Address2,'') 'AddrLine2'
	  ,ISNULL(pp.City,'') 'City'
	  ,ISNULL(pp.State,'') 'State'
	  -- Replace carriage returns in zip codes
      --,ISNULL(REPLACE(pp.Zip, char(13) + char(10), ''),'') 'Zip'
	  ,ISNULL (replace(replace(replace(pp.zip, CHAR(13), ''), CHAR(10), ''),char(9),''), '') 'Zip'
	  ,ISNULL (pp.Country,'') 'Country'
	  ,CASE WHEN pp.phone1 = '9999999999' THEN ''
			ELSE ISNULL(left(pp.phone1,3),'') 
	   END 'HomePhoneAreaCode'
	  ,CASE WHEN pp.phone1 = '9999999999' THEN ''
			ELSE ISNULL(substring(pp.phone1,4,3) + '-' + substring(pp.phone1,7,4),'') 
	   END 'HomePhoneNbr'
	  ,'' 'Extension'
	  ,ISNULL(CONVERT(VARCHAR(8), pp.birthdate, 112),'') 'Encounter_Date'
	  ,ISNULL(pp.Sex,'') 'Gender'
	  ,ISNULL(re.Race,'') 'Race'
	  ,ISNULL(re.PatientPrefLanguage,'') 'Home Language'
	  ,ISNULL(substring(pp.ssn,1,3) + '-' + substring(pp.ssn,4,2) + '-' + substring(pp.ssn,6,4),'') 'SSN'
	  ,ISNULL(pp.DoctorId,'') 'PCP_ID'
	  ,ISNULL (ml.Description,'') 'MaritalStatus'
	  ,'' 'Religion'
	  ,ISNULL(pp.Nickname,'') 'Aliases'
	  ,'' 'MotherMRN'
	  ,ISNULL(CONVERT(VARCHAR(8), pp.DeathDate, 112),'') 'DeathDate'
	  ,ISNULL(pp.FacilityId,'') as 'CenterID'
	  ,CASE WHEN re.UDSEthnicityCode = 'H' THEN 'Hispanic'
            WHEN re.UDSEthnicityCode = 'N' THEN 'Non Hispanic'
            WHEN re.UDSEthnicityCode = 'NR' THEN 'Not Reported'
			ELSE ''
       END 'Ethnicity'
	  ,ISNULL(pp.MothersMaiden, '')  'MotherMaidenNm'
	  ,ISNULL(CONVERT(VARCHAR(8), pp.lastmodified, 112),'') 'LastUpdtDt'
      ,ISNULL(pp.FacilityId,'') 'LastUpdtSiteId'
	  ,ISNULL(pp.Suffix,'') 'NameSuffix'
	  --,ISNULL(pp.Prefix,'') 'Title'
	  ,'' 'Title'
	  ,'' 'remark'
	  ,'' 'Income Level'
	  ,'' 'HousingStatus'
	  ,'' 'InsuranceStatus'
	  ,'' 'BirthCountry'

 FROM centricityps.dbo.PatientProfile pp
 LEFT JOIN centricityps.dbo.Appointments a ON pp.PatientProfileId = a.OwnerId
        AND a.ApptStart <= @EndDt AND a.ApptStart >= @StartDt
        AND a.Canceled is null
        --AND a.ApptStatusMId = 314
        AND a.ApptStart = (SELECT MAX(b.ApptStart)
                            FROM centricityps.dbo.Appointments b
                            WHERE a.OwnerId = b.OwnerId
           		            AND b.ApptStart <= @EndDt AND b.ApptStart >= @StartDt
                            AND b.Canceled is null
                            --AND b.ApptStatusMId = 314  -- 'Checked Out'
                            GROUP BY b.OwnerId)
 LEFT JOIN centricityps.dbo.cusUDSPatientRaceEthnicity_2017 re ON pp.PatientProfileId = re.PatientProfileID
 LEFT OUTER JOIN centricityps.dbo.MedLists ml ON pp.MaritalStatusMId = ml.MedListsId
 LEFT JOIN centricityps.dbo.obs obs ON pp.PId = obs.PID AND obs.obsdate BETWEEN @StartDt AND @EndDt
 LEFT JOIN centricityps.dbo.prescrib pre ON pp.pid = pre.pid AND pre.clinicaldate BETWEEN @StartDt AND @EndDt
 WHERE pp.PatientId NOT LIKE '9999%'
 -- bad patient records to be excluded
 AND pp.PId NOT IN ('1765191591195540')
 
 
 


