/* Fenway notes to GLFHC: Fenway pulls test data FROM the OBS table where results are entered. Information ON the type of test comes FROM the test name.
** DepENDing how GLFHC's system is SET up, you may have more data available ON the lab order, rather than just the result. Comments IN the code indicate certain limitations to this code's approach.
** No data is reported here for the last 13 columns. These default to NULL IN ESP.*/

SET NOCOUNT ON

DECLARE @StartDt DATE
DECLARE @EndDt DATE
--SET @StartDt = DATEADD(m,-24,GETDATE())
SET @StartDt = DATEADD(d,-3,GETDATE())
SET @EndDt = getdate()

--drop table #Results

SELECT pp.pid 'PatientId'
	  ,pp.patientid 'MRN'
	  ,obs.obsid 'LabOrderId'
	  /*The obsdate represents the date ON which the specimen was collected, usually but not always the day the test was ordered*/
	  ,ISNULL(CONVERT(VARCHAR(8), obs.obsdate, 112),'') 'OrderDt'
	  /*Since results are most often input into the obs table automatically, the db_updated_date usually represents the day Fenway received the results*/
	  ,ISNULL(CONVERT(VARCHAR(8), obs.db_updated_date, 112),'') 'ResultDt'
	  ,ISNULL(CAST(u.doctorfacilityid AS VARCHAR),'') 'OrderProvId'
	  ,null 'OrderType'
	  ,ISNULL(CAST(oh.hdid AS VARCHAR),'') 'MasterCode'
	  ,ISNULL(oh.MLCODE,'') 'ComponentTestCd' /*LOINC AND MLI codes*/
	  ,ISNULL(oh.description,'') 'TestName'
	  --,ISNULL(d.Summary,'') 'DocTestName'
	  --,CASE WHEN oh.HDID = -1 THEN d.Summary
			--ELSE oh.description
	  -- END 'NewTestName'
	  --,ISNULL(obs.obsvalue,'') 'TestResults'
	  -- Replace carriage returns in test results with spaces
	  ,ISNULL(REPLACE(obs.obsvalue, char(13) + char(10), ''),'') 'TestResults'
	  ,ISNULL(obs.Abnormal,'') 'Abnormal'
	  ,CASE WHEN SUBSTRING(obs.range,1,1) = '<' THEN ''
			WHEN SUBSTRING(obs.range,1,1) = '>' THEN obs.range
			WHEN obs.range LIKE '%-%' THEN SUBSTRING(range, 1, CHARINDEX('-', range)-1)
			WHEN obs.range IS NULL THEN ''
			ELSE obs.range
	   END 'RefLowValue'
	  ,CASE WHEN SUBSTRING(obs.range,1,1) = '>' THEN ''
			WHEN SUBSTRING(obs.range,1,1) = '<' THEN obs.range
			WHEN obs.range LIKE '%-%' THEN SUBSTRING(range, CHARINDEX('-', range)+1,LEN(range))
			WHEN obs.range IS NULL THEN ''
			ELSE ''
	   END 'RefHighValue'
	  ,ISNULL(oh.unit,'') 'Unit'
	  ,CASE WHEN obs.state is NULL 
				OR obs.state = '' THEN 'F'
			ELSE obs.state 
	   END 'ResultStatus'
	  ,ISNULL(obs.description,'') 'ResultComments'
	  ,'' 'SpecimenId'
	  ,'' 'Impression'
	  ,CASE WHEN oh.MLCODE IN ('MLI-14722','LOI-0691-6' ) THEN 'Urine' /*Chlamydia AND Gonorrhoeae urine*/
			WHEN oh.MLCODE IN ('MLI-314329', 'MLI-359460') THEN 'Throat' /*Chlamydia AND Gonorrhoeae throat*/
			WHEN oh.MLCODE IN ('MLI-117631', 'MLI-359459') THEN 'Rectum'/*Chlamydia, Gonorrhoeae rectum*/
			WHEN oh.MLCODE IN ('LOI-7918-6', 'MLI-144815', 'MLI-498.244', 'MLI-90469','MLI-70594') THEN 'Blood' /*HIVand Syphillis*/
			WHEN oh.MLCODE IN ('LOI-5193-8', 'LOI-5195-3') THEN 'Blood' /*Hep B*/
			WHEN oh.MLCODE IN ('LOI-5199-5', 'MLI-203162', 'MLI-4314') THEN 'Blood' /*Hep C*/
			WHEN oh.MLCODE IN ('MLI-7010') THEN 'Skin Test'
			WHEN oh.MLCODE IN ('MLI-226928') THEN 'Blood'
			WHEN oh.MLCODE IS NULL THEN ''
			ELSE oh.MLCODE 
	   END 'SpecimenSource'
	  ,ISNULL(CONVERT(VARCHAR(8), obs.db_updated_date, 112),'') 'StatusChgDt'
	  ,'' 'InterpreterId'
	  ,'' 'InterpreterIdTyp'
	  ,'' 'InterpreterUID'
	  ,'' 'CLIAId'
	  ,'' 'ObsMethod'
  --INTO #Results
  FROM centricityps.dbo.patientprofile pp
  INNER JOIN centricityps.dbo.obs obs ON pp.PId = obs.PID
  LEFT JOIN centricityps.dbo.usr u ON obs.pubuser = u.pvid
  LEFT JOIN centricityps.dbo.obshead oh ON obs.hdid = oh.hdid
  JOIN centricityps.dbo.DOCUMENT d ON obs.SDID = d.DID
		AND d.DOCTYPE in (2,7,8,9,27,28,32,44,45,1631149916156120)
		--AND d.DOCTYPE in (32)

  --?? why pull by db_updated_date
 where obs.db_updated_date BETWEEN @StartDt AND @EndDt
   AND obs.xid = 1000000000000000000
   AND obs.change NOT IN (0,4,10,11,12) /*Excluded deleted data.*/
   /*Exclude recently updated entries for results over 45 days old*/
   AND obs.obsdate >= dateadd(dd,-30,@StartDt)
   AND pp.PatientStatusMId <> '-903'
   /*Exclude test patients*/            
   AND pp.PatientId NOT LIKE '9999%'
   AND obs.OBSVALUE NOT LIKE  'Pending'
   AND obs.HDID <> -1

--select  *
--  from #Results

   --AND ORDERID is not null
   --AND PatientStatusMId = '-900'
   --AND pp.DoctorId <> 569
 --and obs.STATE = 'F'
 --where doctype = '30'
   --AND (obs.hdid IN (2746,359459,359460) /*gonorrhea*/
   --    OR (obs.hdid IN (3399, 144815,5045, 90469,70594)) /*HIV AND Syphillis*/
   --    OR (obs.hdid IN (78,79))/*Hep B*/
	  -- OR (obs.hdid IN (2722,203162,4314))/*Hep c*/
	  -- OR (obs.hdid IN (7010)) /*TB skin*/
	  -- OR (obs.hdid IN (226928)) /*TB blood*/
	  -- OR (obs.hdid IN (117631,14722,314329))) /*chlamydia*/ 

--select distinct MasterCode -- range top 2000 * --
--	  ,TestName
--	  ,TestResults
--	  ,DocTestName
--	  ,NewTestName
 --WHERE SpecimenSource in ('MLI-14722','LOI-0691-6' )
 --where LabOrderId = 1841069359677310
 --where range not like '%-%'
 --  AND range IS NOT NULL
 --  AND range <> ''
 --where testname like '%linklogic%'
 --  --and PatientId = '1000012378000000'
 --  and testresults <> '(NOTE)'
 --where laborderid = 1841069359676760
 --where PatientId = '1000012378000000'
 --  and testname like '%linklogic%'
 ----  AND range NOT LIKE '%-%'
 --  AND range <> 'NEG'
 --  AND range <> 'NR'
 --  AND range <> 'RARE'
 --  AND range <> 'YEL'
 --  AND range <> 'CLER'
 --  AND range <> '0'
 --  AND substring(range,1,1) <> '>' 
 --  AND substring(range,1,1) <> '<' 
--   and ISNUMERIC(cast(substring(range,1,2) as numeric)) = 0
--   and substring(range,1,1) between '0' and '9'
--   and substring(range,1,1) not between '0' and '9'
--where MasterCode = -1


--SELECT  SUBSTRING(range, 1, CHARINDEX('-', range)-1) AS 'Low'
--	   ,SUBSTRING(range, CHARINDEX('-', range)+1,LEN(range)) AS 'High'
----	  ,SUBSTRING(range, CHARINDEX('_', range)+1, LEN(range)) AS 'High'
--  FROM #Results
-- WHERE range like '%-%' 
   --and range <> ''
   --and range is not null
   --and substring(range,1,1) between '0' and '9'

   