/* Fenway notes to GLFHC: Depending ON the circumstances of treatment, medications for GC/CT may be stored in one of three tables: OBS, Orders, or Medicate (tied to Prescrib).
** This file contains 3 logically separate queries Union-ed together to collate all relevant medications.
** This frequently duplicates a single medication, but DPH has indicated that they can deal with that ON their END.
** No data is reported here for the last 4 columns: route, dose, patient class, or patient status. These default to null in ESP.*/

SET NOCOUNT ON

DECLARE @StartDt date
DECLARE @EndDt date

SET @StartDt = '2016-01-01'
SET @EndDt = getdate()

/*Meds FROM Medicate*/
SELECT pp.pid 'PatientId'
	  ,pp.patientid 'MRN'
	  ,pre.ptid 'OrderId'
	  ,u.doctorfacilityid 'RXProviderId'
	  ,ISNULL(CONVERT(VARCHAR(8), pre.clinicaldate, 112),'') 'OrderDt'
	  ,'' 'OrderStatus'
	  ,REPLACE(REPLACE(med.instructions, CHAR(13), ''), CHAR(10), '') as 'MedDirections' /*Remove erroneous newline characters FROM prescription directions*/
	  ,med.ndcLabProd + med.ndcPackage 'NDC_Code'
	  ,med.description 'RxMedName'
	  ,pre.quantity 'RxQuantity'
	  ,pre.refills 'RxRefills'
	  ,ISNULL(CONVERT(VARCHAR(8), med.startdate, 112),'') 'RxStartDt'
	  ,CASE WHEN med.stopdate = '4700-12-31 00:00:00.000' THEN '' 
			ELSE ISNULL(CONVERT(VARCHAR(8), med.startdate, 112),'')
	   END 'RxENDDt'
	  ,med.Route
	  ,med.Dose

  FROM centricityps.dbo.patientprofile pp
  INNER JOIN centricityps.dbo.prescrib pre ON pp.pid = pre.pid
  INNER JOIN centricityps.dbo.medicate med ON med.pid = pre.pid AND med.mid = pre.mid
  LEFT JOIN centricityps.dbo.usr u ON u.pvid = pre.pvid
  LEFT JOIN centricityps.dbo.locreg lr ON u.homelocation = lr.locid

 WHERE pre.db_updated_date BETWEEN @StartDt AND @EndDt
   AND pre.change not in (0,4,10) /*Excluded deleted data.*/
   AND med.xid = 1000000000000000000
   AND PatientStatusMId = '-900'
   AND pp.DoctorId <> 569
   /*Exclude test patients*/            
   AND PatientId NOT LIKE '9999%'

