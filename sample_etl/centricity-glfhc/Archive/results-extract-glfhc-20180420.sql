/* Fenway notes to GLFHC: Fenway pulls test data FROM the OBS table where results are entered. Information ON the type of test comes FROM the test name.
** DepENDing how GLFHC's system is SET up, you may have more data available ON the lab order, rather than just the result. Comments IN the code indicate certain limitations to this code's approach.
** No data is reported here for the last 13 columns. These default to NULL IN ESP.*/

SET NOCOUNT ON

DECLARE @StartDt DATE
DECLARE @EndDt DATE
SET @StartDt = getdate() - 5
SET @EndDt = getdate()

SELECT pp.pid 'PatientId'
	  ,pp.patientid 'MRN'
	  ,obs.obsid 'LabOrderId'
	  /*The obsdate represents the date ON which the specimen was collected, usually but not always the day the test was ordered*/
	  ,ISNULL(CONVERT(VARCHAR(8), obs.obsdate, 112),'') 'OrderDt'
	  /*Since results are most often input into the obs table automatically, the db_updated_date usually represents the day Fenway received the results*/
	  ,ISNULL(CONVERT(VARCHAR(8), obs.db_updated_date, 112),'') 'ResultDt'
	  ,u.doctorfacilityid 'OrderProvId'
	  ,'Test' 'OrderType'
	  ,oh.hdid 'MasterCode'
	  ,oh.mlcode 'ComponentTestCd' /*LOINC AND MLI codes*/
	  ,oh.description 'TestName'
	  ,obs.obsvalue 'TestResults'
	  ,obs.Abnormal
	  ,NULL 'RefLowValue'
	  ,NULL 'RefHighValue'
	  ,oh.unit 'Unit'
	  ,CASE WHEN obs.state is NULL 
				OR obs.state = '' THEN 'F'
			ELSE obs.state 
	   END 'ResultStatus'
	  ,obs.description 'ResultComments'
	  ,NULL 'SpecimenId'
	  ,NULL 'Impression'
	  ,CASE WHEN oh.mlcode IN ('MLI-14722','LOI-0691-6' ) THEN 'Urine' /*Chlamydia AND Gonorrhoeae urine*/
			WHEN oh.mlcode IN ('MLI-314329', 'MLI-359460') THEN 'Throat' /*Chlamydia AND Gonorrhoeae throat*/
			WHEN oh.mlcode IN ('MLI-117631', 'MLI-359459') THEN 'Rectum'/*Chlamydia, Gonorrhoeae rectum*/
			WHEN oh.mlcode IN ('LOI-7918-6', 'MLI-144815', 'MLI-498.244', 'MLI-90469','MLI-70594') THEN 'Blood' /*HIVand Syphillis*/
			WHEN oh.mlcode IN ('LOI-5193-8', 'LOI-5195-3') THEN 'Blood' /*Hep B*/
			WHEN oh.mlcode IN ('LOI-5199-5', 'MLI-203162', 'MLI-4314') THEN 'Blood' /*Hep C*/
			WHEN oh.mlcode IN ('MLI-7010') THEN 'Skin Test'
			WHEN oh.mlcode IN ('MLI-226928') THEN 'Blood'
			ELSE 'unknown' 
	   END 'SpecimenSource'
	  ,ISNULL(CONVERT(VARCHAR(8), obs.obsdate, 112),'') 'ResultDt'
	  ,NULL 'InterpreterId'
	  ,NULL 'InterpreterIdTyp'
	  ,NULL 'InterpreterUID'
	  ,NULL 'CLIAId'
	  ,NULL 'ObsMethod'
  FROM centricityps.dbo.patientprofile pp
  INNER JOIN centricityps.dbo.obs obs ON pp.PId = obs.PID
  LEFT JOIN centricityps.dbo.usr u ON obs.pubuser = u.pvid
  LEFT JOIN centricityps.dbo.obshead oh ON obs.hdid = oh.hdid

 where obs.db_updated_date BETWEEN @StartDt AND @EndDt
   AND obs.xid = 1000000000000000000
   AND obs.change NOT IN (0,4,10,11,12) /*Excluded deleted data.*/
   --AND (obs.hdid IN (2746,359459,359460) /*gonorrhea*/
   --    OR (obs.hdid IN (3399, 144815,5045, 90469,70594)) /*HIV AND Syphillis*/
   --    OR (obs.hdid IN (78,79))/*Hep B*/
	  -- OR (obs.hdid IN (2722,203162,4314))/*Hep c*/
	  -- OR (obs.hdid IN (7010)) /*TB skin*/
	  -- OR (obs.hdid IN (226928)) /*TB blood*/
	  -- OR (obs.hdid IN (117631,14722,314329))) /*chlamydia*/ 
	   /*Exclude recently updated entries for results over 45 days old*/
   AND obs.obsdate >= dateadd(dd,-30,@StartDt)
   AND OBSVALUE NOT LIKE  'PENDing'
   AND PatientStatusMId = '-900'
   AND pp.DoctorId <> 569
   /*Exclude test patients*/            
   AND PatientId NOT LIKE '9999%'
 --and obs.STATE = 'F'
 --where doctype = '30'



   