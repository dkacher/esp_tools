DECLARE @StartDt date
DECLARE @EndDt date

SET @StartDt = getdate() - 5 --'2018-04-19' --
SET @EndDt = getdate() -- '2018-04-19' --

--DROP TABLE #PatVisit

SELECT DISTINCT 
	   pp.pid
	  ,pp.patientid
	  ,pp.PatientProfileId
	  ,a.appointmentsid
	  ,a.ApptStart
	  ,CONVERT(VARCHAR(8), a.apptstart, 112) AS 'Encounter_Date'
	  ,NULL 'Is_Closed'
	  ,NULL 'Date_Closed'
	  ,a.doctorid 'Provider_Id'
	  ,a.facilityid 'Dept_Id_Num'
	  ,df2.listname 'Dept_Name'
	  ,at.name 'Event_Type'
	  /*Concatenate all diagnoses FROM the visit into one field, separated by ;*/
	  ,CASE WHEN Diag.codetype = 1 THEN (
				substring((SELECT cast('; icd9:' + pv1.code + ' ' + 
							 /*Remove special characters FROM diagnosis descriptions*/ 
					    + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(pv1.description,'&',' '),';',' '),'^',' '),'<',' '),'>',' '),'''',' ')
							AS VARCHAR(1000))
							 FROM centricityps.dbo.patientvisitdiags pv1
							 WHERE (pv1.patientvisitid = a.patientvisitid)
							 FOR XML PATH ('')),3,1000000)
					 )
			WHEN Diag.codetype = 8 THEN (
				substring((SELECT cast('; icd10:' + pv2.code + ' ' + 
								/*Remove special characters FROM diagnosis descriptions*/ 
						+ REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(pv2.description,'&',' '),';',' '),'^',' '),'<',' '),'>',' '),'''',' ')
							AS VARCHAR(1000))
						FROM centricityps.dbo.patientvisitdiags pv2
						WHERE (pv2.patientvisitid = a.patientvisitid)
						FOR XML PATH ('')),3,1000000)
						) END 'ICDCodes'
	  ,a.PatientVisitId
  INTO #PatVisit
  FROM centricityps.dbo.appointments a
  LEFT JOIN centricityps.dbo.patientvisitdiags pvd ON a.patientvisitid = pvd.patientvisitid
  LEFT JOIN centricityps.dbo.patientprofile pp ON a.ownerid = pp.patientprofileid
  LEFT JOIN centricityps.dbo.doctorfacility df ON a.doctorid = df.doctorfacilityid
  LEFT JOIN centricityps.dbo.doctorfacility df2 ON a.facilityid = df2.doctorfacilityid
  LEFT JOIN centricityps.dbo.appttype at ON a.appttypeid = at.appttypeid
  LEFT JOIN centricityps.dbo.Diagnosis diag ON pvd.DiagnosisId = diag.DiagnosisId
 WHERE a.ApptStart BETWEEN @StartDt AND @EndDt
   AND a.apptstatusmid = 314 -- in (314, -920) /*Exclude canceled appointments*/
   AND a.Canceled IS NULL
   AND pp.DoctorId <> 569
   AND pp.patientid NOT LIKE '9999%'

--SELECT *
--  FROM #PatVisit
--order by PatientId

--DROP TABLE #PatTemp

SELECT DISTINCT 
	   pv.PID
	  ,pv.AppointmentsId
	  ,obs.ObsValue 'Temp'
	  ,CAST(obs.ObsDate AS DATE) 'TempDt'
  INTO #PatTemp
  FROM #PatVisit pv
  JOIN centricityps.dbo.OBS obs ON pv.PID = obs.PID
		AND obs.HDID = 2641
		AND CAST(pv.ApptStart AS DATE) = CAST(OBSDATE AS DATE)
ORDER BY pv.PID

--SELECT *
----SELECT count(*) 'TempCnt'
--  FROM #PatTemp
-- WHERE PID in (1000003265000000, 1000006205000000, 1000012889000000)
--order by PID

--DROP TABLE #PatWeight

SELECT DISTINCT 
	   pv.PID
	  ,pv.AppointmentsId
	  ,obs.ObsValue 'Weight'
	  ,CAST(obs.OBSDATE AS DATE) 'WeightDt'
  INTO #PatWeight
  FROM #PatVisit pv
  JOIN centricityps.dbo.OBS obs ON pv.PID = obs.PID
		AND obs.HDID = 61
		AND CAST(pv.ApptStart AS DATE) = CAST(OBSDATE AS DATE)
ORDER BY pv.PID

--SELECT *
----SELECT count(*) 'WghtCnt'
--  FROM #PatWeight
-- WHERE PID in (1000003265000000, 1000006205000000, 1000012889000000)
--order by PID

--DROP TABLE #PatHeight

SELECT DISTINCT 
	   pv.PID
	  ,pv.AppointmentsId
	  ,obs.ObsValue 'oHeight'
	  ,CASE WHEN ISNUMERIC(obs.obsvalue) = 1 
				AND obs.obsvalue <> '.' THEN CAST(CAST((CAST(replace(obs.OBSVALUE,'..','.') AS numeric) / 12) AS INTEGER) AS VARCHAR(10)) 
													+ ''' ' + CAST((cast(replace(obs.OBSVALUE,'..','.') AS numeric) % 12) AS VARCHAR(10)) + '"'
			ELSE ''
	   END 'Height'
	  ,CAST(obs.OBSDATE AS DATE) 'HeightDt'
  INTO #PatHeight
  FROM #PatVisit pv
  JOIN centricityps.dbo.OBS obs ON pv.PID = obs.PID
		AND obs.HDID = 55
		AND CAST(pv.ApptStart AS DATE) = CAST(OBSDATE AS DATE)
ORDER BY pv.PID

--SELECT *
----SELECT count(*) 'HghtCnt'
--  FROM #PatHeight
-- WHERE PID in (1000003265000000, 1000006205000000, 1000012889000000)
--order by PID

--DROP TABLE #PatBp

SELECT DISTINCT 
	   pv.PID
	  ,pv.AppointmentsId
	  ,syst.ObsValue 'BPSystolic'
	  ,dia.ObsValue 'BPDiastolic'
	  ,CAST(syst.OBSDATE AS DATE) 'BPDt'
  INTO #PatBp
  FROM #PatVisit pv
  JOIN centricityps.dbo.OBS syst ON pv.PID = syst.PID
		AND syst.HDID = 54
		AND CAST(pv.ApptStart AS DATE) = CAST(OBSDATE AS DATE)
  JOIN centricityps.dbo.OBS dia ON pv.PID = dia.PID
		AND dia.HDID = 53
		AND CAST(pv.ApptStart AS DATE) = CAST(dia.OBSDATE AS DATE)
		AND syst.OBSDATE = dia.OBSDATE
ORDER BY pv.PID

--SELECT *
----SELECT count(*) 'HghtCnt'
--  FROM #PatBp
-- WHERE PID in (1000003265000000, 1000006205000000, 1000012889000000)
--order by PID

--DROP TABLE #PatBmi

SELECT DISTINCT 
	   pv.PID
	  ,pv.AppointmentsId
	  ,obs.ObsValue 'BMI'
	  ,CAST(obs.OBSDATE AS DATE) 'BMIDt'
  INTO #PatBmi
  FROM #PatVisit pv
  JOIN centricityps.dbo.OBS obs ON pv.PID = obs.PID
		AND obs.HDID = 2788
		AND CAST(pv.ApptStart AS DATE) = CAST(OBSDATE AS DATE)
ORDER BY pv.PID

--SELECT *
----SELECT count(*) 'BmiCnt'
--  FROM #PatBmi
-- WHERE PID in (1000003265000000, 1000006205000000, 1000012889000000)
--order by PID

--DROP TABLE #CPTCode

SELECT pv.PID
	  ,pv.PatientProfileId
	  ,pv.AppointmentsId
	  ,prc.CPTCode
  INTO #CPTCode
  FROM #PatVisit pv
  JOIN centricityps.dbo.PatientVisitProcs pvp ON pv.PatientVisitId = pvp.PatientVisitId
			AND pvp.ListOrder = 1
  JOIN centricityps.dbo.Procedures prc ON pvp.proceduresid = prc.proceduresid
ORDER BY PId

--DROP TABLE #EDDFinal

SELECT obs.PID
	  ,obs.HDID
	  ,oh.NAME 'HDIDName'
	  ,CONVERT(VARCHAR(8), CAST(obs.OBSVALUE AS DATE), 112) 'EDDFinalDt'
  INTO #EDDFinal
  FROM centricityps.dbo.OBS obs
  JOIN centricityps.dbo.OBSHEAD oh ON obs.HDID = oh.HDID
 WHERE obs.HDID = 32754
   AND LEN(obs.OBSVALUE) = 10
   AND obs.OBSVALUE like '%/%/%'
   AND CAST(obs.OBSVALUE AS DATE) >= @EndDt
   AND obs.OBSDATE = (SELECT MAX(a.OBSDATE)
					    FROM centricityps.dbo.OBS a
					   WHERE obs.PID = a.PID
					     AND obs.HDID = 32754
						 AND LEN(obs.OBSVALUE) = 10
						 AND obs.OBSVALUE like '%/%/%'
						 AND CAST(obs.OBSVALUE AS DATE) >= @EndDt
					  GROUP BY PID)

--DROP TABLE #EDDbyLMP

SELECT obs.PID
	  ,obs.HDID
	  ,oh.NAME 'HDIDName'
	  ,CONVERT(VARCHAR(8), CAST(obs.OBSVALUE AS DATE), 112) 'EDDbyLMPDt'
  INTO #EDDbyLMP
  FROM centricityps.dbo.OBS obs
  JOIN centricityps.dbo.OBSHEAD oh ON obs.HDID = oh.HDID
 WHERE obs.HDID = 2756
   AND LEN(obs.OBSVALUE) = 10
   AND obs.OBSVALUE like '%/%/%'
   AND CAST(obs.OBSVALUE AS DATE) >= GETDATE()
   AND obs.OBSDATE = (SELECT MAX(a.OBSDATE)
					    FROM centricityps.dbo.OBS a
					   WHERE obs.PID = a.PID
					     AND obs.HDID = 2756
						 AND LEN(obs.OBSVALUE) = 10
						 AND obs.OBSVALUE like '%/%/%'
						 AND CAST(obs.OBSVALUE AS DATE) >= @EndDt
					  GROUP BY PID)

--DROP TABLE #EDDPreg

SELECT obs.PID
	  ,obs.HDID
	  ,oh.NAME 'HDIDName'
      ,obs.OBSVALUE 'PregInd'
  INTO #EDDPreg
  FROM centricityps.dbo.OBS obs
  JOIN centricityps.dbo.OBSHEAD oh ON obs.HDID = oh.HDID
 WHERE obs.HDID = 8231
   AND obs.OBSVALUE = 'YES'
   AND CAST(obs.OBSDATE AS DATE) >= @EndDt
   AND obs.OBSDATE = (SELECT MAX(a.OBSDATE)
					    FROM centricityps.dbo.OBS a
					   WHERE obs.PID = a.PID
					     AND obs.HDID = 8231
						 AND obs.OBSVALUE = 'YES'
					  GROUP BY PID)

SELECT DISTINCT 
	   pv.PID 'Patient_Id'
	  ,pv.PatientId 'MRN'
	  ,pv.AppointmentsId 'Natural_Key'
	  ,Encounter_Date 
	  ,'Yes' 'is_closed'
	  ,Encounter_Date 'Date_Closed'
	  ,pv.Provider_Id
	  ,pv.Dept_Id_Num
	  ,pv.Dept_Name
	  ,pv.Event_Type
	  ,'APPT' 'Appt'
	  ,CASE WHEN eddf.EDDFinalDt IS NOT NULL THEN eddf.EDDFinalDt
			WHEN eddl.EDDbyLMPDt IS NOT NULL THEN eddl.EDDbyLMPDt
			WHEN eddp.PregInd IS NOT NULL THEN eddp.PregInd
			ELSE ''
	   END 'EDD'
	  ,t.Temp
	  ,cpt.CPTCode
	  ,w.Weight
	  ,h.Height
	  ,bp.BpSystolic
	  ,bp.BpDiastolic
	  ,NULL 'O2_Stat'
	  ,NULL 'Peak_Flow'
	  ,pv.ICDCodes
	  ,bmi.BMI
  FROM #PatVisit pv
  LEFT JOIN #PatTemp t ON pv.PID = t.PID
			AND pv.AppointmentsId = t.AppointmentsId
  LEFT JOIN #PatWeight w ON pv.PID = w.PID
			AND pv.AppointmentsId = w.AppointmentsId
  LEFT JOIN #PatHeight h ON pv.PID = h.PID
			AND pv.AppointmentsId = h.AppointmentsId
  LEFT JOIN #PatBp bp ON pv.PID = bp.PID
			AND pv.AppointmentsId = bp.AppointmentsId
  LEFT JOIN #PatBmi bmi ON pv.PID = bmi.PID
			AND pv.AppointmentsId = bmi.AppointmentsId
  LEFT JOIN #CPTCode cpt ON pv.PID = cpt.PID
			AND pv.AppointmentsId = cpt.AppointmentsId
  LEFT JOIN #EDDFinal eddf ON pv.PID = eddf.PID
  LEFT JOIN #EDDbyLMP eddl ON pv.PID = eddl.PID
  LEFT JOIN #EDDPreg eddp ON pv.PID = eddp.PID
ORDER BY Patient_Id, encounter_date, natural_key

DROP TABLE #PatVisit
DROP TABLE #PatTemp
DROP TABLE #PatWeight
DROP TABLE #PatHeight
DROP TABLE #PatBp
DROP TABLE #PatBmi
DROP TABLE #CPTCode
DROP TABLE #EDDFinal
DROP TABLE #EDDbyLMP
DROP TABLE #EDDPreg
