#!/usr/bin/python

"""
Name: esp_etl.py

Purpose: ETL utilities to support ESP project.

Modifications:
    13-Apr-2010 Created.  JL
    06-Jul-2010 Changed ESP_HOME to /opt/esp because the environmental var isn't available in cron.  JL
    23-Jul-2010 Added optparse and restructured to all command line call of historic_data_load.  JL
    06-Sep-2010 Corrected var name error in historical load.  JL
    23-Sep-2011 Added ESP_SRC_DIR to abstract the location better.  I probably should use ESP.settings.  JL
    28-Sep-2011 Changed call to load_epic to use symlink production and the bin/esp command instead of manage.py.  JL
    28-Sep-2011 Changed the date format for the file stamps to use two digit format.  Newer code base is more strict.  JL
    03-Oct-2011 Added get_datestamp().  The DD format had to be transformed similar to the MM.  JL
    20-Jun-2012 Major changes for ESP version 3 spec.  JL
    09-Aug-2012 Incorporating latest SQL and fixed quoting problem by changing csv.register_dialect quote parameters.  JL
    21-Aug-2012 Deleted alter patient table dcl.  JL
    16-Oct 2012 Added command options.  JL
    28-Nov 2012 Removed putenv call to define port 1433 for FreeTDS and fixed description in this version of pymssql.  JL
"""

import os, sys, time, pymssql
import esp_sql_new as esp_sql, esp_gd
import csv
from subprocess import Popen, PIPE, STDOUT
import optparse

ESP_SRC_DIR=r'/srv/esp/prod/ESP'
ESPDATA=r'/srv/esp/data/epic/incoming/'

MSSQL_HOST="Host:1433"
MSSQL_USER="Insert Username Here"
MSSQL_PASSWORD="Insert Password Here"
MSSQL_DB_NAME="Insert Database Name Here"


def check_date(year,mon,day):
    """Check if date is real"""
    tup_date = (year, mon, day, 0, 0, 0, -1, -1, -1)
    try:
        date_in_secs = time.mktime(tup_date)
        date_from_secs = time.localtime(date_in_secs)
        if tup_date[:3] == date_from_secs[:3]:
            return True
        else:
            return False
    except:
        return False

def get_datestamp(t=time.gmtime()):
    """return datestamp in format MMDDYYYY.  Example: Oct 1,2011 is '10012011'. Default is today's date."""
    # List is used to convert the numeric mon (1-12) to a string based two digit month ('01'-'12')
    MON=['00','01','02','03','04','05','06','07','08','09','10','11','12']
    DD=['00','01','02','03','04','05','06','07','08','09', \
         '10','11','12','13','14','15','16','17','18','19', \
         '20','21','22','23','24','25','26','27','28','29','30','31']
    # Default is today's date
    return MON[t.tm_mon]+DD[t.tm_mday]+str(t.tm_year)

def describe(option, opt_str, value, parser):
    """Describe pymssql"""
    print value
    db = pymssql.connect(host=MSSQL_HOST, user=MSSQL_USER, password=MSSQL_PASSWORD, database=MSSQL_DB_NAME)
    cur = db.cursor()
    sql = 'select top 1 * from %s' % value
    cur.execute(sql)
    desc = cur.description
    for col in desc:
        print col
    cur.close()
    db.close()
    sys.exit()

def run_dcl(sql):
    """Run sql that doesn't return results"""
    db = pymssql.connect(host=MSSQL_HOST, user=MSSQL_USER, password=MSSQL_PASSWORD, database=MSSQL_DB_NAME)
    cur = db.cursor()
    cur.execute(sql)
    db.commit()

def query(sql):
    """Run sql query and return the result set"""
    db = pymssql.connect(host=MSSQL_HOST, user=MSSQL_USER, password=MSSQL_PASSWORD, database=MSSQL_DB_NAME)
    cur = db.cursor()
    cur.execute(sql)
    return cur.fetchall()

def truncate_initial_load_done():
    """Truncates the X_ESP_INITIAL_LOAD_DONE table."""
    initial_load_table = "clarity_production_temp.dbo.X_ESP_INITIAL_LOAD_DONE"
    print "Warning! this will permanently delete all data in %s." % initial_load_table
    # prompt yes no
    x = raw_input("Are you sure you want to truncate the table.  Enter yes or (n)o: ")
    if x.upper() == "YES":
        run_dcl("TRUNCATE TABLE %s" % initial_load_table)
        print "Truncated %s table." % initial_load_table
    else:
        print "Table was not truncated."
    

def create_esp_table(table,DROP=False):
    """Run series of sql commands to create table based on ESP table dictionary"""
    if DROP == True:
        try:
            print "Dropping %s table..." % table['TABLENAME']
            for dropsql in table['DROP']:
                run_dcl(dropsql)
            print "Deleted %s table." % table['TABLENAME']
        except:
            pass
    # Create the table
    print "Creating %s table..." % table['TABLENAME']
    run_dcl(table['CREATE'])
    # Update columns
    print "Altering %s table..." % table['TABLENAME']
    for updatesql in table['UPDATE']:
        run_dcl(updatesql)
    print "%s table created successfully." % table['TABLENAME']

def create_esp_tables():
    """Creates all ESP tables"""
    create_esp_table(esp_sql.ESP_Updated_Patid_DICT,DROP=True)
    create_esp_table(esp_sql.ESP_Patient_DICT,DROP=True)
    create_esp_table(esp_sql.ESP_Visit_DICT,DROP=True)
    create_esp_table(esp_sql.ESP_Immune_DICT,DROP=True)
    create_esp_table(esp_sql.ESP_Allergies_DICT,DROP=True)
    create_esp_table(esp_sql.ESP_Social_Hx_DICT,DROP=True)
    create_esp_table(esp_sql.ESP_Problem_List_DICT,DROP=True)
    create_esp_table(esp_sql.ESP_Provider_DICT,DROP=True)
    create_esp_table(esp_sql.ESP_Labs_Map_DICT,DROP=True)
    create_esp_table(esp_sql.ESP_Tests_DICT,DROP=True)
    create_esp_table(esp_sql.ESP_Test_Results_DICT,DROP=True)
    create_esp_table(esp_sql.ESP_Meds_DICT,DROP=True)
    #create_esp_table(esp_sql.ESP_Pregnancy_DICT,DROP=True)
    run_dcl(esp_sql.ALTER_ESP_Visit)
    # Assumes X_ESP_Initial_Load_Done was created manually
    run_dcl(esp_sql.UPDATE_ESP_Initial_Load_Done)

def create_esploadfile(fileandpath,someiterable):
    """Create esp member load file"""
    csv.register_dialect('epicout', delimiter='^', escapechar="\\", quotechar = '', doublequote = False, lineterminator = '\r\n', quoting=csv.QUOTE_NONE)
    writer = csv.writer(open(fileandpath, "w"), dialect='epicout')
    for outrow in someiterable:
        try:
            writer.writerow(outrow)
        except Exception as e:
           print str(e)

def create_esploadfiles(t=time.gmtime()):
    """Create all of the esp load files"""
    
    # Default is today's date
    datestamp = get_datestamp(t)

    # Create esp load files
    create_esploadfile(ESPDATA+r'epicmem.esp.'+datestamp, query (esp_sql.ESP_Patient_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicvis.esp.'+datestamp, query (esp_sql.ESP_Visit_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicimm.esp.'+datestamp, query (esp_sql.ESP_Immune_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicall.esp.'+datestamp, query (esp_sql.ESP_Allergies_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicsoc.esp.'+datestamp, query (esp_sql.ESP_Social_Hx_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicprb.esp.'+datestamp, query (esp_sql.ESP_Problem_List_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicpro.esp.'+datestamp, query (esp_sql.ESP_Provider_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicord.esp.'+datestamp, query (esp_sql.ESP_Tests_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicres.esp.'+datestamp, query (esp_sql.ESP_Test_Results_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicmed.esp.'+datestamp, query (esp_sql.ESP_Meds_DICT['SELECT']))
    #create_esploadfile(ESPDATA+r'epicprg.esp.'+datestamp, query (esp_sql.ESP_Pregnancy_DICT['SELECT']))

def run_esploads(t=time.gmtime()):
    """Run the epic load"""

    # Create the file date stamp
    dstamp = get_datestamp(t)
    # Set the current working directory to $ESP_HOME/src/ESP
    os.chdir(ESP_SRC_DIR)

    # Run each load
    p = Popen("/srv/esp/production/bin/esp load_epic --file=/srv/esp/data/incoming/epicmem.esp.%s" % dstamp, shell=True, stdout=PIPE, stderr=STDOUT) 
    stdout, stderr = p.communicate()
    open("/srv/esp/data/etl_loadlogs/loadmem.%s" % dstamp,'w').write(stdout)

    p = Popen("/srv/esp/production/bin/esp load_epic --file=/srv/esp/data/incoming/epicvis.esp.%s" % dstamp, shell=True, stdout=PIPE, stderr=STDOUT) 
    stdout, stderr = p.communicate()
    open("/srv/esp/data/etl_loadlogs/loadvis.%s" % dstamp,'w').write(stdout)

    p = Popen("/srv/esp/production/bin/esp load_epic --file=/srv/esp/data/incoming/epicpro.esp.%s" % dstamp, shell=True, stdout=PIPE, stderr=STDOUT) 
    stdout, stderr = p.communicate()
    open("/srv/esp/data/etl_loadlogs/loadpro.%s" % dstamp,'w').write(stdout)

    p = Popen("/srv/esp/production/bin/esp load_epic --file=/srv/esp/data/incoming/epicprb.esp.%s" % dstamp, shell=True, stdout=PIPE, stderr=STDOUT) 
    stdout, stderr = p.communicate()
    open("/srv/esp/data/etl_loadlogs/loadprb.%s" % dstamp,'w').write(stdout)

    p = Popen("/srv/esp/production/bin/esp load_epic --file=/srv/esp/data/incoming/epicmed.esp.%s" % dstamp, shell=True, stdout=PIPE, stderr=STDOUT) 
    stdout, stderr = p.communicate()
    open("/srv/esp/data/etl_loadlogs/loadmed.%s" % dstamp,'w').write(stdout)

    p = Popen("/srv/esp/production/bin/esp load_epic --file=/srv/esp/data/incoming/epicord.esp.%s" % dstamp, shell=True, stdout=PIPE, stderr=STDOUT) 
    stdout, stderr = p.communicate()
    open("/srv/esp/data/etl_loadlogs/loadord.%s" % dstamp,'w').write(stdout)

    p = Popen("/srv/esp/production/bin/esp load_epic --file=/srv/esp/data/incoming/epicres.esp.%s" % dstamp, shell=True, stdout=PIPE, stderr=STDOUT) 
    stdout, stderr = p.communicate()
    open("/srv/esp/data/etl_loadlogs/loadres.%s" % dstamp,'w').write(stdout)

    p = Popen("/srv/esp/production/bin/esp load_epic --file=/srv/esp/data/incoming/epicsoc.esp.%s" % dstamp, shell=True, stdout=PIPE, stderr=STDOUT) 
    stdout, stderr = p.communicate()
    open("/srv/esp/data/etl_loadlogs/loadsoc.%s" % dstamp,'w').write(stdout)

    p = Popen("/srv/esp/production/bin/esp load_epic --file=/srv/esp/data/incoming/epicimm.esp.%s" % dstamp, shell=True, stdout=PIPE, stderr=STDOUT) 
    stdout, stderr = p.communicate()
    open("/srv/esp/data/etl_loadlogs/loadimm.%s" % dstamp,'w').write(stdout)

    p = Popen("/srv/esp/production/bin/esp load_epic --file=/srv/esp/data/incoming/epicall.esp.%s" % dstamp, shell=True, stdout=PIPE, stderr=STDOUT) 
    stdout, stderr = p.communicate()
    open("/srv/esp/data/etl_loadlogs/loadall.%s" % dstamp,'w').write(stdout)

    #p = Popen("/srv/esp/production/bin/esp load_epic --file=/srv/esp/data/incoming/epicprg.esp.%s" % dstamp, shell=True, stdout=PIPE, stderr=STDOUT) 
    #stdout, stderr = p.communicate()
    #open("/srv/esp/data/etl_loadlogs/loadprg.%s" % dstamp,'w').write(stdout)


def historic_data_load(start_yr, start_mon, start_day, end_yr, end_mon, end_day):
    """Run historic data load with start < date < end"""


    # Set the current working directory to $ESP_HOME/src/ESP
    os.chdir(ESP_SRC_DIR)

    #
    # Original historical data load
    #
    # Start date (oldest date)
    #start = time.mktime((2009, 5, 9, 0, 0, 0, -1, -1, -1))
    # End date (more recent date)
    #end = time.mktime((2010, 6, 29, 0, 0, 0, -1, -1, -1))

    # Start date (oldest date)
    start = time.mktime((start_yr, start_mon, start_day, 0, 0, 0, -1, -1, -1))
    # End date (more recent date)
    end = time.mktime((end_yr, end_mon, end_day, 0, 0, 0, -1, -1, -1))


    # Set the dates
    t = time.gmtime(start)
    esp_gd.yesterday = "'"+str(t.tm_mon)+"/"+str(t.tm_mday)+"/"+str(t.tm_year)+"'"
    print "esp_gd.yesterday is: "+esp_gd.yesterday
    t = time.gmtime(end)
    esp_gd.today = "'"+str(t.tm_mon)+"/"+str(t.tm_mday)+"/"+str(t.tm_year)+"'"
    print "esp_gd.today is: "+esp_gd.today
    # Reload to change SQL.  This will update the SQL with the new dates.
    reload(esp_sql)
    # Create the tables
    create_esp_tables()
    # Create the loadfiles with the end date.
    create_esploadfiles(time.gmtime(end))
    # Run the loads with the end date or esp_gd.today which happens to be start at this point.
    #run_esploads(time.gmtime(start))


def historic_data_load_1dayatatime(start_yr, start_mon, start_day, end_yr, end_mon, end_day):
    """Run historic data load with start < date < end"""


    # Set the current working directory to $ESP_HOME/src/ESP
    os.chdir(ESP_SRC_DIR)

    #
    # Original historical data load
    #
    # Start date (oldest date)
    #start = time.mktime((2009, 5, 9, 0, 0, 0, -1, -1, -1))
    # End date (more recent date)
    #end = time.mktime((2010, 6, 29, 0, 0, 0, -1, -1, -1))

    # Start date (oldest date)
    start = time.mktime((start_yr, start_mon, start_day, 0, 0, 0, -1, -1, -1))
    # End date (more recent date)
    end = time.mktime((end_yr, end_mon, end_day, 0, 0, 0, -1, -1, -1))

    # Loop until end date is reached
    while start < end:
	# Set the dates
	t = time.gmtime(start)
        esp_gd.yesterday = "'"+str(t.tm_mon)+"/"+str(t.tm_mday)+"/"+str(t.tm_year)+"'"
        print "esp_gd.yesterday is: "+esp_gd.yesterday
        start = start+(60*60*24)
	t = time.gmtime(start)
        esp_gd.today = "'"+str(t.tm_mon)+"/"+str(t.tm_mday)+"/"+str(t.tm_year)+"'"
        print "esp_gd.today is: "+esp_gd.today
        # Reload to change SQL.  This will update the SQL with the new dates.
        reload(esp_sql)
        # Create the tables
        create_esp_tables()
        # Create the loadfiles with the end date or esp_gd.today which happens to be start at this point.
        create_esploadfiles(time.gmtime(start))
        # Run the loads with the end date or esp_gd.today which happens to be start at this point.
        #run_esploads(time.gmtime(start))

def run_daily_load():
    """Run the daily data load"""
    create_esp_tables()
    create_esploadfiles()
    # The ESP batch command in cron will also load the files.  I've decided to load them here as well.
    #run_esploads()

def main():

    # Some examples:
    #create_esp_table(esp_sql.ESP_Patient_DICT,DROP=True)
    #run_dcl(esp_sql.ALTER_ESP_Patient)
    #print query("select count(*) from X_ESP_Patient")
    #describe('PAT_ENC')

    parser = optparse.OptionParser()
    parser.add_option("-d", "--describe", dest="tablename", type="string",
                  action="callback", callback=describe,
                  help="describe MS SQL <TABLENAME>")
    parser.add_option("-t", "--truncate_initial_load_table", action="store_true",dest="truncate_flag",
                  help="Warning! This command deletes all data in the X_ESP_INITIAL_LOAD_DONE table.")
    group = optparse.OptionGroup(parser, "Historic Data Load Options")
    group.add_option("--historic_load", action="store_true", dest="historic_data_load_flag",
                  help="Perform ESP historic load for date range.  Requires START and END dates.")
    group.add_option("-e", "--end_date", action="store", dest="end", type="string",
                  help="Format: MM/DD/YYYY, <END> date parameter for historic load")
    group.add_option("-s", "--start_date", action="store", dest="start", type="string",
                  help="Format: MM/DD/YYYY. <START> date parameter for historic load")
    group.add_option("-o", "--one_file_per_day", action="store_true", dest="one_file_flag",
                  help="Default is large file.  This option creates etl files for each day")
    parser.add_option_group(group)
    (opts, args) = parser.parse_args()

    if parser.values.truncate_flag:
        truncate_initial_load_done()
        sys.exit()
    elif parser.values.historic_data_load_flag:
        if parser.values.end and parser.values.start:
            start_yyyy,start_dd,start_mm = int(parser.values.start.split('/')[2]), \
                                           int(parser.values.start.split('/')[1]), \
                                           int(parser.values.start.split('/')[0])
            end_yyyy,end_dd,end_mm = int(parser.values.end.split('/')[2]), \
                                     int(parser.values.end.split('/')[1]), \
                                     int(parser.values.end.split('/')[0])
            start_tup = (start_yyyy, start_mm, start_dd,0, 0, 0, -1, -1, -1)
            end_tup = (end_yyyy, end_mm, end_dd,0, 0, 0, -1, -1, -1)
            # Check if valid dates
            if check_date(start_yyyy,start_mm,start_dd) != True:
                raise parser.error("Invalid START date")
            if check_date(end_yyyy,end_mm,end_dd) != True:
                raise parser.error("Invalid END date")
            # Check start_date < end_date
            if time.mktime(start_tup) > time.mktime(end_tup):
                #raise optparse.OptionValueError("START date isn't older than END date.") 
                raise parser.error("START date isn't older than END date.") 
            # Call historic load
            if parser.values.one_file_flag:
                 historic_data_load_1dayatatime(start_yyyy, start_mm, start_dd, end_yyyy, end_mm, end_dd)
            else:
                 historic_data_load(start_yyyy, start_mm, start_dd, end_yyyy, end_mm, end_dd)
            sys.exit()
        else:
           #raise optparse.OptionValueError("START and END dates required for historic load.") 
           raise parser.error("START and END dates required for historic load.") 

    # Default is to run ETL daily load
    run_daily_load()
    sys.exit()

if __name__ == '__main__':
    main()
