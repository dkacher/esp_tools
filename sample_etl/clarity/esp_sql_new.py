#!/usr/bin/python
"""
Name: esp_sql.py

Purpose:  Container for SQL commands.

Modifications:
    14-Apr-2010 Created.  JL
    05-Aug-2010 Change Allergy table sql because of Epic 2009 upgrade changes in Clarity.  JL
    10-Nov-2010 Eliminated date where clause in CREATE_ESP_Test_Results_TABLE sql.  JL
    29-Nov-2010 Added unions to Patient table find patients with updates.  JL
    13-Jan-2011 Fixed UPDATE_ESP_Initial_Load_Done SQL.  JL
    20-Jan-2011 Still trying to get the SQL code right for tests and test_results.  JL
    14-Apr-2011 Modified the test results SQL to provide more detailed description.  JL
    03-Jun-2011 Major SQL change to test and test results SQL.  JL
    28-Dec-2011 Change create SQL for problem_list, allergy, and immune.  Clarity SQL is 
                performing full nightly loads and update_date is always the date the 
                ETL is run on.  JL
    20-Jun-2012 Major changes for ESP version 3 spec.  JL
    02-Aug-2012 Major changes for ESP version 3 spec(7/18) MW
    08-Aug-2012 cast statements were truncated data.  I explicitly specified varchar(255) to correct the problem.  JL
    21-Aug-2012 Major correction and cleanup of SQL.  JL
    28-Aug-2012 cast Null statements were wrong in several select statements.  JL
    10-Oct-2012 Cast as text used as a workaround for known pymssql varchar limitation/truncation.  JL
    26-Feb-2013 Modified select statements per changes specified by M Weiss.  RJZ
    10-Mar-2014 Modified select statements per changes specified by M Weiss.  RJZ
"""

import esp_gd

DROP_TABLES="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Updated_Patid
DROP TABLE clarity_production_temp.dbo.X_ESP_Patient
DROP TABLE clarity_production_temp.dbo.X_ESP_Visit
DROP TABLE clarity_production_temp.dbo.X_ESP_Immune
DROP TABLE clarity_production_temp.dbo.X_ESP_Allergies
DROP TABLE clarity_production_temp.dbo.X_ESP_Social_Hx
DROP TABLE clarity_production_temp.dbo.X_ESP_Problem_List
DROP TABLE clarity_production_temp.dbo.X_ESP_Provider
DROP TABLE clarity_production_temp.dbo.X_ESP_Labs_Map
DROP TABLE clarity_production_temp.dbo.X_ESP_Tests
DROP TABLE clarity_production_temp.dbo.X_ESP_Test_Results 
DROP TABLE clarity_production_temp.dbo.X_ESP_Meds
DROP TABLE clarity_production_temp.dbo.X_ESP_Pregnancy_Temp
DROP TABLE clarity_production_temp.dbo.X_ESP_Pregnancy
"""

DROP_ESP_Updated_Patid_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Updated_Patid
"""

CREATE_ESP_Updated_Patid_TABLE="""
SELECT Pat_ID INTO clarity_production_temp.dbo.X_ESP_Updated_Patid from PATIENT
WHERE UPDATE_DATE < %s and UPDATE_DATE >= %s
UNION
SELECT Pat_ID from PAT_ENC
WHERE CONTACT_DATE < %s and CONTACT_DATE >= %s
UNION
SELECT Pat_ID from ALLERGY
WHERE UPDATE_DATE < %s and UPDATE_DATE >= %s
UNION
SELECT Pat_ID from IMMUNE
WHERE UPDATE_DATE < %s and UPDATE_DATE >= %s
UNION
SELECT Pat_ID from PROBLEM_LIST
WHERE UPDATE_DATE < %s and UPDATE_DATE >= %s
UNION
SELECT Pat_ID from ORDER_PROC
WHERE UPDATE_DATE < %s and UPDATE_DATE >= %s
UNION
SELECT Pat_ID from ORDER_MED
WHERE UPDATE_DATE < %s and UPDATE_DATE >= %s		
""" % (esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.twodaysago \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday)

SELECT_ESP_Updated_Patid_TABLE="""
SELECT  *
FROM clarity_production_temp.dbo.X_ESP_Updated_Patid
"""

ESP_Updated_Patid_DICT= {'TABLENAME':"X_ESP_Updated_Patid", \
                   'DROP':[DROP_ESP_Updated_Patid_TABLE,], \
                   'CREATE':CREATE_ESP_Updated_Patid_TABLE, \
                   'UPDATE':[], \
                   'SELECT':SELECT_ESP_Updated_Patid_TABLE}



DROP_ESP_Patient_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Patient
"""

CREATE_ESP_Patient_TABLE="""
SELECT  patient.Pat_ID, 
	PATIENT.Pat_MRN_ID, 
	PATIENT.Pat_Last_Name,
	PATIENT.Pat_First_Name,
	PATIENT.Pat_Middle_Name,
	PATIENT.Add_Line_1,
	PATIENT.Add_Line_2,
	PATIENT.City,
	zc_State.ABBR as State, 
	PATIENT.Zip, 
	ZC_Country.TITLE as Country_name,
	SUBSTRING(PATIENT.Home_Phone,1,3) as HOME_AREA,
	SUBSTRING(PATIENT.Home_Phone,5,9) as HOME_PHONE ,
	cast (Null as varchar) as Tel_ext, 
	CONVERT (nvarchar(30), PATIENT.Birth_Date , 112  ) as BIRTH_DATE,
	X_ZC_PATIENT_SEX.TITLE as Gender, 
	ZC_PATIENT_RACE.TITLE as RACE,
	ZC_Language.TITLE as Home_Language, 
	PATIENT.SSN,
	PATIENT.Cur_PCP_Prov_ID, 
	ZC_MARITAL_STATUS.TITLE as Marital_status,
	ZC_Religion.TITLE as Religion,
	pata.alias_list,
	MOM_patient.PAT_MRN_ID as Mother_MRN, 
	PATIENT.Death_Date,
	CAST ( case 
                 when PATIENT.Pat_id in (Select pat_id from Patient
                                            where Pat_MRN_id in   (SELECT DISTINCT UNITNO as PAT_MRN_ID
	                                                           FROM [datamart].[report_db].[dbo].[Portal_PCPPanelDetails]
	                                                           where ISNULL(PCPr,'')<>'') 
                                           )  then PATIENT.CUR_PRIM_LOC_ID 
                 ELSE null 
               END as varchar) as CenterID,
        case 
            when xpc.X_HISP_INDICATOR_YN='Y' then 'HISPANIC'
            when xpc.X_HISP_INDICATOR_YN='N' then 'NON HISPANIC'
            else 'NOT REPORTED' 
        end  as ethnicity
INTO clarity_production_temp.dbo.X_ESP_Patient
FROM PATIENT 
	left join X_ZC_PATIENT_SEX on patient.SEX_C=X_ZC_patient_SEX.PATIENT_SEX_C
	LEFT join ZC_Language  on Patient.Language_C = ZC_Language.Language_C
	left join ZC_MARITAL_STATUS on Patient.MARITAL_STATUS_C=ZC_MARITAL_STATUS.MARITAL_STATUS_C
	left join ZC_Religion on patient.RELIGION_C=ZC_Religion.RELIGION_C
	left join ZC_Ethnic_Group on patient.ETHNIC_GROUP_C=ZC_Ethnic_Group.ETHNIC_GROUP_C
	left join ZC_Country on Patient.COUNTRY_C=ZC_Country.COUNTRY_C
	left join ZC_STATE on patient.STATE_C=zc_state.STATE_C
	left join PATIENT as MOM_PATIENT on patient.MOTHER_PAT_ID=MOM_PATIENT.PAT_ID
        left join X_PATIENT_CUSTOM as xpc on patient.pat_id=xpc.pat_id
	Left Join (Select MAX(line) as LINE, pat_id 
		From Patient_race 
		Group by Pat_id )  as T on Patient.PAT_ID =T.PAT_ID
	Left join PATIENT_RACE on T.PAT_ID = Patient_race.PAT_ID and T.LINE=Patient_Race.LINE
	left join ZC_PATIENT_RACE on patient_race.PATIENT_RACE_C=ZC_PATIENT_RACE.PATIENT_RACE_C
	left outer join (select t1.Pat_ID, 
                   		(SELECT ALIAS + ' | ' 
                     	FROM  
                           	(SELECT Patient.PAT_ID, Patient_alias.ALIAS as ALIAS
  				FROM Clarity.dbo.Patient , PATIENT_ALIAS
 				WHERE Patient.PAT_ID=Patient_alias.PAT_ID)  as t2 
			WHERE t2.Pat_ID=t1.Pat_ID  
			ORDER BY ALIAS 
			FOR XML PATH('') ) as alias_list  
                FROM Patient as t1 
                GROUP BY Pat_id) pata on patient.Pat_id = pata.Pat_id 
WHERE PATIENT.Pat_ID in (SELECT Pat_ID FROM clarity_production_temp.dbo.X_ESP_Updated_Patid)
"""

SELECT_ESP_Patient_TABLE="""
SELECT  Pat_ID, 
        Pat_MRN_ID, 
        Pat_Last_Name,
        Pat_First_Name,
        Pat_Middle_Name,
        Add_Line_1,
        Add_Line_2,
        City,
        State, 
        Zip, 
        Country_name,
        HOME_AREA,
        HOME_PHONE ,
        Tel_ext, 
        BIRTH_DATE,
        Gender, 
        RACE,
        Home_Language, 
        SSN,
        Cur_PCP_Prov_ID, 
        Marital_status,
        Religion,
	CAST (alias_list as text) as alias_list,
        Mother_MRN, 
        Death_Date,
        CENTERID,
        ethnicity
FROM clarity_production_temp.dbo.X_ESP_Patient 
"""

ESP_Patient_DICT= {'TABLENAME':"X_ESP_Patient", \
                   'DROP':[DROP_ESP_Patient_TABLE,], \
                   'CREATE':CREATE_ESP_Patient_TABLE, \
                   'UPDATE':[], \
                   'SELECT':SELECT_ESP_Patient_TABLE}


DROP_ESP_Visit_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Visit
"""

CREATE_ESP_Visit_TABLE="""
SELECT DISTINCT enc.Pat_ID,
        pat.Pat_MRN_ID,
        enc.Pat_Enc_CSN_ID,
        enc.Contact_Date,
        enc.Enc_Closed_YN,
        enc.Enc_Close_Date,
        Visit_Prov_ID,
        enc.Department_ID,
        cast (Null as varchar(255)) as Department_Name,
        Enc_Type_C,
        CASE
          WHEN hsp.IP_EPISODE_ID is null THEN CONCAT('O-', enc_type.title)
          WHEN hsp.IP_EPISODE_ID is not null THEN CONCAT('I-', enc_type.title)
          ELSE CONCAT('U-', enc_type.title)
        END Enc_Type_Name,  /*Flag for inpatient outpatient visit combined with encounter type*/
        pat.EDD_DT,
        Temperature,
        Los_Proc_Code,
        Left(Cast((CONVERT(NUMERIC(8,2), (weight*.0625))) as varchar ), 3) +' lb' + ' ' +  cast(convert(numeric(3,2),right( (CONVERT(NUMERIC(8,2), (weight*.0625))), 3)) * 16 as varchar )+  ' oz'  AS WEIGHT,
	LEFT(height,(CHARINDEX('"',HEIGHT))) as Height,
        BP_Systolic,
        BP_Diastolic,
        enc2.PHYS_SPO2 as O2_Stat,
        enc.CHA_PEAK_FLOW as peak_flow,
        dx.dx_list as ICD9,
        bmi,
        enc.HOSP_ADMSN_TIME,
        enc.HOSP_DISCHRG_TIME
INTO clarity_production_temp.dbo.X_ESP_Visit	
FROM PAT_ENC enc 
left join PATIENT pat on enc.Pat_ID = pat.Pat_ID  
left join PAT_ENC_2 enc2 on enc.Pat_enc_csn_id = enc2.Pat_enc_csn_id
left join clarity.dbo.pat_enc_hsp hsp on enc.pat_enc_csn_id=hsp.pat_enc_csn_id  
left join ZC_DISP_ENC_TYPE enc_type on enc.enc_type_c = enc_type.internal_id
left outer join (
                 select t1.pat_enc_csn_id,
                    (select codwthtxt + '; '
                     FROM
                           ( select dx.pat_enc_csn_id,
                             case when edg10.code is not null then 'icd10:' + edg10.code
                                  when edg9.code is not null then 'icd9:' + edg9.code
                                  else null
                                  end AS codwthtxt
                             FROM PAT_ENC_DX dx
                             left join EDG_CURRENT_ICD10 edg10 on edg10.dx_id = dx.dx_id
                             left join EDG_CURRENT_ICD9 edg9 on edg9.dx_id = dx.dx_id
                                )  as t2
                     WHERE t2.pat_enc_csn_id=t1.pat_enc_csn_id
                     GROUP BY codwthtxt
                     ORDER BY codwthtxt
                     FOR XML PATH('') ) as dx_list
                FROM PAT_ENC_DX as t1
                GROUP BY pat_enc_csn_id
                ) dx  on dx.Pat_enc_csn_id = enc.Pat_enc_csn_id
WHERE (enc.Contact_Date < %s and enc.Contact_Date >= %s) 
OR 
-- only process updates for visits that happened in the last six months
( enc.Update_Date >= %s and enc.Contact_Date < %s and enc.Contact_Date >= DATEADD(month,-6,getdate()) )
"""  % (esp_gd.today, esp_gd.yesterday, esp_gd.yesterday, esp_gd.today)

UPDATE_ESP_Visit_Department_Name="""
UPDATE clarity_production_temp.dbo.X_ESP_Visit	
SET Department_Name = dic.Department_Name
FROM clarity_production_temp.dbo.X_ESP_visit esp inner join Clarity_Dep dic on esp.Department_ID = dic.Department_ID
"""

#UPDATE_ESP_Visit_Enc_Type_Name="""
#UPDATE clarity_production_temp.dbo.X_ESP_Visit	
#SET Enc_Type_Name = ABBR
#FROM clarity_production_temp.dbo.X_ESP_Visit esp inner join ZC_Disp_Enc_Type dic on esp.Enc_Type_C = dic.Disp_Enc_Type_C
#"""

# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Visit_TABLE="""
SELECT  Pat_ID, 
        Pat_MRN_ID,
        cast(Pat_Enc_CSN_ID as int),
        Contact_Date,
        Enc_Closed_YN,
        Enc_Close_Date,
        Visit_Prov_ID,
        cast(Department_ID as int),
        Department_Name,
        Enc_Type_Name,
        EDD_DT, 
        Temperature,
        Los_Proc_Code,
        rtrim(Weight),
        rtrim(Height),
        BP_Systolic,
        BP_Diastolic,
        O2_Stat,
        peak_flow,
        cast(replace(replace(Icd9,'&lt;' ,'<'),'&gt;' ,'>') as text) as Icd9,
        bmi,
        HOSP_ADMSN_TIME,
        HOSP_DISCHRG_TIME
FROM clarity_production_temp.dbo.X_ESP_Visit
"""

ESP_Visit_DICT= {'TABLENAME':"X_ESP_Visit", \
                 'DROP':[DROP_ESP_Visit_TABLE,], \
                 'CREATE':CREATE_ESP_Visit_TABLE, \
                 'UPDATE':[UPDATE_ESP_Visit_Department_Name], \
                 'SELECT':SELECT_ESP_Visit_TABLE}

                   #'UPDATE':[UPDATE_ESP_Visit_Department_Name, \
                   #          UPDATE_ESP_Visit_Enc_Type_Name], \

DROP_ESP_Immune_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Immune
"""

CREATE_ESP_Immune_TABLE="""
SELECT 	pat.Pat_id, 
	imm.Immunzatn_id, 
	dic.Name  as Immunization_Name,
	Immune_Date, 
	Dose, 
	imm.MFG_C, 
	mfg.Name as MFG_Name, 
	Lot, 
	Immune_ID,
        patient.PAT_MRN_ID as mrn,
        t.PROV_ID as provider_id,
        cast (Null as varchar(255)) as encounter_date,
        imm.IMMNZTN_STATUS_C
INTO clarity_production_temp.dbo.X_ESP_Immune	
FROM  clarity_production_temp.dbo.X_ESP_Patient pat inner join Immune imm on pat.Pat_ID = imm.Pat_ID
                inner join Clarity_Immunzatn dic on imm.Immunzatn_ID = dic.Immunzatn_ID
                inner join ZC_MFG mfg on imm.MFG_C = mfg.MFG_C
                left join (Select User_id, prov_id from Clarity_EMP where PROV_ID is not null) t  on ENTRY_USER_ID=t.USER_ID 
                left join PATIENT on pat.Pat_ID = PATIENT.PAT_ID
WHERE imm.entry_date >= %s or imm.update_date >= %s
""" % (esp_gd.yesterday, esp_gd.yesterday)


# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Immune_TABLE="""
SELECT  pat_id, 
        cast(Immunzatn_id as int),
        Immunization_Name,
        Immune_Date, 
        Dose,
        MFG_Name, 
        Lot, 
        cast(Immune_ID as int),
	cast (mrn as varchar(255)) as mrn,
	cast (provider_id as varchar(255)) as provider_id,
	cast (encounter_date as varchar(255)) as encounter_date,
        cast(immnztn_status_c as int)
FROM clarity_production_temp.dbo.X_ESP_Immune
"""

ESP_Immune_DICT= {'TABLENAME':"X_ESP_Immune", \
                 'DROP':[DROP_ESP_Immune_TABLE,], \
                   'CREATE':CREATE_ESP_Immune_TABLE, \
                   'UPDATE':[], \
                   'SELECT':SELECT_ESP_Immune_TABLE}

DROP_ESP_Allergies_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Allergies
"""

CREATE_ESP_Allergies_TABLE="""
SELECT  pat.Pat_ID, 
	pat.Pat_MRN_ID, 
	al.Allergy_ID, 
	Date_Noted, 
	al.Allergen_ID, 
	Allergen_Name, 
	zc.Title, 
	Description, 
	al.Alrgy_Entered_Dttm,
        t.PROV_ID as provider_id				
INTO clarity_production_temp.dbo.X_ESP_Allergies		
FROM clarity_production_temp.dbo.X_ESP_Patient pat inner join Allergy al on pat.Pat_ID = al.Pat_ID
                inner join Cl_ELG cl on al.Allergen_Id = cl.Allergen_ID
                inner join ZC_Alrgy_Status zc on al.Alrgy_Status_C = zc.Alrgy_Status_C
                left join (Select User_id, prov_id from Clarity_EMP where PROV_ID is not null) t  on ENTRY_USER_ID=t.USER_ID 
WHERE pat.Pat_id in (SELECT Pat_id FROM clarity_production_temp.dbo.X_ESP_Updated_Patid EXCEPT SELECT Pat_id FROM clarity_production_temp.dbo.X_ESP_Initial_Load_Done)
"""

# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Allergies_TABLE="""
SELECT  pat_id, 
        pat_mrn_id,
        cast(Allergy_ID as int),
        Date_Noted, 
        cast(Allergen_ID as int),
        Allergen_Name,
        Title, 
        Description, 
        Alrgy_Entered_Dttm,
        cast (provider_id as varchar(255)) as provider_id
FROM clarity_production_temp.dbo.X_ESP_Allergies
"""

ESP_Allergies_DICT= {'TABLENAME':"X_ESP_Allergies", \
                 'DROP':[DROP_ESP_Allergies_TABLE,], \
                   'CREATE':CREATE_ESP_Allergies_TABLE, \
                   'UPDATE':[], \
		   'SELECT':SELECT_ESP_Allergies_TABLE}

DROP_ESP_Social_Hx_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Social_Hx
"""

CREATE_ESP_Social_Hx_TABLE="""
SELECT pat.Pat_ID,
       pat.Pat_MRN_ID,
       zc.TITLE as TOBACCO_USER,
       zca.title as IS_Alcohol_user,
       convert(varchar, Pat_enc.contact_date,112) as date_noted,
       coalesce(sh.HX_LNK_ENC_CSN, sh.pat_enc_csn_id) as natural_key,
       pat_enc.VISIT_PROV_ID as provider_ID,
       case when sh.FEMALE_PARTNER_YN = 'Y' and sh.MALE_PARTNER_YN = 'Y' then 'MALE + FEMALE'
       when sh.FEMALE_PARTNER_YN = 'Y' then 'FEMALE'
       when sh.MALE_PARTNER_YN = 'Y' then 'MALE' end as gender_of_sex_partners,
       sh.ALCOHOL_OZ_PER_WK as alcohol_oz_per_week,
       zcill.title as illegal_drug_use,
       zcsexa.title as sexually_active,
       LTRIM(concat (
            (case when condom_yn = 'Y' then ' CONDOM +' else null end), 
            (case when pill_yn = 'Y' then ' PILL +' else null end),
            (case when DIAPHRAGM_YN = 'Y' then ' DIAPHRAGM +' else null end),
            (case when IUD_YN = 'Y' then ' IUD +' else null end),
            (case when SURGICAL_YN = 'Y' then ' SURGICAL +' else null end),
            (case when SPERMICIDE_YN = 'Y' then ' SPERMICIDE +' else null end),
            (case when IMPLANT_YN = 'Y' then ' IMPLANT +' else null end),
            (case when RHYTHM_YN = 'Y' then ' RHYTHM +' else null end),
            (case when INJECTION_YN = 'Y' then ' INJECTION +' else null end),
            (case when SPONGE_YN = 'Y' then ' SPONGE_YN +' else null end),
            (case when INSERTS_YN = 'Y' then ' INSERTS +' else null end),
            (case when ABSTINENCE_YN = 'Y' then ' ABSTINENCE +' else null end))) as birth_control_method
       -- no vaping field yet so leave this commented out for now
       --Val.SMRTDTA_ELEM_VALUE vaping_user
INTO clarity_production_temp.dbo.X_ESP_Social_Hx
FROM Patient pat
             left join Social_Hx sh on pat.Pat_ID = sh.Pat_ID
             inner join PAT_ENC on sh.pat_ENC_CSN_ID = pat_enc.PAT_ENC_CSN_ID
             left join SMRTDTA_ELEM_DATA sed on pat_enc.pat_enc_csn_id=sed.contact_serial_num and sed.element_id='CHA#50170'
             left join SMRTDTA_ELEM_Value val on sed.HLV_ID=val.HLV_ID
             left join ZC_TOBACCO_USER zc on sh.TOBACCO_USER_C=zc.TOBACCO_USER_C
             left join ZC_OB_GPTPAL zca on sh.ALCOHOL_USE_C=zca.OB_GPTPAL_C
             left join ZC_ILL_DRUG_USER zcill on sh.ILL_DRUG_USER_C = zcill.ILL_DRUG_USER_C
             left join ZC_SEXUALLY_ACTIVE zcsexa on sh.SEXUALLY_ACTIVE_C = zcsexa.SEXUALLY_ACTIVE_C
WHERE coalesce(zc.TITLE, zca.title, Val.SMRTDTA_ELEM_VALUE, sh.MALE_PARTNER_YN, sh.FEMALE_PARTNER_YN) is not null
AND pat_enc.Contact_Date < %s and pat_enc.contact_date >= %s
"""  % (esp_gd.today, esp_gd.yesterday)


SELECT_ESP_Social_Hx_TABLE="""
SELECT  *
FROM clarity_production_temp.dbo.X_ESP_Social_Hx
"""

ESP_Social_Hx_DICT= {'TABLENAME':"X_ESP_Social_Hx", \
                 'DROP':[DROP_ESP_Social_Hx_TABLE,], \
                   'CREATE':CREATE_ESP_Social_Hx_TABLE, \
                   'UPDATE':[], \
                   'SELECT':SELECT_ESP_Social_Hx_TABLE}

DROP_ESP_Problem_List_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Problem_List
"""

CREATE_ESP_Problem_List_TABLE="""
SELECT  pat.Pat_ID,
        pat.Pat_MRN_ID,
        concat(Problem_List_ID, '_', edg.line) as Problem_List_ID,
        convert(varchar, Noted_Date, 112) noted_date,
        edg.Code,
        zc.title as Status,
        replace(replace(Problem_Cmt,'^','**'), nchar(0x00A0),' ') as Problem_Cmt,
        t.prov_id as provider_id,
        prob.HOSPITAL_PL_YN,
        'icd10' type
INTO clarity_production_temp.dbo.X_ESP_Problem_List   
FROM Patient pat
             inner join Problem_List prob    on pat.Pat_Id = prob.Pat_Id
             join edg_current_icd10 edg on edg.dx_id = prob.dx_id
             left join ZC_PROBLEM_STATUS zc on prob.PROBLEM_STATUS_C=zc.PROBLEM_STATUS_C
             left join (Select User_id, prov_id from Clarity_EMP where PROV_ID is not null) t  on ENTRY_USER_ID=t.USER_ID
WHERE noted_date > convert(datetime, '20100101', 112)
      and (prob.noted_date >= %s or prob.update_date >= %s)
""" % (esp_gd.yesterday, esp_gd.yesterday)


# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Problem_List_TABLE="""
SELECT  Pat_ID, 
        Pat_MRN_ID,
        Problem_List_ID,
        Noted_Date, 
        Code,
        Status,
        Problem_Cmt,
        cast (provider_id as varchar(255)) as provider_id, 
        hospital_PL_YN,
        type
FROM clarity_production_temp.dbo.X_ESP_Problem_List
"""
# 
UPDATE_ESP_Problem_List_Noted_Date_TABLE="""
UPDATE clarity_production_temp.dbo.X_ESP_Problem_List      
SET Noted_Date = '1/1/1800'
WHERE Noted_Date is NULL
"""

ESP_Problem_List_DICT= {'TABLENAME':"X_ESP_Problem_List", \
                 'DROP':[DROP_ESP_Problem_List_TABLE,], \
                   'CREATE':CREATE_ESP_Problem_List_TABLE, \
                   'UPDATE':[UPDATE_ESP_Problem_List_Noted_Date_TABLE,], \
                   'SELECT':SELECT_ESP_Problem_List_TABLE}


DROP_ESP_Provider_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Provider
"""

CREATE_ESP_Provider_TABLE="""
SELECT  Distinct ser.PROV_ID as provider_id_num,
        LEFT(ser.PROV_NAME, CHARINDEX(',', ser.PROV_NAME + ',') -1) as last_name,
        LEFT(
                SUBSTRING (ser.PROV_NAME, CHARINDEX(',', ser.PROV_NAME)+2, LEN(PROV_NAME)),
                CHARINDEX(' ',
                SUBSTRING (ser.PROV_NAME, CHARINDEX(',', ser.PROV_NAME)+2, LEN(PROV_NAME))
                        + ' ')) as first_name,
        dbo.fnSplitName(ser.PROV_NAME,'Middle') as middle_name,
        ser.CLINICIAN_TITLE as title,
        serdept.DEPARTMENT_ID as department_id,
        coalesce(dept.EXTERNAL_NAME, dept.DEPARTMENT_NAME) as department_name,
                seraddr.ADDR_LINE_1 as dept_address_1,
                seraddr.ADDR_LINE_2 as dept_address_2,
                seraddr.CITY as dept_city,
                case when seraddr.STATE_C = 22 then 'MA' else NULL end as dept_state,
                seraddr.zip as dept_zip,
        isnull (SUBSTRING(ser.OFFICE_PHONE_NUM,1,3), '617') as office_area,
        isnull (SUBSTRING(ser.OFFICE_PHONE_NUM,5,9) , '665-1000')as office_phone,
        cast (Null as varchar) as centerid,
        null as dept_country,
        null dept_county_code,
        null tel_country_code,
        null as tel_ext,
        null as call_info,
        null as clin_address1,
        null as clin_address2,
        null as clin_CITY,
        null as clin_state,
        null as clin_zip,
        null as clin_country,
        null as clin_county_code,
        null as clin_tel_country_code,
        null as clin_areacode,
        null as clin_tel,
        null as clin_tel_ext,
        null as clin_call_info,
        null as suffix,
        null as dept_addr_type,
        null as clin_addr_type,
        ser2.npi as npi,
        1 as provider_type
INTO clarity_production_temp.dbo.X_ESP_Provider
FROM Clarity_SER ser
left join Clarity_SER_2 ser2 on ser.PROV_ID = ser2.Prov_ID
left join (select PROV_ID, ADDR_LINE_1, ADDR_LINE_2, CITY, STATE_C, zip from CLARITY_SER_ADDR where line = 1) seraddr on ser.PROV_ID = seraddr.Prov_ID
left join CLARITY_SER_DEPT serdept on ser.PROV_ID = serdept.Prov_ID
left join CLARITY_DEP dept on dept.DEPARTMENT_ID = serdept.DEPARTMENT_ID
WHERE
ser.PROV_ID is not NULL
AND (SER2.INSTANT_OF_UPDATE_DTTM > DATEADD(yyyy,-1,getdate()) or SER2.INSTANT_OF_UPDATE_DTTM is NULL)
--physicians may serve at mulitple locations we just one the first one
AND (serdept.LINE is null or serdept.LINE = 1)

UNION

-- ADD IN DEPARTMENTS AS FACILITY PROVIDERS
SELECT  Distinct cast(dep.DEPARTMENT_ID AS VARCHAR) as provider_id_num,
        null as last_name,
        null as first_name,
        null as middle_name,
        null as title,
        dep.DEPARTMENT_ID as department_id,
        coalesce(dep.EXTERNAL_NAME, dep.DEPARTMENT_NAME) as department_name,
        addr1.ADDRESS as dept_address_1,
        addr2.ADDRESS as dept_address_2,
        dep2.ADDRESS_CITY as dept_city,
        case when dep2.ADDRESS_STATE_C = 22 then 'MA' else NULL end as dept_state,
        dep2.ADDRESS_ZIP_CODE as dept_zip,
        isnull (SUBSTRING(dep.PHONE_NUMBER,1,3), '617') as office_area,
        isnull (SUBSTRING(dep.PHONE_NUMBER,5,9) , '665-1000')as office_phone,
        cast (Null as varchar) as centerid,
        null as dept_country,
        null dept_county_code,
        null tel_country_code,
        null as tel_ext,
        null as call_info,
        null as clin_address1,
        null as clin_address2,
        null as clin_CITY,
        null as clin_state,
        null as clin_zip,
        null as clin_country,
        null as clin_county_code,
        null as clin_tel_country_code,
        null as clin_areacode,
        null as clin_tel,
        null as clin_tel_ext,
        null as clin_call_info,
        null as suffix,
        null as dept_addr_type,
        null as clin_addr_type,
        null as npi,
        2 as provider_type
FROM CLARITY_DEP dep
left join CLARITY_DEP_2 dep2 on dep2.DEPARTMENT_ID = dep.DEPARTMENT_ID
left join (select department_id, address from CLARITY_DEP_ADDR where line = 1) addr1 ON addr1.department_id = dep.DEPARTMENT_ID
left join (select department_id, address from CLARITY_DEP_ADDR where line = 2) addr2 ON addr2.DEPARTMENT_ID = dep.DEPARTMENT_ID
WHERE
Dep.DEPARTMENT_ID is not NULL;
"""


# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Provider_TABLE="""
SELECT * FROM clarity_production_temp.dbo.X_ESP_Provider
"""

ESP_Provider_DICT= {'TABLENAME':"X_ESP_Provider", \
                 'DROP':[DROP_ESP_Provider_TABLE,], \
                   'CREATE':CREATE_ESP_Provider_TABLE, \
                   'UPDATE':[], \
                   'SELECT':SELECT_ESP_Provider_TABLE}

# New table to support X_ESP_Tests creation.  Note: This should be run before X_ESP_Tests is created.
DROP_ESP_Labs_Map_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Labs_Map
"""

CREATE_ESP_Labs_Map_TABLE="""
SELECT
    EAP.proc_id, 
    EAP.proc_name,
    EAP.PROC_CODE,
    cc.COMMON_NAME,
    cc.component_ID,
    xcc.alt_component_codes MT_Mnemonic,
    cc.common_name as compcommon_name,
    eap.proc_name + '--' + isnull(cc.common_name, ' ' ) as description,
    CAST(PROC_CODE AS VARCHAR(10)) + '--' + isnull(cast(CC.COMPONENT_ID as varchar(10)), ' ') aS NEWID
INTO clarity_production_temp.dbo.X_ESP_Labs_Map
FROM Clarity.dbo.CLARITY_EAP eap 
    inner join clarity.dbo.EAP_STDRD_COMPON esc on EAP.proc_id= esc.PROC_ID
    left outer join clarity.dbo.CLARITY_COMPONENT cc on esc.STDRD_COMPON_ID=cc.COMPONENT_ID
    left outer join clarity.dbo.X_COMPONENT_CODES xcc on cc. COMPONENT_ID=xcc.COMPONENT_ID
WHERE cc.component_id <900000
UNION
SELECT 
    EAP.proc_id,
    EAP.proc_name,
    EAP.PROC_CODE,
    cc.COMMON_NAME,
    cc.component_ID, 
    iei.identity_id MT_Mnemonic, 
    isnull(cc.common_name, eap.PROC_NAME) as compcommon_name ,
    eap.proc_name + '--' + isnull(cc.common_name, ' ' ) as description,
    CAST(PROC_CODE AS VARCHAR(10)) + '--' + isnull(cast(CC.COMPONENT_ID as varchar(10)), ' ')  aS NEWID
FROM clarity.dbo.CLARITY_EAP eap
    inner join clarity.dbo.IDENTITY_EAP_ID iei on eap.PROC_ID = iei.PROC_ID
    left outer join clarity.dbo.x_component_codes xcc on iei.identity_id = xcc.alt_component_codes
    left outer join clarity.dbo.clarity_component cc on xcc.COMPONENT_ID = cc.component_id
    left outer join clarity.dbo.IDENTITY_ID_TYPE iit on iei.IDENTITY_TYPE_ID = iit.ID_TYPE
WHERE iei.IDENTITY_TYPE_ID = 5  or iei.IDENTITY_TYPE_ID = 5034  
UNION
SELECT Distinct 
    EAP.PROC_ID,
    eap.proc_name,
    EAP.PROC_CODE,
    cc.common_name, 
    CC.COMPONENT_ID,
    cast (Null as nvarchar)as MTMnemonic,
    isnull(cc.common_name,eap.PROC_NAME) as compcommon_name,
    eap.proc_name + '--' + isnull(cc.common_name, ' ' ) as description,
    eap.PROC_CODE + '--' + isnull(cast(CC.COMPONENT_ID as varchar(10)), ' ') as NEWID
FROM Clarity.dbo.CLARITY_EAP EAP 
    INNER JOIN Clarity.dbo.EAP_STDRD_COMPON ESC on EAP.PROC_Id = esc.proc_ID
    INNER JOIN Clarity.dbo.CLARITY_COMPONENT CC ON ESC.STDRD_COMPON_ID=cc.component_ID
WHERE COMPONENT_ID <900000
"""

# Note: I'm using an "UPDATE" query to create the index
UPDATE_ESP_Labs_Map_idx_NEWID="""
CREATE CLUSTERED INDEX idx_NEWID on clarity_production_temp.dbo.X_ESP_Labs_Map(NewID)
"""

# Note: This should never need to be used
SELECT_ESP_Labs_Map_TABLE="""
SELECT * from clarity_production_temp.dbo.X_ESP_Labs_Map
"""

ESP_Labs_Map_DICT= {'TABLENAME':"X_ESP_Labs_Map", \
                 'DROP':[DROP_ESP_Labs_Map_TABLE,], \
                   'CREATE':CREATE_ESP_Labs_Map_TABLE, \
                   'UPDATE':[UPDATE_ESP_Labs_Map_idx_NEWID], \
                   'SELECT':SELECT_ESP_Labs_Map_TABLE}


DROP_ESP_Tests_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Tests
"""

# Note: Column order doesn't match spec.  It matches load_epic.py order.
CREATE_ESP_Tests_TABLE="""
SELECT distinct
    pat.Pat_ID,
    pat.Pat_MRN_ID,
    pro.Order_Proc_ID,
    pro.Proc_ID,
    cast (Null as nvarchar) Modifier,
    cast (Null as nvarchar)as specimen_id /*Health accession number no CHA value*/,
    pro.Ordering_Date,
    pro.order_type_c as result_type,
    pro.Authrzing_Prov_ID,
    pro.Description as Title,
    zc.Title as Specimen_Title
INTO clarity_production_temp.dbo.X_ESP_Tests
FROM Patient pat
        inner join clarity.dbo.Order_Proc pro on pat.Pat_ID = pro.Pat_ID
        left join clarity.dbo.order_proc_2 op2 on pro.order_proc_id =op2.order_proc_id
        left join clarity.dbo.ZC_Specimen_Source zc on pro.Specimen_Source_C = zc.Specimen_Source_C
WHERE pro.ordering_date <= %s and pro.ordering_DATE >= dateadd(DAY,-5,%s)
and pro.ORDER_STATUS_C <> 4 and pro.order_type_c is not null
and pro.LAB_STATUS_C is not null
""" % (esp_gd.today, esp_gd.yesterday)



# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Tests_TABLE="""
SELECT  pat_id,
        pat_mrn_id, 
        cast(Order_Proc_ID as int),
        Proc_ID,
        Modifier,
        specimen_id,
        Ordering_Date,
        Result_Type,
        Authrzing_Prov_ID,
        Title,
        Specimen_Title
FROM clarity_production_temp.dbo.X_ESP_Tests
"""

ESP_Tests_DICT= {'TABLENAME':"X_ESP_Tests", \
                 'DROP':[DROP_ESP_Tests_TABLE,], \
                   'CREATE':CREATE_ESP_Tests_TABLE, \
                   'UPDATE':[], \
                   'SELECT':SELECT_ESP_Tests_TABLE}

DROP_ESP_Test_Results_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Test_Results
"""


# Note: Column order doesn't match spec.  It matches load_epic.py order.
CREATE_ESP_Test_Results_TABLE="""
SELECT  distinct
        PRO.PAT_ID,
        VPT.PAT_MRN_Id,
        RES.ORDER_PROC_ID,
        PRO.ORDERING_DATE,
        PRO.RESULT_TIME,
        PRO.AUTHRZING_PROV_ID,
        PRO.ORDER_TYPE_C as result_type,
        PRO.proc_code,
        LAB.COMPONENT_ID,
        EAP.PROC_CODE + '--' + isnull(cast(RES.COMPONENT_ID as varchar(10)), ' ') as NEWID,
        (LAB.description + isnull(CC.common_name, ' ')) as Description,
        RES.ORD_VALUE,
        RES.RESULT_FLAG_C,
        cast(null as nvarchar) as flag_result_title,
        RES.REFERENCE_LOW,
        RES.REFERENCE_HIGH,
        RES.reference_Unit,
        RES.result_Status_c,
        cast (Null as nvarchar)as Result_Status_Title,
        cast (Null as nvarchar)as Results_comp_Cmt,
        cast (Null as nvarchar)as specimen_id,
        cast (Null as nvarchar)as impression,
        ZC.TITLE as Specimen_Title,
        OP2.SPECIMN_TAKEN_DATE,
        EAP.PROC_NAME as Result_Title,
        --pretty clealy CHA specific, but they do not keep CLIA in EPIC or Clarity
        case
          when llb.lab_name='FAULKNER HOSPITAL' then '22D0663738'
          when llb.lab_name='FAULKNER MEDICAL LABORATORIES, INC' then '22D0077095'
          when llb.lab_name='GUARDIAN HEALTHCARE' then '22D2026744'
          when llb.lab_name='LAB CORP' then '22D0966206'
          when llb.lab_name='LABCORP' then '22D0966206'
          when llb.lab_name='LAHEY CLINIC HOSPITAL BURLINGTON' then '22D0677827'
          when llb.lab_name='LAHEY CLINIC HOSPITAL PEABODY' then '22D0881073'
          when llb.lab_name='MASS GENERAL HOSPITAL' then '22D1093931'
          when llb.lab_name='ORTHOPAEDIC DEPARTMENT' then '22D1031087'
          when llb.lab_name='QUEST DIAGNOSTICS' then '22D0650288'
          when llb.lab_name='SOMERVILLE HOSPITAL LABORATORY' then '22D0937127'
          when llb.lab_name='WHIDDEN MEMORIAL HOSPITAL LABORATORY' then '22D0994149'
          when llb.lab_name='WINCHESTER HOSPITAL LAB FAMILY MEDICAL CENTER' then '22D0072375'
          when llb.lab_name='WINCHESTER HOSPITAL LABORATORY' then '22D2027277'
          when llb.lab_name='CHA IP LAB' then '22D0672414'
          else '22D0650282'
        end as clia, -- the else clia is Cambridge Hospital main lab
        null as collection_date_end,
        null as status_date,
        null as interpreter,
        null as interpreter_id,
        null as interp_id_auth,
        null as interp_uid,
        null as lab_method,
        null as ref_text,
        coalesce(ENC.DEPARTMENT_ID, ENC.EFFECTIVE_DEPT_ID) as facility_provider_id 
INTO clarity_production_temp.dbo.X_ESP_Test_Results
FROM Clarity.dbo.ORDER_RESULTS RES 
        join Clarity.dbo.ORDER_PROC PRO ON RES.ORDER_PROC_ID=PRO.ORDER_PROC_ID
        left join Clarity.dbo.CLARITY_EAP EAP ON PRO.PROC_ID=EAP.PROC_ID
        inner join clarity_production_temp.dbo.esp_labs2 LAB ON PRO.PROC_ID=LAB.PROC_ID
        inner join Clarity.dbo.ORDER_PROC_2 OP2 ON RES.ORDER_PROC_ID=OP2.ORDER_PROC_ID
        inner join Clarity.dbo.V_PATIENT VPT ON PRO.PAT_ID=VPT.PAT_ID
        inner join Clarity.dbo.CLARITY_COMPONENT CC on RES.COMPONENT_ID=CC.COMPONENT_ID
        left join Clarity.dbo.ZC_SPECIMEN_SOURCE ZC ON PRO.SPECIMEN_SOURCE_C=ZC.SPECIMEN_SOURCE_C
        left join Clarity.dbo.c_clarity_LLB llb on pro.result_lab_id=llb.lab_id
        left join PAT_ENC ENC ON RES.PAT_ENC_CSN_ID=ENC.PAT_ENC_CSN_ID
WHERE RES.RESULT_DATE >= %s and RES.RESULT_DATE < %s and LAB.COMPONENT_ID is null and pro.ORDER_STATUS_C<>4
""" % (esp_gd.yesterday, esp_gd.today)


UPDATE_ESP_Test_Results_flag_Result_Title="""			
UPDATE clarity_production_temp.dbo.X_ESP_Test_Results
SET  flag_Result_Title	= Title 
FROM clarity_production_temp.dbo.X_ESP_Test_Results res inner join Clarity.dbo.zc_Result_Flag flag 
		on res.Result_Flag_C = flag.Result_Flag_C	
"""

UPDATE_ESP_Test_Results_Result_Status_Title="""	
UPDATE clarity_production_temp.dbo.X_ESP_Test_Results
SET  Result_Status_Title	= Title 
FROM clarity_production_temp.dbo.X_ESP_Test_Results res inner join Clarity.dbo.zc_Result_Status stat
		on res.Result_Status_C = stat.Result_Status_C	
"""

# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Test_Results_TABLE="""
SELECT  Pat_ID,
        Pat_MRN_ID,
        cast(Order_Proc_ID as int),
        Ordering_Date,
        Result_Time,
        Authrzing_Prov_ID,
        Result_Type,
        NEWID,
        cast(Component_id as int),
        Description,
        Ord_value,
        flag_Result_Title,
        Reference_low,
        Reference_High,
        Reference_Unit,
        Result_Status_Title,
        Results_comp_Cmt,
        specimen_id,
        impression,
        Specimen_Title,
        Specimn_Taken_Date,
        Result_Title,
        cast (Null as nvarchar)  as natkey,
        cast (Null as nvarchar) as patclass,
        cast (Null as nvarchar) as patstatus,
        clia,
        collection_date_end,
        status_date,
        interpreter,
        interpreter_id,
        interp_id_auth,
        interp_uid,
        lab_method,
        ref_text,
        facility_provider_id
FROM clarity_production_temp.dbo.X_ESP_Test_Results
"""

ESP_Test_Results_DICT= {'TABLENAME':"X_ESP_Test_Results", \
                 'DROP':[DROP_ESP_Test_Results_TABLE,], \
                 'CREATE':CREATE_ESP_Test_Results_TABLE, \
                 'UPDATE':[UPDATE_ESP_Test_Results_flag_Result_Title, \
                             UPDATE_ESP_Test_Results_Result_Status_Title], \
                 'SELECT':SELECT_ESP_Test_Results_TABLE}

DROP_ESP_Meds_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Meds
"""

CREATE_ESP_Meds_TABLE="""
SELECT  pat.Pat_ID,
        pat.Pat_MRN_ID,
        Order_Med_ID,
        Med_Presc_Prov_ID, 
        isnull (Ordering_Date, Order_Inst) as Ordering_Date,
        zc.Title as Order_Status_Title,
        cl.Name as Order_Med_Name,
        cast (Null as nvarchar)as NDC_Code,
        Sig,
        Quantity,
        Refills,
        Start_Date,
        End_Date,
        cl.Route,
        CAST (null as nvarchar) as Dose,
        case
          when med.order_class_c is not null then
            convert(varchar,ordering_mode_c) + '.' + convert(varchar,med.order_class_c)
          else convert(varchar,ordering_mode_c)
        end as patient_class,
        null as patient_status,
        AUTHRZING_PROV_ID as managing_provider_id, 
        coalesce(med.PAT_LOC_ID, ENC.DEPARTMENT_ID) as facility_provider_id
INTO clarity_production_temp.dbo.X_ESP_Meds
FROM Patient pat 
        inner join Order_Med med on pat.Pat_ID = med.Pat_ID
        left join zc_Order_Status zc on med.Order_Status_C = zc.Order_Status_C
        inner join Clarity_Medication cl on med.Medication_ID = cl.Medication_ID
        left join ZC_ORDER_CLASS zco on med.ORDER_CLASS_C =zco.ORDER_CLASS_C
        left join PAT_ENC enc on enc.PAT_ENC_CSN_ID = med.PAT_ENC_CSN_ID
WHERE med.UPDATE_DATE < %s and med.UPDATE_DATE >= %s 
AND (zco.ORDER_CLASS_C <>3 or zco.ORDER_CLASS_C is null)
AND (zc.order_Status_c <>4 or zc.order_Status_c is null)
""" % (esp_gd.today, esp_gd.yesterday)


UPDATE_ESP_Meds_NDC_Code="""	
UPDATE clarity_production_temp.dbo.X_ESP_Meds
SET  NDC_Code	= rx.NDC_Code 
FROM Rx_Med_NDC_Code rx  inner join Order_Med med on med.Medication_ID = rx.Medication_ID
WHERE LINE = 1
"""

# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Meds_TABLE="""
SELECT  Pat_ID,
        Pat_MRN_ID,
        cast(Order_Med_ID as int),
        Med_Presc_Prov_ID,
        Ordering_Date,
        Order_Status_Title,
        Sig,
        NDC_Code,
        Order_Med_Name,
        Quantity,
        Refills,
        Start_Date,
        End_Date,
        Route,
        dose,
        patient_class,
        patient_status,
        managing_provider_id,
        facility_provider_id
FROM clarity_production_temp.dbo.X_ESP_Meds
"""

ESP_Meds_DICT= {'TABLENAME':"X_ESP_Meds", \
                'DROP':[DROP_ESP_Meds_TABLE,], \
                'CREATE':CREATE_ESP_Meds_TABLE, \
                'UPDATE':[UPDATE_ESP_Meds_NDC_Code,], \
                'SELECT':SELECT_ESP_Meds_TABLE}


# Note: Need to drop temporary tables as well.
DROP_ESP_Pregnancy_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Pregnancy
"""

DROP_ESP_Pregnancy_Temp_TABLE="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Pregnancy_Temp
"""

CREATE_ESP_Pregnancy_Temp_TABLE="""
SELECT distinct
	vr.pat_mrn_id as Baby_MRN, 
	vr.pat_name as Baby_Name, 
	vp.PAT_MRN_ID as Mom_MRN,
	vp.PAT_NAME as Mom_Name,
	vr.BIRTH_DATE,  
	vr.PED_DELIVR_METH_C,
	vr.PED_GEST_AGE,
	(vr.PED_BIRTH_WT) as PED_BIRTH_WT,
	t.episode_id
INTO clarity_production_temp.dbo.X_ESP_Pregnancy_Temp
FROM  v_pat_enc vpe left join vr_patient vr on   vpe.pat_id = vr.pat_id 
	left join IP_episode_link el on vpe.pat_enc_csn_id=el.PAT_ENC_CSN_ID
	left join V_PATIENT vp on vr.MOTHER_PAT_ID=vp.pat_id
	left join ( SELECT distinct el.EPISODE_ID,   pt.Pat_name as Mom, pt.PAT_MRN_ID, 
				obd.OB_DELIVERY_DATE, obds.OBD_SIGNED_USER_ID, obds.OBD_SIGNED_TIME
				FROM V_PAT_ENC vpe
					left join EPISODE_LINK el  on vpe.PAT_ENC_CSN_ID=el.PAT_ENC_CSN_ID
					inner join EPISODE e on el.episode_id  = e.EPISODE_ID
					inner join PATIENT pt on vpe.PAT_ID    = pt.PAT_ID
					left join PATIENT mp  on pt.MOTHER_PAT_ID=mp.PAT_ID
					left join OB_HSB_DELIVERY obd  on  el.episode_id  = obd.SUMMARY_BLOCK_ID  
					Left join [OB_HSB_DEL_SIGN] obds on el.episode_id = obds.SUMMARY_BLOCK_ID 
				WHERE OB_DELIVERY_DATE is not null and obds.LINE=1) T on vp.Pat_mrn_id=t.Pat_mrn_id
WHERE vr.Birth_Date >='5/30/2012'  /* Hard  coded because this is when we went live with STORK*/
	and enc_type_title in ('Hospital Encounter') --and vr.ped_Delivr_meth_c is not null a
	and el.EPISODE_ID is not null
        and vr.pat_mrn_id not in ('0001163596','0001163606','0001165264', '0001169192', '0001172892', '0001172894', 
                                  '0001172896', '0001172897', '0001172900', '0001172902', '0001173018', '0001173019', 
                                  '0001173022', '0001173025', '0001173026')
ORDER BY vr.PAT_MRN_ID,t.EPISODE_ID
"""

CREATE_ESP_Pregnancy_TABLE="""
SELECT distinct 
	PE.Pat_ID,
	PT.PAT_MRN_ID, 
	PE.VISIT_PROV_ID,
	T.EPISODE_ID, 
	PT.EDD_DT ,
	OBD.OB_DELIVERY_DATE,
	Case when MAX(OB.OB_Gravidity)is null then '0' else MAX(OB.OB_Gravidity) END OB_GRAVIDITY,
	Case when MAX(OB.OB_Parity) is Null then '0' Else MAX(OB.OB_Parity) END OB_PARITY,
	Case when MAX(OB.OB_LIVING) IS Null then '0' Else MAX(OB.OB_LIVING) END as TERM, 
	Case when MAX(OB.OB_PREMATURE) IS Null then '0' ELSE MAX(OB.OB_PREMATURE) END as PRETERM  
INTO clarity_production_temp.dbo.X_ESP_Pregnancy
FROM  ( /**Finding max Line from EPISODE***/
		SELECT distinct e.EPISODE_ID, Max(el.Line) as MAXLINE
		FROM EPISODE E left join  EPISODE_LINK EL on e.episode_id  = el.EPISODE_ID
		WHERE el.EPI_STATUS_C = 1 
			and e.Sum_blk_type_id in ('2') /*2 = Pregnancy Episode*/
			and START_DATE >= '1/1/2011'  /* Just to limit down the episodes since those giving birth on new stork should have started before 1/1/2011*/
GROUP BY E.EPISODE_ID) T 
	Left join EPISODE_LINK EL on T.episode_id  = el.EPISODE_ID and T.MAXLINE=EL.LINE
	LEft Join EPISODE E on T.EPISODE_ID=E.EPISODE_ID
	left join PAT_ENC PE on el.PAT_ENC_CSN_ID=PE.PAT_ENC_CSN_ID
	left join PATIENT PT on PE.PAT_ID=PT.PAT_ID
	left join OB_TOTAL OB on PE.PAT_ID=OB.PAT_ID
	left join OB_HSB_DELIVERY OBD on EL.EPISODE_ID=OBD.SUMMARY_BLOCK_ID
WHERE el.EPI_STATUS_C =1 and e.Sum_blk_type_id in ('2') /*Pregnancy*/ and pe.pat_id is not null
	and START_DATE >= '1/1/2011' /* Just to limit down the episodes since those giving birth on new stork should have started before 1/1/2011*/
	and EL.EPISODE_ID is not null
	--AND CC.CONCEPT_ID IN ('EPIC#OB52')
GROUP BY PE.Pat_id,PT.PAT_MRN_ID, 
	PE.VISIT_PROV_ID,
	T.EPISODE_ID,
	PT.EDD_DT,
	OBD.OB_DELIVERY_DATE
ORDER BY Pat_id
"""


# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Pregnancy_TABLE="""
SELECT  
	preg.PAT_ID, 
	preg.PAT_MRN_ID, 
	preg.VISIT_PROV_ID,
	preg.EPISODE_ID, 
	cast (EDD_DT as nvarchar) as OUTCOME,
	ISNULL( convert(varchar(10),preg.EDD_DT, 112),CONVERT (nvarchar(30),(Convert(datetime,l.CONCEPT_VALUE)), 112  )) as EDD_DT, 
	CONVERT (nvarchar(30), preg.OB_DELIVERY_DATE, 112  ) as DELIVERY_DATE, 
	preg.OB_GRAVIDITY as Gravida,
	preg.OB_PARITY as Parity,
	preg.TERM, 
	preg.PRETERM,
	convert(nvarchar(30),SUBSTRING(preg_temp.PED_GEST_AGE,1,2))+'w'+ ISNULL(convert(nvarchar(30),SUBSTRING(preg_temp.PED_GEST_AGE,4,1)),'O')+'d' as GA,/* meeds to be removed*/
	convert(numeric(6,2),(convert(numeric(6,2),preg_temp.PED_BIRTH_WT) *0.0283495231 )) as BIRTH_WT, 
	zc.ABBR as Delivery,
	CAST (null as varchar) as Pre_eclampsia
FROM clarity_production_temp.dbo.X_ESP_Pregnancy preg 
  left join clarity_production_temp.dbo.X_ESP_Pregnancy_Temp preg_temp on preg.EPISODE_ID=preg_temp.episode_id
  left join  ZC_PED_DELIVR_METH zc on preg_temp.PED_DELIVR_METH_C=zc.PED_DELIVR_METH_C
  left join (SELECT * FROM [Clarity].[dbo].[EPISODE_CONCEPT] 
			WHERE CONCEPT_ID = 'MEDCIN#1504') L on preg.EPISODE_ID=l.EPISODE_ID
--order by preg.OB_DELIVERY_DATE
"""

ESP_Pregnancy_DICT= {'TABLENAME':"X_ESP_Pregnancy", \
                'DROP':[DROP_ESP_Pregnancy_TABLE,DROP_ESP_Pregnancy_Temp_TABLE,], \
                'CREATE':CREATE_ESP_Pregnancy_TABLE, \
                'UPDATE':[CREATE_ESP_Pregnancy_Temp_TABLE,], \
                'SELECT':SELECT_ESP_Pregnancy_TABLE}


ALTER_ESP_Visit="""
ALTER TABLE clarity_production_temp.dbo.X_ESP_Visit
DROP COLUMN Enc_Type_C 		
"""

DROP_ESP_Initial_Load_Done="""
DROP TABLE clarity_production_temp.dbo.X_ESP_Initial_Load_Done
"""

CREATE_ESP_Initial_Load_Done="""
CREATE TABLE clarity_production_temp.dbo.X_ESP_Initial_Load_Done
(PAT_ID varchar(18) NOT NULL,
 LOADED_DATE datetime
 PRIMARY KEY (PAT_ID))
"""

UPDATE_ESP_Initial_Load_Done="""
INSERT into clarity_production_temp.dbo.X_ESP_Initial_Load_Done (PAT_ID,LOADED_DATE)
(SELECT PAT_ID,GETDATE() from clarity_production_temp.dbo.X_ESP_Updated_Patid
 WHERE PAT_ID not in (SELECT PAT_ID from clarity_production_temp.dbo.X_ESP_Initial_Load_Done))
"""

if __name__ == '__main__':
    pass
