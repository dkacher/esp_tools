USE [ClarityExtracts]
GO

/****** Object:  StoredProcedure [dbo].[Assemble_ESP_Member]    Script Date: 2/11/2021 8:15:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Assemble_ESP_Member] ( @fileExtractName NVARCHAR(100), @startDate DATETIME2(3), @endDate DATETIME2(3))
AS
BEGIN
    /*--------------------------------------------------------
    Returns patient data required for the ESP member extract
    ---------------------------------------------------------*/
    DECLARE
        @Pat_ID VARCHAR(100)
      , @Line SMALLINT
      , @Alias VARCHAR(160)
      , @Alias_str VARCHAR(1000)
      , @MaxLine SMALLINT
      , @Insert BIT;

    SET NOCOUNT ON;

    CREATE TABLE #Alias ( PAT_ID VARCHAR(20), ALIAS_STR VARCHAR(1000));

    DROP TABLE IF EXISTS #Pat_Enc;

    -- all encounters for past 2 years 
    SELECT
        pe.*
    INTO
        #Pat_Enc
    FROM
        Clarity.dbo.PAT_ENC                                    AS pe
        INNER JOIN Clarity.dbo.PATIENT                         AS pa
           ON pa.PAT_ID = pe.PAT_ID
        INNER JOIN ClarityExtracts.dbo.FileExtractServiceAreas AS fsa
           ON fsa.serviceAreaId = pe.SERV_AREA_ID
        INNER JOIN ClarityExtracts.dbo.FileExtracts            AS ext
           ON ext.fileExtractId = fsa.fileExtractId
              AND ext.fileExtractName = @fileExtractName
    WHERE
        pe.CONTACT_DATE >= DATEADD ( YEAR, -2, GETDATE ())
        AND pe.CONTACT_DATE < @endDate
        AND
          ( --Include only valid appointments (2-Completed, 6-Arrived, 8-Non Encounter Visit, 9-Non-OCHIN Complete)
              pe.APPT_STATUS_C IN ( '2', '6', '8', '9' )
              OR pe.APPT_STATUS_C IS NULL );

    CREATE INDEX ix_IPID ON #Pat_Enc ( INPATIENT_DATA_ID );

    DROP TABLE IF EXISTS #Patient;

    -- All patients
    SELECT DISTINCT
        ii.IDENTITY_ID AS MRN
      , pa.*
    INTO
        #Patient
    FROM
        Clarity.dbo.PATIENT                AS pa
        INNER JOIN #Pat_Enc                AS pe
           ON pe.PAT_ID = pa.PAT_ID
        INNER JOIN Clarity.dbo.IDENTITY_ID AS ii
           ON ii.PAT_ID = pa.PAT_ID
              AND ii.IDENTITY_TYPE_ID = pe.SERV_AREA_ID
    WHERE
        pa.UPDATE_DATE >= @startDate
        AND pa.PAT_NAME NOT LIKE 'ZZ%';

    CREATE UNIQUE INDEX ix_PATID ON #Patient ( PAT_ID );

    DROP TABLE IF EXISTS #Housing_Status;

    -- Housing status
    SELECT
        PAT_ID
      , MEAS_VALUE
    INTO
        #Housing_Status
    FROM
       (   SELECT
               ipf.Y_PAT_ID                                                                AS PAT_ID
             , CASE
                    WHEN ipm.MEAS_VALUE IN ( '0', '1' ) THEN 'NOT HOMELESS'
                    WHEN ipm.MEAS_VALUE = '2' THEN 'HOMELESS'
                    WHEN ipm.MEAS_VALUE = 'DECLINED' THEN ipm.MEAS_VALUE
                    ELSE NULL
               END                                                                         AS MEAS_VALUE
             , ROW_NUMBER () OVER ( PARTITION BY pe.PAT_ID ORDER BY pe.CONTACT_DATE DESC ) AS Rownum
           FROM
               Clarity.dbo.IP_FLWSHT_REC_VIEW             AS ipf
               INNER JOIN Clarity.dbo.IP_FLWSHT_MEAS_VIEW AS ipm
                  ON ipf.FSD_ID = ipm.FSD_ID
               INNER JOIN #Pat_Enc                        AS pe
                  ON ipf.INPATIENT_DATA_ID = pe.INPATIENT_DATA_ID
           WHERE
               ipm.FLO_MEAS_ID = '5693'
               AND ipm.MEAS_VALUE IS NOT NULL ) AS a
    WHERE
        a.Rownum = 1;

    -- patient aliases delimited with |
    DECLARE CURSOR_alias CURSOR FOR
        SELECT
            alias.PAT_ID
          , alias.LINE
          , ( SELECT MAX ( a.LINE ) FROM Clarity.dbo.PATIENT_ALIAS AS a WHERE pa.PAT_ID = a.PAT_ID GROUP BY a.PAT_ID ) AS MAXLINE
          , alias.ALIAS
        FROM
            #Patient                             AS pa
            INNER JOIN Clarity.dbo.PATIENT_ALIAS AS alias
               ON alias.PAT_ID = pa.PAT_ID;

    OPEN CURSOR_alias;
    FETCH NEXT FROM CURSOR_alias
    INTO
        @Pat_ID
      , @Line
      , @MaxLine
      , @Alias;
    SET @Alias_str = @Alias;
    SET @Insert = 0;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        IF @Line = 1
           AND @MaxLine = 1
        BEGIN
            SET @Alias_str = @Alias;
            SET @Insert = 1;
        END;
        ELSE IF @Line <> @MaxLine
        BEGIN
            IF @Line > 1 SET @Alias_str = @Alias_str + '|' + @Alias;
            ELSE SET @Alias_str = @Alias;

            SET @Insert = 0;
        END;
        ELSE BEGIN
			SET @Alias_str = @Alias_str + '|' + @Alias;
			SET @Insert = 1;
        END;

        IF @Insert = 1
           AND CHARINDEX ( '^', @Alias_str ) = 0
        BEGIN
            INSERT INTO
                #Alias
            VALUES
                ( @Pat_ID, @Alias_str );

            SET @Alias_str = '';
        END;

        FETCH NEXT FROM CURSOR_alias
        INTO
            @Pat_ID
          , @Line
          , @MaxLine
          , @Alias;
    END;

    CLOSE CURSOR_alias;
    DEALLOCATE CURSOR_alias;

    IF OBJECT_ID ( 'tempdb..#Final_Results' ) IS NOT NULL
        DROP TABLE #Final_Results;

    SELECT
        pa.PAT_ID
      , pa.MRN
      , pa.PAT_LAST_NAME
      , pa.PAT_FIRST_NAME
      , pa.PAT_MIDDLE_NAME
      , addr1.ADDRESS                               AS ADDRESS1
      , addr2.ADDRESS                               AS ADDRESS2
      , pa.CITY                                     AS CITY
      , st.ABBR                                     AS STATE
      , pa.ZIP                                      AS ZIP
      , ctry1.TITLE                                 AS COUNTRY
      , LEFT(pa.HOME_PHONE, 3)                      AS AREA_CODE
      , RIGHT(pa.HOME_PHONE, 8)                     AS HOME_PHONE
      , NULL                                        AS HOME_PHONE_EXT
      , CONVERT ( VARCHAR(8), pa.BIRTH_DATE, 112 )  AS DOB
      , CASE
             WHEN pa.SEX_C IN ( '1', '2', '3' ) THEN sex.ABBR
             WHEN pa.SEX_C IN ( '4', '5' ) THEN 'T'
             ELSE 'U'
        END                                         AS GENDER
      , CASE
             WHEN prace1.PATIENT_RACE_C IN ( 1, 2, 6 ) THEN zrace.TITLE
             WHEN prace1.PATIENT_RACE_C IS NOT NULL
                  AND prace2.PATIENT_RACE_C IS NOT NULL THEN 'MULTIRACIAL'
             WHEN prace1.PATIENT_RACE_C = 3 THEN 'ALASKAN'
             WHEN prace1.PATIENT_RACE_C = 4 THEN 'INDIAN'
             WHEN prace1.PATIENT_RACE_C = 7 THEN 'ISLANDER/HAWAIIAN'
             WHEN prace1.PATIENT_RACE_C = 8 THEN 'PACIFIC'
             WHEN prace1.PATIENT_RACE_C = 9 THEN 'HISPANIC'
             WHEN prace1.PATIENT_RACE_C IN ( 5, 11 ) THEN 'UNKNOWN'
             WHEN prace1.PATIENT_RACE_C = 10 THEN 'DECLINED/REFUSED'
             ELSE 'UNKNOWN'
        END                                         AS RACE
      , lang.TITLE                                  AS LANGUAGE
      , pa.SSN
      , pa.CUR_PCP_PROV_ID                          AS PCP_ID
      , mari.TITLE                                  AS MARITAL_STATUS
      , reli.TITLE                                  AS RELIGION
      , alias.ALIAS_STR
      , mother.MRN                                  AS MOTHER_MRN
      , CONVERT ( VARCHAR(8), pa.DEATH_DATE, 112 )  AS DEATH_DATE
      , pa.CUR_PRIM_LOC_ID                          AS CENTER_ID
      , CASE
             WHEN pa.ETHNIC_GROUP_C = 1 THEN ethn.TITLE
             WHEN pa.ETHNIC_GROUP_C = 2 THEN 'NOT HISPANIC OR LATINO'
             ELSE 'OTHER OR UNDETERMINED'
        END                                         AS ETHNICITY
      , mother.PAT_NAME                             AS MOTHER_MAIDEN_NAME
      , CONVERT ( VARCHAR(8), pa.UPDATE_DATE, 112 ) AS LAST_UPDATE
      , NULL                                        AS SITE_ID_LAST_UPD
      , sufx.TITLE                                  AS SUFFIX
      , titl.TITLE
      , NULL                                        AS REMARK
      , fpl.INCOME_LEVEL
      , housing.MEAS_VALUE                          AS HOUSING_STATUS
      , fin.TITLE                                   AS INSURANCE_STATUS
      , ctry2.TITLE                                 AS COUNTRY_OF_BIRTH
    INTO
        #Final_Results
    FROM
        #Patient                                                         AS pa
        LEFT JOIN Clarity.dbo.PAT_ADDRESS                                AS addr1
          ON addr1.PAT_ID = pa.PAT_ID
             AND addr1.LINE = 1
        LEFT JOIN Clarity.dbo.PAT_ADDRESS                                AS addr2
          ON addr2.PAT_ID = pa.PAT_ID
             AND addr2.LINE = 2
        LEFT JOIN Clarity.dbo.ZC_STATE                                   AS st
          ON st.STATE_C = pa.STATE_C
        LEFT JOIN Clarity.dbo.ZC_COUNTRY                                 AS ctry1
          ON ctry1.COUNTRY_C = pa.COUNTRY_C
        LEFT JOIN Clarity.dbo.ZC_COUNTRY                                 AS ctry2
          ON ctry2.COUNTRY_C = pa.COUNTRY_OF_ORIG_C
        LEFT JOIN Clarity.dbo.ZC_SEX                                     AS sex
          ON pa.SEX_C = sex.RCPT_MEM_SEX_C
        LEFT JOIN Clarity.dbo.PATIENT_RACE                               AS prace1
          ON pa.PAT_ID = prace1.PAT_ID
             AND prace1.LINE = 1
        LEFT JOIN Clarity.dbo.PATIENT_RACE                               AS prace2
          ON pa.PAT_ID = prace2.PAT_ID
             AND prace2.LINE = 1
        LEFT JOIN Clarity.dbo.ZC_PATIENT_RACE                            AS zrace
          ON prace1.PATIENT_RACE_C = zrace.PATIENT_RACE_C
        LEFT JOIN Clarity.dbo.ZC_LANGUAGE                                AS lang
          ON pa.LANGUAGE_C = lang.LANGUAGE_C
        LEFT JOIN Clarity.dbo.ZC_MARITAL_STATUS                          AS mari
          ON pa.MARITAL_STATUS_C = mari.MARITAL_STATUS_C
        LEFT JOIN Clarity.dbo.ZC_RELIGION                                AS reli
          ON pa.RELIGION_C = reli.RELIGION_C
        LEFT JOIN #Alias                                                 AS alias
          ON pa.PAT_ID = alias.PAT_ID
        LEFT JOIN
           ( SELECT pa.PAT_ID, pa.MRN, pa.PAT_NAME FROM #Patient AS pa ) AS mother
          ON mother.PAT_ID = pa.MOTHER_PAT_ID
        LEFT JOIN Clarity.dbo.ZC_ETHNIC_GROUP                            AS ethn
          ON ethn.ETHNIC_GROUP_C = pa.ETHNIC_GROUP_C
        LEFT JOIN Clarity.dbo.ZC_PAT_NAME_SUFFIX                         AS sufx
          ON sufx.PAT_NAME_SUFFIX_C = pa.PAT_NAME_SUFFIX_C
        LEFT JOIN Clarity.dbo.ZC_PAT_TITLE                               AS titl
          ON pa.PAT_TITLE_C = titl.PAT_TITLE_C
        LEFT JOIN
           (   SELECT
                   pat_id
                 , INCOME_LEVEL
               FROM
                  (   SELECT
                          pat_id
                        , CONCAT ( CONVERT ( BIGINT, fpl_percentage ), '%' )                    AS INCOME_LEVEL
                        , ROW_NUMBER () OVER ( PARTITION BY pat_id ORDER BY fpl_eff_date DESC ) AS Rownum
                      FROM
                          Clarity.dbo.x_fpl_2019 ) AS x
               WHERE
                   x.Rownum = 1 )                                        AS fpl
          ON fpl.pat_id = pa.PAT_ID
        LEFT JOIN Clarity.dbo.PAT_ACCT_CVG                               AS pac
          ON pac.PAT_ID = pa.PAT_ID
             AND pac.LINE = 1
        LEFT JOIN Clarity.dbo.ZC_FIN_CLASS                               AS fin
          ON pac.FIN_CLASS = fin.FIN_CLASS_C
        LEFT JOIN #Housing_Status                                        AS housing
          ON housing.PAT_ID = pa.PAT_ID
    WHERE
        CHARINDEX ( '^', pa.PAT_LAST_NAME ) = 0
        AND CHARINDEX ( '^', ISNULL ( pa.PAT_FIRST_NAME, '' )) = 0
        AND CHARINDEX ( '^', ISNULL ( pa.PAT_MIDDLE_NAME, '' )) = 0;

    SELECT
        *
    FROM
        #Final_Results;

    DROP TABLE #Alias;

END;
GO

