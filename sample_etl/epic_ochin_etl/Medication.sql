USE [ClarityExtracts]
GO

/****** Object:  StoredProcedure [dbo].[Assemble_ESP_Medication]    Script Date: 2/11/2021 8:14:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Assemble_ESP_Medication]
(
    @fileExtractName NVARCHAR(100)
  , @startDate DATETIME2(3)
  , @endDate DATETIME2(3)
)
AS
BEGIN

    SET NOCOUNT ON;
    /*----------------------------------------------------------------------
    Returns patient medication data required for the ESP medication extract
    ------------------------------------------------------------------------*/

    ----------------------- TESTING -----------------------------
    --DECLARE
    --	@fileExtractName NVARCHAR(100) = 'ESP_Medication'
    --  , @startDate DATETIME2(3) = '12/01/2019'
    --  , @endDate   DATETIME2(3)
    --  , @SA        INT          = 133 -- Lynn
    -------------------------------------------------------------

    DROP TABLE IF EXISTS #ORDER_MED;

    -- For improved performance
    SELECT
        ii.IDENTITY_ID AS MRN
      , om.*
    INTO
        #ORDER_MED
    FROM
        Clarity.dbo.ORDER_MED                                  AS om
        INNER JOIN Clarity.dbo.PAT_ENC                         AS pe
           ON pe.PAT_ENC_CSN_ID = om.PAT_ENC_CSN_ID
        INNER JOIN ClarityExtracts.dbo.FileExtractServiceAreas AS fsa
           ON fsa.serviceAreaId = pe.SERV_AREA_ID
        INNER JOIN ClarityExtracts.dbo.FileExtracts            AS ext
           ON ext.fileExtractId = fsa.fileExtractId
              AND ext.fileExtractName = @fileExtractName
        INNER JOIN Clarity.dbo.IDENTITY_ID                     AS ii
           ON ii.PAT_ID = om.PAT_ID
              AND ii.IDENTITY_TYPE_ID = fsa.serviceAreaId
    WHERE
        om.ORDER_STATUS_C <> 4 -- Cancelled
        AND om.ORDERING_DATE >= @startDate;

    CREATE INDEX ix_OrdPatID ON #ORDER_MED ( PAT_ID );
    CREATE INDEX ix_ordCSN ON #ORDER_MED ( PAT_ENC_CSN_ID );
    CREATE UNIQUE INDEX ix_ordmedID ON #ORDER_MED ( ORDER_MED_ID );
    CREATE INDEX ix_mID ON #ORDER_MED ( MEDICATION_ID );

    IF OBJECT_ID( 'tempdb..#PAT_ENC' ) IS NOT NULL
        DROP TABLE #PAT_ENC;

    -- For improved performance
    SELECT DISTINCT
        pe.PAT_ID
      , pe.PAT_ENC_CSN_ID
      , pe.DEPARTMENT_ID
      , pe.CONTACT_DATE
    INTO
        #PAT_ENC
    FROM
        Clarity.dbo.PAT_ENC   AS pe
        INNER JOIN #ORDER_MED AS om
           ON om.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID;

    CREATE UNIQUE INDEX ix_encCSN ON #PAT_ENC ( PAT_ENC_CSN_ID );

    DROP TABLE IF EXISTS #Final_Results;

    -- Final results
    SELECT
        om.PAT_ID
      , om.MRN
      , om.ORDER_MED_ID                              AS ORDER_ID
      , om.MED_PRESC_PROV_ID                         AS RX_PROV_ID
      , CONVERT( VARCHAR(8), om.ORDERING_DATE, 112 ) AS ORDER_DATE
      , os.NAME                                      AS ORDER_STATUS
      , REPLACE( om.SIG, '^', ' ' )                  AS SIG
      , REPLACE( ndc.NDC_CODE, '-', '' )             AS NDC_CODE
      , med.NAME                                     AS MEDICATION_NAME
      , om.QUANTITY
      , om.REFILLS
      , CONVERT( VARCHAR(8), om.START_DATE, 112 )    AS START_DATE
      , CONVERT( VARCHAR(8), om.END_DATE, 112 )      AS END_DATE
      , rt.NAME                                      AS ROUTE
      , om.HV_DISCRETE_DOSE + ' ' + mu.NAME          AS DOSE
      , '3'                                          AS PATIENT_CLASS
      , NULL                                         AS PAT_STATUS_HOSP
      , om.AUTHRZING_PROV_ID                         AS MANAGING_PROV_ID
      , pe.DEPARTMENT_ID                             AS FACILITY_PROV_ID
    INTO
        #Final_Results
    FROM
        #ORDER_MED                               AS om
        LEFT JOIN #PAT_ENC                       AS pe
          ON om.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID
        LEFT JOIN Clarity.dbo.ZC_ORDER_STATUS    AS os
          ON os.ORDER_STATUS_C = om.ORDER_STATUS_C
        LEFT JOIN Clarity.dbo.CLARITY_UCL        AS ucl
          ON om.ORDER_MED_ID = ucl.ORDER_ID
        LEFT JOIN Clarity.dbo.CLARITY_MEDICATION AS med
          ON med.MEDICATION_ID = om.MEDICATION_ID
        LEFT JOIN Clarity.dbo.ZC_ADMIN_ROUTE     AS rt
          ON om.MED_ROUTE_C = rt.MED_ROUTE_C
        LEFT JOIN Clarity.dbo.ZC_MED_UNIT        AS mu
          ON om.DOSE_UNIT_C = mu.DISP_QTYUNIT_C
        LEFT JOIN Clarity.dbo.RX_MED_NDC_CODE    AS ndc
          ON om.MEDICATION_ID = ndc.MEDICATION_ID
             AND ndc.LINE = 1;

    SELECT
        *
    FROM
        #Final_Results;

END;
GO

