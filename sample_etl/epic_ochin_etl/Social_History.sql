USE [ClarityExtracts]
GO

/****** Object:  StoredProcedure [dbo].[Assemble_ESP_Social_History]    Script Date: 2/11/2021 8:18:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Assemble_ESP_Social_History]
(
    @fileExtractName NVARCHAR(100)
  , @startDate DATETIME2(3)
  , @endDate DATETIME2(3)
)
AS
BEGIN

    SET NOCOUNT ON;
    /*----------------------------------------------------------------------
    Returns social history data required for the ESP social history extract
    ------------------------------------------------------------------------*/

    ----------------------- TESTING -----------------------------
    --DECLARE
    --    @startDate DATETIME2(3) = '01/01/2019'
    --  , @endDate   DATETIME2(3)
    --  , @SA        INT          = 133 -- Lynn
    -------------------------------------------------------------

    IF OBJECT_ID( 'tempdb..#Final_Results' ) IS NOT NULL
        DROP TABLE #Final_Results;

    SELECT
        soc.PAT_ID                                  AS PATIENT_ID
      , ii.IDENTITY_ID                              AS MRN
      , tob.NAME                                    AS TOBACCO_USE
      , alc.NAME                                    AS ALCOHOL_USE
      , CONVERT( VARCHAR(8), pe.CONTACT_DATE, 112 ) AS DATE_ENTERED
      , pe.PAT_ENC_CSN_ID                           AS SOCIAL_HX_ID
      , pe.VISIT_PROV_ID                            AS PROVIDER_ID
    INTO
        #Final_Results
    FROM
        Clarity.dbo.SOCIAL_HX                                  AS soc
        INNER JOIN Clarity.dbo.PAT_ENC                         AS pe
           ON pe.PAT_ENC_CSN_ID = soc.PAT_ENC_CSN_ID
        INNER JOIN ClarityExtracts.dbo.FileExtractServiceAreas AS fsa
           ON fsa.serviceAreaId = pe.SERV_AREA_ID
        INNER JOIN ClarityExtracts.dbo.FileExtracts            AS ext
           ON ext.fileExtractId = fsa.fileExtractId
              AND ext.fileExtractName = @fileExtractName
        INNER JOIN Clarity.dbo.IDENTITY_ID                     AS ii
           ON ii.PAT_ID = pe.PAT_ID
              AND ii.IDENTITY_TYPE_ID = pe.SERV_AREA_ID
        LEFT JOIN Clarity.dbo.ZC_TOBACCO_USER                  AS tob
          ON tob.TOBACCO_USER_C = soc.TOBACCO_USER_C
        LEFT JOIN Clarity.dbo.ZC_ALCOHOL_USE                   AS alc
          ON alc.ALCOHOL_USE_C = soc.ALCOHOL_USE_C
    WHERE
        (   tob.NAME IS NOT NULL
            OR alc.NAME IS NOT NULL ) -- don't include rows that don't have either a tobacco or alcohol use status
        AND pe.CONTACT_DATE >= @startDate;

    SELECT
        *
    FROM
        #Final_Results;

END;
GO

