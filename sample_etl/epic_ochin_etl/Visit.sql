USE [ClarityExtracts]
GO

/****** Object:  StoredProcedure [dbo].[Assemble_ESP_Visit]    Script Date: 2/11/2021 8:19:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Assemble_ESP_Visit]
(
    @fileExtractName NVARCHAR(100)
  , @startDate DATETIME2(3)
  , @endDate DATETIME2(3)
)
AS
BEGIN

    SET NOCOUNT ON;
    /*------------------------------------------------------------------
    Returns patient encounter data required for the ESP visit extract
    --------------------------------------------------------------------*/

    DECLARE
        @csn NUMERIC(18, 0)
      , @code VARCHAR(25)
      , @last_csn NUMERIC(18, 0)
      , @code_str VARCHAR(1000)
      , @Insert BIT;

    ----------------------- TESTING -----------------------------
    --DECLARE
    --    @startDate DATETIME2(3) = '01/01/2019'
    --  , @endDate   DATETIME2(3)
    --  , @SA        INT          = 133 -- Lynn
    -------------------------------------------------------------

    IF OBJECT_ID( 'tempdb..#Pat_Enc' ) IS NOT NULL
        DROP TABLE #Pat_Enc;

    SELECT
        ii.IDENTITY_ID AS MRN
      , pe.*
    INTO
        #Pat_Enc
    FROM
        Clarity.dbo.PAT_ENC                    AS pe
        INNER JOIN dbo.FileExtractServiceAreas AS fsa
           ON fsa.serviceAreaId = pe.SERV_AREA_ID
        INNER JOIN dbo.FileExtracts            AS ext
           ON ext.fileExtractId = fsa.fileExtractId
              AND ext.fileExtractName = @fileExtractName
        INNER JOIN Clarity.dbo.IDENTITY_ID     AS ii
           ON ii.PAT_ID = pe.PAT_ID
              AND ii.IDENTITY_TYPE_ID = pe.SERV_AREA_ID
    WHERE
        pe.CONTACT_DATE >= @startDate
		AND pe.CONTACT_DATE < @endDate
        AND
          ( --Include only valid appointments (2-Completed, 6-Arrived, 8-Non Encounter Visit, 9-Non-OCHIN Complete)
              pe.APPT_STATUS_C IN ( '2', '6', '8', '9' )
              OR pe.APPT_STATUS_C IS NULL );

    CREATE INDEX ix_EncPatID ON #Pat_Enc ( PAT_ID );
    CREATE UNIQUE INDEX ix_peCSN ON #Pat_Enc ( PAT_ENC_CSN_ID );

    IF OBJECT_ID( 'tempdb..#Patient' ) IS NOT NULL
        DROP TABLE #Patient;

    -- get distinct patients
    SELECT DISTINCT
        pa.PAT_ID
      , pa.SEX_C
    INTO
        #Patient
    FROM
        Clarity.dbo.PATIENT AS pa
        INNER JOIN #Pat_Enc AS pe
           ON pe.PAT_ID = pa.PAT_ID;

    CREATE UNIQUE INDEX ix_PatID ON #Patient ( PAT_ID );

    IF OBJECT_ID( 'tempdb..#Preg_codes' ) IS NOT NULL
        DROP TABLE #Preg_codes;

    -- pregnancy dx codes
    SELECT
        CASE
             WHEN gcrl.COMPILED_CONTEXT = 'EDG'
                 THEN gcrl.GROUPER_RECORDS_NUMERIC_ID
             ELSE NULL
        END AS DX_ID
    INTO
        #Preg_codes
    FROM
        Clarity.dbo.GROUPER_COMPILED_REC_LIST AS gcrl
    WHERE
        gcrl.COMPILED_GROUPER_ID IN (
            '101420' );

    IF OBJECT_ID( 'tempdb..#Preg_patients' ) IS NOT NULL
        DROP TABLE #Preg_patients;


    /* Pregnancy in Visit Diagnosis */
    SELECT
        pa.PAT_ID
      , pe.PAT_ENC_CSN_ID
      , pa.EDD_DT
      , 'Yes' AS Pregnant
    INTO
        #Preg_patients
    FROM
        Clarity.dbo.PATIENT               AS pa
        INNER JOIN #Pat_Enc               AS pe
           ON pe.PAT_ID = pa.PAT_ID
        INNER JOIN Clarity.dbo.PAT_ENC_DX AS dx
           ON dx.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID
        INNER JOIN #Preg_codes            AS preg
           ON dx.DX_ID = preg.DX_ID
    WHERE
        pa.EDD_DT IS NOT NULL
    /*Pregnancy in LMP*/
    UNION
    SELECT
        pa.PAT_ID
      , pe.PAT_ENC_CSN_ID
      , pa.EDD_DT
      , 'Yes' AS Pregnant
    FROM
        Clarity.dbo.PATIENT            AS pa
        INNER JOIN Clarity.dbo.PAT_ENC AS pe
           ON pa.PAT_ID = pe.PAT_ID
    WHERE
        pe.LMP_OTHER_C = 4; --Pregnant

    IF OBJECT_ID( 'tempdb..#ICD' ) IS NOT NULL
        DROP TABLE #ICD;

    IF OBJECT_ID( 'tempdb..#ICD_STR' ) IS NOT NULL
        DROP TABLE #ICD_STR;

    -- get all DX codes by encounter

    CREATE TABLE #ICD_STR
    (
        PAT_ENC_CSN_ID NUMERIC(18, 0)
      , ICD_STR VARCHAR(1000)
    );

    SELECT DISTINCT
        pe.PAT_ENC_CSN_ID
      , icd.CODE
    INTO
        #ICD
    FROM
        #Pat_Enc                                 AS pe
        INNER JOIN Clarity.dbo.PAT_ENC_DX        AS dx
           ON dx.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID
        INNER JOIN Clarity.dbo.EDG_CURRENT_ICD10 AS icd
           ON icd.DX_ID = dx.DX_ID;

    DECLARE CURSOR_icd CURSOR FOR
        SELECT
            PAT_ENC_CSN_ID
          , CODE
        FROM
            #ICD
        ORDER BY
            PAT_ENC_CSN_ID;

    OPEN CURSOR_icd;
    FETCH NEXT FROM CURSOR_icd
    INTO
        @csn
      , @code;

    SET @code_str = 'icd10:' + @code;
    SET @last_csn = @csn;
    SET @Insert = 0;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        FETCH NEXT FROM CURSOR_icd
        INTO
            @csn
          , @code;

        IF @csn = @last_csn
            SET @code_str = @code_str + ';icd10:' + @code;
        ELSE
            SET @Insert = 1;

        IF @Insert = 1
        BEGIN
            INSERT INTO
                #ICD_STR
            VALUES
            (
                @last_csn, @code_str
            );

            SET @code_str = 'icd10:' + @code;
        END;

        SET @last_csn = @csn;
        SET @Insert = 0;
    END;

    CREATE UNIQUE INDEX ix_CSN ON #ICD_STR ( PAT_ENC_CSN_ID );

    CLOSE CURSOR_icd;
    DEALLOCATE CURSOR_icd;

    IF OBJECT_ID( 'tempdb..#Final_Results' ) IS NOT NULL
        DROP TABLE #Final_Results;

    -- Final results
    SELECT
        pe.PAT_ID
      , pe.MRN
      , pe.PAT_ENC_CSN_ID
      , CONVERT( VARCHAR(8), pe.CONTACT_DATE, 112 )                              AS CONTACT_DATE
      , CASE
             WHEN pe.ENC_CLOSED_YN = 'Y'
                 THEN 'Yes'
             WHEN pe.ENC_CLOSED_YN = 'N'
                 THEN 'No'
             ELSE NULL
        END                                                                      AS ENC_CLOSED
      , CONVERT( VARCHAR(8), pe.ENC_CLOSE_DATE, 112 )                            AS ENC_CLOSE_DATE
      , pe.VISIT_PROV_ID
      , pe.DEPARTMENT_ID
      , dep.EXTERNAL_NAME                                                        AS DEPARTMENT_NAME
      , enc_type.NAME                                                            AS ENC_TYPE
      , CASE
             WHEN pp.EDD_DT IS NOT NULL
                 THEN CONVERT( VARCHAR(8), pp.EDD_DT, 112 )
             WHEN pp.EDD_DT IS NULL
                  AND pp.Pregnant = 'Yes'
                 THEN 'Yes'
             WHEN pa.SEX_C = '1'
                 THEN -- female and not pregnant
      'No'
             ELSE NULL
        END                                                                      AS EDD -- expected date of delivery
      , pe.TEMPERATURE
      , eap.PROC_CODE                                                            AS CPT
      , CONVERT( VARCHAR(10), CONVERT( INT, ROUND( pe.WEIGHT, 0 )) / 16 ) + ' lb '
        + CONVERT( VARCHAR(10), CONVERT( INT, ROUND( WEIGHT, 0 )) % 16 ) + ' oz' AS WEIGHT
      , pe.HEIGHT
      , pe.BP_SYSTOLIC
      , pe.BP_DIASTOLIC
      , pe2.PHYS_SPO2                                                            AS SPO2
      , pe2.PHYS_PEAK_FLOW                                                       AS PEAK_FLOW
      , icd.ICD_STR                                                              AS DX_CODES
      , pe.BMI
      , NULL                                                                     AS Hosp_Adm_Date
      , NULL                                                                     AS Hosp_Disch_Date
      , NULL                                                                     AS Sex_Risk_Scr
      , NULL                                                                     AS Drug_Scr
      , NULL                                                                     AS Referral_Date
      , NULL                                                                     AS Pt_Referred
      , NULL                                                                     AS Risk_Cnsl_Date
      , NULL                                                                     AS HIV_Visit
      , NULL                                                                     AS Service_Line
    INTO
        #Final_Results
    FROM
        #Pat_Enc                               AS pe
        INNER JOIN #Patient                    AS pa
           ON pa.PAT_ID = pe.PAT_ID
        LEFT JOIN Clarity.dbo.CLARITY_DEP      AS dep
          ON dep.DEPARTMENT_ID = pe.DEPARTMENT_ID
        LEFT JOIN Clarity.dbo.ZC_DISP_ENC_TYPE AS enc_type
          ON pe.ENC_TYPE_C = enc_type.DISP_ENC_TYPE_C
        LEFT JOIN #Preg_patients               AS pp
          ON pp.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID
        LEFT JOIN Clarity.dbo.CLARITY_EAP      AS eap
          ON pe.LOS_PRIME_PROC_ID = eap.PROC_ID
        LEFT JOIN Clarity.dbo.PAT_ENC_2        AS pe2
          ON pe2.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID
        LEFT JOIN #ICD_STR                     AS icd
          ON icd.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID;

    SELECT
        *
    FROM
        #Final_Results;

END;
GO

