USE [ClarityExtracts]
GO

/****** Object:  StoredProcedure [dbo].[Assemble_ESP_Immunization]    Script Date: 2/11/2021 8:11:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Assemble_ESP_Immunization]
(
    @fileExtractName NVARCHAR(100)
  , @startDate DATETIME2(3)
  , @endDate DATETIME2(3)
)
AS
BEGIN

    SET NOCOUNT ON;
    /*----------------------------------------------------------------------
    Returns immunizations data required for the ESP immunizations extract
    ------------------------------------------------------------------------*/

    ----------------------- TESTING -----------------------------
    --DECLARE
    --    @startDate DATETIME2(3) = '01/01/2019'
    --  , @endDate   DATETIME2(3)
    --  , @SA        INT          = 133 -- Lynn
    -------------------------------------------------------------

    IF OBJECT_ID( 'tempdb..#Final_Results' ) IS NOT NULL
        DROP TABLE #Final_Results;

    SELECT
        imm.PAT_ID                                  AS PATIENT_ID
      , imm.IMMUNZATN_ID                            AS IMM_TYPE
      , COALESCE( eap.PROC_NAME, cimm.NAME )        AS IMM_NAME -- proc name is more specific
      , CONVERT( VARCHAR(8), imm.IMMUNE_DATE, 112 ) AS IMM_DATE
      , imm.DOSE
      , mfg.NAME                                    AS MANUFACTURER
      , imm.LOT
      , imm.IMMUNE_ID
      , ii.IDENTITY_ID                              AS MRN
      , emp.PROV_ID                                 AS PROVIDER_ID
      , CONVERT( VARCHAR(8), pe.CONTACT_DATE, 112 ) AS VISIT_DATE
      , immstat.NAME                                AS IMM_STATUS
      , eap.PROC_CODE                               AS CPT
      , NULL                                        AS PATIENT_CLASS
      , NULL                                        AS PATIENT_STATUS
    INTO
        #Final_Results
    FROM
        Clarity.dbo.IMMUNE                                     AS imm
        INNER JOIN Clarity.dbo.PAT_ENC                         AS pe
           ON pe.PAT_ENC_CSN_ID = imm.IMM_CSN
        INNER JOIN ClarityExtracts.dbo.FileExtractServiceAreas AS fsa
           ON fsa.serviceAreaId = pe.SERV_AREA_ID
        INNER JOIN ClarityExtracts.dbo.FileExtracts            AS ext
           ON ext.fileExtractId = fsa.fileExtractId
              AND ext.fileExtractName = @fileExtractName
        INNER JOIN Clarity.dbo.IDENTITY_ID                     AS ii
           ON ii.PAT_ID = pe.PAT_ID
              AND ii.IDENTITY_TYPE_ID = fsa.serviceAreaId
        LEFT JOIN Clarity.dbo.CLARITY_IMMUNZATN                AS cimm
          ON cimm.IMMUNZATN_ID = imm.IMMUNZATN_ID
        LEFT JOIN Clarity.dbo.ZC_MFG                           AS mfg
          ON mfg.MFG_C = imm.MFG_C
        LEFT JOIN Clarity.dbo.CLARITY_EMP                      AS emp
          ON imm.GIVEN_BY_USER_ID = emp.USER_ID
        LEFT JOIN Clarity.dbo.ZC_IMMNZTN_STATUS                AS immstat
          ON immstat.IMMNZTN_STATUS_C = imm.IMMNZTN_STATUS_C
        LEFT JOIN Clarity.dbo.ORDER_PROC                       AS op
          ON imm.ORDER_ID = op.ORDER_PROC_ID
        LEFT JOIN Clarity.dbo.CLARITY_EAP                      AS eap
          ON op.PROC_ID = eap.PROC_ID
    WHERE
        imm.IMMUNE_DATE >= @startDate;

    SELECT
        *
    FROM
        #Final_Results;
END;
GO

