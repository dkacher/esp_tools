USE [ClarityExtracts]
GO

/****** Object:  StoredProcedure [dbo].[Assemble_ESP_Lab_Results]    Script Date: 2/11/2021 8:12:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Assemble_ESP_Lab_Results]
(
    @fileExtractName NVARCHAR(100)
  , @startDate DATETIME2(3)
  , @endDate DATETIME2(3)
)
AS
BEGIN

    SET NOCOUNT ON;
    /*----------------------------------------------------------------------
    Returns lab results data required for the ESP results extract
    ------------------------------------------------------------------------*/

    ----------------------- TESTING -----------------------------
    --DECLARE
    --    @startDate DATETIME2(3) = '01/01/2019'
    --  , @endDate   DATETIME2(3)
    --  , @SA        INT          = 133 -- Lynn
    -------------------------------------------------------------

    DROP TABLE IF EXISTS #Order_Proc;

    SELECT
        ii.IDENTITY_ID AS MRN
      , pe.DEPARTMENT_ID
      , op.*
    INTO
        #Order_Proc
    FROM
        Clarity.dbo.ORDER_PROC                                 AS op
        INNER JOIN Clarity.dbo.PAT_ENC                         AS pe
           ON pe.PAT_ENC_CSN_ID = op.PAT_ENC_CSN_ID
        INNER JOIN ClarityExtracts.dbo.FileExtractServiceAreas AS fsa
           ON fsa.serviceAreaId = pe.SERV_AREA_ID
        INNER JOIN ClarityExtracts.dbo.FileExtracts            AS ext
           ON ext.fileExtractId = fsa.fileExtractId
              AND ext.fileExtractName = @fileExtractName
        INNER JOIN Clarity.dbo.IDENTITY_ID                     AS ii
           ON ii.PAT_ID = pe.PAT_ID
              AND ii.IDENTITY_TYPE_ID = fsa.serviceAreaId
    WHERE
        op.ORDER_STATUS_C NOT IN (
            1, 4 ) -- don't include pending or cancelled orders
        AND op.ORDERING_DATE >= @startDate;

    CREATE INDEX ix_OrdPatID ON #Order_Proc ( PAT_ID );
    CREATE UNIQUE INDEX ix_ordProcID ON #Order_Proc ( ORDER_PROC_ID );

    DROP TABLE IF EXISTS #Order_Results;

    SELECT
        res.*
    INTO
        #Order_Results
    FROM
        Clarity.dbo.ORDER_RESULTS AS res
        INNER JOIN #Order_Proc    AS op
           ON op.ORDER_PROC_ID = res.ORDER_PROC_ID
    WHERE
        res.RESULT_STATUS_C IN (
            3, 4 ); -- final or corrected

    CREATE INDEX ix_opid ON #Order_Results ( ORDER_PROC_ID );

    DROP TABLE IF EXISTS #Final_Results;

    -- Final results
    SELECT
        op.PAT_ID
      , op.MRN
      , op.ORDER_PROC_ID                                   AS LAB_ORDER_ID
      , CONVERT( VARCHAR(8), op.ORDERING_DATE, 112 )       AS ORDERING_DATE
      , CONVERT( VARCHAR(8), res.RESULT_DATE, 112 )        AS RESULT_DATE
      , op.AUTHRZING_PROV_ID                               AS ORDERING_PROVIDER_ID
      , ot.NAME                                            AS ORDER_TYPE
      , eap.PROC_CODE                                      AS CPT
      , res.COMPONENT_ID
      , cc.NAME                                            AS TEST_NAME
      , res.ORD_VALUE                                      AS RESULT
      , LEFT(rf.NAME, 20)                                  AS RESULT_FLAG
      , res.REFERENCE_LOW
      , res.REFERENCE_HIGH
      , REPLACE( res.REFERENCE_UNIT, '^', ' ' )            AS REFERENCE_UNIT
      , rs.NAME                                            AS RESULT_STATUS
      , REPLACE( res.COMPONENT_COMMENT, '^', ' ' )         AS COMPONENT_COMMENT
      , NULL                                               AS SPECIMEN_ID
      , REPLACE( res.COMPONENT_COMMENT, '^', ' ' )         AS IMPRESSION_COMMENT
      , ss.NAME                                            AS SPECIMEN_SRC
      , op2.SPECIMN_TAKEN_DATE                             AS COLLECTION_DATE
      , eap.PROC_NAME
      , CONVERT( VARCHAR(20), res.ORDER_PROC_ID ) + CONVERT( VARCHAR(20), res.ORD_DATE_REAL )
        + CONVERT( VARCHAR(3), res.LINE )                  AS LAB_RESULT_ID
      , '3'                                                AS PATIENT_CLASS
      , NULL                                               AS PATIENT_STATUS
      , poi.PERFORMING_ORG_CLIA_NUM                        AS CLIA_NUM
      , CONVERT( VARCHAR(8), op2.SPECIMEN_RECV_DATE, 112 ) AS COLLECTION_END_DATE
      , NULL                                               AS STATUS_DATE
      , NULL                                               AS INTERPRETER
      , NULL                                               AS INTERPRETER_ID
      , NULL                                               AS INTERPRETER_ID_TYPE
      , NULL                                               AS INTERPRETER_UID
      , NULL                                               AS OBS_METHOD
      , NULL                                               AS REF_TEXT
      , op.DEPARTMENT_ID
    INTO
        #Final_Results
    FROM
        #Order_Proc                               AS op
        INNER JOIN #Order_Results                 AS res
           ON res.ORDER_PROC_ID = op.ORDER_PROC_ID
        LEFT JOIN Clarity.dbo.ORDER_PROC_2        AS op2
          ON op2.ORDER_PROC_ID = op.ORDER_PROC_ID
        LEFT JOIN Clarity.dbo.ZC_ORDER_TYPE       AS ot
          ON ot.ORDER_TYPE_C = op.ORDER_TYPE_C
        LEFT JOIN Clarity.dbo.CLARITY_EAP         AS eap
          ON eap.PROC_ID = op.PROC_ID
        LEFT JOIN Clarity.dbo.ZC_RESULT_FLAG      AS rf
          ON rf.RESULT_FLAG_C = res.RESULT_FLAG_C
        LEFT JOIN Clarity.dbo.ZC_RESULT_STATUS    AS rs
          ON rs.RESULT_STATUS_C = res.RESULT_STATUS_C
        LEFT JOIN Clarity.dbo.ZC_SPECIMEN_SOURCE  AS ss
          ON ss.SPECIMEN_SOURCE_C = op.SPECIMEN_SOURCE_C
        LEFT JOIN Clarity.dbo.PERFORMING_ORG_INFO AS poi
          ON op.ORDER_PROC_ID = poi.ORDER_ID
        LEFT JOIN Clarity.dbo.CLARITY_COMPONENT   AS cc
          ON cc.COMPONENT_ID = res.COMPONENT_ID;

    SELECT
        *
    FROM
        #Final_Results;

END;
GO

