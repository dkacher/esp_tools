-- Create extract of all encounter records that have updated info within x days 
-- prior to the startDate passed in to the script and prior to the endDate passed in to the script.

-- Typically startDate would be "yesterday" and endDate would be "today"

-- Modify as needed to meet your requirements. 
-- Some sites may wish to increase the number of "lookback" days 
-- to allow for automatic recovery if in extract does not run for a day.
-- It is set to "0" for NO lookback, but it is recommended to set this to 5 days if feasible.


USE [clarity_production_temp]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[X_ESP_XTR_Assemble_ESP_Visit]
(
  @startDate DATETIME2(3)
  , @endDate DATETIME2(3)
)
AS
BEGIN

    SET NOCOUNT ON;
    /*------------------------------------------------------------------
    Returns patient encounter data required for the ESP visit extract
    --------------------------------------------------------------------*/

    DECLARE
        @csn NUMERIC(18, 0)
      , @code VARCHAR(25)
      , @last_csn NUMERIC(18, 0)
      , @code_str VARCHAR(1000)
      , @Insert BIT;


    DROP TABLE IF EXISTS #X_ESP_XTR_Pat_Enc_VIS;

    SELECT
        pa.Pat_MRN_ID AS MRN
      , pe.*
    INTO
        #X_ESP_XTR_Pat_Enc_VIS
    FROM
        Clarity.dbo.PAT_ENC                    AS pe
        INNER JOIN Clarity.dbo.PATIENT         AS pa
              ON pe.Pat_ID = pa.Pat_ID
    WHERE
        pe.CONTACT_DATE < @endDate 
        
        -- SITES CAN ADJUST "LOOKBACK" DAYS HERE
        AND ( pe.CONTACT_DATE >=  DATEADD ( DAY, -0, @startDate)
                 OR
              pe.UPDATE_DATE >=  DATEADD ( DAY, -0, @startDate) )
              
        -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE  
        -- Exclude specific appointment types  (3-Canceled, 4-No Show, 5-Left Without Seen, 12-Provider Cancelled)
        AND    ( pe.APPT_STATUS_C NOT IN ('3', '4', '5', '12') OR pe.APPT_STATUS_C IS NULL )
  
        -- only process updates for visits that happened in the week prior to the start date
        -- this prevents overloading system with updates but ensures recently updated records are captured
        AND pe.CONTACT_DATE >= DATEADD(DAY, -7, @startDate);

              

    CREATE INDEX ix_EncPatID ON #X_ESP_XTR_Pat_Enc_VIS ( PAT_ID );
    CREATE UNIQUE INDEX ix_peCSN ON #X_ESP_XTR_Pat_Enc_VIS ( PAT_ENC_CSN_ID );

    DROP TABLE IF EXISTS #X_ESP_XTR_Patient_VIS;

    -- get distinct patients
    SELECT DISTINCT
        pa.PAT_ID
      , pa.SEX_C
      , pa.EDD_DT
    INTO
        #X_ESP_XTR_Patient_VIS
    FROM
        Clarity.dbo.PATIENT AS pa
        INNER JOIN #X_ESP_XTR_Pat_Enc_VIS AS pe
           ON pe.PAT_ID = pa.PAT_ID;

    CREATE UNIQUE INDEX ix_PatID ON #X_ESP_XTR_Patient_VIS ( PAT_ID );


    DROP TABLE IF EXISTS #X_ESP_XTR_ICD;

    DROP TABLE IF EXISTS #X_ESP_XTR_ICD_STR;

    -- get all DX codes by encounter

    CREATE TABLE #X_ESP_XTR_ICD_STR
    (
        PAT_ENC_CSN_ID NUMERIC(18, 0)
      , ICD_STR VARCHAR(1000)
    );

    SELECT DISTINCT
        pe.PAT_ENC_CSN_ID
      , icd.CODE
    INTO
        #X_ESP_XTR_ICD
    FROM
        #X_ESP_XTR_Pat_Enc_VIS                   AS pe
        INNER JOIN Clarity.dbo.PAT_ENC_DX        AS dx
           ON dx.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID
        INNER JOIN Clarity.dbo.EDG_CURRENT_ICD10 AS icd
           ON icd.DX_ID = dx.DX_ID;

    DECLARE CURSOR_icd CURSOR FOR
        SELECT
            PAT_ENC_CSN_ID
          , CODE
        FROM
            #X_ESP_XTR_ICD
        ORDER BY
            PAT_ENC_CSN_ID;

    OPEN CURSOR_icd;
    FETCH NEXT FROM CURSOR_icd
    INTO
        @csn
      , @code;

    SET @code_str = 'icd10:' + @code;
    SET @last_csn = @csn;
    SET @Insert = 0;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        FETCH NEXT FROM CURSOR_icd
        INTO
            @csn
          , @code;

        IF @csn = @last_csn
            SET @code_str = @code_str + ';icd10:' + @code;
        ELSE
            SET @Insert = 1;

        IF @Insert = 1
        BEGIN
            INSERT INTO
                #X_ESP_XTR_ICD_STR
            VALUES
            (
                @last_csn, @code_str
            );

            SET @code_str = 'icd10:' + @code;
        END;

        SET @last_csn = @csn;
        SET @Insert = 0;
    END;

    CREATE UNIQUE INDEX ix_CSN ON #X_ESP_XTR_ICD_STR ( PAT_ENC_CSN_ID );

    CLOSE CURSOR_icd;
    DEALLOCATE CURSOR_icd;

    DROP TABLE IF EXISTS #X_ESP_XTR_Final_Results_VIS;

    -- Final results
    SELECT DISTINCT
        pe.PAT_ID
      , pe.MRN
      , pe.PAT_ENC_CSN_ID
      , CONVERT( VARCHAR(8), pe.CONTACT_DATE, 112 )                              AS CONTACT_DATE
      , pe.ENC_CLOSED_YN                                                         AS ENC_CLOSED
      , CONVERT( VARCHAR(8), pe.ENC_CLOSE_DATE, 112 )                            AS ENC_CLOSE_DATE
      , pe.VISIT_PROV_ID
      , pe.DEPARTMENT_ID
      , dep.EXTERNAL_NAME                                                        AS DEPARTMENT_NAME
      , enc_type.NAME                                                            AS ENC_TYPE
      , pa.EDD_DT                                                     
      , pe.TEMPERATURE
      , eap.PROC_CODE                                                            AS CPT
      , CONVERT( VARCHAR(10), CONVERT( INT, ROUND( pe.WEIGHT, 0 )) / 16 ) + ' lb '
        + CONVERT( VARCHAR(10), CONVERT( INT, ROUND( WEIGHT, 0 )) % 16 ) + ' oz' AS WEIGHT
      , pe.HEIGHT
      , pe.BP_SYSTOLIC
      , pe.BP_DIASTOLIC
      , pe2.PHYS_SPO2                                                            AS SPO2
      , pe2.PHYS_PEAK_FLOW                                                       AS PEAK_FLOW
      , icd.ICD_STR                                                              AS DX_CODES
      , pe.BMI
      , pe.HOSP_ADMSN_TIME                                                       AS HOSP_ADM_DATETIME
      , pe.HOSP_DISCHRG_TIME                                                     AS HOSP_DISCH_DATETIME
      , NULL                                                                     AS PRIMARY_PAYER
    INTO
        #X_ESP_XTR_Final_Results_VIS
    FROM
        #X_ESP_XTR_Pat_Enc_VIS                           AS pe
        INNER JOIN #X_ESP_XTR_Patient_VIS                AS pa
           ON pa.PAT_ID = pe.PAT_ID
        LEFT JOIN Clarity.dbo.CLARITY_DEP      AS dep
          ON dep.DEPARTMENT_ID = pe.DEPARTMENT_ID
        LEFT JOIN Clarity.dbo.ZC_DISP_ENC_TYPE AS enc_type
          ON pe.ENC_TYPE_C = enc_type.DISP_ENC_TYPE_C
        LEFT JOIN Clarity.dbo.CLARITY_EAP      AS eap
          ON pe.LOS_PRIME_PROC_ID = eap.PROC_ID
        LEFT JOIN Clarity.dbo.PAT_ENC_2        AS pe2
          ON pe2.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID
        LEFT JOIN #X_ESP_XTR_ICD_STR                     AS icd
          ON icd.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID;

    SELECT
        *
    FROM
        #X_ESP_XTR_Final_Results_VIS;

END;
GO

