-- Create extract of all medicacation records that have updated info within x days 
-- prior to the startDate passed in to the script and prior to the endDate passed in to the script.

-- Typically startDate would be "yesterday" and endDate would be "today"

-- Modify as needed to meet your requirements. 
-- Some sites may wish to increase the number of "lookback" days 
-- to allow for automatic recovery if in extract does not run for a day.
-- It is set to "0" for NO lookback, but it is recommended to set this to 5 days if feasible.


USE [clarity_production_temp]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[X_ESP_XTR_Assemble_ESP_Medication]( @startDate DATETIME2(3), @endDate DATETIME2(3) )
AS
BEGIN

    SET NOCOUNT ON;
    /*----------------------------------------------------------------------
    Returns patient medication data required for the ESP medication extract
    ------------------------------------------------------------------------*/

    DROP TABLE IF EXISTS #X_ESP_XTR_ORDER_MED;

    -- For improved performance
    -- Gather up all of the meds to process
    SELECT
      pa.PAT_MRN_ID AS MRN
     , om.*
    INTO
        #X_ESP_XTR_ORDER_MED
    FROM
        Clarity.dbo.ORDER_MED                                  AS om
        INNER JOIN Clarity.dbo.PAT_ENC                         AS pe
           ON pe.PAT_ENC_CSN_ID = om.PAT_ENC_CSN_ID
        INNER JOIN Clarity.dbo.PATIENT                         AS pa
          ON om.PAT_ID = pa.PAT_ID                            
    WHERE
         -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
        (om.ORDER_STATUS_C <> 4 or om.ORDER_STATUS_C is null) -- Exclude Cancelled
        -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
        AND (om.ORDER_CLASS_C <>3 or om.ORDER_CLASS_C is null) -- Exclude Historical
        -- Meds can be entered after order date so use the update date
        AND om.UPDATE_DATE < @endDate
        -- SITES CAN ADJUST "LOOKBACK" DAYS HERE
        AND om.UPDATE_DATE >= DATEADD ( DAY, -0, @startDate) 
        -- ONLY PROCESS UPDATES FOR MEDS ORDERED IN THE LAST 12 MONTHS
        AND (om.ORDERING_DATE >= DATEADD(month,-12,getdate()) or om.ORDER_INST >= DATEADD(month,-12,getdate()) );

    CREATE INDEX ix_OrdPatID ON #X_ESP_XTR_ORDER_MED ( PAT_ID );
    CREATE INDEX ix_ordCSN ON #X_ESP_XTR_ORDER_MED ( PAT_ENC_CSN_ID );
    CREATE UNIQUE INDEX ix_ordmedID ON #X_ESP_XTR_ORDER_MED ( ORDER_MED_ID );
    CREATE INDEX ix_mID ON #X_ESP_XTR_ORDER_MED ( MEDICATION_ID );

    DROP TABLE IF EXISTS #X_ESP_XTR_PAT_ENC_MED;

    -- For improved performance
    SELECT DISTINCT
        pe.PAT_ID
      , pe.PAT_ENC_CSN_ID
      , pe.DEPARTMENT_ID
      , pe.CONTACT_DATE
    INTO
        #X_ESP_XTR_PAT_ENC_MED
    FROM
        Clarity.dbo.PAT_ENC   AS pe
        INNER JOIN #X_ESP_XTR_ORDER_MED AS om
           ON om.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID;

    CREATE UNIQUE INDEX ix_encCSN ON #X_ESP_XTR_PAT_ENC_MED ( PAT_ENC_CSN_ID );

    DROP TABLE IF EXISTS #X_ESP_XTR_Final_Results_MED;

    -- Final results
    SELECT DISTINCT
        om.PAT_ID
      , om.MRN
      , om.ORDER_MED_ID                              AS ORDER_ID
      , om.MED_PRESC_PROV_ID                         AS RX_PROV_ID
      , ISNULL (CONVERT( VARCHAR(8), om.ORDERING_DATE, 112 ), CONVERT( VARCHAR(8), om.ORDER_INST, 112 ))  AS ORDER_DATE
      , os.TITLE                                     AS ORDER_STATUS
      , REPLACE( om.SIG, '^', ' ' )                  AS DIRECTIONS
      , REPLACE( ndc.NDC_CODE, '-', '' )             AS NDC_CODE
      , med.NAME                                     AS MEDICATION_NAME
      , om.QUANTITY
      , om.REFILLS
      , CONVERT( VARCHAR(8), om.START_DATE, 112 )    AS START_DATE
      , CONVERT( VARCHAR(8), om.END_DATE, 112 )      AS END_DATE
      , rt.NAME                                      AS ROUTE
      , om.HV_DISCRETE_DOSE + ' ' + mu.NAME          AS DOSE
      , CASE
            WHEN om.ORDER_CLASS_C is not null
            THEN CONVERT(varchar,om.ORDERING_MODE_C) + '.' + convert(varchar,om.ORDER_CLASS_C)
            ELSE convert(varchar,om.ORDERING_MODE_C)
        END                                          AS PATIENT_CLASS
      , NULL                                         AS PAT_STATUS_HOSP
      , om.AUTHRZING_PROV_ID                         AS MANAGING_PROV_ID
      -- Add a preceding "D" to designate departments/facilities
      -- Provider Extract must also include preceding "D"
      , concat('D', coalesce(om.PAT_LOC_ID, pe.DEPARTMENT_ID))    AS FACILITY_PROV_ID
    INTO
        #X_ESP_XTR_Final_Results_MED
    FROM
        #X_ESP_XTR_ORDER_MED                     AS om
        LEFT JOIN #X_ESP_XTR_PAT_ENC_MED         AS pe
          ON om.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID
        LEFT JOIN Clarity.dbo.ZC_ORDER_STATUS    AS os
          ON os.ORDER_STATUS_C = om.ORDER_STATUS_C
        LEFT JOIN Clarity.dbo.CLARITY_UCL        AS ucl
          ON om.ORDER_MED_ID = ucl.ORDER_ID
        LEFT JOIN Clarity.dbo.CLARITY_MEDICATION AS med
          ON med.MEDICATION_ID = om.MEDICATION_ID
        LEFT JOIN Clarity.dbo.ZC_ADMIN_ROUTE     AS rt
          ON om.MED_ROUTE_C = rt.MED_ROUTE_C
        LEFT JOIN Clarity.dbo.ZC_MED_UNIT        AS mu
          ON om.DOSE_UNIT_C = mu.DISP_QTYUNIT_C
        LEFT JOIN Clarity.dbo.RX_MED_NDC_CODE    AS ndc
          ON om.MEDICATION_ID = ndc.MEDICATION_ID
             AND ndc.LINE = 1;

    SELECT
        *
    FROM
        #X_ESP_XTR_Final_Results_MED;

END;
GO

