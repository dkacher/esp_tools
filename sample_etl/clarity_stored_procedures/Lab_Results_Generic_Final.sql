-- Create extract of all lab result records that have updated info within x days 
-- prior to the startDate passed in to the script and prior to the endDate passed in to the script.

-- Typically startDate would be "yesterday" and endDate would be "today"

-- Modify as needed to meet your requirements. 
-- Some sites may wish to increase the number of "lookback" days 
-- to allow for automatic recovery if in extract does not run for a day.
-- It is set to "0" for NO lookback, but it is recommended to set this to 5 days if feasible.

USE [clarity_production_temp]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[X_ESP_XTR_Assemble_ESP_Lab_Results]
(
  @startDate DATETIME2(3)
  , @endDate DATETIME2(3)
)
AS
BEGIN

    SET NOCOUNT ON;
    /*----------------------------------------------------------------------
    Returns lab results data required for the ESP results extract
    ------------------------------------------------------------------------*/

    DROP TABLE IF EXISTS #X_ESP_XTR_Final_Results_RES;

    -- Final results
    SELECT DISTINCT
        op.PAT_ID
      , pa.PAT_MRN_Id
      , res.ORDER_PROC_ID                                  AS LAB_ORDER_ID
      , op.ORDERING_DATE                                   AS ORDERING_DATE
      , op.RESULT_TIME                                     AS RESULT_DATE
      , op.AUTHRZING_PROV_ID                               AS ORDERING_PROVIDER_ID
      , ot.NAME                                            AS ORDER_TYPE
      , eap.PROC_CODE                                      AS CPT
      , res.COMPONENT_ID
      , cc.NAME                                            AS TEST_NAME
      , res.ORD_VALUE                                      AS RESULT
      , LEFT(rf.TITLE, 20)                                 AS RESULT_FLAG
      , res.REFERENCE_LOW
      , res.REFERENCE_HIGH
      , REPLACE( res.REFERENCE_UNIT, '^', ' ' )            AS REFERENCE_UNIT
      , rs.TITLE                                           AS RESULT_STATUS
      , REPLACE( res.COMPONENT_COMMENT, '^', ' ' )         AS COMPONENT_COMMENT
      , NULL                                               AS SPECIMEN_ID
      , REPLACE( res.COMPONENT_COMMENT, '^', ' ' )         AS IMPRESSION_COMMENT
      , ss.TITLE                                           AS SPECIMEN_SRC
      , op2.SPECIMN_TAKEN_DATE                             AS COLLECTION_DATE
      , eap.PROC_NAME                                      AS PROCEDURE_NAME
      , NULL                                               AS LAB_RESULT_ID
      , NULL                                               AS PATIENT_CLASS
      , NULL                                               AS PATIENT_STATUS
      -- REPLACE THE FOLLOWING AND REMOVE NULL LINE WITH CLIA COLUMN
      -- , poi.PERFORMING_ORG_CLIA_NUM                        AS CLIA_NUM
      , NULL                                               AS CLIA_NUM
      , CONVERT( VARCHAR(8), op2.SPECIMEN_RECV_DATE, 112 ) AS COLLECTION_END_DATE
      , NULL                                               AS STATUS_DATE
      , NULL                                               AS INTERPRETER
      , NULL                                               AS INTERPRETER_ID
      , NULL                                               AS INTERPRETER_ID_TYPE
      , NULL                                               AS INTERPRETER_UID
      , NULL                                               AS OBS_METHOD
      , NULL                                               AS REF_TEXT
      -- Add a preceding "D" to designate departments/facilities
      -- Provider Extract must also include preceding "D"
      , concat('D', coalesce(pe.DEPARTMENT_ID, pe.EFFECTIVE_DEPT_ID))    AS FACILITY_PROVIDER_ID
    INTO
        #X_ESP_XTR_Final_Results_RES
    FROM
        Clarity.dbo.ORDER_RESULTS                 AS res
        INNER JOIN Clarity.dbo.Order_Proc         AS op
           ON res.ORDER_PROC_ID = op.ORDER_PROC_ID
        INNER JOIN Clarity.dbo.PATIENT            AS pa
           ON op.PAT_ID = pa.PAT_ID
        LEFT JOIN Clarity.dbo.ORDER_PROC_2        AS op2
          ON op2.ORDER_PROC_ID = op.ORDER_PROC_ID
        LEFT JOIN Clarity.dbo.ZC_ORDER_TYPE       AS ot
          ON ot.ORDER_TYPE_C = op.ORDER_TYPE_C
        LEFT JOIN Clarity.dbo.CLARITY_EAP         AS eap
          ON eap.PROC_ID = op.PROC_ID
        LEFT JOIN Clarity.dbo.ZC_RESULT_FLAG      AS rf
          ON rf.RESULT_FLAG_C = res.RESULT_FLAG_C
        LEFT JOIN Clarity.dbo.ZC_RESULT_STATUS    AS rs
          ON rs.RESULT_STATUS_C = res.RESULT_STATUS_C
        LEFT JOIN Clarity.dbo.ZC_SPECIMEN_SOURCE  AS ss
          ON ss.SPECIMEN_SOURCE_C = op.SPECIMEN_SOURCE_C
        -- REPLACE THE FOLLOWING WITH A JOIN TO THE TABLE THAT STORES THE LAB CLIA  
        --LEFT JOIN Clarity.dbo.PERFORMING_ORG_INFO AS poi
        --  ON op.ORDER_PROC_ID = poi.ORDER_ID
        LEFT JOIN Clarity.dbo.CLARITY_COMPONENT   AS cc
          ON cc.COMPONENT_ID = res.COMPONENT_ID
        LEFT JOIN Clarity.dbo.PAT_ENC             AS pe
          ON res.PAT_ENC_CSN_ID=pe.PAT_ENC_CSN_ID
          
     WHERE
        -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
        (res.RESULT_STATUS_C IN ( 3, 4 ) or res.RESULT_STATUS_C IS NULL) -- INCLUDE Final and Corrected
        AND (op.ORDER_STATUS_C NOT IN ( 1, 4 ) or op.ORDER_STATUS_C IS NULL) -- EXCLUDE Pending and Cancelled orders

        AND op.RESULT_TIME < @endDate
        
        -- SITES CAN ADJUST "LOOKBACK" DAYS HERE
        AND (  op.RESULT_TIME >=  DATEADD ( DAY, -0, @startDate)
               OR
              (OP2.LAST_RESULT_UPD_TM >= DATEADD ( DAY, -0, @startDate) 
                   and OP2.LAST_RESULT_UPD_TM < @endDate)
            )
        
        -- IF RESULTS SHOULD BE RESTRICTED TO PARTICULAR ORDER TYPES UNCOMMENT
        -- AND CUSTOMIZE AS NEEDED
        -- AND op.order_type_c in (
        -- 1,         -- Procedures
        -- 5,         -- Radiology
        -- 6,         -- Immunization/Injection
        -- 7,         -- Lab
        -- 10,        -- NURSING
        -- 14,        -- Blood Bank
        -- 16,        -- Microbiology
        -- 21,        -- Respiratory Care
        -- 26,        -- Point of Care Testing
        -- 41,        -- PR Charge
        -- 60,        -- Nursing Transfusion
        -- 67,        -- Point of Care External Testing
        -- 1005,      -- CPVL - PULMONARY PROCEDURES
        -- 1007,      -- CPVL - VASCULAR PROCEDURES
        -- 1014       -- Cytology
        -- )    
            
            ;

    SELECT
        *
    FROM
        #X_ESP_XTR_Final_Results_RES;

END;
GO

