/* Fenway notes to GLFHC: We report all appointments in most of our departments in the past 5 days.
** In order to include both problem list diagnoses and billed diagnoses, we duplicate each visit into two visits that are identical except for the symptom list.
** This file contains 2 logically separate queries Union-ed together to append both versions of each appointment.
** Visits with billed diagnoses have IDs suffixed by 'A'. Visits with problem list diagnoses have IDs suffixed by 'B'.
** Temperature is treated specially because patients may have multiple temperature readings in one visit. The subquery prevents this from duplicating visits.
** No data is reported here for the last 10 columns. These default to null in ESP.*/

declare @startdate date
declare @enddate date
set @startdate = getdate() - 5
set @enddate = getdate()

/*Diagnoses from billing codes*/
select
patient_id,
esp_mrn,
natural_key,
encounter_date,
is_closed,
date_closed,
provider_id,
dept_id_num,
dept_name,
event_type,
edd,
temperature1.temp,
cpt#,
weight,
height,
bp_systolic,
bp_diastolic,
o2_stat,
peak_flow,
icds
from
(
select distinct patientprofile.pid as patient_id,
patientprofile.patientid as esp_mrn,
cast(appointments.appointmentsid as varchar) + 'A' as natural_key,
convert(nvarchar,year(appointments.apptstart)) + right('0' + convert(nvarchar,month(appointments.apptstart)), 2) + '' + right('0' + convert(nvarchar,day(appointments.apptstart)), 2) as encounter_date,
null as is_closed,
null as date_closed,
appointments.doctorid as provider_id,
appointments.facilityid as dept_id_num,
facility.listname as dept_name,
appttype.name as event_type,
null as edd,
temperature.obsdate,
null as cpt#,
null as weight,
null as height,
null as bp_systolic,
null as bp_diastolic,
null as o2_stat,
null as peak_flow,
/*Concatenate all diagnoses from the visit into one field, separated by ;*/
case when Diagnosis.codetype = 1 then (
            substring((select cast('; icd9:' + patientvisitdiags.code + ' ' + 
                         /*Remove special characters from diagnosis descriptions*/ 
                         (case when patientvisitdiags.description like '%&%' then ' '
                              when patientvisitdiags.description like '%;%' then ' '
                              when patientvisitdiags.description like '%^%' then ' '
                              when patientvisitdiags.description like '%<%' then ' '
                              when patientvisitdiags.description like '%>%' then ' '
                              when patientvisitdiags.description like '%''%' then ' '
                              else patientvisitdiags.description end)
                        as varchar(1000))
                 from  patientvisitdiags
                 where (patientvisitdiags.patientvisitid = appointments.patientvisitid)
                 for xml path ('')),3,1000000)
                 )
     when Diagnosis.codetype = 8 then (
            substring((select cast('; icd10:' + patientvisitdiags.code + ' ' + 
                         /*Remove special characters from diagnosis descriptions*/ 
                         (case when patientvisitdiags.description like '%&%' then ' '
                              when patientvisitdiags.description like '%;%' then ' '
                              when patientvisitdiags.description like '%^%' then ' '
                              when patientvisitdiags.description like '%''%' then ' '
                              else patientvisitdiags.description end)
                        as varchar(1000))
                 from  patientvisitdiags
                 where (patientvisitdiags.patientvisitid = appointments.patientvisitid)
                 for xml path ('')),3,1000000)
                 ) end as icds

from appointments
left join patientvisitdiags on patientvisitdiags.patientvisitid = appointments.patientvisitid
left join dbo.Diagnosis ON dbo.Diagnosis.DiagnosisId = dbo.PatientVisitDiags.DiagnosisId
left join patientprofile on appointments.ownerid = patientprofile.patientprofileid
left join doctorfacility as doctor on appointments.doctorid = doctor.doctorfacilityid
left join doctorfacility as facility on appointments.facilityid = facility.doctorfacilityid
left join appttype on appointments.appttypeid = appttype.appttypeid
left join
      (
      select max(obs.obsdate) obsdate, /*Look at last temperature reading, if there are multiple*/
             obs.pid
             from obs
             where obs.hdid = 2641 /*Patient temperature*/
             group by obs.pid
      ) as temperature
      /*Appointment time may not be exactly equal to the time the temperature was taken.*/
      on year(appointments.apptstart) = year(temperature.obsdate)
      and month(appointments.apptstart) = month(temperature.obsdate)
      and day(appointments.apptstart) = day(temperature.obsdate)
      and patientprofile.pid = temperature.pid

where
/*Fenway reports appointments modified in the last day to make sure not to miss data. One could also use appointment date, as below.*/
--appointments.apptstart between @startdate and @enddate
appointments.lastmodified between @startdate and @enddate
and appointments.apptstatusmid in (314, -920) /*Exclude canceled appointments*/
and appointments.facilityid <> 7 /*Update for relevant medical facilities/departments at GLFHC.*/
    
/*Exclude test patients*/           
and patientprofile.first <> 'test'
and patientprofile.last <> 'test'
and patientprofile.searchname <> 'testlast'
and patientprofile.searchname not like '%zzz%'
and patientprofile.searchname not like '%xxx%'
and patientprofile.patientid not like  '%t%'
and patientprofile.last <> 'ffff'
and patientprofile.first <> 'fozzie'
and patientprofile.first <> 'flash'
and patientprofile.last <> 'piggy'
and patientprofile.last not like '%frog%'
and patientprofile.last <> 'dontuse'
and patientprofile.last <> 'donotuse'
and patientprofile.last <> 'do not use'
and patientprofile.last <> 'bogus'
and patientprofile.last not like 'aaaaaa%'
and patientprofile.last <> 'huckelberry'
and patientprofile.searchname not like '%gonzo%tom%'
and patientprofile.searchname not like '%lebowsky%jeff%'
) MainQuery
left join (
      select obs.obsdate,
             obs.pid,
             obs.obsvalue as temp
             from obs
             where obs.hdid = 2641  /*Patient temperature*/
      ) as temperature1
      on MainQuery.obsdate = temperature1.obsdate
      and MainQuery.patient_id = temperature1.pid
      
Union

/*Diagnoses from problem list*/
select
patient_id,
esp_mrn,
natural_key,
encounter_date,
is_closed,
date_closed,
provider_id,
dept_id_num,
dept_name,
event_type,
edd,
temperature1.temp,
cpt#,
weight,
height,
bp_systolic,
bp_diastolic,
o2_stat,
peak_flow,
icds
from
(
select distinct patientprofile.pid as patient_id,
patientprofile.patientid as esp_mrn,
cast(appointments.appointmentsid as varchar) + 'B' as natural_key,
convert(nvarchar,year(appointments.apptstart)) + right('0' + convert(nvarchar,month(appointments.apptstart)), 2) + '' + right('0' + convert(nvarchar,day(appointments.apptstart)), 2) as encounter_date,
null as is_closed,
null as date_closed,
appointments.doctorid as provider_id,
appointments.facilityid as dept_id_num,
facility.listname as dept_name,
appttype.name as event_type,
null as edd,
temperature.obsdate,
null as cpt#,
null as weight,
null as height,
null as bp_systolic,
null as bp_diastolic,
null as o2_stat,
null as peak_flow,
/*Concatenate all diagnoses from the visit into one field, separated by ;*/
/*Fenway's problem list still shows an ICD-9 code for every problem, even when it is entered as ICD-10.*/
substring((select cast('; icd9:' +
                      (case when left(problem.code, 4) = 'ICD-' then right(problem.code, len(problem.code) - 4)
                        else '799.9' end) + /*OTHER UNKNOWN OR UNSPECIFIED CAUSE*/
                        ' ' + 
                        /*Remove special characters from diagnosis descriptions*/ 
                        (case when problem.description like '%&%' then ' '
                              when problem.description like '%;%' then ' '
                              when problem.description like '%^%' then ' '
                              when problem.description like '%<%' then ' '
                              when problem.description like '%>%' then ' '
                              when problem.description like '%''%' then ' '
                              else problem.description end)
                        as varchar(1000))
     from  problem
     where (problem.pid = patientprofile.pid
                  and problem.db_create_date > appointments.apptstart
                  and problem.db_create_date < appointments.apptstart + 1)
                  /*Remove problems that were created in the past and ended today.*/
                  and (problem.stopdate is null
                    or problem.stopdate > getdate()
                    or problem.stopdate = getdate() and problem.onsetdate = getdate())
     for xml path ('')),3,1000000) as icds
     
from appointments
left join patientprofile on appointments.ownerid = patientprofile.patientprofileid
left join problem on problem.pid = patientprofile.pid
left join doctorfacility as doctor on appointments.doctorid = doctor.doctorfacilityid
left join doctorfacility as facility on appointments.facilityid = facility.doctorfacilityid
left join appttype on appointments.appttypeid = appttype.appttypeid
left join
      (
      select max(obs.obsdate) obsdate, /*Look at last temperature reading, if there are multiple*/
             obs.pid
             from obs
             where obs.hdid = 2641 /*Patient temperature*/
             group by obs.pid
      ) as temperature
      /*Appointment time may not be exactly equal to the time the temperature was taken.*/
      on year(appointments.apptstart) = year(temperature.obsdate)
      and month(appointments.apptstart) = month(temperature.obsdate)
      and day(appointments.apptstart) = day(temperature.obsdate)
      and patientprofile.pid = temperature.pid

where
/*Fenway reports appointments modified in the last day to make sure not to miss data. One could also use appointment date, as below.*/
--appointments.apptstart between @startdate and @enddate
appointments.lastmodified between @startdate and @enddate
and appointments.apptstatusmid in (314, -920) /*Exclude canceled appointments*/
and appointments.facilityid <> 7 /*Update for relevant medical facilities/departments at GLFHC.*/

/*Exclude test patients*/           
and patientprofile.first <> 'test'
and patientprofile.last <> 'test'
and patientprofile.searchname <> 'testlast'
and patientprofile.searchname not like '%zzz%'
and patientprofile.searchname not like '%xxx%'
and patientprofile.patientid not like  '%t%'
and patientprofile.last <> 'ffff'
and patientprofile.first <> 'fozzie'
and patientprofile.first <> 'flash'
and patientprofile.last <> 'piggy'
and patientprofile.last not like '%frog%'
and patientprofile.last <> 'dontuse'
and patientprofile.last <> 'donotuse'
and patientprofile.last <> 'do not use'
and patientprofile.last <> 'bogus'
and patientprofile.last not like 'aaaaaa%'
and patientprofile.last <> 'huckelberry'
and patientprofile.searchname not like '%gonzo%tom%'
and patientprofile.searchname not like '%lebowsky%jeff%'

) MainQuery
left join (
      select obs.obsdate,
             obs.pid,
             obs.obsvalue as temp
             from obs
             where obs.hdid = 2641 /*Patient temperature*/
      ) as temperature1
      on MainQuery.obsdate = temperature1.obsdate
      and MainQuery.patient_id = temperature1.pid
      