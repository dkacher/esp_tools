/* Fenway notes to GLFHC: We report all patients with appointments in most of our departments in the past 5 days.
** Review Race data; it may be stored or collected very differently at GLFHC.
** No data is reported here for the last 6 columns: title, remark, income level, housing status, insurance status, or country of birth. These default to null in ESP.*/

declare @startdate date
declare @enddate date
set @startdate = getdate() - 5
set @enddate = getdate()

select distinct patientprofile.pid as natural_key,
patientprofile.patientid as esp_mrn,
patientprofile.last as last_name,
patientprofile.first as first_name,
substring(patientprofile.middle,1,1) as middle_name,
patientprofile.address1,
patientprofile.address2,
patientprofile.city,
patientprofile.state,
REPLACE(REPLACE(patientprofile.zip, CHAR(13), ''), CHAR(10), '') as zip, /*Remove erroneous newline characters from zip codes*/
patientprofile.country,
left(patientprofile.phone1,3) as areacode,
substring(patientprofile.phone1,4,3) + '-' + substring(patientprofile.phone1,7,4) as tel,
null as tel_ext,
convert(nvarchar,year(patientprofile.birthdate)) + right('0' + convert(nvarchar,month(patientprofile.birthdate)), 2) + '' + right('0' + convert(nvarchar,day(patientprofile.birthdate)), 2) as date_of_birth,
/*Fenway has a lot of details on patient Gender, including our own internal problem codes to recognize trans patients. GLFHC may just use PatientProfile.sex.*/
(case when (PatientProfile.pid in (select distinct PatientProfile.pid
      from problem, PatientProfile
      where PatientProfile.pid=problem.pid
      and (problem.code like '%302.85' or problem.code like '%259.9' or problem.code like 'THP%' or problem.code='GENDER')
      and problem.change in (1,2)
      and problem.xid=1000000000000000000
      AND  NOT (problem.qualifier='? of' 
           OR problem.qualifier='Fh of' 
           OR problem.qualifier='R/O' 
           OR problem.qualifier='Rs of')))
      then 'T'
      else PatientProfile.sex end) Gender,
(case when patientrace.PatientRaceSubcategoryMID=179501 
            or patientprofile.pid in (select patientprofile.pid
                  from patientprofile,
                  (select patientrace.pid, count(patientrace.pid) TotRace
                  from patientrace 
                  group by patientrace.pid) TotRace
                  where patientprofile.pid=TotRace.pid 
                  and TotRace.TotRace>1)
            then 'Multiracial'
      when patientrace.patientracemid=1628 or patientrace.PatientRaceSubcategoryMID in (179499,179500,179498) then 'Asian'
      when patientrace.patientracemid=179491 then 'White'
      when patientrace.patientracemid=179493 then 'AMERICAN INDIAN/ALASKAN NATIVE'
      when patientrace.patientracemid=179494 then 'Black'
      when patientrace.patientracemid=179492 then 'PACIFIC ISLANDER/HAWAIIAN'
      when patientrace.PatientRaceSubcategoryMID=179508 then 'Other'
      when patientrace.PatientRaceSubcategoryMID=179506 or patientprofile.EthnicityMID=12036 then 'Hispanic'
      when patientrace.patientracemid=179495 then 'Patient Declined'
      else 'Unknown' end) Race,
language.ShortDescription as home_language,
substring(patientprofile.ssn,1,3) + '-' + substring(patientprofile.ssn,4,2) + '-' + substring(patientprofile.ssn,6,4) as ssn,
patientprofile.doctorid as pcp_id,
married.description as marital_stat,
null as religion,
null as aliases,
null as mother_mrn,
convert(nvarchar,year(patientprofile.deathdate)) + right('0' + convert(nvarchar,month(patientprofile.deathdate)), 2) + '' + right('0' + convert(nvarchar,day(patientprofile.deathdate)), 2) as date_of_death,
/*Center ID for Fenway is always 1*/
1 as center_id,
ethnicity.description as ethnicity,
null as mother_maiden_name,
convert(nvarchar,year(patientprofile.lastmodified)) + right('0' + convert(nvarchar,month(patientprofile.lastmodified)), 2) + '' + right('0' + convert(nvarchar,day(patientprofile.lastmodified)), 2) as last_update,
null as last_update_site,
patientprofile.suffix as name_suffix

from patientprofile
left join patientrace on patientprofile.pid=patientrace.pid 
left join medlists as Race on patientrace.patientracemid=Race.medlistsid
left join medlists as RaceSubCat on patientrace.PatientRaceSubcategoryMID=RaceSubCat.medlistsid
left join language on patientprofile.languageId = language.languageId
left join medlists as married on patientprofile.maritalstatusMId = married.medlistsid
left join medlists as ethnicity on patientprofile.ethnicityMId = ethnicity.medlistsid
left join appointments on patientprofile.patientprofileid = appointments.ownerid

where
/*Exclude test patients*/           
PatientProfile.first <> 'test'
and PatientProfile.last <> 'test'
and PatientProfile.searchname <> 'testlast'
and PatientProfile.searchname not like '%zzz%'
and PatientProfile.searchname not like '%xxx%'
and PatientProfile.patientid not like  '%t%'
and PatientProfile.last <> 'ffff'
and PatientProfile.first <> 'fozzie'
and PatientProfile.first <> 'flash'
and PatientProfile.last <> 'piggy'
and PatientProfile.last not like '%frog%'
and PatientProfile.last <> 'bogus'
and PatientProfile.last <> 'dontuse'
and PatientProfile.last <> 'donotuse'
and PatientProfile.last <> 'do not use'
and PatientProfile.searchname not like '%gonzo%tom%'
and PatientProfile.searchname not like '%lebowsky%jeff%'
and PatientProfile.searchname not like '%anonymous%'
and PatientProfile.searchname <> 'A, A A'
and patientprofile.last not like 'aaaaaa%'
and patientprofile.last <> 'huckelberry'
and patientprofile.searchname not like '%spongebob%'

and appointments.apptstart <= @enddate and appointments.apptstart >= @startdate
and appointments.apptstatusmid in (314, -920) /*Exclude canceled appointments*/
and appointments.facilityid <> 7 /*Update for relevant medical facilities/departments at GLFHC.*/