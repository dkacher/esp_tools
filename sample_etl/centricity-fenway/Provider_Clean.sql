/* Fenway notes to GLFHC: We report all providers in the practice. The file rarely changes, but it's small so we pull everyone every day.
** No data is reported here for the last 2 columns:department code, address code. These default to null in ESP.*/

select distinct doctorfacility.doctorfacilityid as natural_key,
doctorfacility.last as last_name,
doctorfacility.first as first_name,
substring(doctorfacility.middle,1,1) as middle_name,
doctorfacility.prefix as title,
locreg.facilityid as dept_natural_key,
locreg.name as dept,
locreg.address1 as dept_address_1,
locreg.address2 as dept_address_2,
locreg.city as dept_city,
locreg.state as dept_state,
locreg.zip as dept_zip,
left(locreg.primphone,3) as areacode,
substring(locreg.primphone,4,3) + '-' + substring(locreg.primphone,7,4) as telephone,
/*Center ID is 1 for Fenway*/
1 as center_id,
locreg.country as dept_country,
null as dept_country_code,
null as tel_country_code,
null as tel_ext, /*Fenway does not use telephone extensions in primary dept numbers.*/
null as call_info, /*Fenway does not record comments on how to contact departments.*/
doctorfacility.address1 as clin_address1,
doctorfacility.address2 as clin_address2,
doctorfacility.city as clin_city,
doctorfacility.state as clin_state,
doctorfacility.zip as clin_zip,
doctorfacility.country as clin_country,
null as clin_country_code,
null as clin_tel_country_code,
left(doctorfacility.phone1,3) as clin_areacode,
substring(doctorfacility.phone1,4,3) + '-' + substring(doctorfacility.phone1,7,4) as clin_tel,
null as clin_tel_ext, /*Fenway does not use telephone extensions in clinician phone numbers.*/
null as clin_call_info, /*Fenway does not record comments on how to contact clinicians.*/
doctorfacility.suffix as suffix

from doctorfacility
left join locreg on DoctorFacility.LocationId = LOCREG.LOCID

where doctorfacility.inactive = 0
and doctorfacility.type in (1,7,8) /*Include only providers, not other types of staff.*/