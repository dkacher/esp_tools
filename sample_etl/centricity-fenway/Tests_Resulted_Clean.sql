/* Fenway notes to GLFHC: Fenway pulls test data from the OBS table where results are entered. Information on the type of test comes from the test name.
** Depending how GLFHC's system is set up, you may have more data available on the lab order, rather than just the result. Comments in the code indicate certain limitations to this code's approach.
** No data is reported here for the last 13 columns. These default to null in ESP.*/

declare @startdate date
declare @enddate date
set @startdate = getdate() - 5
set @enddate = getdate()

select patientprofile.pid as patient_id,
patientprofile.patientid as esp_mrn,
obs.obsid as order_natural_key,
/*The obsdate represents the date on which the specimen was collected, usually but not always the day the test was ordered*/
convert(nvarchar,year(obs.obsdate)) + right('0' + convert(nvarchar,month(obs.obsdate)), 2) + '' + right('0' + convert(nvarchar,day(obs.obsdate)), 2) as order_date,
/*Since results are most often input into the obs table automatically, the db_updated_date usually represents the day Fenway received the results*/
convert(nvarchar,year(obs.db_updated_date)) + right('0' + convert(nvarchar,month(obs.db_updated_date)), 2) + '' + right('0' + convert(nvarchar,day(obs.db_updated_date)), 2) as result_date,
usr.doctorfacilityid as provider_id,
null as order_type,
substring(obshead.mlcode,5, len(obshead.mlcode)) as code, /*LOINC and MLI codes*/
obshead.mlcode as component,
obshead.description as component_name,
obs.obsvalue as result_string,
obs.abnormal as normal_flag,
null as ref_low,
null as ref_high,
obshead.unit as unit,
(case when (obs.state is null or obs.state = '' or obs.state = ' ')
      then 'F'
      else obs.state end) as status,
obs.description as note,
null as specimen_num,
null as impression,
(case when obshead.mlcode in ('LOI-0688-2', 'MLI-274151', 'MLI-274155', 'MLI-240140')  then 'cervix'
      when obshead.mlcode in ('LOI-0691-6', 'LOC-210025', 'CPT-87110')  then 'unknown' /*Test types without a unique specimen source.*/
      when obshead.mlcode in ('LOI-0696-5', 'MLI-274150', 'MLI-7293', 'MLI-274154')  then 'throat'
      when obshead.mlcode in ('LOI-0697-3', 'MLI-274152', 'MLI-274156', 'MLI-11209', 'MLI-6857', 'MLI-4001.61', 'MLI-537507', 'CPT-87178', 'MLI-249524', 'MLI-587011', 'MLI-587012')  then 'urine'
      when obshead.mlcode in ('MLI-117632', 'LOC-1270003', 'MLI-274149', 'MLI-274153', 'MLI-117631')  then 'rectum'
      else 'unknown' end) as specimen_source

from patientprofile
inner join obs on dbo.PatientProfile.PId = dbo.OBS.PID
left join usr on obs.pubuser = usr.pvid
left join obshead on obs.hdid = obshead.hdid

where obs.db_updated_date between @startdate and @enddate
and obs.xid = 1000000000000000000
and obs.change not in (0,4,10,11,12) /*Excluded deleted data.*/
and (obs.hdid in (2719, 2746, 2772, 3552, 3553, 5556, 7293, 45361, 76052, 80012, 110694, 117632, 153580, 240141, 274149, 274150, 274151, 274152, 12700004, 537507, 587011) /*gonorrhea*/
  or obs.hdid in (274156, 274153, 274154, 11209, 117631, 274155, 2100025, 6857, 2679, 2716, 240140, 249524, 587012)) /*chlamydia*/
/*Exclude recently updated entries for results over 45 days old*/
and obs.obsdate >= dateadd(dd,-30,@startdate)

/*Exclude test patients*/           
and patientprofile.first <> 'test'
and patientprofile.last <> 'test'
and patientprofile.searchname <> 'testlast'
and patientprofile.searchname not like '%zzz%'
and patientprofile.searchname not like '%xxx%'
and patientprofile.patientid not like  '%t%'
and patientprofile.last <> 'ffff'
and patientprofile.first <> 'fozzie'
and patientprofile.first <> 'flash'
and patientprofile.last <> 'piggy'
and patientprofile.last not like '%frog%'
and patientprofile.last <> 'dontuse'
and patientprofile.last <> 'donotuse'
and patientprofile.last <> 'do not use'
and patientprofile.last <> 'bogus'
and patientprofile.last not like 'aaaaaa%'
and patientprofile.last <> 'huckelberry'
and patientprofile.searchname not like '%gonzo%tom%'
and patientprofile.searchname not like '%lebowsky%jeff%'