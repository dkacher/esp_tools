/* Fenway notes to GLFHC: Depending on the circumstances of treatment, medications for GC/CT may be stored in one of three tables: OBS, Orders, or Medicate (tied to Prescrib).
** This file contains 3 logically separate queries Union-ed together to collate all relevant medications.
** This frequently duplicates a single medication, but DPH has indicated that they can deal with that on their end.
** No data is reported here for the last 4 columns: route, dose, patient class, or patient status. These default to null in ESP.*/


declare @startdate date
declare @enddate date
set @startdate = getdate() - 5
set @enddate = getdate()

/*Meds from Medicate*/
select patientprofile.pid as patient_id,
patientprofile.patientid as esp_mrn,
prescrib.ptid as natural_key,
usr.doctorfacilityid as provider_id,
convert(nvarchar,year(prescrib.clinicaldate)) + right('0' + convert(nvarchar,month(prescrib.clinicaldate)), 2) + '' + right('0' + convert(nvarchar,day(prescrib.clinicaldate)), 2) as order_date,
null as status,
REPLACE(REPLACE(medicate.instructions, CHAR(13), ''), CHAR(10), '') as directions, /*Remove erroneous newline characters from prescription directions*/
medicate.ndcLabProd + medicate.ndcPackage as ndc,
medicate.description as drug_desc,
prescrib.quantity as quantity,
prescrib.refills as refills,
convert(nvarchar,year(medicate.startdate)) + right('0' + convert(nvarchar,month(medicate.startdate)), 2) + '' + right('0' + convert(nvarchar,day(medicate.startdate)), 2) as start_date,
convert(nvarchar,year(medicate.stopdate)) + right('0' + convert(nvarchar,month(medicate.stopdate)), 2) + '' + right('0' + convert(nvarchar,day(medicate.stopdate)), 2) as end_date

from patientprofile
inner join prescrib on patientprofile.pid = prescrib.pid
inner join medicate on medicate.pid = prescrib.pid and medicate.mid = prescrib.mid
left join usr on usr.pvid = prescrib.pvid
left join locreg on usr.homelocation = locreg.locid

where prescrib.db_updated_date between @startdate and @enddate
and prescrib.change not in (0,4,10) /*Excluded deleted data.*/
and medicate.xid = 1000000000000000000

and locreg.facilityid in (3,5,10,2679) /*Update for relevant medical facilities/departments at GLFHC.*/

/*Exclude test patients*/           
and patientprofile.first <> 'test'
and patientprofile.last <> 'test'
and patientprofile.searchname <> 'testlast'
and patientprofile.searchname not like '%zzz%'
and patientprofile.searchname not like '%xxx%'
and patientprofile.patientid not like  '%t%'
and patientprofile.last <> 'ffff'
and patientprofile.first <> 'fozzie'
and patientprofile.first <> 'flash'
and patientprofile.last <> 'piggy'
and patientprofile.last not like '%frog%'
and patientprofile.last <> 'dontuse'
and patientprofile.last <> 'donotuse'
and patientprofile.last <> 'do not use'
and patientprofile.last <> 'bogus'
and patientprofile.last not like 'aaaaaa%'
and patientprofile.last <> 'huckelberry'
and patientprofile.searchname not like '%gonzo%tom%'
and patientprofile.searchname not like '%lebowsky%jeff%'

and (
  medicate.description like '%amoxicillin%'
  or medicate.description like '%cefixime%'
  or medicate.description like '%cefotaxime%'
  or medicate.description like '%cefpodoxime%'
  or medicate.description like '%ceftizoxime%'
  or medicate.description like '%ceftriaxone%'
  or medicate.description like '%ciprofloxacin%'
  or medicate.description like '%gatifloxacin%'
  or medicate.description like '%levofloxacin%'
  or medicate.description like '%ofloxacin%'
  or medicate.description like '%spectinomycin%'
  or medicate.description like '%moxifloxacin%'
  or medicate.description like '%suprax%'
  or medicate.description like '%avelox%'
  or medicate.description like '%rocephin%'
  or medicate.description like '%trobicin%'
  or medicate.description like '%augmentin%'
  or medicate.description like '%proquin%'
  or medicate.description like '%vantin%'
  or medicate.description like '%trimox%'
  or medicate.description like '%azithromycin%'
  or medicate.description like '%zithromax%'
  or medicate.description like '%doxycycline%'
  or medicate.description like '%erythromycin%'
  or medicate.description like '%gemifloxacin%'
  or medicate.description like '%factive%'
  or medicate.description like '%ampicillin%'
  or medicate.description like '%erythromycin%'
  or medicate.description like '%gentamicin%'
  or medicate.description like '%clindamycin%'
  or medicate.description like '%cefoxitin%'
  or medicate.description like '%metronidazole%'
  or medicate.description like '%tinidazole%'
  or medicate.description like '%cefuroxime%'
  or medicate.description like '%ceftibuten%'
  or medicate.description like '%cefdinir%'
  or medicate.description like '%omnicef%'
  or medicate.description like '%truvada%'
  
  or medicate.genericmed like '%amoxicillin%'
  or medicate.genericmed like '%cefixime%'
  or medicate.genericmed like '%cefotaxime%'
  or medicate.genericmed like '%cefpodoxime%'
  or medicate.genericmed like '%ceftizoxime%'
  or medicate.genericmed like '%ceftriaxone%'
  or medicate.genericmed like '%ciprofloxacin%'
  or medicate.genericmed like '%gatifloxacin%'
  or medicate.genericmed like '%levofloxacin%'
  or medicate.genericmed like '%ofloxacin%'
  or medicate.genericmed like '%spectinomycin%'
  or medicate.genericmed like '%moxifloxacin%'
  or medicate.genericmed like '%azithromycin%'
  or medicate.genericmed like '%zithromax%'
  or medicate.genericmed like '%doxycycline%'
  or medicate.genericmed like '%erythromycin%'
  or medicate.genericmed like '%gemifloxacin%'
  or medicate.genericmed like '%factive%'
  or medicate.genericmed like '%ampicillin%'
  or medicate.genericmed like '%erythromycin%'
  or medicate.genericmed like '%gentamicin%'
  or medicate.genericmed like '%clindamycin%'
  or medicate.genericmed like '%cefoxitin%'
  or medicate.genericmed like '%metronidazole%'
  or medicate.genericmed like '%tinidazole%'
  or medicate.genericmed like '%cefuroxime%'
  or medicate.genericmed like '%ceftibuten%'
  or medicate.genericmed like '%cefdinir%'
  or medicate.genericmed like '%omnicef%'
  or medicate.genericmed like '%truvada%'
  )

Union

/*Meds from Orders*/
select patientprofile.pid as patient_id,
patientprofile.patientid as esp_mrn,
orders.orderid as natural_key,
usr.doctorfacilityid as provider_id,
convert(nvarchar,year(orders.orderdate)) + right('0' + convert(nvarchar,month(orders.orderdate)), 2) + '' + right('0' + convert(nvarchar,day(orders.orderdate)), 2) as order_date,
null as status,
null as directions,
null as ndc,
orders.description as drug_desc,
null as quantity,
null as refills,
convert(nvarchar,year(orders.orderdate)) + right('0' + convert(nvarchar,month(orders.orderdate)), 2) + '' + right('0' + convert(nvarchar,day(orders.orderdate)), 2) as start_date,
convert(nvarchar,year(orders.orderdate)) + right('0' + convert(nvarchar,month(orders.orderdate)), 2) + '' + right('0' + convert(nvarchar,day(orders.orderdate)), 2) as end_date


from patientprofile
inner join orders on orders.pid = patientprofile.pid
left join usr on usr.pvid = orders.usrid
left join locreg on usr.homelocation = locreg.locid

where orders.db_updated_date between @startdate and @enddate
and orders.change not in (10,11,12) /*Excluded deleted data.*/
and orders.status = 'C' /*Completed orders*/
and orders.xid = 1000000000000000000

and locreg.facilityid in (3,5,10,2679) /*Update for relevant medical facilities/departments at GLFHC.*/

/*Exclude test patients*/           
and patientprofile.first <> 'test'
and patientprofile.last <> 'test'
and patientprofile.searchname <> 'testlast'
and patientprofile.searchname not like '%zzz%'
and patientprofile.searchname not like '%xxx%'
and patientprofile.patientid not like  '%t%'
and patientprofile.last <> 'ffff'
and patientprofile.first <> 'fozzie'
and patientprofile.first <> 'flash'
and patientprofile.last <> 'piggy'
and patientprofile.last not like '%frog%'
and patientprofile.last <> 'dontuse'
and patientprofile.last <> 'donotuse'
and patientprofile.last <> 'do not use'
and patientprofile.last <> 'bogus'
and patientprofile.last not like 'aaaaaa%'
and patientprofile.last <> 'huckelberry'
and patientprofile.searchname not like '%gonzo%tom%'
and patientprofile.searchname not like '%lebowsky%jeff%'

and (
  orders.description like '%amoxicillin%'
  or orders.description like '%cefixime%'
  or orders.description like '%cefotaxime%'
  or orders.description like '%cefpodoxime%'
  or orders.description like '%ceftizosime%'
  or orders.description like '%ceftriaxone%'
  or orders.description like '%ciprofloxacin%'
  or orders.description like '%gatifloxacin%'
  or orders.description like '%levofloxacin%'
  or orders.description like '%ofloxacin%'
  or orders.description like '%spectinomycin%'
  or orders.description like '%moxifloxacin%'
  or orders.description like '%suprax%'
  or orders.description like '%avelox%'
  or orders.description like '%rocephin%'
  or orders.description like '%trobicin%'
  or orders.description like '%augmentin%'
  or orders.description like '%proquin%'
  or orders.description like '%vantin%'
  or orders.description like '%trimox%'
  or orders.description like '%azithromycin%'
  or orders.description like '%zithromax%'
  or orders.description like '%zitromax%' /*This misspelling is a common entry in the orders table*/
  or orders.description like '%doxycycline%'
  or orders.description like '%gemifloxacin%'
  )
  
  Union

/*Meds from OBS*/
select patientprofile.pid as patient_id,
patientprofile.patientid as esp_mrn,
obs.obsid as natural_key,
usr.doctorfacilityid as provider_id,
convert(nvarchar,year(obs.obsdate)) + right('0' + convert(nvarchar,month(obs.obsdate)), 2) + '' + right('0' + convert(nvarchar,day(obs.obsdate)), 2) as order_date,
null as status,
null as directions,
null as ndc,
obshead.description as drug_desc,
obs.obsvalue as quantity,
null as refills,
convert(nvarchar,year(obs.obsdate)) + right('0' + convert(nvarchar,month(obs.obsdate)), 2) + '' + right('0' + convert(nvarchar,day(obs.obsdate)), 2) as start_date,
convert(nvarchar,year(obs.obsdate)) + right('0' + convert(nvarchar,month(obs.obsdate)), 2) + '' + right('0' + convert(nvarchar,day(obs.obsdate)), 2) as end_date

from patientprofile
inner join obs on obs.pid = patientprofile.pid
left join obshead on obs.hdid = obshead.hdid
left join usr on usr.pvid = obs.pubuser /*Person who signed record*/
left join locreg on usr.homelocation = locreg.locid

where obs.db_updated_date between @startdate and @enddate
and obs.change not in (0,4,10) /*Excluded deleted data.*/
and obs.xid = 1000000000000000000

and locreg.facilityid in (3,5,10,2679)  /*Update for relevant medical facilities/departments at GLFHC.*/

/*Exclude test patients*/           
and patientprofile.first <> 'test'
and patientprofile.last <> 'test'
and patientprofile.searchname <> 'testlast'
and patientprofile.searchname not like '%zzz%'
and patientprofile.searchname not like '%xxx%'
and patientprofile.patientid not like  '%t%'
and patientprofile.last <> 'ffff'
and patientprofile.first <> 'fozzie'
and patientprofile.first <> 'flash'
and patientprofile.last <> 'piggy'
and patientprofile.last not like '%frog%'
and patientprofile.last <> 'dontuse'
and patientprofile.last <> 'donotuse'
and patientprofile.last <> 'do not use'
and patientprofile.last <> 'bogus'
and patientprofile.last not like 'aaaaaa%'
and patientprofile.last <> 'huckelberry'
and patientprofile.searchname not like '%gonzo%tom%'
and patientprofile.searchname not like '%lebowsky%jeff%'

and obs.hdid in (5257, 85063, 410489) /*ceftiraxone, azithromycin, doxycycline*/