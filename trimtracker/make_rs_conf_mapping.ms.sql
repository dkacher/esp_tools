IF object_id('gen_pop_tools.rs_conf_mapping', 'U') IS NOT NULL DROP TABLE gen_pop_tools.rs_conf_mapping;
GO
CREATE TABLE gen_pop_tools.rs_conf_mapping
(
  id integer NOT NULL IDENTITY PRIMARY KEY,
  src_table character varying(50) NOT NULL,
  src_field character varying(50) NOT NULL,
  src_value character varying(2000) NOT NULL,
  mapped_value character varying(100) NOT NULL,
  code integer
);
--unique index ensures that there is one and only mapping for a table-field-value combination.
create unique index rs_conf_mapping_unique_idx
on gen_pop_tools.rs_conf_mapping (src_table,src_field,src_value);
--the conf_mapping.csv is created from conf_mapping.csv.default in the esp_tools/trimtracker repo. 
--copy conf_mapping.csv.default to a location where it can be loaded from SSMS
--after you create the rs_conf_mapping table and load the conf_mapping.csv data, you must run
--check_mapped_rs_data.pg.sql to find unmapped data values.  Use this to update conf_mapping.csv
--you may update and check in conf_mapping.csv.default with any additional values you find, if you think other sites may benefit.

