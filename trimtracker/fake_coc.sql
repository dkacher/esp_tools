-- CREATE hepc coc test counts cc_hepc_summaries
-- site A
drop table if exists fake_hepc_a;
create table fake_hepc_a as
select index_enc_yr, agegroup, birth_cohort, sex , race_ethnicity,
floor((random()* 0.08 + 0.9)  * total)::int as total_a ,
floor((random()* 0.08 + 0.9)  * tested0)::int as tested0_a,
floor((random()* 0.08 + 0.9)  * indexpos0)::int as indexpos0_a,
floor((random()* 0.08 + 0.9)  * vltest0)::int as vltest0_a,
floor((random()* 0.08 + 0.9)  * vload0)::int as vload0_a,
floor((random()* 0.08 + 0.9)  * lastvlpos0)::int as lastvlpos0_a ,
floor((random()* 0.08 + 0.9)  * lastvlneg0)::int as lastvlneg0_a,
floor((random()* 0.08 + 0.9)  * tested1)::int as tested1_a ,
floor((random()* 0.08 + 0.9)  * indexpos1)::int as indexpos1_a,
floor((random()* 0.08 + 0.9)  * vltest1)::int as vltest1_a,
floor((random()* 0.08 + 0.9)  * vload1)::int as vload1_a,
floor((random()* 0.08 + 0.9)  * lastvlpos1)::int as lastvlpos1_a,
floor((random()* 0.08 + 0.9)  * lastvlneg1)::int as lastvlneg1_a,
floor((random()* 0.08 + 0.9)  * acute)::int as acute_a ,
floor((random()* 0.08 + 0.9)  * acute_res_norx)::int as acute_res_norx_a,
floor((random()* 0.08 + 0.9)  * acute_res_prst)::int as acute_res_prst_a,
floor((random()* 0.08 + 0.9)  * acute_res_unkn)::int as acute_res_unkn_a,
floor((random()* 0.08 + 0.9)  * chronic)::int as chronic_a,
floor((random()* 0.08 + 0.9)  * chron_notrt)::int as chron_notrt_a,
floor((random()* 0.08 + 0.9)  * chron_notrt_res)::int as chron_notrt_res_a,
floor((random()* 0.08 + 0.9)  * chron_notrt_prst)::int as chron_notrt_prst_a,
floor((random()* 0.08 + 0.9)  * chron_notrt_unkn)::int as chron_notrt_unkn_a,
floor((random()* 0.08 + 0.9)  * chron_trt)::int as chron_trt_a,
floor((random()* 0.08 + 0.9)  * chron_trt_unkn)::int as chron_trt_unkn_a,
floor((random()* 0.08 + 0.9)  * chron_trt_vload)::int as chron_trt_vload_a, 
floor((random()* 0.08 + 0.9)  * chron_trt_lastvlneg)::int as chron_trt_lastvlneg_a, 
floor((random()* 0.08 + 0.9)  * chron_trt24_lastvlneg)::int as chron_trt24_lastvlneg_a 
from gen_pop_tools.cc_hepc_summaries;

-- site B
drop table if exists fake_hepc_b;
create table fake_hepc_b as
select index_enc_yr, agegroup, birth_cohort, sex , race_ethnicity,
floor((random()* 0.08 + 0.8)  * total)::int as total_a,
floor((random()* 0.08 + 0.8)  * tested0)::int as tested0_a,
floor((random()* 0.08 + 0.8)  * indexpos0)::int as indexpos0_a,
floor((random()* 0.08 + 0.8)  * vltest0)::int as vltest0_a,
floor((random()* 0.08 + 0.8)  * vload0)::int as vload0_a,
floor((random()* 0.08 + 0.8)  * lastvlpos0)::int as lastvlpos0_a ,
floor((random()* 0.08 + 0.8)  * lastvlneg0)::int as lastvlneg0_a,
floor((random()* 0.08 + 0.8)  * tested1)::int as tested1_a ,
floor((random()* 0.08 + 0.8)  * indexpos1)::int as indexpos1_a,
floor((random()* 0.08 + 0.8)  * vltest1)::int as vltest1_a,
floor((random()* 0.08 + 0.8)  * vload1)::int as vload1_a,
floor((random()* 0.08 + 0.8)  * lastvlpos1)::int as lastvlpos1_a,
floor((random()* 0.08 + 0.8)  * lastvlneg1)::int as lastvlneg1_a,
floor((random()* 0.08 + 0.8)  * acute)::int as acute_a ,
floor((random()* 0.08 + 0.8)  * acute_res_norx)::int as acute_res_norx_a,
floor((random()* 0.08 + 0.8)  * acute_res_prst)::int as acute_res_prst_a,
floor((random()* 0.08 + 0.8)  * acute_res_unkn)::int as acute_res_unkn_a,
floor((random()* 0.08 + 0.8)  * chronic)::int as chronic_a,
floor((random()* 0.08 + 0.8)  * chron_notrt)::int as chron_notrt_a,
floor((random()* 0.08 + 0.8)  * chron_notrt_res)::int as chron_notrt_res_a,
floor((random()* 0.08 + 0.8)  * chron_notrt_prst)::int as chron_notrt_prst_a,
floor((random()* 0.08 + 0.8)  * chron_notrt_unkn)::int as chron_notrt_unkn_a,
floor((random()* 0.08 + 0.8)  * chron_trt)::int as chron_trt_a,
floor((random()* 0.08 + 0.8)  * chron_trt_unkn)::int as chron_trt_unkn_a,
floor((random()* 0.08 + 0.8)  * chron_trt_vload)::int as chron_trt_vload_a, 
floor((random()* 0.08 + 0.8)  * chron_trt_lastvlneg)::int as chron_trt_lastvlneg_a, 
floor((random()* 0.08 + 0.8)  * chron_trt24_lastvlneg)::int as chron_trt24_lastvlneg_a 
from gen_pop_tools.cc_hepc_summaries;

-- rename columns
ALTER TABLE fake_hepc_a
RENAME total_a TO total;
ALTER TABLE fake_hepc_a
RENAME tested0_a TO tested0;
ALTER TABLE fake_hepc_a
RENAME indexpos0_a TO indexpos0;
ALTER TABLE fake_hepc_a
RENAME vltest0_a TO vltest0;
ALTER TABLE fake_hepc_a
RENAME vload0_a TO vload0;
ALTER TABLE fake_hepc_a
RENAME lastvlpos0_a TO lastvlpos0;
ALTER TABLE fake_hepc_a
RENAME lastvlneg0_a TO lastvlneg0;
ALTER TABLE fake_hepc_a
RENAME tested1_a TO tested1;
ALTER TABLE fake_hepc_a
RENAME indexpos1_a TO indexpos1;
ALTER TABLE fake_hepc_a
RENAME vltest1_a TO vltest1;
ALTER TABLE fake_hepc_a
RENAME vload1_a TO vload1;
ALTER TABLE fake_hepc_a
RENAME lastvlpos1_a TO lastvlpos1;
ALTER TABLE fake_hepc_a
RENAME lastvlneg1_a TO lastvlneg1;
ALTER TABLE fake_hepc_a
RENAME acute_a TO acute;
ALTER TABLE fake_hepc_a
RENAME acute_res_norx_a TO acute_res_norx;
ALTER TABLE fake_hepc_a
RENAME acute_res_prst_a TO acute_res_prst;
ALTER TABLE fake_hepc_a
RENAME acute_res_unkn_a TO acute_res_unkn;
ALTER TABLE fake_hepc_a
RENAME chronic_a TO chronic;
ALTER TABLE fake_hepc_a
RENAME chron_notrt_a TO chron_notrt;
ALTER TABLE fake_hepc_a
RENAME chron_notrt_res_a TO chron_notrt_res;
ALTER TABLE fake_hepc_a
RENAME chron_notrt_prst_a TO chron_notrt_prst;
ALTER TABLE fake_hepc_a
RENAME chron_notrt_unkn_a TO chron_notrt_unkn;
ALTER TABLE fake_hepc_a
RENAME chron_trt_a TO chron_trt;
ALTER TABLE fake_hepc_a
RENAME chron_trt_unkn_a TO chron_trt_unkn;
ALTER TABLE fake_hepc_a
RENAME chron_trt_vload_a TO chron_trt_vload; 
ALTER TABLE fake_hepc_a
RENAME chron_trt_lastvlneg_a TO chron_trt_lastvlneg; 
ALTER TABLE fake_hepc_a
RENAME chron_trt24_lastvlneg_a TO chron_trt24_lastvlneg; 

copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
    '["year","age_group","birth_cohort","sex","race_ethnicity"]'::json filters,
    '{"a":{"name":"Total Population Under Surveillance","description":"Total population observed","nest":"0","pcalc":"a"},"b":{"name":"Patients tested for hepatitis C in selected year or prior years","description":"Patients with an encounter in the selected year(s) whose first test for hepatitis C (elisa or RNA) is in or before selected year","nest":"1","pcalc":"a"},"c":{"name":"Patients with a positive result (RNA or ELISA) in selected year or prior years","description":"Patients with an encounter in the selected year(s) with a positive elisa or RNA hepatitis C result in or before selected year","nest":"2","pcalc":"b"},"d":{"name":"Viral load measured during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with an RNA test (any result value) >= 0 days following first positive hepatitis result","nest":"3","pcalc":"c"},"e":{"name":"Detectable viral load during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with a detectable RNA test >= 0 days following first positive hepatitis result","nest":"4","pcalc":"d"},"f":{"name":"Most recent viral load positive","description":"Patients with an encounter in the selected year(s) whose last viral load was positive","nest":"4","pcalc":"d"}, "g":{"name":"Most recent viral load negative","description":"Patients with an encounter in the selected year(s) whose last viral load was negative","nest":"4","pcalc":"d"},"h":{"name":"Patients tested for hepatitis C in selected year(s)","description":"Patients with an encounter in the selected year(s) whose first test for hepatitis C (elisa or RNA) is in selected year only","nest":"1","pcalc":"a"}, "i":{"name":"Patients with a positive result (RNA or ELISA) in selected year(s)","description":"Patients with an encounter in the selected year(s) with a positive elisa or RNA hepatitis C result in selected year only","nest":"2","pcalc":"h"}, "j":{"name":"Viral load measured during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with an RNA test (any result value) >= 0 days following first positive hepatitis result","nest":"3","pcalc":"i"},"k":{"name":"Detectable viral load during selected year or subsequent years","description":"Patients with an encounter in the selected year(s) with a detectable RNA test >= 0 days following first positive hepatitis result","nest":"4","pcalc":"j"},"l":{"name":"Most recent viral load positive","description":"Patients with an encounter in the selected year(s) whose last viral load was positive","nest":"4","pcalc":"j"}, "m":{"name":"Most recent viral load negative","description":"Patients with an encounter in the selected year(s) whose last viral load was negative","nest":"4","pcalc":"j"}, "n":{"name":"Acute hepatitis C","description":"Patients who met the ESP case definition for acute hep C in the selected year(s)","nest":"1","pcalc":"c"}, "o":{"name":"Resolved, no treatment","description":"Acute hepatitis C cases whose most recent viral load was negative and who have no evidence of ever having been treated","nest":"2","pcalc":"n"}, "p":{"name":"Persistently infected","description":"Acute hepatitis C cases with a positive load at least 1 year following the acute case date or who received medication at least 1 year following the acute case date","nest":"2","pcalc":"n"}, "q":{"name":"Unknown status","description":"Acute hepatitis C cases who have not had a negative viral load within 1 year following case identification or who have not had a viral load measured ≥ 1 year following case identification","nest":"2","pcalc":"n"}, "r":{"name":"Chronic hepatitis C","description":"Patients that had a positive hepatitis C lab and did not spontaneously clear the infection without treatment. Note: this includes acute cases that are persistently infected or have an unknown status","nest":"1","pcalc":"c"}, "s":{"name":"Never on treatment","description":"Chronic hepatitis C cases who have no evidence of being treated","nest":"2","pcalc":"r"},"t":{"name":"Resolved","description":"Chronic hepatitis C cases who have no evidence of being treated and whose last viral load was undetected","nest":"3","pcalc":"s"}, "u":{"name":"Persistently infected","description":"Chronic hepatitis C cases who have no evidence of being treated and whose last viral load was positive","nest":"3","pcalc":"s"}, "v":{"name":"Unknown status","description":"Chronic hepatitis C cases who have no evidence of being treated and have never had a viral load","nest":"3","pcalc":"s"}, "w":{"name":"Received treatment","description":"Chronic hep C cases ever started on HCV treatment","nest":"2","pcalc":"r"}, "x":{"name":"Viral load not measured","description":"Chronic hepatitis C cases who have no follow-up viral load since starting treatment","nest":"3","pcalc":"w"}, "y":{"name":"Viral load measured","description":"Chronic hepatitis C cases who have a follow-up viral load (any result) since starting treatment","nest":"3","pcalc":"w"}, "z":{"name":"Virologic suppression","description":"Chronic hepatitis C cases whose last viral load was undetectable (whether on treatment or post treatment)","nest":"4","pcalc":"y"}, "aa":{"name":"Sustained virological response","description":"Chronic hepatitis C cases with an undetectable viral load who have been off HCV medications for > 24 weeks","nest":"4","pcalc":"y"}}'::json rowmeta,
(select '{'||array_to_string(array_agg(row_),',')||'}' from  (select ('"'||
        case when index_enc_yr = 'x' then 'YEAR' else index_enc_yr end ||
          case when agegroup <> 'x' 
            then (select codeval from gen_pop_tools.cc_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end ||
          case when birth_cohort <> 'x' 
            then (select codeval from gen_pop_tools.cc_birth_cohort_codevals cv where t0.birth_cohort=cv.birth_cohort)
          else 'x' end ||
          case when sex <> 'x' 
            then (select codeval from gen_pop_tools.cc_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end ||
          case when race_ethnicity <> 'x' 
            then (select codeval from gen_pop_tools.cc_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end || '":{"a":'|| total
		  || ',"b":' || tested0
		  || ',"c":' || indexpos0 
		  || ',"d":' || vltest0
		  || ',"e":' || vload0
		  || ',"f":' || lastvlpos0
		  || ',"g":' || lastvlneg0
		  || ',"h":' || tested1 
		  || ',"i":' || indexpos1 
		  || ',"j":' || vltest1  
		  || ',"k":' || vload1 
		  || ',"l":' || lastvlpos1 
		  || ',"m":' || lastvlneg1 
		  || ',"n":' || acute
		  || ',"o":' || acute_res_norx 
		  || ',"p":' || acute_res_prst
		  || ',"q":' || acute_res_unkn 
		  || ',"r":' || chronic
		  || ',"s":' || chron_notrt
		  || ',"t":' || chron_notrt_res
		  || ',"u":' || chron_notrt_prst
		  || ',"v":' || chron_notrt_unkn
		  || ',"w":' || chron_trt
		  || ',"x":' || chron_trt_unkn
		  || ',"y":' || chron_trt_vload
		  || ',"z":' || chron_trt_lastvlneg
		  || ',"aa":' || chron_trt24_lastvlneg
		  || '}') row_
from fake_hepc_a t0 ) t00)::json rowdata) t1) to '/tmp/sitea_hepc_monthly.json';


-- HIV
-- site A
drop table if exists fake_hiv_a;
create table fake_hiv_a as
select hiv_yr, agegroup, sex, race_ethnicity,
floor((random()* 0.08 + 0.89)  * total):: int as total_a,
floor((random()* 0.08 + 0.89)  * scnd_enc):: int as scnd_enc_a,
floor((random()* 0.08 + 0.89)  * hiv_dx):: int as hiv_dx_a,
floor((random()* 0.08 + 0.89)  * hiv_rx):: int as hiv_rx_a,
floor((random()* 0.08 + 0.89)  * retained):: int as retained_a,
floor((random()* 0.08 + 0.89)  * vload):: int as vload_a,
floor((random()* 0.08 + 0.89)  * vl_rx):: int as vl_rx_a,
floor((random()* 0.08 + 0.89)  * sprsd):: int as sprsd_a ,
floor((random()* 0.08 + 0.89)  * spr_rx):: int as spr_rx_a,
floor((random()* 0.08 + 0.89)  * opt):: int as opt_a ,
floor((random()* 0.08 + 0.89)  * tot_csyr):: int as tot_csyr_a,
floor((random()* 0.08 + 0.89)  * linked):: int as linked_a,
floor((random()* 0.08 + 0.89)  * scnd_enc_csyr):: int as scnd_enc_csyr_a,
floor((random()* 0.08 + 0.89)  * hiv_dx_csyr):: int as hiv_dx_csyr_a,
floor((random()* 0.08 + 0.89)  * hiv_rx_csyr):: int as hiv_rx_csyr_a,
floor((random()* 0.08 + 0.89)  * retained_csyr):: int as retained_csyr_a ,
floor((random()* 0.08 + 0.89)  * vload_csyr):: int as vload_csyr_a,
floor((random()* 0.08 + 0.89)  * vl_rx_csyr):: int as vl_rx_csyr_a,
floor((random()* 0.08 + 0.89)  * sprsd_csyr):: int as sprsd_csyr_a,
floor((random()* 0.08 + 0.89)  * opt_csyr):: int as opt_csyr_a
from gen_pop_tools.cc_hiv_summaries;
  
-- site B
drop table if exists fake_hiv_b;
create table fake_hiv_b as
select hiv_yr, agegroup, sex, race_ethnicity,
floor((random()* 0.08 + 0.92)  * total):: int as total_a,
floor((random()* 0.08 + 0.92)  * scnd_enc):: int as scnd_enc_a,
floor((random()* 0.08 + 0.92)  * hiv_dx):: int as hiv_dx_a,
floor((random()* 0.08 + 0.92)  * hiv_rx):: int as hiv_rx_a,
floor((random()* 0.08 + 0.92)  * retained):: int as retained_a,
floor((random()* 0.08 + 0.92)  * vload):: int as vload_a,
floor((random()* 0.08 + 0.92)  * vl_rx):: int as vl_rx_a,
floor((random()* 0.08 + 0.92)  * sprsd):: int as sprsd_a ,
floor((random()* 0.08 + 0.92)  * spr_rx):: int as spr_rx_a,
floor((random()* 0.08 + 0.92)  * opt):: int as opt_a ,
floor((random()* 0.08 + 0.92)  * tot_csyr):: int as tot_csyr_a,
floor((random()* 0.08 + 0.92)  * linked):: int as linked_a,
floor((random()* 0.08 + 0.92)  * scnd_enc_csyr):: int as scnd_enc_csyr_a,
floor((random()* 0.08 + 0.92)  * hiv_dx_csyr):: int as hiv_dx_csyr_a,
floor((random()* 0.08 + 0.92)  * hiv_rx_csyr):: int as hiv_rx_csyr_a,
floor((random()* 0.08 + 0.92)  * retained_csyr):: int as retained_csyr_a ,
floor((random()* 0.08 + 0.92)  * vload_csyr):: int as vload_csyr_a,
floor((random()* 0.08 + 0.92)  * vl_rx_csyr):: int as vl_rx_csyr_a,
floor((random()* 0.08 + 0.92)  * sprsd_csyr):: int as sprsd_csyr_a,
floor((random()* 0.08 + 0.92)  * opt_csyr):: int as opt_csyr_a
from gen_pop_tools.cc_hiv_summaries;
  
-- Rename columns
ALTER Table fake_hiv_a
Rename total_a TO total;
ALTER Table fake_hiv_a
Rename scnd_enc_a TO scnd_enc;
ALTER Table fake_hiv_a
Rename hiv_dx_a TO hiv_dx;
ALTER Table fake_hiv_a
Rename hiv_rx_a TO hiv_rx;
ALTER Table fake_hiv_a
Rename retained_a TO retained;
ALTER Table fake_hiv_a
Rename vload_a TO vload;
ALTER Table fake_hiv_a
Rename vl_rx_a TO vl_rx;
ALTER Table fake_hiv_a
Rename sprsd_a TO sprsd;
ALTER Table fake_hiv_a
Rename spr_rx_a TO spr_rx;
ALTER Table fake_hiv_a
Rename opt_a TO opt;
ALTER Table fake_hiv_a
Rename tot_csyr_a TO tot_csyr;
ALTER Table fake_hiv_a
Rename linked_a TO linked;
ALTER Table fake_hiv_a
Rename scnd_enc_csyr_a TO scnd_enc_csyr;
ALTER Table fake_hiv_a
Rename hiv_dx_csyr_a TO hiv_dx_csyr;
ALTER Table fake_hiv_a
Rename hiv_rx_csyr_a TO hiv_rx_csyr;
ALTER Table fake_hiv_a
Rename retained_csyr_a TO retained_csyr;
ALTER Table fake_hiv_a
Rename vload_csyr_a TO vload_csyr;
ALTER Table fake_hiv_a
Rename vl_rx_csyr_a TO vl_rx_csyr;
ALTER Table fake_hiv_a
Rename sprsd_csyr_a TO sprsd_csyr;
ALTER Table fake_hiv_a
Rename opt_csyr_a TO opt_csyr;

copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
    '["year","age_group","sex","race_ethnicity"]'::json filters,
'{"a":{"name":"HIV patients in care","description":"Patients meeting the ESP case definition for HIV with at least one encounter for any reason during the selected period.","nest":"0","pcalc":"a"},"b":{"name":"Follow-up encounter","description":"Second encounter following qualifying encounter","nest":"1","pcalc":"a"},"c":{"name":"Follow-up encounter with HIV diagnosis code","description":"Encounter with an HIV diagnosis code following the qualifying encounter","nest":"1","pcalc":"a"},"d":{"name":"Patients prescribed HIV medications","description":"Prescription for ARTs during the selected period or later","nest":"1","pcalc":"a"},"e":{"name":"Retained in care","description":"HIV individuals who had two or more of the following performed at least three months apart following the qualifying encounter: viral load test, CD4+ test, encounter with an HIV diagnosis, or prescriptions for HIV medications.","nest":"1","pcalc":"a"},"f":{"name":"Patients with a viral load test in or after the selected period","description":"Viral load with any value during the selected period","nest":"1","pcalc":"a"},"g":{"name":"Patients with a viral load test and prescribed HIV medications","description": "HIV patients receiving medications with viral load test ","nest":"1","pcalc":"a"},"h":{"name":"Virally suppressed","description":"Patients whose most recent HIV viral load was less than 200 copies/mL.","nest":"1","pcalc":"a"},"i":{"name":"Virally suppressed and on medications","description":"Patients receiving medications and whose most recent HIV viral load was less than 200 copies/mL.","nest":"1","pcalc":"a"},"j":{"name":"Opportunistic Infection","description":"Patients with a diagnosis code for any opportunisitic infection in or after the selected period","nest":"1","pcalc":"a"},"k":{"name":"Patients newly diagnosed with HIV during the selected period","description":"Patients with a positive (+Western Blot or +ELISA or +Ab/Ag) on or before the case identification date or history of negative (HIV ELISA or Ab/Ag) within 2 years preceding the case identification date.","nest":"0","pcalc":"k"},"l":{"name":"Linked to care","description":"Patients diagnosed with HIV during the selected period who had at least one viral load or CD4 test or prescription for an HIV medication within 0-90 days following diagnosis ","nest":"1","pcalc":"k"},"m":{"name":"Follow-up encounter","description":"Encounter following case detection","nest":"1","pcalc":"k"},"n":{"name":"Follow-up encounter with HIV diagnosis code","description":"Encounter with an HIV diagnosis code in the selected period or later","nest":"1","pcalc":"k"},"o":{"name":"Patients prescribed HIV medications","description":"Prescription for HIV medications in the selected period or later","nest":"1","pcalc":"k"},"p":{"name":"Retained in care ","description":"Diagnosed individuals who had two or more of the following performed at least three months apart: viral load test, CD4+ test, encounter with an HIV diagnosis, or prescriptions for HIV medications.","nest":"1","pcalc":"k"},"q":{"name":"Patients with a viral load test","description":"Viral load with any value following case identification","nest":"1","pcalc":"k"},"r":{"name":"Patients with a viral load test and prescribed HIV medications","description":"Any value, any time after case detection","nest":"1","pcalc":"k"},"s":{"name":"Virally suppressed","description":"Patients whose most recent HIV viral load was less than 200 copies/mL.","nest":"1","pcalc":"k"},"t":{"name":"Opportunistic Infection","description":"Patients with a diagnosis code for any opportunistic infection following case detection.","nest":"1","pcalc":"k"}}'::json rowmeta,
(select '{'||array_to_string(array_agg(row_),',')||'}' from  (select ('"'||
          case when hiv_yr = 'x' then 'YEAR' else hiv_yr end ||
          case when agegroup <> 'x' 
            then (select codeval from gen_pop_tools.cc_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end ||
          case when sex <> 'x' 
            then (select codeval from gen_pop_tools.cc_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end ||
          case when race_ethnicity <> 'x' 
            then (select codeval from gen_pop_tools.cc_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end || '":{"a":'|| total
		  || ',"b":' || scnd_enc
		  || ',"c":' || hiv_dx
		  || ',"d":' || hiv_rx
		  || ',"e":' || retained
		  || ',"f":' || vload
		  || ',"g":' || vl_rx
		  || ',"h":' || sprsd
		  || ',"i":' || spr_rx
		  || ',"j":' || opt
		  || ',"k":' || tot_csyr
		  || ',"l":' || linked
		  || ',"m":' || scnd_enc_csyr
		  || ',"n":' || hiv_dx_csyr
		  || ',"o":' || hiv_rx_csyr
		  || ',"p":' || retained_csyr
		  || ',"q":' || vload_csyr
		  || ',"r":' || vl_rx_csyr
		  || ',"s":' || sprsd_csyr
		  || ',"t":' || opt_csyr
		  || '}') row_
from fake_hiv_a t0 ) t00)::json rowdata) t1) to '/tmp/sitea_hiv_monthly.json';

-- hiv risk
-- site A
drop table if exists fake_hiv_risk_a;
create table fake_hiv_risk_a as
select rpt_yr, agegroup, sex, race_ethnicity, 
floor((random()* 0.08 + 0.93)  *  total )::numeric(15,1) as total_a,
floor((random()* 0.08 + 0.93)  *  hiv_risk_85 )::numeric(15,1) as hiv_risk_85_a,
floor((random()* 0.08 + 0.93)  *  hiv_risk_10_85 )::numeric(15,1) as hiv_risk_10_85_a,
floor((random()* 0.08 + 0.93)  *  hiv_risk_5_10 )::numeric(15,1) as hiv_risk_5_10_a,
floor((random()* 0.08 + 0.93)  *  subsequent_hiv_85 )::numeric(15,1) as subsequent_hiv_85_a,
floor((random()* 0.08 + 0.93)  *  subsequent_hiv_10_85 )::numeric(15,1) as subsequent_hiv_10_85_a,
floor((random()* 0.08 + 0.93)  *  subsequent_hiv_5_10 )::numeric(15,1) as subsequent_hiv_5_10_a,
floor((random()* 0.08 + 0.93)  *  tested_at_risk_85 )::numeric(15,1) as tested_at_risk_85_a,
floor((random()* 0.08 + 0.93)  *  tested_at_risk_10_85 )::numeric(15,1) as tested_at_risk_10_85_a,
floor((random()* 0.08 + 0.93)  *  tested_at_risk_5_10 )::numeric(15,1) as tested_at_risk_5_10_a,
floor((random()* 0.08 + 0.93)  *  truvada_rx_85 )::numeric(15,1) as truvada_rx_85_a,
floor((random()* 0.08 + 0.93)  *  truvada_rx_10_85 )::numeric(15,1) as truvada_rx_10_85_a,
floor((random()* 0.08 + 0.93)  *  truvada_rx_5_10 )::numeric(15,1) as truvada_rx_5_10_a,
floor((random()* 0.08 + 0.93)  *  truvada_hiv_85 )::numeric(15,1) as truvada_hiv_85_a,
floor((random()* 0.08 + 0.93)  *  truvada_hiv_10_85 )::numeric(15,1) as truvada_hiv_10_85_a,
floor((random()* 0.08 + 0.93)  *  truvada_hiv_5_10 )::numeric(15,1) as truvada_hiv_5_10_a
from gen_pop_tools.cc_hiv_risk_summaries

-- site B
drop table if exists fake_hiv_risk_b;
create table fake_hiv_risk_b as
select rpt_yr, agegroup, sex, race_ethnicity, 
floor((random()* 0.08 + 0.87)  *  total )::numeric(15,1) as total_a,
floor((random()* 0.08 + 0.87)  *  hiv_risk_85 )::numeric(15,1) as hiv_risk_85_a,
floor((random()* 0.08 + 0.87)  *  hiv_risk_10_85 )::numeric(15,1) as hiv_risk_10_85_a,
floor((random()* 0.08 + 0.87)  *  hiv_risk_5_10 )::numeric(15,1) as hiv_risk_5_10_a,
floor((random()* 0.08 + 0.87)  *  subsequent_hiv_85 )::numeric(15,1) as subsequent_hiv_85_a,
floor((random()* 0.08 + 0.87)  *  subsequent_hiv_10_85 )::numeric(15,1) as subsequent_hiv_10_85_a,
floor((random()* 0.08 + 0.87)  *  subsequent_hiv_5_10 )::numeric(15,1) as subsequent_hiv_5_10_a,
floor((random()* 0.08 + 0.87)  *  tested_at_risk_85 )::numeric(15,1) as tested_at_risk_85_a,
floor((random()* 0.08 + 0.87)  *  tested_at_risk_10_85 )::numeric(15,1) as tested_at_risk_10_85_a,
floor((random()* 0.08 + 0.87)  *  tested_at_risk_5_10 )::numeric(15,1) as tested_at_risk_5_10_a,
floor((random()* 0.08 + 0.87)  *  truvada_rx_85 )::numeric(15,1) as truvada_rx_85_a,
floor((random()* 0.08 + 0.87)  *  truvada_rx_10_85 )::numeric(15,1) as truvada_rx_10_85_a,
floor((random()* 0.08 + 0.87)  *  truvada_rx_5_10 )::numeric(15,1) as truvada_rx_5_10_a,
floor((random()* 0.08 + 0.87)  *  truvada_hiv_85 )::numeric(15,1) as truvada_hiv_85_a,
floor((random()* 0.08 + 0.87)  *  truvada_hiv_10_85 )::numeric(15,1) as truvada_hiv_10_85_a,
floor((random()* 0.08 + 0.87)  *  truvada_hiv_5_10 )::numeric(15,1) as truvada_hiv_5_10_a
from gen_pop_tools.cc_hiv_risk_summaries


-- Rename columns
ALTER Table fake_hiv_risk_a
Rename total_a TO total;
ALTER Table fake_hiv_risk_a
Rename hiv_risk_85_a TO hiv_risk_85 ;
ALTER Table fake_hiv_risk_a
Rename hiv_risk_10_85_a TO hiv_risk_10_85 ;
ALTER Table fake_hiv_risk_a
Rename hiv_risk_5_10_a TO hiv_risk_5_10 ;
ALTER Table fake_hiv_risk_a
Rename subsequent_hiv_85_a TO subsequent_hiv_85 ;
ALTER Table fake_hiv_risk_a
Rename subsequent_hiv_10_85_a TO subsequent_hiv_10_85 ;
ALTER Table fake_hiv_risk_a
Rename subsequent_hiv_5_10_a TO subsequent_hiv_5_10 ;
ALTER Table fake_hiv_risk_a
Rename tested_at_risk_85_a TO tested_at_risk_85 ;
ALTER Table fake_hiv_risk_a
Rename tested_at_risk_10_85_a TO tested_at_risk_10_85 ;
ALTER Table fake_hiv_risk_a
Rename tested_at_risk_5_10_a TO tested_at_risk_5_10 ;
ALTER Table fake_hiv_risk_a
Rename truvada_rx_85_a TO truvada_rx_85 ;
ALTER Table fake_hiv_risk_a
Rename truvada_rx_10_85_a TO truvada_rx_10_85 ;
ALTER Table fake_hiv_risk_a
Rename truvada_rx_5_10_a TO truvada_rx_5_10 ;
ALTER Table fake_hiv_risk_a
Rename truvada_hiv_85_a TO truvada_hiv_85 ;
ALTER Table fake_hiv_risk_a
Rename truvada_hiv_10_85_a TO truvada_hiv_10_85 ;
ALTER Table fake_hiv_risk_a
Rename truvada_hiv_5_10_a TO truvada_hiv_5_10 ;

copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
    '["year","age_group","sex","race_ethnicity"]'::json filters,
'{"a":{"name":"Non-HIV patients, total with calculable risk","description":"Patients with sufficient data to calculate a score for risk of HIV acquisition during the measurent period according to an electronic prediction rule.","nest":"0","pcalc":"a"},"b":{"name":"Absolute risk of HIV acquisition ≥1%","description":"This is ≥85 times the MA incidence of 12.1 per 100,000","nest":"1","pcalc":"a"},"c":{"name":"Subsequently acquired HIV","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who subsequently acquired HIV.","nest":"2","pcalc":"b"},"d":{"name":"Tested for HIV during period at risk","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who recieved any HIV test","nest":"2","pcalc":"b"},"e":{"name":"Prescribed Truvada during period at risk","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada","nest":"2","pcalc":"b"},"f":{"name":"Subsequently acquired HIV","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada during the measurement period, and subsequently acquired HIV.","nest":"3","pcalc":"e"},"g":{"name":"“Absolute risk of HIV acquisition 0.1 – 0.99%","description":"This is approx. ≥10 times the MA incidence of 12.1 per 100,000","nest":"1","pcalc":"a"},"h":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who subsequently acquired HIV.","nest":"2","pcalc":"g"},"i":{"name":"Tested for HIV during period at risk","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who recieved any HIV test","nest":"2","pcalc":"g"},"j":{"name":"Prescribed Truvada during period at risk","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada","nest":"2","pcalc":"g"},"k":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada during the measurement period, and subsequently acquired HIV.","nest":"3","pcalc":"j"},"l":{"name":"Absolute risk of HIV acquisition 0.05 – 0.09%","description":"This is approx. ≥5 times the MA incidence of 12.1 per 100,000","nest":"1","pcalc":"a"},"m":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who subsequently acquired HIV.","nest":"2","pcalc":"l"},"n":{"name":"Tested for HIV during period at risk","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who recieved any HIV test","nest":"2","pcalc":"l"},"o":{"name":"Prescribed Truvada during period at risk","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada","nest":"2","pcalc":"l"},"p":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada during the measurement period, and subsequently acquired HIV.","nest":"3","pcalc":"o"}}'::json rowmeta,
(select '{'||array_to_string(array_agg(row_),',')||'}' from  (select ('"'||
          case when rpt_yr = 'x' then 'YEAR' else rpt_yr end ||
          case when agegroup <> 'x' 
            then (select codeval from gen_pop_tools.cc_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end ||
          case when sex <> 'x' 
            then (select codeval from gen_pop_tools.cc_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end ||
          case when race_ethnicity <> 'x' 
            then (select codeval from gen_pop_tools.cc_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end || '":{"a":'|| rtrim(to_char(total,'FM99999999999999.9'), '.')
		  || ',"b":' || rtrim(to_char(hiv_risk_85,'FM99999999999999.9'), '.')
		  || ',"c":' || rtrim(to_char(subsequent_hiv_85,'FM99999999999999.9'), '.')
		  || ',"d":' || rtrim(to_char(tested_at_risk_85,'FM99999999999999.9'), '.')
		  || ',"e":' || rtrim(to_char(truvada_rx_85,'FM99999999999999.9'), '.')
		  || ',"f":' || rtrim(to_char(truvada_hiv_85,'FM99999999999999.9'), '.')
		  || ',"g":' || rtrim(to_char(hiv_risk_10_85,'FM99999999999999.9'), '.')
		  || ',"h":' || rtrim(to_char(subsequent_hiv_10_85,'FM99999999999999.9'), '.')
		  || ',"i":' || rtrim(to_char(tested_at_risk_10_85,'FM99999999999999.9'), '.')
		  || ',"j":' || rtrim(to_char(truvada_rx_10_85,'FM99999999999999.9'), '.')
		  || ',"k":' || rtrim(to_char(truvada_hiv_10_85,'FM99999999999999.9'), '.')
		  || ',"l":' || rtrim(to_char(hiv_risk_5_10,'FM99999999999999.9'), '.')
		  || ',"m":' || rtrim(to_char(subsequent_hiv_5_10,'FM99999999999999.9'), '.')
		  || ',"n":' || rtrim(to_char(tested_at_risk_5_10,'FM99999999999999.9'), '.')
		  || ',"o":' || rtrim(to_char(truvada_rx_5_10,'FM99999999999999.9'), '.')
		  || ',"p":' || rtrim(to_char(truvada_hiv_5_10,'FM99999999999999.9'), '.')
		  || '}') row_
from fake_hiv_risk_a t0 ) t00)::json rowdata) t1) to '/tmp/sitea_hiv_risk_monthly.json';

-- diabetes
-- site A
drop table if exists fake_diab_a;
create table fake_diab_a as
select index_enc_yr, agegroup, sex, race_ethnicity,
floor((random()* 0.08 + 0.91)  *  total)::integer as total_a,
floor((random()* 0.08 + 0.91)  *  a1ctest)::integer as a1ctest_a,
floor((random()* 0.08 + 0.91)  *  a1ctest3)::integer as  a1ctest3_a,
floor((random()* 0.08 + 0.91)  *  a1ctestever)::integer as a1ctestever_a,
floor((random()* 0.08 + 0.91)  *  diabetic)::integer as diabetic_a,
floor((random()* 0.08 + 0.91)  *  norx)::integer as norx_a,
floor((random()* 0.08 + 0.91)  *  norx7)::integer as norx7_a,
floor((random()* 0.08 + 0.91)  *  norx79)::integer as norx79_a,
floor((random()* 0.08 + 0.91)  *  norx9)::integer as norx9_a,
floor((random()* 0.08 + 0.91)  *  norx_noa1c)::integer as norx_noa1c_a,
floor((random()* 0.08 + 0.91)  *  rx)::integer as rx_a,
floor((random()* 0.08 + 0.91)  *  rx7)::integer as rx7_a,
floor((random()* 0.08 + 0.91)  *  rx79)::integer as rx79_a,
floor((random()* 0.08 + 0.91)  *  rx9)::integer as rx9_a,
floor((random()* 0.08 + 0.91)  *  rx_noa1c)::integer as rx_noa1c_a,
floor((random()* 0.08 + 0.91)  *  all7)::integer as all7_a,
floor((random()* 0.08 + 0.91)  *  all79)::integer as all79_a,
floor((random()* 0.08 + 0.91)  *  all9)::integer as all9_a,
floor((random()* 0.08 + 0.91)  *  all_noa1c)::integer as all_noa1c_a
from gen_pop_tools.cc_diab_summaries

-- site B
drop table if exists fake_diab_b;
create table fake_diab_b as
select index_enc_yr, agegroup, sex, race_ethnicity,
floor((random()* 0.08 + 0.94)  *  total)::integer as total_a,
floor((random()* 0.08 + 0.94)  *  a1ctest)::integer as a1ctest_a,
floor((random()* 0.08 + 0.94)  *  a1ctest3)::integer as  a1ctest3_a,
floor((random()* 0.08 + 0.94)  *  a1ctestever)::integer as a1ctestever_a,
floor((random()* 0.08 + 0.94)  *  diabetic)::integer as diabetic_a,
floor((random()* 0.08 + 0.94)  *  norx)::integer as norx_a,
floor((random()* 0.08 + 0.94)  *  norx7)::integer as norx7_a,
floor((random()* 0.08 + 0.94)  *  norx79)::integer as norx79_a,
floor((random()* 0.08 + 0.94)  *  norx9)::integer as norx9_a,
floor((random()* 0.08 + 0.94)  *  norx_noa1c)::integer as norx_noa1c_a,
floor((random()* 0.08 + 0.94)  *  rx)::integer as rx_a,
floor((random()* 0.08 + 0.94)  *  rx7)::integer as rx7_a,
floor((random()* 0.08 + 0.94)  *  rx79)::integer as rx79_a,
floor((random()* 0.08 + 0.94)  *  rx9)::integer as rx9_a,
floor((random()* 0.08 + 0.94)  *  rx_noa1c)::integer as rx_noa1c_a,
floor((random()* 0.08 + 0.94)  *  all7)::integer as all7_a,
floor((random()* 0.08 + 0.94)  *  all79)::integer as all79_a,
floor((random()* 0.08 + 0.94)  *  all9)::integer as all9_a,
floor((random()* 0.08 + 0.94)  *  all_noa1c)::integer as all_noa1c_a
from gen_pop_tools.cc_diab_summaries

-- Rename columns

ALTER Table fake_diab_b
Rename total_a TO total;
ALTER Table fake_diab_b
Rename a1ctest_a TO a1ctest;
ALTER Table fake_diab_b
Rename a1ctest3_a TO  a1ctest3;
ALTER Table fake_diab_b
Rename a1ctestever_a TO a1ctestever;
ALTER Table fake_diab_b
Rename diabetic_a TO diabetic;
ALTER Table fake_diab_b
Rename norx_a TO norx;
ALTER Table fake_diab_b
Rename norx7_a TO norx7;
ALTER Table fake_diab_b
Rename norx79_a TO norx79;
ALTER Table fake_diab_b
Rename norx9_a TO norx9;
ALTER Table fake_diab_b
Rename norx_noa1c_a TO norx_noa1c;
ALTER Table fake_diab_b
Rename rx_a TO rx;
ALTER Table fake_diab_b
Rename rx7_a TO rx7;
ALTER Table fake_diab_b
Rename rx79_a TO rx79;
ALTER Table fake_diab_b
Rename rx9_a TO rx9;
ALTER Table fake_diab_b
Rename rx_noa1c_a TO rx_noa1c;
ALTER Table fake_diab_b
Rename all7_a TO all7;
ALTER Table fake_diab_b
Rename all79_a TO all79;
ALTER Table fake_diab_b
Rename all9_a TO all9;
ALTER Table fake_diab_b
Rename all_noa1c_a TO all_noa1c;

copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
    '["year","age_group","sex","race_ethnicity"]'::json filters,
    '{"a":{"name":"Individuals with at least one encounter in the chosen year(s)","description":"Total population observed","nest":"0","pcalc":"a"},"b":{"name":"≥1 hemoglobin A1C test in the index year","description":"Count of individuals with ≥1 hemoglobin A1C test in the index year","nest":"1","pcalc":"a"},"c":{"name":"≥1 hemoglobin A1C test within the index or 2 preceding years","description":"Count of individuals with ≥1 hemoglobin A1C test within the index or 2 prior years","nest":"1","pcalc":"a"},"d":{"name":"≥1 hemoglobin A1C test ever","description":"Count of individuals with ≥1 hemoglobin A1C test ever","nest":"1","pcalc":"a"},"e":{"name":"Count of individuals with diabetes","description":"Count of patients with diabetes","nest":"1","pcalc":"a"},"f":{"name":"Last hemoglobin A1C <7.0","description":"Count of diabetes patients with hemoglobin A1C <7.0 in the preceding 2 years","nest":"2","pcalc":"e"},"g":{"name":"Last hemoglobin A1C 7-9","description":"Count of diabetes patients with hemoglobin A1C 7-9 in the preceding 2 years","nest":"2","pcalc":"e"},"h":{"name":"Last hemoglobin A1C >9","description":"Count of diabetes patients with hemoglobin A1C >9 in the preceding 2 years","nest":"2","pcalc":"e"},"i":{"name":"Hemoglobin A1C not checked","description":"Count of patients in whom hemoglobin A1C not checked in preceding 2 years","nest":"2","pcalc":"e"},"j":{"name":"Count of individuals with diabetes NOT on diabetes meds ","description":"Count of diabetes patients not receiving diabetes meds in the preceding 2 years","nest":"2","pcalc":"e"},"k":{"name":"Last hemoglobin A1C <7.0","description":"Count of diabetes patients not receiving diabetes meds whose most recent hemoglobin A1C <7.0 in the preceding 2 years","nest":"3","pcalc":"j"},"l":{"name":"Last hemoglobin A1C 7-9","description":"Count of diabetes patients not receiving diabetes meds whose most recent hemoglobin A1C 7-9 in the  preceding 2 years","nest":"3","pcalc":"j"},"m":{"name":"Last hemoglobin A1C >9","description":"Count of diabetes patients not receiving diabetes meds whose most recent hemoglobin A1C >9 in the preceding 2 years","nest":"3","pcalc":"j"},"n":{"name":"Hemoglobin A1C not checked","description":"Count of patients not receiving diabetes meds in whom hemoglobin A1C was not checked in the preceding 2 years","nest":"3","pcalc":"j"},"o":{"name":"Count of individuals with diabetes on meds","description":"Count of diabetes patients prescribed diabetes meds in preceding 2 years","nest":"2","pcalc":"e"},"p":{"name":"Last hemoglobin A1C <7.0","description":"Count of diabetes patients receiving diabetes meds whose most recent hemoglobin A1C <7.0 in the preceding 2 years","nest":"3","pcalc":"o"},"q":{"name":"Last hemoglobin A1C 7-9","description":"Count of diabetes patients prescribed diabetes meds whose most recent hemoglobin A1C 7-9 in preceding 2 years","nest":"3","pcalc":"o"},"r":{"name":"Last hemoglobin A1C >9","description":"Count of diabetes patients prescribed diabetes meds whose most recent hemoglobin A1C >9 in the preceding 2 years","nest":"3","pcalc":"o"},"s":{"name":"Hemoglobin A1C not checked","description":"Count of patients prescribed diabetes meds whose hemoglobin A1C not checked in preceding 2 years","nest":"3","pcalc":"o"}}'::json rowmeta,
(select '{'||array_to_string(array_agg(row_),',')||'}' from  (select ('"'||
        case when index_enc_yr = 'x' then 'YEAR' else index_enc_yr end ||
          case when agegroup <> 'x' 
            then (select codeval from gen_pop_tools.cc_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end ||
          case when sex <> 'x' 
            then (select codeval from gen_pop_tools.cc_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end ||
          case when race_ethnicity <> 'x' 
            then (select codeval from gen_pop_tools.cc_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end || '":{"a":'|| total
		  || ',"b":' || a1ctest
		  || ',"c":' || a1ctest3
		  || ',"d":' || a1ctestever
		  || ',"e":' || diabetic
		  || ',"f":' || all7
		  || ',"g":' || all79
		  || ',"h":' || all9
		  || ',"i":' || all_noa1c
		  || ',"j":' || norx
		  || ',"k":' || norx7
		  || ',"l":' || norx79 
		  || ',"m":' || norx9
		  || ',"n":' || norx_noa1c		  
		  || ',"o":' || rx 
		  || ',"p":' || rx7 
		  || ',"q":' || rx79 
		  || ',"r":' || rx9 
		  || ',"s":' || rx_noa1c 
		  || '}') row_
from fake_diab_a t0 ) t00)::json rowdata) t1) to '/tmp/sitea_diab_monthly.json';

-- cardiac risk
-- site A
drop table if exists fake_cv_a;
create table fake_cv_a as
select rpt_yr, agegroup, sex, race_ethnicity, risk_percentile,
floor((random()* 0.08 + 0.9) * total)::numeric(15,1) as total_a,
floor((random()* 0.08 + 0.9) * cv_risk_smk )::numeric(15,1) as cv_risk_smk_a,
floor((random()* 0.08 + 0.9) * cv_risk_smk_bn )::numeric(15,1) as cv_risk_smk_bn_a,
floor((random()* 0.08 + 0.9) * cv_risk_a1c )::numeric(15,1) as cv_risk_a1c_a,
floor((random()* 0.08 + 0.9) * cv_risk_a1ct )::numeric(15,1) as cv_risk_a1ct_a,
floor((random()* 0.08 + 0.9) * cv_risk_noa1c )::numeric(15,1) as cv_risk_noa1c_a,
floor((random()* 0.08 + 0.9) * cv_risk_ldl )::numeric(15,1) as cv_risk_ldl_a,
floor((random()* 0.08 + 0.9) * cv_risk_ldl_s )::numeric(15,1) as cv_risk_ldl_s_a,
floor((random()* 0.08 + 0.9) * cv_risk_ldlu )::numeric(15,1) as cv_risk_ldlu_a,
floor((random()* 0.08 + 0.9) * cv_risk_hyp )::numeric(15,1) as cv_risk_hyp_a,
floor((random()* 0.08 + 0.9) * cv_risk_hypu )::numeric(15,1) as cv_risk_hypu_a,
floor((random()* 0.08 + 0.9) * cv_risk_sal )::numeric(15,1) as cv_risk_sal_a
from gen_pop_tools.cc_cr_risk_summaries

-- site B
drop table if exists fake_cv_b;
create table fake_cv_b as
select rpt_yr, agegroup, sex, race_ethnicity, risk_percentile,
floor((random()* 0.08 + 0.95) * total)::numeric(15,1) as total_a,
floor((random()* 0.08 + 0.95) * cv_risk_smk )::numeric(15,1) as cv_risk_smk_a,
floor((random()* 0.08 + 0.95) * cv_risk_smk_bn )::numeric(15,1) as cv_risk_smk_bn_a,
floor((random()* 0.08 + 0.95) * cv_risk_a1c )::numeric(15,1) as cv_risk_a1c_a,
floor((random()* 0.08 + 0.95) * cv_risk_a1ct )::numeric(15,1) as cv_risk_a1ct_a,
floor((random()* 0.08 + 0.95) * cv_risk_noa1c )::numeric(15,1) as cv_risk_noa1c_a,
floor((random()* 0.08 + 0.95) * cv_risk_ldl )::numeric(15,1) as cv_risk_ldl_a,
floor((random()* 0.08 + 0.95) * cv_risk_ldl_s )::numeric(15,1) as cv_risk_ldl_s_a,
floor((random()* 0.08 + 0.95) * cv_risk_ldlu )::numeric(15,1) as cv_risk_ldlu_a,
floor((random()* 0.08 + 0.95) * cv_risk_hyp )::numeric(15,1) as cv_risk_hyp_a,
floor((random()* 0.08 + 0.95) * cv_risk_hypu )::numeric(15,1) as cv_risk_hypu_a,
floor((random()* 0.08 + 0.95) * cv_risk_sal )::numeric(15,1) as cv_risk_sal_a
from gen_pop_tools.cc_cr_risk_summaries

-- drop columns
ALTER Table fake_cv_a
Rename total_a TO total;
ALTER Table fake_cv_a
Rename cv_risk_smk_a TO cv_risk_smk;
ALTER Table fake_cv_a
Rename cv_risk_smk_bn_a TO cv_risk_smk_bn;
ALTER Table fake_cv_a
Rename cv_risk_a1c_a TO cv_risk_a1c;
ALTER Table fake_cv_a
Rename cv_risk_a1ct_a TO cv_risk_a1ct;
ALTER Table fake_cv_a
Rename cv_risk_noa1c_a TO cv_risk_noa1c;
ALTER Table fake_cv_a
Rename cv_risk_ldl_a TO cv_risk_ldl;
ALTER Table fake_cv_a
Rename cv_risk_ldl_s_a TO cv_risk_ldl_s;
ALTER Table fake_cv_a
Rename cv_risk_ldlu_a TO cv_risk_ldlu;
ALTER Table fake_cv_a
Rename cv_risk_hyp_a TO cv_risk_hyp;
ALTER Table fake_cv_a
Rename cv_risk_hypu_a TO cv_risk_hypu;
ALTER Table fake_cv_a
Rename cv_risk_sal_a TO cv_risk_sal;

copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
'["year","age_group","sex","race_ethnicity","risk_percentile"]'::json filters,
'{"a":{"name":"All patients with calculable risk or presence of disease","description":"Patients with sufficient data to determine presence of cardiovascular disease, or calculate a score for risk according to the American College of Cardiologyâ€™s Atherosclerotic Cardiovascular Disease (ASCVD) Risk Score algorithm.","nest":"0","pcalc":"a"},"b":{"name":"Current smoker","description":"Patients who are current smokers.","nest":"1","pcalc":"a"},"c":{"name":"Current smoker with bupropion or nicotine replacement prescription","description":"Patients who are current smokers and also have a prescription for bupropion or nicotine replacement therapy.","nest":"2","pcalc":"b"},"d":{"name":"Patients with A1C>9","description":"Patients whose most recent hemoglobin A1C test value is greater than 9.","nest":"1","pcalc":"a"},"e":{"name":"Patients with A1C>9 on antidiabetic meds","description":"Patients whose most recent hemoglobin A1C test value is greater than 9, and who had a prescription for an antidiabetic in the past year.","nest":"2","pcalc":"d"},"f":{"name":"Patients with unmeasured A1C","description":"Patients who have not had a hemoglobin A1C test in the prior 3 years.","nest":"1","pcalc":"a"},"g":{"name":"Patients with LDL>160","description":"Patients whose most recent LDL cholesterol test within the past two years was above 160.","nest":"1","pcalc":"a"},"h":{"name":"Patients with LDL>160 on statin","description":"Patients whose most recent LDL cholesterol test within the past two years was above 160, and who have a prescription for statin medication.","nest":"2","pcalc":"g"},"i":{"name":"Patients with unmeasured LDL","description":"Patients without a LDL cholesterol test within the past two years.","nest":"1","pcalc":"a"},"j":{"name":"Patients with hypertension","description":"Patients with a diagnosis for hypertension.","nest":"1","pcalc":"a"},"k":{"name":"Patients with untreated hypertension","description":"Patients with a diagnosis for hypertension without a prescription for hypertension within the prior year.","nest":"2","pcalc":"j"},"l":{"name":"Patients taking aspirin","description":"Patients with a prescription for aspirin.","nest":"1","pcalc":"a"}}'::json rowmeta,
(select '{'||array_to_string(array_agg(row_),',')||'}' from  (select ('"'||
          case when rpt_yr = 'x' then 'YEAR' else rpt_yr end ||
          case when agegroup <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end ||
          case when sex <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end ||
		  case when race_ethnicity <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end ||
          case when risk_percentile <> 'x' 
            then (select codeval from gen_pop_tools.cc_cr_risk_percentile_codevals cv where t0.risk_percentile=cv.risk_percentile)
          else 'x' end || '":{"a":'|| rtrim(to_char(total,'FM99999999999999.9'), '.')
          || ',"b":' || rtrim(to_char(cv_risk_smk,'FM99999999999999.9'), '.')
          || ',"c":' || rtrim(to_char(cv_risk_smk_bn,'FM99999999999999.9'), '.')
          || ',"d":' || rtrim(to_char(cv_risk_a1c,'FM99999999999999.9'), '.')
          || ',"e":' || rtrim(to_char(cv_risk_a1ct,'FM99999999999999.9'), '.')
          || ',"f":' || rtrim(to_char(cv_risk_noa1c,'FM99999999999999.9'), '.')
          || ',"g":' || rtrim(to_char(cv_risk_ldl,'FM99999999999999.9'), '.')
          || ',"h":' || rtrim(to_char(cv_risk_ldl_s,'FM99999999999999.9'), '.')
          || ',"i":' || rtrim(to_char(cv_risk_ldlu,'FM99999999999999.9'), '.')
          || ',"j":' || rtrim(to_char(cv_risk_hyp,'FM99999999999999.9'), '.')
          || ',"k":' || rtrim(to_char(cv_risk_hypu,'FM99999999999999.9'), '.')
          || ',"l":' || rtrim(to_char(cv_risk_sal,'FM99999999999999.9'), '.') 
          || '}') row_
from fake_cv_a t0 ) t00)::json rowdata) t1) to '/tmp/sitea_cr_monthly.json';




















