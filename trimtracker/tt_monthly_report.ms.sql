﻿/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                             TT Monthly Report
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2016 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------*/

IF object_id('gen_pop_tools.tt_out', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_out;
GO
SELECT 
  tt_pat.patient_id as aa
, case
    when tt_pat.gender='M' then 1
	when tt_pat.gender='F' then 2
  end as ab
, case tt_pat.race
    when 'CAUCASIAN' then 1
    when 'ASIAN' then 2
    when 'BLACK' then 3
    when 'HISPANIC' then 4
    when 'OTHER' then 5
    else 6
  end as ac
, case
    when birth_year<1945 then 0
	when birth_year>=1945 and birth_year<= 1965 then 1
	when birth_year>1965 then 2
  end as ad
, case
    when substring(tt_pat.zip,6,1)='-' then substring(tt_pat.zip,1,5)
    else tt_pat.zip
  end as ae
, SUBSTRING(tt_pat_seq_enc.year_month,1,4) as af
, SUBSTRING(tt_pat_seq_enc.year_month,6,2) as ag
,  case 
    when tt_pat_seq_enc.age<=4 then 1
    when tt_pat_seq_enc.age>=5 and tt_pat_seq_enc.age <=9 then 2
    when tt_pat_seq_enc.age>=10 and tt_pat_seq_enc.age <=14 then 3
    when tt_pat_seq_enc.age>=15 and tt_pat_seq_enc.age <=19 then 4
    when tt_pat_seq_enc.age>=20 and tt_pat_seq_enc.age <=24 then 5
    when tt_pat_seq_enc.age>=25 and tt_pat_seq_enc.age <=29 then 6
    when tt_pat_seq_enc.age>=30 and tt_pat_seq_enc.age <=39 then 7
    when tt_pat_seq_enc.age>=40 and tt_pat_seq_enc.age <=49 then 8
    when tt_pat_seq_enc.age>=50 and tt_pat_seq_enc.age <=59 then 9
    when tt_pat_seq_enc.age>=60 and tt_pat_seq_enc.age <=69 then 10
    when tt_pat_seq_enc.age>=70 and tt_pat_seq_enc.age <=79 then 11
    when tt_pat_seq_enc.age>=80 then 12
  end as ah 
, tt_pat_seq_enc.prior1 as ai
, tt_pat_seq_enc.prior2 as aj
, case
    when tt_bmi.bmi>0 then tt_bmi.bmi
    when tt_pat.age>=12 then 0
  end as ak
, case
    when tt_bmi_pct.bmipct<=5 then 1
    when tt_bmi_pct.bmipct>5 and tt_bmi_pct.bmipct<=50 then 2
    when tt_bmi_pct.bmipct>50 and tt_bmi_pct.bmipct<=85 then 3
    when tt_bmi_pct.bmipct>85 and tt_bmi_pct.bmipct<=95 then 4
    when tt_bmi_pct.bmipct>95 then 5
    when tt_pat.age<=20 then 0
  end as al
, case
    when tt_preg1.recent_pregnancy=1 and tt_pat.gender='F' then 1
    when tt_pat.gender='F' then 0
  end as am
, case
    when tt_gdm1.gdm=1 and tt_preg1.recent_pregnancy=1 
	  then 1
	when tt_pat.gender='F' then 0
  end as an
, case
    when tt_bp1.max_bp_systolic<=130 then 1
    when tt_bp1.max_bp_systolic>130 and tt_bp1.max_bp_systolic<=139 then 2
    when tt_bp1.max_bp_systolic>139 then 3
    else 0
  end as ao
, case 
    when tt_bp1.syspct<=5 then 1
    when tt_bp1.syspct>5 and tt_bp1.syspct<=50 then 2
    when tt_bp1.syspct>50 and tt_bp1.syspct<=85 then 3
    when tt_bp1.syspct>85 and tt_bp1.syspct<=95 then 4
    when tt_bp1.syspct>95 then 5
    else 0
  end as ap
, case
    when tt_bp1.max_bp_diastolic<=80 then 1
    when tt_bp1.max_bp_diastolic>80 and tt_bp1.max_bp_diastolic<=89 then 2
    when tt_bp1.max_bp_diastolic>89 then 3
    else 0
  end as aq
, case 
    when tt_bp1.diapct<=5 then 1
    when tt_bp1.diapct>5 and tt_bp1.diapct<=50 then 2
    when tt_bp1.diapct>50 and tt_bp1.diapct<=85 then 3
    when tt_bp1.diapct>85 and tt_bp1.diapct<=95 then 4
    when tt_bp1.diapct>95 then 5
    else 0
  end as ar
, case
    when tt_ldl1.max_ldl1<=100 then 1
    when tt_ldl1.max_ldl1>100 and tt_ldl1.max_ldl1<=129 then 2
    when tt_ldl1.max_ldl1>129 and tt_ldl1.max_ldl1<=159 then 3
    when tt_ldl1.max_ldl1>159 then 4
    else 0
  end as "as"
, case
    when tt_trig1.max_trig1<=199 then 1
    when tt_trig1.max_trig1>199 and tt_trig1.max_trig1<=499 then 2
    when tt_trig1.max_trig1>500 then 3
    else 0
  end as at
, case
    when tt_a1c1.max_a1c1<=5.6 then 1
    when tt_a1c1.max_a1c1>5.6 and tt_a1c1.max_a1c1<=6.4 then 2
    when tt_a1c1.max_a1c1>6.4 and tt_a1c1.max_a1c1<=8.0 then 3
    when tt_a1c1.max_a1c1>8.0 then 4
    else 0
  end as au
, case
    when tt_predm.predm=1 then 1
    else 0
  end as av
, case
    when tt_type1.type_1_diabetes=1 then 1
    else 0
  end as aw
, case
    when tt_type2.type2=1 then 1
    else 0
  end as ax
, case
    when tt_insulin.insulin=1 then 1
    else 0
  end as ay
, case
    when tt_metformin.metformin=1 then 1
    else 0
  end as az
, case
    when tt_flu_cur.influenza_vaccine=1 then 1
    else 0
  end as ba
, case 
    when CAST(tt_smoking.smoking AS integer)>0 then CAST(tt_smoking.smoking AS integer)
    else 0
  end as bb
, case 
    when tt_asthma.asthma = 1 then 1
    else 0
  end as bc
, case 
    when CAST(tt_chlamydia.recent_chlamydia AS integer) >= 1 then CAST(tt_chlamydia.recent_chlamydia AS integer)
    else 0
  end as bd
, case 
    when CAST(tt_depression.depression AS integer)=1 then 1
    else 0
  end as be
, case 
    when CAST(tt_opi0.opioidrx AS integer)=1 then 1
    else 0
  end as bf
, case 
    when CAST(tt_opi1.benzorx AS integer)=1  then 1
    else 0
  end as bg
, case 
    when CAST(tt_opi2.opibenzorx AS integer)=1 then 1
    else 0
  end as bh
, case 
    when CAST(tt_opi3.highopi AS integer)=1 then 1
    else 0
  end as bi
, case 
    when CAST(tt_gonorrhea.recent_gonorrhea AS integer) >= 1 then CAST(tt_gonorrhea.recent_gonorrhea AS integer)
    else 0
  end as bj
, case 
    when CAST(tt_hypertension.hypertension AS integer)=1 then 1
	when CAST(tt_hypertension.hypertension AS integer)=2 then 2
    else 0
  end as bk
, case 
    when CAST(tt_ili_cur.ili AS integer)=1 then 1
    else 0
  end as bl
, case 
    when CAST(tt_hepc.hepc AS integer)=1 then 1
    else 0
  end as bm
, case 
    when CAST(tt_lyme.lyme AS integer)=1 then 1
    else 0
  end as bn
, case 
    when CAST(tt_syph.syph AS integer) >= 1 then CAST(tt_syph.syph AS integer)
    else 0
  end as bo
, case 
    when CAST(tt_tdap.tdap_vaccine AS integer)=1 then 1
    else 0
  end as bp
, case 
    when CAST(tt_tdap.tdap_vaccine AS integer)=1 and tt_preg1.recent_pregnancy=1 and tt_pat.gender='F' then 1
    when tt_pat.gender='F' then 0
  end as bq
--
-- lyme 1 yr lag
--
, case 
    when CAST(tt_lyme_lag.lyme AS integer)=1 then 1
    else 0
  end as br
, tt_pat_seq_enc.allprior as bs
, case 
    when CAST(tt_ili_cur.ili AS integer)=1 then 1
    else 0
  end as bt
--
-- lyme cur yr
--
, case 
    when CAST(tt_lyme_cur.lyme AS integer)=1 then 1
    else 0
  end as bu
--
-- pertussis monthly
--
, case 
    when CAST(tt_pertussis.pertussis AS integer)=1 then 1
    else 0
  end as bv
--
-- pertussis carry forward 1 yr (see below)
--
, case 
    when CAST(tt_pertussis.pertussis AS integer)=1 then 1
    else 0
  end as bw
--
-- CV Risk
--
, tt_cvrisk.cvrisk bx
--diagnosedhypertension
, case 
    when CAST(tt_diaghypert.hypertension AS integer)=1 then 1
	when CAST(tt_diaghypert.hypertension AS integer)=2 then 2
	when CAST(tt_diaghypert.hypertension AS integer)=3 then 3    
    else 0
  end as "by"
--primary_payer
, case
    when tt_primary_payer.primary_payer>=0 then cast(tt_primary_payer.primary_payer as int )
    else 0
  end as bz
--diagnosed diabetes
, case
   when tt_diagdiabetes.diagdiab='0' then 0
      when tt_diagdiabetes.diagdiab='1' then 1
      when tt_diagdiabetes.diagdiab='2' then 2
      when tt_diagdiabetes.diagdiab='3' then 3
      else 4
   end as ca
--census tract
, cast(null as int) cb
--
-- output table
--
into gen_pop_tools.tt_out
--
-- tables built from tt_weekly_tables.pg.sql
--
FROM gen_pop_tools.tt_pat
--
-- tt_pat_seq_enc  
--
JOIN gen_pop_tools.tt_pat_seq_enc on gen_pop_tools.tt_pat_seq_enc.patient_id=gen_pop_tools.tt_pat.patient_id
--
-- most recent BMI Condition
--
LEFT JOIN gen_pop_tools.tt_bmi
	ON gen_pop_tools.tt_bmi.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_bmi.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- most recent BMI PCT
--
LEFT JOIN gen_pop_tools.tt_bmi_pct
	ON gen_pop_tools.tt_bmi_pct.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_bmi_pct.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- pregnancy
--
LEFT JOIN gen_pop_tools.tt_preg1
	ON gen_pop_tools.tt_preg1.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_preg1.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Gestational diabetes
--
LEFT JOIN gen_pop_tools.tt_gdm1
	ON gen_pop_tools.tt_gdm1.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_gdm1.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Blood pressure
--
LEFT JOIN gen_pop_tools.tt_bp1
	ON gen_pop_tools.tt_bp1.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_bp1.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Max ldl lab result last year
--
LEFT JOIN gen_pop_tools.tt_ldl1
	ON gen_pop_tools.tt_ldl1.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_ldl1.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Max trig lab result last year
--
LEFT JOIN gen_pop_tools.tt_trig1
	ON gen_pop_tools.tt_trig1.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_trig1.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Max A1C lab result last year
--
LEFT JOIN gen_pop_tools.tt_a1c1
	ON gen_pop_tools.tt_a1c1.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_a1c1.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Prediabetes
--
LEFT JOIN gen_pop_tools.tt_predm
	ON gen_pop_tools.tt_predm.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_predm.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Type 1 Diabetes
--
LEFT JOIN gen_pop_tools.tt_type1
	ON gen_pop_tools.tt_type1.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_type1.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Type 2 Diabetes
--
LEFT JOIN gen_pop_tools.tt_type2
	ON gen_pop_tools.tt_type2.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_type2.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Insulin
--    Prescription for insulin within the previous year
--
LEFT JOIN gen_pop_tools.tt_insulin
	ON gen_pop_tools.tt_insulin.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_insulin.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Metformin
--     Prescription for metformin within the previous year
--
LEFT JOIN gen_pop_tools.tt_metformin
	ON gen_pop_tools.tt_metformin.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_metformin.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Influenza vaccine
--     Prescription for influenza vaccine current flu season
--
LEFT JOIN gen_pop_tools.tt_flu_cur
	ON gen_pop_tools.tt_flu_cur.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
    and gen_pop_tools.tt_flu_cur.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Smoking
--
LEFT JOIN gen_pop_tools.tt_smoking 
        ON gen_pop_tools.tt_smoking.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_smoking.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- asthma
--
LEFT JOIN gen_pop_tools.tt_asthma
        on gen_pop_tools.tt_asthma.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_asthma.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Most recent chlamydia test
--
LEFT JOIN gen_pop_tools.tt_chlamydia
	ON gen_pop_tools.tt_chlamydia.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
       and gen_pop_tools.tt_chlamydia.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- depression
--
LEFT JOIN gen_pop_tools.tt_depression
        on gen_pop_tools.tt_depression.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_depression.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Opioid -- have to join this 4 times as each of the results are in separate rows.
--
LEFT JOIN gen_pop_tools.tt_opioidrx tt_opi0
        on tt_opi0.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and tt_opi0.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
LEFT JOIN gen_pop_tools.tt_benzorx tt_opi1
        on tt_opi1.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and tt_opi1.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
LEFT JOIN gen_pop_tools.tt_opibenzorx tt_opi2
        on tt_opi2.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and tt_opi2.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
LEFT JOIN gen_pop_tools.tt_highopi tt_opi3
        on tt_opi3.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and tt_opi3.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Gonorrhea
--
LEFT JOIN gen_pop_tools.tt_gonorrhea
        on gen_pop_tools.tt_gonorrhea.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_gonorrhea.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Hypertension
--
LEFT JOIN gen_pop_tools.tt_hypertension
        on gen_pop_tools.tt_hypertension.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_hypertension.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- current ili
--
LEFT JOIN gen_pop_tools.tt_ili_cur
        on gen_pop_tools.tt_ili_cur.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_ili_cur.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- Hep C
--
LEFT JOIN gen_pop_tools.tt_hepc
        on gen_pop_tools.tt_hepc.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_hepc.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- lyme
--
LEFT JOIN gen_pop_tools.tt_lyme
        on gen_pop_tools.tt_lyme.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_lyme.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- lyme 1 yr lag
--
LEFT JOIN 
    (select max(lyme) lyme, patient_id, CAST(SUBSTRING(year_month,1,4) AS varchar(max)) as year
     from gen_pop_tools.tt_lyme 
     group by patient_id, CAST(SUBSTRING(year_month,1,4) AS varchar(max))) tt_lyme_lag
        on tt_lyme_lag.patient_id = tt_pat_seq_enc.patient_id 
          and tt_lyme_lag.year=convert(varchar(4),(convert(int,SUBSTRING(tt_pat_seq_enc.year_month,1,4)) - 1))
--
-- syphilis 
--
LEFT JOIN gen_pop_tools.tt_syph
        on gen_pop_tools.tt_syph.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_syph.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- tdap
--
LEFT JOIN gen_pop_tools.tt_tdap
        on gen_pop_tools.tt_tdap.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_tdap.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- more lyme.  Calendar year
--
LEFT JOIN 
    (select sum(lyme) over (partition by tt_pat_seq_enc.patient_id, 
                       SUBSTRING(tt_pat_seq_enc.year_month,1,4) 
                       order by tt_pat_seq_enc.year_month)  lyme, 
	   tt_pat_seq_enc.patient_id, 
	   tt_pat_seq_enc.year_month
     from gen_pop_tools.tt_pat_seq_enc 
      LEFT JOIN gen_pop_tools.tt_lyme
        on gen_pop_tools.tt_lyme.patient_id =gen_pop_tools. tt_pat_seq_enc.patient_id 
          and gen_pop_tools.tt_lyme.year_month=gen_pop_tools.tt_pat_seq_enc.year_month) tt_lyme_cur
on tt_lyme_cur.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
          and tt_lyme_cur.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- pertussis
--
LEFT JOIN gen_pop_tools.tt_pertussis
        on gen_pop_tools.tt_pertussis.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_pertussis.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- CV Risk
--
LEFT JOIN gen_pop_tools.tt_cvrisk
        on gen_pop_tools.tt_cvrisk.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_cvrisk.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- diagnosedhypertension
--
LEFT JOIN gen_pop_tools.tt_diaghypert
        on gen_pop_tools.tt_diaghypert.patient_id = gen_pop_tools.tt_pat_seq_enc.patient_id 
           and gen_pop_tools.tt_diaghypert.year_month=gen_pop_tools.tt_pat_seq_enc.year_month
--
-- diagnoseddiabetes
--
LEFT JOIN gen_pop_tools.tt_diagdiabetes
        on tt_diagdiabetes.patient_id = tt_pat_seq_enc.patient_id and tt_diagdiabetes.year_month=tt_pat_seq_enc.year_month
--
-- primary payer
--
LEFT JOIN gen_pop_tools.tt_primary_payer
        on tt_primary_payer.patient_id=tt_pat_seq_enc.patient_id and tt_primary_payer.year_month=tt_pat_seq_enc.year_month 
--
-- census tract
--
LEFT JOIN gen_pop_tools.patient_census_tract tt_census
       on tt_census.patient_id=tt_pat_seq_enc.patient_id
--
-- Ordering
--
ORDER BY gen_pop_tools.tt_pat_seq_enc.patient_id, gen_pop_tools.tt_pat_seq_enc.year_month
;
GO
alter table gen_pop_tools.tt_out alter column aa int NOT NULL;
alter table gen_pop_tools.tt_out alter column af varchar(4) NOT NULL;
alter table gen_pop_tools.tt_out alter column ag varchar(2) NOT NULL;
GO
alter table gen_pop_tools.tt_out add primary key (aa, af, ag);
GO
UPDATE STATISTICS gen_pop_tools.tt_out;
GO
--
-- downfill2
--
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='ak';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='al';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='ao';							  
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='ap';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='aq';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='ar';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='as';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='at';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='au';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='av', @extlim='500'; --predm, link to dm type2 later
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='aw', @extlim='500'; --dm type1
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='ax', @extlim='500'; --dm type2
exec gen_pop_tools.fluseason2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='ba';
exec gen_pop_tools.fluseason2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='bt';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='bb', @extlim='500';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='bd', @extlim='11';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='be';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='bj', @extlim='11';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='bm', @extlim='500'; -- hep c
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='bo', @extlim='11';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='bq', @extlim='11';
exec gen_pop_tools.downfill2 @intable='gen_pop_tools.tt_out', @partby='aa', @orderby='af+ag', @col='bw', @extlim='11';
delete from gen_pop_tools.tt_out
where af+ag=gen_pop_tools.dateToYyyyMm(getdate()); --this of course breaks any partial month results for current month.						  
update gen_pop_tools.tt_out
 set av=0 where ax=1 and av is not null;
update gen_pop_tools.tt_out
 set bo=0 where am = 0 and bo=1;
update gen_pop_tools.tt_out
 set bq=0 where am = 0 and bq=1;
GO
