﻿/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                             TT Weekly Report
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2016 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------*/
set search_path to gen_pop_tools, public;

drop table if exists tt_out;
create table tt_out as
SELECT 
  tt_pat.patient_id as AA
, case
    when tt_pat.gender='M' then 1
	when tt_pat.gender='F' then 2
  end as AB
, case tt_pat.race
    when 'CAUCASIAN' then 1
    when 'ASIAN' then 2
    when 'BLACK' then 3
    when 'HISPANIC' then 4
    when 'OTHER' then 5
    else 6
  end as AC
, case
    when birth_year<1945 then 0
	when birth_year>=1945 and birth_year<= 1965 then 1
	when birth_year>1965 then 2
  end as AD
, case
    when substring(tt_pat.zip,6,1)='-' then substring(tt_pat.zip,1,5)
    else tt_pat.zip
  end as AE
, substr(tt_pat_seq_enc.year_month,1,4) as AF
, substr(tt_pat_seq_enc.year_month,6,2) as AG
,  case 
    when tt_pat.age<=4 then 1
    when tt_pat.age>=5 and tt_pat.age <=9 then 2
    when tt_pat.age>=10 and tt_pat.age <=14 then 3
    when tt_pat.age>=15 and tt_pat.age <=19 then 4
    when tt_pat.age>=20 and tt_pat.age <=24 then 5
    when tt_pat.age>=25 and tt_pat.age <=29 then 6
    when tt_pat.age>=30 and tt_pat.age <=40 then 7
    when tt_pat.age>=40 and tt_pat.age <=49 then 8
    when tt_pat.age>=50 and tt_pat.age <=59 then 9
    when tt_pat.age>=60 and tt_pat.age <=69 then 10
    when tt_pat.age>=70 and tt_pat.age <=79 then 11
    when tt_pat.age>=80 then 12
  end as AH 
, tt_pat_seq_enc.prior1 as AI
, tt_pat_seq_enc.prior2 as AJ
, case
    when tt_bmi.bmi>0 then tt_bmi.bmi
    when tt_pat.age>=12 then 0
  end as AK
, case
    when tt_bmi_pct.bmipct<=5 then 1
    when tt_bmi_pct.bmipct>5 and tt_bmi_pct.bmipct<=50 then 2
    when tt_bmi_pct.bmipct>50 and tt_bmi_pct.bmipct<=85 then 3
    when tt_bmi_pct.bmipct>85 and tt_bmi_pct.bmipct<=95 then 4
    when tt_bmi_pct.bmipct>95 then 5
    when tt_pat.age<=20 then 0
  end as AL
, case
    when tt_preg1.recent_pregnancy=1 and tt_pat.gender='F' then 1
    when tt_pat.gender='F' then 0
  end as AM
, case
    when tt_gdm1.gdm=1 and tt_preg1.recent_pregnancy=1 
	  then 1
	when tt_pat.gender='F' then 0
  end as AN
, case
    when tt_bp1.max_bp_systolic<=130 then 1
    when tt_bp1.max_bp_systolic>130 and tt_bp1.max_bp_systolic<=139 then 2
    when tt_bp1.max_bp_systolic>139 then 3
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AO
, case 
    when tt_bp1.syspct<=5 then 1
    when tt_bp1.syspct>5 and tt_bp1.syspct<=50 then 2
    when tt_bp1.syspct>50 and tt_bp1.syspct<=85 then 3
    when tt_bp1.syspct>85 and tt_bp1.syspct<=95 then 4
    when tt_bp1.syspct>95 then 5
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AP
, case
    when tt_bp1.max_bp_diastolic<=80 then 1
    when tt_bp1.max_bp_diastolic>80 and tt_bp1.max_bp_diastolic<=89 then 2
    when tt_bp1.max_bp_diastolic>89 then 3
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AQ
, case 
    when tt_bp1.diapct<=5 then 1
    when tt_bp1.diapct>5 and tt_bp1.diapct<=50 then 2
    when tt_bp1.diapct>50 and tt_bp1.diapct<=85 then 3
    when tt_bp1.diapct>85 and tt_bp1.diapct<=95 then 4
    when tt_bp1.diapct>95 then 5
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AR
, case
    when tt_ldl1.max_ldl1<=100 then 1
    when tt_ldl1.max_ldl1>100 and tt_ldl1.max_ldl1<=129 then 2
    when tt_ldl1.max_ldl1>129 and tt_ldl1.max_ldl1<=159 then 3
    when tt_ldl1.max_ldl1>159 then 4
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AS
, case
    when tt_trig1.max_trig1<=199 then 1
    when tt_trig1.max_trig1>199 and tt_trig1.max_trig1<=499 then 2
    when tt_trig1.max_trig1>500 then 3
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AT
, case
    when tt_a1c1.max_a1c1<=5.6 then 1
    when tt_a1c1.max_a1c1>5.6 and tt_a1c1.max_a1c1<=6.4 then 2
    when tt_a1c1.max_a1c1>6.4 and tt_a1c1.max_a1c1<=8.0 then 3
    when tt_a1c1.max_a1c1>8.0 then 4
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AU
, case
    when tt_predm.predm=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AV
, case
    when tt_type1.type_1_diabetes=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AW
, case
    when tt_type2.type2=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AX
, case
    when tt_insulin.insulin=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AY
, case
    when tt_metformin.metformin=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as AZ
, case
    when tt_flu_cur.influenza_vaccine=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BA
, case 
    when tt_smoking.smoking::integer>0 then tt_smoking.smoking::integer
    else 0
  end as BB
, case 
    when tt_asthma.asthma = 1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BC
, case 
    when tt_chlamydia.recent_chlamydia::integer = 1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BD
, case 
    when tt_depression.depression::integer=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BE
, case 
    when tt_opi0.opioidrx::integer=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BF
, case 
    when tt_opi1.benzorx::integer=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BG
, case 
    when tt_opi2.opibenzorx::integer=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BH
, case 
    when tt_opi3.highopi::integer=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BI
, case 
    when tt_gonorrhea.recent_gonorrhea::integer=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BJ
, case 
    when tt_hypertension.hypertension::integer=1 then 1
	when tt_hypertension.hypertension::integer=2 then 2
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BK
, case 
    when tt_ili_cur.ili::integer=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BL
, case 
    when tt_hepc.hepc::integer=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BM
, case 
    when tt_lyme.lyme::integer=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BN
, case 
    when tt_syph.syph::integer=1 and tt_preg1.recent_pregnancy=1 and tt_pat.gender='F' then 1
    when tt_pat.gender='F' then 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BO
, case 
    when tt_tdap.tdap_vaccine::integer=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BP
, case 
    when tt_tdap.tdap_vaccine::integer=1 and tt_preg1.recent_pregnancy=1 and tt_pat.gender='F' then 1
    when tt_pat.gender='F' then 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BQ
--
-- lyme 1 yr lag
--
, case 
    when tt_lyme_lag.lyme::integer=1 then 1
    else 0
--  when tt_pat_seq_enc.prior2>0 then 0
  end as BR
, tt_pat_seq_enc.allprior as BS
--
-- tables built from tt_weekly_tables.pg.sql
--
FROM gen_pop_tools.tt_pat
--
-- tt_pat_seq_enc  
--
JOIN tt_pat_seq_enc on tt_pat_seq_enc.patient_id=tt_pat.patient_id
--
-- most recent BMI Condition
--
LEFT JOIN (select * from gen_pop_tools.tt_bmi where year_month=max_ym) tt_bmi
	ON tt_bmi.patient_id = tt_pat_seq_enc.patient_id and tt_bmi.year_month=tt_pat_seq_enc.year_month
--
-- most recent BMI PCT
--
LEFT JOIN (select * from gen_pop_tools.tt_bmi_pct where year_month=max_ym) tt_bmi_pct
	ON tt_bmi_pct.patient_id = tt_pat_seq_enc.patient_id and tt_bmi_pct.year_month=tt_pat_seq_enc.year_month
--
-- pregnancy
--
LEFT JOIN (select * from gen_pop_tools.tt_preg1 where year_month=max_ym) tt_preg1
	ON tt_preg1.patient_id = tt_pat_seq_enc.patient_id and tt_preg1.year_month=tt_pat_seq_enc.year_month
--
-- Gestational diabetes
--
LEFT JOIN (select * from gen_pop_tools.tt_gdm1 where year_month=max_ym) tt_gdm1
	ON tt_gdm1.patient_id = tt_pat_seq_enc.patient_id and tt_gdm1.year_month=tt_pat_seq_enc.year_month
--
-- Blood pressure
--
LEFT JOIN (select * from gen_pop_tools.tt_bp1 where year_month=max_ym) tt_bp1
	ON tt_bp1.patient_id = tt_pat_seq_enc.patient_id and tt_bp1.year_month=tt_pat_seq_enc.year_month
--
-- Max ldl lab result last year
--
LEFT JOIN (select * from gen_pop_tools.tt_ldl1 where year_month=max_ym) tt_ldl1
	ON tt_ldl1.patient_id = tt_pat_seq_enc.patient_id and tt_ldl1.year_month=tt_pat_seq_enc.year_month
--
-- Max trig lab result last year
--
LEFT JOIN (select * from gen_pop_tools.tt_trig1 where year_month=max_ym) tt_trig1
	ON tt_trig1.patient_id = tt_pat_seq_enc.patient_id and tt_trig1.year_month=tt_pat_seq_enc.year_month
--
-- Max A1C lab result last year
--
LEFT JOIN (select * from gen_pop_tools.tt_a1c1 where year_month=max_ym) tt_a1c1
	ON tt_a1c1.patient_id = tt_pat_seq_enc.patient_id and tt_a1c1.year_month=tt_pat_seq_enc.year_month
--
-- Prediabetes
--
LEFT JOIN (select * from gen_pop_tools.tt_predm where year_month=max_ym) tt_predm
	ON tt_predm.patient_id = tt_pat_seq_enc.patient_id and tt_predm.year_month=tt_pat_seq_enc.year_month
--
-- Type 1 Diabetes
--
LEFT JOIN (select * from gen_pop_tools.tt_type1 where year_month=max_ym) tt_type1
	ON tt_type1.patient_id = tt_pat_seq_enc.patient_id and tt_type1.year_month=tt_pat_seq_enc.year_month
--
-- Type 2 Diabetes
--
LEFT JOIN (select * from gen_pop_tools.tt_type2 where year_month=max_ym) tt_type2
	ON tt_type2.patient_id = tt_pat_seq_enc.patient_id and tt_type2.year_month=tt_pat_seq_enc.year_month
--
-- Insulin
--    Prescription for insulin within the previous year
--
LEFT JOIN (select * from gen_pop_tools.tt_insulin where year_month=max_ym) tt_insulin
	ON tt_insulin.patient_id = tt_pat_seq_enc.patient_id and tt_insulin.year_month=tt_pat_seq_enc.year_month
--
-- Metformin
--     Prescription for metformin within the previous year
--
LEFT JOIN (select * from gen_pop_tools.tt_metformin where year_month=max_ym) tt_metformin
	ON tt_metformin.patient_id = tt_pat_seq_enc.patient_id and tt_metformin.year_month=tt_pat_seq_enc.year_month
--
-- Influenza vaccine
--     Prescription for influenza vaccine current flu season
--
LEFT JOIN (select * from gen_pop_tools.tt_flu_cur where year_month=max_ym) tt_flu_cur
	ON tt_flu_cur.patient_id = tt_pat_seq_enc.patient_id and tt_flu_cur.year_month=tt_pat_seq_enc.year_month
--
-- Smoking
--
LEFT JOIN (select * from gen_pop_tools.tt_smoking where year_month=max_ym) tt_smoking
        ON tt_smoking.patient_id = tt_pat_seq_enc.patient_id and tt_smoking.year_month=tt_pat_seq_enc.year_month
--
-- asthma
--
LEFT JOIN (select * from gen_pop_tools.tt_asthma where year_month=max_ym) tt_asthma
        on tt_asthma.patient_id = tt_pat_seq_enc.patient_id and tt_asthma.year_month=tt_pat_seq_enc.year_month
--
-- Most recent chlamydia test
--
LEFT JOIN (select * from gen_pop_tools.tt_chlamydia where year_month=max_ym) tt_chlamydia
	ON tt_chlamydia.patient_id = tt_pat_seq_enc.patient_id and tt_chlamydia.year_month=tt_pat_seq_enc.year_month
--
-- depression
--
LEFT JOIN (select * from gen_pop_tools.tt_depression where year_month=max_ym) tt_depression
        on tt_depression.patient_id = tt_pat_seq_enc.patient_id and tt_depression.year_month=tt_pat_seq_enc.year_month
--
-- Opioid -- have to join this 4 times as each of the results are in separate rows.
--
LEFT JOIN (select * from gen_pop_tools.tt_opioidrx where year_month=max_ym) tt_opi0
        on tt_opi0.patient_id = tt_pat_seq_enc.patient_id and tt_opi0.year_month=tt_pat_seq_enc.year_month
LEFT JOIN (select * from gen_pop_tools.tt_benzorx where year_month=max_ym) tt_opi1
        on tt_opi1.patient_id = tt_pat_seq_enc.patient_id and tt_opi1.year_month=tt_pat_seq_enc.year_month
LEFT JOIN (select * from gen_pop_tools.tt_opibenzorx where year_month=max_ym) tt_opi2
        on tt_opi2.patient_id = tt_pat_seq_enc.patient_id and tt_opi2.year_month=tt_pat_seq_enc.year_month
LEFT JOIN (select * from gen_pop_tools.tt_highopi where year_month=max_ym) tt_opi3
        on tt_opi3.patient_id = tt_pat_seq_enc.patient_id and tt_opi3.year_month=tt_pat_seq_enc.year_month
--
-- Gonorrhea
--
LEFT JOIN (select * from gen_pop_tools.tt_gonorrhea where year_month=max_ym) tt_gonorrhea
        on tt_gonorrhea.patient_id = tt_pat_seq_enc.patient_id and tt_gonorrhea.year_month=tt_pat_seq_enc.year_month
--
-- Hypertension
--
LEFT JOIN (select * from gen_pop_tools.tt_hypertension where year_month=max_ym) tt_hypertension
        on tt_hypertension.patient_id = tt_pat_seq_enc.patient_id and tt_hypertension.year_month=tt_pat_seq_enc.year_month
--
-- current ili
--
LEFT JOIN (select * from gen_pop_tools.tt_ili_cur where year_month=max_ym) tt_ili_cur
        on tt_ili_cur.patient_id = tt_pat_seq_enc.patient_id and tt_ili_cur.year_month=tt_pat_seq_enc.year_month
--
-- Hep C
--
LEFT JOIN (select * from gen_pop_tools.tt_hepc where year_month=max_ym) tt_hepc
        on tt_hepc.patient_id = tt_pat_seq_enc.patient_id and tt_hepc.year_month=tt_pat_seq_enc.year_month
--
-- lyme
--
LEFT JOIN (select * from gen_pop_tools.tt_lyme where year_month=max_ym) tt_lyme
        on tt_lyme.patient_id = tt_pat_seq_enc.patient_id and tt_lyme.year_month=tt_pat_seq_enc.year_month
--
-- lyme 1 yr lag
--
LEFT JOIN (select * from gen_pop_tools.tt_lyme where year_month=max_ym) tt_lyme_lag
        on tt_lyme.patient_id = tt_pat_seq_enc.patient_id and tt_lyme.year_month=to_char(to_date(tt_pat_seq_enc.year_month,'yyyy_mm') + interval  '1 year', 'yyyy_mm')
--
-- syphilis 
--
LEFT JOIN (select * from gen_pop_tools.tt_syph where year_month=max_ym) tt_syph
        on tt_syph.patient_id = tt_pat_seq_enc.patient_id and tt_syph.year_month=tt_pat_seq_enc.year_month
--
-- tdap
--
LEFT JOIN (select * from gen_pop_tools.tt_tdap where year_month=max_ym) tt_tdap
        on tt_tdap.patient_id = tt_pat_seq_enc.patient_id and tt_tdap.year_month=tt_pat_seq_enc.year_month
--
-- where filter for last full month (no longer needed because of runDOM filter for tt_month_series)
--
--where tt_pat_seq_enc.year_month<=to_char(date_trunc('month',current_date) - interval '1 month','yyyy_mm')
--
-- Ordering
--
ORDER BY tt_pat_seq_enc.patient_id, tt_pat_seq_enc.year_month
;
alter table gen_pop_tools.tt_out add primary key (AA, AF, AG);
analyze gen_pop_tools.tt_out;
--
-- downfill
--
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'ak'::varchar,
                              'AF||AG'::varchar);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'al'::varchar,
                              'AF||AG'::varchar);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'ao'::varchar,
                              'AF||AG'::varchar);							  
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'ap'::varchar,
                              'AF||AG'::varchar);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'aq'::varchar,
                              'AF||AG'::varchar);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'ar'::varchar,
                              'AF||AG'::varchar);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'as'::varchar,
                              'AF||AG'::varchar);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'at'::varchar,
                              'AF||AG'::varchar);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'au'::varchar,
                              'AF||AG'::varchar);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'av'::varchar,
                              'AF||AG'::varchar,
                               500::integer); --predm, link to dm type2 later
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'aw'::varchar,
                              'AF||AG'::varchar,
                               500::integer); --dm type1
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'ax'::varchar,
                              'AF||AG'::varchar,
							   500::integer); --dm type2
select gen_pop_tools.fluseason2('gen_pop_tools.tt_out'::varchar, 
                              'AA'::varchar, 
                              'AF||AG'::varchar, 
                              'ba'::varchar);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar, 
                              'AA'::varchar, 
                              'AF||AG'::varchar, 
                              'bb'::varchar,
                              'AF||AG'::varchar,
                               500::integer);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'bd'::varchar,
                              'AF||AG'::varchar,
                               11::integer);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'be'::varchar,
                              'AF||AG'::varchar);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar,
                              'AA'::varchar,
                              'AF||AG'::varchar,
                              'bj'::varchar,
                              'AF||AG'::varchar,
                               11::integer);
select gen_pop_tools.fluseason2('gen_pop_tools.tt_out'::varchar, 
                              'AA'::varchar, 
                              'AF||AG'::varchar, 
                              'bl'::varchar);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar, 
                              'AA'::varchar, 
                              'AF||AG'::varchar, 
                              'bm'::varchar,
                              'AF||AG'::varchar,
                               500::integer); -- hep c
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar, 
                              'AA'::varchar, 
                              'AF||AG'::varchar, 
                              'bo'::varchar,
                              'AF||AG'::varchar,
                              11::integer);
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar, 
                              'AA'::varchar, 
                              'AF||AG'::varchar, 
                              'bp'::varchar,
                              'AF||AG'::varchar); -- this is not correct
							  -- need a new downfill for TDAP regular
select gen_pop_tools.downfill2('gen_pop_tools.tt_out'::varchar, 
                              'AA'::varchar, 
                              'AF||AG'::varchar, 
                              'bq'::varchar,
                              'AF||AG'::varchar,
                              11::integer);

delete from gen_pop_tools.tt_out where AF||AG<(select max(AF||AG) from gen_pop_tools.tt_out);

update gen_pop_tools.tt_out
set AV=0 where AX=1 and AV is not null;
update gen_pop_tools.tt_out
set BO=0 where AM = 0 and BO=1;
update gen_pop_tools.tt_out
set BQ=0 where AM = 0 and BQ=1;

copy gen_pop_tools.tt_out to :'pathToFile' with CSV delimiter ',' header;
