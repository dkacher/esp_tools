﻿/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                             TT Monthly Tables
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2016 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some PostgreSQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/

--
-- build patient table
--

select 'Starting to build gen_pop_tools.tt_pat at: ',getdate();
IF object_id('gen_pop_tools.tt_pat', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_pat;
GO
SELECT 
  pat.id AS patient_id, gen_pop_tools.age(CAST(pat.date_of_birth AS DATE)) as age, upper(SUBSTRING(pat.gender,1,1)) gender, 
  datepart(year,CAST(date_of_birth AS DATE)) birth_year,
  case 
     when (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='ethnicity' and t00.src_value=pat.ethnicity
                  and t00.src_table='emr_patient' and t00.mapped_value='HISPANIC') is not null
	   then (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='ethnicity' and t00.src_value=pat.ethnicity
                  and t00.src_table='emr_patient' and t00.mapped_value='HISPANIC')
	 else (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='race' and t00.src_value=pat.race
                  and t00.src_table='emr_patient')
  end as race, -- pat.race as old_race, pat.ethnicity, --uncomment this for QA
  pat.date_of_death, pat.date_of_birth,
  case
    when SUBSTRING(pat.zip,6,1)='-' then SUBSTRING(pat.zip,1,5)
    else pat.zip
  end as zip
INTO gen_pop_tools.tt_pat
FROM dbo.emr_patient pat;
GO
alter table gen_pop_tools.tt_pat add primary key (patient_id);
UPDATE STATISTICS gen_pop_tools.tt_pat;
GO
--
-- Build the patient_encounter list
---
select 'Starting to build gen_pop_tools.tt_enc_pat at: ',getdate();
IF object_id('gen_pop_tools.tt_enc_pat', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_enc_pat;
GO
CREATE TABLE gen_pop_tools.tt_enc_pat(
    patient_id int NOT NULL,
    year_month varchar(10) NOT NULL,
    counts int
);
GO
INSERT INTO gen_pop_tools.tt_enc_pat
select patient_id,
       gen_pop_tools.dateToYyyyMm(date) year_month, 
       count(*) as counts
from gen_pop_tools.clin_enc
group by patient_id, gen_pop_tools.dateToYyyyMm(date);
alter table gen_pop_tools.tt_enc_pat add primary key (patient_id, year_month);
UPDATE STATISTICS gen_pop_tools.tt_enc_pat;
GO

IF object_id('gen_pop_tools.tt_1stenc_pat', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_1stenc_pat;
GO
select patient_id, min(year_month) as year_month
INTO gen_pop_tools.tt_1stenc_pat 
from gen_pop_tools.tt_enc_pat
group by patient_id;
alter table gen_pop_tools.tt_1stenc_pat add primary key (patient_id);
UPDATE STATISTICS gen_pop_tools.tt_1stenc_pat;
--
--utilty table with all months in seqence to current date;
--
GO

IF object_id('gen_pop_tools.tt_month_series', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_month_series;
GO
CREATE TABLE gen_pop_tools.tt_month_series(
    year_month varchar(10) NOT NULL
);
GO
INSERT INTO gen_pop_tools.tt_month_series
select year_month from
(select gen_pop_tools.dateToYyyyMm(dates) as year_month FROM gen_pop_tools.generate_month_series('2010-01-01', getdate())) t0
where year_month <= gen_pop_tools.dateToYyyyMm(DATEADD(month, -1, getdate()));
GO
--Since we will be running this on at least a lag of 2 days, and we only want full months of data, the current date will never represent a full month.
alter table gen_pop_tools.tt_month_series add primary key (year_month); 
UPDATE STATISTICS gen_pop_tools.tt_month_series;
GO
--
-- pat_seq table with age at each year_month
--

IF object_id('gen_pop_tools.tt_pat_seq', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_pat_seq;
GO
select ser.year_month, p1st.patient_id
INTO gen_pop_tools.tt_pat_seq
from gen_pop_tools.tt_month_series ser
join gen_pop_tools.tt_1stenc_pat p1st on p1st.year_month<=ser.year_month;
GO
alter table gen_pop_tools.tt_pat_seq add primary key (patient_id, year_month);
--now pat_seq_enc, which adds count of encounters and age at each month
GO

IF object_id('gen_pop_tools.tt_pat_seq_enc', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_pat_seq_enc;
GO
select pat.patient_id, seq.year_month, 
       gen_pop_tools.age_2(CONVERT(DATE, REPLACE(seq.year_month, '_', '')+'01',112),CAST(pat.date_of_birth AS DATE)) as age,
       sum(eseq.counts) over (partition by pat.patient_id order by seq.year_month range between unbounded preceding and current row) as allprior,
	   sum(eseq.counts) over (partition by pat.patient_id order by seq.year_month rows between 23 preceding and current row) as prior2,
	   sum(eseq.counts) over (partition by pat.patient_id order by seq.year_month rows between 11 preceding and current row) as prior1
INTO gen_pop_tools.tt_pat_seq_enc
from gen_pop_tools.tt_pat pat
join gen_pop_tools.tt_pat_seq seq on pat.patient_id=seq.patient_id
left join gen_pop_tools.tt_enc_pat eseq on seq.patient_id=eseq.patient_id and seq.year_month=eseq.year_month;
GO
--that was a left join of the encounters, so if there are no prior1 or prior2 counts, you get null values.
alter table gen_pop_tools.tt_pat_seq_enc alter column patient_id int NOT NULL;
alter table gen_pop_tools.tt_pat_seq_enc alter column year_month varchar(10) NOT NULL;
alter table gen_pop_tools.tt_pat_seq_enc add primary key (patient_id, year_month);
GO
UPDATE STATISTICS gen_pop_tools.tt_pat_seq_enc;
GO
--
-- BMI - by month for patients age >=12 on detection date 
--
SELECT 'Starting to build gen_pop_tools.tt_bmi at: ',getdate();
IF object_id('gen_pop_tools.tt_bmi', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_bmi;
GO
SELECT pat.id patient_id
   , yyyy_mm year_month, 
  max(case when condition = 'BMI <25' then 1
       WHEN condition = 'BMI >=25 and <30' THEN 2
       WHEN condition = 'BMI >= 30' THEN 3
       WHEN condition = 'No Measured BMI' THEN 0
  END) bmi
  , max(yyyy_mm) over (partition by pat.id) max_ym
INTO gen_pop_tools.tt_bmi
FROM (SELECT *, gen_pop_tools.dateToYyyyMm(DATEADD(day, date, CAST('1960-01-01' AS DATE))) as yyyy_mm FROM dbo.esp_condition) cond
JOIN dbo.emr_patient pat ON (cond.patid = pat.natural_key)
WHERE lower(condition) like '%bmi%' AND age_at_detect_year >= 12
group by pat.id, cond.yyyy_mm;  
GO
--
-- BMI PCT 
--
select 'Starting to build gen_pop_tools.tt_bmi_pct at: ',getdate();
IF object_id('gen_pop_tools.tt_bmi_pct', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_bmi_pct;
GO
select enc.patient_id
    , max(gen_pop_tools.CDC_BMI(DATEDIFF(month, CAST(pat.date_of_birth AS DATE), enc.date),
            (case when upper(SUBSTRING(pat.gender,1,1))='M' then '1' when upper(SUBSTRING(pat.gender,1,1))='F' then '2' else null end),
            null,
            null,
            enc.bmi,
            'BMIPCT')) as bmipct
    , gen_pop_tools.dateToYyyyMm(enc.date) as year_month
    , max(gen_pop_tools.dateToYyyyMm(enc.date)) over (partition by enc.patient_id) max_ym
INTO gen_pop_tools.tt_bmi_pct
from emr_encounter enc
JOIN dbo.emr_patient pat on pat.id=enc.patient_id
--where enc.raw_encounter_type <> 'HISTORY'
where (enc.weight>0 or enc.height>0
                       or enc.bp_systolic>0 or enc.bp_diastolic>0
                       or enc.temperature>0 or enc.pregnant=1 or enc.edd is not null)
        or exists (select null from emr_encounter_dx_codes dx
                  where dx.encounter_id=enc.id and dx.dx_code_id<>'icd9:799.9')
GROUP BY enc.patient_id, gen_pop_tools.dateToYyyyMm(enc.date);
GO
--
-- pregnancy by month
--
select 'Starting to build gen_pop_tools.tt_preg1 at: ',getdate();
IF object_id('gen_pop_tools.tt_preg1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_preg1;
GO
SELECT seq.patient_id
	, seq.year_month
	, max(1) recent_pregnancy
    , max(seq.year_month) over (partition by seq.patient_id) max_ym
INTO gen_pop_tools.tt_preg1
FROM hef_timespan span
JOIN gen_pop_tools.tt_pat_seq_enc seq on seq.patient_id=span.patient_id 
     and DATEADD(month, 1, CONVERT(DATE, REPLACE(seq.year_month, '_', '')+'01',112)) > span.start_date
	 and DATEADD(month, 1, CONVERT(DATE, REPLACE(seq.year_month, '_', '')+'01',112)) < case 
	                             when span.end_date is not null then span.end_date
				     when DATEADD(month, 10, span.start_date) > getdate() then getdate()
				   end
GROUP BY seq.patient_id, seq.year_month;
GO
--
-- Recent Gestational diabetes 1 year
--
select 'Starting to build gen_pop_tools.tt_gdm1 at: ',getdate();
IF object_id('gen_pop_tools.tt_gdm1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_gdm1;
GO
SELECT seq.patient_id
    , seq.year_month       
	, max(1) AS gdm
    , max(seq.year_month) over (partition by seq.patient_id) max_ym
INTO gen_pop_tools.tt_gdm1
FROM dbo.nodis_case c
JOIN nodis_case_timespans nct ON nct.case_id = c.id
JOIN hef_timespan ts ON nct.timespan_id = ts.id
JOIN gen_pop_tools.tt_pat_seq_enc seq on seq.patient_id=ts.patient_id 
     and DATEADD(month, 1, CONVERT(DATE, REPLACE(seq.year_month, '_', '')+'01',112)) > ts.start_date
	 and DATEADD(month, 1, CONVERT(DATE, REPLACE(seq.year_month, '_', '')+'01',112)) < case 
	                            when ts.end_date is not null then ts.end_date
	                            when DATEADD(month, 10, ts.start_date) > getdate() then getdate()
				  end
WHERE c.condition = 'diabetes:gestational' 
group by seq.patient_id, seq.year_month;
GO
--
-- Max blood pressure 
--
select 'Starting to build gen_pop_tools.tt_enc_bp at: ',getdate();
IF object_id('gen_pop_tools.tt_enc_bp', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_enc_bp;
GO
select patient_id, date, bp_systolic, bp_diastolic, (2*bp_diastolic + bp_systolic)/3 as mean_arterial, height
INTO gen_pop_tools.tt_enc_bp
from emr_encounter e
where bp_systolic is not null and bp_diastolic is not null ;
GO
create index tt_enc_bp_idx
on gen_pop_tools.tt_enc_bp (patient_id, date);
UPDATE STATISTICS gen_pop_tools.tt_enc_bp;
GO
--
-- Max blood pressure using max mean aterial pressure for the period
--
select 'Starting to build gen_pop_tools.tt_bp1 at: ',getdate();
IF object_id('gen_pop_tools.tt_bp1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_bp1;
GO
select * 
INTO gen_pop_tools.tt_bp1
from (
	SELECT 
	  t0.patient_id
	, t0.bp_systolic AS max_bp_systolic
	, t0.bp_diastolic AS max_bp_diastolic
    , t1.max_mean_arterial as map
    , gen_pop_tools.NHBP(gen_pop_tools.age_2(t0.date, CAST(pat.date_of_birth AS DATE)), 
             upper(SUBSTRING(pat.gender,1,1)), 
             gen_pop_tools.CDC_HGT(DATEDIFF(month, CAST(pat.date_of_birth AS DATE), t0.date), 
             (case when upper(SUBSTRING(pat.gender,1,1))='M' then '1' when upper(SUBSTRING(pat.gender,1,1))='F' then '2' else null end), 
             t0.height, 
             'HTZ'), 
             t0.bp_systolic, 
             'SYS', 
             'BPPCT') as syspct
    , gen_pop_tools.NHBP(gen_pop_tools.age_2(t0.date, CAST(pat.date_of_birth AS DATE)), 
             upper(SUBSTRING(pat.gender,1,1)), 
             gen_pop_tools.CDC_HGT(DATEDIFF(month, CAST(pat.date_of_birth AS DATE), t0.date), 
             (case when upper(SUBSTRING(pat.gender,1,1))='M' then '1' when upper(SUBSTRING(pat.gender,1,1))='F' then '2' else null end), 
             t0.height, 
             'HTZ'), 
             t0.bp_diastolic, 
             'DIA', 
             'BPPCT') as diapct
    , row_number() over (partition by t0.patient_id order by t0.date desc) as rownum
    , t1.year_month
    , max(t1.year_month) over (partition by t1.patient_id) max_ym
	FROM gen_pop_tools.tt_enc_bp t0,
         (select seq.patient_id, seq.year_month, max(encbp.mean_arterial) as max_mean_arterial  
              from gen_pop_tools.tt_enc_bp encbp 
			  join gen_pop_tools.tt_pat_seq_enc seq on seq.patient_id=encbp.patient_id and gen_pop_tools.dateToYyyyMm(encbp.date)=seq.year_month
              group by seq.patient_id, seq.year_month) as t1,
          dbo.emr_patient as pat
	WHERE gen_pop_tools.dateToYyyyMm(t0.date)=t1.year_month 
	and t0.mean_arterial = t1.max_mean_arterial and t0.patient_id=t1.patient_id
        and t0.patient_id=pat.id) as t
        where rownum=1;
GO
--
-- lab subset
--
select 'Starting to build gen_pop_tools.tt_sublab at: ',getdate();
IF object_id('gen_pop_tools.tt_sublab', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_sublab;
GO
select patient_id, date, result_float, t1.test_name
INTO gen_pop_tools.tt_sublab
from emr_labresult t0
  inner join conf_labtestmap t1
    on t1.native_code=t0.native_code 
where t1.test_name in ('a1c','cholesterol-ldl','triglycerides','hepatitis_c_rna','hepatitis_c_elisa',
                       'rpr','vdrl','tpps','fta-abs','tp-igg','tp-igm','vdrl-csf','tppa-csf','fta-avs-csf') 
  and (t0.result_float is not null or t1.test_name in ('hepatitis_c_rna','hepatitis_c_elisa',
                       'rpr','vdrl','tpps','fta-abs','tp-igg','tp-igm','vdrl-csf','tppa-csf','fta-avs-csf'));
GO
create index tt_sublab_patid_idx on gen_pop_tools.tt_sublab (patient_id);
create index tt_sublab_testname_idx on gen_pop_tools.tt_sublab (test_name);
create index tt_sublab_date_idx on gen_pop_tools.tt_sublab (date);
UPDATE STATISTICS gen_pop_tools.tt_sublab;
GO
--
-- Max ldl lab result
--
select 'Starting to build gen_pop_tools.tt_ldl1 at: ',getdate();
IF object_id('gen_pop_tools.tt_ldl1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_ldl1;
GO
SELECT 
	  l0.patient_id
	, gen_pop_tools.dateToYyyyMm(date) year_month
	, MAX(l0.result_float) AS max_ldl1
	, max(gen_pop_tools.dateToYyyyMm(date)) over (partition by l0.patient_id) max_ym
INTO gen_pop_tools.tt_ldl1
FROM gen_pop_tools.tt_sublab l0
	WHERE l0.test_name = 'cholesterol-ldl'
	GROUP BY l0.patient_id, gen_pop_tools.dateToYyyyMm(date);
GO
--
-- Max trig lab result last year
--
select 'Starting to build gen_pop_tools.tt_trig1 at: ',getdate();
IF object_id('gen_pop_tools.tt_trig1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_trig1;
GO
SELECT 
	  l0.patient_id
	, gen_pop_tools.dateToYyyyMm(date) year_month
	, MAX(l0.result_float) AS max_trig1
	, max(gen_pop_tools.dateToYyyyMm(date)) over (partition by l0.patient_id) max_ym
INTO gen_pop_tools.tt_trig1
FROM gen_pop_tools.tt_sublab l0
	WHERE l0.test_name = 'triglycerides'
	GROUP BY 
	  l0.patient_id, gen_pop_tools.dateToYyyyMm(date);
GO
--
-- Max A1C lab result last year
--
select 'Starting to build gen_pop_tools.tt_a1c1 at: ',getdate();
IF object_id('gen_pop_tools.tt_a1c1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_a1c1;
GO
SELECT 
	  l0.patient_id
	, gen_pop_tools.dateToYyyyMm(date) year_month
	, MAX(l0.result_float) AS max_a1c1
	, max(gen_pop_tools.dateToYyyyMm(date)) over (partition by l0.patient_id) max_ym
INTO gen_pop_tools.tt_a1c1
FROM gen_pop_tools.tt_sublab l0
	WHERE l0.test_name = 'a1c'
	GROUP BY 
	  l0.patient_id, gen_pop_tools.dateToYyyyMm(date);
GO
--
-- Prediabetes
--
select 'Starting to build gen_pop_tools.tt_predm at: ',getdate();
IF object_id('gen_pop_tools.tt_predm', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_predm;
GO
SELECT max(1) predm
	, c.patient_id
	, seq.year_month
	, max(seq.year_month) over (partition by c.patient_id) max_ym
INTO gen_pop_tools.tt_predm
FROM dbo.nodis_case c
	join gen_pop_tools.tt_pat_seq_enc seq on seq.patient_id=c.patient_id
	join (select h.case_id, h.status, h.date as strtdt, 
               case 
			     when lead(h.date) over (partition by h.case_id order by h.date) is not null 
                    then lead(h.date) over (partition by h.case_id order by h.date)
				 when c.isactive=1 then getdate()
               end enddt 
	    from nodis_caseactivehistory h 
		join dbo.nodis_case c on c.id=h.case_id) cah on c.id=cah.case_id 
            AND gen_pop_tools.dateToYyyyMm(cah.strtdt) <= seq.year_month
            AND gen_pop_tools.dateToYyyyMm(cah.enddt) > seq.year_month
	WHERE c.condition = 'diabetes:prediabetes' and cah.status in ('I','R')
	group by c.patient_id, seq.year_month;
GO
--
-- Type 1 Diabetes
--
select 'Starting to build gen_pop_tools.tt_type1 at: ',getdate();
IF object_id('gen_pop_tools.tt_type1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_type1;
GO
SELECT 
	patient_id
	, gen_pop_tools.dateToYyyyMm(date) year_month
	, max(1) AS type_1_diabetes
INTO gen_pop_tools.tt_type1
FROM dbo.nodis_case
	WHERE condition = 'diabetes:type-1'
	group by patient_id, gen_pop_tools.dateToYyyyMm(date);
GO
--
-- Type 2 Diabetes
--
select 'Starting to build gen_pop_tools.tt_type2 at: ',getdate();
IF object_id('gen_pop_tools.tt_type2', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_type2;
GO
SELECT max(1) type2
	, c.patient_id
	, seq.year_month
	, max(seq.year_month) over (partition by c.patient_id) max_ym
INTO gen_pop_tools.tt_type2
FROM dbo.nodis_case c
	join gen_pop_tools.tt_pat_seq_enc seq on seq.patient_id=c.patient_id
	join (select h.case_id, h.status, h.date as strtdt, 
               case 
			     when lead(h.date) over (partition by h.case_id order by h.date) is not null 
                    then lead(h.date) over (partition by h.case_id order by h.date)
				 when c.isactive=1 then getdate()
               end enddt 
	    from nodis_caseactivehistory h 
		join dbo.nodis_case c on c.id=h.case_id) cah on c.id=cah.case_id 
            AND gen_pop_tools.dateToYyyyMm(cah.strtdt) <= seq.year_month
            AND gen_pop_tools.dateToYyyyMm(cah.enddt) > seq.year_month
	WHERE c.condition = 'diabetes:type-2' and cah.status in ('I','R')
	group by c.patient_id, seq.year_month;
GO
--
-- Insulin
--    Prescription for insulin within the previous year
--
select 'Starting to build gen_pop_tools.tt_insulin at: ',getdate();
IF object_id('gen_pop_tools.tt_insulin', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_insulin;
GO
SELECT 
	patient_id
	, max(1) AS insulin
	, gen_pop_tools.dateToYyyyMm(date) year_month
INTO gen_pop_tools.tt_insulin
FROM emr_prescription
	WHERE lower(name) LIKE '%insulin%'
	group by patient_id, gen_pop_tools.dateToYyyyMm(date);
GO
--
-- Metformin
--     Prescription for metformin within the previous year
--
select 'Starting to build gen_pop_tools.tt_metformin at: ',getdate();
IF object_id('gen_pop_tools.tt_metformin', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_metformin;
GO
SELECT 
	  patient_id
	, gen_pop_tools.dateToYyyyMm(date) year_month
	, max(1) AS metformin
INTO gen_pop_tools.tt_metformin
FROM emr_prescription
	WHERE lower(name) LIKE '%metformin%'
	group by patient_id, gen_pop_tools.dateToYyyyMm(date);
GO
--
-- Influenza vaccine
--     Prescription for influenza vaccine current flu season
--
select 'Starting to build gen_pop_tools.tt_flu_cur at: ',getdate();
IF object_id('gen_pop_tools.tt_flu_cur', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_flu_cur;
GO
SELECT 
	  patient_id
	, gen_pop_tools.dateToYyyyMm(date) year_month
	, max(1) AS influenza_vaccine
INTO  gen_pop_tools.tt_flu_cur
FROM emr_immunization
	WHERE (lower(name) LIKE '%influenza%' or name = 'flu')
	group by patient_id, gen_pop_tools.dateToYyyyMm(date);
GO
--
-- tdap vaccine
--
select 'Starting to build gen_pop_tools.tt_tdap at: ',getdate();
IF object_id('gen_pop_tools.tt_tdap_pre0', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_tdap_pre0;
GO
SELECT 
	  i.patient_id
	, gen_pop_tools.dateToYyyyMm(i.date) year_month
	, max(1) AS tdap_vaccine
INTO gen_pop_tools.tt_tdap_pre0
FROM emr_immunization i
	WHERE (lower(i.name) LIKE '%tdap%') or i.name='diphtheria, tetanus toxoids and pertussis vaccine'
	group by i.patient_id, gen_pop_tools.dateToYyyyMm(i.date);
GO

IF object_id('gen_pop_tools.tt_tdap_pre', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_tdap_pre;
GO
select t0.*
      , gen_pop_tools.age_2(CONVERT(DATE, REPLACE(t0.year_month, '_', '')+'01',112),CAST(p.date_of_birth AS DATE)) age
INTO gen_pop_tools.tt_tdap_pre
from gen_pop_tools.tt_tdap_pre0 t0 join dbo.emr_patient p on p.id=t0.patient_id;
GO

IF object_id('gen_pop_tools.tt_tdap', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_tdap;
GO
select tdapp.patient_id, pse.year_month, max(tdapp.tdap_vaccine) tdap_vaccine
INTO gen_pop_tools.tt_tdap
from gen_pop_tools.tt_pat_seq_enc pse
  join gen_pop_tools.tt_tdap_pre tdapp on pse.patient_id=tdapp.patient_id 
    and ((pse.year_month>=tdapp.year_month and tdapp.age<=18 and pse.age<=18) 
         or (pse.year_month>=tdapp.year_month and tdapp.age>18 and pse.age>18)) 
  group by tdapp.patient_id, pse.year_month;
GO
--
-- Smoking
--
select 'Starting to build gen_pop_tools.tt_smoking at: ',getdate();
IF object_id('gen_pop_tools.tt_smoking', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_smoking;
GO
select *, max(year_month) over (partition by patient_id) max_ym
into gen_pop_tools.tt_smoking
from (
select cast(max(cm.code) as varchar) smoking,
           seq.patient_id,
		   seq.year_month
from gen_pop_tools.tt_pat_seq_enc seq
left join emr_socialhistory sh on seq.patient_id=sh.patient_id and gen_pop_tools.dateToYyyyMm(sh.date)=seq.year_month
left join gen_pop_tools.rs_conf_mapping cm on cm.src_value=sh.tobacco_use and cm.src_table='emr_socialhistory' and cm.src_field='tobacco_use'
     where sh.tobacco_use is not null and sh.tobacco_use<>''
     group by seq.patient_id, seq.year_month) t0;
GO
--
-- Asthma
--
select 'Starting to build gen_pop_tools.tt_asthma at: ',getdate();
IF object_id('gen_pop_tools.tt_asthma', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_asthma;
GO
select max(1) asthma,
  c.patient_id,
  seq.year_month
	, max(seq.year_month) over (partition by c.patient_id) max_ym
INTO gen_pop_tools.tt_asthma
from dbo.nodis_case c
  join gen_pop_tools.tt_pat_seq_enc seq on seq.patient_id=c.patient_id
  join (select h.case_id, h.status, h.date as strtdt, 
               case 
			     when lead(h.date) over (partition by h.case_id order by h.date) is not null 
                    then lead(h.date) over (partition by h.case_id order by h.date)
				 when c.isactive=1 then getdate()
               end enddt 
	    from nodis_caseactivehistory h 
		join dbo.nodis_case c on c.id=h.case_id) cah on c.id=cah.case_id 
            AND gen_pop_tools.dateToYyyyMm(cah.strtdt) <= seq.year_month
            AND gen_pop_tools.dateToYyyyMm(cah.enddt) > seq.year_month
where c.condition='asthma' and cah.status in ('I','R')
group by c.patient_id, seq.year_month;
GO
--
-- events for chlamydia and gonnorhea
--
select 'Starting to build gen_pop_tools.tt_subevents at: ',getdate();
IF object_id('gen_pop_tools.tt_subevents', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_subevents;
GO
select name, patient_id, gen_pop_tools.dateToYyyyMm(date) year_month
INTO gen_pop_tools.tt_subevents
from hef_event
	where name in ('lx:gonorrhea:positive', 'lx:gonorrhea:negative', 'lx:gonorrhea:indeterminate',
	               'lx:chlamydia:positive', 'lx:chlamydia:negative', 'lx:chlamydia:indeterminate');
GO
--
-- most recent chlamydia lab result
--
select 'Starting to build gen_pop_tools.tt_chlamydia at: ',getdate();
IF object_id('gen_pop_tools.tt_chlamydia', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_chlamydia;
GO
SELECT 
	  e0.patient_id
	, e0.year_month
	, max(case when e0.name='lx:chlamydia:indeterminate' then 1
               when e0.name='lx:chlamydia:negative' then 2
               when e0.name='lx:chlamydia:positive' then 3
          end) recent_chlamydia
	, max(e0.year_month) over (partition by e0.patient_id) max_ym
INTO gen_pop_tools.tt_chlamydia
FROM gen_pop_tools.tt_subevents e0
	WHERE e0.name in ('lx:chlamydia:positive', 'lx:chlamydia:negative', 'lx:chlamydia:indeterminate')
	GROUP BY e0.patient_id, e0.year_month;
GO
--
-- Depression
--
select 'Starting to build gen_pop_tools.tt_depression at: ',getdate();
IF object_id('gen_pop_tools.tt_depression', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_depression;
GO
 select max(1) depression,
  c.patient_id,
  seq.year_month
	, max(seq.year_month) over (partition by c.patient_id) max_ym
INTO gen_pop_tools.tt_depression
from dbo.nodis_case c
  join gen_pop_tools.tt_pat_seq_enc seq on seq.patient_id=c.patient_id
  join (select h.case_id, h.status, h.date as strtdt, 
               case 
			     when lead(h.date) over (partition by h.case_id order by h.date) is not null 
                    then lead(h.date) over (partition by h.case_id order by h.date)
				 when c.isactive=1 then getdate()
               end enddt 
	    from nodis_caseactivehistory h 
		join dbo.nodis_case c on c.id=h.case_id) cah on c.id=cah.case_id 
            AND gen_pop_tools.dateToYyyyMm(cah.strtdt) <= seq.year_month
            AND gen_pop_tools.dateToYyyyMm(cah.enddt) > seq.year_month
where c.condition='depression' and cah.status in ('I','R')
group by c.patient_id, seq.year_month;
GO
--
-- Opioid
--
select 'Starting to build tt_opi at: ',getdate();
IF object_id('gen_pop_tools.tt_opioidrx', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_opioidrx;
GO
select t1.patient_id, t1.year_month
  , max(1) opioidrx  
INTO gen_pop_tools.tt_opioidrx
from dbo.esp_condition t0
join dbo.emr_patient t00 on t0.patid = t00.natural_key
join  gen_pop_tools.tt_pat_seq t1
  on t1.patient_id=t00.id 
    AND gen_pop_tools.dateToYyyyMm(t0.cond_start_date) <= t1.year_month
    AND gen_pop_tools.dateToYyyyMm(t0.cond_expire_date) > t1.year_month
where t0.condition='opioidrx'
group by t1.patient_id, t1.year_month;
GO

--simple identification of benzo prescription months per patient
IF object_id('gen_pop_tools.tt_benzorx', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_benzorx;
GO
select t1.patient_id, t1.year_month
  , max(1) benzorx  
INTO gen_pop_tools.tt_benzorx
from dbo.esp_condition t0
join dbo.emr_patient t00 on t0.patid = t00.natural_key
join  gen_pop_tools.tt_pat_seq t1
  on t1.patient_id=t00.id 
    AND gen_pop_tools.dateToYyyyMm(t0.cond_start_date) <= t1.year_month
    AND gen_pop_tools.dateToYyyyMm(t0.cond_expire_date) > t1.year_month
where t0.condition='benzodiarx'
group by t1.patient_id, t1.year_month;
GO

--months with overlapping benzo-opioid prescriptions per patient
IF object_id('gen_pop_tools.tt_opibenzorx', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_opibenzorx;
GO
select t1.patient_id, t1.year_month
  , max(1) opibenzorx  
INTO gen_pop_tools.tt_opibenzorx
from dbo.esp_condition t0
join dbo.emr_patient t00 on t0.patid = t00.natural_key
join  gen_pop_tools.tt_pat_seq t1
  on t1.patient_id=t00.id 
    AND gen_pop_tools.dateToYyyyMm(t0.cond_start_date) <= t1.year_month
    AND gen_pop_tools.dateToYyyyMm(t0.cond_expire_date) > t1.year_month
where t0.condition='benzopiconcurrent'
group by t1.patient_id, t1.year_month;
GO

--months with high opioid prescriptions (> 100 for > 90 days)
IF object_id('gen_pop_tools.tt_highopi', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_highopi;
GO
select t1.patient_id, t1.year_month
  , max(1) highopi  
INTO gen_pop_tools.tt_highopi
from dbo.esp_condition t0
join dbo.emr_patient t00 on t0.patid = t00.natural_key
join  gen_pop_tools.tt_pat_seq t1
  on t1.patient_id=t00.id 
    AND gen_pop_tools.dateToYyyyMm(t0.cond_start_date) <= t1.year_month
    AND gen_pop_tools.dateToYyyyMm(t0.cond_expire_date) > t1.year_month
where t0.condition='highopioiduse'
group by t1.patient_id, t1.year_month;
GO
--
-- most recent gonorrhea lab result
--
select 'Starting to build gen_pop_tools.tt_gonorrhea at: ',getdate();
IF object_id('gen_pop_tools.tt_gonorrhea', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_gonorrhea;
GO
SELECT 
	  e0.patient_id
	, e0.year_month
	, max(case when e0.name='lx:gonorrhea:indeterminate' then 1
               when e0.name='lx:gonorrhea:negative' then 2
               when e0.name='lx:gonorrhea:positive' then 3
          end) recent_gonorrhea
	, max(e0.year_month) over (partition by e0.patient_id) max_ym
INTO gen_pop_tools.tt_gonorrhea
FROM gen_pop_tools.tt_subevents e0
	WHERE e0.name in ('lx:gonorrhea:positive', 'lx:gonorrhea:negative', 'lx:gonorrhea:indeterminate')
	GROUP BY e0.patient_id, e0.year_month;
GO
--
-- Hypertension
--
select 'Starting to build gen_pop_tools.tt_hypertension at: ',getdate();
IF object_id('gen_pop_tools.tt_hypertension', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_hypertension;
GO
select patient_id, year_month,
	  case when hypertension = '3' then '1' else hypertension end as hypertension 
	, max(year_month) over (partition by patient_id) max_ym
INTO gen_pop_tools.tt_hypertension
from (select max(
        case
          when cah.status in ('I','R') then '3'
          when cah.status = 'D' then '2'
          else '0'
        end) as hypertension,
        c.patient_id,
        seq.year_month
     from dbo.nodis_case c
     join gen_pop_tools.tt_pat_seq_enc seq on seq.patient_id=c.patient_id
     join (select case_id, status, date as strtdt, 
               case 
                   when lead(date) over (partition by case_id order by date) is not null 
                       then lead(date) over (partition by case_id order by date)
                   else getdate()
               end enddt 
	    from nodis_caseactivehistory) cah on c.id=cah.case_id 
            AND gen_pop_tools.dateToYyyyMm(cah.strtdt) <= seq.year_month
            AND gen_pop_tools.dateToYyyyMm(cah.enddt) > seq.year_month
where c.condition='hypertension' 
group by c.patient_id, seq.year_month) t0;
GO
--
-- Diagnosed Hypertension
--
select 'Starting to build gen_pop_tools.tt_diaghypert at: ',getdate();
IF object_id('gen_pop_tools.tt_diaghypert', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_diaghypert;
GO
select patient_id, year_month,
	  case 
          when hypertension = '3' then '1' 
          when hypertension = '1' then '3'
          else hypertension
      end as hypertension 
	, max(year_month) over (partition by patient_id) max_ym
INTO gen_pop_tools.tt_diaghypert
from (select max(
        case
          when cah.status ='C' then '3'
          when cah.status = 'U' then '2'
          when cah.status = 'UK' then '1'
          else '0'
        end) as hypertension,
        c.patient_id,
        seq.year_month
     from dbo.nodis_case c
     join gen_pop_tools.tt_pat_seq_enc seq on seq.patient_id=c.patient_id
     join (select case_id, status, date as strtdt, 
               case 
                   when lead(date) over (partition by case_id order by date) is not null 
                       then lead(date) over (partition by case_id order by date)
                   else getdate()
               end enddt 
	    from nodis_caseactivehistory) cah on c.id=cah.case_id 
            AND gen_pop_tools.dateToYyyyMm(cah.strtdt) <= seq.year_month
            AND gen_pop_tools.dateToYyyyMm(cah.enddt) > seq.year_month
where c.condition='diagnosedhypertension' 
group by c.patient_id, seq.year_month) t0;
GO
--
-- ILI
--
select 'Starting to build gen_pop_tools.tt_ili_cur at: ',getdate();
IF object_id('gen_pop_tools.tt_ili_cur', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_ili_cur;
GO
select patient_id
    , gen_pop_tools.dateToYyyyMm(date) year_month
	, max(1) ili
INTO gen_pop_tools.tt_ili_cur
from dbo.nodis_case c
where c.condition='ili' 
group by patient_id, gen_pop_tools.dateToYyyyMm(date);
GO
--
-- hep c
--
select 'Starting to build gen_pop_tools.tt_hepc at: ',getdate();
IF object_id('gen_pop_tools.tt_hepc', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_hepc;
GO
SELECT 
	  sl.patient_id
    , gen_pop_tools.dateToYyyyMm(date) year_month	  
	, max(1) hepc 
	, max(gen_pop_tools.dateToYyyyMm(date)) over (partition by sl.patient_id) max_ym
INTO gen_pop_tools.tt_hepc
FROM gen_pop_tools.tt_sublab sl
	WHERE sl.test_name in ('hepatitis_c_rna','hepatitis_c_elisa')
	GROUP BY sl.patient_id, gen_pop_tools.dateToYyyyMm(date);
GO
--
-- Lyme
--
select 'Starting to build gen_pop_tools.tt_lyme at: ',getdate();
IF object_id('gen_pop_tools.tt_lyme', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_lyme;
GO
select patient_id
    , gen_pop_tools.dateToYyyyMm(date) year_month
	, max(1) lyme
INTO gen_pop_tools.tt_lyme
from dbo.nodis_case c
where c.condition='lyme'
group by patient_id, gen_pop_tools.dateToYyyyMm(date);
GO
--
-- syphilis
--
select 'Starting to build gen_pop_tools.tt_syph at: ',getdate();
IF object_id('gen_pop_tools.tt_syph', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_syph;
GO
SELECT seq.patient_id
	, seq.year_month
	, case 
	    when DATEDIFF(day, CONVERT(DATE, REPLACE(lb_lead.year_month, '_', '')+'01',112), CONVERT(DATE, REPLACE(seq.year_month, '_', '')+'01',112)) between 0 and 365 then 1
	    when DATEDIFF(day, CONVERT(DATE, REPLACE(lb_lead.year_month, '_', '')+'01',112), CONVERT(DATE, REPLACE(seq.year_month, '_', '')+'01',112)) between 366 and 730 then 2
	    when DATEDIFF(day, CONVERT(DATE, REPLACE(lb_lead.year_month, '_', '')+'01',112), CONVERT(DATE, REPLACE(seq.year_month, '_', '')+'01',112)) > 730 then 3
		else 0
	  end as syph
INTO gen_pop_tools.tt_syph
from
	(select patient_id,
	       year_month,
		   lead(year_month) over (partition by patient_id order by year_month) as next_ym
    from
    (select patient_id, 
	       gen_pop_tools.dateToYyyyMm(lb.date) year_month
	 FROM (select * from gen_pop_tools.tt_sublab 
	      where test_name in ('rpr','vdrl','tpps','fta-abs','tp-igg','tp-igm','vdrl-csf','tppa-csf','fta-avs-csf')) lb
     group by patient_id, gen_pop_tools.dateToYyyyMm(lb.date)) lb_agg) lb_lead
     right join gen_pop_tools.tt_pat_seq seq on lb_lead.patient_id=seq.patient_id 
	   and (lb_lead.next_ym>seq.year_month or lb_lead.next_ym is null) 
	   and lb_lead.year_month <=seq.year_month
		;
GO
--
-- Pertussis
--
select 'Starting to build gen_pop_tools.tt_pertussis at: ',getdate();
IF object_id('gen_pop_tools.tt_pertussis', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_pertussis;
GO
select patient_id
    , gen_pop_tools.dateToYyyyMm(date) year_month
	, max(1) pertussis
INTO gen_pop_tools.tt_pertussis
from dbo.nodis_case c
where c.condition='pertussis' 
group by patient_id, gen_pop_tools.dateToYyyyMm(date);
--
-- Cardio Risk
--
select 'Submitting the cardiac risk script'
GO
:r "/srv/esp/esp_tools/ContinuityOfCare/Cardiac_risk/cardiac-risk-prediction-historical.ms.sql"
GO
select 'Starting to build gen_pop_tools.tt_cvrisk at: ',getdate();
GO

IF object_id('gen_pop_tools.tt_cvrisk', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_cvrisk;
GO
select patient_id
    , year_month
	, case
        when cv_risk = 1 then 1
        when cv_risk < 1 and cv_risk >= .2 then 2
        when cv_risk < .2 and cv_risk >= .15 then 3
        when cv_risk < .15 and cv_risk >= .1 then 4
        when cv_risk < .1 and cv_risk >= .05 then 5
        when cv_risk < .05 then 6
      end cvrisk
INTO gen_pop_tools.tt_cvrisk
from gen_pop_tools.cc_cr_risk_score
    where cv_risk <= 1;
    
--
-- Primary Payer
--
select 'Starting to build gen_pop_tools.primary_payer at: ',getdate();
GO

IF object_id('gen_pop_tools.tt_primary_payer', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_primary_payer;
GO
select t0.patient_id, t1.year_month, 
max(t00.code) as primary_payer
into gen_pop_tools.tt_primary_payer 
from emr_encounter t0
join (select patient_id, gen_pop_tools.dateToYyyyMm(date) year_month, max(date) maxdate
      from emr_encounter where primary_payer is not null 
      group by patient_id, gen_pop_tools.dateToYyyyMm(date)) t1 on t0.patient_id=t1.patient_id and t0.date=t1.maxdate
join gen_pop_tools.rs_conf_mapping t00 on t00.src_table='emr_encounter' and t00.src_field='primary_payer' 
                  and t00.src_value = t0.primary_payer
where t0.primary_payer is not null or t0.primary_payer != ''
group by t0.patient_id, t1.year_month;
--
-- Diagnosed Diabetes
--

select 'Starting to build tt_diagdiabetes at: ',getdate();
GO
IF object_id('gen_pop_tools.tt_diagdiabetes', 'U') IS NOT NULL DROP TABLE gen_pop_tools.tt_diagdiabetes;
GO
select patient_id, year_month, diagdiab,
    max(year_month) over (partition by patient_id) max_ym
into gen_pop_tools.tt_diagdiabetes
from (select min(
        case
          when cah.status ='DIAB-W' then '0'
          when cah.status = 'DIAB-C' then '1'
          when cah.status = 'DIAB-U' then '2'
          when cah.status = 'DIAB-N' then '3'
        end) as diagdiab,
        c.patient_id,
        seq.year_month
     from nodis_case c
     join gen_pop_tools.tt_pat_seq_enc seq on seq.patient_id=c.patient_id
     join (select case_id, status, date as strtdt,
               case
                   when lead(date) over (partition by case_id order by date) is not null
                       then lead(date) over (partition by case_id order by date)
                   else getdate()
               end enddt
            from nodis_caseactivehistory) cah on c.id=cah.case_id
                                                  and format(cah.strtdt,'yyyy_MM')<=seq.year_month
                                          and format(cah.enddt, 'yyyy_MM')>=format(dateadd(month, 1, convert(date, replace(seq.year_month,'_','')+'01',112)),'yyyy_MM')
where c.condition='diabetes:diagnosed'
group by c.patient_id, seq.year_month) t0;
GO
