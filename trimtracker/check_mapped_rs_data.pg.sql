--run this to create an output file
--from the bash shell: psql -f check_mapped_rs_data.pg.sql
copy 
(select distinct primary_payer as unmapped_value, 'primary_payer' as target
from emr_encounter e
where not exists (
  select null from gen_pop_tools.rs_conf_mapping cm 
  where cm.src_table='emr_encounter'
    and cm.src_field='primary_payer'
    and cm.src_value=e.primary_payer)
union
select distinct ethnicity as unmapped_value, 'ethnicity' as target
from emr_patient p
where not exists (
  select null from gen_pop_tools.rs_conf_mapping cm 
  where cm.src_table='emr_patient'
    and cm.src_field='ethnicity'
    and cm.src_value=p.ethnicity)
union
select distinct race as unmapped_value, 'race' as target
from emr_patient p
where not exists (
  select null from gen_pop_tools.rs_conf_mapping cm 
  where cm.src_table='emr_patient'
    and cm.src_field='race'
    and cm.src_value=p.race)
union
select distinct tobacco_use as unmapped_value, 'tobacco_use' as target
from emr_socialhistory sh
where not exists (
  select null from gen_pop_tools.rs_conf_mapping cm 
  where cm.src_table='emr_socialhistory'
    and cm.src_field='tobacco_use'
    and cm.src_value=sh.tobacco_use)
order by target, unmapped_value) 
to '/tmp/unmapped.csv' csv header;
