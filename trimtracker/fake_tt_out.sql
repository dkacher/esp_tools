-- CREATE FAKE tt_out table
-- all patients
drop table if exists fake_pt1;
create table fake_pt1 as
select distinct aa from gen_pop_tools.tt_out;

-- select 200K patients, order by random
drop table if exists fake_pt2;
create table fake_pt2 as
select *
from fake_pt1
order by random() limit 200000;

-- generate ROW NUMBER and pick up other info associated with old patient id
drop table if exists fake_dta0;
create table fake_dta0 as
select t0.rnum, t1.*
from (select *, row_number () over (order by aa) AS rnum
from fake_pt2) t0
join gen_pop_tools.tt_out t1 on t0.aa = t1.aa;

-- generate sequence used as new patient id, random race ( I ended up not using the new race value in the end), order by random
drop table if exists fake_dta1;
create table fake_dta1 as
SELECT aa_1, FLOOR(RANDOM() * 6 + 1) AS ac_1 -- race value
FROM  GENERATE_SERIES (1, 200000) AS s(aa_1)
ORDER BY RANDOM();

-- join table containg new patient id with the main table containing patient info on rnum
drop table if exists fake_dta2;
create table fake_dta2 as
SELECT t0.aa_1, t0.ac_1, t1.*
FROM (SELECT 
row_number () over () AS rnum, aa_1, ac_1
FROM fake_dta1) t0
JOIN fake_dta0 t1 ON t0.rnum = t1.rnum;

-- drop columns;
drop table if exists fake_dta5;
create table fake_dta5 as
select * from fake_dta2;

ALTER TABLE fake_dta5
DROP COLUMN aa,
DROP COLUMN ac_1,
DROP COLUMN ae;

-- rename columns
ALTER TABLE fake_dta5
RENAME aa_1 TO aa;


-- split into two pools 
-- site A
drop table if exists fake_dta_a_tmp0;
create table fake_dta_a_tmp0 as
select * from fake_dta5
where aa between 1 and 100000;

ALTER TABLE fake_dta_a_tmp0
DROP COLUMN rnum;

-- generate repeated seq
drop table if exists fake_dta_a_tmp1;
create table fake_dta_a_tmp1 as
select *, case when aa%1167 = 0 then 1167 else aa%1167 end as rnum
from fake_dta_a_tmp0;

-- assign zipcode
drop table if exists fake_dta_a_tmp;
create table fake_dta_a_tmp as
SELECT t0.rnum as zip_rnum, zipcode as ae, t1.*
FROM (SELECT 
row_number () over () AS rnum, zipcode 
FROM ma_zipcodes where zipcode is not null) t0
JOIN fake_dta_a_tmp1 t1 ON t0.rnum = t1.rnum;

-- randomize encoutner count and prescriptions
drop table if exists fake_tt_a;
create table fake_tt_a as
select *, round((random()*0.08 + 0.8) * ai) as ai_1, round((random()*0.08 + 0.8) * aj) as aj_1, 
round((random()* 0.08 + 1) * bs) as bs_1, round(random())as ay_1, round(random()) as az_1, 
round(random()) as bf_1, round(random()) as bg_1, round(random()) as bh_1, round(random()) as bi_1
from fake_dta_a_tmp;

-- drop columns 
ALTER TABLE fake_tt_a
DROP COLUMN ai,
DROP COLUMN aj,
DROP COLUMN ay,
DROP COLUMN az,
DROP COLUMN bf,
DROP COLUMN bg,
DROP COLUMN bh,
DROP COLUMN bi,
DROP COLUMN bs,
DROP COLUMN zip_rnum,
DROP COLUMN rnum;

--- rename columns
ALTER TABLE fake_tt_a
RENAME ai_1 TO ai;
ALTER TABLE fake_tt_a
RENAME aj_1 TO aj;
ALTER TABLE fake_tt_a
RENAME ay_1 TO ay;
ALTER TABLE fake_tt_a
RENAME az_1 TO az;
ALTER TABLE fake_tt_a
RENAME bf_1 TO bf;
ALTER TABLE fake_tt_a
RENAME bg_1 TO bg;
ALTER TABLE fake_tt_a
RENAME bh_1 TO bh;
ALTER TABLE fake_tt_a
RENAME bi_1 TO bi;
ALTER TABLE fake_tt_a
RENAME bs_1 TO bs;


-- site B
drop table if exists fake_dta_b_tmp0;
create table fake_dta_b_tmp0 as
select (aa - 100000) as aa_1, *
from fake_dta5
where aa between 100001 and 200000;

ALTER TABLE fake_dta_b_tmp0
DROP COLUMN aa,
DROP COLUMN rnum;

ALTER TABLE fake_dta_b_tmp0
Rename aa_1 TO aa;

-- generate repeated seq
drop table if exists fake_dta_b_tmp1;
create table fake_dta_b_tmp1 as
select *, case when aa%1167 = 0 then 1167 else aa%1167 end as rnum
from fake_dta_b_tmp0;

-- assign zipcode
drop table if exists fake_dta_b_tmp;
create table fake_dta_b_tmp as
SELECT t0.rnum as zip_rnum, zipcode as ae, t1.*
FROM (SELECT 
row_number () over () AS rnum, zipcode 
FROM ma_zipcodes where zipcode is not null) t0
JOIN fake_dta_b_tmp1 t1 ON t0.rnum = t1.rnum;

-- randomize encoutner count and prescriptions
drop table if exists fake_tt_b;
create table fake_tt_b as
select *, round((random()*0.08 + 0.8) * ai) as ai_1, round((random()*0.08 + 0.8) * aj) as aj_1, 
round((random()* 0.08 + 1) * bs) as bs_1, round(random())as ay_1, round(random()) as az_1, 
round(random()) as bf_1, round(random()) as bg_1, round(random()) as bh_1, round(random()) as bi_1
from fake_dta_b_tmp;

-- drop columns 
ALTER TABLE fake_tt_b
DROP COLUMN ai,
DROP COLUMN aj,
DROP COLUMN ay,
DROP COLUMN az,
DROP COLUMN bf,
DROP COLUMN bg,
DROP COLUMN bh,
DROP COLUMN bi,
DROP COLUMN bs,
DROP COLUMN zip_rnum,
DROP COLUMN rnum;

--- rename columns:
ALTER TABLE fake_tt_b
RENAME ai_1 TO ai;
ALTER TABLE fake_tt_b
RENAME aj_1 TO aj;
ALTER TABLE fake_tt_b
RENAME ay_1 TO ay;
ALTER TABLE fake_tt_b
RENAME az_1 TO az;
ALTER TABLE fake_tt_b
RENAME bf_1 TO bf;
ALTER TABLE fake_tt_b
RENAME bg_1 TO bg;
ALTER TABLE fake_tt_b
RENAME bh_1 TO bh;
ALTER TABLE fake_tt_b
RENAME bi_1 TO bi;
ALTER TABLE fake_tt_b
RENAME bs_1 TO bs;

copy (select ab, ac, ad, ae, af, ag, ah, ai, aj, ak, al, am, an, ao, ap, aq, ar, "as", at, au, av, aw, ax, ay, az,
         ba, bb, bc, bd, be, bf, bg, bh, bi, bj, bk, bl, bm, bn, bo, bp, bq, br, bs, bt, bu, bv, bw, bx
  from fake_tt_a) 
  to '/tmp/sitea_tt_monthly_20190708.csv' with CSV delimiter ',' header;

copy (select ab, ac, ad, ae, af, ag, ah, ai, aj, ak, al, am, an, ao, ap, aq, ar, "as", at, au, av, aw, ax, ay, az,
         ba, bb, bc, bd, be, bf, bg, bh, bi, bj, bk, bl, bm, bn, bo, bp, bq, br, bs, bt, bu, bv, bw, bx
  from fake_tt_b) 
  to '/tmp/siteb_tt_monthly_20190708.csv' with CSV delimiter ',' header;






















