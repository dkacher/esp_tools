-- Function: gen_pop_tools.downfill(character varying, character varying, character varying, character varying)

-- DROP FUNCTION gen_pop_tools.downfill(character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION gen_pop_tools.downfill2(intable character varying, partby character varying, orderby character varying, col character varying, 
                                                  enddate character varying = 'year_month',  extlim int = 23)
  RETURNS void AS
$BODY$
DECLARE
  holdval varchar;
  holddt varchar;
  patid int;
  incr int;
  cursrow record;
  sqltxt varchar;
  counter int :=0;
BEGIN
--  for patid in select patient_id from gen_pop_tools.tt_pat order by patient_id
  for patid in select distinct patient_id from gen_pop_tools.tt_1stenc_pat order by patient_id
  loop
    holdval := null;
    holddt := null;
    incr := 0;
    sqltxt := 'select ' || partby || ' as partid, ' || orderby || ' as orderid, "' || col
                         || '" as updtcol, ' || enddate || ' as enddate ' 
                         || 'from ' || intable || ' where ' || partby || '=' || patid || ' order by ' 
                         || orderby;       
    for cursrow in execute sqltxt
    loop
      if (cursrow.updtcol = 0 and holdval is not null and incr < extlim) then
        sqltxt := 'update ' || intable || ' set "' || col || '"=' || holdval || ' where ' 
                          || partby || '=' || cursrow.partid || ' and ' || orderby || '='''
                          || cursrow.orderid || ''''; 
        execute sqltxt;
        if cursrow.orderid > holddt then
          incr := incr+1;
        end if;
      elsif (cursrow.updtcol > 0) then
        holdval:=cursrow.updtcol;
        holddt:=cursrow.enddate;
		incr:=0;
      else
        holdval := null;
        holddt := null;
        incr := 0;
      end if;
    end loop;
    counter := counter+1;
    if counter % 1000 = 0 then
      raise notice 'current patient count: %', counter;
    end if;
  end loop;
END;
$BODY$
LANGUAGE plpgsql;

-- Function: gen_pop_tools.fluseason(character varying, character varying, character varying, character varying)

-- DROP FUNCTION gen_pop_tools.fluseason(character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION gen_pop_tools.fluseason2(intable character varying, partby character varying, orderby character varying, col character varying)
  RETURNS void AS
$BODY$
DECLARE
  holdval varchar;
  patid int;
  cursrow record;
  sqltxt varchar;
  counter int :=0;
BEGIN
--  for patid in select patient_id from gen_pop_tools.tt_pat order by patient_id
  for patid in select distinct patient_id from gen_pop_tools.tt_1stenc_pat order by patient_id
  loop
    holdval := null;
    sqltxt := 'select ' || partby || ' as partid, ' || orderby || ' as orderid, "' || col
                         || '" as updtcol from ' || intable || ' where ' || partby || '=' || patid || ' order by ' 
                         || orderby;
    for cursrow in execute sqltxt
    loop
      if (cursrow.updtcol=0 and holdval is not null and cursrow.orderid not like '%09') then
        sqltxt := 'update ' || intable || ' set "' || col || '"=' || holdval || ' where ' 
                          || partby || '=' || cursrow.partid || ' and ' || orderby || '='''
                          || cursrow.orderid || ''''; 
        execute sqltxt;
      elsif (cursrow.updtcol >0) then
        holdval:=cursrow.updtcol;
      elsif (cursrow.orderid like '%09' and holdval is not null and cursrow.updtcol is not null) then
        holdval:='0';
      else
	    holdval := null;
	  end if;
    end loop;
    counter := counter+1;
    if counter % 1000 = 0 then
      raise notice 'current patient count: %', counter;
    end if;
  end loop;
END;
$BODY$
  LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION gen_pop_tools.calyrdown(intable character varying, partby character varying, orderby character varying, col character varying)
  RETURNS void AS
$BODY$
DECLARE
  holdval varchar;
  patid int;
  cursrow record;
  sqltxt varchar;
  counter int :=0;
BEGIN
--  for patid in select patient_id from gen_pop_tools.tt_pat order by patient_id
  for patid in select distinct patient_id from gen_pop_tools.tt_1stenc_pat order by patient_id
  loop
    holdval := null;
    sqltxt := 'select ' || partby || ' as partid, ' || orderby || ' as orderid, "' || col
                         || '" as updtcol from ' || intable || ' where ' || partby || '=' || patid || ' order by ' 
                         || orderby;
    for cursrow in execute sqltxt
    loop
      if (cursrow.updtcol=0 and holdval is not null and cursrow.orderid not like '%01') then
        sqltxt := 'update ' || intable || ' set "' || col || '"=' || holdval || ' where ' 
                          || partby || '=' || cursrow.partid || ' and ' || orderby || '='''
                          || cursrow.orderid || ''''; 
        execute sqltxt;
      elsif (cursrow.updtcol >0) then
        holdval:=cursrow.updtcol;
        sqltxt := 'update ' || intable || ' set "' || col || '"=' || holdval || ' where ' 
                          || partby || '=' || cursrow.partid || ' and ' || orderby || '='''
                          || cursrow.orderid || ''''; 
        execute sqltxt;
      else
	    holdval := null;
      end if;
      if (cursrow.orderid like '%01') then
        holdval:='0';
      end if;     
    end loop;
    counter := counter+1;
    if counter % 1000 = 0 then
      raise notice 'current patient count: %', counter;
    end if;
  end loop;
END;
$BODY$
  LANGUAGE plpgsql;
