drop table if exists gen_pop_tools.rs_conf_mapping;
drop sequence if exists gen_pop_tools.rs_conf_mapping_seq;
CREATE SEQUENCE gen_pop_tools.rs_conf_mapping_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
CREATE TABLE gen_pop_tools.rs_conf_mapping
(
  id integer NOT NULL DEFAULT nextval('gen_pop_tools.rs_conf_mapping_seq'::regclass),
  src_table character varying(50) NOT NULL,
  src_field character varying(50) NOT NULL,
  src_value character varying(2000) NOT NULL,
  mapped_value character varying(100) NOT NULL,
  code integer,
  CONSTRAINT rs_conf_mapping_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
--unique index ensures that there is one and only mapping for a table-field-value combination.
create unique index rs_conf_mapping_unique_idx
on gen_pop_tools.rs_conf_mapping (src_table,src_field,src_value);
--the conf_mapping.csv is created from conf_mapping.csv.default in the esp_tools/trimtracker repo. 
--copy conf_mapping.csv.default to a location where the postgres user has read access for the copy command to work
--everyone has access to /tmp, so that's a good place for it.
copy gen_pop_tools.rs_conf_mapping (src_table, src_field, src_value, mapped_value, code)
from '/tmp/conf_mapping.csv' delimiter ',' CSV;
--Here is additional checking for smoking status.  This is a free text field at some sites...
insert into gen_pop_tools.rs_conf_mapping (src_table, src_field, src_value, mapped_value, code)
select distinct 'emr_socialhistory' src_table, 'tobacco_use' src_field, tobacco_use as src_value, 'Current' mapped_value, 4 code
from emr_socialhistory sh
where not exists (select null from gen_pop_tools.rs_conf_mapping cm where cm.src_value=sh.tobacco_use and cm.src_table='emr_socialhistory'
                      and cm.src_field='tobacco_use')
      and (tobacco_use similar to '(improve|stress|<|>|More than|Yes|occ|Some|roll|Continue|less|Current|Still|social|half|one|pack)%'
          or tobacco_use ~ '^[0123456789]' or tobacco_use ~ '^.[0123456789]'
          or tobacco_use ilike '%smokes%'
          or tobacco_use similar to '%(cut down|continues|almost|ready to|cessation|addicted|current|contemplat)%');
insert into gen_pop_tools.rs_conf_mapping (src_table, src_field, src_value, mapped_value, code)
select distinct 'emr_socialhistory' src_table, 'tobacco_use' src_field, tobacco_use as src_value, 'Former' mapped_value, 3 code
from emr_socialhistory sh
where not exists (select null from gen_pop_tools.rs_conf_mapping cm where cm.src_value=sh.tobacco_use and cm.src_table='emr_socialhistory'
                      and cm.src_field='tobacco_use')
      and tobacco_use similar to '(quit|Quit|Previous|Used to|Former)%'
--after you create the rs_conf_mapping table and load the conf_mapping.csv data, you must run
--check_mapped_rs_data.pg.sql to find unmapped data values.  Use this to update conf_mapping.csv
--you may update and check in conf_mapping.csv.default with any additional values you find, if you think other sites may benefit.

