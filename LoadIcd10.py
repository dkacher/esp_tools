#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program reads a text file containing a list of icd10 codes from
# https://www.cms.gov/Medicare/Coding/ICD10/2020-ICD-10-CM.html
# The file contains records (lines) with fixed length fields consisting of
# The first 8 chars are for for the icd10 code, and the remainder of the line is the code description


import sys
import argparse
import psycopg2
from psycopg2.extensions import adapt

parser = argparse.ArgumentParser(description="Read a list of ICD10 codes from a text file and load them into a database.\n" +
    "Usage: LoadIcd10.py <database_url> <database_password> <input_text_file>")
parser.add_argument("url")
parser.add_argument("password")
parser.add_argument("input_text_file")
aArg = parser.parse_args()

sUrl = aArg.url
sPassword = aArg.password
sFileName = aArg.input_text_file

try:
    # connect to the database
    sError = "Could not connect to the specified database."
    conn = None
    conn = psycopg2.connect(host=sUrl, dbname='esp', user='esp', password=sPassword)
    conn.set_session(autocommit=True)
    cursor = conn.cursor()
    
    # open the input file
    sError = "Problem reading from file: " + sFileName
    file = open(sFileName, "r")
    asLine = file.readlines()
    file.close()

    # For each icd code
    for sLine in asLine:
        sCode = sLine[:3] + "." + sLine[3:8]
        sCode = sCode.strip()
        sDescription = sLine[8:].strip()
        
        # escape the description for sql
        sDescription = str(adapt(sDescription))
        
        # add the code and description to the database
        try:
            sQry = ("INSERT INTO public.static_icd10_code(code, description) VALUES('" 
                + sCode + "', " + sDescription + ");")
            cursor.execute(sQry)
        except Exception as e:
            sys.stderr.write("***" + "Error executing sql: " + sQry + "\n")
            sys.stderr.write("***" + str(e) + "\n")

except Exception as e:
    sys.stderr.write("***" + sError + "\n")
    sys.stderr.write("***" + str(e) + "\n")
    sys.exit(1)

finally:
    if conn:
        conn.close()

sys.exit(0)

