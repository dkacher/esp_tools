-- CCDA Build #1.0.2303 (1014) Updated: Wednesday January 6, 2016 at 8:00am POSTGRESQL
-- Analysis name: Obesity Reporting
-- Analysis description: Catalyst Obesity Reporting
-- Script generated for database: POSTGRESQL

--
-- Script setup section 
--

DROP VIEW IF EXISTS public.obesity_a_100033_s_100751 CASCADE;

DROP VIEW IF EXISTS public.obesity_a_100033_s_100752 CASCADE;

DROP VIEW IF EXISTS public.obesity_a_100033_s_100760 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100753 CASCADE;
DROP VIEW IF EXISTS public.obesity_a_100033_s_100753 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100754 CASCADE;
DROP VIEW IF EXISTS public.obesity_a_100033_s_100754 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100756 CASCADE;
DROP VIEW IF EXISTS public.obesity_a_100033_s_100756 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100757 CASCADE;
DROP VIEW IF EXISTS public.obesity_a_100033_s_100757 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100761 CASCADE;
DROP VIEW IF EXISTS public.obesity_a_100033_s_100761 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100762 CASCADE;
DROP VIEW IF EXISTS public.obesity_a_100033_s_100762 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100766 CASCADE;
DROP VIEW IF EXISTS public.obesity_a_100033_s_100766 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100768 CASCADE;
DROP VIEW IF EXISTS public.obesity_a_100033_s_100768 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100763 CASCADE;
DROP VIEW IF EXISTS public.obesity_a_100033_s_100763 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100758 CASCADE;
DROP VIEW IF EXISTS public.obesity_a_100033_s_100758 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100967 CASCADE;
DROP VIEW IF EXISTS public.obesity_a_100033_s_100967 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_all_encounters CASCADE;
DROP VIEW IF EXISTS public.obesity_a_all_encounters CASCADE;

DROP TABLE IF EXISTS public.obesity_a_unk_bmi_all CASCADE;
DROP VIEW IF EXISTS public.obesity_a_unk_bmi_all CASCADE;

DROP TABLE IF EXISTS public.obesity_a_unk_bmi_dates CASCADE;
DROP VIEW IF EXISTS public.obesity_a_unk_bmi_dates CASCADE;

DROP TABLE IF EXISTS public.obesity_a_bmi_to_process CASCADE;
DROP VIEW IF EXISTS public.obesity_a_bmi_to_process CASCADE;

DROP TABLE IF EXISTS public.obesity_a_bmi_new_unmeasured CASCADE;
DROP VIEW IF EXISTS public.obesity_a_bmi_new_unmeasured CASCADE;

DROP TABLE IF EXISTS public.obesity_a_bmi_last2years CASCADE;
DROP VIEW IF EXISTS public.obesity_a_bmi_last2years CASCADE;

DROP TABLE IF EXISTS ccda_bmi CASCADE;
DROP VIEW IF EXISTS ccda_bmi CASCADE;

--
-- Script body 
--

-- Step 1: Access - public.emr_encounter
CREATE VIEW public.obesity_a_100033_s_100751 AS SELECT * FROM public.emr_encounter;

-- Step 2: Access - public.emr_patient
CREATE VIEW public.obesity_a_100033_s_100752 AS SELECT * FROM public.emr_patient;

-- Step 3: Access - public.esp_condition
CREATE VIEW public.obesity_a_100033_s_100760 AS SELECT * FROM public.esp_condition;

-- Step 4: Query - Get All BMI Values For Encounters For Date Range. 
CREATE  TABLE public.obesity_a_all_encounters  WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.bmi,date date FROM  public.obesity_a_100033_s_100751 T1   
WHERE date >= (date(current_date) -integer '365');

-- Step 4: Query - Get Details For Encounters That Have  BMI must be lower than 200
CREATE  TABLE public.obesity_a_100033_s_100753  WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.bmi,date date FROM  public.obesity_a_all_encounters T1   
WHERE bmi is not null and bmi <= 200;


-- Step 5: Query - Get the most recent date for the BMI patients
CREATE  TABLE public.obesity_a_100033_s_100754  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,max(date) max_date 
FROM  public.obesity_a_100033_s_100753 T1   
GROUP BY T1.patient_id)  AS DUMMYALIAS302 ;

-- Step 6: SQL - Get the BMI for the most recent encounter for the patient
CREATE  TABLE public.obesity_a_100033_s_100756  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT DISTINCT ON (T1.patient_id) T1.patient_id,T1.date, T1.bmi
FROM public.obesity_a_100033_s_100753 T1, public.obesity_a_100033_s_100754 T2
WHERE T1.patient_id = T2.patient_id 
AND T1.date = T2.max_date)  AS DUMMYALIAS303 ;

-- Gather Details for Patients With No BMI In the Time Period
CREATE  TABLE public.obesity_a_unk_bmi_all  WITHOUT OIDS  AS 
SELECT T1.patient_id
FROM public.obesity_a_all_encounters T1
WHERE bmi is null 
GROUP BY patient_id
EXCEPT
SELECT T1.patient_id
FROM public.obesity_a_all_encounters T1
WHERE bmi is NOT null 
group by patient_id;

-- Get the most recent date for No Measured BMI patients
CREATE  TABLE public.obesity_a_unk_bmi_dates  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM (SELECT T1.patient_id,max(date) date, T2.bmi 
FROM  public.obesity_a_unk_bmi_all T1, public.obesity_a_all_encounters T2
WHERE T1.patient_id = T2.patient_id   
GROUP BY T1.patient_id, T2.bmi)  AS DUMMYALIASt302 ;

-- Union together all the BMI data (measured and not measured)
CREATE TABLE public.obesity_a_bmi_to_process WITHOUT OIDS  AS
(SELECT patient_id, date, bmi FROM public.obesity_a_100033_s_100756)
UNION (SELECT patient_id, date, bmi FROM public.obesity_a_unk_bmi_dates);


-- Step 7: Query - Get fields from patient table for population in to esp_condition. Derive condition for patient.
CREATE  TABLE public.obesity_a_100033_s_100757  WITHOUT OIDS  AS 
SELECT DISTINCT * FROM 
(SELECT case when T2.center_id is null or T2.center_id = '' then '1'::text else T2.center_id end centerid,
T2.natural_key patid,
case when bmi < 25 then 'BMI <25'  
when bmi >= 25 and bmi <30 then 'BMI >=25 and <30' 
when bmi >= 30 then 'BMI >= 30' 
when bmi is null then 'No Measured BMI' 
end condition,
date  - ('1960-01-01'::date)  date,
--this is really age at detect date. Due to age restrictions on BMI, we need to be more specific
date_part('year', age(T1.date, date_of_birth)) age_at_detect_year,
--date_part('year', T1.date) - date_part('year', date_of_birth) age_at_detect_year,
T1.date date_orig,T1.patient_id 
FROM public.obesity_a_bmi_to_process T1 
INNER JOIN public.obesity_a_100033_s_100752 T2 
ON ((T1.patient_id = T2.id) AND (T1.patient_id = T2.id)) )  AS DUMMYALIAS304 ;

-- Step 8: Query - Patients Who Do Not Already Have This Condition 
-- Exclude "No Measured BMI Entires"
CREATE  TABLE public.obesity_a_100033_s_100761  WITHOUT OIDS  AS 
SELECT T1.centerid,T1.patid,T1.condition,T1.date,T1.age_at_detect_year 
FROM public.obesity_a_100033_s_100757 T1 
LEFT OUTER JOIN public.obesity_a_100033_s_100760 T2 ON ((T1.patid = T2.patid) AND (T1.condition = T2.condition))  
WHERE  T2.patid is null
AND T1.condition != 'No Measured BMI';

-- Step 9: Query - Get New Data For Patients Who Already Have This Condition But The Data Is Over A Year Old 
-- Exclude "No Measured BMI Entires"
CREATE  TABLE public.obesity_a_100033_s_100762  WITHOUT OIDS  AS 
SELECT T1.centerid,T1.patid,T1.condition,T1.date,T1.age_at_detect_year 
FROM public.obesity_a_100033_s_100757 T1 
INNER JOIN public.obesity_a_100033_s_100760 T2 ON ((T1.patid = T2.patid) AND (T1.condition = T2.condition)) 
WHERE T1.condition != 'No Measured BMI'  
GROUP BY T1.centerid,T1.patid,T1.condition,T1.date,T1.age_at_detect_year HAVING T1.date > (max(T2.date) + 365);

-- Step 10: SQL - Derive max_date for existing BMI conditions in the esp_condition table 
CREATE  TABLE public.obesity_a_100033_s_100766  WITHOUT OIDS  AS SELECT
  *,  max(date) OVER (PARTITION BY patid) as max_date
FROM public.obesity_a_100033_s_100760
where condition ilike '%bmi%';

-- Step 11: Query - Patients Who Have Had This Condition But the Most Recent Condition Is Different
-- Exclude "No Measured BMI Entires"
CREATE  TABLE public.obesity_a_100033_s_100768  WITHOUT OIDS  AS
SELECT T1.centerid,T1.patid,T1.condition,T1.date,T1.age_at_detect_year 
FROM public.obesity_a_100033_s_100757 T1 
INNER JOIN public.obesity_a_100033_s_100766 T2 ON ((T1.patid = T2.patid))  
WHERE  T2.date =  T2.max_date and  T1.condition <>  T2.condition 
AND T1.condition != 'No Measured BMI'
and T2.patid not in (
	select T1.patid 
	from esp_condition T1, public.obesity_a_100033_s_100766 T2
	where T2.date =  T2.max_date and  T1.condition =  T2.condition
	AND T1.patid = T2.patid) ;

	
-- For efficiency of next query create a table that contains current query patients
-- who have a max bmi in the last 2 years
CREATE TABLE public.obesity_a_bmi_last2years WITHOUT OIDS AS
SELECT T2.patid 
FROM public.obesity_a_100033_s_100766 T1, public.obesity_a_100033_s_100757 T2 
WHERE T1.patid = T2.patid
AND T1.condition ilike '%bmi%' 
AND max_date > T2.date - 730;
	
	
-- We only want to assign the "No Measured BMI" condition to patients 
-- who have not had a BMI condition in the last 2 years. 
-- Otherwise, each null BMI entry will overwrite a possible recent good entry
CREATE TABLE public.obesity_a_bmi_new_unmeasured WITHOUT OIDS AS 
SELECT T1.centerid,T1.patid,T1.condition,T1.date,T1.age_at_detect_year 
FROM public.obesity_a_100033_s_100757 T1 
WHERE condition = 'No Measured BMI'
AND patid not in (SELECT patid 
                  FROM public.obesity_a_bmi_last2years); 


-- Step 12: Union - Union Together Both The New and Updated Entries (Also eliminates duplicates between new and diff conditions)
CREATE  TABLE public.obesity_a_100033_s_100763  WITHOUT OIDS  AS 
(SELECT centerid,patid,condition,date,age_at_detect_year FROM public.obesity_a_100033_s_100761) 
UNION (SELECT centerid,patid,condition,date,age_at_detect_year FROM public.obesity_a_100033_s_100762) 
UNION (SELECT centerid,patid,condition,date,age_at_detect_year FROM public.obesity_a_100033_s_100768)
UNION (SELECT centerid,patid,condition,date,age_at_detect_year FROM public.obesity_a_bmi_new_unmeasured);

-- Step 13: Query - Derive Age Groups & Critera
CREATE  TABLE public.obesity_a_100033_s_100758  WITHOUT OIDS  AS SELECT DISTINCT * FROM 
(SELECT T1.centerid,T1.patid,T1.condition,T1.date,T1.age_at_detect_year,
CASE when (age_at_detect_year <= 9) then '0-9'when (age_at_detect_year <=19) then '10-19' 
when (age_at_detect_year <=29) then '20-29' when (age_at_detect_year <=39) then '30-39' 
when (age_at_detect_year <=49) then '40-49' when (age_at_detect_year <=59) then '50-59' 
when (age_at_detect_year <=69) then '60-69' when (age_at_detect_year <=79) then '70-79' 
when (age_at_detect_year <=89) then '80-89' when (age_at_detect_year <=99) then '90-99' 
else'100+'
end age_group_10yr,
CASE when  (age_at_detect_year) <= 4  THEN  '0-4' 
when age_at_detect_year <= 9  THEN '5-9' 
when age_at_detect_year <= 14 THEN '10-14' 
when age_at_detect_year <= 19 THEN '15-19' 
when age_at_detect_year <= 24 THEN '20-24' 
when age_at_detect_year <= 29 THEN '25-29' 
when age_at_detect_year <= 34 THEN '30-34' 
when age_at_detect_year <= 39 THEN '35-39' 
when age_at_detect_year <= 44 THEN '40-44' 
when age_at_detect_year <= 49 THEN '45-49' 
when age_at_detect_year <= 54 THEN '50-54' 
when age_at_detect_year <= 59 THEN '55-59' 
when age_at_detect_year <= 64 THEN '60-64' 
when age_at_detect_year <= 69 THEN '65-69' 
when age_at_detect_year <= 74 THEN '70-74' 
when age_at_detect_year <= 79 THEN '75-79' 
when age_at_detect_year <= 84 THEN '80-84' 
when age_at_detect_year <= 89 THEN '85-89' 
when age_at_detect_year <= 94 THEN '90-94' 
when age_at_detect_year <= 99 THEN '95-99' 
ELSE '100+' 
END age_group_5yr,
CASE when age_at_detect_year <= 1  THEN '0-1' 
when age_at_detect_year <= 4 THEN '2-4' 
when age_at_detect_year <= 9 THEN '5-9' 
when age_at_detect_year <= 14 THEN '10-14' 
when age_at_detect_year <= 18 THEN '15-18' 
when age_at_detect_year <= 21 THEN '19-21' 
when age_at_detect_year <= 44 THEN '22-44' 
when age_at_detect_year <= 64 THEN '45-64' 
when age_at_detect_year <= 74 THEN '65-74' 
ELSE '75+' 
END  age_group_ms,
case when condition =  'BMI <25' then  'BMI Less Than 25' 
when condition =  'BMI >= 30' then  'BMI Greater Than or Equal To 30' 
when condition =  'BMI >=25 and <30' then  'BMI Greater Than or Equal To 25 and Less Than 30' 
when condition = 'No Measured BMI' then 'BMI Value Has Never Been Recorded'
end criteria,
'NO'::text status,
null::text notes 
FROM  public.obesity_a_100033_s_100763 T1  )  AS DUMMYALIAS305 ;

-- Step 14: Query - Filter Out Patients That Are Under the Age of 20 & Overwrite Center ID For Atrius & CHA. Needs to be removed for MLCHC.
CREATE  TABLE public.obesity_a_100033_s_100967  WITHOUT OIDS  AS 
SELECT '1'::text centerid,
T1.patid,
T1.condition,
T1.date,
T1.age_at_detect_year,
T1.age_group_10yr,
T1.age_group_5yr,
T1.age_group_ms,
T1.criteria,
T1.status,
T1.notes 
FROM  public.obesity_a_100033_s_100758 T1;   
-- Going to create for all age groups and then limit population to MDPHnet
-- and other data locations as required
--WHERE  age_at_detect_year >= 20;

-- Step 15: Save - ccda_bmi
CREATE  TABLE ccda_bmi  WITHOUT OIDS  AS SELECT * FROM public.obesity_a_100033_s_100967;

-- Populate the ESP Condition Table
INSERT INTO esp_condition (SELECT * from ccda_bmi);

--
-- Script shutdown section 
--

DROP VIEW IF EXISTS public.obesity_a_100033_s_100751 CASCADE;

DROP VIEW IF EXISTS public.obesity_a_100033_s_100752 CASCADE;

DROP VIEW IF EXISTS public.obesity_a_100033_s_100760 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100753 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100754 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100756 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100757 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100761 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100762 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100766 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100768 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100763 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100758 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_100033_s_100967 CASCADE;

DROP TABLE IF EXISTS public.obesity_a_all_encounters CASCADE;

DROP TABLE IF EXISTS public.obesity_a_unk_bmi_all CASCADE;

DROP TABLE IF EXISTS public.obesity_a_unk_bmi_dates CASCADE;

DROP TABLE IF EXISTS public.obesity_a_bmi_to_process CASCADE;

DROP TABLE IF EXISTS public.obesity_a_bmi_last2years CASCADE;

DROP TABLE IF EXISTS public.obesity_a_bmi_new_unmeasured CASCADE;


